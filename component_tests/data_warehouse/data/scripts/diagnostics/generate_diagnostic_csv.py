#!/usr/bin/env python

__author__ = 'elusterman'

"""
This set of scripts is used to create SDS CSV data for Diagnotic reports.
Currently, this supports reports from Grade 3-8 for ELA.
Further extension will generate data for Grade 3-8 for Math.
Since we want to drill down from current reports to Diagnostic reports,
we use existing SDS data to generate new SDS data. Columns from existing
data that we need are copied over to the new tables, and any 'random' data
we need generated is generated using the ColumnGenerator static methods.
"""
from report_models.vocabulary import vocabulary_columns
from report_models.comprehension import comprehension_columns
from report_models.fluency import fluency_columns
from report_models.decoding import decoding_columns
from report_models.writing import writing_columns
from report_models.survey import rm_survey_columns
from report_models.math_cluster import math_cluster_columns
from report_models.math_fluency import math_fluency_columns
from report_models.math_grade_level import math_grade_level_columns
from report_models.math_locator import math_locator_columns
from report_models.math_progression import math_progression_columns
import csv
import os

EDWARE_HOME = os.environ.get('EDWARE_HOME', '/opt/edware/')
DATA_PATH = os.path.join(
    EDWARE_HOME, 'component_tests/common/data/data_sets')
ELA_FILES = [os.path.join(DATA_PATH, f) for f in [
    'SDS_ELA_Summative_3/sds_ela_summ_data.csv',
]]
REPORT_COLUMN_MODELS = {
    'Vocabulary': vocabulary_columns,
    'Comprehension': comprehension_columns,
    'Fluency': fluency_columns,
    'Decoding': decoding_columns,
    'Writing': writing_columns,
    'Survey': rm_survey_columns,
}
MATH_FILES = [os.path.join(DATA_PATH, f) for f in [
    'SDS_MATH_Summative_03/sds_math_summ_data.csv',
    'SDS_MATH_Summative_05/sds_math_summ_data.csv',
    'SDS_MATH_Summative_08/sds_math_summ_data.csv',
]]
MATH_REPORT_COLUMN_MODELS = {
    'Cluster': math_cluster_columns,
    'Fluency': math_fluency_columns,
    'Grade Level': math_grade_level_columns,
    'Locator': math_locator_columns,
    'Progression': math_progression_columns,
}
DUPLICATE_COL = 'MultipleRecordFlag'
SAMPLE_DATA_CSV = os.path.join(EDWARE_HOME, 'component_tests/data_warehouse/data/scripts/diagnostics/test_out.csv')


class CSVCreator(object):

    def __init__(self, columns, input_fname, output_fname=None):
        """
        Creates a CSV file for a particular diagnostics report.

        Keyword arguments:
        columns -- a dict with a list of ordered columns
            e.g. {'name': 'Vocabulary', 'columns': [Column1, ..., ColumnN]}
        input_fname -- existing CSV data file from another report for which student data are based
        """
        self.columns = columns
        self.input_fname = input_fname
        self.lookup_cols = self.make_lookup_cols()
        self.output_fname = output_fname
        if not self.output_fname:
            self.output_fname = self.make_output_fname()

    def create(self):
        if not os.path.exists(os.path.dirname(self.output_fname)):
            os.makedirs(os.path.dirname(self.output_fname))
        with open(self.input_fname, 'rb') as f, open(self.output_fname, 'wb') as o, open(SAMPLE_DATA_CSV, 'rb') as s:
            fieldnames = [c.name for c in self.columns['columns']]
            self.reader = csv.DictReader(f, quotechar='"')
            self.writer = csv.DictWriter(o, fieldnames=fieldnames, quotechar='"')
            self.new_cols = list(
                set(self.writer.fieldnames) - set(self.reader.fieldnames))
            self.writer.writeheader()
            self.sample_csv = csv.DictReader(s, quotechar='"')
            for row in self.reader:
                self.process_row(row)

    def process_row(self, row):
        # This method will write 1 or 2 rows to the output CSV
        sample_row = next(self.sample_csv)
        out_row = {}
        for key, value in row.iteritems():
            if key in self.writer.fieldnames:
                # generate column (likely identity generator)
                col_val = self.generate_row(out_row, key, value, False, sample_row)
                out_row[key] = col_val
        for key in self.new_cols:
            if key in self.writer.fieldnames:
                col_val = self.generate_row(out_row, key, value, False, sample_row)
                out_row[key] = col_val
        self.writer.writerow(out_row)
        # if this row indicates there are duplicates, add a second row and
        # regenerate the new column data
        if out_row[DUPLICATE_COL] == 'Y':
            sample_row = next(self.sample_csv)
            dup_row = dict(out_row)
            for key in self.new_cols:
                if key in self.writer.fieldnames:
                    col_val = self.generate_row(
                        out_row, key, value, True, sample_row)
                    dup_row[key] = col_val
            dup_row[DUPLICATE_COL] = 'Y'
            self.writer.writerow(dup_row)

    def generate_row(self, out_row, key, value, duplicate=False, sample_row=None):
        return self.lookup_cols[key].generate(row=out_row, key=key, value=value, duplicate=duplicate, sample=sample_row, col_name=self.lookup_cols[key].name)

    def make_lookup_cols(self):
        # Need order of columns, also need fast lookup of column models
        lookup_cols = {}
        for col in self.columns['columns']:
            lookup_cols[col.name] = col
        return lookup_cols

    def make_output_fname(self):
        return self.input_fname.replace('Summative', 'Diagnostic_{0}'.format(self.columns['name'])).replace('summ', 'diag')


def main():
    # ELA
    for csv_file in ELA_FILES:
        for key, report_column_model in REPORT_COLUMN_MODELS.iteritems():
            CSVCreator(report_column_model, csv_file).create()
    # MATH
    for csv_file in MATH_FILES:
        for key, report_column_model in MATH_REPORT_COLUMN_MODELS.iteritems():
            CSVCreator(report_column_model, csv_file).create()

if __name__ == '__main__':
    main()
