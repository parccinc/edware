#!/usr/bin/env python

__author__ = 'elusterman'

from random import random, randint, shuffle


class Column(object):

    def __init__(self, name, generator):
        """
        Column model for CSVCreator

        Keyword arguments:
        name -- column heading (e.g. "ParccStudentGuid")
        generator -- function which computes and returns a value for that column in a particular row
        """
        self.name = name
        self.generator = generator

    def generate(self, *args, **kwargs):
        return self.generator(*args, **kwargs)


class ColumnGenerator(object):
    """
    The following methods generate column values given the current column model and
    the current row
    """

    @staticmethod
    def identity(*args, **kwargs):
        # Use an existing column from the data we're going through
        return kwargs['value']

    @staticmethod
    def random_int(*args, **kwargs):
        low = int(kwargs['low'])
        high = int(kwargs['high'])
        return randint(low, high)

    @staticmethod
    def random_float(*args, **kwargs):
        low = float(kwargs['low'])
        high = float(kwargs['high'])
        return low + random() * (high - low)

    @staticmethod
    def blank(*args, **kwargs):
        return ''

    @staticmethod
    def record_type(*args, **kwargs):
        return '04'

    @staticmethod
    def blank_or_y(*args, **kwargs):
        # returns Y for multiple records for this student, test, & period, else blank
        # duplicate_rate = 0.2
        # if random() < duplicate_rate:
        #     return 'Y'
        # return ''
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def asmt_year(*args, **kwargs):
        return '2014-2015'

    @staticmethod
    def asmt_grade(*args, **kwargs):
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def asmt_date(*args, **kwargs):
        if kwargs.get('duplicate') is True:
            return '2015-02-02'
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def different_date(*args, **kwargs):
        return '02-02-2015'

    @staticmethod
    def state(*args, **kwargs):
        return 'RI'

    @staticmethod
    def district_id(*args, **kwargs):
        return 'R0001'

    @staticmethod
    def district_name(*args, **kwargs):
        return 'Newport'

    @staticmethod
    def school_id(*args, **kwargs):
        return 'RN001'

    @staticmethod
    def school_name(*args, **kwargs):
        return 'Rogers High School'

    @staticmethod
    def school_name(*args, **kwargs):
        return 'Rogers High School'

    @staticmethod
    def staff_id(*args, **kwargs):
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def scale_score(*args, **kwargs):
        # return ColumnGenerator.random_int(low=kwargs['low'], high=kwargs['high'])
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def scale_score_vocab(*args, **kwargs):
        # return ColumnGenerator.scale_score(low=300, high=600)
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def scale_score_comprehension(*args, **kwargs):
        # return ColumnGenerator.scale_score(low=300, high=600)
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def IRL(*args, **kwargs):
        # return "%.1f" % ColumnGenerator.random_float(low=1.0, high=9.9)
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def passage_type(*args, **kwargs):
        # PASSAGES = ["Literary", "Informational"]
        # LOW = 0
        # HIGH = 1
        # num_passages = randint(LOW, HIGH)
        # return PASSAGES[randint(0,len(PASSAGES)-1)]
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def passage_RMM(*args, **kwargs):
        # return "%.1f" % ColumnGenerator.random_float(low=1.0, high=9.9)
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def wpm(*args, **kwargs):
        # # wcpm, accuracy, and wpm can be computed in any order,
        # # so we add special cases where we compute one from the other two
        # row = kwargs['row']
        # wcpm = row.get('WCPM')
        # accuracy = row.get('Accuracy')
        # if wcpm and accuracy:
        #     return int(wcpm * 100. / accuracy)
        # elif wcpm and not accuracy:
        #     return ColumnGenerator.random_int(low=int(wcpm), high=150)
        # return ColumnGenerator.random_int(low=30, high=150)
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def wcpm(*args, **kwargs):
        # row = kwargs['row']
        # wpm = row.get('WPM')
        # accuracy = row.get('Accuracy')
        # if wpm and accuracy:
        #     return int(accuracy * wpm / 100.)
        # elif wpm and not accuracy:
        #     return ColumnGenerator.random_int(low=30, high=int(wpm))
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def accuracy(*args, **kwargs):
        # row = kwargs['row']
        # wpm = row.get('WPM')
        # wcpm = row.get('WCPM')
        # if wpm and wcpm:
        #     return int((100. * wcpm) / wpm)
        # accuracy = ColumnGenerator.random_int(low=1, high=100)
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def expressiveness(*args, **kwargs):
        # return ColumnGenerator.random_int(low=0, high=4)
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def zero_to_one(*args, **kwargs):
        # return "%.2f" % ColumnGenerator.random_float(low=0.0, high=1.0)
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def zero_one_blank(*args, **kwargs):
        # Return 0 or 1 optionally later
        return ''

    @staticmethod
    def writing_scores(*args, **kwargs):
        # return ColumnGenerator.random_int(low=1, high=5)
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def survey_response(*args, **kwargs):
        # LETTERS = ['A','B','C','D']
        # shuffle(LETTERS)
        # num_responses = 40/randint(10,40)
        # return '.'.join(sorted(LETTERS[0:num_responses]))
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def is_calculator(*args, **kwargs):
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def code_cluster(*args, **kwargs):
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def prob_cluster(*args, **kwargs):
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def class_cluster(*args, **kwargs):
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def skill_name(*args, **kwargs):
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def scale_score_locator(*args, **kwargs):
        return kwargs['sample']['ScaleScoreMathLocator']

    @staticmethod
    def suggested_grade_level(*args, **kwargs):
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def skill_totalitems(*args, **kwargs):
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def skills_totalitems(*args, **kwargs):
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def skill_time(*args, **kwargs):
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def skill_correcttime(*args, **kwargs):
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def skill_numbercorrect(*args, **kwargs):
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def skill_numberincorrect(*args, **kwargs):
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def skills_time(*args, **kwargs):
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def skills_correcttime(*args, **kwargs):
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def skills_numbercorrect(*args, **kwargs):
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def skills_numberincorrect(*args, **kwargs):
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def cluster_grade_level(*args, **kwargs):
        return get_value_from_row(*args, **kwargs)

    @staticmethod
    def progression(*args, **kwargs):
        return get_value_from_row(*args, **kwargs)


def get_value_from_row(*args, **kwargs):
    col_name = kwargs['col_name']
    return kwargs['sample'][col_name]
