from column_generator import Column, ColumnGenerator
from generate_diagnostic_csv import CSVCreator, EDWARE_HOME
import os

__author__ = 'elusterman'

"""
This script was used to generate random data once in test_out.csv. Not Necessary
to use again. Might as well just change column names manually in the data_set files.
"""

tester_columns = {
    'name': 'Tester',
    'columns': [
        Column(name='MultipleRecordFlag', generator=ColumnGenerator.blank_or_y),
        Column(name='ResearchScore', generator=ColumnGenerator.writing_scores),
        Column(name='LiteraryAnalysisScore', generator=ColumnGenerator.writing_scores),
        Column(name='NarrativeScore', generator=ColumnGenerator.writing_scores),
        Column(name='ScaleScore', generator=ColumnGenerator.scale_score_vocab),
        Column(name='Response1', generator=ColumnGenerator.survey_response),
        Column(name='Response2', generator=ColumnGenerator.survey_response),
        Column(name='Response3', generator=ColumnGenerator.survey_response),
        Column(name='Response4', generator=ColumnGenerator.survey_response),
        Column(name='Response5', generator=ColumnGenerator.survey_response),
        Column(name='Response6', generator=ColumnGenerator.survey_response),
        Column(name='Response7', generator=ColumnGenerator.survey_response),
        Column(name='Response8', generator=ColumnGenerator.survey_response),
        Column(name='Response9', generator=ColumnGenerator.survey_response),
        Column(name='Response10', generator=ColumnGenerator.survey_response),
        Column(name='Response11', generator=ColumnGenerator.survey_response),
        Column(name='Response12', generator=ColumnGenerator.survey_response),
        Column(name='Response13', generator=ColumnGenerator.survey_response),
        Column(name='Response14', generator=ColumnGenerator.survey_response),
        Column(name='Response15', generator=ColumnGenerator.survey_response),
        Column(name='Response16', generator=ColumnGenerator.survey_response),
        Column(name='Response17', generator=ColumnGenerator.survey_response),
        Column(name='Response18', generator=ColumnGenerator.survey_response),
        Column(name='Response19', generator=ColumnGenerator.survey_response),
        Column(name='WPM', generator=ColumnGenerator.wpm),
        Column(name='WCPM', generator=ColumnGenerator.wcpm),
        Column(name='Accuracy', generator=ColumnGenerator.accuracy),
        Column(name='Expressiveness', generator=ColumnGenerator.expressiveness),
        Column(name='P1CVC', generator=ColumnGenerator.zero_to_one),
        Column(name='P2Blend', generator=ColumnGenerator.zero_to_one),
        Column(name='P3Cvow', generator=ColumnGenerator.zero_to_one),
        Column(name='P4Ccons', generator=ColumnGenerator.zero_to_one),
        Column(name='P5Msyl1', generator=ColumnGenerator.zero_to_one),
        Column(name='P6Msyl2', generator=ColumnGenerator.zero_to_one),
        Column(name='P1Class', generator=ColumnGenerator.zero_one_blank),
        Column(name='P2Class', generator=ColumnGenerator.zero_one_blank),
        Column(name='P3Class', generator=ColumnGenerator.zero_one_blank),
        Column(name='P4Class', generator=ColumnGenerator.zero_one_blank),
        Column(name='P5Class', generator=ColumnGenerator.zero_one_blank),
        Column(name='P6Class', generator=ColumnGenerator.zero_one_blank),
        Column(name='ScaleScore', generator=ColumnGenerator.scale_score_comprehension),
        Column(name='IRL', generator=ColumnGenerator.IRL),
        Column(name='Passage1Type', generator=ColumnGenerator.passage_type),
        Column(name='Passage1RMM', generator=ColumnGenerator.passage_RMM),
        Column(name='Passage2Type', generator=ColumnGenerator.passage_type),
        Column(name='Passage2RMM', generator=ColumnGenerator.passage_RMM),
        Column(name='Passage3Type', generator=ColumnGenerator.passage_type),
        Column(name='Passage3RMM', generator=ColumnGenerator.passage_RMM),
    ]
}

test_csv = os.path.join(EDWARE_HOME, 'component_tests/data_warehouse/data/scripts/diagnostics/test.csv')
test_csv_out = os.path.join(EDWARE_HOME, 'component_tests/data_warehouse/data/scripts/diagnostics/test_out.csv')
CSVCreator(tester_columns, test_csv, test_csv_out).create()
