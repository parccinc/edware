Feature: Verify existense of Analytics Data Stores

  # wip; test doesn't do much, and syntax for ping seems to have changed somehow
  @wip
  @RALLY_US30703
  Scenario: Ping the Pentaho Server
    When the Pentaho Server is ready
    Then I should be able to ping the data store    

  # wip; test doesn't do much, and syntax for ping seems to have changed somehow
  @wip
  @RALLY_US30703
  Scenario: Ping the Pentaho Utility Server
    When the Pentaho Utility Server is ready
    Then I should be able to ping the data store  
