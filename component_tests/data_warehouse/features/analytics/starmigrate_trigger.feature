Feature: Verify that the triggering mechanism for StarMigrate is functional

  @RALLY_US35526
  @RALLY_US35439
  Scenario: StarMigrate encounters a problem with bad data file
	Given we have incomplete data files
	When StarMigrate encounters a problem
	Then StarMigrate should raise an exception
	Then StarMigrate should mark the file as failed
	And provide a log message "Reading root dump archive failed with exception!"

  @RALLY_US35526
  @RALLY_US35439
  Scenario: No Trigger Files
	Given we have no trigger data files
	When StarMigrate is started
	Then StarMigrate should do nothing
	And provide a log message "no data to process"

  @RALLY_US35439
  Scenario: One Trigger File
	Given we have one trigger data files
	When StarMigrate is started
	Then StarMigrate should load that data
	And begin the star migration process

  @RALLY_US35439
  Scenario: Multiple Trigger Files
	Given we have multiple trigger data files
	When StarMigrate is started
	Then StarMigrate should load the most recent file
	And delete all the relevant files when it is done
