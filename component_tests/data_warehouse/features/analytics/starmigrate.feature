Feature: Verify the functionality of StarMigrate

  @RALLY_US31782
  @RALLY_US34688
  @RALLY_US35526
  Scenario: Verify the Functionality of StarMigrate
    Given that we have a empty Analytics DB for the second tenant
    And the expected env is properly set up
    When we add the known multi assessment input data dumpfile for second tenant
    And StarMigrate is started
    Then StarMigrate should load that data
    When Kettle completes successfully

    Then the dim_accomod table should have 5 rows in analytics schema
    And the number of columns in dim_accomod should be 46
    And the dim_school table should have 13 rows in analytics schema
    And the number of columns in dim_school should be 12
    And the dim_opt_state_data table should have 5 rows in analytics schema
    And the number of columns in dim_opt_state_data should be 19
    And the dim_poy table should have 4 rows in analytics schema
    And the number of columns in dim_poy should be 7
    And the dim_student table should have 8 rows in analytics schema
    And the number of columns in dim_student should be 34
    And the dim_test table should have 17 rows in analytics schema
    And the number of columns in dim_test should be 14
    And the dim_unique_test table should have 3 rows in analytics schema
    And the number of columns in dim_unique_test should be 7
    And the fact_sum table should have 8 rows in analytics schema
    And the number of columns in fact_sum should be 80

    And the dim_accomod table should have 2 rows where the accomod_screen_reader column is Y and accomod_asl_video column is N
    And the dim_school table should have 1 rows where the dist_name column is New York City Geographic District#4 and school_id column is TestSchoolIdenf
    And the dim_poy table should have 1 rows where the poy column is QA-Test-12345678912- and school_year column is 2016
    And the dim_student table should have 2 rows where the student_first_name column is Amanda and student_parcc_id column is PARCStudentIdentifier-QATesting12345
    And the dim_test table should have 8 rows where the test_subject column is Algebra I and _record_version column is 1
    And the fact_sum table should have 1 rows where the report_suppression_code column is 99 and me_flag column is math
    And the fact_sum table should have 4 rows when joined with dim_student on _key_student and _key_student and student_sex column is M
    And the fact_sum table should have 2 rows when joined with dim_poy on _key_poy and _key_poy and school_year column is 2016
    And the fact_sum table should have 4 rows when joined with dim_test on _key_pba_test and _key_test and test_subject column is Algebra I

   @now
   @RALLY_US35526
   Scenario: StarMigrate checking for proper row count, column count and spot checks with math assessment
    Given that we have a empty Analytics DB for the second tenant
    And the expected env is properly set up
    When we add the known math assessment input data dumpfile for second tenant
    And StarMigrate is started
    And Kettle completes successfully

    Then the dim_accomod table should have 5 rows in analytics schema
    And the dim_school table should have 13 rows in analytics schema
    And the dim_poy table should have 4 rows in analytics schema
    And the dim_student table should have 5 rows in analytics schema
    And the dim_test table should have 9 rows in analytics schema

    And the dim_school table should have 1 rows where the dist_name column is New York City Geographic District#2 and school_name column is Ps 172 Beacon School of Excellence
    # And the dim_accomod table should have 1 rows where the accomod_color_contrast column is blue and accomod_answer_rec column is Y
    And the dim_poy table should have 1 rows where the school_year column is 2016 and _record_version column is 1
    And the dim_student table should have 1 rows where the student_first_name column is James and primary_disabil_type column is AUT
    And the dim_test table should have 1 rows where the test_subject column is Algebra I and pba_category column is B
    And the fact_sum table should have 1 rows where the report_suppression_code column is 99 and me_flag column is math
    And the fact_sum table should have 2 rows when joined with dim_student on _key_student and _key_student and student_sex column is M
    And the fact_sum table should have 1 rows when joined with dim_poy on _key_poy and _key_poy and school_year column is 2016
    And the fact_sum table should have 4 rows when joined with dim_test on _key_pba_test and _key_test and test_subject column is Algebra I

   @RALLY_US35526
   Scenario: StarMigrate checking for proper row count, column count and spot checks with ela assessment
    Given that we have a empty Analytics DB for the second tenant
    And the expected env is properly set up
    When we add the known ela assessment input data dumpfile for second tenant
    And StarMigrate is started
    And Kettle completes successfully

    Then the number of columns in dim_accomod should be 46
    And the number of columns in dim_school should be 12

    And using _key_student to join the fact_sum table where report_suppression_code is 99 with the dim_student table where student_parcc_id is PARCStudentIdentifier-QATesting12346 results in 1 rows
    And the dim_accomod table should have 2 rows where the accomod_ell column is Y and accomod_braille_ela column is Y
    And the dim_school table should have 4 rows where the school_id column is TestSchoolIdenf and district_id column is TestDistrictIdn
