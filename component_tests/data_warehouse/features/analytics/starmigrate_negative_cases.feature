Feature: StarMigrate fails to migrate data when appropriate

    @wip # With new ETL approach we create DB when run kettle, so this testcase will never appear anymore
    @RALLY_US35526
    Scenario: StarMigrate will 'fail' with no proper databases exist and a msg is provided in log
      Given the analytics database does not exist
      When StarMigrate is started
      Then StarMigrate should raise an exception
      And provide a log message "Refresh Failed"
