@cat_gen
Feature: Create fixture data for cat tenant based on dog tenant, and import cat/dog, cat/dog cds, and parcc cds

  Background: Work with a clean database
    Given we have a clean second tenant's data warehouse database
    And we have a clean first tenant's data warehouse database
    Given we have a clean dog tenant cds database
    And we have a clean cat tenant cds database
    And we have a clean parcc cds database

  # The scenario below is to be run !once! to generate CSVs for cat tenant cds, and parcc cds with dog and cat tenant data
  # To run this, you should already have the data for dog tenant, dog tenant cds, and parcc cds (with dog data only),
  # already available in SDS_CSV_FILES, or you can generate the CSVs through fixture_generator.
  # development.ini can only contain one tenant for tenant-level CDS migrations, so we need to separate data generation
  # like this. development.ini should contain the following for this test:

  # cds.consortium.migrate_dest.db.parcc.schema_name = edware_cds
  # cds.consortium.migrate_dest.db.parcc.url = postgresql+psycopg2://edware:edware2013@localhost:5432/edware
  # cds.consortium.migrate_source.db.cat.schema_name = edware_tenant_cds_cat
  # cds.consortium.migrate_source.db.cat.url = postgresql+psycopg2://edware:edware2013@localhost:5432/edware
  # cds.consortium.migrate_source.db.dog.schema_name = edware_tenant_cds_dog
  # cds.consortium.migrate_source.db.dog.url = postgresql+psycopg2://edware:edware2013@localhost:5432/edware
  # cds.tenant.migrate_dest.db.cat.schema_name = edware_tenant_cds_cat
  # cds.tenant.migrate_dest.db.cat.url = postgresql+psycopg2://edware:edware2013@localhost:5432/edware
  # cds.tenant.migrate_source.db.cat.schema_name = edware_prod_cat
  # cds.tenant.migrate_source.db.cat.url = postgresql+psycopg2://edware:edware2013@localhost:5432/edware

  @local
  Scenario: CDS migrate cat data, and dump cat tenant cds, and consortium cds with cat and dog tenant data to CSV
    When I import sds data into the dog tenant
    And I import sds data into the cat tenant
    And I import tenant level sds data into the dog tenant cds
    # The following line imports dog consortium data
    And I import consortium level sds data into the cds

    Then I migrate the data for cat tenant cds in tenant mode for year 2015
    Then the cat tenant cds databases should have data in the ela assessment results table
    And that table should have 53 total rows
    And the cat tenant cds databases should have data in the math assessment results table
    And that table should have 65 total rows
    And the cat tenant cds databases should have data in the ELA RIF table
    And that table should have 119 total rows
    And the cat tenant cds databases should have data in the Math RIF table
    And that table should have 151 total rows

    Then I migrate the data for cat tenant cds in tenant mode for year 2014
    Then the cat tenant cds databases should have data in the ela assessment results table
    And that table should have 56 total rows
    And the cat tenant cds databases should have data in the math assessment results table
    And that table should have 68 total rows
    And the cat tenant cds databases should have data in the ELA RIF table
    And that table should have 119 total rows
    And the cat tenant cds databases should have data in the Math RIF table
    And that table should have 151 total rows

    Then I migrate the data for parcc cds in consortium mode for year 2015
    And the parcc cds databases should have data in the ela assessment results table
    And that table should have 111 total rows
    And the parcc cds databases should have data in the math assessment results table
    And that table should have 136 total rows
    And the parcc cds databases should have data in the ELA RIF table
    And that table should have 238 total rows
    And the parcc cds databases should have data in the Math RIF table
    And that table should have 307 total rows

    Then I migrate the data for parcc cds in consortium mode for year 2014
    And the parcc cds databases should have data in the ela assessment results table
    And that table should have 114 total rows
    And the parcc cds databases should have data in the math assessment results table
    And that table should have 139 total rows
    And the parcc cds databases should have data in the ELA RIF table
    And that table should have 238 total rows
    And the parcc cds databases should have data in the Math RIF table
    And that table should have 307 total rows
    When I take the data dump from cat prod database to CSV files
