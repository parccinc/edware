from random import randrange
__author__ = 'npandey'


def generate_xml():
    xml_skeleton = '<assessmentResult> \
        <context> \
            <!-- alt for defining the student, test, and form uniquely --> \
            <sessionIdentifier sourceID="/pearson/student" identifier="{student_id}"/> \
            <sessionIdentifier sourceID="/parcc/assessmentRegistrationRefId" identifier="{assmt_attempt}"/> \
        </context> \
        <itemResult identifier="{item_id}" datestamp="2014-05-23T19:08:00" sessionStatus="pendingResponseProcessing"> \
            <responseVariable identifier="RESPONSE1" cardinality="single" baseType="identifier"> \
                <candidateResponse> \
                    <value>C</value> \
                </candidateResponse> \
            </responseVariable> \
            <responseVariable identifier="numAttempts" cardinality="single" baseType="integer"> \
                <candidateResponse> \
                    <value>1</value> \
                </candidateResponse> \
            </responseVariable> \
            <outcomeVariable identifier="completionStatus" cardinality="single" baseType="identifier"> \
                <value>completed</value> \
            </outcomeVariable> \
            <outcomeVariable identifier="Score" cardinality="single" baseType="identifier"> \
                <value>{score}</value> \
            </outcomeVariable> \
        </itemResult> \
    </assessmentResult>'

    math_student_attempt_guids = [('W5ha3fX9kVTgGJArrfTUQoeAnWt750A7BdyvrBKn', '19489898-d469-41e2-babc-265ecbab2337'),
                                  ('2Wy9gooYhPcak00Aqmlbqe5u7vIptgFL72MpVzSP', '34b99412-fd5b-48f0-8ce8-f8ca3788634a'),
                                  ('3U0OtmjB78LYZWbshMMMCpOfAR3RpQtsxomc1xLS', '3efe8485-9c16-4381-ab78-692353104cce'),
                                  ('sAsGPgjkONoqGVHYLwh8xEIEVmgMVw9993tOF2GS', '1d4d792b-1f76-40d8-95e4-ed9bb7ff9cae'),
                                  ('DjIJ6bssvSxfRiv4yjHaxcP0Ax5Isafra6YOooRW', 'e2c4e2c0-2a2d-4572-81fb-529b511c6e8c'),
                                  ('zSRcimp3YtlNS9pEPbSfdW6PlB5xPLuKxrG0iuW9', '8f7df227-54c4-49b2-b970-645304aaf9a8'),
                                  ('rxwyAFyV19FoPAF8HcS28v9yeWDh5YO2xxJtJVqu', 'f87ede20-3da3-4f82-bfec-f2b97315dfa4'),
                                  ('aGnsGBGEthXJJ5hEiWc1WML9WXeanCzt2ryKPreT', '6d91f753-3239-4f28-a1ea-0cee24f51242'),
                                  ('jHRDslla1RpoAySXUwoPem57j9jKUq79x1NxjJ0b', '6650cae1-d542-4a10-acf5-228ee7ad2bc8'),
                                  ('DsMdpCwjjbgAC2AfrHIz6zpQkQH7hJL6gjiDDw78', '82c6aeb0-e063-4e83-87f3-9e463b16ae12'),
                                  ('MGM1XQUzYdHW5s0FHSqPPmKV2SD7b34lvzPDIxG9', '38af4bb1-89ad-442a-a88b-2a3be51aca0b'),
                                  ('QThYUAyGA8UA1AQOJSk9d33yWrEaheojyRQitC5C', '0db87cf3-8e79-424c-811d-93aacc941768'),
                                  ('kbsZ3qHkl1ouuG4ujpxVZ5MmU9Iy2tKKORH73nA1', 'e56c2221-530c-4f75-8930-b3454d2bd1e5'),
                                  ('TbebPuczmq8cjEQs4B1oa78jmgNusK00xJQiwexL', 'c93d9efe-0db3-47a5-85a6-50195e0af5f6'),
                                  ('dEjmIiufmGJdA4huGxbxdVBYisYXctCpys96DVtK', '35514d25-bb19-4f64-9a2b-d3af2f76780b'),
                                  ('ElMkLAPjlOY85B36bVmmhjBrEvcCyr9pqNJAvPRB', '45ec8192-bdce-479e-a157-f11600de4479'),
                                  ('b5diKGv2PgyBRuIYxq6g3HQmNNbVBCXAJj0wopiF', '2b4743d7-c2d5-430c-ac8a-5e0bc06669df'),
                                  ('MeDrZegRZQrdBigoGhDu9UO2kadzUjejBfrGQopk', 'e67a9825-0b31-4a8c-bcce-96aff92c0b18'),
                                  ('nef15qDZp0FkdrZYwdUfDnz9W75JGeKXDmiq5whF', 'eaf91b3a-6a66-4138-b613-ba4c249aa53d'),
                                  ('gMFYPCCUYR9lmb0B0cJPe1sgRZXJpfZJnRbOAYrG', '224c1a91-a244-46a6-bf73-0a3162a632a3')]

    ela_student_attempt_guids = [('E9fHez5Rp8SfdyyhzYgAlyLyIE1JYmpYL8d9Up4W', '720ee112-7a89-4b62-a32f-e270fa7ae6b6'),
                                 ('hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ', 'd94160df-2efb-456a-b4b0-f5e98b65c5ac'),
                                 ('zxITiM66QUVOcTyhRaooB0UIOWemGFP8eQE7xPSx', 'de886bec-4501-4426-b969-c77ac2594776'),
                                 ('nHaADPpFPr1tpkcoS2Xj5mhmvdViLrumXuJMSox3', '97f053df-baa5-4556-a9c1-ee306f8abea8'),
                                 ('LaPd7vVIU9G9NvGr44WMWgwNwvupwIyWtC5kGetF', '69aca536-56f6-4fd1-86dc-e22e69932591'),
                                 ('6xduCZW2oSUGSLzpiFpJQQK8YSAzmBrMjAh4WEkn', '9890459a-631e-4ab8-b7c4-a999346d9f7c'),
                                 ('cS2xAK3xDPCBR7ZWbAh9FS08IqjzWxgbZeitAa40', '2a0d1ab7-f625-4a42-915c-4b4a1459dd89'),
                                 ('wUfuK1C1vpevOq8cV6oqQxNNwJV1TbzeraL8fC4v', '8c5cfbdc-c166-419d-91ea-0a52d4c76f3f'),
                                 ('UmOkwomla6wQuMu8rde3f3OkVwf23neTGCISpB28', 'ba15bf2b-9e0b-4139-ab48-3a211cf6b11e'),
                                 ('Avrxze2AmndYm8dxmLJxDxhLsKpRgxaISvx3KS6F', 'e0c84d00-1583-446a-bdaa-ef9fde48d944'),
                                 ('sYbJYuJUBNDgRcwDUOr3LhFnvsw424uSYhmE0rx7', '9969bb67-2342-4e2b-9d5b-7896af498c9e'),
                                 ('Z94Y0ynyVyq3VCwLTjIRZHTZSYkFHT0lBN8meOgb', 'f91598c3-223e-40fc-98d0-acd5d4c3dd35'),
                                 ('maHNc5BQaVj9ddANVpBvFluBikLI3H06gqH3P1zi', '17163354-a065-47b2-b261-922d89f4a560'),
                                 ('r1s4BnsoqFMAxUnLv1q3GbBD9SWWz0yUXPaFp0Nd', 'd33f82b0-da8e-4914-a2b7-b3d4114e2a97'),
                                 ('haGegI5PgD6OzaQ1Mu478Og0zw693ZmLAgQTbjLb', '5fb648f6-dda2-4424-a272-40811268ddb7'),
                                 ('zoxqZgWkKoS9mT7YODBFwUZS48qNkoXzGPZjXUHi', '10036c01-f6c8-4561-ac9a-57f3b8e98367'),
                                 ('U49Ivq4Y5D2NTB0EAuHv0sKhVA90oVkCwOgJErDW', '39f29de2-052d-41d9-8cec-d99310c072fe'),
                                 ('Cnt3veDS2HW94jTAnU4bUASdLgIbP9y0lvQpQqer', '2975fd67-250d-4c07-88da-c9b7674acbe9'),
                                 ('wD6y2AQPgV3HtqM23RFtegCe56oenrPa6DdbElzI', 'a550a5fb-c7f6-41f7-8ac7-dc54ebcb2085'),
                                 ('JRAWX24sIH3Bh2KcmpYNn6ZyGr8e6p6ctkAMFRw5', '8140131b-3e72-418e-a4aa-a2f4c9197543')]

    math_item_guids = [('430a7964-b2fb-4a63-a1f3-ca735ccd6999', 10),
                       ('e6de8048-0441-413a-96c9-dd6a2011c72d', 5),
                       ('51b4276c-95a3-46b3-9ab4-f7086f604db2', 6),
                       ('96414ce4-8c8e-4ee2-b697-3f4c16f789f2', 4),
                       ('90d08456-59ce-4eda-86eb-ac882b4067dc', 5)]

    ela_item_guids = [('f6a6335d-a174-4cf2-b00e-7ffd833d24cf', 10),
                      ('38675020-3302-4e7e-8ded-6b7f98c9807a', 5),
                      ('c4a75f19-c98d-402d-8eee-3c42ec0763a3', 4),
                      ('8768bd00-d323-4ed6-ae1d-33b45391f317', 6),
                      ('b49cfae4-7c20-4bfa-a081-72d205ecce5c', 7),
                      ('0f176c21-8338-4279-a51c-bccba5079188', 3),
                      ('105bdcaf-ba34-4fa7-a636-d1f052b13780', 5),
                      ('6679d1c7-123f-4e16-80c5-d19163d29813', 10),
                      ('7edc25e9-9ed1-4dee-90a0-5e9bbc3646c1', 3),
                      ('39837494-8fe2-469a-867d-fd36346dd1af', 2),
                      ('83862331-d193-41c9-8176-3286aab73ac4', 8),
                      ('5874170c-b3cd-4de0-b837-8d9c868121ed', 5),
                      ('4615300b-d333-4792-a111-20f14e9b7d81', 10),
                      ('0f8d0a80-28a1-4dab-abcd-ac5f67b3e7d3', 2),
                      ('f83e7b2e-6d8f-4bf7-8fdc-772507cfd359', 10)]

    with open('/Users/nparoha/parcc/edware/component_tests/data_warehouse/data/data_sets/SDS_ELA_Summative_Item/new_sds_ela_summ_item_results.xml', 'w') as xml_file:
        xml_file.write('<?xml version="1.0" encoding="UTF-8"?>')
        xml_file.write('<assessmentResults xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.imsglobal.org/xsd/imsqti_result_v2p1" xsi:schemaLocation="http://www.imsglobal.org/xsd/imsqti_result_v2p1 pearsonAssessmentResults.xsd" batch="123ABC">')

        for student_guid, asmt_attempt_guid in ela_student_attempt_guids:
            for item_guid, max_score in ela_item_guids:
                xml_file.write(xml_skeleton.format(student_id=student_guid, assmt_attempt=asmt_attempt_guid, item_id=item_guid, score=randrange(0, max_score)))
        xml_file.write('</assessmentResults>')

generate_xml()
