@sds_dump_analytics_cat
Feature: Generate and dump analytics SDS data from the DW (for cat)

  @local
  Scenario: Generate and dump analytics SDS data from the DW (for cat)
    Then the first tenant's data warehouse databases should have data in the ela assessment results table
     And that table should have 99 total rows
     And that table should have 99 active rows
     And the first tenant's data warehouse databases should have data in the math assessment results table
     And that table should have 107 total rows
     And that table should have 107 active rows

  When I dump the analytics db for "cat" to csv.gz file

# TODO: Steps below don't work with ned ETL approach, we should rewrite it for new approach (04.04.2016 )
#    When I force the analytics ETL to run against cat tenant data
#    Then the dim_accomod table should have 91 rows in analytics schema
#     And the number of columns in dim_accomod should be 45
#     And the dim_institution table should have 9 rows in analytics schema
#     And the number of columns in dim_institution should be 17
#     And the dim_opt_state_data table should have 96 rows in analytics schema
#     And the number of columns in dim_opt_state_data should be 14
#     And the dim_poy table should have 3 rows in analytics schema
#     And the number of columns in dim_poy should be 8
#     And the dim_student table should have 205 rows in analytics schema
#     And the number of columns in dim_student should be 32
#     And the dim_test table should have 206 rows in analytics schema
#     And the number of columns in dim_test should be 15
#     And the dim_unique_test table should have 27 rows in analytics schema
#     And the number of columns in dim_unique_test should be 5
#     And the fact_sum table should have 206 rows in analytics schema
#     And the number of columns in fact_sum should be 100
#
#    When I dump the analytics db for cat to SQL
