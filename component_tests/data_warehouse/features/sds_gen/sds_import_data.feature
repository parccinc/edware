@sds_import

Feature: Imports the SDS Fixture Data (CSV files) into the edware_prod schema for 'dog' tenant'
  Use the fixture data CSV files to import data into edware_prod schema for 'dog' tenant

  Background: Work with a clean database
    Given we have a clean second tenant's data warehouse database
    And we have a clean first tenant's data warehouse database
    And we have a clean third tenant's data warehouse database
    Given we have a clean dog tenant cds database
    And we have a clean cat tenant cds database
    And we have a clean parcc cds database

  # If you run fixture generator, you will need to run sds_cat_gen_and_import.feature before running this test.
  # If you already have dog/cat, and cds data, then you can run this test.
  @local
  Scenario: Import SDS. (We already have cat and dog tenant cds CSV, and consortium with cat+dog data CSV, and we want to import the data to DB)
    When I import sds data into the dog tenant
    Then the second tenant's data warehouse databases should have data in the ela assessment results table
    And that table should have 103 total rows
    And that table should have 103 active rows
    Then the second tenant's data warehouse databases should have data in the math assessment results table
    And that table should have 111 total rows
    And that table should have 111 active rows
    Then the second tenant's data warehouse databases should have data in the ELA RIF table
    And that table should have 298 total rows
    And that table should have 298 active rows
    Then the second tenant's data warehouse databases should have data in the Math RIF table
    And that table should have 286 total rows
    And that table should have 286 active rows
    And the second tenant's data warehouse databases should have data in the Test Level table
    And that table should have 65 total rows
    And that table should have 65 active rows
    And the second tenant's data warehouse databases should have data in the Test Score table
    And that table should have 13 total rows
    And that table should have 13 active rows
    Then the second tenant's data warehouse databases should have data in the ELA vocabulary results table
    And that table should have 26 total rows
    And that table should have 26 active rows
    And the second tenant's data warehouse databases should have data in the ELA writing results table
    And that table should have 26 total rows
    And that table should have 26 active rows
    And the second tenant's data warehouse databases should have data in the ELA reading fluency results table
    And that table should have 26 total rows
    And that table should have 26 active rows
    And the second tenant's data warehouse databases should have data in the ELA comprehension results table
    And that table should have 26 total rows
    And that table should have 26 active rows
    And the second tenant's data warehouse databases should have data in the ELA decoding results table
    And that table should have 26 total rows
    And that table should have 26 active rows
    And the second tenant's data warehouse databases should have data in the Reader Motivation Survey results table
    And that table should have 26 total rows
    And that table should have 26 active rows
    And the second tenant's data warehouse databases should have data in the Math fluency results table
    And that table should have 13 total rows
    And that table should have 13 active rows
    And the second tenant's data warehouse databases should have data in the Math locator results table
    And that table should have 13 total rows
    And that table should have 13 active rows
    And the second tenant's data warehouse databases should have data in the Math progression results table
    And that table should have 13 total rows
    And that table should have 13 active rows
    And the second tenant's data warehouse databases should have data in the Math grade level results table
    And that table should have 13 total rows
    And that table should have 13 active rows
    And the second tenant's data warehouse databases should have data in the Math cluster results table
    And that table should have 13 total rows
    And that table should have 13 active rows

    When I import sds data into the cat tenant
    Then the first tenant's data warehouse databases should have data in the ela assessment results table
    And that table should have 99 total rows
    And the first tenant's data warehouse databases should have data in the math assessment results table
    And that table should have 107 total rows
    And the first tenant's data warehouse databases should have data in the ELA RIF table
    And that table should have 298 total rows
    And the first tenant's data warehouse databases should have data in the Math RIF table
    And that table should have 281 total rows

    When I import sds data into the fish tenant
    Then the first tenant's data warehouse databases should have data in the ela assessment results table
    And that table should have 99 total rows
    And the first tenant's data warehouse databases should have data in the math assessment results table
    And that table should have 107 total rows
    And the first tenant's data warehouse databases should have data in the ELA RIF table
    And that table should have 298 total rows
    And the first tenant's data warehouse databases should have data in the Math RIF table
    And that table should have 281 total rows

    When I import tenant level sds data into the dog tenant cds
    Then the dog tenant cds databases should have data in the ela assessment results table
    And that table should have 58 total rows
    And the dog tenant cds databases should have data in the math assessment results table
    And that table should have 71 total rows
    And the dog tenant cds databases should have data in the ELA RIF table
    And that table should have 119 total rows
    And the dog tenant cds databases should have data in the Math RIF table
    And that table should have 156 total rows

    When I import tenant level sds data into the cat tenant cds
    Then the cat tenant cds databases should have data in the ela assessment results table
    And that table should have 56 total rows
    And the cat tenant cds databases should have data in the math assessment results table
    And that table should have 68 total rows
    And the cat tenant cds databases should have data in the ELA RIF table
    And that table should have 119 total rows
    And the cat tenant cds databases should have data in the Math RIF table
    And that table should have 151 total rows

    When I import consortium level sds data into the cds
    Then the parcc cds databases should have data in the ela assessment results table
    And that table should have 114 total rows
    And the parcc cds databases should have data in the math assessment results table
    And that table should have 139 total rows
    And the parcc cds databases should have data in the ELA RIF table
    And that table should have 238 total rows
    And the parcc cds databases should have data in the Math RIF table
    And that table should have 307 total rows
