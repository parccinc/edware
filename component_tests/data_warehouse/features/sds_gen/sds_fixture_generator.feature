@sds_create_fixture

Feature: Create new SDS Fixture Data (CSV files) from the SDS Landing Zone files on local environment(only) on an On-demand basis
  This test is only meant to be run locally to generate new fixture data (SDS_CSV_FILES) whenever there is a change in SDS data
  Landing Zone files are tarred and encrypted and passed through UDL.
  UDL loads data into the staging database
  Migrate migrates data from staging to pre prod
  Take a data dump of all the tables into CSV files
  These fixture data CSV files will be used to import data into edware_prod schema

  Background: Work with a clean database
    Given an empty UDL stats table
    And an empty UDL batch table
    And we have a clean second tenant's data warehouse database
    Given we have a clean dog tenant cds database
    And we have a clean parcc cds database

  Scenario: Drop valid summative ELA & Math SDS LZ files in the landing zone and verify stats table indicates udl succeeded. Verify successful migration from staging to data warehouse
    When multiple valid summative SDS files are dropped into the second tenant's landing zone
    And multiple valid summative SDS item files are dropped into the second tenant's landing zone
    And a valid Item psychometric data file is dropped into the second tenant's landing zone
    When multiple valid diagnostic SDS files are dropped into the second tenant's landing zone
    And enough time is given for UDL to process the file for dog tenant
    Then the stats table should have data for 37 records
    Then the stats table for dog tenant should have the UDL record marked as UDL succeeded
    ##TODO: Update these step to include multiple file results
    ##And there should be 99 records in the final UDL table for math summative results
    ##And there should be 99 records in the final UDL table for ela summative results
    ##TODO: Update these step to get the schema name from udl_staging and use that to verify the data and staging schema instead of creating a new schema using the csv files
    ##Given a mock ELA summative staging schema exists for the default tenant
    ##And a mock math summative staging schema exists for the default tenant
    ##And a record exists for all the mock schemas in the UDL stats table with load status as udl complete
    When enough time is given for Migrate to process the UDL data for dog tenant
    Then the UDL stats table should have the load status for all the records as dump successful
    And the second tenant's data warehouse databases should have data in the ela assessment results table
    And that table should have 103 total rows
    And that table should have 103 active rows
    And the second tenant's data warehouse databases should have data in the math assessment results table
    And that table should have 111 total rows
    And that table should have 111 active rows
    And the second tenant's data warehouse databases should have data in the ELA RIF table
    And that table should have 298 total rows
    And that table should have 298 active rows
    And the second tenant's data warehouse databases should have data in the Math RIF table
    And that table should have 286 total rows
    And that table should have 286 active rows
    And the second tenant's data warehouse databases should have data in the Test Level table
    And that table should have 65 total rows
    And that table should have 65 active rows
    And the second tenant's data warehouse databases should have data in the Test Score table
    And that table should have 13 total rows
    And that table should have 13 active rows
    And the second tenant's data warehouse databases should have data in the Psychometric item data table
    And that table should have 19 total rows
    And that table should have 19 active rows
    Then the second tenant's data warehouse databases should have data in the ELA vocabulary results table
    And that table should have 26 total rows
    And that table should have 26 active rows
    And the second tenant's data warehouse databases should have data in the ELA writing results table
    And that table should have 26 total rows
    And that table should have 26 active rows
    And the second tenant's data warehouse databases should have data in the ELA reading fluency results table
    And that table should have 26 total rows
    And that table should have 26 active rows
    And the second tenant's data warehouse databases should have data in the ELA comprehension results table
    And that table should have 26 total rows
    And that table should have 26 active rows
    And the second tenant's data warehouse databases should have data in the ELA decoding results table
    And that table should have 26 total rows
    And that table should have 26 active rows
    And the second tenant's data warehouse databases should have data in the Reader Motivation Survey results table
    And that table should have 26 total rows
    And that table should have 26 active rows
    And the second tenant's data warehouse databases should have data in the Math fluency results table
    And that table should have 13 total rows
    And that table should have 13 active rows
    And the second tenant's data warehouse databases should have data in the Math locator results table
    And that table should have 13 total rows
    And that table should have 13 active rows
    And the second tenant's data warehouse databases should have data in the Math progression results table
    And that table should have 13 total rows
    And that table should have 13 active rows
    And the second tenant's data warehouse databases should have data in the Math grade level results table
    And that table should have 13 total rows
    And that table should have 13 active rows
    And the second tenant's data warehouse databases should have data in the Math cluster results table
    And that table should have 13 total rows
    And that table should have 13 active rows
    # CDS Migrate steps
    Then I migrate the data for dog tenant cds in tenant mode for year 2015
    And the dog tenant cds databases should have data in the ela assessment results table
    And that table should have 55 total rows
    And the dog tenant cds databases should have data in the math assessment results table
    And that table should have 68 total rows
    And the dog tenant cds databases should have data in the ELA RIF table
    And that table should have 119 total rows
    And the dog tenant cds databases should have data in the Math RIF table
    And that table should have 156 total rows

    Then I migrate the data for dog tenant cds in tenant mode for year 2014
    And the dog tenant cds databases should have data in the ela assessment results table
    And that table should have 58 total rows
    And the dog tenant cds databases should have data in the math assessment results table
    And that table should have 71 total rows
    And the dog tenant cds databases should have data in the ELA RIF table
    And that table should have 119 total rows
    And the dog tenant cds databases should have data in the Math RIF table
    And that table should have 156 total rows

    Then I migrate the data for parcc cds in consortium mode for year 2015
    And the parcc cds databases should have data in the ela assessment results table
    And that table should have 55 total rows
    And the parcc cds databases should have data in the math assessment results table
    And that table should have 68 total rows
    And the parcc cds databases should have data in the ELA RIF table
    And that table should have 119 total rows
    And the parcc cds databases should have data in the Math RIF table
    And that table should have 156 total rows

    Then I migrate the data for parcc cds in consortium mode for year 2014
    And the parcc cds databases should have data in the ela assessment results table
    And that table should have 58 total rows
    And the parcc cds databases should have data in the math assessment results table
    And that table should have 71 total rows
    And the parcc cds databases should have data in the ELA RIF table
    And that table should have 119 total rows
    And the parcc cds databases should have data in the Math RIF table
    And that table should have 156 total rows

    When I take the data dump from dog prod database to CSV files
    ##TODO: Currently ignores rpt_file_store table since the file_data results are not exported in a correct format for item level. This causes errors while importing data.
