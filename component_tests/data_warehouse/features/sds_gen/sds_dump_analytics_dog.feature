@sds_dump_analytics_dog
Feature: Generate and dump analytics SDS data from the DW (for dog)

  @local
  Scenario: Generate and dump analytics SDS data from the DW (for dog)
    Then the second tenant's data warehouse databases should have data in the ela assessment results table
     And that table should have 103 total rows
     And that table should have 103 active rows
     And the second tenant's data warehouse databases should have data in the math assessment results table
     And that table should have 111 total rows
     And that table should have 111 active rows

    When I dump the analytics db for "dog" to csv.gz file
#
#    When I force the analytics ETL to run against dog tenant data
#    Then the dim_accomod table should have 95 rows in analytics schema
#     And the number of columns in dim_accomod should be 45
#     And the dim_institution table should have 9 rows in analytics schema
#     And the number of columns in dim_institution should be 17
#     And the number of columns in dim_opt_state_data should be 14
#     And the dim_poy table should have 3 rows in analytics schema
#     And the number of columns in dim_poy should be 8
#     And the dim_student table should have 213 rows in analytics schema
#     And the number of columns in dim_student should be 32
#     And the dim_test table should have 214 rows in analytics schema
#     And the number of columns in dim_test should be 15
#     And the fact_sum table should have 214 rows in analytics schema
#     And the number of columns in fact_sum should be 100
#     And the dim_accomod table should have 9 rows where the accomod_screen_reader column is Y and accomod_asl_video column is N
#     And the dim_institution table should have 2 rows where the dist_name column is Providence and school_id column is RP003
#     And the dim_poy table should have 1 rows where the poy column is Spring and school_year column is 2014-2015
#     And the dim_student table should have 2 rows where the student_first_name column is Arnette and student_parcc_id column is 6DRp6GlmW7bu4tn1PhckfVhL9MHH85oXIaSfRJep
#     And the dim_test table should have 18 rows where the test_subject column is Algebra I and rec_version column is 1
#     And the fact_sum table should have 14 rows where the report_suppression_code column is 5 and me_flag column is Math
#     And the fact_sum table should have 108 rows when joined with dim_student on student_key and student_key and student_sex column is M
#     And the fact_sum table should have 208 rows when joined with dim_poy on poy_key and poy_key and school_year column is 2014-2015
#     And the fact_sum table should have 18 rows when joined with dim_test on eoy_test_key and test_form_key and test_subject column is Algebra I
#
#    When I dump the analytics db for dog to SQL
