Feature: UDL properly rejects files when needed

  Background: Work with empty UDL stats and batch tables
    Given an empty UDL stats table
    And an empty UDL batch table

  @RALLY_US28302
  @RALLY_US33449
  Scenario: Drop a file in which the Summative Assessment CSV is missing state abbreviation header in the landing zone and check stats table for relevant error code
    When a Summative Assessment CSV file with missing state abbreviation header is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL failed
    And the batch table indicates that UDL failed on the csv structure validation step

  @RALLY_US28302
  @RALLY_US33449
  Scenario: Drop a file in which the Summative Assessment CSV is missing testing district identifier column in the landing zone and check stats table for relevant error code
    When a Summative Assessment CSV file with missing testing district identifier column is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL failed
    And the batch table indicates that UDL failed on the csv schema validation step

  @RALLY_US31352
  @RALLY_US35021
  Scenario Outline: Drop a file in which the ELA Vocab CSV is missing <field> field data in the landing zone and check stats table for relevant error code
    When an ELA Vocab CSV file with missing <field> field data is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL failed
    And the batch table indicates that UDL failed on the csv field data validation step

  Examples: Fields
    | field                     |
    | assessment score          |
    | record type               |
    | birthdate                 |

  @RALLY_US28039
  @RALLY_US35021
  Scenario Outline: Drop a file in which the Math Diagnostic CSV is missing <field> field data in the landing zone and check stats table for relevant error code
    When a Math Diagnostic CSV file with missing <field> field data is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL failed
    And the batch table indicates that UDL failed on the csv field data validation step

  Examples: Fields
    | field                     |
    | assessment score          |
    | birthdate                 |

  @RALLY_US31351
  Scenario Outline: Drop a file in which the Diagnostic ELA Item CSV is missing <field> field data in the landing zone and check stats table for relevant error code
    When a Diagnostic ELA Item file with missing <field> field data is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL failed
    And the batch table indicates that UDL failed on the csv field data validation step

  Examples: Fields
    | field                     |
    | student guid              |
    | item guid                 |

  @RALLY_US31354
  Scenario Outline: Drop a file in which the Diagnostic Math Item CSV is missing <field> field data in the landing zone and check stats table for relevant error code
    When a Diagnostic Math Item file with missing <field> field data is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL failed
    And the batch table indicates that UDL failed on the csv field data validation step

  Examples: Fields
    | field                     |
    | student guid              |
    | item guid                 |

  @RALLY_US28302
  @RALLY_US33449
  Scenario Outline: Drop a file in which the Summative Assessment JSON is missing <key> key in the landing zone and check stats table for relevant error code
    When a Summative Assessment JSON file with missing <key> key is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL failed
    And the batch table indicates that UDL failed on the json validation step

  Examples: Keys
    | key |
    | summative period |
    | test code        |
    | test levels      |



  @RALLY_US28302
  @RALLY_US33449
  Scenario Outline: Drop a file in which the Summative Assessment JSON is missing <value> value in the landing zone and check stats table for relevant error code
    When a Summative Assessment JSON file with missing <value> value is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL failed
    And the batch table indicates that UDL failed on the json validation step

  Examples: Values
    | value |
    | summative period |
    | test code        |
    | test levels      |
    | test level cutpoint low |

  @RALLY_US31522
  Scenario Outline: Drop a file in which the Math RIF CSV is missing <field> field data in the landing zone and check stats table for relevant error code
    When an Math RIF CSV file with missing <field> field data is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL failed
    And the batch table indicates that UDL failed on the csv field data validation step

  Examples: Fields
    | field                     |
    | SingleParentItemScore     |
    | ItemMaxPoints             |
    | StateStudentIdentifier    |

  @RALLY_US31522
  Scenario Outline: Drop a file in which the ELA RIF CSV is missing <field> field data in the landing zone and check stats table for relevant error code
    When an ELA RIF CSV file with missing <field> field data is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL failed
    And the batch table indicates that UDL failed on the csv field data validation step

  Examples: Fields
    | field                     |
    | SingleParentItemScore     |
    | ItemMaxPoints             |
    | StateStudentIdentifier    |


  @RALLY_US28302
  @RALLY_US33449
  Scenario: Drop a file in which the JSON has null value for summative period in the landing zone and check stats table for relevant error code
    When a Summative Assessment JSON file with null value for summative period is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL failed
    And the batch table indicates that UDL failed on the json validation step

  @RALLY_US28779
  @RALLY_US33449
  Scenario: Drop a file in which the Summative Assessment CSV is missing the subject field
    When a file with a missing subject column in the CSV is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL failed
    And the batch table indicates that UDL failed on the load type step

  @RALLY_US28779
  @RALLY_US33449
  Scenario: Drop a file in which the CSV has un-supported subject value
    When a file with an incorrect subject value in the CSV is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL failed
    And the batch table indicates that UDL failed on the load type step

  @RALLY_US28163
  Scenario: Drop a file which is unencrypted in the landing zone and verify stats table indicates udl failed
    When an unencrypted file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL failed
    And the batch table indicates that UDL failed on the file decryption step

  @aws
  @RALLY_US28163
  Scenario: Drop a file that isn't the right type in the landing zone and verify that the file grabber ignores the file
    When an unsupported file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should not have data
    And the file is still in the landing zone

  @RALLY_US28163
  Scenario: Drop a file in which the Summative Assessment JSON is missing into the landing zone and verify stats table indicates udl failed
    When a file without a JSON is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL failed
    And the batch table indicates that UDL failed on the file unpacking step

  @RALLY_US28847
  Scenario: Drop a file that contains a mix of valid and invalid file types into the landing zone and verify stats table indicates udl failed
    When a file that contains a mix of valid and invalid types is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL failed
    And the batch table indicates that UDL failed on the file unpacking step
