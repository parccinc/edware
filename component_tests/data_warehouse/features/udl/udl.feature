Feature: UDL can load data into the database

  Background: Work with an empty UDL stats table
    Given an empty UDL stats table
    And an empty UDL batch table
    And clear backup storage for second tenant

  @RALLY_US34435
  Scenario: Drop a valid summative SDS files in the landing zone and verify backup creation
    When multiple valid summative SDS item files are dropped into the second tenant's landing zone
    And enough time is given for UDL to process the file
    Then summative SDS item backup files are created in secure location for second tenant

  Scenario: Drop a valid extracted summative ELA file in the landing zone and verify stats table indicates udl succeeded
    When a ELA Summative extracted dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 23 records in the final UDL table for ELA summative results

  Scenario: Drop a valid summative ELA spring 2016 format file in the landing zone and verify stats table indicates udl succeeded
    When a ELA Summative 2016 file dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 6 records in the final UDL table for ELA summative results

  @RALLY_US28163
  @RALLY_US28460
  @RALLY_US30343
  @RALLY_US34518
  @RALLY_US33449
  Scenario: Drop a valid summative ELA file in the landing zone and verify stats table indicates udl succeeded
    When a valid ELA summative file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 12 records in the final UDL table for ELA summative results
    And that table should have the correct report suppression flags set
    | seq |record_type | summative_flag | suppression_code | suppression_action | in_roster    |parcc_flag  | state_flag  | district_flag  | school_flag  | roster_flag  | isr_flag |
    | 1   | 01         | Y              | 01               | 01                 |              | False      | False       | False          | False        | N            | False    |
    | 2   | 01         | Y              | 01               | 02                 |              | False      | False       | False          | False        | Y            | True     |
    | 3   | 01         | Y              | 01               | 03                 |              | False      | False       | False          | False        | W            | False    |
    | 4   | 01         | Y              | 01               | 04                 |              | True       | True        | True           | True         | N            | False    |
    | 5   | 01         | Y              | 01               | 05                 |              | False      | False       | False          | False        | N            | True     |
    | 6   | 01         | Y              | 01               | 06                 |              | True       | True        | True           | False        | Y            | True     |
    | 7   | 01         | N              |                  |                    |              | False      | False       | False          | False        | N            | False    |
    | 8   | 01         | Y              | 12               |                    |              | True       | True        | True           | True         | Y            | True     |
    | 9   | 02         |                |                  |                    | N            | False      | False       | False          | False        | N            | False    |
    | 10  | 02         |                |                  |                    | Y            | False      | False       | False          | False        | W            | False    |
    And that table should have 6 records where the student's first name column is "James"
    And that table should have 1 records where the parcc student id column is "PARCStudentIdentifier-QATesting12341"
    And there should be 1 records in the final UDL table for test score metadata
    And that table should have 1 records where the test code column is "QA-Test-12345678912-"
    And there should be 5 records in the final UDL table for test levels metadata
    And that table should have 1 records where the cutpoint label column is "minimal"

  @JIRA_PARCC_127
  Scenario: Drop a valid summative ELA file in OLD FORMAT in the landing zone and verify stats table indicates udl succeeded
    When a valid ELA summative file in OLD FORMAT is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 12 records in the final UDL table for ELA summative results
    And that table should have the correct report suppression flags set
    | seq |record_type | summative_flag | suppression_code | suppression_action | in_roster    |parcc_flag  | state_flag  | district_flag  | school_flag  | roster_flag  | isr_flag |
    | 1   | 01         | Y              | 01               | 01                 |              | False      | False       | False          | False        | N            | False    |
    | 2   | 01         | Y              | 01               | 02                 |              | False      | False       | False          | False        | Y            | True     |
    | 3   | 01         | Y              | 01               | 03                 |              | False      | False       | False          | False        | W            | False    |
    | 4   | 01         | Y              | 01               | 04                 |              | True       | True        | True           | True         | N            | False    |
    | 5   | 01         | Y              | 01               | 05                 |              | False      | False       | False          | False        | N            | True     |
    | 6   | 01         | Y              | 01               | 06                 |              | True       | True        | True           | False        | Y            | True     |
    | 7   | 01         | N              |                  |                    |              | False      | False       | False          | False        | N            | False    |
    | 8   | 01         | Y              | 12               |                    |              | True       | True        | True           | True         | Y            | True     |
    | 9   | 02         |                |                  |                    | N            | False      | False       | False          | False        | N            | False    |
    | 10  | 02         |                |                  |                    | Y            | False      | False       | False          | False        | W            | False    |
    And that table should have 6 records where the student's first name column is "James"
    And that table should have 1 records where the parcc student id column is "PARCStudentIdentifier-QATesting12341"
    And there should be 1 records in the final UDL table for test score metadata
    And that table should have 1 records where the test code column is "QA-Test-12345678912-"
    And there should be 5 records in the final UDL table for test levels metadata
    And that table should have 1 records where the cutpoint label column is "minimal"


  @RALLY_US28163
  @RALLY_US28460
  @RALLY_US30343
  @RALLY_US34518
  @RALLY_US33449
  Scenario: Drop a valid summative Math file in the landing zone and verify stats table indicates udl succeeded
    When a valid Math Summative file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 4 records in the final UDL table for math summative results
    And that table should have 2 records where the student's first name column is "James"
    And that table should have the correct report suppression flags set
    | seq |record_type | summative_flag | suppression_code | suppression_action | in_roster    |parcc_flag  | state_flag  | district_flag  | school_flag  | roster_flag  | isr_flag |
    | 1   | 01         | Y              | 01               | 06                 |              | True       | True        | True           | False        | Y            | True     |
    And that table should have 4 records where the subject column is "Algebra I"
    And there should be 1 records in the final UDL table for test score metadata
    And that table should have 1 records where the test code column is "QA-Math-12345678912-"
    And there should be 5 records in the final UDL table for test levels metadata


  @RALLY_US28163
  Scenario: Drop multiple valid files in the landing zone and verify stats table indicates udl succeeded for all the files
    When multiple valid files are dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded

  @RALLY_US38793
  @RALLY_US28042
  @RALLY_US34530
  Scenario: Drop a valid psychometric file in the landing zone and verify stats table indicates udl succeeded
    When a valid psychometric file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 100 records in the final UDL table for psychometric data
    And that table should have 12 records where the item delivery mode column is "PBT"
    And that table should have 4 records where the third psychometric evidence statement column is "RL 4.4.1"
    And that table should have 4 records where the third psychometric standard column is "RL.4.04"

  @aws @wip
  @RALLY_US29574
  @RALLY_US33449
  Scenario: Drop files into multiple tenants and verify right tables get populated
    When a valid ELA summative file is dropped into the first tenant's landing zone
    And a valid Math Summative file is dropped into the second tenant's landing zone
    And enough time is given for UDL to process the file
    Then the stats table should have data for 2 records
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 4 records in the first tenant's final UDL table for ELA summative results
    And that table should have 2 records where the student's first name column is "James"
    And there should be 1 records in the first tenant's final UDL table for test score metadata
    And that table should have 1 records where the test code column is "QA-Test-12345678912-"
    And there should be 5 records in the first tenant's final UDL table for test levels metadata
    And there should be 4 records in the second tenant's final UDL table for math summative results
    And that table should have 2 records where the student's first name column is "James"
    And there should be 1 records in the second tenant's final UDL table for test score metadata
    And that table should have 1 records where the test code column is "QA-Test-12345678912-"
    And there should be 5 records in the second tenant's final UDL table for test levels metadata

  @RALLY_US28041
  Scenario: Drop a valid MYA Math file in the landing zone and verify stats table indicates udl succeeded
    When a valid MYA Math file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 5 records in the final UDL table for MYA math
    And that table should have 4 records where the assessment format column is "P"
    And there should be 1 records in the final UDL table for assessment levels
    And there should be 1 records in the final UDL table for assessment metadata
    And there should be 1 records in the final UDL table for assessment admin
    And there should be 1 records in the final UDL table for assessment forms
    And there should be 4 records in the final UDl table for form groups
    And there should be 4 records in the final UDL table for MYA items

  @RALLY_US28041
  Scenario: Drop a valid MYA ELA file in the landing zone and verify stats table indicates udl succeeded
    When a valid MYA ELA file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 5 records in the final UDL table for MYA ELA
    And that table should have 1 records where the assessment format column is "O"
    And there should be 2 records in the final UDL table for assessment levels
    And there should be 1 records in the final UDL table for assessment metadata
    And there should be 1 records in the final UDL table for assessment admin
    And there should be 1 records in the final UDL table for assessment forms
    And there should be 7 records in the final UDl table for form groups
    And there should be 7 records in the final UDL table for MYA items

  @RALLY_US28041
  Scenario: Drop a valid MYA Math item file in the landing zone and indicates udl succeeded
    When a valid MYA Math item file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 10 records in the final UDL table for MYA math item
    And that table should have 1 records where the student's item score column is "79"

  @RALLY_US28041
  Scenario: Drop a valid MYA ELA item file in the landing zone and verify stats table indicates udl succeeded
    When a valid MYA ELA item file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 10 records in the final UDL table for MYA ELA item
    And that table should have 2 records where the student's item score column is "89"

  @RALLY_US28040
  Scenario: Drop a valid S&L Assessment file in the landing zone and verify stats table indicates udl succeeded
    When a valid S&L Assessment file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 5 records in the final UDL table for S&L results
    And that table should have 3 records where the assessment location column is "School Testing Room 1"
    And there should be 3 records in the final UDL table for S&L specific assessment metadata
    And that table should have 1 records where the first evidence statement column is "a.1"
    And there should be 4 records in the final UDL table for S&L assessment mode
    And that table should have 3 records where the assessment mode column is "2"
    And there should be 4 records in the final UDL table for S&L task metadata
    And that table should have 2 records where the task description column is "Performance"
    And there should be 1 records in the final UDL table for assessment admin
    And that table should have 1 records where the year column is "2014"
    And there should be 5 records in the final UDL table for assessment levels
    And that table should have 1 records where the assessment level designation column is "4"

  @RALLY_US28040
  Scenario: Drop a valid S&L Task file in the landing zone and verify stats table indicates udl succeeded
    When a valid S&L Task file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 100 records in the final UDL table for S&L student tasks scores
    And that table should have 3 records where the student's overall task score column is "97"

  @RALLY_US31351
  Scenario: Drop a valid Item ELA Reading Comprehension file in the landing zone and verify stats table indicates udl succeeded
    When a valid Item ELA Reading Comprehension file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 5 records in the final UDL table for Item ELA reading comprehension
    And that table should have 2 records where the student's item score column is "2"

  @RALLY_US31351
  Scenario: Drop a valid Item ELA Decoding file in the landing zone and verify stats table indicates udl succeeded
    When a valid Item ELA Decoding file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 6 records in the final UDL table for Item ELA decoding
    And that table should have 3 records where the student's item score column is "1"

  @RALLY_US31351
  Scenario: Drop a valid Item ELA Vocabulary file in the landing zone and verify stats table indicates udl succeeded
    When a valid Item ELA Vocabulary file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 7 records in the final UDL table for Item ELA vocabulary
    And that table should have 2 records where the student's item score column is "0"

  @RALLY_US31352
  @RALLY_US35021
  Scenario: Drop a valid ELA Vocabulary file in the landing zone and verify stats table indicates udl succeeded
    When a valid ELA Vocabulary file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 4 records in the final UDL table for ELA vocabulary
    And that table should have 1 records where the student's first name column is "Joella"

  @RALLY_US31354
  Scenario: Drop a valid Item Math Comprehension file in the landing zone and verify stats table indicates udl succeeded
    When a valid Item Math Comprehension file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 5 records in the final UDL table for Item Math comprehension
    And that table should have 3 records where the student's item score column is "1"

  @RALLY_US31354
  Scenario: Drop a valid Item Math Progression file in the landing zone and verify stats table indicates udl succeeded
    When a valid Item Math Progression file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 5 records in the final UDL table for Item Math progression
    And that table should have 3 records where the student's item score column is "1"

  @RALLY_US31354
  Scenario: Drop a valid Item Math Fluency file in the landing zone and verify stats table indicates udl succeeded
    When a valid Item Math Fluency file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 5 records in the final UDL table for Item Math fluency
    And that table should have 3 records where the student's item score column is "1"

  @RALLY_US30240
  @RALLY_US35021
  Scenario: Drop a valid ELA Decoding file in the landing zone and verify stats table indicates udl succeeded
    When a valid ELA Decoding file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 3 records in the final UDL table for ELA decoding
    And that table should have 1 records where the student's first name column is "Lois"

  @RALLY_US30240
  @RALLY_US35021
  Scenario: Drop a valid ELA Comprehension file in the landing zone and verify stats table indicates udl succeeded
    When a valid ELA Comprehension file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 6 records in the final UDL table for ELA comprehension
    And that table should have 1 records where the student's first name column is "Thurman"

  @RALLY_US30240
  @RALLY_US35021
  Scenario: Drop a valid ELA Reading Fluency file in the landing zone and verify stats table indicates udl succeeded
    When a valid ELA Reading Fluency file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 5 records in the final UDL table for ELA reading fluency
    And that table should have 1 records where the student's first name column is "Debby"

  @RALLY_US30240
  @RALLY_US35021
  Scenario: Drop a valid ELA Writing file in the landing zone and verify stats table indicates udl succeeded
    When a valid ELA Writing file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 4 records in the final UDL table for ELA writing
    And that table should have 1 records where the student's first name column is "Loni"

  @RALLY_US28039
  @RALLY_US35021
  Scenario: Drop a valid Math Locator file in the landing zone and verify stats table indicates udl succeeded
    When a valid Math Locator file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 4 records in the final UDL table for Math locator
    And that table should have 1 records where the student's first name column is "Chase"

  @RALLY_US28039
  @RALLY_US35021
  Scenario: Drop a valid Math Fluency file in the landing zone and verify stats table indicates udl succeeded
    When a valid Math Fluency file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 4 records in the final UDL table for Math fluency
    And that table should have 1 records where the student's first name column is "Roman"

  @RALLY_US28039
  @RALLY_US35021
  Scenario: Drop a valid Math Progression file in the landing zone and verify stats table indicates udl succeeded
    When a valid Math Progression file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 5 records in the final UDL table for Math progression
    And that table should have 1 records where the student's first name column is "David"

  @RALLY_US35021
  Scenario: Drop a valid Math Grade Level file in the landing zone and verify stats table indicates udl succeeded
    When a valid Math Grade Level file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 4 records in the final UDL table for Math grade level
    And that table should have 1 records where the student's first name column is "Christopher"

  @RALLY_US35021
  Scenario: Drop a valid Math Cluster file in the landing zone and verify stats table indicates udl succeeded
    When a valid Math Cluster file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 6 records in the final UDL table for Math cluster
    And that table should have 1 records where the student's first name column is "Cathy"

  @RALLY_US31405
  @RALLY_US35021
  Scenario: Drop a valid Reader Motivation Survey file in the landing zone and verify stats table indicates udl succeeded
    When a valid Reader Motivation Survey file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 3 records in the final UDL table for Reader Motivation Survey
    And that table should have 1 records where the student's first name column is "Lois"

  @RALLY_US31522
  Scenario: Drop a valid ELA RIF file in the landing zone and verify stats table indicates udl succeeded
    When a valid ELA RIF file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 4 records in the final UDL table for ELA RIF
    And that table should have 1 records where the parcc student id column is "parccid1213"
    And that table should have 4 records where the assessment subject column is "English Language Arts/Literacy"

  @RALLY_US31522
  Scenario: Drop a valid Math RIF file in the landing zone and verify stats table indicates udl succeeded
    When a valid Math RIF file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 4 records in the final UDL table for Math RIF
    And that table should have 1 records where the parcc student id column is "parccid2213"
    And that table should have 4 records where the assessment subject column is "Mathematics"

  @RALLY_US38793
  @RALLY_US33008
  Scenario: Drop a psychometric file with new metadata in the landing zone and verify stats table indicates udl succeeded
    When a valid item metadata psychometric file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 50 records in the final UDL table for psychometric data