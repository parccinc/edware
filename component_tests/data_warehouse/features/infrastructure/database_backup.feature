Feature: Ingested data can be backed up and restored

  @US30538 @aws 
  Scenario: Drop a valid MYA Math file in the landing zone and verify stats table indicates udl succeeded
    Given an empty UDL stats table
    And an empty UDL batch table
    When a valid MYA Math file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 5 records in the final UDL table for MYA math
    And that table should have 4 records where the assessment format column is "P"
    And there should be 1 records in the final UDL table for assessment levels
    And there should be 1 records in the final UDL table for assessment metadata
    And there should be 1 records in the final UDL table for assessment admin
    And there should be 1 records in the final UDL table for assessment forms
    And there should be 4 records in the final UDL table for form groups
    And there should be 4 records in the final UDL table for MYA items

  @US30703 @aws
  Scenario: Take a backup of the database, restore from it and verify if data has been restored
    When the data backup process is initiated
    When the test_schema schema does not exist in the database
    When a backup file is created for a database and exists in the S3 bucket
    And a new test_schema schema is created
    When a database is restored from the backup file
    And a test_schema schema does not exist in the database after restoration
    And a edware_prod schema exists in the database after restoration
    And a backup file does not exist in the S3 bucket
