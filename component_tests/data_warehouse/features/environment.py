import configparser
import os
import re
from unittest import TestCase
from sqlalchemy import create_engine
from components_tests.common import get_formatted_postgres_url
from components_tests.flaky_tests_handler.flaky_tests_handler import handle_failed_feature

__author__ = 'smuhit'

# USE: behave -D BEHAVE_DEBUG_ON_ERROR         (to enable  debug-on-error)
# USE: behave -D BEHAVE_DEBUG_ON_ERROR=yes     (to enable  debug-on-error)
# USE: behave -D BEHAVE_DEBUG_ON_ERROR=no      (to disable debug-on-error)
BEHAVE_DEBUG_ON_ERROR = False


def setup_debug_on_error(userdata):
    global BEHAVE_DEBUG_ON_ERROR
    BEHAVE_DEBUG_ON_ERROR = userdata.getbool("BEHAVE_DEBUG_ON_ERROR")


def before_all(context):
    config = configparser.ConfigParser()
    config_file_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', 'test.ini'))
    if not os.path.exists(config_file_path):
        raise Exception("could not find test configuration: %s. perhaps run generate_ini_for_tests.sh?" % (config_file_path,))

    config.read(config_file_path)

    config_values = dict(config.items('test'))

    context.tc = TestCase()
    setup_debug_on_error(context.config.userdata)
    for key, value in config_values.items():
        setattr(context, key, value)


def after_step(context, step):
    if BEHAVE_DEBUG_ON_ERROR and step.status == "failed":
        # -- ENTER DEBUGGER: Zoom in on failure location.
        # NOTE: Use IPython debugger, same for pdb (basic python debugger).
        import ipdb
        ipdb.post_mortem(step.exc_traceback)


def after_feature(context, feature):
    if 'UDL can load data into the database' == feature.name:
        staging_db_user = context.staging_db_user
        staging_db_password = context.staging_db_password
        staging_db_server = context.staging_rds_server
        staging_db = context.staging_db
        staging_port = context.staging_rds_port

        edware_url = get_formatted_postgres_url(staging_db_user, staging_db_password,
                                                staging_db_server, staging_port, staging_db)
        edware_engine = create_engine(edware_url)

        with edware_engine.connect() as conn:
            schemas = conn.execute("select schema_name from information_schema.schemata")
            for schema in schemas:
                schema_name = schema[0]
                if re.match('^\w{8}-\w{4}-\w{4}-\w{4}-\w{12}$', schema_name):
                    conn.execute('drop schema "{schema}" cascade'.format(schema=schema_name))

    if feature.status == 'failed':
        rerun_mode = context.config.userdata.getbool('rerun_mode')
        handle_failed_feature(feature, rerun_mode)
