Feature: UDL can load generated data into the database

  Background: Work with an empty UDL stats table
    Given an empty UDL stats table
    And an empty UDL batch table

  @RALLY_US28159
  Scenario: Generate a valid math summative file in the landing zone and verify stats table indicates udl succeeded
    When a valid summative assessment math file with 100 records is generated using the data generator
    And the generated file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 100 records in the final UDL table for math summative results

  @RALLY_US28159
  Scenario: Generate a valid ela summative file in the landing zone and verify stats table indicates udl succeeded
    When a valid summative assessment ela file with 100 records is generated using the data generator
    And the generated file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 100 records in the final UDL table for ELA summative results

  @RALLY_US28159
  Scenario: Generate a valid psychometric file in the landing zone and verify stats table indicates udl succeeded
    When a valid summative psychometric file with 100 records is generated using the data generator
    And the generated file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 100 records in the final UDL table for psychometric data

  @RALLY_US28160
  Scenario: Generate a valid item file in the landing zone and verify udl succeeded
    When a valid summative item file with 100 records is generated using the data generator
    And the generated file is dropped into the landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    And the stats table should have the UDL record marked as UDL succeeded
    And there should be 100 records in the final UDL table for student item results
