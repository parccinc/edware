Feature: Migrate fails to migrate data from staging to data warehouse when appropriate

  @RALLY_US28303
  Scenario: Add dummy UDL ingested record and verify migration failed because of no relevant staging schema.
    Given no mock staging schema exists
    And a record exists for all the mock schemas in the UDL stats table with load status as udl complete
    When enough time is given for Migrate to process the UDL data
    Then the UDL stats table should have the load status for all the records as migrate failed

  @RALLY_US28303
  Scenario: Add mock UDL ingested data and verify migration failed because ingested data isn't supported by data warehouse
    Given a mock unsupported staging schema exists for the default tenant
    And a record exists for all the mock schemas in the UDL stats table with load status as udl complete
    When enough time is given for Migrate to process the UDL data
    Then the UDL stats table should have the load status for all the records as migrate failed
