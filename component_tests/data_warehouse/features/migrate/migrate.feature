
Feature: Migrate can migrate data from staging to data warehouse

  Background: Work with an empty data warehouse database for each scenario
    Given we have a clean data warehouse database
    Given an empty UDL stats table
    And an empty UDL batch table


  @RALLY_US28303
  Scenario: Add mock UDL ingested data and verify successful migration from staging to data warehouse
    Given a mock ELA summative staging schema exists for the default tenant
    And a record exists for all the mock schemas in the UDL stats table with load status as udl complete
    When enough time is given for Migrate to process the UDL data than we check data dump for the default tenant
    Then the UDL stats table should have the load status for all the records as dump successful
    # Then edmigrate should complete a data dump file for the default tenant
    When enough time is given for the slave database servers to replicate from master
    Then the data warehouse databases should have data in the ela assessment results table
    And that table should have 12 total rows
    And that table should have 12 active rows
    And the data warehouse databases should have data in the assessment test score table
    And that table should have 1 total rows
    And that table should have 1 active rows
    And the data warehouse databases should have data in the assessment test level table
    And that table should have 3 total rows
    And that table should have 3 active rows

  @RALLY_US28303
  @RALLY_US28780
  Scenario: Add multiple mock UDL ingested data and verify successful migration from staging to data warehouse
    Given a mock ELA summative staging schema exists for the default tenant
    And a mock math summative staging schema exists for the default tenant
    And a mock Math RIF staging schema exists for the default tenant
    And a mock ELA RIF staging schema exists for the default tenant
    And a mock psychometric staging schema exists for the default tenant
    And a mock reader motivation survey staging schema exists for the default tenant
    And a mock MYA Math staging schema exists for the default tenant
    And a mock MYA ELA staging schema exists for the default tenant
    And a mock MYA Math item staging schema exists for the default tenant
    And a mock MYA ELA item staging schema exists for the default tenant
    And a mock S&L assessment staging schema exists for the default tenant
    And a mock S&L student task staging schema exists for the default tenant
    And a mock Item ELA Reading Comprehension staging schema exists for the default tenant
    And a mock Item ELA Decoding staging schema exists for the default tenant
    And a mock Item ELA Vocabulary staging schema exists for the default tenant
    And a mock Item Math Comprehension staging schema exists for the default tenant
    And a mock Item Math Fluency staging schema exists for the default tenant
    And a mock Item Math Progression staging schema exists for the default tenant
    And a mock ELA Vocabulary staging schema exists for the default tenant
    And a mock ELA Reading Fluency staging schema exists for the default tenant
    And a mock ELA Comprehension staging schema exists for the default tenant
    And a mock ELA Decoding staging schema exists for the default tenant
    And a mock ELA Writing staging schema exists for the default tenant
    And a mock Math Fluency staging schema exists for the default tenant
    And a mock Math Locator staging schema exists for the default tenant
    And a mock Math Progression staging schema exists for the default tenant
    And a mock Math Grade Level staging schema exists for the default tenant
    And a mock Math Cluster staging schema exists for the default tenant
    And a record exists for all the mock schemas in the UDL stats table with load status as udl complete
    When enough time is given for Migrate to process the UDL data than we check data dump for the default tenant
    Then the UDL stats table should have the load status for all the records as dump successful
    # Then edmigrate should complete a data dump file for the default tenant
    When enough time is given for the slave database servers to replicate from master
    Then the data warehouse databases should have data in the ela assessment results table
    And that table should have 12 total rows
    And that table should have 12 active rows
    And the data warehouse databases should have data in the ELA RIF table
    And that table should have 5 total rows
    And that table should have 5 active rows
    And the data warehouse databases should have data in the Math RIF table
    And that table should have 5 total rows
    And that table should have 5 active rows
    And the data warehouse databases should have data in the assessment test score table
    And that table should have 2 total rows
    And that table should have 2 active rows
    And the data warehouse databases should have data in the assessment test level table
    And that table should have 6 total rows
    And that table should have 6 active rows
    And the data warehouse databases should have data in the math assessment results table
    And that table should have 12 total rows
    And that table should have 12 active rows
    And the data warehouse databases should have data in the assessment admin table
    And that table should have 3 total rows
    And that table should have 3 active rows
    And the data warehouse databases should have data in the assessment metadata table
    And that table should have 2 total rows
    And that table should have 2 active rows
    And the data warehouse databases should have data in the assessment levels table
    And that table should have 8 total rows
    And that table should have 8 active rows
    And the data warehouse databases should have data in the assessment forms table
    And that table should have 2 total rows
    And that table should have 2 active rows
    And the data warehouse databases should have data in the form groups table
    And that table should have 11 total rows
    And that table should have 11 active rows
    And the data warehouse databases should have data in the psychometric data table
    And that table should have 100 total rows
    And that table should have 100 active rows
    And the data warehouse databases should have data in the MYA math table
    And that table should have 5 total rows
    And that table should have 5 active rows
    And the data warehouse databases should have data in the MYA ELA table
    And that table should have 5 total rows
    And that table should have 5 active rows
    And the data warehouse databases should have data in the MYA items table
    And that table should have 11 total rows
    And that table should have 11 active rows
    And the data warehouse databases should have data in the MYA math item table
    And that table should have 5 total rows
    And that table should have 5 active rows
    And the data warehouse databases should have data in the MYA ELA item table
    And that table should have 4 total rows
    And that table should have 4 active rows
    And the data warehouse databases should have data in the S&L assessment results table
    And that table should have 5 total rows
    And that table should have 5 active rows
    And the data warehouse databases should have data in the S&L assessment metadata table
    And that table should have 3 total rows
    And that table should have 3 active rows
    And the data warehouse databases should have data in the S&L assessment mode table
    And that table should have 4 total rows
    And that table should have 4 active rows
    And the data warehouse databases should have data in the S&L task metadata table
    And that table should have 4 total rows
    And that table should have 4 active rows
    And the data warehouse databases should have data in the S&L student task results table
    And that table should have 100 total rows
    And that table should have 100 active rows
    And the data warehouse databases should have data in the Item ELA Decoding table
    And that table should have 6 total rows
    And that table should have 6 active rows
    And the data warehouse databases should have data in the Item ELA Vocabulary table
    And that table should have 7 total rows
    And that table should have 7 active rows
    And the data warehouse databases should have data in the Item ELA Reading Comprehension table
    And that table should have 5 total rows
    And that table should have 5 active rows
    And the data warehouse databases should have data in the Item Math Comprehension table
    And that table should have 5 total rows
    And that table should have 5 active rows
    And the data warehouse databases should have data in the Item Math Fluency table
    And that table should have 5 total rows
    And that table should have 5 active rows
    And the data warehouse databases should have data in the Item Math Progression table
    And that table should have 5 total rows
    And that table should have 5 active rows
    And the data warehouse databases should have data in the ELA vocabulary results table
    And that table should have 4 total rows
    And that table should have 4 active rows
    And the data warehouse databases should have data in the ELA reading fluency results table
    And that table should have 5 total rows
    And that table should have 5 active rows
    And the data warehouse databases should have data in the ELA comprehension results table
    And that table should have 6 total rows
    And that table should have 6 active rows
    And the data warehouse databases should have data in the ELA decoding results table
    And that table should have 3 total rows
    And that table should have 3 active rows
    And the data warehouse databases should have data in the ELA writing results table
    And that table should have 4 total rows
    And that table should have 4 active rows
    And the data warehouse databases should have data in the Math fluency results table
    And that table should have 4 total rows
    And that table should have 4 active rows
    And the data warehouse databases should have data in the Math locator results table
    And that table should have 4 total rows
    And that table should have 4 active rows
    And the data warehouse databases should have data in the Math progression results table
    And that table should have 5 total rows
    And that table should have 5 active rows
    And the data warehouse databases should have data in the Math grade level results table
    And that table should have 4 total rows
    And that table should have 4 active rows
    And the data warehouse databases should have data in the Math cluster results table
    And that table should have 6 total rows
    And that table should have 6 active rows
    And the data warehouse databases should have data in the Reader Motivation Survey results table
    And that table should have 3 total rows
    And that table should have 3 active rows

  @RALLY_US28303
  @RALLY_US28780
  Scenario: Add multiple mock UDL ingested data that overwrite some of each other's data and verify successful migration from staging to data warehouse
    Given a mock ELA summative staging schema exists for the default tenant
    And a mock second ELA summative staging schema exists for the default tenant
    And a mock ELA RIF staging schema exists for the default tenant
    And a mock second ELA RIF staging schema exists for the default tenant
    And a mock psychometric staging schema exists for the default tenant
    And a mock second psychometric staging schema exists for the default tenant
    And a mock reader motivation survey staging schema exists for the default tenant
    And a mock second reader motivation survey staging schema exists for the default tenant
    And a mock MYA Math staging schema exists for the default tenant
    And a mock second MYA Math staging schema exists for the default tenant
    And a mock MYA ELA item staging schema exists for the default tenant
    And a mock second MYA ELA item staging schema exists for the default tenant
    And a mock S&L assessment staging schema exists for the default tenant
    And a mock S&L student task staging schema exists for the default tenant
    And a mock second S&L assessment staging schema exists for the default tenant
    And a mock second S&L student task staging schema exists for the default tenant
    And a mock Item ELA Decoding staging schema exists for the default tenant
    And a mock Item Math Comprehension staging schema exists for the default tenant
    And a mock second Item Math Comprehension staging schema exists for the default tenant
    And a mock second Item ELA Decoding staging schema exists for the default tenant
    And a mock ELA Vocabulary staging schema exists for the default tenant
    And a mock second ELA Vocabulary staging schema exists for the default tenant
    And a mock Math Fluency staging schema exists for the default tenant
    And a mock second Math Fluency staging schema exists for the default tenant
    And a record exists for all the mock schemas in the UDL stats table with load status as udl complete
    When enough time is given for Migrate to process the UDL data than we check data dump for the default tenant
    Then the UDL stats table should have the load status for all the records as dump successful
    # Then edmigrate should complete a data dump file for the default tenant
    When enough time is given for the slave database servers to replicate from master
    Then the data warehouse databases should have data in the ela assessment results table
    And that table should have 24 total rows
    And that table should have 12 active rows
    And the data warehouse databases should have data in the ELA RIF table
    And that table should have 10 total rows
    And that table should have 8 active rows
    And the data warehouse databases should have data in the assessment admin table
    And that table should have 4 total rows
    And that table should have 2 active rows
    And the data warehouse databases should have data in the assessment metadata table
    And that table should have 2 total rows
    And that table should have 1 active rows
    And the data warehouse databases should have data in the assessment levels table
    And that table should have 12 total rows
    And that table should have 6 active rows
    And the data warehouse databases should have data in the assessment forms table
    And that table should have 2 total rows
    And that table should have 1 active rows
    And the data warehouse databases should have data in the form groups table
    And that table should have 8 total rows
    And that table should have 4 active rows
    And the data warehouse databases should have data in the psychometric data table
    And that table should have 160 total rows
    And that table should have 100 active rows
    And the data warehouse databases should have data in the MYA math table
    And that table should have 8 total rows
    And that table should have 6 active rows
    And the data warehouse databases should have data in the MYA items table
    And that table should have 8 total rows
    And that table should have 4 active rows
    And the data warehouse databases should have data in the MYA ELA item table
    And that table should have 7 total rows
    And that table should have 5 active rows
    And the data warehouse databases should have data in the S&L assessment results table
    And that table should have 8 total rows
    And that table should have 7 active rows
    And the data warehouse databases should have data in the S&L assessment metadata table
    And that table should have 6 total rows
    And that table should have 3 active rows
    And the data warehouse databases should have data in the S&L assessment mode table
    And that table should have 8 total rows
    And that table should have 4 active rows
    And the data warehouse databases should have data in the S&L task metadata table
    And that table should have 8 total rows
    And that table should have 4 active rows
    And the data warehouse databases should have data in the S&L student task results table
    And that table should have 111 total rows
    And that table should have 101 active rows
    And the data warehouse databases should have data in the Item ELA Decoding table
    And that table should have 9 total rows
    And that table should have 7 active rows
    And the data warehouse databases should have data in the Item Math Comprehension table
    And that table should have 7 total rows
    And that table should have 6 active rows
    And the data warehouse databases should have data in the ELA vocabulary results table
    And that table should have 6 total rows
    And that table should have 5 active rows
    And the data warehouse databases should have data in the Math fluency results table
    And that table should have 6 total rows
    And that table should have 5 active rows
    And the data warehouse databases should have data in the Reader Motivation Survey results table
    And that table should have 5 total rows
    And that table should have 4 active rows
    And the data warehouse databases should have data in the assessment test score table
    And that table should have 2 total rows
    And that table should have 2 active rows
    And the data warehouse databases should have data in the assessment test level table
    And that table should have 6 total rows
    And that table should have 4 active rows


  @aws @wip
  @RALLY_US29574
  Scenario: Add multiple mock UDL ingested schemas for different tenants and verify that dump successfully moves data to the correct databases
    Given we have a clean second tenant's data warehouse database
    And a mock ELA summative staging schema exists for the first tenant
    And a mock second ELA summative staging schema exists for the second tenant
    And a record exists for all the mock schemas in the UDL stats table with load status as udl complete
    When enough time is given for Migrate to process the UDL data than we check data dump for the first tenant
    Then the UDL stats table should have the load status for all the records as dump successful
    # Then edmigrate should complete a data dump file for the first tenant
    When enough time is given for the slave database servers to replicate from master
    Then the first tenant's data warehouse databases should have data in the ela assessment results table
    And that table should have 4 total rows
    And that table should have 4 active rows
    And the second tenant's data warehouse databases should have data in the ela assessment results table
    And that table should have 2 total rows
    And that table should have 2 active rows

  @RALLY_US32336
  Scenario: CDS migration - tenant level
    Given we have a clean second tenant's data warehouse database
    And we have a clean dog tenant cds database
    When I import sds data into the dog tenant
    Then I migrate the data for dog tenant cds in tenant mode for year 2015
    And the dog tenant cds databases should have data in the ela assessment results table
    And that table should have 55 total rows
    And the dog tenant cds databases should have data in the math assessment results table
    And that table should have 68 total rows
    And the dog tenant cds databases should have data in the ELA RIF table
    And that table should have 119 total rows
    And the dog tenant cds databases should have data in the Math RIF table
    And that table should have 156 total rows
    Then I migrate the data for dog tenant cds in tenant mode for year 2012
    And the dog tenant cds databases should have data in the ela assessment results table
    And that table should have 55 total rows
    And the dog tenant cds databases should have data in the math assessment results table
    And that table should have 68 total rows
    And the dog tenant cds databases should have data in the ELA RIF table
    And that table should have 119 total rows
    And the dog tenant cds databases should have data in the Math RIF table
    And that table should have 156 total rows

  @RALLY_US32336
  Scenario: CDS migration - parcc level
    Given we have a clean second tenant's data warehouse database
    And we have a clean dog tenant cds database
    And we have a clean cat tenant cds database
    And we have a clean parcc cds database
    # Import data directly for cat (since locally, we can't have 2 different srcs for tenant level cds migration
    When I import tenant level sds data into the cat tenant cds
    Then the cat tenant cds databases should have data in the ela assessment results table
    And that table should have 56 total rows
    And the cat tenant cds databases should have data in the math assessment results table
    And that table should have 68 total rows
    And the cat tenant cds databases should have data in the ELA RIF table
    And that table should have 119 total rows
    And the cat tenant cds databases should have data in the Math RIF table
    And that table should have 151 total rows
    # Migrate data for dog
    When I import sds data into the dog tenant
    Then I migrate the data for dog tenant cds in tenant mode for year 2015
    Then I migrate the data for parcc cds in consortium mode for year 2015
    And the parcc cds databases should have data in the ela assessment results table
    And that table should have 108 total rows
    And the parcc cds databases should have data in the math assessment results table
    And that table should have 133 total rows
    And the parcc cds databases should have data in the ELA RIF table
    And that table should have 238 total rows
    And the parcc cds databases should have data in the Math RIF table
    And that table should have 307 total rows

  @RALLY_US35439
  Scenario: Verify edmigrate uploads a dump when finished successfully
    Given a mock ELA summative staging schema exists for the default tenant
    And a mock math summative staging schema exists for the default tenant
    And a record exists for all the mock schemas in the UDL stats table with load status as udl complete
    When enough time is given for Migrate to process the UDL data than we check data dump for the default tenant
    Then the UDL stats table should have the load status for all the records as dump successful
    # And edmigrate should complete a data dump file for the default tenant
