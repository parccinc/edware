from behave import given, when, then
from sqlalchemy import create_engine, types
from sqlalchemy.schema import MetaData, Table
from sqlalchemy.sql import select, and_
from components_tests.api_helper.api_helper import ApiHelper
from components_tests.frontend.common import go_to_url, \
    close_browser, DOWNLOAD_DIR
from components_tests.frontend.support import wait_for_condition
from components_tests.common import get_formatted_postgres_url
import paramiko
import os
import stat
import subprocess
import shutil
import csv
import re
import hashlib
import gnupg
import tarfile
from time import sleep
import sys

__author__ = 'smuhit'


def delete_remote_directory(sftp, remote_path):
    for f in sftp.listdir_attr(remote_path):
        rpath = os.path.join(remote_path, f.filename)
        if stat.S_ISDIR(f.st_mode):
            delete_remote_directory(sftp, rpath)
        else:
            sftp.remove(rpath)
    sftp.rmdir(remote_path)


def create_md5_checksum_file(file_with_path):
    md5 = hashlib.md5()
    checksum_file = file_with_path + '.done'
    with open(file_with_path, 'rb') as f:
        data = f.read()
        md5.update(data)
    md5_hash = md5.hexdigest()

    with open(checksum_file, 'w') as f:
        f.write('MD5 (' + file_with_path + ') = ' + md5_hash)
    return checksum_file


def remote_exists(sftp, path):
    try:
        sftp.stat(path)
    except FileNotFoundError:
        return False
    else:
        return True

TENANT_MAP = {
    'primary': {
        'name': 'first_tenant_name',
        'dw_master_server': 'master_db_server1',
        'dw_slave_servers': ['slave_db_server1', 'slave_db_server2'],
        'dw_user': 'master_db_user',
        'dw_password': 'master_db_password',
        'dw_database': 'master_db',
        'dw_schema': 'master_db_schema'
    },
    'secondary': {
        'name': 'second_tenant_name',
        'dw_master_server': 'master_db_server2',
        'dw_slave_servers': ['slave_db_server4', 'slave_db_server3'],
        'dw_user': 'master_db_user_second_tenant',
        'dw_password': 'master_db_password_second_tenant',
        'dw_database': 'master_db_second_tenant',
        'dw_schema': 'master_db_schema_second_tenant'
    },
    'third': {
        'name': 'third_tenant_name',
        'dw_master_server': 'master_db_server3',
        'dw_slave_servers': ['slave_db_server5', 'slave_db_server6'],
        'dw_user': 'master_db_user',
        'dw_password': 'master_db_password',
        'dw_database': 'master_db',
        'dw_schema': 'master_db_schema'
    },
    'cds': {
        'name': 'cds_tenant_name',
        'dw_master_server': 'master_db_cds_first_tenant_server',
        'dw_slave_servers': ['slave_db_cds_first_tenant_server1', 'slave_db_cds_first_tenant_server2'],
        'dw_user': 'master_db_user_cds_first_tenant',
        'dw_password': 'master_db_password_cds_first_tenant',
        'dw_database': 'master_db_cds_first_tenant',
        'dw_schema': 'master_db_schema_cds_first_tenant'
    }
}

TABLE_NAME_MAP = {
    'ela assessment results': 'rpt_ela_sum',
    'math assessment results': 'rpt_math_sum',
    'assessment test score': 'rpt_sum_test_score_m',
    'assessment test level': 'rpt_sum_test_level_m',
    'math released item': 'rpt_math_sum_item_score',
    'ela released item': 'rpt_ela_sum_item_score',
    'psychometric item': 'rpt_item_p_data'
}


@given('the data warehouse is loaded with mock {data_type} data for the {tenant_type} tenant')
def step_impl(context, data_type, tenant_type):
    data_type_map = {
        'ELA summative': 'ELA_Summative',
        'ELA Summative Multiple Grade/Course': 'ELA_Summative_Multi_GradeCourse',
        'Math Summative Multiple Grade/Course': 'Math_Summative_Multi_GradeCourse',
        'math Released Item File': 'Math_Released_Item',
        'ELA Released Item File': 'ELA_Released_Item',
        'Psychometric Item File': 'Psychometric',
        'CDS ELA summative': 'CDS_ELA_Summative',
    }
    here = os.path.dirname(__file__)
    directory = os.path.abspath(
        os.path.join(here, '..', 'data', 'fixture', 'Bulk_Extract', data_type_map[data_type])
    )

    context.mock_data_directory = directory
    csv_files = []

    for directory_entry in os.listdir(directory):
        if os.path.isfile(os.path.join(directory, directory_entry)):
            if directory_entry.endswith(".csv"):
                csv_files.append(directory_entry)
    db_user = getattr(context, TENANT_MAP[tenant_type]['dw_user'])
    db_password = getattr(context, TENANT_MAP[tenant_type]['dw_password'])
    db_server = getattr(context, TENANT_MAP[tenant_type]['dw_master_server'])
    database = getattr(context, TENANT_MAP[tenant_type]['dw_database'])
    db_schema = getattr(context, TENANT_MAP[tenant_type]['dw_schema'])

    assert len(csv_files) > 0, 'No csv files found in directory'

    edware_master_url = get_formatted_postgres_url(db_user, db_password, db_server, 5432, database)
    engine = create_engine(edware_master_url)

    metadata = MetaData(bind=engine, schema=db_schema)
    metadata.reflect(bind=engine, schema=db_schema)

    for csv_file in csv_files:
        name = os.path.splitext(csv_file)[0]
        table_name = name
        csv_file_name = os.path.join(directory, csv_file)

        with open(csv_file_name, 'rU') as data:
            csv_reader = csv.reader(data, delimiter=',')
            header_row = None
            for row in csv_reader:
                if header_row is None:
                    header_row = row
                    continue

                values = dict(zip(header_row, row))

                table = Table(table_name, metadata, autoload=True)
                for c in table.columns:
                    if values[c.name] == '':
                        values[c.name] = None
                    elif isinstance(c.type, types.Integer):
                        values[c.name] = int(values[c.name])
                    elif isinstance(c.type, types.Boolean):
                        values[c.name] = bool(values[c.name])
                    elif isinstance(c.type, types.LargeBinary):
                        values[c.name] = bytes(values[c.name].encode('utf-8'))
                    else:
                        values[c.name] = str(values[c.name])

                table.insert().execute(values)


@then('the response body contains a url in a files list')
def step_impl(context):
    json_response = context.api._response.json()
    assert 'files' in json_response
    assert 'download_url' in json_response['files'][0]
    context.download_url = json_response['files'][0]['download_url']


@then(u'I wait until I see my download file')
def wait_for_file(context):
    def is_file_generating():
        files = os.listdir(DOWNLOAD_DIR)
        is_empty = len(files) == 0
        if is_empty:
            # Trigger reload
            go_to_url(context, context.download_url)
        return is_empty
    wait_for_condition(is_file_generating)
    context.extract_files = os.listdir(DOWNLOAD_DIR)


@when('the bulk extract file is retrieved')
def step_impl(context):
    context.local_extract_file = os.path.join(DOWNLOAD_DIR, context.extract_files[0])


@when('the extract file is encrypted and copied into the UDL landing zone')
def step_impl(context):

    here = os.path.dirname(__file__)

    gpg_home_directory = os.path.abspath(
        os.path.join(here, '..', 'data', 'keys', context.gpg_key_location)
    )

    encrypted_extract_file = context.local_extract_file + '.gpg'

    gpg = gnupg.GPG(gnupghome=gpg_home_directory)
    with open(context.local_extract_file, 'rb') as f:
        gpg.encrypt_file(file=f, recipients=[context.gpg_recipient], sign=context.gpg_key, passphrase=context.gpg_passphrase, output=encrypted_extract_file)

    os.remove(context.local_extract_file)

    if context.environment == 'local':
        tenant_dir = '/opt/edware/zones/landing/arrivals/cat/ca_user/filedrop/'
        if not os.path.exists(tenant_dir):
            os.makedirs(tenant_dir)
        shutil.copy2(encrypted_extract_file, tenant_dir)
        checksum_file = create_md5_checksum_file(encrypted_extract_file)
        shutil.copy2(checksum_file, tenant_dir)
        driver_path = os.path.abspath(
            os.path.join(here, "..", "..", "..", "edudl2", "scripts", "driver.py")
        )
        command = "python {driver_path} --loop-once".format(driver_path=driver_path)
        subprocess.call(command, shell=True)
    elif context.environment == 'AWS':
        key_file = os.path.join(os.path.expanduser('~'), '.ssh', context.udl_lz_key)
        username = context.udl_lz_user

        host = context.udl_lz_server
        port = 22

        private_key = paramiko.RSAKey.from_private_key_file(key_file)

        transport = paramiko.Transport((host, port))
        transport.connect(username=username, pkey=private_key)

        sftp = paramiko.SFTPClient.from_transport(transport)

        remote_path = '/file_drop/' + os.path.basename(encrypted_extract_file)
        sftp.put(encrypted_extract_file, remote_path)

        sftp.close()
        transport.close()
    else:
        raise AssertionError('Invalid environment')

    os.remove(encrypted_extract_file)
    context.num_files = 1


@then('the extract file is not empty')
def step_impl(context):
    with tarfile.open(context.local_extract_file) as tar:
        assert len(tar.getmembers()) > 0, 'No files found in the extract'
        for member in tar.getmembers():
            assert member.size > 0, 'Empty files found in the extract'
    os.remove(context.local_extract_file)


@then('the {table} table data in the {tenant_type} tenant is the same as what was loaded')
def step_impl(context, table, tenant_type):
    db_user = getattr(context, TENANT_MAP[tenant_type]['dw_user'])
    db_password = getattr(context, TENANT_MAP[tenant_type]['dw_password'])
    db_server = getattr(context, TENANT_MAP[tenant_type]['dw_master_server'])
    database = getattr(context, TENANT_MAP[tenant_type]['dw_database'])
    db_schema = getattr(context, TENANT_MAP[tenant_type]['dw_schema'])

    table_name = TABLE_NAME_MAP[table]

    edware_master_url = get_formatted_postgres_url(db_user, db_password, db_server, 5432, database)
    engine = create_engine(edware_master_url)

    metadata = MetaData(bind=engine, schema=db_schema)
    metadata.reflect(bind=engine, schema=db_schema)

    with engine.connect() as conn:
        table = Table(table_name, metadata)

        if table_name == 'rpt_ela_sum':
            for row in mock_data_content(context, table_name):
                query = select([table.c.resp_school_name, table.c.sum_scale_score, table.c.student_parcc_id, table.c.resp_dist_id]).where(table.c.eoy_test_uuid == row['eoy_test_uuid'])
                results = conn.execute(query).fetchall()
                assert results[0]['resp_school_name'] == row['resp_school_name'], 'Unexpected school name value.\nExpected: {0}\nActual: {1}'.format(row['resp_school_name'], results[0]['resp_school_name'])
                assert results[0]['sum_scale_score'] == int(row['sum_scale_score']), 'Unexpected assessment scaled score value.\nExpected: {0}\nActual: {1}'.format(row['sum_scale_score'], results[0]['sum_scale_score'])
                assert results[0]['student_parcc_id'] == row['student_parcc_id'], 'Unexpected assessment format value.\nExpected: {0}\nActual: {1}'.format(row['student_parcc_id'], results[0]['student_parcc_id'])
                assert results[0]['resp_dist_id'] == row['resp_dist_id'], 'Unexpected assessment total items in unit 1 value.\nExpected: {0}\nActual: {1}'.format(row['resp_dist_id'], results[0]['resp_dist_id'])
        elif table_name == 'rpt_sum_test_score_m':
            for row in mock_data_content(context, table_name):
                query = select([table.c.sum_period]).where(table.c.test_code == row['test_code'])
                results = conn.execute(query).fetchall()
                assert results[0]['sum_period'] == row['sum_period'], 'Unexpected summative period value.\nExpected: {0}\nActual: {1}'.format(row['sum_period'], results[0]['sum_period'])
        elif table_name == 'rpt_sum_test_level_m':
            for row in mock_data_content(context, table_name):
                query = select([table.c.test_code, table.c.cutpoint_label, table.c.cutpoint_low, table.c.cutpoint_upper]).where(table.c.perf_lvl_id == row['perf_lvl_id'])
                results = conn.execute(query).fetchall()
                assert results[0]['test_code'] == row['test_code'], 'Unexpected test code value.\nExpected: {0}\nActual: {1}'.format(row['test_code'], results[0]['test_code'])
                assert results[0]['cutpoint_label'] == row['cutpoint_label'], 'Unexpected cutpoint label value.\nExpected: {0}\nActual: {1}'.format(row['cutpoint_label'], results[0]['cutpoint_label'])
                assert str(results[0]['cutpoint_low']) == row['cutpoint_low'], 'Unexpected cutpoint low value.\nExpected: {0}\nActual: {1}'.format(row['cutpoint_low'], results[0]['cutpoint_low'])
                assert str(results[0]['cutpoint_upper']) == row['cutpoint_upper'], 'Unexpected cutpoint upper value.\nExpected: {0}\nActual: {1}'.format(row['cutpoint_upper'], results[0]['cutpoint_upper'])
        elif table_name == 'rpt_student_item_score':
            for row in mock_data_content(context, table_name):
                query = select([table.c.p_value, table.c.student_guid]).where(table.c.student_item_score == row['student_item_score'])
                results = conn.execute(query).fetchall()
                assert results[0]['asmt_attempt_guid'] == row['asmt_attempt_guid'], 'Unexpected assessment attempt guid value.\nExpected: {0}\nActual: {1}'.format(row['asmt_attempt_guid'], results[0]['asmt_attempt_guid'])
                assert results[0]['student_guid'] == row['student_guid'], 'Unexpected student guid value.\nExpected: {0}\nActual: {1}'.format(row['student_guid'], results[0]['student_guid'])
        elif table_name == 'rpt_item_p_data':
            for row in mock_data_content(context, table_name):
                query = select([table.c.p_value, table.c.item_delivery_mode, table.c.test_item_type]).where(table.c.item_guid == row['item_guid'])
                results = conn.execute(query).fetchall()
                assert results[0]['p_value'] == float(row['p_value']), 'Unexpected p value.\nExpected: {0}\nActual: {1}'.format(row['p_value'], results[0]['p_value'])
                assert results[0]['item_delivery_mode'] == row['item_delivery_mode'], 'Unexpected item delivery mode value.\nExpected: {0}\nActual: {1}'.format(row['item_delivery_mode'], results[0]['item_delivery_mode'])
                assert results[0]['test_item_type'] == row['test_item_type'], 'Unexpected test item type value.\nExpected: {0}\nActual: {1}'.format(row['test_item_type'], results[0]['test_item_type'])
        elif table_name in ['rpt_math_sum_item_score', 'rpt_ela_sum_item_score']:
            for row in mock_data_content(context, table_name):
                query = select([table.c.form_id, table.c.form_format, table.c.student_grade]).where(and_(table.c.student_parcc_id == row['student_parcc_id'],
                                                                                                         table.c.item_uin == row['item_uin']))
                results = conn.execute(query).fetchall()
                assert results[0]['form_id'] == row['form_id'], 'Unexpected form id value.\nExpected: {0}\nActual: {1}'.format(row['form_id'], results[0]['form_id'])
                assert results[0]['form_format'] == row['form_format'], 'Unexpected form format value.\nExpected: {0}\nActual: {1}'.format(row['form_format'], results[0]['form_format'])
                assert results[0]['student_grade'] == row['student_grade'], 'Unexpected student grade value.\nExpected: {0}\nActual: {1}'.format(row['student_grade'], results[0]['student_grade'])


@then('I expect "{num}" entities in the untarred bulk extract csv file starting with "{prefix}"')
def step_impl(context, num, prefix):
    verify_entity_count(context.untar_directory, num, prefix)


@then('I expect "{num}" entities in the untarred bulk extract csv file')
def step_impl(context, num):
    verify_entity_count(context.untar_directory, num)


def verify_entity_count(untar_directory, count, prefix=None):
    file_list = os.listdir(untar_directory)
    if prefix:
        csv_file = [file_name for file_name in file_list if file_name.startswith(prefix)][0]
    else:
        csv_file = [file_name for file_name in file_list if file_name.endswith('.csv')][0]
    contents = load_csv_data(os.path.join(untar_directory, csv_file))
    assert len(contents) == int(count), "Expected: {0}\nActual: {1}".format(count, len(contents))


def untar_file(file):
    with tarfile.open(file) as tar:
        untar_directory = os.path.join(os.path.dirname(file), 'untar_directory')
        if os.path.exists(untar_directory):
            shutil.rmtree(untar_directory)
        os.makedirs(untar_directory)
        tar.extractall(untar_directory)
    return untar_directory


def mock_data_content(context, table_name):
    directory = context.mock_data_directory
    table_name_file_mapping = {'rpt_ela_sum': 'rpt_ela_sum.csv',
                               'rpt_sum_test_score_m': 'rpt_sum_test_score_m.csv',
                               'rpt_sum_test_level_m': 'rpt_sum_test_level_m.csv',
                               'rpt_math_sum_item_score': 'rpt_math_sum_item_score.csv',
                               'rpt_ela_sum_item_score': 'rpt_ela_sum_item_score.csv',
                               'rpt_student_item_score': 'rpt_student_item_score.csv',
                               'rpt_item_p_data': 'rpt_item_p_data.csv'
                               }

    contents = []
    file_to_check = table_name_file_mapping.get(table_name)
    if os.path.exists(os.path.join(directory, file_to_check)):
        contents = load_csv_data(os.path.join(directory, file_to_check))
    return contents


def load_csv_data(file_path_and_name):
    contents = []
    with open(file_path_and_name, 'r', encoding='utf-8') as csv_file:
        csv_file.seek(0)
        dict_reader = csv.DictReader(csv_file, delimiter=',')
        for row in dict_reader:
            contents.append(row)
    return contents
