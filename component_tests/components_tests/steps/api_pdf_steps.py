from components_tests.api_helper.api_helper import ApiHelper
from behave import given, when, then
import urllib.parse


@when(u'the individual student report PDF API is accessed with the following parameters')
def step_impl(context):
    context.api.set_request_header("content-type", "application/pdf")
    payload = {r['param']: r['value'] for r in context.table}
    params = urllib.parse.urlencode(payload)
    context.api.send_request(
        "GET", "/services/pdf/studentReport.html?%s" % params)


@then('the response content length should be larger than "{size}"')
def step_impl(context, size=30000):
    assert int(context.api._response.headers['content-length']) > int(
        size), "Expected Content-Length > {0}, got {1}".format(size, context.api._response.headers['content-length'])


@then(u'the response should have a content type of "{content_type}"')
def step_impl(context, content_type):
    assert content_type in context.api._response.headers['content-type']
