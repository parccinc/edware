from components_tests.common import get_edware_stats_url, get_formatted_postgres_url
from behave import given, when, then
from time import sleep
from sqlalchemy.sql import select, func
from sqlalchemy import create_engine, types
from sqlalchemy.schema import CreateSchema, MetaData, Table
import csv
import datetime
import uuid
import os
import subprocess
import boto
import boto.s3
from boto.s3.key import Key
import re

DW_TABLES = [
    'rpt_ela_snl',
    'rpt_snl_asmt_m',
    'rpt_snl_asmt_mode_m',
    'rpt_snl_task_m',
    'rpt_student_task_snl',
    'rpt_math_mya',
    'rpt_ela_mya',
    'rpt_asmt',
    'rpt_form',
    'rpt_asmt_lvl',
    'rpt_admin',
    'rpt_form_group',
    'rpt_form_group_item',
    'rpt_item',
    'rpt_item_p_data',
    'rpt_student_item_score',
    'rpt_item_mya_meta',
    'rpt_file_store',
    'rpt_ela_sum',
    'rpt_math_sum',
    'rpt_item_ela_mya',
    'rpt_item_math_mya',
    'rpt_item_ela_decod',
    'rpt_item_ela_vocab',
    'rpt_item_ela_read_comp',
    'rpt_dgnst_asmt_m',
    'rpt_dgnst_ela_item_m',
    'rpt_dgnst_asmt_lvl_m',
    'rpt_dgnst_math_item_m',
    'rpt_asmt_skill_m',
    'rpt_ela_vocab',
    'rpt_ela_read_flu',
    'rpt_ela_comp',
    'rpt_ela_decod',
    'rpt_ela_writing',
    'rpt_item_math_comp',
    'rpt_item_math_flu',
    'rpt_math_progress',
    'rpt_math_locator',
    'rpt_math_flu',
    'rpt_math_grade_level',
    'rpt_math_cluster',
    'rpt_item_math_progress',
    'rpt_reader_motiv',
    'rpt_ela_sum_item_score',
    'rpt_math_sum_item_score'
]

TABLE_NAME_MAP = {
    'ela assessment results': 'ela_sum',
    'math assessment results': 'math_sum',
    'assessment test score': 'sum_test_score_m',
    'assessment test level': 'sum_test_level_m',
    'file storage': 'file_store',
    'assessment admin': 'admin',
    'assessment metadata': 'asmt',
    'assessment levels': 'asmt_lvl',
    'assessment forms': 'form',
    'form groups': 'form_group',
    'form items': 'item',
    'psychometric data': 'item_p_data',
    'MYA math': 'math_mya',
    'MYA ELA': 'ela_mya',
    'MYA math item': 'item_math_mya',
    'MYA ELA item': 'item_ela_mya',
    'MYA items': 'item_mya_meta',
    'S&L assessment results': 'ela_snl',
    'S&L assessment metadata': 'snl_asmt_m',
    'S&L assessment mode': 'snl_asmt_mode_m',
    'S&L task metadata': 'snl_task_m',
    'S&L student task results': 'student_task_snl',
    'Item ELA Decoding': 'item_ela_decod',
    'Item ELA Vocabulary': 'item_ela_vocab',
    'Item ELA Reading Comprehension': 'item_ela_read_comp',
    'diagnostic assessment metadata': 'dgnst_asmt_m',
    'diagnostic assessment levels': 'dgnst_asmt_lvl_m',
    'diagnostic ela item metadata': 'dgnst_ela_item_m',
    'diagnostic math item metadata': 'dgnst_math_item_m',
    'diagnostic math fluency skills metadata': 'asmt_skill_m',
    'ELA vocabulary results': 'ela_vocab',
    'ELA reading fluency results': 'ela_read_flu',
    'ELA comprehension results': 'ela_comp',
    'ELA decoding results': 'ela_decod',
    'ELA writing results': 'ela_writing',
    'Math fluency results': 'math_flu',
    'Math locator results': 'math_locator',
    'Math progression results': 'math_progress',
    'Math grade level results': 'math_grade_level',
    'Math cluster results': 'math_cluster',
    'Item Math Comprehension': 'item_math_comp',
    'Item Math Fluency': 'item_math_flu',
    'Item Math Progression': 'item_math_progress',
    'Reader Motivation Survey results': 'reader_motiv',
    'ELA RIF': 'ela_sum_item_score',
    'Math RIF': 'math_sum_item_score',
    'Test Level': 'sum_test_level_m',
    'Test Score': 'sum_test_score_m',
    'Psychometric item data': 'item_p_data'
}


@then('the {database_name} databases should have data in the {table_name} table')
def step_impl(context, database_name, table_name):
    table = TABLE_NAME_MAP[table_name]

    if database_name == 'data warehouse' or database_name == "first tenant's data warehouse":
        db_servers = [context.slave_db_server1,
                      context.slave_db_server2, context.master_db_server1]
        table = 'rpt_' + table
        user = context.master_db_user
        password = context.master_db_password
        database = context.master_db
        schema = context.master_db_schema
    elif database_name == "second tenant's data warehouse":
        table = 'rpt_' + table
        db_servers = [context.slave_db_server4,
                      context.slave_db_server3, context.master_db_server2]
        user = context.master_db_user_second_tenant
        password = context.master_db_password_second_tenant
        database = context.master_db_second_tenant
        schema = context.master_db_schema_second_tenant
    elif database_name in ["dog tenant cds", "second tenant's cds"]:
        table = 'rpt_' + table
        db_servers = [context.slave_db_cds_second_tenant_server2,
                      context.slave_db_cds_second_tenant_server1,
                      context.master_db_cds_second_tenant_server]
        user = context.master_db_user_cds_second_tenant
        password = context.master_db_password_cds_second_tenant
        database = context.master_db
        schema = context.master_db_schema_cds_second_tenant
    elif database_name in ["cat tenant cds", "first tenant's cds"]:
        table = 'rpt_' + table
        db_servers = [context.slave_db_cds_first_tenant_server2,
                      context.slave_db_cds_first_tenant_server1,
                      context.master_db_cds_first_tenant_server]
        user = context.master_db_user_cds_first_tenant
        password = context.master_db_password_cds_first_tenant
        database = context.master_db
        schema = context.master_db_schema_cds_first_tenant
    elif database_name == "parcc cds":
        table = 'rpt_' + table
        db_servers = [context.slave_db_cds_parcc_server2,
                      context.slave_db_cds_parcc_server1, context.master_db_cds_parcc_server]
        user = context.master_db_user_second_tenant
        password = context.master_db_password_second_tenant
        database = context.master_db
        schema = context.master_db_schema_cds_parcc

    else:
        raise AssertionError('Database unknown')

    context.table_to_check = table
    context.db_servers = db_servers
    context.db_user = user
    context.db_password = password
    context.database = database
    context.db_schema = schema

    for server in db_servers:
        edware_master_schema = schema
        edware_master_url = get_formatted_postgres_url(user, password, server, 5432, database)
        engine = create_engine(edware_master_url)

        metadata = MetaData(bind=engine, schema=edware_master_schema)
        metadata.reflect(bind=engine, schema=edware_master_schema)
        with engine.connect() as conn:
            table_to_check = Table(table, metadata)
            query = select([func.count()]).select_from(table_to_check)
            record_count = conn.execute(query).fetchall()[0][0]
            context.tc.assertGreater(record_count, 0,
                                     msg='No data found in {db}/{schema}.{table}'.format(
                                         db=edware_master_url,
                                         schema=edware_master_schema,
                                         table=table))


@given('no mock staging schema exists')
def step_impl(context):
    context.staging_db_schemas = [(str(uuid.uuid4()), 'asmt_data.math', context.first_tenant_name)]


@given('a mock {type} staging schema exists for {tenant_name} tenant')
def step_impl(context, type, tenant_name):

    here = os.path.dirname(__file__)

    if type == 'ELA summative':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'ELA_Summative')
        )
        asmt_type = 'asmt_data.ela'
    elif type == 'math summative':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Math_Summative')
        )
        asmt_type = 'asmt_data.math'
    elif type == 'unsupported':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Unsupported')
        )
        asmt_type = 'asmt_data.ela'
    elif type == 'second ELA summative':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Second_ELA_Summative')
        )
        asmt_type = 'asmt_data.ela'
    elif type == 'psychometric':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Psychometric')
        )
        asmt_type = 'p_data'
    elif type == 'second psychometric':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Second_Psychometric')
        )
        asmt_type = 'p_data'
    elif type == 'MYA Math':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'MYA_Math_Assessment')
        )
        asmt_type = 'mya_math_asmt_data'
    elif type == "MYA ELA":
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'MYA_ELA_Assessment')
        )
        asmt_type = 'mya_ela_asmt_data'
    elif type == "MYA Math item":
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'MYA_Math_Item')
        )
        asmt_type = 'mya_math_item_data'
    elif type == "MYA ELA item":
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'MYA_ELA_Item')
        )
        asmt_type = 'mya_ela_item_data'
    elif type == 'second MYA Math':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Second_MYA_Math_Assessment')
        )
        asmt_type = 'mya_math_asmt_data'
    elif type == "second MYA ELA item":
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Second_MYA_ELA_Item')
        )
        asmt_type = 'mya_ela_item_data'
    elif type == "S&L assessment":
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'S&L_Assessment')
        )
        asmt_type = 's&l_total_asmt_data'
    elif type == "S&L student task":
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'S&L_Student_Task')
        )
        asmt_type = 'snl_task_data'
    elif type == "second S&L assessment":
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Second_S&L_Assessment')
        )
        asmt_type = 's&l_total_asmt_data'
    elif type == "second S&L student task":
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Second_S&L_Student_Task')
        )
        asmt_type = 'snl_task_data'
    elif type == "Item ELA Reading Comprehension":
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'ELA_Read_Comp_Item')
        )
        asmt_type = 'diagnostic_ela_item_reading_comp'
    elif type == "Item ELA Decoding":
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'ELA_Decoding_Item')
        )
        asmt_type = 'diagnostic_ela_item_decoding'
    elif type == "Item ELA Vocabulary":
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'ELA_Vocab_Item')
        )
        asmt_type = 'diagnostic_ela_item_vocab'
    elif type == 'second Item ELA Decoding':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Second_ELA_Decoding_Item')
        )
        asmt_type = 'diagnostic_ela_item_decoding'
    elif type == 'ELA Vocabulary':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'ELA_Vocabulary')
        )
        asmt_type = 'diagnostic_ela_vocab'
    elif type == 'second ELA Vocabulary':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Second_ELA_Vocabulary')
        )
        asmt_type = 'diagnostic_ela_vocab'
    elif type == 'reader motivation survey':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Reader_Motivation')
        )
        asmt_type = 'diagnostic_reader_motivation'
    elif type == 'second reader motivation survey':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Second_Reader_Motivation')
        )
        asmt_type = 'diagnostic_reader_motivation'
    elif type == 'ELA Reading Fluency':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'ELA_Reading_Fluency')
        )
        asmt_type = 'diagnostic_ela_reading_fluency'
    elif type == 'ELA Comprehension':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'ELA_Comp')
        )
        asmt_type = 'diagnostic_ela_comp'
    elif type == 'ELA Decoding':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'ELA_Decoding')
        )
        asmt_type = 'diagnostic_ela_decoding'
    elif type == 'ELA Writing':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'ELA_Writing')
        )
        asmt_type = 'diagnostic_ela_writing'
    elif type == "Item Math Comprehension":
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Math_Comp_Item')
        )
        asmt_type = 'diagnostic_math_item_comp'
    elif type == "Item Math Fluency":
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Math_Fluency_Item')
        )
        asmt_type = 'diagnostic_math_item_fluency'
    elif type == "Item Math Progression":
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Math_Progression_Item')
        )
        asmt_type = 'diagnostic_math_item_progression'
    elif type == "second Item Math Comprehension":
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Second_Math_Comp_Item')
        )
        asmt_type = 'diagnostic_math_item_comp'
    elif type == 'Math Fluency':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Math_Fluency')
        )
        asmt_type = 'diagnostic_math_fluency'
    elif type == 'Math Locator':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Math_Locator')
        )
        asmt_type = 'diagnostic_math_locator'
    elif type == 'Math Progression':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Math_Progression')
        )
        asmt_type = 'diagnostic_math_progression'
    elif type == 'Math Grade Level':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Math_Grade_Level')
        )
        asmt_type = 'diagnostic_math_gradelevel'
    elif type == 'Math Cluster':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Math_Cluster')
        )
        asmt_type = 'diagnostic_math_cluster'
    elif type == 'second Math Fluency':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Second_Math_Fluency')
        )
        asmt_type = 'diagnostic_math_fluency'
    elif type == 'ELA RIF':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'ELA_RIF')
        )
        asmt_type = 'ela_item_student_score'
    elif type == 'second ELA RIF':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Second_ELA_RIF')
        )
        asmt_type = 'ela_item_student_score'
    elif type == 'Math RIF':
        directory = os.path.abspath(
            os.path.join(
                here, '..', 'data', 'fixture', 'Migrate', 'Math_RIF')
        )
        asmt_type = 'math_item_student_score'
    else:
        raise AssertionError('Invalid staging schema to load')

    if tenant_name == 'the default' or tenant_name == 'the first':
        tenant = context.first_tenant_name
        staging_db_user = context.staging_db_user
        staging_db_password = context.staging_db_password
        staging_db = context.staging_db
    elif tenant_name == 'the second':
        tenant = context.second_tenant_name
        staging_db_user = context.staging_db_user_second_tenant
        staging_db_password = context.staging_db_password_second_tenant
        staging_db = context.staging_db_second_tenant
    else:
        raise AssertionError('Unsupported tenant name')

    staging_db_server = context.staging_rds_server
    staging_port = context.staging_rds_port

    csv_files = []

    for directory_entry in os.listdir(directory):
        if os.path.isfile(os.path.join(directory, directory_entry)):
            if directory_entry.endswith(".csv"):
                csv_files.append(directory_entry)

    assert len(csv_files) > 0, 'No csv files found in directory'

    staging_db_schema = str(uuid.uuid4())
    schema_entry = (staging_db_schema, asmt_type, tenant)
    if hasattr(context, 'staging_db_schemas'):
        context.staging_db_schemas.append(schema_entry)
    else:
        context.staging_db_schemas = [schema_entry]
    edware_master_url = get_formatted_postgres_url(staging_db_user, staging_db_password,
                                                   staging_db_server, staging_port, staging_db)
    engine = create_engine(edware_master_url)
    with engine.connect() as conn:
        conn.execute(CreateSchema(staging_db_schema))

    metadata = MetaData(bind=engine, schema=staging_db_schema)
    metadata.reflect(bind=engine, schema=staging_db_schema)

    for csv_file in csv_files:
        name = os.path.splitext(csv_file)[0]
        table_name = name
        sql_file_name = os.path.join(
            here, '..', 'data', 'fixture', 'Migrate', 'sql', name + '.sql')
        csv_file_name = os.path.join(directory, csv_file)

        with open(sql_file_name, 'rU') as file_name:
            create_table_query = str(file_name.read())
        create_table_query = create_table_query.replace('\n', ' ')
        create_table_query = create_table_query.replace(
            '***schema_name***', '"' + staging_db_schema + '"')
        with engine.connect() as conn:
            conn.execute(create_table_query)

        with open(csv_file_name, 'rU') as data:
            csv_reader = csv.reader(data, delimiter=',')
            header_row = None
            for row in csv_reader:
                if header_row is None:
                    header_row = row
                    continue

                values = dict(zip(header_row, row))
                if table_name != 'custom_metadata':
                    values.update({'batch_guid': staging_db_schema})

                table = Table(table_name, metadata, autoload=True)
                for c in table.columns:
                    if values[c.name] == '':
                        values[c.name] = None
                    elif isinstance(c.type, types.Integer):
                        values[c.name] = int(values[c.name])
                    elif isinstance(c.type, types.Boolean):
                        values[c.name] = bool(values[c.name])
                    elif isinstance(c.type, types.LargeBinary):
                        values[c.name] = bytes(values[c.name].encode('utf-8'))
                    else:
                        values[c.name] = str(values[c.name])

                table.insert().execute(values)


@given('we have a clean {tenant} database')
def step_impl(context, tenant):
    if tenant == "second tenant's data warehouse":
        schemas_to_clean = [context.master_db_schema_second_tenant]
        dw_url = get_formatted_postgres_url(context.master_db_user_second_tenant, context.master_db_password_second_tenant,
                                            context.master_db_server2, 5432, context.master_db_second_tenant)
    elif tenant == "third tenant's data warehouse":
        schemas_to_clean = [context.master_db_schema_third_tenant]
        dw_url = get_formatted_postgres_url(context.master_db_user_third_tenant, context.master_db_password_third_tenant,
                                            context.master_db_server3, 5432, context.master_db_third_tenant)
    elif tenant == "data warehouse" or tenant == "first tenant's data warehouse":
        schemas_to_clean = [context.master_db_schema]
        dw_url = get_formatted_postgres_url(context.master_db_user, context.master_db_password,
                                            context.master_db_server1, 5432, context.master_db)
    elif tenant in ["dog tenant cds", "second tenant's cds"]:
        schemas_to_clean = [context.master_db_schema_cds_second_tenant]
        dw_url = get_formatted_postgres_url(context.master_db_user_cds_second_tenant, context.master_db_password_cds_second_tenant,
                                            context.master_db_cds_second_tenant_server, 5432, context.master_db)
    elif tenant in ["cat tenant cds", "first tenant's cds"]:
        schemas_to_clean = [context.master_db_schema_cds_first_tenant]
        dw_url = get_formatted_postgres_url(context.master_db_user_cds_first_tenant, context.master_db_password_cds_first_tenant,
                                            context.master_db_cds_first_tenant_server, 5432, context.master_db)
    elif tenant == "parcc cds":
        schemas_to_clean = [context.master_db_schema_cds_parcc]
        dw_url = get_formatted_postgres_url(context.master_db_user_cds_parcc, context.master_db_password_cds_parcc,
                                            context.master_db_cds_parcc_server, 5432, context.master_db)
    else:
        raise AssertionError('Invalid or unsupported tenant')
    engine = create_engine(dw_url)

    for dw_schema in schemas_to_clean:
        metadata = MetaData(bind=engine, schema=dw_schema)
        metadata.reflect(bind=engine, schema=dw_schema)
        with engine.connect() as conn:
            for table in reversed(metadata.sorted_tables):
                conn.execute(table.delete())
                query = select([func.count()]).select_from(table)
                record_count = conn.execute(query).fetchall()[0][0]
                assert record_count == 0


@given('a record exists for all the mock schemas in the UDL stats table with load status as {status}')
def step_impl(context, status):

    if status == 'udl complete':
        load_status = 'udl.ingested'
    else:
        raise AssertionError('Unsupported or invalid load status')

    edware_stats_schema = context.edware_stats_schema
    edware_stats_url = get_edware_stats_url(context)
    engine = create_engine(edware_stats_url)

    today = datetime.date.today()

    metadata = MetaData(bind=engine, schema=edware_stats_schema)
    metadata.reflect(bind=engine, schema=edware_stats_schema)
    with engine.connect() as conn:
        udl_stats_table = Table('udl_stats', metadata)
        conn.execute(udl_stats_table.delete())
        i = udl_stats_table.insert()
        for staging_db_entry in context.staging_db_schemas:
            i.execute({
                'tenant': staging_db_entry[2],
                'dest_tenant': staging_db_entry[2],
                'load_type': staging_db_entry[1],
                'file_arrived': today,
                'load_end': today,
                'load_status': load_status,
                'batch_guid': staging_db_entry[0],
                'schema_name': staging_db_entry[0],
                'record_loaded_count': 10
            })
        count = select([func.count()]).select_from(udl_stats_table)
        record_count = conn.execute(count).fetchall()[0][0]
        assert record_count == len(context.staging_db_schemas)


@when('enough time is given for Migrate to process the UDL data')
def step_impl(context):
    context.call_start_time = datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S.%f")
    if context.environment == 'local':
        command = "python -m edmigrate.main --migrateOnly -t tenant"
        subprocess.call(command, shell=True)
    elif context.environment == 'AWS':
        if context.migrate_frequency == 'minutely':
            wait_period = 360
            frequency = 2
        elif context.migrate_frequency == 'daily':
            wait_period = 86400
            frequency = 1800
        else:
            raise AssertionError('Unsupported migration frequency')

        edware_stats_schema = context.edware_stats_schema
        edware_stats_url = get_edware_stats_url(context)
        engine = create_engine(edware_stats_url)

        metadata = MetaData(bind=engine, schema=edware_stats_schema)
        metadata.reflect(bind=engine, schema=edware_stats_schema)
        migration_messages = ['dumpdb.created', 'dumpdb.failed', 'migrate.failed']
        with engine.connect() as conn:
            udl_stats_table = Table('udl_stats', metadata)
            query = select([udl_stats_table.c.load_status])
            timer = 0
            result = conn.execute(query).fetchall()
            resultant = list(x[0] for x in result)
            while timer < wait_period and not set(resultant).issubset(migration_messages):
                sleep(frequency)
                timer += frequency
                result = conn.execute(query).fetchall()
                resultant = list(x[0] for x in result)

        # Enough time for the slaves to accept connections again after
        # migration
        sleep(15)
    else:
        raise AssertionError('Invalid environment')


@when('enough time is given for the slave database servers to replicate from master')
def step_impl(context):
    if context.environment == 'AWS':
        sleep(45)


@when('enough time is given for Migrate to process the UDL data than we check data dump for the {tenant_name} tenant')
def step_impl(context, tenant_name):
    context.call_start_time = datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S.%f")
    if context.environment == 'local':
        command = "python -m edmigrate.main --migrateOnly -t tenant"
        subprocess.call(command, shell=True)
    elif context.environment == 'AWS':
        if context.migrate_frequency == 'minutely':
            wait_period = 360
            frequency = 2
        elif context.migrate_frequency == 'daily':
            wait_period = 86400
            frequency = 1800
        else:
            raise AssertionError('Unsupported migration frequency')

        edware_stats_schema = context.edware_stats_schema
        edware_stats_url = get_edware_stats_url(context)
        engine = create_engine(edware_stats_url)

        metadata = MetaData(bind=engine, schema=edware_stats_schema)
        metadata.reflect(bind=engine, schema=edware_stats_schema)
        migration_messages = ['dumpdb.created', 'dumpdb.failed', 'migrate.failed']
        with engine.connect() as conn:
            udl_stats_table = Table('udl_stats', metadata)
            query = select([udl_stats_table.c.load_status])
            timer = 0
            result = conn.execute(query).fetchall()
            resultant = list(x[0] for x in result)
            while timer < wait_period and not set(resultant).issubset(migration_messages):
                sleep(frequency)
                timer += frequency
                result = conn.execute(query).fetchall()
                resultant = list(x[0] for x in result)
        check_dump(context, tenant_name)
        # Enough time for the slaves to accept connections again after
        # migration
        sleep(15)
    else:
        raise AssertionError('Invalid environment')


def check_dump(context, tenant_name):
    """
     this is a pretty weak test, but it's non-trivial to calculate the filename generated if we can't parse the ini
     with the edmigrate utils.
    """
    s3_bucket_name = context.analytics_source_bucket_prefix
    tenant = context.first_tenant_name

    if tenant_name == 'the default' or tenant_name == 'the first':
        tenant = context.first_tenant_name

    elif tenant_name == 'the second':
        tenant = context.second_tenant_name

    if context.environment == 'AWS':
        s3_bucket_name += '-' + tenant

    context.dump_tenant = getattr(context, "dump_tenant", tenant)
    conn = boto.connect_s3()
    bucket = conn.get_bucket(s3_bucket_name)
    timestamp_format = "(?P<date_part>[^_]+)_(?P<hour>[^:]+):(?P<minute>[^:]+):(?P<second>[^.]+).(?P<milli>[^:]+)"
    timestamp_pattern = re.compile(timestamp_format)
    start_time_parts = timestamp_pattern.match(context.call_start_time)

    # this is a pretty weak test, but it's non-trivial to calculate the filename generated if we can't parse the ini
    # with the edmigrate utils.
    expected_filename_pattern = context.realm + "/.*@\^%s@\^(?:complete|processing)@#%s_(?P<hour>\d{2}):(?P<minute>\d{2}):\d{2}.\d{6}-edmigrate-dump.gz$" % (tenant, start_time_parts.group("date_part"))
    p = re.compile(expected_filename_pattern)

    last_trigger_file = None
    items_in_bucket = False
    items_match_pattern = False

    for s3_file in bucket.list():
        items_in_bucket = True

        m = p.match(s3_file.name)
        if m is not None:
            items_match_pattern = True

            #see if it's within an hour
            if int(start_time_parts.group("hour")) - 1 <= int(m.group("hour")) <= int(start_time_parts.group("hour")) + 1:
                last_trigger_file = s3_file.name
    context.tc.assertTrue(items_in_bucket,
                          msg="no dumps found in s3 bucket {name}".format(name=s3_bucket_name))
    context.tc.assertTrue(items_match_pattern,
                          msg="no dump files match pattern {pattern}".format(
                              pattern=expected_filename_pattern))
    context.tc.assertIsNotNone(last_trigger_file,
                               msg="no dump file was created within an hour of {t_range}".format(
                                   t_range=start_time_parts.group("hour")))

    context.edmigrate_has_completed_data_dump = True
    context.dumpfile_location = "s3://{bucket_name}".format(bucket_name=s3_bucket_name)
    context.last_trigger_file = last_trigger_file
    bucket.delete_key(last_trigger_file)


@when('enough time is given for Migrate to process the UDL data for {tenant_name} tenant')
def step_impl(context, tenant_name):
    context.call_start_time = datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S.%f")
    if tenant_name == "the first":
        tenant_name = context.first_tenant_name
    elif tenant_name == "the second":
        tenant_name = context.second_tenant_name

    context.dump_tenant = tenant_name
    if context.environment == 'local':
        command = "python -m edmigrate.main --migrateOnly -t tenant -n " + \
            tenant_name
        rv = subprocess.call(command, shell=True)
        if rv:
            raise Exception("Failed calling %s (rv: %s)" % (command, rv))
    elif context.environment == 'AWS':
        if context.migrate_frequency == 'minutely':
            wait_period = 150
            frequency = 2
        elif context.migrate_frequency == 'daily':
            wait_period = 86400
            frequency = 1800
        else:
            raise AssertionError('Unsupported migration frequency')

        edware_stats_schema = context.edware_stats_schema
        edware_stats_url = get_edware_stats_url(context)
        engine = create_engine(edware_stats_url)

        metadata = MetaData(bind=engine, schema=edware_stats_schema)
        metadata.reflect(bind=engine, schema=edware_stats_schema)
        migration_messages = ['dumpdb.created', 'dumpdb.failed', 'migrate.failed']
        with engine.connect() as conn:
            udl_stats_table = Table('udl_stats', metadata)
            query = select([udl_stats_table.c.load_status])
            timer = 0
            result = conn.execute(query).fetchall()
            resultant = list(x[0] for x in result)
            while timer < wait_period and not set(resultant).issubset(migration_messages):
                sleep(frequency)
                timer += frequency
                result = conn.execute(query).fetchall()
                resultant = list(x[0] for x in result)

        # Enough time for the slaves to accept connections again after
        # migration
        sleep(15)
    else:
        raise AssertionError('Invalid environment')


@then('the UDL stats table should have the load status for all the records as {status}')
def step_impl(context, status):
    if status == 'migrate failed':
        load_status = 'migrate.failed'
    elif status == 'dump successful':
        load_status = 'dumpdb.created'
    elif status == 'dump failed':
        load_status = 'dumpdb.failed'
    else:
        raise AssertionError('Unsupported or invalid load status')

    edware_stats_schema = context.edware_stats_schema
    edware_stats_url = get_edware_stats_url(context)
    engine = create_engine(edware_stats_url)

    metadata = MetaData(bind=engine, schema=edware_stats_schema)
    metadata.reflect(bind=engine, schema=edware_stats_schema)
    with engine.connect() as conn:
        udl_stats_table = Table('udl_stats', metadata)
        query = select([udl_stats_table.c.load_status]).where(
            udl_stats_table.c.dest_tenant.notilike("%crds%"))
        results = conn.execute(query).fetchall()
        for result in results:
            assert load_status == result[0], 'Expected: {load_status}, Actual: {result}'.format(
                load_status=load_status, result=result[0])


@then('that table should have {count} total rows')
def step_impl(context, count):
    table = context.table_to_check
    db_servers = context.db_servers
    user = context.db_user
    password = context.db_password
    database = context.database
    schema = context.db_schema

    for server in db_servers:
        edware_master_schema = schema
        edware_master_url = get_formatted_postgres_url(user, password, server, 5432, database)
        engine = create_engine(edware_master_url)

        metadata = MetaData(bind=engine, schema=edware_master_schema)
        metadata.reflect(bind=engine, schema=edware_master_schema)
        with engine.connect() as conn:
            table_to_check = Table(table, metadata)
            query = select([func.count()]).select_from(table_to_check)
            record_count = conn.execute(query).fetchall()[0][0]

            assert record_count == int(count), 'Expected number of total rows in {table}: {expected}. Actual: {actual}'.format(
                table=table, expected=count, actual=record_count)


@then('that table should have {count} active rows')
def step_impl(context, count):
    table = context.table_to_check
    db_servers = context.db_servers
    user = context.db_user
    password = context.db_password
    database = context.database
    schema = context.db_schema

    for server in db_servers:
        edware_master_schema = schema
        edware_master_url = get_formatted_postgres_url(user, password, server, 5432, database)
        engine = create_engine(edware_master_url)

        metadata = MetaData(bind=engine, schema=edware_master_schema)
        metadata.reflect(bind=engine, schema=edware_master_schema)
        with engine.connect() as conn:
            table_to_check = Table(table, metadata)
            query = select([func.count()]).select_from(
                table_to_check).where(table_to_check.c.rec_status == 'C')
            record_count = conn.execute(query).fetchall()[0][0]
            assert record_count == int(count), 'Expected number of active rows in {table}: {expected}. Actual: {actual}'.format(
                table=table, expected=count, actual=record_count)

# TODO: (optimization) This step was commented as extremely flaky, it used in conjunction with test
# "enough time is given for Migrate to process the UDL data"   (1)
# To check dump creation and avoid flaky behavior this logic moved to helper method "check_dump".
# Conjunction of steps described above replaced by one step that handle logic of those steps:
# "enough time is given for Migrate to process the UDL data than we check data dump for the {tenant_name} tenant"

# @then('edmigrate should complete a data dump file for {tenant_name} tenant')
# def step_impl(context, tenant_name):
#     """
#     this is a pretty weak test, but it's non-trivial to calculate the filename generated if we can't parse the ini
#     with the edmigrate utils.
#
#     :param context:
#     :param tenant_name:
#     :return:
#     """
#     s3_bucket_name = context.analytics_source_bucket_prefix
#     tenant = context.first_tenant_name
#
#     if tenant_name == 'the default' or tenant_name == 'the first':
#         tenant = context.first_tenant_name
#
#     elif tenant_name == 'the second':
#         tenant = context.second_tenant_name
#
#     if context.environment == 'AWS':
#         s3_bucket_name += '-' + tenant
#
#     context.dump_tenant = getattr(context, "dump_tenant", tenant)
#     conn = boto.connect_s3()
#     bucket = conn.get_bucket(s3_bucket_name)
#     timestamp_format = "(?P<date_part>[^_]+)_(?P<hour>[^:]+):(?P<minute>[^:]+):(?P<second>[^.]+).(?P<milli>[^:]+)"
#     timestamp_pattern = re.compile(timestamp_format)
#     start_time_parts = timestamp_pattern.match(context.call_start_time)
#
#     # this is a pretty weak test, but it's non-trivial to calculate the filename generated if we can't parse the ini
#     # with the edmigrate utils.
#     expected_filename_pattern = context.realm + "/.*@\^%s@\^(?:complete|processing)@#%s_(?P<hour>\d{2}):(?P<minute>\d{2}):\d{2}.\d{6}-edmigrate-dump.gz$" % (tenant, start_time_parts.group("date_part"))
#     p = re.compile(expected_filename_pattern)
#
#     last_trigger_file = None
#     items_in_bucket = False
#     items_match_pattern = False
#
#     for s3_file in bucket.list():
#         items_in_bucket = True
#
#         m = p.match(s3_file.name)
#         if m is not None:
#             items_match_pattern = True
#
#             #see if it's within an hour
#             if int(start_time_parts.group("hour")) - 1 <= int(m.group("hour")) <= int(start_time_parts.group("hour")) + 1:
#                 last_trigger_file = s3_file.name
#     context.tc.assertTrue(items_in_bucket,
#                           msg="no dumps found in s3 bucket {name}".format(name=s3_bucket_name))
#     context.tc.assertTrue(items_match_pattern,
#                           msg="no dump files match pattern {pattern}".format(
#                               pattern=expected_filename_pattern))
#     context.tc.assertIsNotNone(last_trigger_file,
#                                msg="no dump file was created within an hour of {t_range}".format(
#                                    t_range=start_time_parts.group("hour")))
#
#     context.edmigrate_has_completed_data_dump = True
#     context.dumpfile_location = "s3://{bucket_name}".format(bucket_name=s3_bucket_name)
#     context.last_trigger_file = last_trigger_file
#     bucket.delete_key(last_trigger_file)


@when('edmigrate has completed a data dump file')
def step_impl(context):
    if context.edmigrate_has_completed_data_dump is True:
        return True
    return False
