import csv
from datetime import datetime
import re
from sqlalchemy import create_engine, types
from sqlalchemy.schema import MetaData, Table, DropSchema
from components_tests.common import get_formatted_postgres_url
from behave import when, then
import os
import shutil
from sqlalchemy.sql.ddl import CreateSchema
from zipfile import ZipFile, ZIP_STORED
import tempfile
import subprocess

__author__ = 'nparoha'


_HERE = os.path.dirname(__file__)

DW_TABLES = [
    'rpt_ela_sum_item_score',
    'rpt_ela_sum',
    'rpt_math_sum_item_score',
    'rpt_math_sum',
    'rpt_sum_test_level_m',
    'rpt_sum_test_score_m',
    'rpt_item_p_data',
    'rpt_ela_vocab',
    'rpt_ela_writing',
    'rpt_ela_read_flu',
    'rpt_ela_comp',
    'rpt_ela_decod',
    'rpt_reader_motiv',
    'rpt_math_cluster',
    'rpt_math_locator',
    'rpt_math_grade_level',
    'rpt_math_flu',
    'rpt_math_progress',
]

CDS_TABLES = [
    'rpt_ela_sum_item_score',
    'rpt_ela_sum',
    'rpt_math_sum_item_score',
    'rpt_math_sum',
    'rpt_sum_test_level_m'
]


@when('I take the data dump from {tenant_name} prod database to CSV files')
def generate_cvs_templates(context, tenant_name):
    if tenant_name == 'cat':
        host_name = context.master_db_server1
        database_name = context.master_db
        username = context.master_db_user
        password = context.master_db_password
        schema_name = context.master_db_schema
        cds_schema_name = context.master_db_schema_cds_first_tenant

    elif tenant_name == 'dog':
        host_name = context.master_db_server2
        database_name = context.master_db_second_tenant
        username = context.master_db_user_second_tenant
        password = context.master_db_password_second_tenant
        schema_name = context.master_db_schema_second_tenant
        cds_schema_name = context.master_db_schema_cds_second_tenant

    elif tenant_name == 'fish':
        host_name = context.master_db_server3
        database_name = context.master_db_third_tenant
        username = context.master_db_user_third_tenant
        password = context.master_db_password_third_tenant
        schema_name = context.master_db_schema_third_tenant
        cds_schema_name = context.master_db_schema_cds_third_tenant

    else:
        raise AssertionError("Unknown tenant")

    dest_path = os.path.abspath(os.path.join(_HERE, '..', 'data', 'fixture', 'SDS_CSV_FILES'))
    cds_path_tenant = os.path.abspath(os.path.join(dest_path, 'cds_tenant', tenant_name))
    cds_path_consortium = os.path.abspath(os.path.join(dest_path, 'cds_consortium'))

    if os.path.isdir(dest_path):
        if tenant_name == 'cat':
            # note: I'm not sure why the logic here is different for cat? (mjacob)

            shutil.rmtree(cds_path_tenant, ignore_errors=True)
            shutil.rmtree(cds_path_consortium, ignore_errors=True)

            for path in (dest_path, cds_path_tenant, cds_path_consortium):
                if not os.path.exists(path):
                    os.makedirs(path)

        else:
            shutil.rmtree(dest_path, ignore_errors=True)
            os.makedirs(dest_path)
            os.makedirs(cds_path_tenant)
            os.makedirs(cds_path_consortium)
    else:
        raise Exception('NO SUCH DIRECTORY, {0}'.format(dest_path))

    if context.environment == 'local':
        edware_master_url = get_formatted_postgres_url(username, password, host_name, 5432, database_name)
        engine = create_engine(edware_master_url)
        connection = engine.connect()

        # Don't rewrite the SDS data for cat tenant, as it contains a filtered version of SDS. Just use one set of SDS.
        # Cat SDS has a few students deleted, and state_code changed to NY

        with tempfile.TemporaryDirectory(dir='/tmp') as temp_path:  # dump to a temp dir for the case that postgres is running as current user
            os.chmod(temp_path, 0o777)  # make writable by postgres

            if tenant_name not in ('cat', 'fish'):
                schema_query = "set schema '{0}'".format(schema_name)
                connection.execute(schema_query)

                # #TODO: Include rpt_file_store in DW_TABLES. Currently it does not export the file_data results in correct format for item level in the CSV files.
                # #This causes errors while importing the data from CSV to database

                for table in DW_TABLES:
                    _dump_to_csv(temp_path, dest_path, connection, table)

            # tenant level CDS dumps
            schema_query = "set schema '{0}'".format(cds_schema_name)
            connection.execute(schema_query)
            for table in CDS_TABLES:
                _dump_to_csv(temp_path, cds_path_tenant, connection, table)

            # consortium level CDS dump
            schema_query = "set schema 'edware_cds'"
            connection.execute(schema_query)
            for table in CDS_TABLES:
                _dump_to_csv(temp_path, cds_path_consortium, connection, table)


@when('I dump the analytics db for "{tenant_name}" to csv.gz file')
def step_impl(context, tenant_name):
    if tenant_name == 'cat':
        db_user = context.master_db_user
        db_password = context.master_db_password
        db_server = context.master_db_server1
        database = context.master_db
        db_schema = context.master_db_schema

    elif tenant_name == 'dog':
        db_user = context.master_db_user_second_tenant
        db_password = context.master_db_password_second_tenant
        db_server = context.master_db_server2
        database = context.master_db_second_tenant
        db_schema = context.master_db_schema_second_tenant
    else:
        raise Exception('Unknown tenant %s' % (tenant_name,))

    gz_dump_path = os.path.abspath(os.path.join(
        _HERE, '..', 'data', 'fixture', 'Analytics_SDS_Data_Files'))

    archive_path = os.path.join(gz_dump_path, '{tenant}.gz'.format(tenant=tenant_name))
    tmp_csv_gz_file_path = os.path.join(gz_dump_path, '{}.csv.gz')

    tables_for_export = ['rpt_ela_sum', 'rpt_math_sum']
    db_conn_string = 'postgres://{user}:{password}@{host}/{db}'.format(user=db_user,
                                                                       password=db_password,
                                                                       host=db_server,
                                                                       db=database)

    commands, file_patches = _build_csv_gz_commands(db_conn_string, db_schema,
                                                    tables_for_export, tmp_csv_gz_file_path)

    with ZipFile(archive_path, mode='w', compression=ZIP_STORED, allowZip64=True) as zf:
        processes = [subprocess.Popen(command, shell=True) for command in commands]
        returncodes = [p.wait() for p in processes]
        for fpath in file_patches:
            zf.write(fpath, arcname=os.path.basename(fpath))

    for fpath in file_patches:
        os.unlink(fpath)


def _build_csv_gz_commands(db_conn_string: str, schema: str, tables: list, tmp_file_path: str)-> list:
    """
    returns commands that create csv.gz files for required tables and patch where to store those files
    """
    file_patch = []
    commands = []

    for table in tables:
        tmp_filename = tmp_file_path.format(table)
        mask = 'psql {db} -c "COPY {schema}.{table} TO STDOUT DELIMITERS \',\' CSV HEADER" | gzip -8 > {f}'
        command = mask.format(db=db_conn_string, schema=schema, table=table, f=tmp_filename)

        commands.append(command)
        file_patch.append(tmp_filename)

    return commands, file_patch


@when('I dump the analytics db for {tenant_name} to SQL')
def dump_analytics(context, tenant_name):
    if tenant_name == 'cat':
        ana_db_server = context.analytics_state_master_db_server1
        ana_db_name = context.analytics_db
        ana_db_user = context.analytics_db_user
        ana_db_password = context.analytics_db_password
        ana_db_dest_schema = context.analytics_db_schema

    elif tenant_name == 'dog':
        ana_db_server = context.analytics_state_master_db_server2
        ana_db_name = context.analytics_db
        ana_db_user = context.analytics_db_user
        ana_db_password = context.analytics_db_password
        ana_db_dest_schema = context.analytics_db_schema

    else:
        raise Exception('Unknown tenant %s' % (tenant_name,))

    analytics_dest_path = os.path.abspath(os.path.join(_HERE,
                                                       '..',
                                                       'data',
                                                       'fixture',
                                                       'Analytics_SDS_Data_Files',
                                                       '{tenant}_{db}.sql'.format(tenant=tenant_name,
                                                                                  db=ana_db_name)))

    with subprocess.Popen(['pg_dump',
                           '--schema=' + ana_db_dest_schema,
                           '--dbname=postgres://{user}:{password}@{host}/{db}'.format(user=ana_db_user,
                                                                                      password=ana_db_password,
                                                                                      host=ana_db_server,
                                                                                      db=ana_db_name),
                           '--file=' + analytics_dest_path,
                           '--format=p']) as pg_dump:
        pg_dump.wait(120)
        rv = pg_dump.returncode
        if rv != 0:
            raise Exception('error dumping analytics DB: return code %s' % (rv,))


def _dump_to_csv(temp_path, real_path, connection, table):
    """
    :param temp_path: needed in the case that postgres isn't running as the current user
    :param real_path:
    :param connection:
    :param table:
    :return:
    """
    temp_csv_path = os.path.join(temp_path, table + '.csv')
    real_csv_path = os.path.join(real_path, table + '.csv')

    copy_query = "copy {table_name} to '{fname}' delimiter ',' csv header".format(table_name=table, fname=temp_csv_path)
    connection.execute(copy_query)
    shutil.move(temp_csv_path, real_csv_path)


@when('I create an empty {sds_schema_name} schema')
def step_impl(context, sds_schema_name):
    host_name = context.master_db_server1
    database_name = context.master_db
    username = context.master_db_user
    password = context.master_db_password
    sds_schema_name = sds_schema_name

    if context.environment == 'local':
        # Drop an existing schema if it exists
        edware_master_url = get_formatted_postgres_url(username, password, host_name, 5432, database_name)
        engine = create_engine(edware_master_url)
        connection = engine.connect()
        query = "select '{schema_name}' from information_schema.schemata where schema_name='{schema_name}'".format(schema_name=sds_schema_name)
        result = connection.execute(query).fetchone()
        if result is not None:
            schema_exists = True
        else:
            schema_exists = False
        if schema_exists:
            connection.execute(DropSchema(sds_schema_name, cascade=True))

        # Create an Empty Schema
        metadata_generator_path = os.path.abspath(
            os.path.join(_HERE, "..", "..", "..", "..", "edschema", "edschema", "metadata_generator.py"))
        command = ("python {metadata_generator_path} -s {sds_schema_name} -d '{database_name}' -m '{metadata_name}' "
                   "--host '{host_name}':5432 -u '{username}' -p '{password}'"
                   .format(metadata_generator_path=metadata_generator_path,
                           sds_schema_name=sds_schema_name,
                           database_name=context.master_db,
                           host_name=context.master_db_server1,
                           metadata_name="edware",
                           username=context.master_db_user,
                           password=context.master_db_password))

        subprocess.call(command, shell=True)
        query = "select '{schema_name}' from information_schema.schemata where schema_name='{schema_name}'".format(schema_name=sds_schema_name)
        result = connection.execute(query).fetchone()
        assert(result is not None)


remove_parcc_ids_cat_tenant = [
    'Xwk5McYQSIRVCwDpzpWzMfHYetWqy5BUNy6Z2ogV',
    'WAhvqzYnESXaMgGYTK04IH1RPLeTatzuOubPg8U7',
    'WQSZZ0TnQLwHd1VBV596z6mqD7T07BZiUgXDa2o2',
    'SY0CEikzlomxMRW6NsjHhNuLpK17yruc5pjGavX8'
]


def sds_import_csv_files(csv_files, directory, metadata, tenant_name):
    for csv_file in csv_files:
        name = os.path.splitext(csv_file)[0]
        table_name = name
        csv_file_name = os.path.join(directory, csv_file)

        # Initialize vars for filtering cat data (so it is a little different from dog)
        state_code_idx = None
        student_parcc_id_idx = None
        with open(csv_file_name, 'rU') as data:
            csv_reader = csv.reader(data, delimiter=',')
            header_row = None
            print(csv_file_name)
            for row in csv_reader:
                if header_row is None:
                    header_row = row
                    if tenant_name in ('cat', 'fish'):
                        try:
                            state_code_idx = header_row.index('state_code')
                        except:
                            pass
                        try:
                            student_parcc_id_idx = header_row.index('student_parcc_id')
                        except:
                            pass
                    continue

                if tenant_name == 'cat':
                    if student_parcc_id_idx is not None:
                        if row[student_parcc_id_idx] in remove_parcc_ids_cat_tenant:
                            continue
                    if state_code_idx is not None:
                        row[state_code_idx] = 'NY'
                elif tenant_name == 'fish':
                    if state_code_idx is not None:
                        row[state_code_idx] = 'VT'

                values = dict(zip(header_row, row))

                table = Table(table_name, metadata, autoload=True)
                for c in table.columns:
                    if values[c.name] == '':
                        values[c.name] = None
                    elif isinstance(c.type, types.Integer):
                        values[c.name] = int(values[c.name])
                    elif isinstance(c.type, types.Boolean):
                        values[c.name] = to_boolean(values[c.name])
                    elif isinstance(c.type, types.LargeBinary):
                        values[c.name] = bytes(values[c.name].encode('utf-8'))
                    elif isinstance(c.type, types.DateTime):
                        try:
                            values[c.name] = datetime.strptime(values[c.name], '%Y-%m-%d %H:%M:%S.%f')

                        except ValueError:
                            try:
                                values[c.name] = datetime.strptime(values[c.name], '%Y-%m-%d %H:%M:%S.%f%z')

                            except ValueError:
                                values[c.name] = datetime.strptime(values[c.name] + '00', '%Y-%m-%d %H:%M:%S.%f%z')

                    elif isinstance(c.type, types.Date):
                        values[c.name] = datetime.strptime(values[c.name], '%Y-%m-%d')

                    else:
                        values[c.name] = str(values[c.name])

                table.insert().execute(values)


@when('I import sds data into the {tenant_name} tenant')
def step_impl(context, tenant_name):
    directory = os.path.abspath(
        os.path.join(_HERE, '..', 'data', 'fixture', 'SDS_CSV_FILES')
    )

    csv_files = []

    for directory_entry in os.listdir(directory):
        if os.path.isfile(os.path.join(directory, directory_entry)):
            if directory_entry.endswith(".csv"):
                csv_files.append(directory_entry)

    if tenant_name in ('cat',):
        db_user = context.master_db_user
        db_password = context.master_db_password
        db_server = context.master_db_server1
        database = context.master_db
        db_schema = context.master_db_schema

    elif tenant_name == 'dog':
        db_user = context.master_db_user_second_tenant
        db_password = context.master_db_password_second_tenant
        db_server = context.master_db_server2
        database = context.master_db_second_tenant
        db_schema = context.master_db_schema_second_tenant

    elif tenant_name == 'fish':
        db_user = context.master_db_user_third_tenant
        db_password = context.master_db_password_third_tenant
        db_server = context.master_db_server3
        database = context.master_db_third_tenant
        db_schema = context.master_db_schema_third_tenant
    else:
        raise AssertionError("Unknown tenant %s" % (tenant_name,))

    edware_master_url = get_formatted_postgres_url(db_user, db_password, db_server, 5432, database)
    engine = create_engine(edware_master_url)

    metadata = MetaData(bind=engine, schema=db_schema)
    metadata.reflect(bind=engine, schema=db_schema)

    sds_import_csv_files(csv_files, directory, metadata, tenant_name)


def to_boolean(string_value):
    if string_value.lower() in ['true', 't']:
        return True
    elif string_value.lower() in ['false', 'f']:
        return False
    else:
        raise ValueError("A boolean value must be T, t, True, true, F, f, False or false")


@when('I import tenant level sds data into the {tenant_name} tenant cds')
def step_impl(context, tenant_name):
    directory = os.path.abspath(
        os.path.join(_HERE, '..', 'data', 'fixture', 'SDS_CSV_FILES', 'cds_tenant', tenant_name)
    )
    print("directory is {dir}".format(dir=directory))

    csv_files = []

    for directory_entry in os.listdir(directory):
        if os.path.isfile(os.path.join(directory, directory_entry)):
            if directory_entry.endswith(".csv"):
                csv_files.append(directory_entry)

    print("csv files: {0}".format(csv_files))

    if tenant_name == 'cat':
        db_user = context.master_db_user_cds_first_tenant
        db_password = context.master_db_password_cds_first_tenant
        db_server = context.master_db_cds_first_tenant_server
        database = context.master_db_cds_first_tenant
        db_schema = context.master_db_schema_cds_first_tenant

    elif tenant_name == 'dog':
        db_user = context.master_db_user_cds_second_tenant
        db_password = context.master_db_password_cds_second_tenant
        db_server = context.master_db_cds_second_tenant_server
        database = context.master_db_cds_second_tenant
        db_schema = context.master_db_schema_cds_second_tenant
    else:
        raise Exception("Unknown tenant %s" % (tenant_name,))

    edware_master_url = get_formatted_postgres_url(db_user, db_password, db_server, 5432, database)
    engine = create_engine(edware_master_url)

    metadata = MetaData(bind=engine, schema=db_schema)
    metadata.reflect(bind=engine, schema=db_schema)

    sds_import_csv_files(csv_files, directory, metadata, tenant_name)


@when('I import consortium level sds data into the cds')
def step_impl(context):
    directory = os.path.abspath(
        os.path.join(_HERE, '..', 'data', 'fixture', 'SDS_CSV_FILES', 'cds_consortium')
    )

    csv_files = []

    for directory_entry in os.listdir(directory):
        if os.path.isfile(os.path.join(directory, directory_entry)):
            if directory_entry.endswith(".csv"):
                csv_files.append(directory_entry)

    db_user = context.master_db_user_cds_parcc
    db_password = context.master_db_password_cds_parcc
    db_server = context.master_db_cds_parcc_server
    database = context.master_db_cds_parcc
    db_schema = context.master_db_schema_cds_parcc

    edware_master_url = get_formatted_postgres_url(db_user, db_password, db_server, 5432, database)
    engine = create_engine(edware_master_url)

    metadata = MetaData(bind=engine, schema=db_schema)
    metadata.reflect(bind=engine, schema=db_schema)

    sds_import_csv_files(csv_files, directory, metadata, 'consortium')


def _get_ana_db_source_schema():
    schema_matcher = re.compile('^SRC_AMPDB_SCHE=(.*)$', flags=re.MULTILINE)
    with open(os.path.join(os.path.expanduser('~'), '.kettle', 'kettle.properties')) as fh:
        m = schema_matcher.search(fh.read())
        if m:
            return m.group(1)

        else:
            raise Exception("Could not find source schema in kettle.properties")


@when('I force the analytics ETL to run against {tenant_name} tenant data')
def step_impl(context, tenant_name):
    ana_db_source_schema = _get_ana_db_source_schema()

    if tenant_name == 'cat':
        dw_db_server = context.master_db_server1
        dw_db_name = context.master_db
        dw_db_user = context.master_db_user
        dw_db_password = context.master_db_password
        dw_db_schema = context.master_db_schema

        ana_db_server = context.analytics_state_master_db_server1
        ana_db_name = context.analytics_db
        ana_db_user = context.analytics_db_user
        ana_db_password = context.analytics_db_password
        ana_db_dest_schema = context.analytics_db_schema

    elif tenant_name == 'dog':
        dw_db_server = context.master_db_server2
        dw_db_name = context.master_db_second_tenant
        dw_db_user = context.master_db_user_second_tenant
        dw_db_password = context.master_db_password_second_tenant
        dw_db_schema = context.master_db_schema_second_tenant

        ana_db_server = context.analytics_state_master_db_server2
        ana_db_name = context.analytics_db_second_tenant
        ana_db_user = context.analytics_db_user_second_tenant
        ana_db_password = context.analytics_db_password_second_tenant
        ana_db_dest_schema = context.analytics_db_schema_second_tenant

    else:
        raise Exception("Unknown tenant %s" % (tenant_name,))

    dw_url = get_formatted_postgres_url(dw_db_user, dw_db_password, dw_db_server, 5432, dw_db_name)
    dw_engine = create_engine(dw_url)

    dw_metadata = MetaData(bind=dw_engine, schema=dw_db_schema)
    dw_metadata.reflect(bind=dw_engine, schema=dw_db_schema)

    ana_url = get_formatted_postgres_url(ana_db_user, ana_db_password, ana_db_server, 5432, ana_db_name)
    ana_engine = create_engine(ana_url)

    ana_source_meta = MetaData(bind=ana_engine, schema=ana_db_source_schema)
    ana_source_meta.reflect(bind=ana_engine, schema=ana_db_source_schema)

    # 1. drop source schema from analytics db & recreate
    try:
        ana_engine.execute(DropSchema(ana_db_source_schema, cascade=True))

    except Exception as e:
        print("Error dropping schema: %s" % (e,))
    ana_engine.execute(CreateSchema(ana_db_source_schema))

    # 2. copy rpt_*_sum tables from source db to analytics db
    for table_name in 'rpt_ela_sum', 'rpt_math_sum':
        full_table_name = "%s.%s" % (dw_db_schema, table_name)
        full_ana_table_name = "%s.%s" % (ana_db_source_schema, table_name)

        if full_table_name not in dw_metadata.tables:
            raise Exception("%s not in %s" % (full_table_name, sorted(dw_metadata.tables)))
        src_table = dw_metadata.tables[full_table_name]
        dst_table = Table(table_name, ana_source_meta)

        for column in src_table.columns:
            dst_table.append_column(column.copy())

        print("---> creating %s/%s" % (ana_url, full_ana_table_name))
        dst_table.create()

        insert = dst_table.insert()
        for row in src_table.select().execute():
            insert.execute(row)

    # 3. invoke kitchen
    with subprocess.Popen(['/opt/pentaho/data-integration/kitchen.sh', ' -param:AMP_POST_CLEAN=FALSE',
                           '-file', '/opt/pentaho/edware-analytics/ETL/jb_load.kjb']) as kettle:
        kettle.wait()
        if kettle.returncode:
            raise Exception("Kettle failed w/ rc %s" % (kettle.returncode,))
