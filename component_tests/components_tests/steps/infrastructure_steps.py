import os
import re
import boto
import subprocess
from behave import when
from sqlalchemy import create_engine
from sqlalchemy.schema import CreateSchema
from components_tests.common import get_formatted_postgres_url


def get_connection(context):
    dw_url = get_formatted_postgres_url(
        context.master_db_user,
        context.master_db_password,
        context.master_db_server1,
        5432, context.master_db)
    engine = create_engine(dw_url)
    return engine.connect()


def check_schema(connection, condition, schema_name):
    schema_exists = connection.execute(
        ' '.join("""SELECT exists(SELECT schema_name
            FROM information_schema.schemata WHERE
            schema_name = '{}')""".split()).format(schema_name))
    for row in schema_exists:
        if row[0] == condition:
            raise AssertionError()
    schema_exists.close()


@when('the data backup process is initiated')
def step_impl(context):
    here = os.environ['WORKSPACE']
    context.base_data_set_directory = os.path.abspath(
        os.path.join(here, 'edware_devops', 'ansible_playbooks'))
    env = os.environ
    env.update({'PATH': '/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin'})
    context.env = env


@when('the {schema_name} schema does not exist in the database')
def step_impl(context, schema_name):
    connection = get_connection(context)
    drop_schema = connection.execute("DROP SCHEMA IF EXISTS {}".format(schema_name))
    drop_schema.close()
    connection.close()


@when('a backup file is created for a database and exists in the S3 bucket')
def step_impl(context):
    backup_command = ' '.join('''cd {} && ansible-playbook -i applications/inventory/ec2.sh
                        applications/dw-backup.yml -e realm={}
                        -e vaulted_var_file=../vault_vars/dev_passwords.yml
                        --private-key=~/.ssh/amplify_parcc_ci_adminkeys.pem
                        '''.split()).format(context.base_data_set_directory, context.realm)
    result = subprocess.check_output(backup_command, shell=True, env=context.env)
    m = re.search('(\d+?){10}', str(result))
    global timestamp
    timestamp = m.group(0)
    s3 = boto.connect_s3()
    key = s3.get_bucket('parcc-db-backup-dev').get_key(
        'daily/cat/cat-dw-{}.sql.gz'.format(timestamp))
    assert key.exists()


@when('a new {schema_name} schema is created')
def step_impl(context, schema_name):
    connection = get_connection(context)
    schema = connection.execute(CreateSchema('{}'.format(schema_name)))
    schema.close()
    connection.close()


@when('a database is restored from the backup file')
def step_impl(context):
    restore_command = ' '.join(''' cd {} && ansible-playbook -i applications/inventory/ec2.sh
                        applications/dw-restore.yml -e realm={}
                        -e backup_period=daily -e tenant_name=cat -e timestamp={}
                        -e vaulted_var_file=../vault_vars/dev_passwords.yml
                        --private-key=~/.ssh/amplify_parcc_ci_adminkeys.pem &> /dev/null
                        '''.split()).format(context.base_data_set_directory, context.realm, timestamp)
    result = subprocess.call(restore_command, shell=True, env=context.env)
    assert result == 0


@when('a {schema_name} schema does not exist in the database after restoration')
def step_impl(context, schema_name):
    connection = get_connection(context)
    check_schema(connection, True, schema_name)
    connection.close()


@when('a {schema_name} schema exists in the database after restoration')
def step_impl(context, schema_name):
    connection = get_connection(context)
    check_schema(connection, False, schema_name)
    connection.close()


@when('a backup file does not exist in the S3 bucket')
def step_impl(context):
    s3 = boto.connect_s3()
    keys = s3.get_bucket('parcc-db-backup-dev').list()
    for key in keys:
        if timestamp in key.name:
            key.delete()
    assert True
