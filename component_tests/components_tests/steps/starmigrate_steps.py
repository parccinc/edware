from behave import given, when, then
from sqlalchemy.sql import select, func
from sqlalchemy import create_engine, types, and_, DDL
from sqlalchemy.schema import MetaData, Table
from time import sleep
from components_tests.common import get_formatted_postgres_url
import csv
import configparser
import subprocess
import datetime
import logging
import glob
import os
import re
import paramiko
from scp import SCPClient
import sys
import tempfile
from shutil import copyfile
import zipfile
import boto
import boto.s3
from boto.s3.key import Key

INPUT_TABLES = ['rpt_math_sum',
                'rpt_ela_sum']


def get_analytics_db_conn(context, tenant_name):
    context.analytics_tenant = tenant_name
    context.analytics_state_master = context.analytics_state_master_db_server1
    context.dump_tenant = 'cat'
    if tenant_name == 'second' or tenant_name == 'dog':
        context.analytics_state_master = context.analytics_state_master_db_server2
        context.dump_tenant = 'dog'

    analytics_master_url = get_formatted_postgres_url(context.analytics_db_user,
                                                      context.analytics_db_password,
                                                      context.analytics_state_master,
                                                      5432,
                                                      context.analytics_db)

    engine = create_engine(analytics_master_url)
    context.analytics_master_url = analytics_master_url
    return engine.connect()


@given('that we have a empty Analytics DB for the {tenant_name} tenant')
def step_impl(context, tenant_name):
    with get_analytics_db_conn(context, tenant_name) as anaconn:
        anaconn.execute(DDL('DROP SCHEMA IF EXISTS {schema} CASCADE'.format(schema=context.analytics_db_schema)))
        anaconn.execute(DDL('DROP SCHEMA IF EXISTS {schema} CASCADE'.format(schema=context.master_db_schema)))
        anaconn.execute(DDL('DROP SCHEMA IF EXISTS {schema} CASCADE'.format(schema=context.master_db_schema_second_tenant)))


@when('we add the known {input_type} assessment input data dumpfile for {tenant_name} tenant')
def step_impl(context, tenant_name, input_type):
    tenant = 'cat'
    if tenant_name == 'second' or tenant_name == 'dog':
        tenant = 'dog'

    context.dump_tenant = tenant
    context.analytics_tenant = tenant
    context.assessment_type = input_type
    context = get_dumpfile_location(context)

    # create one file
    create_trigger_file(1, location=context.dumpfile_location, context=context)


@then('the {table_name} table should have {num_rows} rows in analytics schema')
def step_impl(context, table_name, num_rows):
    engine = create_engine(get_analytics_url(context))

    metadata = MetaData(bind=engine, schema=context.analytics_db_schema)
    metadata.reflect(bind=engine, schema=context.analytics_db_schema)

    table = metadata.tables['.'.join([context.analytics_db_schema, table_name])]
    query = select([func.count()]).select_from(table)

    with engine.connect() as conn:
        actual = conn.execute(query).fetchone()[0]
        context.tc.assertEqual(int(num_rows), actual,
                               msg="expected {expected} rows, got: {actual}".format(
                                   expected=num_rows, actual=actual))


@then('the {table_name} table should have {num_rows} rows where the {column_name_1} column is {column_value_1} and {column_name_2} column is {column_value_2}')
def step_impl(context, table_name, num_rows, column_name_1, column_value_1, column_name_2, column_value_2):
    engine = create_engine(get_analytics_url(context))

    metadata = MetaData(bind=engine, schema=context.analytics_db_schema)
    metadata.reflect(bind=engine, schema=context.analytics_db_schema)

    table = metadata.tables['.'.join([context.analytics_db_schema, table_name])]
    query = select([func.count()], and_(table.columns[column_name_1] == column_value_1,
                                        table.columns[column_name_2] == column_value_2))
    with engine.connect() as conn:
        actual = conn.execute(query).fetchone()[0]
        context.tc.assertEqual(int(num_rows), actual,
                               msg="expected {expected} rows, got: {actual}".format(
                                   expected=num_rows, actual=actual))


@then('using {join_column} to join the {table_name_1} table where {column_name_1} is {column_value_1} with the {table_name_2} table where {column_name_2} is {column_value_2} results in {num_rows} rows')
def step_impl(context, table_name_1, table_name_2, num_rows, join_column, column_name_1, column_value_1, column_name_2, column_value_2):
    engine = create_engine(get_analytics_url(context))
    query = "select count(*) from analytics.{0} FIRST, analytics.{1} SECOND where FIRST.{2} = SECOND.{2} and FIRST.{3}='{4}' and SECOND.{5}='{6}';".format(table_name_1, table_name_2, join_column, column_name_1, column_value_1, column_name_2, column_value_2)

    with engine.connect() as conn:
        actual = int(conn.execute(query).fetchone()['count'])
        context.tc.assertEqual(int(num_rows), actual,
                               msg="expected {expected} rows, got: {actual}".format(
                                   expected=num_rows, actual=actual))


@then('the {table_name} table should have {num_rows} rows when joined with {table_name_2} on {join_column} and {join_column_2} and {column_name_1} column is {column_value_1}')
def step_impl(context, table_name, table_name_2, num_rows, join_column, join_column_2, column_name_1, column_value_1):
    engine = create_engine(get_analytics_url(context))

    metadata = MetaData(bind=engine, schema=context.analytics_db_schema)
    metadata.reflect(bind=engine, schema=context.analytics_db_schema)

    table = metadata.tables['.'.join([context.analytics_db_schema, table_name])]
    table2 = metadata.tables['.'.join([context.analytics_db_schema, table_name_2])]
    query = select([func.count()], and_(table2.columns[column_name_1] == column_value_1)).select_from(table.join(table2, table.columns[join_column] == table2.columns[join_column_2]))
    with engine.connect() as conn:
        actual = conn.execute(query).fetchone()[0]
        context.tc.assertEqual(int(num_rows), actual,
                               msg="expected {expected} rows, got: {actual}".format(
                                   expected=num_rows, actual=actual))


@then('the number of columns in {process_string}')
def step_impl(context, process_string):
    engine = create_engine(get_analytics_url(context))

    metadata = MetaData(bind=engine, schema=context.analytics_db_schema)
    metadata.reflect(bind=engine, schema=context.analytics_db_schema)
    table_name, expected_num_columns = process_string.split(' should be ')
    calculated_num_columns = sum(1 for _ in metadata.tables['.'.join([context.analytics_db_schema, table_name])].columns)
    context.tc.assertEqual(calculated_num_columns, int(expected_num_columns),
                           msg="expected num columns: {expected}, actual num columns: {actual}".format(
                               expected=int(expected_num_columns), actual=calculated_num_columns))


@then('{process_string} should match with the rowcount in corresponding report tables in edware report database')
def step_impl(context, process_string):
    analytics_engine = create_engine(get_analytics_url(context))
    analytics_metadata = MetaData(bind=analytics_engine, schema=context.analytics_db_schema)
    analytics_metadata.reflect(bind=analytics_engine, schema=context.analytics_db_schema)
    analytics_table_name = process_string.split('the rowcount in ')[1]
    analytics_query = select([func.count()]).select_from(analytics_metadata.tables['.'.join([context.analytics_db_schema, analytics_table_name])])

    reporting_engine = create_engine(get_analytics_url(context))
    master_db_schema_name = context.master_db_schema
    if context.dump_tenant == 'dog':
        master_db_schema_name = context.master_db_schema_second_tenant
    reporting_metadata = MetaData(bind=reporting_engine, schema=master_db_schema_name)
    reporting_metadata.reflect(bind=reporting_engine, schema=master_db_schema_name)
    ela_reporting_table_name = 'rpt_ela_sum'
    ela_reporting_query = select([func.count()]).select_from(reporting_metadata.tables['.'.join([master_db_schema_name, ela_reporting_table_name])])

    with reporting_engine.connect() as conn:
        ela_reporting_rows = conn.execute(ela_reporting_query).fetchone()[0]

    math_reporting_table_name = 'rpt_math_sum'
    math_reporting_query = select([func.count()]).select_from(reporting_metadata.tables['.'.join([master_db_schema_name, math_reporting_table_name])])

    with reporting_engine.connect() as conn:
        reporting_rows = conn.execute(math_reporting_query).fetchone()[0]

    with analytics_engine.connect() as conn:
        analytics_rows = conn.execute(analytics_query).fetchone()[0]
    context.tc.assertEqual(analytics_rows, ela_reporting_rows + reporting_rows)


@given('we have {data_items} data files')
def step_impl(context, data_items):
    """
    'incomplete': make sure that the location has one incomplete trigger file
    'multiple trigger': make sure that the location has multiple valid trigger files for starmigrate
    'one trigger': make sure that the location has one and only one of valid trigger files for starmigrate
    'no trigger': make sure that the location is devoid of valid trigger files for starmigrate
    """
    context = get_dumpfile_location(context)
    set_custom_config_location(context)
    custom_config = {'starmigrate.dump_gz_archive_path': context.dump_gz_archive_path,
                     'migrate.dumpfile_location': context.dumpfile_location}
    make_custom_config(custom_config, context)
    if data_items == 'incomplete':
        context.corrupted_file = True
        create_trigger_file(1, location=context.dumpfile_location, context=context, create_corrupted=True)
    elif data_items == 'multiple trigger':
        create_trigger_file(5, location=context.dumpfile_location, context=context)
    elif data_items == 'one trigger':
        create_trigger_file(1, location=context.dumpfile_location, context=context)
    elif data_items == 'no trigger':    # skip file creation for this case
        pass


@given('the analytics database does not exist')
def step_impl(context):
    """
    wipe out the db urls before running
    """
    context = get_dumpfile_location(context)
    # create one file
    create_trigger_file(1, location=context.dumpfile_location, context=context)
    context.dump_tenant = getattr(context, "dump_tenant", get_default_tenant())
    tenant_db_url = 'starmigrate.dest.db.%s.url' % context.dump_tenant
    custom_values = {tenant_db_url: ""}
    set_custom_config_location(context)
    make_custom_config(custom_values, context)


@given('StarMigrate replication fails')
def step_impl(context):
    """
    screw up the replication settings to ensure failure
    """
    context = get_dumpfile_location(context)
    # create one file
    create_trigger_file(1, location=context.dumpfile_location, context=context)
    custom_values = {"starmigrate.edreplicate.conductor.player_find_time_wait": "0",
                     "starmigrate.edreplicate.conductor.thread.lock_timeout": "0",
                     "starmigrate.edreplicate.replication_monitor.timeout": "0",
                     "starmigrate.edreplicate.thread.lock_timeout": "0",
                     "starmigrate.edreplicate.replication_monitor.time_lag_tolerance": "0",
                     "starmigrate.edreplicate.replication_monitor.replication_lag_tolerance": "0",
                     "starmigrate.edreplicate.replication_monitor.apply_lag_tolerance": "0",
                     "starmigrate.edreplicate.conductor.exchange": "DUMMY VALUE",
                     "starmigrate.edreplicate.conductor.routing_key": "DUMMY VALUE",
                     "starmigrate.edreplicate.replication_monitor.db.cat.schema_name": "DUMMY VALUE",
                     "starmigrate.edreplicate.replication_monitor.db.dog.schema_name": "DUMMY VALUE", }
    set_custom_config_location(context)
    make_custom_config(custom_values, context)


@given('the expected env is properly set up')
def step_impl(context):
    """
    Set up custom config here as needed.
    """
    # custom_values = {}
    context = get_dumpfile_location(context)
    custom_config = {'starmigrate.dump_gz_archive_path': context.dump_gz_archive_path,
                     'migrate.dumpfile_location': context.dumpfile_location}
    set_custom_config_location(context)
    make_custom_config(custom_config, context)


@when('enough time is given for StarMigrate to process')
def step_impl(context):
    context.call_start_time = datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S.%f")
    if context.environment == 'local':
        command = "python -m starmigrate.main -t tenant"
        subprocess.call(command, shell=True)
    elif context.environment == 'AWS':
        # Enough time for the slaves to accept connections again after
        # migration
        sleep(60)
    else:
        raise AssertionError('Invalid environment')


@when('StarMigrate is started')
def step_impl(context):
    """
    Invoke StarMigrate as a subprocess, providing different command line switch values
    depending on needed behavior
    """

    context.dump_tenant = getattr(context, "dump_tenant", get_default_tenant())
    ini_switch = ""
    if context.custom_config_location:
        ini_switch = "-i {config}".format(config=context.custom_config_location)
    cmd_mask = "python -m starmigrate.main -t {tenant} {ini_switch}"
    command = cmd_mask.format(tenant=context.dump_tenant, ini_switch=ini_switch)
    run_starmigrate(context, command)


@when('Starmigrate encounters a problem')
def step_impl(context):
    """
    run starmigrate but ignore any exceptions generated
    """
    context.dump_tenant = getattr(context, "dump_tenant", get_default_tenant())
    ini_switch = ""
    if context.custom_config_location:
        ini_switch = "-i {config}".format(config=context.custom_config_location)
    cmd_mask = "python -m starmigrate.main -t {tenant} {ini_switch}"
    command = cmd_mask.format(tenant=context.dump_tenant, ini_switch=ini_switch)
    try:
        run_starmigrate(context, command)
    except:
        # we expect StarMigrate to fail
        pass


@then('StarMigrate should do nothing')
def step_impl(context):
    log_string = "no data to process"
    context.tc.assertTrue(context.log_capture.findEvent(log_string))


@then('StarMigrate should load that data')
def step_impl(context):
    # filename should be the last trigger filename, with complete replaced with processing
    expected_filename = re.escape(re.sub("complete", "processing", context.last_trigger_file))
    expected_log = "loaded dumpfile {filename}".format(filename=expected_filename)
    context.tc.assertTrue(context.log_capture.findEvent(expected_log))


@then('begin the star migration process')
def step_impl(context):
    log_string = "beginning StarMigration"
    context.tc.assertTrue(context.log_capture.findEvent(log_string))


@then('StarMigrate should load the most recent file')
def step_impl(context):
    # filename should be the last trigger filename, with complete replaced with processing
    expected_filename = re.escape(re.sub("complete", "processing", context.last_trigger_file))
    expected_log = "loaded dumpfile {filename}".format(filename=expected_filename)
    context.tc.assertTrue(context.log_capture.findEvent(expected_log))


@then('delete all the relevant files when it is done')
def step_impl(context):
    # glob the directory, see that there is are no files
    context.dump_tenant = getattr(context, "dump_tenant", get_default_tenant())

    filename_format = "*-edmigrate-dump.gz"
    trigger_pattern = get_dbdump_path_pattern(context, context.dumpfile_location,
                                              filename_format, context.dump_tenant)
    trigger_files = glob.glob(trigger_pattern)
    context.tc.assertEqual(len(trigger_files), 0)


@then('StarMigrate should leave the file as it found it')
def step_impl(context):
    # check that the one trigger filename reads as complete, not processing
    if context.environment == 'local':
        context.tc.assertTrue(os.path.exists(context.last_trigger_file))
    elif context.environment == 'AWS':
        ssh = ssh_to_sm_master(context)
        stdin, stdout, stderr = ssh.exec_command("if test -e %s; then echo 'exists'; fi" % context.last_trigger_file, timeout=10)
        test_result = stdout.readline().rstrip("\n")
        context.tc.assertEqual(test_result, "exists")


@then('StarMigrate should mark the file as failed')
def step_impl(context):
    # check that the one trigger filename reads as complete, not processing
    expected_filename = re.sub("complete", "failed", context.last_trigger_file)
    if context.environment == 'local':
        context.tc.assertTrue(os.path.exists(expected_filename))
    elif context.environment == 'AWS':
        ssh = ssh_to_sm_master(context)
        stdin, stdout, stderr = ssh.exec_command("if test -e %s; then echo 'exists'; fi" % expected_filename, timeout=10)
        test_result = stdout.readline().rstrip("\n")
        context.tc.assertEqual(test_result, "exists")


@then('StarMigrate should halt execution')
def step_impl(context):
    # check that the one trigger filename reads as complete, not processing
    context.tc.assertTrue(os.path.exists(context.last_trigger_file))


@then('provide a log message "{expected_log}"')
def step_impl(context, expected_log):
    context.tc.assertTrue(context.log_capture.findEvent(expected_log))


@then('StarMigrate should raise an exception')
def step_impl(context):
    exception_indication_string = "Traceback (most recent call last):"
    context.tc.assertTrue(context.log_capture.findEvent(re.escape(exception_indication_string)))


@when('enough time is given for replication and we check {server}')
def step_impl(context, server):
    sleep(60)
    context.analytics_db_server = server


@when('Kettle completes successfully')
def step_impl(context):
    """
    Kettle succeeds: we get a Successful Refresh message
    """
    context.execute_steps('''
        Then provide a log message "Successful Refresh"
    ''')


@then('we put known files into migrate_dump folder for this tenant')
def step_impl(context):
    source_file = context.star_dump_file
    # extract subarchives from root archive
    source_dir = os.path.dirname(source_file)

    subfiles = []
    with zipfile.ZipFile(source_file, mode='r') as zipf:
        for subfile in zipf.namelist():
            zipf.extract(subfile, source_dir)
            subfiles.append(os.path.join(source_dir, subfile))

    dump_gz_archive_path = getattr(context, "dump_gz_archive_path", None) or '/vol/migrate_dbdump/'
    scp_on_starmigrate(context, subfiles, dump_gz_archive_path, multiple=True)
    for fpath in subfiles:
        os.unlink(fpath)


@then('we trigger kettle')
def step_impl(context):
    run_command = '/opt/pentaho/run_load.sh'
    ssh = ssh_to_sm_master(context)
    stdin, stdout, stderr = ssh.exec_command(run_command)
    output = "".join(stdout.readlines())
    ssh.close()

###################################################
#                                                 #
#                  helper functions               #
#                                                 #
###################################################


def run_starmigrate(context, command):

    logger = logging.getLogger('starmigrate_steps')

    if context.environment == 'local':
        doit = subprocess.Popen(command, universal_newlines=True, shell=True,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (done, fail) = doit.communicate()
        code = doit.wait()
        logger.info(done)
        logger.info(fail)
    elif context.environment == 'AWS':
        ssh = ssh_to_sm_master(context)
        logfile = '/opt/edware/log/application.log'

        # get the log file's last line
        loginitin, loginitout, loginiterr = ssh.exec_command('tail -n 1 {0}'.format(logfile))
        line_from = loginitout.readline()
        # execute our starmigrate command
        comstdin, comstdout, comstderr = ssh.exec_command('source /opt/virtualenv/starmigrate/bin/activate; {0}'.format(command), get_pty=True, timeout=60)

        # This is necessary for some reason, SM command won't execute. MJacob feels this might block.
        # also we need to grab stdout to catch starmigrate run command errors
        command_output, command_errors = comstdout.readlines(), comstderr.readlines()

        # get the updated log file and read after the last line
        logrin, logrout, logrerr = ssh.exec_command('tail -n "+$(($(grep -Fn \'{0}\' {1} | head -n 1 | cut -d ":" -f 1)+1))" {1}'.format(line_from, logfile), timeout=5)

        output = "".join(logrout.readlines())

        ssh.close()

        # feed the logger the captured lines
        logger.info(output)
        logger.info(command_output)
        logger.info(command_errors)
    else:
        raise AssertionError('Invalid environment')


def get_dumpfile_location(context):
    dumpfile_location = getattr(context, "dumpfile_location", None)
    dump_gz_archive_path = getattr(context, "dump_gz_archive_path", None)
    if dumpfile_location and dump_gz_archive_path:
        return context
    if context.environment == 'local':
        context.dumpfile_location = tempfile.mkdtemp()
        context.dump_gz_archive_path = tempfile.mkdtemp()
    elif context.environment == 'AWS':
        context.dumpfile_location = dump_path = "/tmp/smft/%s" % (datetime.datetime.now().strftime("%Y%m%d%H%M%S%f"))
        context.dump_gz_archive_path = gz_path = "/vol/migrate_dbdump/"
        ssh = ssh_to_sm_master(context)
        for tmp_path in [dump_path, gz_path]:
            stdin, stdout, stderr = ssh.exec_command("mkdir -p %s" % tmp_path, timeout=10)
            if stdout.channel.recv_exit_status():
                raise OSError("unable to create location '{tmp_location}' on remote server: {server}".format(
                    tmp_location=tmp_path, server=stderr.readlines()))
        ssh.close()
    return context


def make_custom_config(custom_dict, context=None):
    """
    take a dict of custom values, read in standard config, replace and write
    config to location
    """
    location = context.custom_config_location
    if location is None:
        raise ValueError("location for custom config can not be None")

    ini_file = '/opt/edware/conf/smarter.ini'
    config = configparser.ConfigParser()
    if context.environment == 'local':
        if not os.path.exists(ini_file):
            raise FileNotFoundError("standard config file not found: %s" % (ini_file,))

        config.read(ini_file)
    elif context.environment == 'AWS':
        ssh = ssh_to_sm_master(context)
        stdin, stdout, stderr = ssh.exec_command("cat %s" % ini_file, timeout=10)
        if stdout.channel.recv_exit_status():
            raise FileNotFoundError("unable to cat ini_file %s on remote server: %s" % (ini_file, stderr.readlines()))
        config.readfp(stdout)
        ssh.close()

    section = 'app:main'
    for key in config[section]:
        config[section][key] = custom_dict.get(key, config[section][key])

    if context.environment == 'local':
        with open(location, 'w') as configfile:
            config.write(configfile)
    elif context.environment == 'AWS':
        ssh = ssh_to_sm_master(context)
        sftp = paramiko.SFTPClient.from_transport(ssh.get_transport())
        remote_conf = sftp.open(location, "w")
        config.write(remote_conf)
        remote_conf.close()
        ssh.close()


def create_trigger_file(num_files, location=None, context=None, create_corrupted=False):
    """
    create functional trigger files for the test
    create_corrupted: If yes create bad dump that can't be processed by starmigrate
    """
    context.dump_tenant = getattr(context, "dump_tenant", get_default_tenant())
    source_file = get_dbdump_source_file(context, create_corrupted)
    create_trigger_file_with_source(source_file, num_files, location=location, context=context)


def create_trigger_file_with_source(source_file, num_files, location=None, context=None):
    """
    create trigger files for the test
    """
    if location is None:
        raise ValueError("location for dumpfiles can not be None")

    for i in range(0, num_files):
        filename_format = "{dbid}-edmigrate-dump.gz"
        destination = get_dbdump_path_pattern(context, location, filename_format, context.dump_tenant)
        if i == num_files - 1:
            context.last_trigger_file = destination

        if context.environment == 'local':
            copyfile(source_file, destination)
        elif context.environment == 'AWS':
            scp_on_starmigrate(context, source_file, destination)


def scp_on_starmigrate(context, source, destination, multiple=False):
    """
    copy file on remote starmigrate server
    """
    ssh = ssh_to_sm_master(context)
    scp = SCPClient(ssh.get_transport())
    scp.put(source, destination, recursive=multiple)
    ssh.close()


def get_dbdump_path_pattern(context, location, filename_format, tenant):
    tenant_separator = "@^"
    status = "complete"
    status_separator = "@#"
    formatted_filename = filename_format.format(dbid=datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S.%f"))
    filename = "%s%s%s%s%s" % (tenant, tenant_separator, formatted_filename, status_separator, status)
    return '{location}/{filename}'.format(location=location.rstrip('/'), filename=filename)


def get_dbdump_source_file(context, get_corrupted=False):
    dumpfile_mask = 'sample_bad_dbdump_{file_suffix}.gz' if get_corrupted else 'sample_dbdump_{file_suffix}.gz'
    dump_file = dumpfile_mask.format(file_suffix=get_source_file_suffix(context))

    here = os.path.dirname(__file__)
    source_file = os.path.abspath(os.path.join(here, '..', 'data', 'fixture', 'Trigger_DumpFile', dump_file))
    return source_file


def get_source_file_suffix(context):
    type_suffix = ""
    assessment_type = getattr(context, "assessment_type", None)
    if assessment_type:
        type_suffix = "%s_" % assessment_type
    if context.environment == 'local':
        return "%s%s" % (type_suffix, context.dump_tenant)
    elif context.environment == 'AWS':
        return "%s%s" % (type_suffix, "aws")


def ssh_to_sm_master(context):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    private_key_file = os.path.join(os.path.expanduser('~'), '.ssh', 'amplify_parcc_ci_adminkeys.pem')
    private_key = paramiko.RSAKey.from_private_key_file(private_key_file)
    user = 'root'

    master_host = get_analytics_db_server_for_tenant(context)

    ssh.connect(hostname=master_host, username=user, pkey=private_key)

    return ssh


def get_default_tenant():
    return "dog"


def get_analytics_db_server_for_tenant(context):
    context.dump_tenant = getattr(context, "dump_tenant", get_default_tenant())
    context.analytics_db_server = getattr(context, "analytics_db_server", "master")
    if context.dump_tenant == "cat":
        if context.analytics_db_server == "slave 1":
            return context.analytics_state_slave_db_server1
        elif context.analytics_db_server == "slave 2":
            return context.analytics_state_slave_db_server2
        else:
            return context.analytics_state_master_db_server1
    elif context.dump_tenant == "dog":
        if context.analytics_db_server == "slave 1":
            return context.analytics_state_slave_db_server3
        elif context.analytics_db_server == "slave 2":
            return context.analytics_state_slave_db_server4
        else:
            return context.analytics_state_master_db_server2
    else:
        raise AssertionError('Invalid tenant')


def get_analytics_url(context, alt_schema=None):
    user = context.analytics_db_user
    password = context.analytics_db_password
    database = context.analytics_db
    server = get_analytics_db_server_for_tenant(context)
    schema = context.analytics_db_schema
    if alt_schema is not None:
        schema = alt_schema
    master_url = get_formatted_postgres_url(user, password, server, 5432, database)
    return master_url


def set_custom_config_location(context):
    if hasattr(context, "dumpfile_location"):
        context.custom_config_location = "{}/{}".format(context.dumpfile_location, "custom_config")
    else:
        context.custom_config_location = None
