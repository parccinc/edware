from behave import when, then, given
import os


@when('the {db_server} is ready')
def step_impl(context, db_server):
    if db_server == 'Cat State Master Data Store':
        context.DB_SERVER_HOST = context.analytics_state_master_db_server1
    elif db_server == 'Cat State Slave Data Store 1':
        context.DB_SERVER_HOST = context.analytics_state_slave_db_server1
    elif db_server == 'Cat State Slave Data Store 2':
        context.DB_SERVER_HOST = context.analytics_state_slave_db_server2
    elif db_server == 'Dog State Master Data Store':
        context.DB_SERVER_HOST = context.analytics_state_master_db_server2
    elif db_server == 'Dog State Slave Data Store 1':
        context.DB_SERVER_HOST = context.analytics_state_slave_db_server3
    elif db_server == 'Dog State Slave Data Store 2':
        context.DB_SERVER_HOST = context.analytics_state_slave_db_server4
    elif db_server == 'Pentaho Server':
        context.DB_SERVER_HOST = context.pentaho_server
    elif db_server == 'Pentaho Utility Server':
        context.DB_SERVER_HOST = context.pentaho_utility
    assert True


@then('I should be able to ping the {db_store}')
def step_impl(context, db_store):
    ping_command = "ping -c 1 " + context.DB_SERVER_HOST
    if os.system(ping_command) == 0:
        assert True
    else:
        assert False
