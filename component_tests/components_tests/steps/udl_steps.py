from components_tests.bucket_helper import BucketHelper
from components_tests.common import get_edware_stats_url, get_formatted_postgres_url
from behave import given, when, then
from datetime import datetime
from time import sleep
from sqlalchemy.sql import select, func, and_
from sqlalchemy import create_engine
from sqlalchemy.schema import MetaData, Table
import os
import shutil
import subprocess
import paramiko
import tarfile
import gnupg
import hashlib
import ast

__author__ = 'smuhit'


def create_archive_from_data_set(data_set):
    here = os.path.dirname(__file__)

    base_data_set_directory = os.path.abspath(
        os.path.join(here, '..', 'data', 'data_sets')
    )

    recede = '..'
    previous_directory = os.getcwd()
    os.chdir(os.path.join(base_data_set_directory, data_set))

    if '/' in data_set:
        data_set = '_'.join(data_set.split('/'))
        recede = '../..'

    archive_name = data_set + '.tar.gz'

    with tarfile.open(name=os.path.join(recede, archive_name), mode='w:gz') as archive_file:
        for directory_entry in os.listdir('.'):
            if os.path.isfile(directory_entry):
                if not os.path.basename(directory_entry).startswith("."):
                    archive_file.add(directory_entry)

    os.chdir(previous_directory)

    return os.path.join(base_data_set_directory, archive_name)


def create_data_set(data_set, key, recipient, passphrase, location):
    here = os.path.dirname(__file__)

    gpg_home_directory = os.path.abspath(
        os.path.join(here, '..', 'data', 'keys', location)
    )

    gpg = gnupg.GPG(gnupghome=gpg_home_directory)

    archive_file = create_archive_from_data_set(data_set)
    encrypted_archive_file = archive_file + '.gpg'
    with open(archive_file, 'rb') as f:
        gpg.encrypt_file(file=f, recipients=[
                         recipient], sign=key, passphrase=passphrase, output=encrypted_archive_file)
    os.remove(archive_file)

    return encrypted_archive_file


def create_md5_checksum_file(file_with_path):
    md5 = hashlib.md5()
    checksum_file = file_with_path + '.done'
    with open(file_with_path, 'rb') as f:
        data = f.read()
        md5.update(data)
    md5_hash = md5.hexdigest()

    with open(checksum_file, 'w') as f:
        f.write('MD5 (' + file_with_path + ') = ' + md5_hash)
    return checksum_file


@given('an empty {table_name} table')
def step_impl(context, table_name):
    if table_name == 'UDL stats':
        schema = context.edware_stats_schema
        db_url = get_edware_stats_url(context)
        table = 'udl_stats'
    elif table_name == 'UDL batch':
        schema = context.udl_schema
        db_url = get_formatted_postgres_url(context.loader_db_user, context.loader_db_password,
                                            context.loader_db_server, 5432, context.loader_db)
        table = 'udl_batch'
    else:
        raise AssertionError('Invalid table to empty')

    engine = create_engine(db_url)
    metadata = MetaData(bind=engine, schema=schema)
    metadata.reflect(bind=engine, schema=schema)
    with engine.connect() as conn:
        table_to_delete = Table(table, metadata)
        conn.execute(table_to_delete.delete())
        count = select([func.count()]).select_from(table_to_delete)
        record_count = conn.execute(count).fetchall()[0][0]
        assert record_count == 0


@when('{file_type} dropped into the {tenant_lz}')
def step_impl(context, file_type, tenant_lz):

    here = os.path.dirname(__file__)
    if file_type == 'a valid ELA summative file is':
        data_files = [create_data_set('ELA_Summative', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid ELA summative file in OLD FORMAT is':
        data_files = [create_data_set('ELA_Summative_old_format', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a ELA Summative extracted':
        data_files = [create_data_set('ELA_Summative_extracted', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a ELA Summative 2016 file':
        data_files = [create_data_set('ELA_Summative_Spring_2016', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid Math Summative file is':
        data_files = [create_data_set('Math_Summative', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a file with a missing subject column in the CSV is':
        data_files = [create_data_set('CSV_Missing_Subject', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a file with an incorrect subject value in the CSV is':
        data_files = [create_data_set('CSV_Wrong_Subject', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'an unencrypted file is':
        data_file = create_archive_from_data_set('Math_Summative')
        os.rename(data_file, data_file + '-unencrypted.gpg')
        data_file += '-unencrypted.gpg'
        data_files = [data_file]
    elif file_type == 'a file with missing fields in the JSON is':
        data_files = [create_data_set('JSON_Missing_Fields', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'an unsupported file is':
        data_files = [os.path.abspath(
            os.path.join(here, '..', 'data', 'wrong_file_type.txt')
        )]
    elif file_type == 'multiple valid files are':
        data_files = [create_data_set('Math_Summative', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('ELA_Summative', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]

    elif file_type == 'multiple valid summative SDS files are':
        data_files = [create_data_set('SDS_ELA_Summative_3', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_ELA_Summative_9', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_ELA_Summative_10', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_ELA_Summative_11', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_Math_Summative_03', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_Math_Summative_05', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_Math_Summative_08', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_Math_Summative_Alg1', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_Math_Summative_Alg2', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_Math_Summative_Geom', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_Math_Summative_IMath1', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_Math_Summative_IMath2', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_Math_Summative_IMath3', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]

    elif file_type == 'multiple valid summative SDS item files are':
        data_files = [create_data_set('SDS_Math_Summative_Item', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_ELA_Summative_Item', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]

    elif file_type == 'a valid Item psychometric data file is':
        data_files = [create_data_set(
            'Item_Metadata', context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]

    elif file_type == 'multiple valid diagnostic SDS files are':
        data_files = [create_data_set('SDS_ELA_Diagnostic_Vocabulary_3', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_ELA_Diagnostic_Writing_3', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_ELA_Diagnostic_Decoding_3', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_ELA_Diagnostic_Fluency_3', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_ELA_Diagnostic_Comprehension_3', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_ELA_Diagnostic_Survey_3', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_MATH_Diagnostic_Locator_03', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_MATH_Diagnostic_Locator_05', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_MATH_Diagnostic_Locator_08', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_MATH_Diagnostic_Cluster_03', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_MATH_Diagnostic_Cluster_05', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_MATH_Diagnostic_Cluster_08', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_MATH_Diagnostic_GradeLevel_03', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_MATH_Diagnostic_GradeLevel_05', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_MATH_Diagnostic_GradeLevel_08', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_MATH_Diagnostic_Fluency_03', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_MATH_Diagnostic_Fluency_05', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_MATH_Diagnostic_Fluency_08', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_MATH_Diagnostic_Progression_03', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_MATH_Diagnostic_Progression_05', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location),
                      create_data_set('SDS_MATH_Diagnostic_Progression_08', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]

    elif file_type == 'a file without a JSON is':
        data_files = [create_data_set(
            'Missing_JSON', context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a file that contains a mix of valid and invalid types is':
        data_files = [create_data_set('JSON_And_Invalid_File', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid psychometric file is':
        data_files = [create_data_set(
            'Psychometric', context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid item metadata psychometric file is':
        data_files = [create_data_set('New_Item_Psychometric_Metadata', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'the generated file is':
        data_files = [context.generated_gpg]
    elif file_type == 'a valid MYA Math file is':
        data_files = [create_data_set(
            'MYA_Math', context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid MYA ELA file is':
        data_files = [create_data_set(
            'MYA_ELA', context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid MYA Math item file is':
        data_files = [create_data_set('MYA_Math_Item', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid MYA ELA item file is':
        data_files = [create_data_set(
            'MYA_ELA_Item', context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid S&L Assessment file is':
        data_files = [create_data_set(
            'S&L_Asmt', context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid S&L Task file is':
        data_files = [create_data_set('S&L_Student_Task', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid ELA RIF file is':
        data_files = [create_data_set(
            'ELA_RIF', context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid Math RIF file is':
        data_files = [create_data_set(
            'Math_RIF', context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    ## NEGATIVE CASE TESTS block ##
    elif 'a Summative Assessment CSV file with missing' in file_type:
        file_type = file_type.replace(
            'a Summative Assessment CSV file with missing', '').strip()
        if file_type == 'state abbreviation header is':
            data_files = [create_data_set('Summative_Assessment_CSV_Missing_Headers_Fields/State_Abbreviation_Missing_Header',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        elif file_type == 'testing district identifier column is':
            data_files = [create_data_set('Summative_Assessment_CSV_Missing_Headers_Fields/Responsible_District_Identifier_Missing_Header_And_Field',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        elif file_type == 'responsible school field data is':
            data_files = [create_data_set('Summative_Assessment_CSV_Missing_Headers_Fields/Responsible_School_Missing_Field',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        elif file_type == 'parcc student identifier field data is':
            data_files = [create_data_set('Summative_Assessment_CSV_Missing_Headers_Fields/PARCC_Student_Identifier_Missing_Field',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        elif file_type == 'last name field data is':
            data_files = [create_data_set('Summative_Assessment_CSV_Missing_Headers_Fields/Last_Name_Missing_Field',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        elif file_type == 'year field data is':
            data_files = [create_data_set('Summative_Assessment_CSV_Missing_Headers_Fields/Year_Missing_Field',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        elif file_type == 'record type field data is':
            data_files = [create_data_set('Summative_Assessment_CSV_Missing_Headers_Fields/Record_Type_Missing_Field',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        elif file_type == 'test code field data is':
            data_files = [create_data_set('Summative_Assessment_CSV_Missing_Headers_Fields/Test_Code_Missing_Field',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif 'a Summative Assessment JSON file with missing' in file_type:
        file_type = file_type.replace(
            'a Summative Assessment JSON file with missing', '').strip()
        if file_type == 'summative period key is':
            data_files = [create_data_set('Summative_Assessment_JSON_Missing_Key_Values/Summative_Period_Missing_Key',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        elif file_type == 'test code key is':
            data_files = [create_data_set('Summative_Assessment_JSON_Missing_Key_Values/Test_Code_Missing_Key',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        elif file_type == 'test levels key is':
            data_files = [create_data_set('Summative_Assessment_JSON_Missing_Key_Values/Test_Levels_Missing_Key',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        elif file_type == 'assessment performance level identifier value is':
            data_files = [create_data_set('Summative_Assessment_JSON_Missing_Key_Values/Assessment_Performance_Level_Identifier_Missing_Value',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        elif file_type == 'summative period value is':
            data_files = [create_data_set('Summative_Assessment_JSON_Missing_Key_Values/Summative_Period_Missing_Value',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        elif file_type == 'test code value is':
            data_files = [create_data_set('Summative_Assessment_JSON_Missing_Key_Values/Test_Code_Missing_Value',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        elif file_type == 'test levels value is':
            data_files = [create_data_set('Summative_Assessment_JSON_Missing_Key_Values/Test_Levels_Missing_Value',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        elif file_type == 'test level cutpoint low value is':
            data_files = [create_data_set('Summative_Assessment_JSON_Missing_Key_Values/Test_Level_Cutpoint_Low_Missing_Value',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif 'a Summative Assessment JSON file with null value for' in file_type:
        file_type = file_type.replace(
            'a Summative Assessment JSON file with null value for', '').strip()
        if file_type == 'summative period is':
            data_files = [create_data_set('Summative_Assessment_JSON_Missing_Key_Values/Summative_Period_Null_Value',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif 'an ELA Vocab CSV file with missing' in file_type:
        file_type = file_type.replace(
            'an ELA Vocab CSV file with missing', '').strip()
        if file_type == 'assessment score field data is':
            data_files = [create_data_set('Diagnostic_ELA_Missing_Fields/Assessment_Score_Missing_Field',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        if file_type == 'record type field data is':
            data_files = [create_data_set('Diagnostic_ELA_Missing_Fields/Record_Type_Missing_Field', context.gpg_key,
                                          context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        if file_type == 'birthdate field data is':
            data_files = [create_data_set('Diagnostic_ELA_Missing_Fields/Birthdate_Missing_Field', context.gpg_key,
                                          context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif 'a Math Diagnostic CSV file with missing' in file_type:
        file_type = file_type.replace(
            'a Math Diagnostic CSV file with missing', '').strip()
        if file_type == 'assessment score field data is':
            data_files = [create_data_set('Diagnostic_Math_Missing_Fields/Assessment_Score_Missing_Field',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
       # Commented cause record_type is not mandatory field anymore
       # if file_type == 'record type field data is':
       #     data_files = [create_data_set('Diagnostic_Math_Missing_Fields/Record_Type_Missing_Field', context.gpg_key,
       #                                   context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        if file_type == 'birthdate field data is':
            data_files = [create_data_set('Diagnostic_Math_Missing_Fields/Birthdate_Missing_Field', context.gpg_key,
                                          context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif 'a Diagnostic ELA Item file with missing' in file_type:
        file_type = file_type.replace(
            'a Diagnostic ELA Item file with missing', '').strip()
        if file_type == 'student guid field data is':
            data_files = [create_data_set('Diagnostic_ELA_Item_Missing_Fields/Student_Guid_Missing_Field',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        if file_type == 'item guid field data is':
            data_files = [create_data_set('Diagnostic_ELA_Item_Missing_Fields/Item_Guid_Missing_Field', context.gpg_key,
                                          context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif 'a Diagnostic Math Item file with missing' in file_type:
        file_type = file_type.replace(
            'a Diagnostic Math Item file with missing', '').strip()
        if file_type == 'student guid field data is':
            data_files = [create_data_set('Diagnostic_Math_Item_Missing_Fields/Student_Guid_Missing_Field',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        if file_type == 'item guid field data is':
            data_files = [create_data_set('Diagnostic_Math_Item_Missing_Fields/Item_Guid_Missing_Field',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif 'an ELA RIF CSV file with missing' in file_type:
        file_type = file_type.replace(
            'an ELA RIF CSV file with missing', '').strip()
        if file_type == 'SingleParentItemScore field data is':
            data_files = [create_data_set('ELA_RIF_Missing_Fields/SingleParentItemScore_Missing_Field', context.gpg_key,
                                          context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        if file_type == 'ItemMaxPoints field data is':
            data_files = [create_data_set('ELA_RIF_Missing_Fields/ItemMaxPoints_Missing_Field', context.gpg_key,
                                          context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        if file_type == 'StateStudentIdentifier field data is':
            data_files = [create_data_set('ELA_RIF_Missing_Fields/StateStudentIdentifier_Missing_Field',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif 'an Math RIF CSV file with missing' in file_type:
        file_type = file_type.replace(
            'an Math RIF CSV file with missing', '').strip()
        if file_type == 'SingleParentItemScore field data is':
            data_files = [create_data_set('Math_RIF_Missing_Fields/SingleParentItemScore_Missing_Field',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        if file_type == 'ItemMaxPoints field data is':
            data_files = [create_data_set('Math_RIF_Missing_Fields/ItemMaxPoints_Missing_Field', context.gpg_key,
                                          context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
        if file_type == 'StateStudentIdentifier field data is':
            data_files = [create_data_set('Math_RIF_Missing_Fields/StateStudentIdentifier_Missing_Field',
                                          context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]

    elif file_type == 'a valid Item ELA Reading Comprehension file is':
        data_files = [create_data_set('ELA_Reading_Comp_Item', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid Item ELA Decoding file is':
        data_files = [create_data_set('ELA_Decoding_Item', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid Item ELA Vocabulary file is':
        data_files = [create_data_set('ELA_Vocab_Item', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid Item Math Comprehension file is':
        data_files = [create_data_set('Math_Comp_Item', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid Item Math Progression file is':
        data_files = [create_data_set('Math_Progression_Item', context.gpg_key,
                                      context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid Item Math Fluency file is':
        data_files = [create_data_set('Math_Fluency_Item', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid ELA Vocabulary file is':
        data_files = [create_data_set('ELA_Vocabulary', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid ELA Decoding file is':
        data_files = [create_data_set(
            'ELA_Decoding', context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid ELA Comprehension file is':
        data_files = [create_data_set('ELA_Comp', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid ELA Reading Fluency file is':
        data_files = [create_data_set('ELA_Reading_Fluency', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid ELA Writing file is':
        data_files = [create_data_set(
            'ELA_Writing', context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid Math Locator file is':
        data_files = [create_data_set(
            'Math_Locator', context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid Math Fluency file is':
        data_files = [create_data_set(
            'Math_Fluency', context.gpg_key, context.gpg_recipient, context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid Math Progression file is':
        data_files = [create_data_set('Math_Progression', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid Math Grade Level file is':
        data_files = [create_data_set('Math_Grade_Level', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid Math Cluster file is':
        data_files = [create_data_set('Math_Cluster', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    elif file_type == 'a valid Reader Motivation Survey file is':
        data_files = [create_data_set('Reader_Motivation', context.gpg_key, context.gpg_recipient,
                                      context.gpg_passphrase, context.gpg_key_location)]
    else:
        raise AssertionError(
            'Invalid (or unsupported) file type to drop into landing zone')

    if hasattr(context, 'num_files'):
        context.num_files += len(data_files)
    else:
        context.num_files = len(data_files)

    if context.environment == 'local':
        context.tenant = context.first_tenant_name
        tenant_dir = '/opt/edware/zones/landing/arrivals/{0}/ca_user/filedrop/'.format(context.first_tenant_name)
        if tenant_lz == "second tenant's landing zone":
            context.tenant = context.second_tenant_name
            tenant_dir = '/opt/edware/zones/landing/arrivals/{0}/ca_user/filedrop/'.format(context.second_tenant_name)
        if not os.path.exists(tenant_dir):
            os.makedirs(tenant_dir)
        for data_file in data_files:
            shutil.copy2(data_file, tenant_dir)
            checksum_file = create_md5_checksum_file(data_file)
            shutil.copy2(checksum_file, tenant_dir)
        driver_path = os.path.abspath(
            os.path.join(
                here, "..", "..", "..", "edudl2", "scripts", "driver.py")
        )
        command = "python {driver_path} --loop-once".format(
            driver_path=driver_path)
        subprocess.call(command, shell=True)
    elif context.environment == 'AWS':
        context.tenant = context.first_tenant_name
        if tenant_lz == "second tenant's landing zone":
            context.tenant = context.second_tenant_name
            key_file = os.path.join(
                os.path.expanduser('~'), '.ssh', context.udl_lz_key_second_tenant)
            username = context.udl_lz_user_second_tenant
        elif tenant_lz in ["landing zone", "first tenant's landing zone"]:
            key_file = os.path.join(
                os.path.expanduser('~'), '.ssh', context.udl_lz_key)
            username = context.udl_lz_user
        else:
            raise AssertionError('Invalid or unsupported tenant')

        host = context.udl_lz_server
        port = 22

        private_key = paramiko.RSAKey.from_private_key_file(key_file)

        transport = paramiko.Transport((host, port))
        transport.connect(username=username, pkey=private_key)

        sftp = paramiko.SFTPClient.from_transport(transport)

        context.dropped_file = []

        for data_file in data_files:
            remote_path = '/file_drop/' + os.path.basename(data_file)
            sftp.put(data_file, remote_path)
            context.dropped_file.append(remote_path)

        sftp.close()
        transport.close()
    else:
        raise AssertionError('Invalid environment')


@when('enough time is given for {process_type} to process the file')
def step_impl(context, process_type):
    if context.environment == 'local':
        context.tenant = context.first_tenant_name
        wait_period = 25
    else:
        context.tenant = context.first_tenant_name
        wait_period = 120
    if process_type == 'UDL':
        dest_tenant = context.tenant
    elif process_type == 'CRDS':
        dest_tenant = context.tenant + '_crds'
        # for tenant = 'cat_crds' and dest_tenant = 'crds'
        crds_dest_tenant = 'crds'
    edware_stats_schema = context.edware_stats_schema
    edware_stats_url = get_edware_stats_url(context)
    engine = create_engine(edware_stats_url)

    metadata = MetaData(bind=engine, schema=edware_stats_schema)
    metadata.reflect(bind=engine, schema=edware_stats_schema)
    file_received_message = ('udl.received',)
    with engine.connect() as conn:
        udl_stats_table = Table('udl_stats', metadata)
        count_query = select([func.count()]).select_from(
            udl_stats_table).where(udl_stats_table.c.dest_tenant == dest_tenant)
        query = select([udl_stats_table.c.load_status]).where(
            udl_stats_table.c.dest_tenant == dest_tenant)
        timer = 0
        record_count = conn.execute(count_query).fetchall()[0][0]
        result = conn.execute(query).fetchall()
        while timer < wait_period and (record_count < context.num_files or file_received_message in result):
            sleep(1)
            timer += 1
            record_count = conn.execute(count_query).fetchall()[0][0]
            result = conn.execute(query).fetchall()


# @when('enough time is given for {process_type} to process the file for {tenant_name} tenant')
# def step_impl(context, process_type, tenant_name):
#     if context.environment == 'local':
#         wait_period = 15
#     else:
#         wait_period = 90
#     if process_type == 'UDL':
#         dest_tenant = tenant_name
#     elif process_type == 'CRDS':
#         dest_tenant = tenant_name + '_crds'
# for tenant = 'cat_crds' and dest_tenant = 'crds'
#         crds_dest_tenant = 'crds'
#     edware_stats_schema = context.edware_stats_schema
#     edware_stats_url = 'postgresql://' + context.loader_db_user + ':' + \
#         context.loader_db_password + '@' + context.loader_db_server + \
#         ':5432/' + context.edware_stats_db
#     engine = create_engine(edware_stats_url)
#
#     metadata = MetaData(bind=engine, schema=edware_stats_schema)
#     metadata.reflect(bind=engine, schema=edware_stats_schema)
#     file_received_message = ('udl.received',)
#     with engine.connect() as conn:
#         udl_stats_table = Table('udl_stats', metadata)
#         count_query = select([func.count()]).select_from(
#             udl_stats_table).where(udl_stats_table.c.dest_tenant == dest_tenant)
#         query = select([udl_stats_table.c.load_status]).where(
#             udl_stats_table.c.dest_tenant == dest_tenant)
#         timer = 0
#         record_count = conn.execute(count_query).fetchall()[0][0]
#         result = conn.execute(query).fetchall()
#         while timer < wait_period and (record_count < context.num_files or file_received_message in result):
#             sleep(1)
#             timer += 1
#             record_count = conn.execute(count_query).fetchall()[0][0]
#             result = conn.execute(query).fetchall()


@when('enough time is given for {process_type} to process the file for {tenant_name} tenant')
def step_impl(context, process_type, tenant_name):
    if context.environment == 'local':
        wait_period = 100
    else:
        wait_period = 90
    if process_type == 'UDL':
        dest_tenant = tenant_name
    elif process_type == 'CRDS':
        dest_tenant = tenant_name + '_crds'
        # for tenant = 'cat_crds' and dest_tenant = 'crds'
        crds_dest_tenant = 'crds'
    edware_stats_schema = context.edware_stats_schema
    edware_stats_url = get_edware_stats_url(context)
    engine = create_engine(edware_stats_url)

    metadata = MetaData(bind=engine, schema=edware_stats_schema)
    metadata.reflect(bind=engine, schema=edware_stats_schema)
    file_received_message = ('udl.received',)
    with engine.connect() as conn:
        udl_stats_table = Table('udl_stats', metadata)
        count_query = select([func.count()]).select_from(
            udl_stats_table).where(udl_stats_table.c.dest_tenant == dest_tenant)
        query = select([udl_stats_table.c.load_status]).where(
            udl_stats_table.c.dest_tenant == dest_tenant)
        timer = 0
        record_count = conn.execute(count_query).fetchall()[0][0]
        result = conn.execute(query).fetchall()
        while timer < wait_period and (record_count < context.num_files or file_received_message in result):
            sleep(1)
            timer += 1
            record_count = conn.execute(count_query).fetchall()[0][0]
            result = conn.execute(query).fetchall()


@when('a valid {data_set_vars} file with {num_records} records is generated using the data generator')
def step_impl(context, data_set_vars, num_records):
    here = os.path.dirname(__file__)

    generate_path = os.path.abspath(
        os.path.join(here, "..", "..", "..", "data_gen",
                     "parcc", "generate_package_encrypt.py")
    )

    component_test_path = os.path.abspath(
        os.path.join(here, '..', 'data')
    )
    output_path = os.path.join(component_test_path, 'data_sets')
    key_path = os.path.join(
        component_test_path, 'keys', context.gpg_key_location)
    timestamp = datetime.now().strftime('%Y%m%d%H%M%S')

    data_set_vars = data_set_vars.split(' ')
    data_set_category = data_set_vars[0]
    data_set_type = data_set_vars[1]

    try:
        data_set_subject = data_set_vars[2]
    except IndexError:
        data_set_subject = 'math'

    if data_set_category == 'summative' and data_set_type in ['psychometric', 'item']:
        file_name = 'Generated_{0}_{1}'.format(
            data_set_category, data_set_type, timestamp)
    else:
        file_name = 'Generated_{0}_{1}_{2}'.format(
            data_set_category, data_set_type, data_set_subject, timestamp)

    command = "python {generate_path} -l {key_path} -r {recipient} -p '{passphrase}' -k {key} -d '{file_dest}' -n '{file_name}' -u {num_records} -c '{data_set_category}' -t '{data_set_type}' -s '{data_set_subject}'".format(
        generate_path=generate_path,
        key_path=key_path,
        recipient=context.gpg_recipient,
        passphrase=context.gpg_passphrase,
        key=context.gpg_key,
        file_dest=os.path.abspath(output_path),
        file_name=file_name,
        num_records=num_records,
        data_set_category=data_set_category,
        data_set_type=data_set_type,
        data_set_subject=data_set_subject
    )

    print(command)
    subprocess.call(command, shell=True)
    context.generated_gpg = os.path.join(
        output_path, file_name + '.tar.gz.gpg')


@then('the UDL stats table should have data for {process_type}')
def step_impl(context, process_type):
    context.tenant = context.first_tenant_name
    if process_type == 'UDL':
        dest_tenant = context.tenant
    elif process_type == 'CRDS':
        dest_tenant = context.tenant + '_crds'
    edware_stats_schema = context.edware_stats_schema
    edware_stats_url = get_edware_stats_url(context)
    engine = create_engine(edware_stats_url)

    metadata = MetaData(bind=engine, schema=edware_stats_schema)
    metadata.reflect(bind=engine, schema=edware_stats_schema)
    with engine.connect() as conn:
        udl_stats_table = Table('udl_stats', metadata)
        query = select([func.count()]).select_from(udl_stats_table).where(
            udl_stats_table.c.dest_tenant == dest_tenant)
        record_count = conn.execute(query).fetchall()[0][0]
        assert record_count == context.num_files, 'Expected: {expected} records.\nActual: {actual}'.format(
            expected=context.num_files, actual=record_count)


@then('the stats table should have data for {count} records')
def step_impl(context, count):
    edware_stats_schema = context.edware_stats_schema
    edware_stats_url = get_edware_stats_url(context)
    engine = create_engine(edware_stats_url)

    metadata = MetaData(bind=engine, schema=edware_stats_schema)
    metadata.reflect(bind=engine, schema=edware_stats_schema)
    with engine.connect() as conn:
        udl_stats_table = Table('udl_stats', metadata)
        query = select([func.count()]).select_from(udl_stats_table).where(
            udl_stats_table.c.dest_tenant.notilike("%crds%"))
        record_count = conn.execute(query).fetchall()[0][0]
        assert record_count == int(count), 'Expected: {expected} records.\nFound: {actual} records'.format(
            expected=count, actual=record_count)


@then('the UDL stats table should not have data')
def step_impl(context):
    edware_stats_schema = context.edware_stats_schema
    edware_stats_url = get_edware_stats_url(context)
    engine = create_engine(edware_stats_url)

    metadata = MetaData(bind=engine, schema=edware_stats_schema)
    metadata.reflect(bind=engine, schema=edware_stats_schema)
    with engine.connect() as conn:
        udl_stats_table = Table('udl_stats', metadata)
        query = select([func.count()]).select_from(udl_stats_table)
        record_count = conn.execute(query).fetchall()[0][0]
        assert record_count == 0, 'Unexpected number of records found.\nExpected: 0\nActual: {actual}'.format(
            actual=record_count)


@then('the file is still in the landing zone')
def step_impl(context):
    key_file = os.path.join(
        os.path.expanduser('~'), '.ssh', context.udl_lz_key)

    host = context.udl_lz_server
    port = 22
    username = context.udl_lz_user

    private_key = paramiko.RSAKey.from_private_key_file(key_file)

    transport = paramiko.Transport((host, port))
    transport.connect(username=username, pkey=private_key)

    sftp = paramiko.SFTPClient.from_transport(transport)

    # This would raise an error if the file didn't exist
    sftp.stat(context.dropped_file[0])

    sftp.remove(context.dropped_file[0])

    sftp.close()
    transport.close()


@then('the stats table should have the {process_type} record marked as UDL {status}')
def step_impl(context, status, process_type):
    context.tenant = context.first_tenant_name
    if status == 'succeeded':
        message = 'udl.ingested'
    elif status == 'failed':
        message = 'udl.failed'
    else:
        raise AssertionError('Invalid status to check')
    if process_type == 'UDL':
        dest_tenant = context.tenant
    elif process_type == 'CRDS':
        dest_tenant = context.tenant + '_crds'
    edware_stats_schema = context.edware_stats_schema
    edware_stats_url = get_edware_stats_url(context)
    engine = create_engine(edware_stats_url)

    metadata = MetaData(bind=engine, schema=edware_stats_schema)
    metadata.reflect(bind=engine, schema=edware_stats_schema)
    with engine.connect() as conn:
        udl_stats_table = Table('udl_stats', metadata)
        query = select([udl_stats_table.c.load_status]).where(
            udl_stats_table.c.dest_tenant == dest_tenant)
        results = conn.execute(query).fetchall()
        for result in results:
            assert message == result[0], 'Unexpected status.\nExpected: {expected}\nActual: {actual}'.format(
                expected=message, actual=result[0])


@then('the stats table for {tenant_name} tenant should have the {process_type} record marked as UDL {status}')
def step_impl(context, status, process_type, tenant_name):
    context.tenant = tenant_name
    if status == 'succeeded':
        message = 'udl.ingested'
    elif status == 'failed':
        message = 'udl.failed'
    else:
        raise AssertionError('Invalid status to check')
    if process_type == 'UDL':
        dest_tenant = context.tenant
    elif process_type == 'CRDS':
        dest_tenant = context.tenant + '_crds'
    edware_stats_schema = context.edware_stats_schema
    edware_stats_url = get_edware_stats_url(context)
    engine = create_engine(edware_stats_url)

    metadata = MetaData(bind=engine, schema=edware_stats_schema)
    metadata.reflect(bind=engine, schema=edware_stats_schema)
    with engine.connect() as conn:
        udl_stats_table = Table('udl_stats', metadata)
        query = select([udl_stats_table.c.load_status]).where(
            udl_stats_table.c.dest_tenant == dest_tenant)
        results = conn.execute(query).fetchall()
        for result in results:
            assert message == result[0], 'Unexpected status.\nExpected: {expected}\nActual: {actual}'.format(
                expected=message, actual=result[0])


@then('the batch table indicates that UDL failed on the {failure} step')
def step_impl(context, failure):
    error_code_dict = {'csv_structure': '3008', 'csv_column': '3006', 'csv_field_data': '3019', 'json': '3013'}
    if failure == 'file decryption':
        phase = 'udl2.W_file_decrypter.task'
        error_desc = None
    elif failure == 'file unpacking':
        phase = 'udl2.W_file_expander.task'
        error_desc = None
    elif failure == 'load type':
        phase = 'udl2.W_get_values_from_json.task'
        error_desc = None
    elif failure == 'csv schema validation':
        phase = 'udl2.W_data_translator.task'
        error_desc = None
    elif failure == 'csv structure validation':
        phase = 'udl2.W_simple_file_validator.task'
        error_desc = error_code_dict['csv_structure']
    elif failure == 'csv column validation':
        phase = 'udl2.W_data_validator.task'
        error_desc = error_code_dict['csv_column']
    elif failure == 'csv field data validation':
        phase = 'udl2.W_data_validator.task'
        error_desc = error_code_dict['csv_field_data']
    elif failure == 'json validation':
        phase = 'udl2.W_data_validator.task'
        error_desc = error_code_dict['json']
    elif failure == 'csv schema validation':
        phase = 'udl2.W_data_translator.task'
        error_desc = None
    else:
        raise AssertionError('Invalid failure step')

    failure_message = [('FAILURE',)]

    udl_schema = context.udl_schema
    udl_db_url = get_formatted_postgres_url(context.loader_db_user, context.loader_db_password,
                                            context.loader_db_server, 5432, context.loader_db)
    engine = create_engine(udl_db_url)

    metadata = MetaData(bind=engine, schema=udl_schema)
    metadata.reflect(bind=engine, schema=udl_schema)
    with engine.connect() as conn:
        batch_table = Table('udl_batch', metadata)
        if error_desc and error_desc in error_code_dict.values():
            batch_table_status = select([batch_table.c.udl_phase_step_status]).where(
                and_(batch_table.c.error_desc.contains(error_desc), batch_table.c.udl_phase == phase))
        else:
            batch_table_status = select([batch_table.c.udl_phase_step_status]).where(
                batch_table.c.udl_phase == phase)
        result = conn.execute(batch_table_status).fetchall()
        assert result == failure_message, "Expected: {0}. \n Actual: {1}".format(
            failure_message, result)


@then('there should be {count} records in the {tenant} UDL table for {table_type}')
def step_impl(context, count, tenant, table_type):
    table_map = {
        'ELA summative results': 'int_ela_sum',
        'math summative results': 'int_math_sum',
        'test score metadata': 'int_sum_test_score_m',
        'test levels metadata': 'int_sum_test_level_m',
        'file storage': 'int_file_store',
        'assessment admin': 'int_admin',
        'assessment metadata': 'int_asmt',
        'assessment levels': 'int_asmt_lvl',
        'assessment forms': 'int_form',
        'form groups': 'int_form_group',
        'form items': 'int_item',
        'student item results': 'int_student_item_score',
        'psychometric data': 'int_item_p_data',
        'MYA math': 'int_math_mya',
        'MYA ELA': 'int_ela_mya',
        'MYA math item': 'int_item_math_mya',
        'MYA ELA item': 'int_item_ela_mya',
        'MYA items': 'int_item_mya_meta',
        'S&L results': 'int_ela_snl',
        'S&L specific assessment metadata': 'int_snl_asmt_m',
        'S&L assessment mode': 'int_snl_asmt_mode_m',
        'S&L task metadata': 'int_snl_task_m',
        'S&L student tasks scores': 'int_student_task_snl',
        'Item ELA vocabulary': 'int_item_ela_vocab',
        'Item ELA reading comprehension': 'int_item_ela_read_comp',
        'Item ELA decoding': 'int_item_ela_decod',
        'Item Math comprehension': 'int_item_math_comp',
        'Item Math progression': 'int_item_math_progress',
        'Item Math fluency': 'int_item_math_flu',
        'ELA vocabulary': 'int_ela_vocab',
        'ELA decoding': 'int_ela_decod',
        'ELA comprehension': 'int_ela_comp',
        'ELA reading fluency': 'int_ela_read_flu',
        'ELA writing': 'int_ela_writing',
        'Math locator': 'int_math_locator',
        'Math fluency': 'int_math_flu',
        'Math progression': 'int_math_progress',
        'Math grade level': 'int_math_grade_level',
        'Math cluster': 'int_math_cluster',
        'diagnostic assessment metadata': 'int_dgnst_asmt_m',
        'diagnostic assessment level': 'int_dgnst_asmt_lvl_m',
        'diagnostic ela item metadata': 'int_dgnst_ela_item_m',
        'diagnostic math item metadata': 'int_dgnst_math_item_m',
        'diagnostic Math fluency skills metadata': 'int_asmt_skill_m',
        'Reader Motivation Survey': 'int_reader_motiv',
        'ELA RIF': 'int_ela_sum_item_score',
        'Math RIF': 'int_math_sum_item_score'
    }
    table = table_map[table_type]

    if tenant == 'final' or tenant == "first tenant's final":
        staging_db_user = context.staging_db_user
        staging_db_password = context.staging_db_password
        staging_db = context.staging_db
        tenant = context.first_tenant_name
    elif tenant == "second tenant's final":
        staging_db_user = context.staging_db_user_second_tenant
        staging_db_password = context.staging_db_password_second_tenant
        staging_db = context.staging_db_second_tenant
        tenant = context.second_tenant_name
    else:
        raise AssertionError('Invalid or unsupported tenant')

    staging_db_server = context.staging_rds_server
    staging_port = context.staging_rds_port

    context.int_table = table

    context.staging_server = staging_db_server
    context.staging_user = staging_db_user
    context.staging_password = staging_db_password
    context.staging_database = staging_db
    context.staging_port = staging_port

    edware_stats_schema = context.edware_stats_schema
    edware_stats_url = get_edware_stats_url(context)
    stats_engine = create_engine(edware_stats_url)

    stats_metadata = MetaData(bind=stats_engine, schema=edware_stats_schema)
    stats_metadata.reflect(bind=stats_engine, schema=edware_stats_schema)
    with stats_engine.connect() as conn:
        udl_stats_table = Table('udl_stats', stats_metadata)
        query = select([udl_stats_table.c.batch_guid]).where(
            udl_stats_table.c.tenant == tenant)
        results = conn.execute(query).fetchall()
        schema = results[0][0]

    context.int_schema = schema

    edware_url = get_formatted_postgres_url(staging_db_user, staging_db_password,
                                            staging_db_server, staging_port, staging_db)

    edware_engine = create_engine(edware_url)
    edware_metadata = MetaData(bind=edware_engine, schema=schema)
    edware_metadata.reflect(bind=edware_engine, schema=schema)

    with edware_engine.connect() as conn:
        table_to_check = Table(table, edware_metadata)
        query = select([func.count()]).select_from(table_to_check)
        record_count = conn.execute(query).fetchall()[0][0]
        assert record_count == int(count), 'Unexpected number of records found.\nExpected: {expected}\nActual: {actual}'.format(
            expected=count, actual=record_count)


@then('that table should have {count} records where {column} column is "{value}"')
def step_impl(context, count, column, value):
    column_map = {
        "the student's first name": 'student_first_name',
        'the year': 'date_taken_year',
        'the assessment subject': 'asmt_subject',
        'the assessment level designation': 'asmt_lvl_design',
        'the form version': 'form_version',
        'the group name': 'group_name',
        'the max points': 'item_max_points',
        "the student's item score": 'student_item_score',
        'the item delivery mode': 'item_delivery_mode',
        'the assessment format': 'asmt_format',
        'the assessment location': 'where_taken_name',
        'the first evidence statement': 'evidence_stmt1',
        'the task description': 'task_descript',
        "the student's overall task score": 'total_task_score',
        "the assessment mode": 'asmt_mode',
        'the content standard': 'content_standard',
        'the skill name': 'skill_name',
        'the assessment ID': 'asmt_guid',
        'the parcc student id': 'student_parcc_id',
        'the subject': 'asmt_subject',
        'the test code': 'test_code',
        'the cutpoint label': 'cutpoint_label',
        'the third psychometric evidence statement': 'evidence_statement3',
        'the third psychometric standard': 'standard3'
    }
    column_name = column_map[column]
    edware_url = get_formatted_postgres_url(context.staging_user, context.staging_password, context.staging_server,
                                            context.staging_port, context.staging_database)
    edware_engine = create_engine(edware_url)
    edware_metadata = MetaData(bind=edware_engine, schema=context.int_schema)
    edware_metadata.reflect(bind=edware_engine, schema=context.int_schema)

    with edware_engine.connect() as conn:
        table_to_check = Table(context.int_table, edware_metadata)
        query = select([func.count()]).select_from(table_to_check).where(
            getattr(table_to_check.c, column_name) == value)
        record_count = conn.execute(query).fetchall()[0][0]
        print(query)
        assert record_count == int(count), 'Unexpected number of records found.\nExpected: {expected}\nActual: {actual}'.format(
            expected=count, actual=record_count)


@then('that table should have the correct report suppression flags set')
def step_impl(context):
    edware_url = get_formatted_postgres_url(context.staging_user, context.staging_password, context.staging_server,
                                            context.staging_port, context.staging_database)
    edware_engine = create_engine(edware_url)
    edware_metadata = MetaData(bind=edware_engine, schema=context.int_schema)
    edware_metadata.reflect(bind=edware_engine, schema=context.int_schema)

    for row in context.table:
        with edware_engine.connect() as conn:
            table_to_check = Table(context.int_table, edware_metadata)
            where_condition = and_(
                table_to_check.c.record_type == row['record_type'])
            if row['record_type'] in ['02', '03']:
                where_condition = and_(
                    where_condition, table_to_check.c.is_roster_reported == row['in_roster'])
            elif row['summative_flag'] == 'N':
                where_condition = and_(
                    where_condition, table_to_check.c.is_reported_summative == row['summative_flag'])
            elif int(row['suppression_code']) > 10 or int(row['suppression_code']) < 1:
                where_condition = and_(
                    where_condition, table_to_check.c.report_suppression_code == row['suppression_code'])
            else:
                where_condition = and_(where_condition, table_to_check.c.report_suppression_code == row['suppression_code'],
                                       table_to_check.c.report_suppression_action == row[
                                           'suppression_action'],
                                       table_to_check.c.is_reported_summative == row['summative_flag'])

            query = select([table_to_check.c.include_in_parcc, table_to_check.c.include_in_state, table_to_check.c.include_in_district, table_to_check.c.include_in_school, table_to_check.c.include_in_roster, table_to_check.c.include_in_isr],
                           from_obj=table_to_check).\
                where(where_condition)

            results = conn.execute(query)

        all_results = results.fetchall()
        if len(all_results) > 0:
            for result in all_results:
                assert result['include_in_parcc'] == ast.literal_eval(
                    row['parcc_flag']), "PARCC check failed for test seq " + row['seq']
                assert result['include_in_state'] == ast.literal_eval(
                    row['state_flag']), "State check failed for test seq " + row['seq']
                assert result['include_in_district'] == ast.literal_eval(
                    row['district_flag']), "Distrct check failed for test seq " + row['seq']
                assert result['include_in_school'] == ast.literal_eval(
                    row['school_flag']), "School check failed for test seq " + row['seq']
                assert result['include_in_roster'] == row[
                    'roster_flag'], "Roster check failed for test seq " + row['seq']
                assert result['include_in_isr'] == ast.literal_eval(
                    row['isr_flag']), "ISR check failed for test seq " + row['seq']
        else:
            raise AssertionError(
                "No results found for input for seq" + row['seq'])


@then("{file_type} backup files are created in secure location for {tenant} tenant")
def step_impl(context, file_type, tenant):
    data_files = []
    if file_type == 'summative SDS':
        data_files = ['SDS_ELA_Summative_3',
                      'SDS_ELA_Summative_9',
                      'SDS_ELA_Summative_10',
                      'SDS_ELA_Summative_11',
                      'SDS_Math_Summative_03',
                      'SDS_Math_Summative_05',
                      'SDS_Math_Summative_08',
                      'SDS_Math_Summative_Alg1',
                      'SDS_Math_Summative_Alg2',
                      'SDS_Math_Summative_Geom',
                      'SDS_Math_Summative_IMath1',
                      'SDS_Math_Summative_IMath2',
                      'SDS_Math_Summative_IMath3']
    elif file_type == 'summative SDS item':
        data_files = ['SDS_Math_Summative_Item',
                      'SDS_ELA_Summative_Item']
    else:
        context.tc.fail('File type %s not supported' % file_type)

    formats = ('.tar.gz.gpg', '.tar.gz.gpg.done', '.zip.gpg', 'zip.gpg.done')
    tenant_name = None
    if tenant == 'first':
        tenant_name = 'cat'
    elif tenant == 'second':
        tenant_name = 'dog'
    else:
        context.tc.fail('could not find %s tenant' % tenant)
    if BucketHelper.location_is_on_s3(context.backup_storage):
        bucket = BucketHelper.get_bucket(context.backup_storage)
        keys = [item.name for item in bucket.list(prefix=tenant_name)]
    else:
        storage_directory = os.path.join(context.backup_storage, tenant_name)
        keys = []
        for root, directory, files in os.walk(storage_directory):
            keys.extend(files)
    context.tc.assertTrue(keys, 'storage should not be empty')
    context.tc.assertTrue(all(any(key.endswith(name + fileformat) for fileformat in formats for key in keys)
                              for name in data_files),
                          'All files should be on storage')


@given("clear backup storage for {tenant} tenant")
def step_impl(context, tenant):
    tenant_name = None
    if tenant == 'first':
        tenant_name = 'cat'
    elif tenant == 'second':
        tenant_name = 'dog'
    else:
        context.tc.fail('could not find %s tenant' % tenant)

    bucket = BucketHelper.get_bucket(context.backup_storage)
    keys = [item.name for item in bucket.list(prefix=tenant_name)]
    bucket.delete_keys(keys)


@then("no backup files for {tenant} tenant in storage")
def step_impl(context, tenant):
    tenant_name = None
    if tenant == 'first':
        tenant_name = 'cat'
    elif tenant == 'second':
        tenant_name = 'dog'
    else:
        context.tc.fail('could not find %s tenant' % tenant)

    bucket = BucketHelper.get_bucket(context.backup_storage)
    keys = [item.name for item in bucket.list(prefix=tenant_name)]
    context.tc.assertFalse(keys, 'bucket should be empty')
