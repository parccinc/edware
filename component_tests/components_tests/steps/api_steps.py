from components_tests.api_helper.api_helper import ApiHelper
from behave import given, when, then
from hamcrest import assert_that, equal_to, has_length, has_item, is_not
from pprint import pprint
import os

__author__ = 'smuhit', 'nparoha'
# ## SHARED API TEST STEPS ACROSS ALL THE REPORTS GO IN THIS FILE ###


@given("the API is set up")
def step_impl(context):
    if hasattr(context, 'reporting_elb_address') and context.reporting_elb_address:
        hostname = context.reporting_elb_address
    else:
        hostname = context.reporting_app_server
    context.api = ApiHelper(hostname=hostname,
                            port=context.reporting_app_port,
                            protocol=context.reporting_app_protocol,
                            idp_hostname=context.idp_hostname)


@given('a cookie is set for user "{user_name}"')
def step_impl(context, user_name):
    context.api.set_request_cookie(user_name)


@then('the response code should be {code}')
def step_impl(context, code):
    context.api.check_response_code(int(code))


@then('the response body has the json fields "{fields}"')
def step_impl(context, fields):
    # Validate the fields available in the json
    field_list = fields.split(",")
    response_body = context.api._response.json()
    context.api.check_fields(response_body, field_list)


@then('the response body has no "{field}" field')
def assert_field_not_present(context, field):
    response_body = context.api._response.json()
    context.api.check_field_absent(response_body, field)


@then('the response body has {num_items} items for breadcrumbs on the {report_name} report')
def step_impl(context, num_items, report_name):
    # Validate the number of breadcrumbs for a particular report
    response_body = context.api._response.json()
    context.api.check_fields(response_body["context"], ["items"])
    context.breadcrumb_elements = response_body["context"]["items"]

    assert int(num_items) == len(
        context.breadcrumb_elements), 'Expected number of breadcrumbs not found'
    # Validate the Home bread crumb
    context.api.check_number_list_elements(context.breadcrumb_elements[0], 2)
    context.api.check_fields(context.breadcrumb_elements[0], ['name', 'type'])
    # Validate the remaining bread crumb trail
    for index in range(1, int(num_items)):
        if index == 4:
            # grade breadcrumb also includes a boolean for courses
            context.api.check_number_list_elements(
                context.breadcrumb_elements[index], 4)
            context.api.check_fields(
                context.breadcrumb_elements[index], ['id', 'name', 'type', 'isCourse'])
            continue
        context.api.check_fields(
            context.breadcrumb_elements[index], ['id', 'name', 'type'])


@then('the {number} breadcrumb type is {expected_type} and display name is {expected_name}')
def step_impl(context, number, expected_type, expected_name):
    # Validate the number of breadcrumbs for a particular report
    elements = context.api._response.json()["context"]["items"]
    index = {"first": 0, "second": 1, "third": 2,
             "fourth": 3, "fifth": 4, "sixth": 5}
    check_breadcrumb_elements(
        elements[index[number]], expected_type, expected_name)


@then('the grade breadcrumb isCourse field has value {boolean}')
def step_impl(context, boolean):
    grade_elem = context.api._response.json()["context"]["items"][4]
    expected_value = True if boolean.lower() == "true" else False
    assert grade_elem["isCourse"] == expected_value, "expected {0}, got {1}".format(expected_value, grade_elem["isCourse"])


def check_breadcrumb_elements(json, expected_type, expected_name):
    # Validate the {type: expected_type, name: expected_name} for each
    # breadcrumb in the json
    assert expected_type == json[
        'type'], "{0} is not found".format(expected_type)
    assert expected_name == json[
        'name'], "{0} is not found".format(expected_name)


@then('Item {item_num} should have {num} responses')
def item_analysis_responses(context, item_num, num):
    response_body = context.api._response.json()
    item_idx = int(item_num) - 1
    expected = int(num)
    actual = response_body['columns']['SUMMATIVE'][context.report_subject][item_idx]['item_num_responses']
    assert actual == expected, "expected {0}, got {1} for item number {2}".format(expected, actual, item_num)


@then('the response body has the expected number of fields for a {summ_or_diag} report')
def step_impl(context, summ_or_diag):
    response_body = context.api._response.json()

    if summ_or_diag == "student roster":
        context.api.check_fields(response_body["assessments"], ["SUMMATIVE"])
        assert 2 == len(response_body["assessments"]["SUMMATIVE"]), 'Expected number of subjects not found in assessments: ' + str(len(response_body["assessments"]["SUMMATIVE"]))
    elif summ_or_diag == "diagnostic":
        context.api.check_fields(response_body["assessments"], ["DIAGNOSTIC"])
        assert 1 == len(response_body["assessments"]["DIAGNOSTIC"]), 'Expected number of subjects not found in assessments: ' + str(len(response_body["assessments"]["DIAGNOSTIC"]))
    elif summ_or_diag == "individual student":
        context.api.check_fields(
            response_body["assessments"], ["subject1", "subject2"])
        assert 2 == len(response_body[
                        "assessments"]), 'Expected number of subjects not found in assessments'


@then('the response body has {expected_number} students for a {subject} diagnostic report')
def step_impl(context, expected_number, subject):
    if subject == "math":
        subject_code = "subject1"
    elif subject == "ela":
        subject_code = "subject2"
    else:
        assert False, 'Invalid subject {0}'.format(subject)

    response_body = context.api._response.json()
    assert int(expected_number) == len(response_body["assessments"]["DIAGNOSTIC"][subject_code])


@then('the response body has the following data for a {subject} diagnostic report')
def step_impl(context, subject):
    if subject == "math":
        subject_code = "subject1"
    elif subject == "ela":
        subject_code = "subject2"
    else:
        assert False, 'Invalid subject {0}'.format(subject)
    main_row_names = context.table.headings
    response_body = context.api._response.json()
    context.student_elements = response_body["assessments"]["DIAGNOSTIC"][subject_code]
    for idx, row in enumerate(context.table):
        expected = {k: str(row[k]) for k in main_row_names}
        actual = {k: str(v) for k, v in context.student_elements[idx].items()}
        for key in actual:
            if key == 'clusters':
                # these are more complex and, as such, handled separately
                continue
            expected_value = expected[key] if expected[key] else 'None'
            assert key in main_row_names, "\nExpected: {0}\nActual: {1}".format(main_row_names, key)
            assert actual[key] == expected_value, "\nExpected: {0}\nActual: {1} -- for key {2}".format(expected_value, actual[key], key)


@then('the response body has the following ordered cluster data')
def step_impl(context):
    response = context.api._response.json()
    assessments = response['assessments']['DIAGNOSTIC']['subject1']
    clusters = [record['clusters'] for record in assessments]
    cc = 'code_cluster'
    pc = 'prob_cluster'
    expecteds = [[] for _ in clusters]
    for row in context.table:
        i = int(row['student_number'])
        expecteds[i].append({
            cc: row[cc],
            pc: float(row[pc]),
        })
    assert_with_print(expecteds, clusters)


def assert_with_print(expected, actual, key=''):
    prt = "Expected {exp}, Got {act}"
    if key:
        prt = "For key `{key}`: " + prt
    assert expected == actual, prt.format(exp=expected, act=actual, key=key)


@when('the {report_type} report API is accessed with the following parameters')
def step_impl(context, report_type):
    context.api.set_request_header("content-type", "application/json")
    payload = {}
    for r in context.table:
        key = r['param']
        val = r['value']
        if key == 'subject':
            context.report_subject = val
        if key in payload:
            if type(payload[key]) is not list:
                payload[key] = payload[key].split()
            payload[key].append(val)
        else:
            payload[key] = val
    context.api.set_payload(payload)
    rpt = {
        ('student roster', 'SUMMATIVE'): '/data/student_roster',
        ('comparing populations', 'SUMMATIVE'): '/data/comparing_populations',
        ('individual student', 'SUMMATIVE'): '/data/summative_student_report',
        ('individual student', 'DIAGNOSTIC'): '/data/diagnostic_student_report',
        ('school', 'SUMMATIVE'): '/data/summative_school_report',
        ('school', 'DIAGNOSTIC'): '/data/diagnostic_school_report',
    }
    url = rpt.get((report_type, payload.get('asmtType', 'SUMMATIVE')))
    if url is None:
        assert False, "Invalid report type specified"
    context.api.send_request("POST", url)


@when('a bulk extract triggered with the following parameters')
def step_impl(context):
    context.api.set_request_header("content-type", "application/json")
    payload = {}
    for r in context.table:
        key = r['param']
        val = r['value']
        if key in payload:
            payload[key].append(val)
        else:
            if key == 'year':
                payload[key] = int(val)
            elif key == 'gradeCourse':
                payload[key] = [val]
            else:
                payload[key] = val
    context.api.set_payload(payload)
    url = '/services/extract'
    context.api.send_request("POST", url)


@when('a bulk extract for the {tenant} tenant is triggered with the following parameters')
def step_impl(context, tenant):
    tenant_to_state_code = {
        'first': context.first_tenant_state_code,
        'second': context.second_tenant_state_code
    }
    state_code = tenant_to_state_code[tenant]
    steps = ('When a bulk extract triggered with the following parameters' + os.linesep +
             '  | param     | value |' + os.linesep +
             '  | stateCode | {0}   |'.format(state_code))
    for row in context.table:
        steps += os.linesep + '  | {0} | {1} |'.format(row['param'], row['value'])
    context.execute_steps(steps)


@then('I expect "{num}" entities')
def step_impl(context, num):
    response_body = context.api._response.json()
    assert int(num) == len(response_body['assessments']['SUMMATIVE'].keys())


@then('I expect "{num}" entities in "{subject}"')
def step_impl(context, num, subject):
    verify_entity_count(context, subject, 'assessments', num)


@then('in summary row, I expect "{num}" entities in "{subject}"')
def step_impl(context, num, subject):
    verify_entity_count(context, subject, 'summary', num)


@then('I expect "{num}" entities for the subject "{subject}"')
def step_impl(context, num, subject):
    response_body = context.api._response.json()
    subject_occurence = 0
    for entity in response_body['assessments']['SUMMATIVE']:
        if subject in response_body['assessments']['SUMMATIVE'][entity]:
            subject_occurence += 1
    assert int(num) == subject_occurence


@then('I expect "{count}" {name} for entity "{entity}" in subject "{subject}"')
def step_impl(context, count, name, entity, subject):
    verify_entity_results(context, count, name, entity, subject, 'assessments')


@then('in summary row, I expect "{count}" {name} for entity "{entity}" in subject "{subject}"')
def step_impl(context, count, name, entity, subject):
    verify_entity_results(context, count, name, entity, subject, 'summary')


@then('the entity with guid "{guid}" should have {flag_type} flag set')
def step_impl(context, guid, flag_type):
    if flag_type == "no":
        assert not find_entity(context, False, 'is_less_than_min_cell_size', guid, context.report_subject, 'assessments')
    else:
        flag = flag_type.replace('the ', '')
        verify_entity_results(context, True, flag, guid, context.report_subject, 'assessments')


@then('row {row_num} has the following reading comprehension info')
def step_impl(context, row_num):
    main_row_names = context.table.headings
    expected_row_data = context.table[0]
    response_body = context.api._response.json()
    actual_row_data = response_body["assessments"]["DIAGNOSTIC"]["subject2"][int(row_num) - 1]
    assert_row_data(actual_row_data, expected_row_data, main_row_names)


@then('the {subject} student report has the following {report_type} report info')
def step_impl(context, subject, report_type):
    if subject == 'math':
        subject = 'subject1'
    else:
        subject = 'subject2'
    main_row_names = context.table.headings
    expected_row_data = context.table.rows
    response_body = context.api._response.json()
    actual_row_data = response_body["assessments"]["DIAGNOSTIC"][subject][report_type]
    for i in range(len(expected_row_data)):
        assert_row_data(actual_row_data[i], expected_row_data[i], main_row_names)


def assert_row_data(actual_row_data, expected_values, row_keys):
    '''
    Also asserts on the passage_info dict if present in the response
    '''
    count = 0
    i = 0
    for row_key in row_keys:
        expected_value = expected_values[i]
        if row_key == 'passage_type' or row_key == 'passage_rmm':
            actual_value = actual_row_data['passage_info'][count][row_key]
            # increase count after one set of passage_type and passage_rmm have been checked
            if row_key == 'passage_rmm':
                count += 1
        elif row_key == 'code_cluster' or row_key == 'prob_cluster':
            actual_value = actual_row_data['clusters'][count][row_key]
            # increase count after one set of code_cluster and prob_cluster have been checked
            if row_key == 'prob_cluster':
                count += 1
        else:
            actual_value = actual_row_data[row_key]
        assert str(actual_value) == expected_value, "\nExpected: {0}\nActual: {1}".format(expected_value, actual_value)
        i += 1


def verify_entity_count(context, subject, section, count):
    summary_rows = context.api._response.json()[section]['SUMMATIVE'][subject]
    expected, actual = int(count), len(summary_rows)
    assert expected == actual, "expect %d, actual rows %d" % (expected, actual)


def find_entity(context, value, name, entity, subject, section):
    entities = context.api._response.json()[section]['SUMMATIVE'][subject]
    found = False
    for e in entities:
        if e.get(name) is not None and (e.get('guid') == entity or e.get('id') == entity):
            found = True
            data_type = type(e[name])
            assert e[name] == data_type(value)
            break
    return found


def verify_entity_results(context, value, name, entity, subject, section):
    # Verifies an integer item inside blob of data in our API response
    entities = context.api._response.json()[section]['SUMMATIVE'][subject]
    assert find_entity(context, value, name, entity, subject, section)


@then('response should not have any {field} data')
def step_impl(context, field):
    response_body = context.api._response.json()
    assert field not in response_body, "response body shouldn't contain any %s data" % field


@then('the response body should have growth chart cut points')
def step_impl(context):
    response_body = context.api._response.json()
    cut_points = response_body["legend"]["cut_points"]
    assert_that(cut_points, has_length(5))

    expected_start_points = [1, 101, 201, 301, 401]
    expected_end_points = [100, 200, 300, 400, 500]

    for index, start_point in enumerate(expected_start_points):
        assert_that(start_point, cut_points[index]["start"])

    for index, end_point in enumerate(expected_end_points):
        assert_that(end_point, cut_points[index]["end"])


@then('the {summary_type} level summary data is suppressed for {subject}')
def assert_summary_suppressed(context, summary_type, subject):
    response_body = context.api._response.json()
    summary_data = response_body['summary']['SUMMATIVE'][subject]
    for summary in summary_data:
        if summary['type'] == summary_type:
            assert summary['disabled'], '% summary is not disabled' % summary_type
            assert summary.keys() == {'type', 'disabled'}, '% summary data has extra values' % summary_type
