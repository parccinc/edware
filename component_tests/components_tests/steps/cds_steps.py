__author__ = 'dtoub'

from behave import then
import subprocess
import os
import paramiko
from time import sleep


@then("I migrate the data for {destination} cds in {mode} mode for year {year}")
def step_impl(context, destination, mode, year):
    if context.environment == 'local':
        run_local_cds_migration(mode, year)
    else:
        run_aws_cds_migration(context, destination, mode, year)


def run_local_cds_migration(mode, year):
    here = os.path.dirname(__file__)
    config = os.path.abspath(
        os.path.join(here, '..', '..', '..', 'config', 'development.ini'))
    command = "python -m edmigrate.cds -m {mode} -y {year} -i {config}".format(mode=mode,
                                                                               year=year,
                                                                               config=config)
    subprocess.call(command, shell=True)


def run_aws_cds_migration(context, destination, mode, year):
    command = '''
    source /opt/virtualenv/edmigrate/bin/activate &&
    python -m edmigrate.cds -m {mode} -y {year} -i {config} --withReplication
    '''.format(mode=mode,
               year=year,
               config="/opt/edware/conf/parcc.ini")

    username = context.extract_server_user
    if destination in ['dog tenant', 'the second tenant']:
        host = context.master_db_cds_second_tenant_server
    elif destination in ['cat tenant', 'the first tenant']:
        host = context.master_db_cds_first_tenant_server
    elif destination == 'parcc':
        host = context.master_db_cds_parcc_server
    else:
        raise ValueError("unknow cds destination")
    key_file = os.path.join(
        os.path.expanduser('~'), '.ssh', context.extract_server_key)
    key = paramiko.RSAKey.from_private_key_file(key_file)
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(host, username=username, pkey=key)
    stdin, stdout, stderr = ssh.exec_command(command)
    exit_status = stdout.channel.recv_exit_status()
    ssh.close()
