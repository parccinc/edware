import os
import re
import boto
from boto.s3.bucket import Bucket
from boto.s3.key import Key


class BucketHelper:

    S3_BUCKET_PROTOCOL = 's3://'

    @classmethod
    def get_bucket(cls, location: str, aws_access_key=None, aws_secret_access_key=None) -> Bucket:
        """
        Opens a boto s3 conn and returns a boto s3 bucket
        :param location: The url to the s3 bucket
        :param aws_access_key: aws cli access key
        :param aws_secret_access_key: aws cli secret access key
        :return: s3 bucket
        """
        bucket_name = cls.get_bucketname_from_location(location)
        return cls.get_bucket_by_name(bucket_name, aws_access_key, aws_secret_access_key)

    @classmethod
    def get_bucket_by_name(cls, bucket_name: str, aws_access_key=None, aws_secret_access_key=None) -> Bucket:
        """
        Opens a boto s3 conn and returns a boto s3 bucket
        :param bucket_name: The name of the bucket
        :param aws_access_key: aws cli access key
        :param aws_secret_access_key: aws cli secret access key
        :return: s3 bucket
        """
        conn = boto.connect_s3(aws_access_key, aws_secret_access_key)
        bucket = conn.get_bucket(bucket_name)
        return bucket

    @classmethod
    def get_key(cls, location: str, aws_access_key=None, aws_secret_access_key=None) -> Key:
        """
        Create key from bucket
        :param location: The url to the s3 bucket
        :param aws_access_key: aws cli access key
        :param aws_secret_access_key: aws cli secret access key
        :return: bucket key
        """
        bucket = cls.get_bucket(location)
        return Key(bucket)

    @classmethod
    def location_is_on_s3(cls, location: str) -> bool:
        """
        Check if location is s3 bucket url
        :param location: The url to the s3 bucket
        """
        return location.startswith(cls.S3_BUCKET_PROTOCOL)

    @classmethod
    def get_bucketname_from_location(cls, location: str) -> str:
        """
        We indicate s3 locations by prefixing with "s3://",
        strip it off for the bucket name
        :param location: The url to the s3 bucket
        :return: bucket name
        """
        m = cls.match_s3_location(location)
        return m.group("bucketname")

    @classmethod
    def get_prefix_from_location(cls, location: str) -> str:
        """
        We indicate s3 locations by prefixing with "s3://",
        strip it off for the bucket name
        :param location: The url to the s3 bucket
        :return: bucket prefix
        """
        m = cls.match_s3_location(location)
        return m.group("location_prefix").lstrip("/")

    @classmethod
    def match_s3_location(cls, location: str):
        """
        Returns MatchObject that contains bucket name and prefix
        :param location: The url to the s3 bucket
        :return: compiled regular expression
        """
        pattern = "^%s(?P<bucketname>[^/]+?)(?P<location_prefix>/.*)?$" % cls.S3_BUCKET_PROTOCOL
        m = re.match(pattern, location)
        if m is None:
            raise ValueError("location does not indicate s3, or has additional path added to bucket name")
        return m

    @classmethod
    def put_file_into_bucket(cls, location: str, incoming_file_path: str, outgoing_file_path: str):
        """
        Opens a boto s3 conn and returns a boto s3 bucket
        :param location: The url to the s3 bucket
        :param incoming_file_path: path to file on local storage
        """
        prefix = cls.get_prefix_from_location(location)
        k = BucketHelper.get_key(location)
        k.key = os.path.join(prefix, os.path.basename(outgoing_file_path))
        k.set_contents_from_filename(incoming_file_path, encrypt_key=True)
