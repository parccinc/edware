import re
from hamcrest import assert_that, equal_to, has_length
from components_tests.frontend.support import BasePage, wait_for_condition


VIEW_SELECTORS = {
    'Scores': 'scores',
    'Item Analysis': 'items',
}


SUBJECT_SELECTOR = {
    'ELA': '#subjectSelector li:nth-child(1) > a',
    'ELA/L': '#subjectSelector li:nth-child(1) > a',
    'Mathematics': '#subjectSelector li:nth-child(2) > a',
}


class InfoBar(BasePage):

    def get_title(self):
        return self.by_class('reportTitle').text

    def get_subtitle(self):
        return self.by_class('reportSubtitle').text

    def get_claimed_length_from_subtitle(self):
        subtitle = self.get_subtitle()
        return int(re.sub(r'[^\d]+', '', subtitle))

    def click_view(self, view):
        self.by_class(VIEW_SELECTORS[view]).click()

    def get_compare_bar(self):
        return self.by_class('compare-controls')

    def get_compare_checkboxes(self):
        checkbox_labels = self.get_compare_bar().find_elements_by_tag_name("label")
        return [label.text for label in checkbox_labels]

    def get_disabled_compare_checkboxes(self):
        checkbox_labels = self.get_compare_bar().find_elements_by_class_name("disabled")
        return [label.text for label in checkbox_labels]

    def hover_disabled_parcc_icon(self):
        self.move_to_element(".info.compare-opt-out-message")
        self.wait_for_css('.popover-content', timeout=2)

    def get_alert_popover_message(self, expected_text):
        self.wait_for_css('.popover-content', timeout=2)
        self.wait_for_text('.popover-content', expected_text)
        return self.by_class('popover-content').text

    def get_checked_compare_checkboxes(self):
        options = self.get_compare_bar().find_elements_by_tag_name("label")
        checked = []
        for option in options:
            checkbox = option.find_element_by_tag_name('input')
            if checkbox.get_attribute("checked"):
                checked.append(option.text)
        return checked

    def get_diagnostic_views(self):
        views = self.by_css('.selectors .view-selector').find_elements_by_class_name('btn')
        return [view.text for view in views]

    def click_diagnostic_view(self, view):
        view_btns = self.by_css('.selectors .view-selector').find_elements_by_class_name('btn')
        matched_btn = [each for each in view_btns if each.text == view]
        assert_that(matched_btn, has_length(1))  # view name should be unique
        matched_btn[0].click()

    def hover_diagnostic_view(self, view):
        view_btns = self.by_css('.selectors .view-selector').find_elements_by_class_name('btn')

        # matched_btn = [each for each in view_btns if each.text == view]
        # assert_that(matched_btn, has_length(1))  # view name should be unique
        # self.move_to_element(matched_btn)

    def check_all_compare_checkboxes(self):
        options = self.get_compare_bar().find_elements_by_tag_name("label")
        checked = set()
        for option in options:
            checkbox = option.find_element_by_tag_name('input')
            if not checkbox.get_attribute("checked"):
                checkbox.click()
            checked.add(option.text)
        return checked

    def check_compare_checkboxes(self, names):
        for name in names.lower().split(','):
            name = name.lower()
            self.check_compare_checkbox(name.strip())

    def check_compare_checkbox(self, name):
        selector = 'input[name="compare_%s"]' % name
        self.click_element_by_css(selector)

    def test_title(self, expected_title):
        report_title = self.get_title()
        assert_that(report_title, equal_to(expected_title))

    def test_subtitle(self, expected_subtitle):
        report_subtitle = self.get_subtitle()
        assert_that(report_subtitle, equal_to(expected_subtitle))

    def click_download_csv(self):
        self.click_element_by_css('.csv-link')

    def click_download(self):
        self.click_element_by_css('.download-link')

    def click_print_button(self, mode):
        print_button = self.by_css(".print-button")
        print_button.click()
        # self.click_element_by_css('.print-button')
        option_selector = ".print-option.%s" % mode.lower()
        self.click_element_by_css(option_selector)


class GradeCourse(BasePage):

    def get_dropdown_items(self):
        items = self.by_css("#gradeCourseSelector li", multiple=True)
        return [item.text for item in items if item.text]

    def click_dropdown(self):
        self.click_element_by_css('#gradeCourseSelector button')

    def select_dropdown_option(self, course):
        options = self.by_css('#gradeCourseSelector li a', multiple=True)
        selected = [option for option in options if option.text == course]
        selected[0].click()


class ResultSelector(BasePage):

    def click_dropdown(self):
        self.click_element_by_css('#resultSelector button')

    def select_dropdown_option(self, result):
        options = self.by_css('#resultSelector li a', multiple=True)
        selected = [option for option in options if option.text == result]
        selected[0].click()


class SubjectSelector(BasePage):

    def click_subject(self, subject):
        self.click_element_by_css(SUBJECT_SELECTOR[subject])

    def click_dropdown(self):
        self.click_element_by_css('#subjectSelector button')
