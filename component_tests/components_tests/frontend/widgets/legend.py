from components_tests.frontend.support import BasePage


class Legend(BasePage):

    def get_legend_label(self):
        return self.by_css('.legend .legend-label').text

    def get_legend_levels(self):
        return self.by_class('legend').find_elements_by_class_name('level')

    def click_hide_button(self):
        hide_button = '.legend .hide'
        self.click_element_by_css(hide_button)

    def get_legend_container(self):
        return self.by_css('#footer').get_attribute('innerHTML')

    def get_legend_classes(self):
        return self.by_css('#footer .legend').get_attribute('class').split(" ")
