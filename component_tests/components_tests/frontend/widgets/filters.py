from hamcrest import assert_that, equal_to
from components_tests.frontend.support import BasePage


class Filter(BasePage):

    def get_label(self):
        return self.by_class('filters-link').text

    def click_filter_link(self):
        return self.by_class('filters-link').click()

    def test_filter_label(self):
        filter_label = self.get_label()
        assert_that(filter_label, equal_to('FILTERS'))
