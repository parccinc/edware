from hamcrest import assert_that, equal_to, has_length
from components_tests.frontend.support import BasePage


class Breadcrumb(BasePage):

    def get_breadcrumbs(self):
        return self.by_id('breadcrumb').find_elements_by_tag_name('a')

    def test_breadcrumbs(self, expected_breadcrumbs):
        levels = self.get_breadcrumbs()
        assert_that(levels, has_length(len(expected_breadcrumbs)))
        for level, expected in zip(levels, expected_breadcrumbs):
            assert_that(level.text, equal_to(expected['value']))
