from components_tests.frontend.support import BasePage


class YearSelector(BasePage):

    def get_academic_year(self):
        return self.by_id("yearSelector").text

    def click_academic_year(self):
        self.click_element_by_css('#yearSelector button')

    def select_academic_year(self, year):
        options = self.by_css("#yearSelector .dropdown-menu a", multiple=True)
        selected = [option for option in options if option.text == year]
        selected[0].click()
