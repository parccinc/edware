from selenium.webdriver.common.keys import Keys
from components_tests.frontend.support import BasePage
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class Header(BasePage):

    def get_student_name(self):
        return self.by_class('headerStudentName').text

    def get_user_name(self):
        return self.by_id('username').text

    def click_parcc(self):
        self.by_id("logo").click()

    def click_link(self, link):
        links = self.by_class('headerLink', multiple=True)
        target = [l for l in links if link in l.text]
        if not target:
            raise AssertionError("link %s not found" % link)
        target[0].click()

    def close_modal(self):
        close_btn = '.modal-footer .btn-default'
        self.click_element_by_css(close_btn)

    def get_links(self):
        links = self.by_class('glyphicon', multiple=True)
        return [link.text for link in links]

    def get_modal_title(self):
        self.wait_for_class('modal-title')
        return self.by_class('modal-title').text

    def get_dropdown_menu(self):
        options = self.by_css('.modal-body .btn-group .dropdown-menu li', multiple=True)
        return [option.text for option in options if option.text]


class DataExportModal(BasePage):

    def click_academic_year(self):
        selector = '#DataExportModal button[name="academicYearMenu"]'
        self.click_element_by_css(selector)

    def click_export_type(self):
        self.click_element_by_css('.exportType .dropdown-toggle span:first-child')

    def get_data_export_message(self):
        return self.by_css('.modal-body>p').text

    def get_academic_year(self):
        return self.by_css('#DataExportModal button[name="academicYearMenu"]').text

    def verify_close_button_clickable(self):
        WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, '.DataExportContainer .modal-footer .btn-default')))


class HelpModal(BasePage):

    def get_tab(self, tab_name):
        tabs = self.by_css('#HelpMenuModal .nav-tabs>li>a', multiple=True)
        selected = [tab for tab in tabs if tab.text == tab_name]
        return selected[0]

    def click_tab(self, tab_name):
        self.get_tab(tab_name).click()

    def get_links_count_on_tab(self, tab_name):
        selector = '#help_%s a' % tab_name.lower()
        return self.by_css(selector, multiple=True)

    def get_faq_section(self, section_name):
        return self.by_class(section_name).text

    def scroll_down(self):
        go_to_top = self.by_css('.help_anchor.go_to_top:last-child')
        go_to_top.send_keys(Keys.ARROW_DOWN)

    def click_go_to_top(self):
        self.click_element_by_css('.help_anchor.go_to_top:last-child')
