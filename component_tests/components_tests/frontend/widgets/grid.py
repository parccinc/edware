from selenium.common.exceptions import NoSuchElementException, TimeoutException
from hamcrest import assert_that, equal_to, has_length, greater_than
from components_tests.frontend.support import BasePage, wait_for_condition
import re

SUBJECT_TO_CLASS_NAME = {
    'ALGEBRA I': 'Algebra_I',
    'ALGEBRA II': 'Algebra_II',
    'GEOMETRY': 'Geometry',
    'MATHEMATICS I': 'Mathematics_I',
    'MATHEMATICS II': 'Mathematics_II',
    'MATHEMATICS III': 'Mathematics_III',
}


DIAGNOSTIC_TABLE_INDEX = {
    'VOCABULARY': 2,
    'WRITING': 4,
}


DIAGNOSTIC_COLUMN_INDEX = {
    'TEACHER': 1,
    'SCORE': 3,
}


class Grid(BasePage):

    def wait_while_reloading(self):
        """
        Instead of using the flaky, built-in waits from Selenium, roll our own. Check if the body element has the
        'body-loader' class and perform an exponential back-off if it does. We check the body's classes since it is much
        more efficient than having Selenium search the DOM for the loading mask.
        """
        def is_page_loading():
            return 'body-loader' in self.by_tag('body').get_attribute('class').split(' ')
        wait_for_condition(is_page_loading, msg='Grid too long to reload')

    def click_next(self):
        self.by_css('#sticky-header .pull-right').click()

    def click_previous(self):
        self.by_css('#sticky-header .pull-left').click()

    def click_column(self, column):
        headers = self.by_css('#gridHeader .slick-column-name', multiple=True)
        selected = [header for header in headers if header.text == column]
        selected[0].click()

    def click_column_in_table(self, column, table):
        table_index = DIAGNOSTIC_TABLE_INDEX[table]
        sections = self.by_css('.grid-wrapper:nth-child(%d)' % table_index)
        column_index = DIAGNOSTIC_COLUMN_INDEX[column]
        selected = sections.find_element_by_css_selector('.slick-header-column:nth-child(%d)' % column_index)
        selected.click()

    def click_link(self, link):
        self.by_css('.content-grid .grid-canvas').find_element_by_link_text(link).click()

    def click_group_toggle(self, subject):
        selector = '.group-toggle.%s' % SUBJECT_TO_CLASS_NAME[subject]
        self.by_css(selector).click()

    def get_number_of_columns(self):
        num_of_cols = self.by_css('#gridHeader .slick-header-columns').find_elements_by_class_name('slick-column-name')
        return len(num_of_cols)

    def get_headers(self):
        headers = self.by_id('gridHeader').find_elements_by_class_name('slick-column-name')
        return [header.text for header in headers]

    def get_group_headers(self):
        headers = self.by_id('gridTable').find_elements_by_class_name("group-header-title")
        return [header.text for header in headers]

    def get_rows(self):
        rows = self.by_id('gridTable').find_elements_by_class_name('slick-row')
        return rows

    def get_number_of_rows(self):
        # more useful than self.get_rows(self) for the 0 case, which is otherwise slow
        grid_table_inner_html = self.by_id('gridTable').get_attribute('innerHTML')
        actual_rows = re.findall(r'class=\"(.+?slick-row.+?)\"', grid_table_inner_html)
        return len(actual_rows)

    def get_first_grouped_row(self):
        return self.by_css('div.slick-row:first-child .slick-cell')

    def get_grouped_rows(self):
        levels = self.by_id('gridTable').find_elements_by_class_name("summary-compare-row")
        compared = []
        for level in levels:
            level_desc = level.find_element_by_css_selector('div[title]').text
            compared.append(level_desc)
        return compared

    def get_summary_row_names(self):
        summaries = self.by_css('div.grid-canvas').find_elements_by_css_selector('div.slick-cell.l0.r0')
        return [summary.text for summary in summaries]

    def get_pagination_link(self, num_of_columns, direction):
        if direction == 'NEXT':
            return self.by_css('#sticky-header .pull-right').text
        else:  # PREVIOUS
            return self.by_css('#sticky-header .pull-left').text

    def hover_alert_icon(self):
        self.move_to_element(".icon.alert-icon-suppression")
        self.wait_for_css('.popover-content', timeout=2)

    def hover_parcc_summary_info_icon(self):
        self.move_to_element(".info.name")
        self.wait_for_css('.popover-content', timeout=2)

    def hover_parcc_icon(self):
        self.move_to_element(".info.parcc_growth_percent")

    def hover_state_icon(self):
        self.move_to_element(".info.state_growth_percent")

    def hover_column_info_icon(self, col_name):
        name = col_name.lower()
        selector = ".info.{0}".format(name)
        self.move_to_element(selector)

    def get_writing_selector_name(self, col_name):
        WRITING_COL_TO_SELECTOR_MAP = {'RESEARCH': 'score_res', 'LITERARY ANALYSIS': 'score_lit', 'NARRATIVE WRITING': 'score_nar'}
        return WRITING_COL_TO_SELECTOR_MAP.get(col_name)

    def click_first_row_link(self, field_position):
        self.by_css('#gridTable .slick-viewport .slick-row:{0}-child .l0.r0>a'.format(field_position)).click()

    def check_comparison_row_name(self, field_value):
        actual_res = self.by_css('.slick-cell.l0.r0').text.splitlines()[0]
        assert_that(field_value, equal_to(actual_res))

    # This will only work for one grid on the page
    def get_value_at_row_column(self, row_num, col_name):
        column_number = int(col_name)
        rows = self.by_css(".slick-row", True)
        row = rows[int(row_num) - 1]
        cells = row.find_elements_by_css_selector(".slick-cell")
        cells = [cell.text.replace('\n', '') for cell in cells]
        return cells[column_number]

    def get_value_at_x_y(self, x, y):
        # x is row index, y is col index
        fetch_row_from_app = self.by_id("gridTable").find_element_by_class_name(
            "grid-canvas").find_elements_by_class_name("slick-row")
        row_pos = int(y) - 1
        current_row = fetch_row_from_app[row_pos].text.splitlines()
        col_pos = int(x) - 1
        return current_row[col_pos]

    def get_values_at_row(self, row_num):
        selector = '#gridTable .grid-canvas .slick-row:nth-child({0})'.format(row_num)
        cells = self.by_css(selector).find_elements_by_class_name('slick-cell')
        return sum([cell.text.splitlines() for cell in cells], [])

    def num_alert_icons_in_row(self, row_num):
        try:
            selector = "div#gridTable .slick-row:nth-child(%s) .alert-icon-suppression" % row_num
            # TODO: THIS TIMEOUT DOES NOT WORK, IT WAITS SLIGHTLY MORE THAN 10 SECONDS
            self.wait_for_css(selector, timeout=1)
            return self.by_css(selector)
        except TimeoutException:
            return None
        except NoSuchElementException:
            return None

    def get_first_row_of_col_num(self, col_num):
        subscore_text = self.by_css('div.slick-cell.l{0}.r{0}'.format(col_num)).text
        return subscore_text.split('\n')

    def get_alert_popover_message(self, expected_text):
        self.wait_for_text('.popover-content', expected_text)
        return self.by_class('popover-content').text

    def get_tooltip_message(self):
        return self.by_class('popover-content').text

    def get_num_drillable(self, drillable=True):
        if not drillable:
            return len(self.by_css("div.ui-widget-content.slick-row.noDrillDown", True))
        return len(self.by_css('.columnLink', True))

    def get_student_elements(self):
        return self.by_css('.slick-row .slick-cell:nth-child(2)', True)

    def get_group_header_rows(self):
        return self.by_css('.group-header-row', True)

    def get_subtitle_number(self, subdivision_type):
        subtitle = self.by_class('reportSubtitle').text
        return int(subtitle.replace(" {0}".format(subdivision_type), ""))

    def get_grid_by_section_name(self, section_name):
        section_name = section_name.lower().replace(' ', '_')
        section_names = {
            'progression': 'progress',
            'fluency': 'flu',
            'vocabulary': 'vocab',
            'writing': 'writing',
        }
        if section_name in section_names:
            section_name = section_names[section_name]
        return self.by_css('[data-component-grid-container="{0}"]'.format(section_name))

    def get_first_row_of_column_by_name(self, column_name):
        ROW_POSITION = {
            'SCHOOL': 1,
            'DISTRICT': 1,
            'STUDENT': 1,
            'STATE': 1,
            'STUDENTS': 2,
            'OVERALL': 5,
        }
        row_index = ROW_POSITION[column_name]
        selector = '#gridTable .slick-row:first-child .slick-cell:nth-child(%d)' % row_index
        return self.by_css(selector).text

    def hover_header(self):
        self.move_to_element('#header')

    def scroll_down(self):
        script = "window.scrollTo(0, document.body.scrollHeight);"
        self.driver.execute_script(script)

    def hover_on_subclaim(self, subclaim):
        subclaim_position = {
            'Literary Text': 4,
            'Informational Text': 5,
            'Vocabulary': 6,
            'Writing Expression': 8,
            'Knowledge and Convention': 9,
            'Major Content': 3,
            'Additional & Supporting': 4,
            'Expressing Reasoning': 5,
            'Modeling & Application': 6,
        }
        position = subclaim_position[subclaim]
        locator = "#gridHeader .slick-row:first-child .l{0}.r{0}".format(position)
        self.move_to_element(locator)
        self.by_class('popover')

    def get_performance_popover(self, expected):
        for each in expected:
            self.wait_for_text('.popover-content', each, timeout=3)
        return self.by_class('popover-content').text.split()

    def get_student_with_text(self, student_name):
        rows = self.by_id('gridTable').find_elements_by_class_name('slick-row')
        for row in rows:
            element = row.find_element_by_css_selector('.l0.r0')
            if element.text == student_name:
                return element
