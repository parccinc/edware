from hamcrest import assert_that, equal_to, has_length, greater_than, is_not, empty
from components_tests.frontend.support import BasePage, wait_for_condition
from selenium.webdriver.common.action_chains import ActionChains


class GrowthChart(BasePage):

    def get_title(self):
        return self.by_id("growthSummary").find_element_by_class_name(
            "name-value-pairs-title").text

    def get_growth_summary(self):
        return self.by_id("growthSummary")

    def get_summary_bar_info(self):
        # ex: returns {"STUDENTS": "12",
        #              "AVG SCORE": "239",
        #              "DISTRIBUTION": "17%,17%,25%,25%,16%"}
        summary_sections = self.get_growth_summary().find_elements_by_class_name(
            "growthSummaryItem")
        names = [sec.find_element_by_class_name("name").text for sec in summary_sections]
        values = [sec.find_element_by_class_name("value").text for sec in summary_sections[0:2]]
        distribution_percentages = [perc.text for perc in summary_sections[2].find_elements_by_class_name("percentageOnBar")]
        distributions = ','.join(distribution_percentages)
        values.append(distributions)
        return dict(zip(names, values))

    def get_gray_bar(self):
        return self.get_growth_summary().find_elements_by_class_name("grayBar")

    def get_comparison_sources(self):
        comparison_source_div = self.by_class("comparisonSource")
        radios = comparison_source_div.find_elements_by_class_name("radio")
        return {radio.text.lower() for radio in radios}

    def get_current_comparison_source(self):
        comparison = self.by_id("growthChart").find_element_by_class_name("comparisonSource")
        # sanity check the compare text next to radio buttons
        comp_text = comparison.find_element_by_tag_name("span").text
        assert_that("Growth compared to:", equal_to(comp_text))
        return comparison

    def click_radio_button(self, selection):
        comparison = self.get_current_comparison_source()
        data_reactid = comparison.get_attribute('data-reactid')
        mask = "input[type='radio'][data-reactid='{reactid}.1:{index}.$checkboxRadioWrapper.$input']"
        button_index = {
            "PARCC": 0,
            "STATE": 1
        }
        try:
            comparison.find_element_by_css_selector(mask.format(reactid=data_reactid,
                                                                index=button_index[selection])).click()
        except Exception as e:
            raise AssertionError("Invalid growth chart comparison name provided to the step") from e

    def get_number_of_checked_rows(self):
        return len(context.driver.find_element_by_id("gridTable").find_elements_by_class_name("slick-row"))

    def click_checkbox_by_school_name(self, sch_name):
        rows = self.by_id("gridTable").find_elements_by_class_name("slick-row")
        for row in rows:
            if row.find_element_by_class_name("firstColumn").find_element_by_tag_name("a").text == sch_name:
                input_tag = row.find_element_by_tag_name("input")
                assert input_tag.get_attribute("checked"), "{0} row is not checked.".format(sch_name)
                # assumption: action is always = uncheck
                row.find_element_by_tag_name("input").click()
                break

    def get_axis_id(self, axis):
        if axis == "X":
            return "xLabels"
        elif axis == "Y":
            return "yLabels"
        else:
            raise AssertionError("Invalid axis name provided to the step")

    def get_axis_label(self, axis):
        axis_id = self.get_axis_id(axis)
        labels = self.by_id("growthChart").find_element_by_id(axis_id).text
        # remove newlines
        return labels.replace("\n", '')

    def get_axis_cutpoints(self, axis):
        axis_class = axis.lower()
        axis = self.by_id("growthChart").find_element_by_class_name(axis_class)
        return [cutpoint.text for cutpoint in axis.find_elements_by_class_name('tick')]

    def is_line_present(self, line_name):
        origins = self.driver.find_elements_by_class_name("origin")
        for origin in origins:
            text_element = origin.find_element_by_tag_name("text")
            origin_text = text_element.text.lower()
            if line_name.lower() == origin_text:
                return True
        return False

    def is_circle_at_x_position(self, x_position):
        act_circles = self.by_id("growthChart").find_element_by_id("bubbles").find_elements_by_tag_name("circle")
        for each in act_circles:
            if each.get_attribute("cx") == x_position:
                return True
        return False

    def get_number_of_circles(self):
        growth_chart = self.by_id("growthChart").get_attribute('innerHTML')
        return growth_chart.count('<circle')

    def mouseover_grid_at_position(self, position):
        selector = 'div.ui-widget-content:nth-child(%s)' % position
        self.move_to_element(selector)

    def mouseover_bubble_at_position(self, x, y):

        def find_circle(circle, x, y):
            cx = float(circle.get_attribute('cx'))
            cy = float(circle.get_attribute('cy'))
            return int(cx) == int(x) and int(cy) == int(y)

        circles = self.by_tag("circle", multiple=True)
        circle = [c for c in circles if find_circle(c, x, y)]
        assert_that(circle, is_not(empty()), "Bubble not found at position: {0} {1}".format(x, y))
        bubble_to_mouseover = circle[0]
        loc = bubble_to_mouseover.location
        script = "window.scrollTo(%s, %s);" % (loc['x'], loc['y'])
        self.driver.execute_script(script)
        ActionChains(self.driver).move_to_element(bubble_to_mouseover).perform()
        assert_that(self.get_num_active_bubbles(), equal_to(1))

    def get_num_active_bubbles(self):
        return len(self.by_css(".bubble.activeBubble", multiple=True))

    def get_bubbles(self):
        return self.by_class('bubble', multiple=True)

    def get_active_bubble_idx(self, school_name):
        bubbles = self.get_bubbles()
        for i, bubble in enumerate(bubbles):
            if 'activeBubble' in bubble.get_attribute('class'):
                return i, len(bubbles)
        raise AssertionError('no active bubble found')

    def get_popover_title(self, expected=None):
        selector = ".popover-content .name-value-pairs-title"
        if expected:
            self.wait_for_text(selector, expected)
        return self.by_css(selector).text

    def get_popover_subtitle(self):
        return self.by_css(".popover-content .sub-title").text

    def get_popover_content(self):
        names = self.by_css(".popover-content .name-value-pair .name", multiple=True)
        values = self.by_css(".popover-content .name-value-pair .value", multiple=True)
        metrices = {}
        for name, value in zip(names, values):
            metrices[name.text] = value.text
        return metrices

    def get_text_below_chart(self):
        return self.by_id("growthChart").find_element_by_tag_name("p").text

    def get_first_row_col_value(self, col_name):
        if col_name == "SCHOOL":
            position = 1
        elif col_name == "STUDENTS":
            position = 2
        else:
            raise AssertionError("Invalid column name provided to step")
        row = self.driver.find_elements_by_css_selector(
            "div.grid-canvas .slick-row.even div")
        return row[position].text

    def get_header_columns(self):
        return self.by_css("#gridTable .slick-column-name", multiple=True)
