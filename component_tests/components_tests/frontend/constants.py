class Constants:

    FIREFOX = 'firefox'
    PHANTOMJS = 'phantomjs'


class Reports:

    ISR = "/assets/html/studentReport.html"
    STUDENT_ROSTER = "/assets/html/studentRosterReport.html"
    SCHOOL_REPORT = "/assets/html/schoolReport.html"
    CPOP = "/assets/html/comparingPopulationsReport.html"
