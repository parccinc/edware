from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from selenium import webdriver
import uuid
import configparser
from os import path, makedirs
from shutil import rmtree
from components_tests.frontend.support import BasePage


DOWNLOAD_DIR = path.join('/tmp/webdriver/', str(uuid.uuid4()))


def go_to_url(context, webpage):
    context.driver.get(webpage)


def setup_browser(context):
    if _run_headless(context):
        context.driver = webdriver.PhantomJS()
    else:
        profile = _create_firefox_profile()
        if _setup_proxy(context):
            profile.set_preference("network.proxy.type", 1)
            profile.set_preference("network.proxy.http", context.proxy_ip)
            profile.set_preference("network.proxy.http_port", int(context.proxy_port))
            profile.set_preference("general.useragent.override", "whater_useragent")
            profile.update_preferences()
        context.driver = webdriver.Firefox(profile)
    context.driver.set_window_size(1280, 1024)
    context.driver.implicitly_wait(10)


def read_config(context):
    config = configparser.ConfigParser()
    config_file_path = path.abspath(path.join(path.dirname(__file__), '..', '..', 'test.ini'))
    config.read(config_file_path)
    config_values = dict(config.items('test'))
    for key, value in config_values.items():
        setattr(context, key, value)


def _run_headless(context):
    return context.browser == 'phantomjs'


def _create_firefox_profile():
    profile = FirefoxProfile()
    profile.accept_untrusted_certs = True
    profile.set_preference('browser.download.folderList', 2)  # custom location
    profile.set_preference('browser.download.manager.showWhenStarting', False)
    profile.set_preference('browser.download.dir', DOWNLOAD_DIR)
    profile.set_preference('browser.helperApps.neverAsk.saveToDisk', 'text/csv, application/octet-stream')
    return profile


def _setup_proxy(context):
    """
    For contractors purposes, they need it to get access to all services
    to enable add param "PROXY" when run behave test
    Example: behave -D PROXY test_some_frontend_feature.feature
    :param context: behave context object
    :return: bool
    """
    return context.config.userdata.getbool("PROXY")


def close_alert_if_exist(context):
    try:
        alert = context.driver.switch_to.alert
        alert.accept()
    except NoAlertPresentException:
        pass


def close_browser(context):
    context.driver.quit()


def make_download_directory():
    makedirs(DOWNLOAD_DIR, exist_ok=True)


def remove_download_directory():
    rmtree(DOWNLOAD_DIR, ignore_errors=True)


class WindowManager(BasePage):

    def go_to_new_window(self):
        self.context.previous_window = self.context.driver.current_window_handle
        for handle in self.context.driver.window_handles:
            self.context.driver.switch_to_window(handle)

    def close_new_window(self):
        self.context.driver.close()
        previous = self.context.previous_window
        self.context.driver.switch_to_window(previous)
