from hamcrest import assert_that, is_not
from components_tests.frontend.support import BasePage, wait_for_condition


class FileActions(BasePage):
    LINKS_MAP = {'open': 'openButton',
                 'open in a new window': 'openNewButton',
                 'edit': 'editButton',
                 'cut': 'cutbutton',
                 'copy': 'copyButton',
                 'move to trash': 'deleteButton',
                 'rename...': 'renameButton',
                 'add to favorites': 'favoritesButton',
                 'properties...': 'propertiesButton'}
    SECTION_ID = 'fileBrowserButtons'
    MODAL = 'pentaho-dialog'

    def get_title(self):
        return self.by_id(FileActions.SECTION_ID).find_element_by_id('buttonsHeader').text

    def get_items(self):
        return self.by_id(FileActions.SECTION_ID).find_element_by_class_name("body").find_elements_by_tag_name("button")

    def click_item(self, name):
        button_id = FileActions.LINKS_MAP.get(name.lower())
        assert_that(button_id, is_not(None))
        self.by_id(FileActions.SECTION_ID).find_element_by_id(button_id).click()

    def return_opened_report(self):
        return self.by_id('pucContent').find_element_by_class_name('pentaho-tabWidget-selected').text
