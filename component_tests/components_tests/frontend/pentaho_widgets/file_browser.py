from components_tests.frontend.support import BasePage


NAME_ID_MAP = {"folders": "fileBrowserFolders",
               "files": "fileBrowserFiles",
               "recents": "recents-content-panel",
               "favorites": "favorites-content-panel"}

NAME_SELECTOR_MAP = {"folders": "div.title",
                     "files": "div.title",
                     "recents": "a span.pad-left",
                     "favorites": "a span.pad-left"}


class FileBrowser(BasePage):

    def get_browser_with_name(self, box_name):
        return self.by_id(NAME_ID_MAP.get(box_name))

    def get_items(self, browser_type):
        section = self.by_id(NAME_ID_MAP.get(browser_type))
        selector = NAME_SELECTOR_MAP.get(browser_type)
        objects = section.find_elements_by_css_selector(selector)
        return [element.text.lower() for element in objects]

    def click_file(self, file_name, browser_name):
        files_browser = self.by_id(NAME_ID_MAP.get(browser_name))
        selector = NAME_SELECTOR_MAP.get(browser_name)
        files = files_browser.find_elements_by_css_selector(selector)
        for f in files:
            if f.text.lower() == file_name.lower():
                f.click()
                return
        assert False, 'File not found'

    def click_favorite_icon(self, file_name, browser_name):
        files_browser = self.by_id(NAME_ID_MAP.get(browser_name))
        files = files_browser.find_elements_by_tag_name('a')
        for f in files:
            if f.text.lower() == file_name.lower():
                f.find_element_by_css_selector('i.pull-right').click()
                return
        # If you don't find the element and return, then raise an exception
        assert False, str(files)

    # This assumes unique folder names
    # If you want to do this right, use the directory structure and give a full path to a folder
    def click_folder(self, folder_name):
        folders_browser = self.by_id('fileBrowserFolders')
        folders = folders_browser.find_elements_by_css_selector('div.title')
        for folder in folders:
            if folder.text.lower() == folder_name.lower():
                folder.click()
                return
        assert False, 'Folder not found'

    # This assumes unique folder names
    # If you want to do this right, use the directory structure and give a full path to a folder
    def click_arrow(self, folder_name):
        folders_browser = self.by_id('fileBrowserFolders')
        folders = folders_browser.find_elements_by_css_selector('div.element')
        for folder in folders:
            if folder.text.lower() == folder_name.lower():
                folder.find_element_by_class_name('expandCollapse').click()
                return
        assert False, 'Folder not found'

    def click_refresh(self):
        self.wait_for_id('refreshBrowserIcon')
        self.by_id('refreshBrowserIcon').click()
