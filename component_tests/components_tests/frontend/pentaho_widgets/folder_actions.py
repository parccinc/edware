from hamcrest import assert_that, is_not
from components_tests.frontend.support import BasePage, wait_for_condition


class FolderActions(BasePage):
    LINKS_MAP = {'new folder...': 'newFolderButton',
                 'move to trash': 'deleteFolderButton',
                 'rename...': 'renameButton',
                 'properties...': 'propertiesButton'}
    SECTION_ID = 'fileBrowserButtons'
    MODAL = 'pentaho-dialog'

    def get_title(self):
        return self.by_id(FolderActions.SECTION_ID).find_element_by_id('buttonsHeader').text

    def get_items(self):
        return self.by_id(FolderActions.SECTION_ID).find_element_by_class_name("body").find_elements_by_tag_name("button")

    def click_item(self, name):
        button_id = FolderActions.LINKS_MAP.get(name.lower())
        assert_that(button_id, is_not(None))
        self.by_id(FolderActions.SECTION_ID).find_element_by_id(button_id).click()

    def is_modal_present(self):
        def is_modal_loading():
            return FolderActions.MODAL not in self.driver.find_element_by_tag_name('body').get_attribute('innerHTML')
        wait_for_condition(is_modal_loading, msg='Modal took too long to reload')

    def send_text(self, text):
        self.by_class(FolderActions.MODAL).find_element_by_class_name('gwt-TextBox').send_keys(text)

    def click_OK(self):
        self.by_id('okButton').click()

    def click_yes(self):
        self.by_class('footer').find_element_by_class_name('ok').click()

    def is_modal_title_present(self, title):
        def is_modal_loading():
            return title not in self.by_class('bootstrap').get_attribute('innerHTML')
        wait_for_condition(is_modal_loading, msg='Modal took too long to reload')

    def send_text_to_rename(self, text):
        self.by_id('rename-field').send_keys(text)

    def click_yes_to_rename(self):
        self.by_id('dialogRename').find_element_by_class_name('ok').click()
