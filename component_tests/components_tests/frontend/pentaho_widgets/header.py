from components_tests.frontend.support import BasePage


class Header(BasePage):

    def get_student_name(self):
        return self.by_id('pucUserDropDown').text

    def select_view(self, view_name):
        self.by_css('div#mantle-perspective-switcher td.custom-dropdown-arrow').click()
        views = self.by_class('gwt-MenuItem', True)
        for view in views:
            if view.text.lower() == view_name.lower():
                view.click()
                return
        assert False, 'Could not find view'

    def click_link(self, link):
        if link == 'reporting':
            self.by_id('pucReporting').find_element_by_tag_name('a').click()
        elif link == 'user':
            self.by_css('div#pucUserDropDown td.custom-dropdown-arrow').click()

    def log_out(self):
        self.by_id('customDropdownPopupMinor').find_element_by_class_name('gwt-MenuItem').click()
