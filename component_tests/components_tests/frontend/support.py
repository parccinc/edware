from time import sleep
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains


def get_url(context):
    if hasattr(context, 'reporting_elb_address') and context.reporting_elb_address:
        reporting_server_address = "https://" + context.reporting_elb_address
    else:
        hostname = context.reporting_app_server
        port = context.reporting_app_port
        reporting_server_address = "http://{0}:{1}".format(hostname, port)
    return reporting_server_address


def open_report(context, endpoint):
    base_url = get_url(context)
    query_params = "&".join(r['param'] + "=" + r['value'] for r in context.table)
    url = "{base_url}{endpoint}?{query_params}".format(
        base_url=base_url,
        endpoint=endpoint,
        query_params=query_params,
    )
    print(url)
    context.driver.get(url)


def wait_for_condition(condition_func, attempts=6, wait=0.25, msg='Waited too long'):
    '''
    Waits for some condition to return false and retries for some fixed time
    '''
    tries = 0
    loading = condition_func()
    while loading and tries < attempts:  # Max wait time is ~15.75 seconds.
        sleep(wait * (2 ** tries))
        tries += 1
        loading = condition_func()

    if loading:
        raise AssertionError(msg)


class WebDriverHelper:

    def __init__(self, context):
        self.context = context
        self.driver = context.driver

    def by_id(self, element_id):
        return self.driver.find_element_by_id(element_id)

    def by_class(self, element_class, multiple=False):
        if multiple:
            return self.driver.find_elements_by_class_name(element_class)
        return self.driver.find_element_by_class_name(element_class)

    def by_name(self, element_name):
        return self.driver.find_element_by_name(element_name)

    def by_tag(self, element_tag, multiple=False):
        if multiple:
            return self.driver.find_elements_by_tag_name(element_tag)
        return self.driver.find_element_by_tag_name(element_tag)

    def by_css(self, css, multiple=False):
        if multiple:
            return self.driver.find_elements_by_css_selector(css)
        return self.driver.find_element_by_css_selector(css)

    def by_xpath(self, xpath):
        return self.driver.find_element_by_xpath(xpath)

    def by_jquery(self, jquery_selector):
        javascript = "return $(\"{0}\")[0]".format(jquery_selector)
        return self.driver.execute_script(javascript)

    def wait_for_id(self, element_id, timeout=20):
        WebDriverWait(self.driver, timeout).until(
            EC.visibility_of_element_located((By.ID, element_id)))

    def wait_for_class(self, class_name, timeout=20):
        WebDriverWait(self.driver, timeout).until(
            EC.visibility_of_element_located((By.CLASS_NAME, class_name)))

    def wait_for_css(self, css_name, timeout=20, condition=EC.visibility_of_element_located):
        WebDriverWait(self.driver, timeout).until(condition((By.CSS_SELECTOR, css_name)))

    def wait_for_text(self, css_name, text, timeout=20):
        WebDriverWait(self.driver, timeout).until(
            EC.text_to_be_present_in_element((By.CSS_SELECTOR, css_name), text))

    def wait_for_xpath(self, xpath, timeout=20):
        WebDriverWait(self.driver, timeout).until(
            EC.presence_of_element_located((By.XPATH, xpath)))

    def move_to_element(self, selector, engine='css'):
        if engine =='css':
            element = self.by_css(selector)
        elif engine == 'xpath':
            element = self.by_xpath(selector)
        else:
            raise AssertionError("Please provide correct engine for this step")
        ActionChains(self.driver).move_to_element(element).perform()

    def click_element_by_css(self, selector):
        self.wait_for_css(selector, condition=EC.element_to_be_clickable)
        element = self.by_css(selector)
        ActionChains(self.driver).move_to_element(element).click().perform()

    def click_element_by_xpath(self, xpath):
        element = self.by_xpath(xpath)
        ActionChains(self.driver).move_to_element(element).click().perform()

    def navigate_to_iframe(self, frame_ref):
        self.driver.switch_to_frame(frame_ref)

    def switch_out_iframe(self):
        self.driver.switch_to_default_content()


class BasePage(WebDriverHelper):
    pass
