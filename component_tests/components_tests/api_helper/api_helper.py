import requests
import re
import html.parser
import unittest
import json

__author__ = 'smuhit'


class ApiHelper(unittest.TestCase):
    """
    Helper methods for EdApi calls
    """

    def __init__(self, hostname, port, idp_hostname, protocol='http', *args, **kwargs):
        unittest.TestCase.__init__(self, *args, **kwargs)
        self._hostname = hostname
        self._port = port
        self._protocol = protocol
        self._idp_hostname = idp_hostname
        self._response = None
        self._request_header = {}
        self._items_to_check = None

    # This method needed so unittest doesn't actually try to run this class as a test
    def runTest(self):
        pass

    def get_hostname(self):
        return self._hostname

    def get_port(self):
        return self._port

    def get_idp_hostname(self):
        return self._idp_hostname

    def get_protocol(self):
        return self._protocol

    def get_url(self):
        return "{0}://{1}:{2}".format(self.get_protocol(), self.get_hostname(), self.get_port())

    # Makes http requests
    def send_request(self, verb, end_point, base='', use_base=True):
        """
        Makes restful requests as per the verb passed.
        :param verb:  Any one of the given http verbs: OPTIONS, GET, POST, DE:LETE
        :type verb: string
        :param end_point: appends the end point to the url from test.ini file to make the request
        :type end_point: string
        """
        if not base and use_base:
            base = self.get_url()
        verb = verb.upper()
        if verb == "OPTIONS":
            self._response = requests.options(base + end_point, verify=False, **self._request_header)
        elif verb == "GET":
            self._response = requests.get(base + end_point, verify=False, **self._request_header)
        elif verb == "POST":
            self._response = requests.post(base + end_point, verify=False, **self._request_header)
        elif verb == "DELETE":
            self._response = requests.delete(base + end_point, verify=False, **self._request_header)
        elif verb == "PUT":
            self._response = requests.put(base + end_point, verify=False, **self._request_header)
        else:
            print("Error: Entered an invalid request verb: " + verb)

    def send_post(self, url, data):
        """
        POST request for non-json content body
        :param url: http url
        :type url:string
        :param data: data to post in the payload
        :type data: string
        """
        self._response = requests.post(url, data=data, verify=False, **self._request_header)

    # USED
    def set_request_cookie(self, user_id):
        """
        POST request for non-json content body
        :param user_id: userid used to send the request
        :type user_id:string
        """
        # Assumes password is always user_id + 1234
        password = user_id + '1234'

        # Make a request to Access the smarter URL for the purpose of being re directed to the OPEN AM page.
        self.send_request("GET", "/data")
        json_response = self._response.json()
        redirect = json_response['redirect']
        self.send_request("GET", redirect, use_base=False)
        # This should redirect us to IDP page. Extract the response message.
        response = self._response.content.decode("utf-8")

        # Search for regular expressions from the response body
        goto = re.search('name=\\"goto\\" value=\\"(.*)\\"', response).group(1)
        sun_query = re.search('name=\\"SunQueryParamsString\\" value=\\"(.*)\\"', response).group(1)
        self.set_request_header('content-type', 'application/x-www-form-urlencoded')
        # Get the LOGIN FORM. Compose a redirect PATH using the parameters extracted from the last GET request made to smarter
        # Submit the request to get the login form from IDP.
        request_data = {'goto': goto, 'SunQueryParamsString': sun_query, 'IDButton': 'Log In', 'gx_charset': 'UTF-8', 'encoded': 'true', 'IDToken1': user_id, 'IDToken2': password}

        # Send login request to IDP
        self.send_post(self.get_idp_hostname(), request_data)
        # Extract the response received from IDP
        response = self._response.content.decode("utf-8")
        # https: Submit the login information in the login form received from the from previous request a
        # and send a post request to smarter to get the cookie information
        parser = html.parser.HTMLParser()
        url = re.search('action=\\"(.*?)\\"', response).group(1)
        samlresponse = re.search('name=\\"SAMLResponse\\" value=\\"(.*?)\\"', response).group(1)
        relaystate = re.search('name=\\"RelayState\\" value=\\"(.*?)\\"', response).group(1)
        data = {'SAMLResponse': samlresponse, 'RelayState': relaystate}

        self.set_request_header('content-type', 'application/x-www-form-urlencoded')
        # un-escape the strings
        url = parser.unescape(url)
        data['SAMLResponse'] = parser.unescape(data['SAMLResponse'])
        data['RelayState'] = parser.unescape(data['RelayState'])

        # Send post request
        self.send_post(url, data)
        response = self._response.content.decode("utf-8")

        # Get the cookie from response
        cookie_value = self._response.cookies
        self._request_header['cookies'] = cookie_value

    # Checks reponse code
    def check_response_code(self, code):
        expected = self._response.status_code
        self.assertEqual(expected, code, 'Actual return code: {0} Expected: {1}'.format(expected, code))

    # Checks to verify that the current page isn't the error page
    def check_not_error_page(self):
        self.assertNotRegex(self._response.text, 'You\'ve reached this page in error.', 'Error page hit')

    # Checks the size of json response
    # USED
    def check_number_list_elements(self, elements, expected_size):
        element_length = len(elements)
        self.assertEqual(element_length, expected_size, 'Actual size: {0} Expected: {1}'.format(element_length, expected_size))

    # Checks the Fields in main json response body
    def check_resp_list_fields(self, elements=None, expected_fields=None):
        if not elements:
            elements = self._response.json()
        self.__check_number_of_fields(elements, expected_fields)
        self.__check_contains_fields(elements, expected_fields)

    # Checks fields for every item in the body
    def check_each_item_in_body_for_fields(self, expected_fields):
        for row in self._response.json():
            self.__check_contains_fields(row, expected_fields)
            self.__check_number_of_fields(row, expected_fields)

    # Checks fields for every item in the body
    def check_each_item_in_list_for_fields(self, elements, expected_fields):
        """
        Checks fields for every item in the body
        :param elements:  Part of the json body where you need to valid the expected response fields
        :type elements: json element
        :param expected_fields: list of string where each item represents the response field in the json response
        :type expected_fields: list
        """
        for row in elements:
            self.__check_number_of_fields(row, expected_fields)
            self.__check_contains_fields(row, expected_fields)

    # Checks both response fields and values
    # expected_key_values is a list
    def check_response_fields(self, item, expected_key_values):
        self.__check_response_field_or_values(item, expected_key_values)

    # Checks both response fields and values
    # expected_key_values is a dict
    def check_response_fields_and_values(self, item, expected_key_values):
        self.__check_response_field_or_values(item, expected_key_values, True)

    def check_fields_and_values(self, actual, item, expected_key_values):
        self.__check_response_field_or_values(item, expected_key_values, True, actual=actual)

    # USED
    def check_fields(self, body, expected_fields):
        """
        Checks every field from the expected_fields in the body
        :param body:  Part of the json body where you need to validate the expected response fields
        :type body: json response body
        :param expected_fields: list of string where each item represents the response field name
        :type expected_fields: list
        """
        for row in expected_fields:
            self.assertIn(row, body, "{0} is not found".format(row))

    def check_field_absent(self, body, field):
        self.assertNotIn(field, body, "{0} found in response".format(field))

    # Sets request header
    def set_request_header(self, key, value):
        self._request_header['headers'] = {key: value}

    def update_request_header(self, key, value):
        self._request_header['headers'].update({key: value})

    def remove_request_header(self, key):
        self._request_header['headers'].pop(key, None)

    # Sets payload of POST
    def set_payload(self, payload):
        self._request_header['data'] = json.dumps(payload)

    def update_payload(self, payload):
        data = json.loads(self._request_header['data'])
        data.update(payload)
        self.set_payload(data)

    def set_non_json_payload(self, payload):
        self._request_header['data'] = payload

    def set_query_params(self, key, value):
        try:
            params = self._request_header['params']
        except KeyError:
            params = self._request_header['params'] = {}
        params[key] = value

    def get_response_field(self, key):
        json_body = self._response.json()
        return json_body[key]

    def get_response_header_field(self):
        return self._response.headers

    def save_file_from_response(self, file_path):
        with open(file_path, "wb") as file_to_write:
            file_to_write.write(self._response.content)

    # Checks response body for error message
    def check_resp_error(self, msg):
        json_body = self._response.json()
        self.assertEqual(json_body['error'], msg)

    # Checks the Number of Fields in Json response body
    def __check_number_of_fields(self, body, expected_fields):
        expected_count = len(expected_fields)
        actual_count = len(body)
        self.assertEqual(actual_count, expected_count)

    # Checks that somewhere inside json body, the expected fields are there
    # expected_fields is a list for checking fields only
    # expected_fields is a dict for checking fields and values
    def __check_contains_fields(self, body, expected_fields, verify_value=False):
        for row in expected_fields:
            self.assertIn(row, body, "{0} is not found".format(row))
            if verify_value:
                if type(body[row]) == dict:
                    self.__check_contains_fields(body[row], expected_fields[row], True)
                else:
                    self.assertEqual(expected_fields[row].lower(), str(body[row]).lower(), "{0} is not found".format(expected_fields[row]))

    # Checks both response fields and values
    # expected_key_values is a dict
    def __check_response_field_or_values(self, item, expected_key_values, check_value=False, actual=None):
        self._items_to_check = []
        if actual is None:
            actual = self._response.json()
        self.__recursively_get_map(actual, item)
        for row in self._items_to_check:
            self.__check_contains_fields(row, expected_key_values, check_value)
            self.__check_number_of_fields(row, expected_key_values)

    # key is a string, dictionary based, separated by :
    # Map is the data from the table from the test steps (Feature file)
    def __recursively_get_map(self, body, key):
        keys = key.split(':')
        if type(body) is dict:
            self.assertIn(keys[0], body, "{0} is not found".format(keys[0]))
            if len(keys) > 1:
                new_body = body[keys[0]]
                keys.pop(0)
                self.__recursively_get_map(new_body, ':'.join(keys))
            else:
                self._items_to_check.append(body[keys[0]])
        elif type(body) is list:
            for elem in body:
                self.assertIn(keys[0], elem)
                if len(keys) > 1:
                    new_body = elem[keys[0]]
                    keys.pop(0)
                    self.__recursively_get_map(new_body, ':'.join(keys))
                else:
                    self._items_to_check.append(elem[keys[0]])
