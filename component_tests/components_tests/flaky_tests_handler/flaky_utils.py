TITLE = 'title'
LINE = 'line'
STEP = 'step'
ERROR_MESSAGE = 'error_message'

FEATURE = 'feature'
ERROR_TYPE = 'error_type'
FAILED_SCENARIOS = 'failed_scenarios'
FILENAME = 'filename'
FAILED = 'failed'
PASSED = 'passed'


def format_filename(filename, rerun_mode: bool=False) -> str:
    """
    Split filename and extension part (.feature)
    Add suffix '_errors' for error file.
    If feature run in rerun_mode - add suffix '_rerun'
    :return: str: error filename
    """
    f_base = filename.split('.')[0]
    suffix = '_errors'
    rerun = '_rerun' if rerun_mode else ''
    return '{}{}{}.txt'.format(f_base, suffix, rerun)
