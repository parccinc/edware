from behave.model import Feature
from collections import namedtuple, OrderedDict
import json

from components_tests.flaky_tests_handler import flaky_utils as utils


BACKGROUND = 'background'
FAILED = 'failed'
SCENARIO = 'scenario'


class ErrorsHandler(object):
    """
    Handle failed feature, create file by mask "feature_filename_errors.txt".
    We can have background and step errors, data stored in json file in format:
    {
        feature: [feature name],
        filename: [component_tests/[path to the feature]/.feature],
        error_type: error type [background/scenario],
        failed_scenarios : [
            {   [failed_scenario_1]
                line: [line]
                error_message: [error message]
                step: [error step title]
                title: [scenario/background title]
            },
            {   [failed_scenario_2]
                ...
             }
        ]
    }
    EOF
    """
    ErrorStep = namedtuple('ErrorStep', ['step', 'line', 'error_message', 'exception'])
    ErrorObj = namedtuple('ErrorObj', ['error_type', 'title', 'error_step'])

    def __init__(self, rerun_mode: bool):
        """
        Initialize ErrorHandler
        :param rerun_mode:  bool: flag for rerun mode
        """
        self.rerun_mode = rerun_mode
        self.feature = None
        self.feature_name = None
        self.filename = None
        self.error_type = None
        self.errors = []

    def register_feature(self, feature: Feature):
        """
        Register failed feature and feature parameters for further processing
        :param feature: The feature for processing
        """
        self.feature = feature
        self.feature_name = self._get_name(feature)
        self.filename = feature.filename
        self.error_type = BACKGROUND if self._background_step_failed else SCENARIO

    def register_errors(self):
        scenarios = self.feature.scenarios
        if self.error_type == BACKGROUND:
            scenario = scenarios[0]
            self.errors = [self.ErrorObj(BACKGROUND,
                                         self._get_name(self.feature.background),
                                         self._get_failed_step(scenario.background_steps))]
        else:
            failed_scenarios = (sc for sc in scenarios if sc.status == FAILED)
            self.errors = [self.ErrorObj(SCENARIO,
                                         self._get_name(scenario),
                                         self._get_failed_step(scenario.steps))
                           for scenario in failed_scenarios]

    def format_error_file(self):
        """
        Save errors data to the file
        """
        error_file = utils.format_filename(self.filename, self.rerun_mode)
        errors = self._format_errors()
        with open(error_file, 'w') as out:
            json.dump(errors, out, indent=4)

    def _format_errors(self) -> OrderedDict:
        """
        Assign errors to dict
        :return: OrderedDict
        """
        errors = OrderedDict()
        errors[utils.FEATURE] = self.feature_name
        errors[utils.FILENAME] = self.filename
        errors[utils.ERROR_TYPE] = self.error_type
        errors[utils.FAILED_SCENARIOS] = [self._err_to_dict(error) for error in self.errors]
        return errors

    @property
    def _background_step_failed(self) -> bool:
        """
        We run background steps for all scenarios so if it fail once it will fail at all,
        we need only one scenario from feature to check background steps.
        Check if background steps defined, if yes - check if they failed or noot
        return: bool
        """
        if self.feature.background:
            scenario = self.feature.scenarios[0]
            background_steps = scenario.background_steps
            return any(step for step in background_steps if step.status == FAILED)

    def _get_failed_step(self, steps):
        """
        Filter scenario steps and process failed step
        :param steps: Scenario steps
        :return: ErrorStep object with error details
        """
        step = next(step for step in steps if step.status == FAILED)
        return self.ErrorStep(step.name, step.line, step.error_message, step.exception)

    @staticmethod
    def _err_to_dict(error: ErrorObj):
        """
        Save error data to dict for further steps
        :param error: ErrorObj
        :return: OrderedDict
        """
        error_step = error.error_step
        return OrderedDict({
            utils.TITLE: error.title,
            utils.STEP: error_step.step,
            utils.LINE: error_step.line,
            utils.ERROR_MESSAGE: error_step.error_message})

    @staticmethod
    def _get_name(obj) -> str:
        """
        helper method, return parameter "name" from incoming object
        :param obj: Some behave object (feature, scenario, background)
        :return: string
        """
        return obj.name


def handle_failed_feature(feature: Feature, rerun_mode: bool):
    """
    Base method to handle failed scenarios in feature
    :param feature: Feature object that failed
    :param rerun_mode: If rerun_mode = True, save failed scenarios to another file
    """
    handler = ErrorsHandler(rerun_mode)
    handler.register_feature(feature)
    handler.register_errors()
    handler.format_error_file()
