--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: analytics; Type: SCHEMA; Schema: -; Owner: edware
--

CREATE SCHEMA analytics;


ALTER SCHEMA analytics OWNER TO edware;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = analytics, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: dim_accomod; Type: TABLE; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE TABLE dim_accomod (
    _key_accomod integer NOT NULL,
    _hash_accomod character varying(32),
    _valid_from date DEFAULT '1900-01-01'::date NOT NULL,
    _valid_to date DEFAULT '2199-12-31'::date NOT NULL,
    _record_version smallint DEFAULT 0 NOT NULL,
    accomod_ell character(1),
    accomod_504 character(1),
    accomod_ind_ed character(1),
    accomod_freq_breaks character(1),
    accomod_alt_location character(1),
    accomod_small_group character(1),
    accomod_special_equip character(1),
    accomod_spec_area character(1),
    accomod_time_day character(1),
    accomod_answer_mask character(1),
    accomod_color_contrast character varying(19),
    accomod_asl_video character(1),
    accomod_screen_reader character(1),
    accomod_close_capt_ela character(1),
    accomod_braille_ela character(1),
    accomod_tactile_graph character(1),
    accomod_answer_rec character(1),
    accomod_braille_resp character varying(16),
    accomod_construct_resp_ela character varying(20),
    accomod_select_resp_ela character varying(20),
    accomod_monitor_test_resp character(1),
    accomod_word_predict character(1),
    accomod_native_lang character(1),
    accomod_loud_native_lang character varying(40),
    accomod_w_2_w_dict character(1),
    accomod_extend_time character varying(6),
    accomod_alt_paper_test character(1),
    accomod_human_read_sign character varying(14),
    accomod_large_print character(1),
    accomod_braille_tactile character(1),
    accomod_math_resp_el character varying(20),
    accomod_calculator character(1),
    accomod_math_trans_online character(3),
    accomod_paper_trans_math character(3),
    accomod_math_resp character varying(20),
    accomod_text_2_speech_ela character(1),
    accomod_text_2_speech_math character(1),
    accomod_read_ela character varying(50),
    accomod_read_math character varying(50)
);


ALTER TABLE analytics.dim_accomod OWNER TO edware;

--
-- Name: COLUMN dim_accomod.accomod_ell; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_ell IS 'Assessment Accommodation:  English learner (EL)';


--
-- Name: COLUMN dim_accomod.accomod_504; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_504 IS 'Assessment Accommodation: 504. 504 accommodations needed for a given assessment. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_ind_ed; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_ind_ed IS 'Assessment Accommodation: Individualized Educational Plan (IEP)';


--
-- Name: COLUMN dim_accomod.accomod_freq_breaks; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_freq_breaks IS 'Frequent Breaks. Y, Blank. Personal Needs Profile field. Student is allowed to take breaks, at their request, during the testing session. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_alt_location; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_alt_location IS 'Additional Breaks; Separate/Alternate Location';


--
-- Name: COLUMN dim_accomod.accomod_small_group; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_small_group IS 'Small Testing Group';


--
-- Name: COLUMN dim_accomod.accomod_special_equip; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_special_equip IS 'Specialized Equipment or Furniture';


--
-- Name: COLUMN dim_accomod.accomod_spec_area; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_spec_area IS 'Specified Area or Setting';


--
-- Name: COLUMN dim_accomod.accomod_time_day; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_time_day IS 'Time Of Day';


--
-- Name: COLUMN dim_accomod.accomod_answer_mask; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_answer_mask IS 'Answer Masking. Y, blank. Personal Needs Profile field. Specifies as part of an Assessment Personal Needs Profile the type of masks the user is able to create to cover portions of the question until needed. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_color_contrast; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_color_contrast IS 'Color Contrast. black-cream black-light blue black-light magenta white-black light blue-dark blue gray-green Color Overlay blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field. Defines as part of an Assessment Personal Needs Profile the access for preference to invert the foreground and background colors.';


--
-- Name: COLUMN dim_accomod.accomod_asl_video; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_asl_video IS 'ASL Video. y , blank. Personal Needs Profile field. American Sign Language content is provided to the student by a human signer through a video.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_screen_reader; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_screen_reader IS 'Screen Reader OR other Assistive Technology (AT) Application Y blank. Personal Needs Profile field. Screen Reader OR other Assistive Technology (AT) Application used to deliver online test form for ELA/L and Math. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_braille_ela; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_braille_ela IS 'Refreshable Braille Display for ELA/L. Y blank. Personal Needs Profile field. Student uses external device which converts the text from the Screen Reader into Braille.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_tactile_graph; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_tactile_graph IS 'Tactile Graphics. Y blank, Personal Needs Profile field. A tactile representation of the graphic, charts and images information is available outside of the computer testing system. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_answer_rec; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_answer_rec IS 'Answers Recorded in Test Book. Y blank. Personal Needs Profile field. The student records answers directly in the test book. Responses must be transcribed verbatim by a test administrator in a student answer book or answer sheet. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_braille_resp; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_braille_resp IS 'Braille Response. BrailleWriter, BrailleNotetaker, blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.A student who is blind or visually impaired and their response are captured by a Braille Writer or Note taker.';


--
-- Name: COLUMN dim_accomod.accomod_construct_resp_ela; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_construct_resp_ela IS 'ELA/L Constructed Response. SpeechToText HumanScribe HumanSigner ExternalATDevice blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer for Constructed Response item types';


--
-- Name: COLUMN dim_accomod.accomod_select_resp_ela; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_select_resp_ela IS 'ELA/L Selected Response or Technology Enhanced Items. SpeechToText, HumanScribe, HumanSigner, ExternalATDevice, blank.  Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer for Selected Response or Technology Enhanced items types.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_monitor_test_resp; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_monitor_test_resp IS 'Monitor Test Response. Y blank. Personal Needs Profile field. The test administrator or assigned accommodator monitors proper placement of student responses on a test book/answer sheet. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_word_predict; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_word_predict IS 'Word Prediction. Y blank. Personal Needs Profile field. The student uses a word prediction external device that provides a bank of frequently -or recently -used words as a result of the student entering the first few letters of a word. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_native_lang; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_native_lang IS 'General Administration Directions Clarified in Student\92s Native Language (by test administrator)';


--
-- Name: COLUMN dim_accomod.accomod_loud_native_lang; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_loud_native_lang IS 'General Administration Directions Read Aloud and Repeated as Needed in Student\92s Native Language
(by test administrator)';


--
-- Name: COLUMN dim_accomod.accomod_w_2_w_dict; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_w_2_w_dict IS 'Word to Word Dictionary (English/Native Language). Y, blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.';


--
-- Name: COLUMN dim_accomod.accomod_extend_time; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_extend_time IS 'Extended Time';


--
-- Name: COLUMN dim_accomod.accomod_alt_paper_test; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_alt_paper_test IS 'Alternate Representation - Paper Test';


--
-- Name: COLUMN dim_accomod.accomod_human_read_sign; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_human_read_sign IS 'Human Reader or Human Signer. HumanSigner, HumanReadAloud ,blank. The paper test is read aloud or signed to the student by the test administrator (Human Reader). Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_calculator; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_calculator IS 'Calculation Device and Mathematics Tools.Y blank. Personal Needs Profile field. The student is allowed to use a calculator as an accommodation, including for items in test sections designated as non-calculator sections. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_math_trans_online; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_math_trans_online IS 'TranslationoftheMathematicsAssessmentOnline.SPA = Spanish, Blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.';


--
-- Name: COLUMN dim_accomod.accomod_paper_trans_math; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_paper_trans_math IS 'TranslationoftheMathematicsAssessmentOnline.SPA = Spanish, Blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.';


--
-- Name: COLUMN dim_accomod.accomod_math_resp; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_math_resp IS 'Mathematics Response. SpeechToText, HumanScribe, HumanSigner ,ExternalATDevice, blank. Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: dim_accomod__key_accomod_seq; Type: SEQUENCE; Schema: analytics; Owner: edware
--

CREATE SEQUENCE dim_accomod__key_accomod_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE analytics.dim_accomod__key_accomod_seq OWNER TO edware;

--
-- Name: dim_accomod__key_accomod_seq; Type: SEQUENCE OWNED BY; Schema: analytics; Owner: edware
--

ALTER SEQUENCE dim_accomod__key_accomod_seq OWNED BY dim_accomod._key_accomod;


--
-- Name: dim_opt_state_data; Type: TABLE; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE TABLE dim_opt_state_data (
    _key_opt_state_data integer NOT NULL,
    _hash_opt_state_data character varying(32),
    state_id character(2) DEFAULT 'NA'::character(1) NOT NULL,
    district_id character varying(15) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    _valid_from date DEFAULT '1900-01-01'::date NOT NULL,
    _valid_to date DEFAULT '2199-12-31'::date NOT NULL,
    _record_version smallint DEFAULT 0 NOT NULL,
    opt_state_data1 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data2 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data3 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data4 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data5 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data6 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data7 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data8 character varying(20) DEFAULT 'NA'::character varying NOT NULL
);


ALTER TABLE analytics.dim_opt_state_data OWNER TO edware;

--
-- Name: dim_opt_state_data__key_opt_state_data_seq; Type: SEQUENCE; Schema: analytics; Owner: edware
--

CREATE SEQUENCE dim_opt_state_data__key_opt_state_data_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE analytics.dim_opt_state_data__key_opt_state_data_seq OWNER TO edware;

--
-- Name: dim_opt_state_data__key_opt_state_data_seq; Type: SEQUENCE OWNED BY; Schema: analytics; Owner: edware
--

ALTER SEQUENCE dim_opt_state_data__key_opt_state_data_seq OWNED BY dim_opt_state_data._key_opt_state_data;


--
-- Name: dim_poy; Type: TABLE; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE TABLE dim_poy (
    _key_poy integer NOT NULL,
    _valid_from date DEFAULT '1900-01-01'::date NOT NULL,
    _valid_to date DEFAULT '2199-12-31'::date NOT NULL,
    _record_version smallint DEFAULT 0 NOT NULL,
    poy character varying(20) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    end_year character varying(9) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    school_year character(9) DEFAULT 'UNKNOWN'::bpchar NOT NULL
);


ALTER TABLE analytics.dim_poy OWNER TO edware;

--
-- Name: COLUMN dim_poy._key_poy; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_poy._key_poy IS 'YEAR || 01 OR YEAR || 02';


--
-- Name: COLUMN dim_poy.poy; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_poy.poy IS 'Fall, Spring';


--
-- Name: COLUMN dim_poy.end_year; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_poy.end_year IS 'Academic year end year,example 2014 for 2013-2014';


--
-- Name: dim_poy__key_poy_seq; Type: SEQUENCE; Schema: analytics; Owner: edware
--

CREATE SEQUENCE dim_poy__key_poy_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE analytics.dim_poy__key_poy_seq OWNER TO edware;

--
-- Name: dim_poy__key_poy_seq; Type: SEQUENCE OWNED BY; Schema: analytics; Owner: edware
--

ALTER SEQUENCE dim_poy__key_poy_seq OWNED BY dim_poy._key_poy;


--
-- Name: dim_school; Type: TABLE; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE TABLE dim_school (
    _key_school integer NOT NULL,
    _hash_school character varying(32),
    state_id character(2) DEFAULT 'NA'::character(1) NOT NULL,
    district_id character varying(15) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    school_id character varying(15) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    _valid_from date DEFAULT '1900-01-01'::date NOT NULL,
    _valid_to date DEFAULT '2199-12-31'::date NOT NULL,
    _record_version smallint DEFAULT 0 NOT NULL,
    institution_role character varying(4),
    dist_name character varying(60) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    school_name character varying(60) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    school_year character varying(9)
);


ALTER TABLE analytics.dim_school OWNER TO edware;

--
-- Name: COLUMN dim_school.district_id; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_school.district_id IS 'The district responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.  If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';


--
-- Name: COLUMN dim_school.school_id; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_school.school_id IS 'The school responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';


--
-- Name: COLUMN dim_school.institution_role; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_school.institution_role IS 'Role of this institution, RESP, EOY, PBA';


--
-- Name: COLUMN dim_school.dist_name; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_school.dist_name IS 'Name of Responsible district. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';


--
-- Name: COLUMN dim_school.school_name; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_school.school_name IS 'Responsible School/Institution Name - Name of Responsible school. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';


--
-- Name: dim_school__key_school_seq; Type: SEQUENCE; Schema: analytics; Owner: edware
--

CREATE SEQUENCE dim_school__key_school_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE analytics.dim_school__key_school_seq OWNER TO edware;

--
-- Name: dim_school__key_school_seq; Type: SEQUENCE OWNED BY; Schema: analytics; Owner: edware
--

ALTER SEQUENCE dim_school__key_school_seq OWNED BY dim_school._key_school;


--
-- Name: dim_student; Type: TABLE; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE TABLE dim_student (
    _key_student integer NOT NULL,
    _hash_student character varying(32),
    state_id character(2) DEFAULT 'NA'::character(1) NOT NULL,
    district_id character varying(15) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    school_id character varying(15) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    _valid_from date DEFAULT '1900-01-01'::date NOT NULL,
    _valid_to date DEFAULT '2199-12-31'::date NOT NULL,
    _record_version smallint DEFAULT 0 NOT NULL,
    student_parcc_id character varying(40) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    student_sex character(1),
    ethnicity character varying(150) DEFAULT 'could not resolve'::character varying,
    ethn_hisp_latino character(1),
    ethn_indian_alaska character(1),
    ethn_asian character(1),
    ethn_black character(1),
    ethn_hawai character(1),
    ethn_white character(1),
    ethn_filler character(1),
    ethn_two_or_more_races character(1),
    ell character(1),
    lep_status character(1),
    gift_talent character(1),
    migrant_status character(1),
    econo_disadvantage character(1),
    disabil_student character(1),
    primary_disabil_type text,
    student_state_id character varying(40) DEFAULT 'UNKNOWN'::character varying,
    student_local_id character varying(40) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    student_name character varying DEFAULT 'UNKNOWN'::character varying NOT NULL,
    student_dob character varying(10) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    student_first_name character varying(150),
    student_middle_name character varying(150),
    student_last_name character varying(150),
    staff_id character varying(150)
);


ALTER TABLE analytics.dim_student OWNER TO edware;

--
-- Name: COLUMN dim_student.student_parcc_id; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.student_parcc_id IS 'Amplify name: PARCCStudentIdentifier, PARCC name:PARCC Student Identifier, A unique number or alphanumeric code assigned to a student by a school, school system, a state, or other agency or entity. This does not need to be the code associated with the student''s educational record.';


--
-- Name: COLUMN dim_student.ethn_hisp_latino; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_hisp_latino IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_indian_alaska; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_indian_alaska IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_asian; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_asian IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_black; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_black IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_hawai; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_hawai IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_white; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_white IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_filler; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_filler IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_two_or_more_races; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_two_or_more_races IS 'Parcc name: Demographic Race Two or More Races.A person having origins in any of more than one of the racial groups. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20425';


--
-- Name: COLUMN dim_student.ell; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.ell IS 'Parcc name:English Language Learner ELL. English Language Learner
 (ELL).';


--
-- Name: COLUMN dim_student.lep_status; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.lep_status IS 'Ampliofy name:LEPStatus, Parcc name:Limited English Proficient (LEP)   sed to indicate persons (A) who are ages 3 through 21; (B) who are enrolled or preparing to enroll in an elementary school or a secondary school; (C ) (who are I, ii, or iii) (i) who were not born in the United States or whose native languages are languages other than English; (ii) (who are I and II) (I) who are a Native American or Alaska Native, or a native resident of the outlying areas; and (II) who come from an environment where languages other than English have a significant impact on their level of language proficiency; or (iii) who are migratory, whose native languages are languages other than English, and who come from an environment where languages other than English are dominant; and (D) whose difficulties in speaking, reading, writing, or understanding the English language may be sufficient to deny the individuals (who are denied I or ii or iii) (i) the ability to meet the state''s proficient level of achievement on state assessments described in section 1111(b)(3); (ii) the ability to successfully achieve in classrooms where the language of instruction is English; or (iii) the opportunity to participate fully in society.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20783';


--
-- Name: COLUMN dim_student.gift_talent; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.gift_talent IS 'Gifted and Talented. Y,N,Blank. An indication that the student is participating in and served by a Gifted/Talented program. Rule: Test Administration Level Student Data: If either PBA or EOY has Y/true populated then Y/true must be reported on 01 - Summative Score Record.';


--
-- Name: COLUMN dim_student.migrant_status; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.migrant_status IS 'Parcc name:Migrant Status. Persons who are, or whose parents or spouses are, migratory agricultural workers, including migratory dairy workers, or migratory fishers, and who, in the preceding 36 months, in order to obtain, or accompany such parents or spouses, in order to obtain, temporary or seasonal employment in agricultural or fishing work (A) have moved from one LEA to another; (B) in a state that comprises a single LEA, have moved from one administrative area to another within such LEA; or (C) reside in an LEA of more than 15,000 square miles, and migrate a distance of 20 miles or more to a temporary residence to engage in a fishing activity. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20789';


--
-- Name: COLUMN dim_student.econo_disadvantage; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.econo_disadvantage IS 'Parcc field: Economic Disadvantage Status. An indication that the student met the State criteria for classification as having an economic disadvantage. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20741';


--
-- Name: COLUMN dim_student.disabil_student; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.disabil_student IS 'Parcc name:Student With Disability. An indication of whether a person is classified as disabled under the American''s with Disability Act (ADA).https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=3569\A0';


--
-- Name: COLUMN dim_student.student_state_id; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.student_state_id IS 'Parcc name: State Student Identifier. A State assigned student Identifier which is unique within that state. This identifier is used by states that do not wish to share student personal identifying information outside of state-deployed systems. Components sending data to Consortium-deployed systems would use this alternate identifier rather than the student''s SSID.';


--
-- Name: COLUMN dim_student.student_local_id; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.student_local_id IS 'Parcc name: LocalStudentIdentifier. ';


--
-- Name: COLUMN dim_student.student_name; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.student_name IS 'Last name||", "|| First name ||", "||Middle Name';


--
-- Name: COLUMN dim_student.student_dob; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.student_dob IS 'PARCC name:Birthdate, format:YYYY-MM-DD. The year, month and day on which a person was born.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=19995';


--
-- Name: dim_student__key_student_seq; Type: SEQUENCE; Schema: analytics; Owner: edware
--

CREATE SEQUENCE dim_student__key_student_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE analytics.dim_student__key_student_seq OWNER TO edware;

--
-- Name: dim_student__key_student_seq; Type: SEQUENCE OWNED BY; Schema: analytics; Owner: edware
--

ALTER SEQUENCE dim_student__key_student_seq OWNED BY dim_student._key_student;


--
-- Name: dim_test; Type: TABLE; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE TABLE dim_test (
    _key_test integer NOT NULL,
    _hash_test character varying(32),
    state_id character(2) DEFAULT 'NA'::character(1) NOT NULL,
    district_id character varying(15) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    _valid_from date DEFAULT '1900-01-01'::date NOT NULL,
    _valid_to date DEFAULT '2199-12-31'::date NOT NULL,
    _record_version smallint DEFAULT 0 NOT NULL,
    form_id character varying(14) DEFAULT 'NA'::character varying NOT NULL,
    test_category character varying(2) DEFAULT 'NA'::character varying NOT NULL,
    test_subject character varying(35) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    asmt_grade character varying(12) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    test_code character varying(20) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    pba_form_id character varying(50),
    eoy_form_id character varying(50),
    pba_category character(1),
    eoy_category character(1)
);


ALTER TABLE analytics.dim_test OWNER TO edware;

--
-- Name: TABLE dim_test; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON TABLE dim_test IS 'removed course, course_key fields';


--
-- Name: COLUMN dim_test._key_test; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_test._key_test IS 'Test form, NA for summative different for different test_codes';


--
-- Name: COLUMN dim_test.form_id; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_test.form_id IS 'PBA Form ID or  EOY Form ID.  form student tested, NA for Summative';


--
-- Name: COLUMN dim_test.test_category; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_test.test_category IS 'A =  Test Attemptedness flag is blank for both PBA and EOY components test attempts;B = Test attempts in both PBA and EOY components are Y for Voided PBA/EOY Score Code field ;C =  Test attempts in either PBA and EOY ;omponents are Y for Voided PBA/EOY Score Code field and there are no test assignements or test attempts in the other component;D = Test attempts in either PBA and EOY components have the Test Attemptedness flag as blank  and there are no test assignements or test attempts in the other component;E =  Test Assignments exist in both  PBA and EOY components  ;F =  Test Assignment exist in one component (either PBA or EOY) and the other Component has a test attempt that did not meet attemptedness rules ;G =  Test Assignment exist in one component (either PBA or EOY) and the other component has a test attempt that has a Y for Voided PBA/EOY Score Code field ; H =  Test Assignment exist in one component (either PBA or EOY) and no test assignment or test attempt is present in the other component ';


--
-- Name: COLUMN dim_test.test_subject; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_test.test_subject IS 'Amplify name: AssessmentAcademicSubject. scription of the academic content or subject area being evaluated. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21356';


--
-- Name: COLUMN dim_test.asmt_grade; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_test.asmt_grade IS '2 - 02 = Second grade;3 - 03 = Third grade;04 = Fourth grade;05 = Fifth grade; 06 = Sixth grade; 07 = Seventh grade; 08 = Eighth grade; 09 = Ninth grade; 10 = Tenth grade; 11 = Eleventh grade';


--
-- Name: dim_test__key_test_seq; Type: SEQUENCE; Schema: analytics; Owner: edware
--

CREATE SEQUENCE dim_test__key_test_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE analytics.dim_test__key_test_seq OWNER TO edware;

--
-- Name: dim_test__key_test_seq; Type: SEQUENCE OWNED BY; Schema: analytics; Owner: edware
--

ALTER SEQUENCE dim_test__key_test_seq OWNED BY dim_test._key_test;


--
-- Name: dim_unique_test; Type: TABLE; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE TABLE dim_unique_test (
    _key_unique_test integer NOT NULL,
    _valid_from date DEFAULT '1900-01-01'::date NOT NULL,
    _valid_to date DEFAULT '2199-12-31'::date NOT NULL,
    _record_version smallint DEFAULT 0 NOT NULL,
    test_code character varying(20),
    test_subject character varying(35),
    asmt_grade character varying(12)
);


ALTER TABLE analytics.dim_unique_test OWNER TO edware;

--
-- Name: dim_unique_test__key_unique_test_seq; Type: SEQUENCE; Schema: analytics; Owner: edware
--

CREATE SEQUENCE dim_unique_test__key_unique_test_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE analytics.dim_unique_test__key_unique_test_seq OWNER TO edware;

--
-- Name: dim_unique_test__key_unique_test_seq; Type: SEQUENCE OWNED BY; Schema: analytics; Owner: edware
--

ALTER SEQUENCE dim_unique_test__key_unique_test_seq OWNED BY dim_unique_test._key_unique_test;


--
-- Name: fact_sum; Type: TABLE; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE TABLE fact_sum (
    rec_id bigint,
    state_id character(2) DEFAULT 'NA'::character(1) NOT NULL,
    district_id character varying(15) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    school_id character varying(15) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    _key_opt_state_data integer,
    _key_poy integer,
    _key_resp_school integer,
    _key_student integer,
    _key_accomod integer,
    _key_eoy_test_school integer,
    _key_pba_test_school integer,
    _key_pba_test integer,
    _key_eoy_test integer,
    _key_unique_test integer,
    student_grade character varying(12) NOT NULL,
    sum_scale_score integer,
    sum_csem integer,
    sum_perf_lvl smallint,
    sum_read_scale_score integer,
    sum_read_csem integer,
    sum_write_scale_score integer,
    sum_write_csem integer,
    subclaim1_category smallint,
    subclaim2_category smallint,
    subclaim3_category smallint,
    subclaim4_category smallint,
    subclaim5_category smallint,
    subclaim6_category smallint,
    state_growth_percent numeric(5,2),
    district_growth_percent numeric(5,2),
    parcc_growth_percent numeric(5,2),
    multirecord_flag character(2) DEFAULT (-1) NOT NULL,
    result_type character varying(10) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    record_type character varying(20) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    reported_score_flag character(8) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    report_suppression_code character(2),
    report_suppression_action smallint,
    reported_roster_flag character varying(2) DEFAULT 'NA'::character varying NOT NULL,
    include_in_parcc character(1),
    include_in_state character(1),
    include_in_district character(1),
    include_in_school character(1),
    include_in_roster character(1),
    create_date date DEFAULT ('now'::text)::date NOT NULL,
    me_flag character varying(8),
    form_category character varying(2),
    pba_test_uuid character varying(36),
    eoy_test_uuid character varying(36),
    sum_score_rec_uuid character varying(36),
    pba_total_items integer,
    eoy_total_items integer,
    pba_attempt_flag character(1),
    eoy_attempt_flag character(1),
    pba_total_items_attempt integer,
    eoy_total_items_attempt integer,
    pba_total_items_unit1 integer,
    eoy_total_items_unit1 integer,
    pba_total_items_unit2 integer,
    eoy_total_items_unit2 integer,
    pba_total_items_unit3 integer,
    eoy_total_items_unit3 integer,
    pba_total_items_unit4 integer,
    eoy_total_items_unit4 integer,
    pba_total_items_unit5 integer,
    eoy_total_items_unit5 integer,
    pba_unit1_items_attempt integer,
    eoy_unit1_items_attempt integer,
    pba_unit2_items_attempt integer,
    eoy_unit2_items_attempt integer,
    pba_unit3_items_attempt integer,
    eoy_unit3_items_attempt integer,
    pba_unit4_items_attempt integer,
    eoy_unit4_items_attempt integer,
    pba_unit5_items_attempt integer,
    eoy_unit5_items_attempt integer,
    pba_not_tested_reason character varying(150),
    eoy_not_tested_reason character varying(150),
    pba_void_reason character varying(150),
    eoy_void_reason character varying(150),
    student_parcc_id character varying(40)
);


ALTER TABLE analytics.fact_sum OWNER TO edware;

--
-- Name: COLUMN fact_sum._key_poy; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum._key_poy IS 'YEAR || 01 OR YEAR || 02';


--
-- Name: COLUMN fact_sum.student_grade; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.student_grade IS 'The typical grade or combination of grade-levels, developmental levels, or age-levels for which an assessment is designed. (multiple selections supported) . KG, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, PS.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21362';


--
-- Name: COLUMN fact_sum.sum_scale_score; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score';


--
-- Name: COLUMN fact_sum.sum_csem; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_csem IS 'Summative CSEM';


--
-- Name: COLUMN fact_sum.sum_perf_lvl; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_perf_lvl IS 'Summative Performance Level';


--
-- Name: COLUMN fact_sum.sum_read_scale_score; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_read_scale_score IS 'Summative Reading Scale Score';


--
-- Name: COLUMN fact_sum.sum_read_csem; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_read_csem IS 'Summative Reading CSEM';


--
-- Name: COLUMN fact_sum.sum_write_scale_score; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_write_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score';


--
-- Name: COLUMN fact_sum.sum_write_csem; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_write_csem IS 'Summative Writing CSEM';


--
-- Name: COLUMN fact_sum.subclaim1_category; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim1_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- Name: COLUMN fact_sum.subclaim2_category; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim2_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- Name: COLUMN fact_sum.subclaim3_category; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim3_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- Name: COLUMN fact_sum.subclaim4_category; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim4_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- Name: COLUMN fact_sum.subclaim5_category; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim5_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- Name: COLUMN fact_sum.subclaim6_category; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim6_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- Name: COLUMN fact_sum.state_growth_percent; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.state_growth_percent IS 'Student Growth Percentile Compared to State. Blank yr 1 (2014-2015) Yr 2+, student level summative and state level performance from previous year will be extracted from the data warehouse, compared to current year and student growth calculation will be applied Always Blank in PearsonAccessnext - Field placeholder for data warehouse';


--
-- Name: COLUMN fact_sum.district_growth_percent; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.district_growth_percent IS 'Student Growth Percentile Compared to District';


--
-- Name: COLUMN fact_sum.parcc_growth_percent; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.parcc_growth_percent IS 'Student Growth Percentile Compared to PARCC';


--
-- Name: COLUMN fact_sum.multirecord_flag; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.multirecord_flag IS '1 - Y, 0 - N, -1 = blank';


--
-- Name: COLUMN fact_sum.result_type; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.result_type IS 'Summative, PBA, EOY';


--
-- Name: COLUMN fact_sum.record_type; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.record_type IS ' Summative Score, Single Component, No Component';


--
-- Name: COLUMN fact_sum.reported_score_flag; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.reported_score_flag IS 'Is any score reported: Y, N, blank';


--
-- Name: COLUMN fact_sum.reported_roster_flag; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.reported_roster_flag IS 'Y, N, NA';


--
-- Name: _key_accomod; Type: DEFAULT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY dim_accomod ALTER COLUMN _key_accomod SET DEFAULT nextval('dim_accomod__key_accomod_seq'::regclass);


--
-- Name: _key_opt_state_data; Type: DEFAULT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY dim_opt_state_data ALTER COLUMN _key_opt_state_data SET DEFAULT nextval('dim_opt_state_data__key_opt_state_data_seq'::regclass);


--
-- Name: _key_poy; Type: DEFAULT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY dim_poy ALTER COLUMN _key_poy SET DEFAULT nextval('dim_poy__key_poy_seq'::regclass);


--
-- Name: _key_school; Type: DEFAULT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY dim_school ALTER COLUMN _key_school SET DEFAULT nextval('dim_school__key_school_seq'::regclass);


--
-- Name: _key_student; Type: DEFAULT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY dim_student ALTER COLUMN _key_student SET DEFAULT nextval('dim_student__key_student_seq'::regclass);


--
-- Name: _key_test; Type: DEFAULT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY dim_test ALTER COLUMN _key_test SET DEFAULT nextval('dim_test__key_test_seq'::regclass);


--
-- Name: _key_unique_test; Type: DEFAULT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY dim_unique_test ALTER COLUMN _key_unique_test SET DEFAULT nextval('dim_unique_test__key_unique_test_seq'::regclass);


--
-- Data for Name: dim_accomod; Type: TABLE DATA; Schema: analytics; Owner: edware
--

COPY dim_accomod (_key_accomod, _hash_accomod, _valid_from, _valid_to, _record_version, accomod_ell, accomod_504, accomod_ind_ed, accomod_freq_breaks, accomod_alt_location, accomod_small_group, accomod_special_equip, accomod_spec_area, accomod_time_day, accomod_answer_mask, accomod_color_contrast, accomod_asl_video, accomod_screen_reader, accomod_close_capt_ela, accomod_braille_ela, accomod_tactile_graph, accomod_answer_rec, accomod_braille_resp, accomod_construct_resp_ela, accomod_select_resp_ela, accomod_monitor_test_resp, accomod_word_predict, accomod_native_lang, accomod_loud_native_lang, accomod_w_2_w_dict, accomod_extend_time, accomod_alt_paper_test, accomod_human_read_sign, accomod_large_print, accomod_braille_tactile, accomod_math_resp_el, accomod_calculator, accomod_math_trans_online, accomod_paper_trans_math, accomod_math_resp, accomod_text_2_speech_ela, accomod_text_2_speech_math, accomod_read_ela, accomod_read_math) FROM stdin;
1	94f548e1a0f4244674f5daa1b03c4428	1900-01-01	2199-12-31	1	Y	N	Y	Y	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
2	7fcae0fa7ebe260da78655373f3eb235	1900-01-01	2199-12-31	1	Y	N	Y	N	N	N	Y	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
3	3158289fb3bfff3393d46795b0cfda32	1900-01-01	2199-12-31	1	Y	N	N	N	N	N	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	\N	N	N  	\N	N	N	N	N	N
4	304ac8623eb9769de220fe8399c641cd	1900-01-01	2199-12-31	1	Y	Y	Y	N	N	N	Y	N	Y	N	N	Y	Y	N	Y	N	N	N	oralresponse	N	N	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
5	3555f0f06776511a70a1b4ce860c0550	1900-01-01	2199-12-31	1	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
6	758d4a2a88c495f30fd4d560fc71f49d	1900-01-01	2199-12-31	1	N	Y	N	N	N	N	Y	N	N	N	N	Y	N	N	Y	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
7	2f0644cea2d89357e854a178c7620324	1900-01-01	2199-12-31	1	Y	Y	Y	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
8	78cc14dfbb600f5b1f0626ec25652932	1900-01-01	2199-12-31	1	Y	Y	Y	N	N	N	Y	N	N	N	N	Y	Y	N	N	N	N	N	N	SelectedResponse1	N	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
9	4775047f88602684ef6d25e6720a939d	1900-01-01	2199-12-31	1	Y	Y	N	N	N	N	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
10	50a47faa525eefa2ed782e8334cae662	1900-01-01	2199-12-31	1	N	Y	N	N	N	N	Y	N	N	N	N	Y	N	N	N	N	N	N	oralresponse	N	N	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
11	f9a77c06e7e0b886d6f85097ab14075e	1900-01-01	2199-12-31	1	N	N	N	N	N	N	Y	N	N	N	N	Y	Y	N	N	N	N	N	N	N	Y	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
12	48e9afbcc43194a70e6eb050e2565d46	1900-01-01	2199-12-31	1	N	N	N	N	Y	N	Y	N	N	Y	N	Y	N	N	N	N	N	BrailleNotetaker	N	N	N	N	Y	N	Y	N	N	N	N	N	\N	Y	N  	\N	N	N	N	HumanSigner	N
13	6b1e9fb9f87aad5caf4cfcaae03cb48b	1900-01-01	2199-12-31	1	N	N	N	N	N	N	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	Spa	\N	SpeechToText	N	N	N	N
14	9d749a7195b588c7e030484c641f70b8	1900-01-01	2199-12-31	1	N	Y	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	Spa	\N	N	N	Y	N	N
15	a4098781487a3747581efb369ee9ba73	1900-01-01	2199-12-31	1	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	BrailleWriter	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
16	c9ffc1aa67f5e60181ba829a5a7bfa1e	1900-01-01	2199-12-31	1	N	Y	N	N	Y	N	Y	N	N	Y	N	N	Y	N	N	N	Y	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	HumanReadAloud	HumanSigner
17	1f36cd943324f2f7798313065d26f4a9	1900-01-01	2199-12-31	1	N	N	Y	N	Y	N	Y	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	Y	\N	Y	N  	\N	N	Y	Y	N	HumanReadAloud
18	2521fdb4903b095c25fd8a5813cc28da	1900-01-01	2199-12-31	1	N	N	Y	N	N	N	N	N	N	N	N	N	N	Y	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	Y	N	N	N
19	e6560fac9abda531eb0e5be2d8a8e775	1900-01-01	2199-12-31	1	N	N	Y	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
20	6b0f6f78ed0afd18a6f359bea371631f	1900-01-01	2199-12-31	1	N	Y	Y	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	\N	Y	N  	\N	N	N	N	N	N
21	db5a17fca0ce4bb2e8d1db2b0a07123d	1900-01-01	2199-12-31	1	Y	N	N	N	N	N	Y	N	N	N	Y	N	Y	Y	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	Y	\N	Y	N  	\N	N	N	N	N	N
22	bdc251afa05295996bfa5ab57f85a034	1900-01-01	2199-12-31	1	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	HumanSigner	N
23	d21cf32bab659ad043af841cd45e01b5	1900-01-01	2199-12-31	1	Y	Y	Y	N	N	N	Y	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	OralScriptReadbyTestAdministratorARA	N	N	N	N	N	N	\N	Y	Spa	\N	SpeechToText	N	N	N	N
24	5370c18f05213b18b4444780cfc80e3a	1900-01-01	2199-12-31	1	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	\N	N	N  	\N	N	Y	N	HumanSigner	HumanSigner
25	9e6331d7d8047d3612f29d524bd90945	1900-01-01	2199-12-31	1	N	Y	Y	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
26	cbde30f30a5ae9112496ee74b03c1fdb	1900-01-01	2199-12-31	1	Y	Y	Y	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	SelectedResponse1	N	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
27	b7205c12302b849dc1b590b2e893a487	1900-01-01	2199-12-31	1	N	Y	Y	N	N	N	N	N	N	N	N	Y	N	N	Y	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
28	2fcf95f21a8f7311e7354afa52e32fbe	1900-01-01	2199-12-31	1	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	SelectedResponse1	Y	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	HumanSigner	N
29	176acfa22cdc9ad3ca853b47a0b85bc9	1900-01-01	2199-12-31	1	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	Y	Y	N	Y	N	N	N	N	Y	\N	N	N  	\N	N	N	N	N	N
30	3d6029001391ead9d274f1e823bd401b	1900-01-01	2199-12-31	1	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	HumanSigner
31	8a13272a0db147ebc4b999486023bb13	1900-01-01	2199-12-31	1	N	Y	Y	N	N	N	N	N	N	N	N	Y	Y	Y	N	N	Y	BrailleWriter	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
32	5f1f68698b43e0424d4f4d7bfc1a4891	1900-01-01	2199-12-31	1	N	Y	N	Y	N	Y	Y	N	N	N	N	N	Y	N	N	N	N	N	N	N	Y	N	N	N	N	Y	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
33	06656708f45fe7522d7244baa19f765f	1900-01-01	2199-12-31	1	Y	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	Spa	\N	N	Y	N	N	N
34	cdd0d37e077a6cc2339ea4822ee5a333	1900-01-01	2199-12-31	1	Y	Y	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	SpeechToText	N	N	N	N
35	f8fd233571923095ff5af6d029b06d99	1900-01-01	2199-12-31	1	N	N	Y	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
36	0a1d2a797fdf50b5369c200c205de0f6	1900-01-01	2199-12-31	1	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
37	78b6298a2f0909fbcd65acaa8efa52e5	1900-01-01	2199-12-31	1	N	N	Y	N	N	Y	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
38	43ae4d19c3dd02b2214e080a6635375d	1900-01-01	2199-12-31	1	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
39	f2e4a34d73238af8b0b0cbf55caf2786	1900-01-01	2199-12-31	1	Y	N	Y	N	N	N	N	Y	Y	N	N	N	Y	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
40	4aca36ecd8d7f911516990a3e8a1397c	1900-01-01	2199-12-31	1	Y	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
41	4be85136d7a79c845f08f73ba7284d1a	1900-01-01	2199-12-31	1	Y	N	Y	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
42	2cc568d6a09587142941a2ed7f4d4313	1900-01-01	2199-12-31	1	Y	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
43	2f6fca59f9a11314e017d4d5e3eca2a3	1900-01-01	2199-12-31	1	N	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
44	b39a1d59af5964aabc0a912b6e1bb0cd	1900-01-01	2199-12-31	1	Y	N	N	N	Y	N	N	Y	N	N	N	Y	Y	Y	N	N	N	N	oralresponse	SelectedResponse1	N	N	N	N	N	N	N	N	N	Y	\N	Y	Spa	\N	N	N	N	N	HumanReadAloud
45	511df7e99beb9d05769a2139741d8f97	1900-01-01	2199-12-31	1	N	Y	Y	N	Y	N	N	N	N	N	N	Y	N	Y	N	N	N	N	N	SelectedResponse1	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
46	a026593b0a1749cea1dd807a48d0e9a1	1900-01-01	2199-12-31	1	Y	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	HumanSigner	N
47	f1baa91078f4d8d2161fca3128f29405	1900-01-01	2199-12-31	1	Y	Y	N	N	Y	N	N	N	N	N	N	N	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
48	c96d6227af52f75fee84f08b588ca151	1900-01-01	2199-12-31	1	N	N	N	N	N	N	Y	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
49	b76f6e75066a06b05ebb071faa00b336	1900-01-01	2199-12-31	1	N	Y	N	N	N	N	Y	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
50	aa6648e323deff20427218b0944eed1d	1900-01-01	2199-12-31	1	N	N	Y	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
51	5c59fa516d20df094e8ce3186f517cfb	1900-01-01	2199-12-31	1	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	Y	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
52	864e796810356f9f45150661290ef3f5	1900-01-01	2199-12-31	1	N	N	Y	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	Y	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
53	23a9f4553acb537a752b75136b188af7	1900-01-01	2199-12-31	1	Y	Y	N	Y	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
54	172ace0d0a23a92b1b7fb2606534ca66	1900-01-01	2199-12-31	1	N	Y	N	N	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
55	1fda8619da5ee6e31bcff8dc124f667b	1900-01-01	2199-12-31	1	N	Y	N	Y	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	Spa	\N	N	N	N	N	N
56	cbb9373455cfa0ab65256ccadd9becf7	1900-01-01	2199-12-31	1	N	Y	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
57	5f8c204fc63b8b6819422d541adab4c8	1900-01-01	2199-12-31	1	N	Y	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
58	8e6e0c2eff2335f594dfebaa48e7b19d	1900-01-01	2199-12-31	1	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
59	2439d1e6ac4ee71e82eba55c49b70413	1900-01-01	2199-12-31	1	N	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
60	f2789e8d2ab772e2356f543521f1022c	1900-01-01	2199-12-31	1	Y	N	Y	N	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	Spa	\N	N	N	N	N	N
61	2c56849572b852ccfeb46873771e3dcd	1900-01-01	2199-12-31	1	Y	N	N	N	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	Spa	\N	N	N	N	N	N
62	609d338410e877e4e8c286f2825bfc72	1900-01-01	2199-12-31	1	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
63	fd6ad4c75bd8e39177733cd20a8e1077	1900-01-01	2199-12-31	1	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
64	d980eec4fd69b3c36a3cb66e167933d6	1900-01-01	2199-12-31	1	N	Y	Y	N	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	Spa	\N	N	N	N	N	N
65	75b9edfa9820a9e4ba7c1935c1d6e1db	1900-01-01	2199-12-31	1	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
66	fc464ea6f2c1357d6c1701930428a665	1900-01-01	2199-12-31	1	N	Y	Y	N	N	N	Y	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
67	cd6673b77958ecf3ab72fd61dbea1c3b	1900-01-01	2199-12-31	1	N	N	N	N	N	N	Y	N	Y	N	N	Y	N	N	Y	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	Y	\N	Y	N  	\N	N	Y	N	N	N
68	234d42497dab403f8c66013384e5c351	1900-01-01	2199-12-31	1	Y	Y	N	Y	N	N	N	N	N	N	N	Y	N	N	Y	N	N	N	N	N	N	N	Y	N	N	N	Y	N	N	N	\N	N	Spa	\N	N	N	N	N	N
69	e76dfd2b9938df4a219d4406f04fb783	1900-01-01	2199-12-31	1	N	Y	Y	N	N	N	Y	N	N	N	N	Y	Y	N	Y	N	N	N	N	N	N	N	Y	OralScriptReadbyTestAdministratorARA	N	Y	N	N	N	Y	\N	Y	N  	\N	SpeechToText	Y	N	HumanReadAloud	N
70	d4e74a1fca0c1b7a9fa243468bd4aeee	1900-01-01	2199-12-31	1	N	Y	Y	N	N	Y	N	N	N	N	N	Y	Y	N	Y	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	Spa	\N	N	N	N	N	N
71	bd32cf106a01c91e62476cce35283578	1900-01-01	2199-12-31	1	Y	Y	N	Y	N	N	Y	N	N	N	N	N	N	N	Y	N	Y	N	N	N	N	N	Y	N	N	Y	N	N	Y	Y	\N	Y	N  	\N	N	N	N	N	HumanSigner
72	dabb9d55926a6340766c38168916d93c	1900-01-01	2199-12-31	1	Y	N	N	N	N	Y	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	Spa	\N	N	N	N	HumanSigner	N
73	bad05b01c526f19b00e840d6520afb52	1900-01-01	2199-12-31	1	N	N	N	N	N	N	Y	N	N	N	Y	N	Y	N	N	N	Y	N	N	N	N	N	Y	N	N	N	N	N	N	Y	\N	Y	N  	\N	SpeechToText	N	N	N	N
74	18becb20beef5c6c6a15f76ae3734a38	1900-01-01	2199-12-31	1	Y	N	N	N	N	Y	N	N	N	N	N	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	HumanReadAloud	N
75	82832f7d902729192793197ed23d8d06	1900-01-01	2199-12-31	1	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	oralresponse	N	N	N	Y	N	N	Y	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
76	20bf4c68b1208b9780936f2492eef4fd	1900-01-01	2199-12-31	1	N	N	Y	N	N	Y	N	N	N	N	N	Y	Y	Y	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	HumanReadAloud	HumanReadAloud
77	3d47b011bc52b3372982fb7ebff40e6f	1900-01-01	2199-12-31	1	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	oralresponse	N	N	N	Y	N	N	Y	N	N	N	N	\N	Y	N  	\N	SpeechToText	N	N	N	N
78	1f38f190edb6a07d9cf25b48a0c61c3f	1900-01-01	2199-12-31	1	N	Y	N	N	N	Y	N	N	N	N	N	N	Y	Y	N	N	Y	BrailleNotetaker	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	Y	N	N	HumanReadAloud
79	811dfce09bdddf4d9b5fa74d601d04b6	1900-01-01	2199-12-31	1	N	Y	N	N	N	N	Y	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	Y	OralScriptReadbyTestAdministratorARA	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
80	d83696d89621390b25d0020e2fc75b2c	1900-01-01	2199-12-31	1	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	BrailleNotetaker	N	N	N	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	HumanReadAloud
81	0032d78fc32cced1e9118a21816fe922	1900-01-01	2199-12-31	1	Y	N	N	N	N	Y	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
82	bdd8f1fb89798c1c3c98e24c0c6047a4	1900-01-01	2199-12-31	1	Y	N	Y	N	N	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	Y	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
83	36d058b5bbd0027429008c722a81f269	1900-01-01	2199-12-31	1	Y	Y	Y	N	N	N	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
84	30e8dd469b7e71b9eda89f0148c4417b	1900-01-01	2199-12-31	1	Y	N	Y	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	SelectedResponse1	N	N	Y	N	N	Y	N	N	N	N	\N	N	N  	\N	SpeechToText	N	N	N	N
85	29ff755775e373268236e7d1f076579d	1900-01-01	2199-12-31	1	Y	Y	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	OralScriptReadbyTestAdministratorARA	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
86	6743ff0cb9faea747967ee99aa6a5741	1900-01-01	2199-12-31	1	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	OralScriptReadbyTestAdministratorARA	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
87	d6778fa4904b86a26329c82514ea64d0	1900-01-01	2199-12-31	1	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	OralScriptReadbyTestAdministratorARA	N	N	N	N	N	N	\N	N	N  	\N	N	N	N	N	N
88	f38affe1742a9228f12b8aa51cff8cdb	1900-01-01	2199-12-31	1	N	N	N	N	N	N	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	Spa	\N	N	N	N	N	N
89	ab37314c9306e271464e04015fd1175b	1900-01-01	2199-12-31	1	Y	N	Y	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	SelectedResponse1	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
90	900bc05ee76ee20318e05a2459c3b04f	1900-01-01	2199-12-31	1	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	oralresponse	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	N	N	N	N
0	0	1900-01-01	2199-12-31	0	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	N  	N	N	N	N	N
\.


--
-- Name: dim_accomod__key_accomod_seq; Type: SEQUENCE SET; Schema: analytics; Owner: edware
--

SELECT pg_catalog.setval('dim_accomod__key_accomod_seq', 90, true);


--
-- Data for Name: dim_opt_state_data; Type: TABLE DATA; Schema: analytics; Owner: edware
--

COPY dim_opt_state_data (_key_opt_state_data, _hash_opt_state_data, state_id, district_id, _valid_from, _valid_to, _record_version, opt_state_data1, opt_state_data2, opt_state_data3, opt_state_data4, opt_state_data5, opt_state_data6, opt_state_data7, opt_state_data8) FROM stdin;
1	91feb5e6277edcd47dc25d1096678e7d	NY	R0001	1900-01-01	2199-12-31	1	N	NY0nKXcg2ZoH6GfGM	NY0nKXcg2ZoH6GfGM	NY0nKXcg2ZoH6GfGM	NY0nKXcg2ZoH6GfGM	NY0nKXcg2ZoH6GfGM	NY0nKXcg2ZoH6GfGM	NY0nKXcg2ZoH6GfGM
2	9bf520029bc5ec95c194affa4ce56b65	NY	R0001	1900-01-01	2199-12-31	1	N	eQTOG9VhE3	eQTOG9VhE3	eQTOG9VhE3	eQTOG9VhE3	eQTOG9VhE3	eQTOG9VhE3	eQTOG9VhE3
3	f00fd47b52eb7f48006060d3823ae6fe	NY	R0001	1900-01-01	2199-12-31	1	N	w3Cme6NvN7UxsU4	w3Cme6NvN7UxsU4	w3Cme6NvN7UxsU4	w3Cme6NvN7UxsU4	w3Cme6NvN7UxsU4	w3Cme6NvN7UxsU4	w3Cme6NvN7UxsU4
4	964e69e2f82b41ae67152409cad1c6bd	NY	R0001	1900-01-01	2199-12-31	1	N	37g	37g	37g	37g	37g	37g	37g
5	22f5dd06516365e6a4e23c223bad14ca	NY	R0002	1900-01-01	2199-12-31	1	N	N	N	N	N	N	N	N
6	e84f04f7cab27f857625155fc5e32b44	NY	R0001	1900-01-01	2199-12-31	1	N	ABHIjAtBdXJYnOVKx	ABHIjAtBdXJYnOVKx	ABHIjAtBdXJYnOVKx	ABHIjAtBdXJYnOVKx	ABHIjAtBdXJYnOVKx	ABHIjAtBdXJYnOVKx	ABHIjAtBdXJYnOVKx
7	deaf5465df8b824ea160112c0888078d	NY	R0002	1900-01-01	2199-12-31	1	N	1UbnSd2QJMmJ0Y7J	1UbnSd2QJMmJ0Y7J	1UbnSd2QJMmJ0Y7J	1UbnSd2QJMmJ0Y7J	1UbnSd2QJMmJ0Y7J	1UbnSd2QJMmJ0Y7J	1UbnSd2QJMmJ0Y7J
8	8943b82f5b58d14c92f4ab8a10bbafc4	NY	R0002	1900-01-01	2199-12-31	1	N	B80ISk2pkFJDa0	B80ISk2pkFJDa0	B80ISk2pkFJDa0	B80ISk2pkFJDa0	B80ISk2pkFJDa0	B80ISk2pkFJDa0	B80ISk2pkFJDa0
9	a884656de2ada166a5adb6b8477bd57d	NY	R0002	1900-01-01	2199-12-31	1	N	spwemE1dqM	spwemE1dqM	spwemE1dqM	spwemE1dqM	spwemE1dqM	spwemE1dqM	spwemE1dqM
10	5bcd9dfc252e96d3cc4dc34a9558bb38	NY	R0002	1900-01-01	2199-12-31	1	N	nKmBv46Ei8mejG	nKmBv46Ei8mejG	nKmBv46Ei8mejG	nKmBv46Ei8mejG	nKmBv46Ei8mejG	nKmBv46Ei8mejG	nKmBv46Ei8mejG
11	b14a6d6e0b8935c323a73534b6e1600c	NY	R0003	1900-01-01	2199-12-31	1	N	S6i	S6i	S6i	S6i	S6i	S6i	S6i
12	11ffc6de9df8186486527ce5581ca8af	NY	R0003	1900-01-01	2199-12-31	1	N	pp14le3Zr1B7pxKKGdM	pp14le3Zr1B7pxKKGdM	pp14le3Zr1B7pxKKGdM	pp14le3Zr1B7pxKKGdM	pp14le3Zr1B7pxKKGdM	pp14le3Zr1B7pxKKGdM	pp14le3Zr1B7pxKKGdM
13	1e59d3d6d9a14d62929a84312e18e90e	NY	R0003	1900-01-01	2199-12-31	1	N	qAoIfWmsm1U4VL	qAoIfWmsm1U4VL	qAoIfWmsm1U4VL	qAoIfWmsm1U4VL	qAoIfWmsm1U4VL	qAoIfWmsm1U4VL	qAoIfWmsm1U4VL
14	e52e9d0182cae736363dc9488dbacc65	NY	R0003	1900-01-01	2199-12-31	1	N	v2j	v2j	v2j	v2j	v2j	v2j	v2j
15	2fb0f4ce5e06a5d7ccd165250ab494c6	NY	R0003	1900-01-01	2199-12-31	1	N	te3e	te3e	te3e	te3e	te3e	te3e	te3e
16	0582bca1b3fc04f805a3e613e903c5ef	NY	R0003	1900-01-01	2199-12-31	1	N	GPnC0vRqe9lMILMaE	GPnC0vRqe9lMILMaE	GPnC0vRqe9lMILMaE	GPnC0vRqe9lMILMaE	GPnC0vRqe9lMILMaE	GPnC0vRqe9lMILMaE	GPnC0vRqe9lMILMaE
17	2a936b3567de5c21dfc822378067328e	NY	R0003	1900-01-01	2199-12-31	1	N	5SaXfXdCL4zarJCqnY3	5SaXfXdCL4zarJCqnY3	5SaXfXdCL4zarJCqnY3	5SaXfXdCL4zarJCqnY3	5SaXfXdCL4zarJCqnY3	5SaXfXdCL4zarJCqnY3	5SaXfXdCL4zarJCqnY3
18	c73f73151b0fb4da94c4bcd40a0fe5e0	NY	R0003	1900-01-01	2199-12-31	1	N	9CdyHvSWO2C2uNrDmP	9CdyHvSWO2C2uNrDmP	9CdyHvSWO2C2uNrDmP	9CdyHvSWO2C2uNrDmP	9CdyHvSWO2C2uNrDmP	9CdyHvSWO2C2uNrDmP	9CdyHvSWO2C2uNrDmP
19	b94becbd513b1dd6424f1dab3e9afaf8	NY	R0003	1900-01-01	2199-12-31	1	N	vHmP2m0wkmCluGkz	vHmP2m0wkmCluGkz	vHmP2m0wkmCluGkz	vHmP2m0wkmCluGkz	vHmP2m0wkmCluGkz	vHmP2m0wkmCluGkz	vHmP2m0wkmCluGkz
20	bad67d2cf25d67cfe7c4c1f0d74dd942	NY	R0003	1900-01-01	2199-12-31	1	N	LnWxz	LnWxz	LnWxz	LnWxz	LnWxz	LnWxz	LnWxz
21	14781de16329e6483b655625a2489c1e	NY	R0003	1900-01-01	2199-12-31	1	N	cUHfZBYYqD4kDSMdO	cUHfZBYYqD4kDSMdO	cUHfZBYYqD4kDSMdO	cUHfZBYYqD4kDSMdO	cUHfZBYYqD4kDSMdO	cUHfZBYYqD4kDSMdO	cUHfZBYYqD4kDSMdO
22	629e640ba166b9460db512740ca7d5e9	NY	R0003	1900-01-01	2199-12-31	1	N	mJuy6a3GcGHbwR	mJuy6a3GcGHbwR	mJuy6a3GcGHbwR	mJuy6a3GcGHbwR	mJuy6a3GcGHbwR	mJuy6a3GcGHbwR	mJuy6a3GcGHbwR
23	b818b182251a334fddd4370210c829b8	NY	R0003	1900-01-01	2199-12-31	1	N	kYw0	kYw0	kYw0	kYw0	kYw0	kYw0	kYw0
24	999fb04b58bef0d1e4b607b6f730c55b	NY	R0003	1900-01-01	2199-12-31	1	N	vtnA2m	vtnA2m	vtnA2m	vtnA2m	vtnA2m	vtnA2m	vtnA2m
25	78d0ea4578484156139c44c509fa014b	NY	R0001	1900-01-01	2199-12-31	1	N	GL7fec1XzKzib	GL7fec1XzKzib	GL7fec1XzKzib	GL7fec1XzKzib	GL7fec1XzKzib	GL7fec1XzKzib	GL7fec1XzKzib
26	e669baafcdb4c9add95d0b024db40c04	NY	R0001	1900-01-01	2199-12-31	1	N	AofVP2mMC41Yt1	AofVP2mMC41Yt1	AofVP2mMC41Yt1	AofVP2mMC41Yt1	AofVP2mMC41Yt1	AofVP2mMC41Yt1	AofVP2mMC41Yt1
27	2d7d3603acbd83b504441d12393dd085	NY	R0001	1900-01-01	2199-12-31	1	N	KM08fO	KM08fO	KM08fO	KM08fO	KM08fO	KM08fO	KM08fO
28	53bc6068305ff877d217f62fe7b420b4	NY	R0001	1900-01-01	2199-12-31	1	N	zfCkJ5pIBilv	zfCkJ5pIBilv	zfCkJ5pIBilv	zfCkJ5pIBilv	zfCkJ5pIBilv	zfCkJ5pIBilv	zfCkJ5pIBilv
29	ebdb62a2f6cfe9a35f9a4006ea746223	NY	R0002	1900-01-01	2199-12-31	1	N	vEcx9C9wPnkP8BRD	vEcx9C9wPnkP8BRD	vEcx9C9wPnkP8BRD	vEcx9C9wPnkP8BRD	vEcx9C9wPnkP8BRD	vEcx9C9wPnkP8BRD	vEcx9C9wPnkP8BRD
30	7d44a2b1f90d02ffe82ad97f87e55c8c	NY	R0002	1900-01-01	2199-12-31	1	N	bEcar4ZudgE7	bEcar4ZudgE7	bEcar4ZudgE7	bEcar4ZudgE7	bEcar4ZudgE7	bEcar4ZudgE7	bEcar4ZudgE7
31	4338f5a96d38bc3244cb8cd74498f7a6	NY	R0002	1900-01-01	2199-12-31	1	N	LNjtpdOtesj1c4amNTk	LNjtpdOtesj1c4amNTk	LNjtpdOtesj1c4amNTk	LNjtpdOtesj1c4amNTk	LNjtpdOtesj1c4amNTk	LNjtpdOtesj1c4amNTk	LNjtpdOtesj1c4amNTk
32	df82b117748ef97ae788d16820debe9c	NY	R0002	1900-01-01	2199-12-31	1	N	9opCz4520wIrA5pt	9opCz4520wIrA5pt	9opCz4520wIrA5pt	9opCz4520wIrA5pt	9opCz4520wIrA5pt	9opCz4520wIrA5pt	9opCz4520wIrA5pt
33	16f4b065046a60e4c28079d4a563b863	NY	R0003	1900-01-01	2199-12-31	1	N	5VeHNBZEifdweJB	5VeHNBZEifdweJB	5VeHNBZEifdweJB	5VeHNBZEifdweJB	5VeHNBZEifdweJB	5VeHNBZEifdweJB	5VeHNBZEifdweJB
34	653f593022b32d258fd12c0c083d27e9	NY	R0003	1900-01-01	2199-12-31	1	N	ZUbaT2XarRNdc71	ZUbaT2XarRNdc71	ZUbaT2XarRNdc71	ZUbaT2XarRNdc71	ZUbaT2XarRNdc71	ZUbaT2XarRNdc71	ZUbaT2XarRNdc71
35	4bb45526cd6efce43a4ca5283ca274ef	NY	R0003	1900-01-01	2199-12-31	1	N	jg	jg	jg	jg	jg	jg	jg
36	bd697b2cbd9ac140b9c7ba1a02ad5b1c	NY	R0003	1900-01-01	2199-12-31	1	N	f8xX7D9KWP6ArNd8	f8xX7D9KWP6ArNd8	f8xX7D9KWP6ArNd8	f8xX7D9KWP6ArNd8	f8xX7D9KWP6ArNd8	f8xX7D9KWP6ArNd8	f8xX7D9KWP6ArNd8
37	6ae834ec454ba58b1f7640c107ec0c46	NY	R0003	1900-01-01	2199-12-31	1	N	idlDCzYl45cO7	idlDCzYl45cO7	idlDCzYl45cO7	idlDCzYl45cO7	idlDCzYl45cO7	idlDCzYl45cO7	idlDCzYl45cO7
38	296bce008ae95c2cb90486468ccf8d61	NY	R0003	1900-01-01	2199-12-31	1	N	wHtmUDzScbdN7QHw	wHtmUDzScbdN7QHw	wHtmUDzScbdN7QHw	wHtmUDzScbdN7QHw	wHtmUDzScbdN7QHw	wHtmUDzScbdN7QHw	wHtmUDzScbdN7QHw
39	131f3c147b7da3521febe246bb72ff5b	NY	R0003	1900-01-01	2199-12-31	1	N	5dj9tnUtD0BaIWR	5dj9tnUtD0BaIWR	5dj9tnUtD0BaIWR	5dj9tnUtD0BaIWR	5dj9tnUtD0BaIWR	5dj9tnUtD0BaIWR	5dj9tnUtD0BaIWR
40	00666888fd49508e4acec35641c17d47	NY	R0003	1900-01-01	2199-12-31	1	N	PBX1kyfKLuDPY9	PBX1kyfKLuDPY9	PBX1kyfKLuDPY9	PBX1kyfKLuDPY9	PBX1kyfKLuDPY9	PBX1kyfKLuDPY9	PBX1kyfKLuDPY9
41	690b7f60e703cd98b98a2bb39619b575	NY	R0003	1900-01-01	2199-12-31	1	N	zzCOsCpPTKTDl6b	zzCOsCpPTKTDl6b	zzCOsCpPTKTDl6b	zzCOsCpPTKTDl6b	zzCOsCpPTKTDl6b	zzCOsCpPTKTDl6b	zzCOsCpPTKTDl6b
42	5f2e9f15e46ebf8c1970d271be2b23cd	NY	R0003	1900-01-01	2199-12-31	1	N	9mGeugc3KNKzBWDQoe	9mGeugc3KNKzBWDQoe	9mGeugc3KNKzBWDQoe	9mGeugc3KNKzBWDQoe	9mGeugc3KNKzBWDQoe	9mGeugc3KNKzBWDQoe	9mGeugc3KNKzBWDQoe
43	feab7e5446b554e97b3051f96d9e037c	NY	R0003	1900-01-01	2199-12-31	1	N	YKNI8ZHfGL	YKNI8ZHfGL	YKNI8ZHfGL	YKNI8ZHfGL	YKNI8ZHfGL	YKNI8ZHfGL	YKNI8ZHfGL
44	9aec359dd9d874de8a8f5c28e2887e09	NY	R0001	1900-01-01	2199-12-31	1	N	WdC3qlWLnLqd3	WdC3qlWLnLqd3	WdC3qlWLnLqd3	WdC3qlWLnLqd3	WdC3qlWLnLqd3	WdC3qlWLnLqd3	WdC3qlWLnLqd3
45	6aaaa2c11f303c1b49637a1544253f84	NY	R0001	1900-01-01	2199-12-31	1	N	LrOZusXVqM8	LrOZusXVqM8	LrOZusXVqM8	LrOZusXVqM8	LrOZusXVqM8	LrOZusXVqM8	LrOZusXVqM8
46	e23dba71d34a6609da0b569a6da716d2	NY	R0001	1900-01-01	2199-12-31	1	N	E8Ecxsy2JttginOv	E8Ecxsy2JttginOv	E8Ecxsy2JttginOv	E8Ecxsy2JttginOv	E8Ecxsy2JttginOv	E8Ecxsy2JttginOv	E8Ecxsy2JttginOv
47	4da455c57f84f6fce8f2fb08773e7202	NY	R0001	1900-01-01	2199-12-31	1	N	YxmBdJ387mTd	YxmBdJ387mTd	YxmBdJ387mTd	YxmBdJ387mTd	YxmBdJ387mTd	YxmBdJ387mTd	YxmBdJ387mTd
48	dbb6a7ae5019f2e326c5c45a7f7831eb	NY	R0001	1900-01-01	2199-12-31	1	N	lrYyyzuoIeeUodL	lrYyyzuoIeeUodL	lrYyyzuoIeeUodL	lrYyyzuoIeeUodL	lrYyyzuoIeeUodL	lrYyyzuoIeeUodL	lrYyyzuoIeeUodL
49	b4cf90c0eeba02f97a6c956835677356	NY	R0002	1900-01-01	2199-12-31	1	N	XDqx0NRSSlODiFCNLIPO	XDqx0NRSSlODiFCNLIPO	XDqx0NRSSlODiFCNLIPO	XDqx0NRSSlODiFCNLIPO	XDqx0NRSSlODiFCNLIPO	XDqx0NRSSlODiFCNLIPO	XDqx0NRSSlODiFCNLIPO
50	77185a5b155f48c245de0babba398e14	NY	R0002	1900-01-01	2199-12-31	1	N	Nd8aL6OZHpTzAK	Nd8aL6OZHpTzAK	Nd8aL6OZHpTzAK	Nd8aL6OZHpTzAK	Nd8aL6OZHpTzAK	Nd8aL6OZHpTzAK	Nd8aL6OZHpTzAK
51	218ff204edb88265ea86f99471dcb7cb	NY	R0002	1900-01-01	2199-12-31	1	N	MWzV6dFs4TRRo	MWzV6dFs4TRRo	MWzV6dFs4TRRo	MWzV6dFs4TRRo	MWzV6dFs4TRRo	MWzV6dFs4TRRo	MWzV6dFs4TRRo
52	3fb2148bc843c5fc3a2b0edad6492011	NY	R0002	1900-01-01	2199-12-31	1	N	IWu1IFLDr1GBos5kOyz0	IWu1IFLDr1GBos5kOyz0	IWu1IFLDr1GBos5kOyz0	IWu1IFLDr1GBos5kOyz0	IWu1IFLDr1GBos5kOyz0	IWu1IFLDr1GBos5kOyz0	IWu1IFLDr1GBos5kOyz0
53	c4186551d4baf544983f5a53390596c0	NY	R0002	1900-01-01	2199-12-31	1	N	SamCuvOAmoCIEUh	SamCuvOAmoCIEUh	SamCuvOAmoCIEUh	SamCuvOAmoCIEUh	SamCuvOAmoCIEUh	SamCuvOAmoCIEUh	SamCuvOAmoCIEUh
54	0f3915e21a01354216dc72c900bf70d1	NY	R0003	1900-01-01	2199-12-31	1	N	Z4cKN	Z4cKN	Z4cKN	Z4cKN	Z4cKN	Z4cKN	Z4cKN
55	5b3e6b1555e2848722d4a506d1935c20	NY	R0003	1900-01-01	2199-12-31	1	N	8zyJiQcSCrTg5H	8zyJiQcSCrTg5H	8zyJiQcSCrTg5H	8zyJiQcSCrTg5H	8zyJiQcSCrTg5H	8zyJiQcSCrTg5H	8zyJiQcSCrTg5H
56	aaf4221703ab4295492f38a2764cd79f	NY	R0003	1900-01-01	2199-12-31	1	N	aKzeeht9E	aKzeeht9E	aKzeeht9E	aKzeeht9E	aKzeeht9E	aKzeeht9E	aKzeeht9E
57	90caf35f1d158589338b1df316c587c4	NY	R0003	1900-01-01	2199-12-31	1	N	Le	Le	Le	Le	Le	Le	Le
58	b23cd46be434208747b144c4d0e32094	NY	R0003	1900-01-01	2199-12-31	1	N	D6OsMGC3sUXby0e60	D6OsMGC3sUXby0e60	D6OsMGC3sUXby0e60	D6OsMGC3sUXby0e60	D6OsMGC3sUXby0e60	D6OsMGC3sUXby0e60	D6OsMGC3sUXby0e60
59	e562038cefc6b4667c1ba3cbfdebeaf4	NY	R0003	1900-01-01	2199-12-31	1	N	VGQWB6vNRZh4xhV4VF	VGQWB6vNRZh4xhV4VF	VGQWB6vNRZh4xhV4VF	VGQWB6vNRZh4xhV4VF	VGQWB6vNRZh4xhV4VF	VGQWB6vNRZh4xhV4VF	VGQWB6vNRZh4xhV4VF
60	2a1ff19a03cfd10f3416fd35450d210d	NY	R0003	1900-01-01	2199-12-31	1	N	PemxLX8i5cOCkNKhO	PemxLX8i5cOCkNKhO	PemxLX8i5cOCkNKhO	PemxLX8i5cOCkNKhO	PemxLX8i5cOCkNKhO	PemxLX8i5cOCkNKhO	PemxLX8i5cOCkNKhO
61	dea8cf9cad74cde3d8f87afa69b78b9d	NY	R0003	1900-01-01	2199-12-31	1	N	W	W	W	W	W	W	W
62	74f1dcacce73d94e6ce0ae0cfaa46752	NY	R0003	1900-01-01	2199-12-31	1	N	dxQLUs	dxQLUs	dxQLUs	dxQLUs	dxQLUs	dxQLUs	dxQLUs
63	629f631c3f69471343e9b9fe4aa58e85	NY	R0003	1900-01-01	2199-12-31	1	N	WKrYyOXjojbf0o8kl	WKrYyOXjojbf0o8kl	WKrYyOXjojbf0o8kl	WKrYyOXjojbf0o8kl	WKrYyOXjojbf0o8kl	WKrYyOXjojbf0o8kl	WKrYyOXjojbf0o8kl
64	2ef94f8015443cbb1cd712227a378c9d	NY	R0003	1900-01-01	2199-12-31	1	N	qx	qx	qx	qx	qx	qx	qx
65	6ecdab8f87051b7491340f410c4ccc4d	NY	R0003	1900-01-01	2199-12-31	1	N	JPLuCUHgPC7B	JPLuCUHgPC7B	JPLuCUHgPC7B	JPLuCUHgPC7B	JPLuCUHgPC7B	JPLuCUHgPC7B	JPLuCUHgPC7B
66	1c9a69b15f2a8703fa42fa9c09d33ae3	NY	R0003	1900-01-01	2199-12-31	1	N	aHF7OVhLeElfq	aHF7OVhLeElfq	aHF7OVhLeElfq	aHF7OVhLeElfq	aHF7OVhLeElfq	aHF7OVhLeElfq	aHF7OVhLeElfq
67	3424eaa341c91b40ca29a1f19a70d0f0	NY	R0003	1900-01-01	2199-12-31	1	N	rtURxgiDqHd5TD	rtURxgiDqHd5TD	rtURxgiDqHd5TD	rtURxgiDqHd5TD	rtURxgiDqHd5TD	rtURxgiDqHd5TD	rtURxgiDqHd5TD
68	bfd353528a0f605c51fb318b7ecf6195	NY	R0003	1900-01-01	2199-12-31	1	N	1gqQM8R2ELM1W	1gqQM8R2ELM1W	1gqQM8R2ELM1W	1gqQM8R2ELM1W	1gqQM8R2ELM1W	1gqQM8R2ELM1W	1gqQM8R2ELM1W
69	13ffd002801c01ad6dae0ca6659a2d47	NY	R0003	1900-01-01	2199-12-31	1	N	gelByZdArm	gelByZdArm	gelByZdArm	gelByZdArm	gelByZdArm	gelByZdArm	gelByZdArm
70	ac9e1f732537d70c00e5850fd4f98dbb	NY	R0003	1900-01-01	2199-12-31	1	N	837	837	837	837	837	837	837
71	d7cfab79c80de7038218bd68d2d69966	NY	R0001	1900-01-01	2199-12-31	1	N	dyDjumLSDXXCb0W	dyDjumLSDXXCb0W	dyDjumLSDXXCb0W	dyDjumLSDXXCb0W	dyDjumLSDXXCb0W	dyDjumLSDXXCb0W	dyDjumLSDXXCb0W
72	f8b875c0c898dc8e31917249c01f249b	NY	R0002	1900-01-01	2199-12-31	1	N	0HiUbZx60MdcK036GyKn	0HiUbZx60MdcK036GyKn	0HiUbZx60MdcK036GyKn	0HiUbZx60MdcK036GyKn	0HiUbZx60MdcK036GyKn	0HiUbZx60MdcK036GyKn	0HiUbZx60MdcK036GyKn
73	4af05a2aafb3271641a9565948ba5ddf	NY	R0001	1900-01-01	2199-12-31	1	N	FzI	FzI	FzI	FzI	FzI	FzI	FzI
74	b2c3502e29b4623dd7cece19d3fc1ff5	NY	R0002	1900-01-01	2199-12-31	1	N	Rx3hHFwPUfma3ln	Rx3hHFwPUfma3ln	Rx3hHFwPUfma3ln	Rx3hHFwPUfma3ln	Rx3hHFwPUfma3ln	Rx3hHFwPUfma3ln	Rx3hHFwPUfma3ln
75	f2d359f017ba856e913e1ca4e9db8a50	NY	R0001	1900-01-01	2199-12-31	1	N	sDW3tntyGwrqMVwTL7Yr	sDW3tntyGwrqMVwTL7Yr	sDW3tntyGwrqMVwTL7Yr	sDW3tntyGwrqMVwTL7Yr	sDW3tntyGwrqMVwTL7Yr	sDW3tntyGwrqMVwTL7Yr	sDW3tntyGwrqMVwTL7Yr
76	6b9e72e51945329710b8af726a0dd222	NY	R0003	1900-01-01	2199-12-31	1	N	EFK3uCDr	EFK3uCDr	EFK3uCDr	EFK3uCDr	EFK3uCDr	EFK3uCDr	EFK3uCDr
77	22b5293f767eeb9a0ddb1c70432ca158	NY	R0001	1900-01-01	2199-12-31	1	N	Wd03DTPLXjY4	Wd03DTPLXjY4	Wd03DTPLXjY4	Wd03DTPLXjY4	Wd03DTPLXjY4	Wd03DTPLXjY4	Wd03DTPLXjY4
78	dc06ffee4bbe069b32c5a332423880a7	NY	R0003	1900-01-01	2199-12-31	1	N	91a5	91a5	91a5	91a5	91a5	91a5	91a5
79	ff9c505f7951004dc0846fb883ccebdc	NY	R0001	1900-01-01	2199-12-31	1	N	NADBOT1uvz5R	NADBOT1uvz5R	NADBOT1uvz5R	NADBOT1uvz5R	NADBOT1uvz5R	NADBOT1uvz5R	NADBOT1uvz5R
80	9627ee84cc03de1e6975c3c5d435bb13	NY	R0003	1900-01-01	2199-12-31	1	N	hvLjm	hvLjm	hvLjm	hvLjm	hvLjm	hvLjm	hvLjm
81	685b61fd8d327f8b5246159dd6511b9a	NY	R0002	1900-01-01	2199-12-31	1	N	Dak7I0au0HGetJ8ldU9	Dak7I0au0HGetJ8ldU9	Dak7I0au0HGetJ8ldU9	Dak7I0au0HGetJ8ldU9	Dak7I0au0HGetJ8ldU9	Dak7I0au0HGetJ8ldU9	Dak7I0au0HGetJ8ldU9
82	81d8e8062e4ff673755e968ce8fbd249	NY	R0003	1900-01-01	2199-12-31	1	N	b	b	b	b	b	b	b
83	ba61d6ca52737497dc3bddc7a5f00b27	NY	R0002	1900-01-01	2199-12-31	1	N	RltLCEWj8P9nwO	RltLCEWj8P9nwO	RltLCEWj8P9nwO	RltLCEWj8P9nwO	RltLCEWj8P9nwO	RltLCEWj8P9nwO	RltLCEWj8P9nwO
84	7cd1c1629e6f4dc86eb941a4755cde59	NY	R0003	1900-01-01	2199-12-31	1	N	a0AURmkAHWweqOwiOH	a0AURmkAHWweqOwiOH	a0AURmkAHWweqOwiOH	a0AURmkAHWweqOwiOH	a0AURmkAHWweqOwiOH	a0AURmkAHWweqOwiOH	a0AURmkAHWweqOwiOH
85	707ea70536cf001720b3e71de4c47f28	NY	R0002	1900-01-01	2199-12-31	1	N	2R48ezkfuLr21YkRr	2R48ezkfuLr21YkRr	2R48ezkfuLr21YkRr	2R48ezkfuLr21YkRr	2R48ezkfuLr21YkRr	2R48ezkfuLr21YkRr	2R48ezkfuLr21YkRr
86	046d219b50b77ea170fa0602fe910954	NY	R0003	1900-01-01	2199-12-31	1	N	LdOLqMh0IDHixZZ	LdOLqMh0IDHixZZ	LdOLqMh0IDHixZZ	LdOLqMh0IDHixZZ	LdOLqMh0IDHixZZ	LdOLqMh0IDHixZZ	LdOLqMh0IDHixZZ
87	6b183798bb1cfc413a2f102c829fa198	NY	R0003	1900-01-01	2199-12-31	1	N	Kn1dcUynMwWGO6yw	Kn1dcUynMwWGO6yw	Kn1dcUynMwWGO6yw	Kn1dcUynMwWGO6yw	Kn1dcUynMwWGO6yw	Kn1dcUynMwWGO6yw	Kn1dcUynMwWGO6yw
88	2922a8612b3edf4fc87e039ae53bb4d6	NY	R0003	1900-01-01	2199-12-31	1	N	yIBbjZTSidZf	yIBbjZTSidZf	yIBbjZTSidZf	yIBbjZTSidZf	yIBbjZTSidZf	yIBbjZTSidZf	yIBbjZTSidZf
89	1fad025c43d1b4dac09caf0eef4389d0	NY	R0003	1900-01-01	2199-12-31	1	N	SAmjSHZtr0CeCRXFz	SAmjSHZtr0CeCRXFz	SAmjSHZtr0CeCRXFz	SAmjSHZtr0CeCRXFz	SAmjSHZtr0CeCRXFz	SAmjSHZtr0CeCRXFz	SAmjSHZtr0CeCRXFz
90	a91b391b54d208d9443892538a89572c	NY	R0003	1900-01-01	2199-12-31	1	N	gv7WsQJmtXpiAVwhd	gv7WsQJmtXpiAVwhd	gv7WsQJmtXpiAVwhd	gv7WsQJmtXpiAVwhd	gv7WsQJmtXpiAVwhd	gv7WsQJmtXpiAVwhd	gv7WsQJmtXpiAVwhd
91	8715ed8a5f7d5ff6fa603c3dd9a90ad4	NY	R0003	1900-01-01	2199-12-31	1	N	xVGmYF	xVGmYF	xVGmYF	xVGmYF	xVGmYF	xVGmYF	xVGmYF
92	3cb8b33426681d48cdcc4c26913f6cb3	NY	R0003	1900-01-01	2199-12-31	1	N	BdqzqT0LhEzTiIdRbVw	BdqzqT0LhEzTiIdRbVw	BdqzqT0LhEzTiIdRbVw	BdqzqT0LhEzTiIdRbVw	BdqzqT0LhEzTiIdRbVw	BdqzqT0LhEzTiIdRbVw	BdqzqT0LhEzTiIdRbVw
93	b19d8f0772a725549475e609ee165bbf	NY	R0003	1900-01-01	2199-12-31	1	N	0qj4Q7MIVmMeP	0qj4Q7MIVmMeP	0qj4Q7MIVmMeP	0qj4Q7MIVmMeP	0qj4Q7MIVmMeP	0qj4Q7MIVmMeP	0qj4Q7MIVmMeP
94	897932700664fb0dedbdef2de86a198f	NY	R0003	1900-01-01	2199-12-31	1	N	ZZdBBLmmXhKr2Z	ZZdBBLmmXhKr2Z	ZZdBBLmmXhKr2Z	ZZdBBLmmXhKr2Z	ZZdBBLmmXhKr2Z	ZZdBBLmmXhKr2Z	ZZdBBLmmXhKr2Z
95	4077fbc2d0560d7b9d3a66bea138738f	NY	R0003	1900-01-01	2199-12-31	1	N	hGaOD5arW64LWlk2	hGaOD5arW64LWlk2	hGaOD5arW64LWlk2	hGaOD5arW64LWlk2	hGaOD5arW64LWlk2	hGaOD5arW64LWlk2	hGaOD5arW64LWlk2
0	0	N 	UNKNOWN	1900-01-01	2199-12-31	0	NA	NA	NA	NA	NA	NA	NA	NA
\.


--
-- Name: dim_opt_state_data__key_opt_state_data_seq; Type: SEQUENCE SET; Schema: analytics; Owner: edware
--

SELECT pg_catalog.setval('dim_opt_state_data__key_opt_state_data_seq', 95, true);


--
-- Data for Name: dim_poy; Type: TABLE DATA; Schema: analytics; Owner: edware
--

COPY dim_poy (_key_poy, _valid_from, _valid_to, _record_version, poy, end_year, school_year) FROM stdin;
1	1900-01-01	2199-12-31	1	Spring	2014-2015	2014-2015
2	1900-01-01	2199-12-31	1	Spring	2013-2014	2013-2014
\.


--
-- Name: dim_poy__key_poy_seq; Type: SEQUENCE SET; Schema: analytics; Owner: edware
--

SELECT pg_catalog.setval('dim_poy__key_poy_seq', 2, true);


--
-- Data for Name: dim_school; Type: TABLE DATA; Schema: analytics; Owner: edware
--

COPY dim_school (_key_school, _hash_school, state_id, district_id, school_id, _valid_from, _valid_to, _record_version, institution_role, dist_name, school_name, school_year) FROM stdin;
1	8565e3abd61a0d01beabdb39440f8275	NY	R0001	RN001	1900-01-01	2199-12-31	1	RESP	Newport	Rogers High School	2014-2015
2	c209ab8e33c260dc4864fce11607c221	NY	R0002	RE001	1900-01-01	2199-12-31	1	RESP	East Greenwich	East Greenwich High School	2014-2015
3	f06d0fb1c004b0f16ba962bf25caa39c	NY	R0003	RP001	1900-01-01	2199-12-31	1	RESP	Providence	Alvarez High School	2014-2015
4	1076ca2328454967f0e96a37607783e6	NY	R0003	RP002	1900-01-01	2199-12-31	1	RESP	Providence	Mount Pleasant High School	2014-2015
5	061cc1be36132d0fd4045fda2237677e	NY	R0003	RP003	1900-01-01	2199-12-31	1	RESP	Providence	Hope High School	2014-2015
6	d95360f98f6c73cf4bee193ad7b64467	NY	R0003	RP003	1900-01-01	2199-12-31	1	RESP	Providence	Hope High School	2013-2014
0	0	N 	UNKNOWN	UNKNOWN	1900-01-01	2199-12-31	0	NA	NA	NA	NA
\.


--
-- Name: dim_school__key_school_seq; Type: SEQUENCE SET; Schema: analytics; Owner: edware
--

SELECT pg_catalog.setval('dim_school__key_school_seq', 6, true);


--
-- Data for Name: dim_student; Type: TABLE DATA; Schema: analytics; Owner: edware
--

COPY dim_student (_key_student, _hash_student, state_id, district_id, school_id, _valid_from, _valid_to, _record_version, student_parcc_id, student_sex, ethnicity, ethn_hisp_latino, ethn_indian_alaska, ethn_asian, ethn_black, ethn_hawai, ethn_white, ethn_filler, ethn_two_or_more_races, ell, lep_status, gift_talent, migrant_status, econo_disadvantage, disabil_student, primary_disabil_type, student_state_id, student_local_id, student_name, student_dob, student_first_name, student_middle_name, student_last_name, staff_id) FROM stdin;
33	61c04a698500f3b6d2346792d85d2f45	NY	R0003	RP001	1900-01-01	2016-01-13	1	hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ	F	Black or African American	N	N	N	N	N	Y	\N	\N	Y	N	N	N	Y	N	DB	Tvw1NS4wwtZ4tkwb0iVisHEi7Ry715pRRjGtbL1r	mvxrV6SfCFq11d05bN01KupSnan42G0QEJyFwwO5	Forst, Jacquline Zofia	9999-01-01	Jacquline	Zofia	Forst	3c6b9327b0484f5f84741b204a6c9f02208bc1
34	3cbf0e282797dc6f16bc6ca57add4985	NY	R0003	RP001	1900-01-01	2016-01-13	1	zxITiM66QUVOcTyhRaooB0UIOWemGFP8eQE7xPSx	F	Hispanic or Latino	N	N	N	N	N	N	\N	\N	Y	N	Y	N	N	Y	SLI	6oZAVILFTfpG9poJ4qFAJOvw0XYa9suCqZPc2Jkf	UZJGBAKBhDPPLpfVCc225qzGY4J1Ei8vjBnnFwh6	Mccreery, Zofia Keshia	9999-01-01	Zofia	Keshia	Mccreery	5f3bcce2cac34dd38578b9d423801dc3b5caf6
96	3300c36a17c748101008fe4591826fc9	NY	R0003	RP003	1900-01-01	2199-12-31	1	nQXqvAHQ0W1FOGH2IFhG68Aa590JN9bpNQtSXdiw	F	Hispanic or Latino	N	N	N	N	N	N	\N	\N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Augustus, Orval Marisela	0009-02-05	Orval	Marisela	Augustus	0a4061fcdd3f4e6ea8db682199fee2757e8275
42	13052b7a1b3376e95eb5458da3afaa61	NY	R0003	RP003	1900-01-01	2016-01-13	1	6boPVVoO1Vuyg8VJ5AqV0Zha4rVGrZt3uPrM7xgw	F	Native Hawaiian or other Pacific Islander	N	N	N	Y	N	N	\N	\N	Y	N	Y	Y	N	N	EMN	qZL2LXxVKg8Sz8vOKANWLueLW3V5giAyz2jpg4D0	L5wQxkNvAhoDiwvT3jKyOEYyA25RaEL0WVIPOH0Y	Parr, Betty Erasmo	9999-01-01	Betty	Erasmo	Parr	43e0d272a0c245bfb568ee9e944a12c9436f17
43	c4e48fce2c23536a19b6ae7d1e229eb2	NY	R0003	RP003	1900-01-01	2016-01-13	1	KL3R8L1EaO8MgymDcMI59fe7DnjLk0ZuDZiwO2J2	F	Two or more races	N	N	Y	N	N	N	\N	\N	Y	N	Y	Y	Y	Y	AUT	YF1IdV4ztKcjrlJlJkwf1fqIg8yuj2xn9ESdUdsV	WX95D3DMB8lxa2WOd0oFWiUvcMsat1t5gLC8yJTU	Gatti, Sandy Verena	9999-01-01	Sandy	Verena	Gatti	43e0d272a0c245bfb568ee9e944a12c9436f17
99	bf88e1cbb831b7817559f38d8d770066	NY	R0003	RP003	1900-01-01	2199-12-31	1	D3de8PGASi2QhIZ0zHLSNJXZI0okSm1VqPJMeJAw	F	Hispanic or Latino	N	N	N	N	N	N	\N	\N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Matsuura, Roman Marisela	0002-09-05	Roman	Marisela	Matsuura	5f3bcce2cac34dd38578b9d423801dc3b5caf6
100	f6118e311b68b30be553129ae571a44c	NY	R0003	RP003	1900-01-01	2199-12-31	1	CYh72rsxfT7A4jmupxUkrb3S76bTra2BmlgrR57F	F	Hispanic or Latino	N	N	N	N	N	N	\N	\N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Pando, Joella Marisela	0002-09-05	Joella	Marisela	Pando	dcdaa08b9b9046beb3e77419481c860bc6598e
101	f8154922b6d7889a6aaff043d2c91a44	NY	R0003	RP003	1900-01-01	2199-12-31	1	m5i2mSxYFwrFTnW09BEYlrXBvQIw3bTmo2zm1GaI	F	Hispanic or Latino	N	N	N	N	N	N	\N	\N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Perla, Orval Marisela	0002-09-05	Orval	Marisela	Perla	0a4061fcdd3f4e6ea8db682199fee2757e8275
102	95721fc5458b603f970fa554993bc6ec	NY	R0002	RE001	2016-01-13	2199-12-31	2	1SWd4Oky5wnWUNuH078iTM1reO6FjlIYmYkVHcQw	F	Hispanic or Latino	Y	N	N	N	N	N	\N	\N	Y	N	Y	N	Y	Y	HI	MnQn4XYlZppmKAIik2GbO6qo7UUtxyJT8EAQvFod	Kqm3TmmLkcC9V8uszQcM50BuMlfquTl5a9TW363O	Mantle, Darci Roman	0002-09-05	Darci	Roman	Mantle	0a4061fcdd3f4e6ea8db682199fee2757e8275
54	95721fc5458b603f970fa554993bc6ec	NY	R0002	RE001	1900-01-01	2016-01-13	1	1SWd4Oky5wnWUNuH078iTM1reO6FjlIYmYkVHcQw	F	Hispanic or Latino	Y	N	N	N	N	N	\N	\N	Y	N	Y	N	Y	Y	HI	MnQn4XYlZppmKAIik2GbO6qo7UUtxyJT8EAQvFod	Kqm3TmmLkcC9V8uszQcM50BuMlfquTl5a9TW363O	Mantle, Darci Roman	0009-02-05	Darci	Roman	Mantle	0a4061fcdd3f4e6ea8db682199fee2757e8275
103	de70bc7ef176d235816826bfe7768b9e	NY	R0003	RP001	2016-01-13	2199-12-31	2	Z94Y0ynyVyq3VCwLTjIRZHTZSYkFHT0lBN8meOgb	F	Asian	Y	N	N	N	N	N	\N	\N	N	N	Y	Y	Y	Y	SLD	Vqp8InuVS0EmPH5SGuynzzrxnZLVYrRCQRH1EIzQ	N0pp23Z9iEtPwPGjuQXLEiii0xy2ohcTWVkffgjg	Cartlidge, Keshia Vincent	0002-09-05	Keshia	Vincent	Cartlidge	0a4061fcdd3f4e6ea8db682199fee2757e8275
55	de70bc7ef176d235816826bfe7768b9e	NY	R0003	RP001	1900-01-01	2016-01-13	1	Z94Y0ynyVyq3VCwLTjIRZHTZSYkFHT0lBN8meOgb	F	Asian	Y	N	N	N	N	N	\N	\N	N	N	Y	Y	Y	Y	SLD	Vqp8InuVS0EmPH5SGuynzzrxnZLVYrRCQRH1EIzQ	N0pp23Z9iEtPwPGjuQXLEiii0xy2ohcTWVkffgjg	Cartlidge, Keshia Vincent	0009-02-05	Keshia	Vincent	Cartlidge	0a4061fcdd3f4e6ea8db682199fee2757e8275
104	74dd7ccd0740cf7785996ed75b98ce6b	NY	R0003	RP001	2016-01-13	2199-12-31	2	maHNc5BQaVj9ddANVpBvFluBikLI3H06gqH3P1zi	F	Black or African American	N	N	N	Y	N	N	\N	\N	Y	Y	Y	Y	N	Y	SLI	NuR72lVSYLmdau6elNP0mugSv2GnuJVHM6MKDPGb	WapHLkZfW6VL8yTizh6k8TM6YWe4SImeCUW5u4Rz	Carriere, Vincent Kimberely	0002-09-05	Vincent	Kimberely	Carriere	0a4061fcdd3f4e6ea8db682199fee2757e8275
88	1ec26c48aeab93aa7ea050b3a39e1891	NY	R0003	RP002	1900-01-01	2016-01-13	1	loKBQQ7EnGEJKKoK39nYLCNWtoJIDC2yNW9cPcmh	F	Hispanic or Latino	N	N	N	N	N	Y	\N	\N	N	N	Y	N	Y	N	SLI	x1Gi3Q1cKlhWvsthoxJSHWSnAsFkXOiW8VJivQ6e	LAkGZnolk1llJQO3UvrPZ8Ze4YveAohGbYjb5edQ	Pedone, Debby Betty	0009-02-05	Debby	Betty	Pedone	0a4061fcdd3f4e6ea8db682199fee2757e8275
56	74dd7ccd0740cf7785996ed75b98ce6b	NY	R0003	RP001	1900-01-01	2016-01-13	1	maHNc5BQaVj9ddANVpBvFluBikLI3H06gqH3P1zi	F	Black or African American	N	N	N	Y	N	N	\N	\N	Y	Y	Y	Y	N	Y	SLI	NuR72lVSYLmdau6elNP0mugSv2GnuJVHM6MKDPGb	WapHLkZfW6VL8yTizh6k8TM6YWe4SImeCUW5u4Rz	Carriere, Vincent Kimberely	0009-02-05	Vincent	Kimberely	Carriere	0a4061fcdd3f4e6ea8db682199fee2757e8275
105	e09f2ef243673b002ee5727c92f1d9fa	NY	R0003	RP001	2016-01-13	2199-12-31	2	E9fHez5Rp8SfdyyhzYgAlyLyIE1JYmpYL8d9Up4W	F	Hispanic or Latino	N	N	N	Y	N	N	\N	\N	Y	Y	N	Y	N	N	ID	zR4uPNZtt7KQSSb1OtijkI8Z3lDaRazHsiO03wGY	OUjRHF0b6p41lL5GwuVzXyXQpk2yqduCxsp3eZ3P	Gillett, Debby Senaida	0002-09-05	Debby	Senaida	Gillett	0a4061fcdd3f4e6ea8db682199fee2757e8275
11	e09f2ef243673b002ee5727c92f1d9fa	NY	R0003	RP001	1900-01-01	2016-01-13	1	E9fHez5Rp8SfdyyhzYgAlyLyIE1JYmpYL8d9Up4W	F	Hispanic or Latino	N	N	N	Y	N	N	\N	\N	Y	Y	N	Y	N	N	ID	zR4uPNZtt7KQSSb1OtijkI8Z3lDaRazHsiO03wGY	OUjRHF0b6p41lL5GwuVzXyXQpk2yqduCxsp3eZ3P	Gillett, Debby Senaida	0009-02-05	Debby	Senaida	Gillett	0a4061fcdd3f4e6ea8db682199fee2757e8275
106	81667f5bdc2bf7a1cfb3d65a80db5d77	NY	R0003	RP001	2016-01-13	2199-12-31	2	r1s4BnsoqFMAxUnLv1q3GbBD9SWWz0yUXPaFp0Nd	M	Asian	N	N	N	N	N	N	\N	\N	Y	Y	Y	Y	Y	N	DD	troNcSA0WmW7Z5fiOOibDVRaR3DYhxpYzHklOou4	DdzkB0orkdmRQVp9nbhO7tbzjspzWKCOfIO788v0	Cabello, Kimberely Chase	0002-09-05	Kimberely	Chase	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275
57	81667f5bdc2bf7a1cfb3d65a80db5d77	NY	R0003	RP001	1900-01-01	2016-01-13	1	r1s4BnsoqFMAxUnLv1q3GbBD9SWWz0yUXPaFp0Nd	M	Asian	N	N	N	N	N	N	\N	\N	Y	Y	Y	Y	Y	N	DD	troNcSA0WmW7Z5fiOOibDVRaR3DYhxpYzHklOou4	DdzkB0orkdmRQVp9nbhO7tbzjspzWKCOfIO788v0	Cabello, Kimberely Chase	0009-02-05	Kimberely	Chase	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275
107	b362886e53d817a48af5d884485133b1	NY	R0003	RP001	2016-01-13	2199-12-31	2	haGegI5PgD6OzaQ1Mu478Og0zw693ZmLAgQTbjLb	F	Black or African American	N	N	Y	N	N	N	\N	\N	N	N	N	N	N	Y	MD	ECFaO3SYBBsvZJk11tbA0G4gSO1mOKoTq7rDRXw1	RJSgbORr4aXV4WGkMDLihE6pHnpbe4tp9JAX56wE	Cabello, Jacquline Orval	0002-09-05	Jacquline	Orval	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275
12	b362886e53d817a48af5d884485133b1	NY	R0003	RP001	1900-01-01	2016-01-13	1	haGegI5PgD6OzaQ1Mu478Og0zw693ZmLAgQTbjLb	F	Black or African American	N	N	Y	N	N	N	\N	\N	N	N	N	N	N	Y	MD	ECFaO3SYBBsvZJk11tbA0G4gSO1mOKoTq7rDRXw1	RJSgbORr4aXV4WGkMDLihE6pHnpbe4tp9JAX56wE	Cabello, Jacquline Orval	0009-02-05	Jacquline	Orval	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275
108	6e7e8c867309f63f8166550026cd310a	NY	R0003	RP001	2016-01-13	2199-12-31	2	Cnt3veDS2HW94jTAnU4bUASdLgIbP9y0lvQpQqer	M	Black or African American	N	N	N	Y	N	N	\N	\N	N	Y	Y	Y	Y	N	SLD	XddKt8voU4Ws8Q3RPZNSJsf8ZMPzzEeI8OM1pxZt	WIpn1enuYlVIe53JejMJqSJSCeKWxc36LGWyZekf	Mcgowin, Cornelius Joella	0002-09-05	Cornelius	Joella	Mcgowin	0a4061fcdd3f4e6ea8db682199fee2757e8275
14	6e7e8c867309f63f8166550026cd310a	NY	R0003	RP001	1900-01-01	2016-01-13	1	Cnt3veDS2HW94jTAnU4bUASdLgIbP9y0lvQpQqer	M	Black or African American	N	N	N	Y	N	N	\N	\N	N	Y	Y	Y	Y	N	SLD	XddKt8voU4Ws8Q3RPZNSJsf8ZMPzzEeI8OM1pxZt	WIpn1enuYlVIe53JejMJqSJSCeKWxc36LGWyZekf	Mcgowin, Cornelius Joella	0009-02-05	Cornelius	Joella	Mcgowin	0a4061fcdd3f4e6ea8db682199fee2757e8275
109	a4acfa175915ad4a67ef607271f9873b	NY	R0003	RP001	2016-01-13	2199-12-31	2	zoxqZgWkKoS9mT7YODBFwUZS48qNkoXzGPZjXUHi	M	Hispanic or Latino	N	N	N	N	N	Y	\N	\N	N	N	Y	Y	N	N	MD	3QSWsRpOpp9qwGRb7174DczCwDiIuk9mrbM0k1qI	sPKaMbmrXE2w80ZQ4SIcRhQNZ1HZeTolaWaDw2k6	Jewell, Betty Senaida	0002-09-05	Betty	Senaida	Jewell	0a4061fcdd3f4e6ea8db682199fee2757e8275
58	a4acfa175915ad4a67ef607271f9873b	NY	R0003	RP001	1900-01-01	2016-01-13	1	zoxqZgWkKoS9mT7YODBFwUZS48qNkoXzGPZjXUHi	M	Hispanic or Latino	N	N	N	N	N	Y	\N	\N	N	N	Y	Y	N	N	MD	3QSWsRpOpp9qwGRb7174DczCwDiIuk9mrbM0k1qI	sPKaMbmrXE2w80ZQ4SIcRhQNZ1HZeTolaWaDw2k6	Jewell, Betty Senaida	0009-02-05	Betty	Senaida	Jewell	0a4061fcdd3f4e6ea8db682199fee2757e8275
110	66cc01d74edbdf6113bb98a7d13d8d80	NY	R0003	RP001	2016-01-13	2199-12-31	2	U49Ivq4Y5D2NTB0EAuHv0sKhVA90oVkCwOgJErDW	M	Asian	N	N	N	N	N	Y	\N	\N	N	Y	N	Y	Y	Y	SLD	upsehex48pDKkcKQZC0mj8VAI6H7uFKlfpnxQRhz	Aax8ihQ82nR88np1G1SxRB7uYY74BtDGIJkW40Us	Mcgowin, Betty Vincent	0002-09-05	Betty	Vincent	Mcgowin	0a4061fcdd3f4e6ea8db682199fee2757e8275
13	66cc01d74edbdf6113bb98a7d13d8d80	NY	R0003	RP001	1900-01-01	2016-01-13	1	U49Ivq4Y5D2NTB0EAuHv0sKhVA90oVkCwOgJErDW	M	Asian	N	N	N	N	N	Y	\N	\N	N	Y	N	Y	Y	Y	SLD	upsehex48pDKkcKQZC0mj8VAI6H7uFKlfpnxQRhz	Aax8ihQ82nR88np1G1SxRB7uYY74BtDGIJkW40Us	Mcgowin, Betty Vincent	0009-02-05	Betty	Vincent	Mcgowin	0a4061fcdd3f4e6ea8db682199fee2757e8275
111	f8b76b0e5993f62d41fa1dd2b4abb4f3	NY	R0003	RP001	2016-01-13	2199-12-31	2	wD6y2AQPgV3HtqM23RFtegCe56oenrPa6DdbElzI	F	Hispanic or Latino	N	N	N	N	N	Y	\N	\N	Y	N	N	Y	N	N	VI	zXNJatxmXL0LbQYd3ANwEsjGbQkCOIeoy0B5vVXG	pSU02h0m7dKGePY7gH3JrcROBfeTqXMoVJAqxvT0	Carriere, Erasmo Orval	0002-09-05	Erasmo	Orval	Carriere	0a4061fcdd3f4e6ea8db682199fee2757e8275
77	f8b76b0e5993f62d41fa1dd2b4abb4f3	NY	R0003	RP001	1900-01-01	2016-01-13	1	wD6y2AQPgV3HtqM23RFtegCe56oenrPa6DdbElzI	F	Hispanic or Latino	N	N	N	N	N	Y	\N	\N	Y	N	N	Y	N	N	VI	zXNJatxmXL0LbQYd3ANwEsjGbQkCOIeoy0B5vVXG	pSU02h0m7dKGePY7gH3JrcROBfeTqXMoVJAqxvT0	Carriere, Erasmo Orval	0009-02-05	Erasmo	Orval	Carriere	0a4061fcdd3f4e6ea8db682199fee2757e8275
112	6b37acda81a260e816e66239c20a74db	NY	R0001	RN001	2016-01-13	2199-12-31	2	RQaWFAiKM91B9AbbwRXpJzOo302G4qKWtzbpRano	F	White	Y	N	N	N	N	N	\N	\N	Y	Y	N	Y	N	Y	HI	fnBllhnGFCbGz3Ifmcapa13xiR6NaWJlqqbGO6iD	9BKPvX6AfXTVdEXx473rHEXEPeMfxkAmYRJgq5lI	Perla, Thurman Betty	0002-09-05	Thurman	Betty	Perla	0a4061fcdd3f4e6ea8db682199fee2757e8275
72	6b37acda81a260e816e66239c20a74db	NY	R0001	RN001	1900-01-01	2016-01-13	1	RQaWFAiKM91B9AbbwRXpJzOo302G4qKWtzbpRano	F	White	Y	N	N	N	N	N	\N	\N	Y	Y	N	Y	N	Y	HI	fnBllhnGFCbGz3Ifmcapa13xiR6NaWJlqqbGO6iD	9BKPvX6AfXTVdEXx473rHEXEPeMfxkAmYRJgq5lI	Perla, Thurman Betty	0009-02-05	Thurman	Betty	Perla	0a4061fcdd3f4e6ea8db682199fee2757e8275
113	c159b6b617c53bc35627264df4b58b1f	NY	R0001	RN001	2016-01-13	2199-12-31	2	6DRp6GlmW7bu4tn1PhckfVhL9MHH85oXIaSfRJep	M	Native Hawaiian or other Pacific Islander	N	N	N	Y	N	N	\N	\N	Y	Y	Y	Y	Y	N	SLI	n1Yqb9qw1NhnALHkEpjpkyqea3XGgt6UxI0mqNDp	uZhrK9csJYrSv4OHgyLwMmaLWRMpeuVirwLiR6yi	Flavors, Arnette Thurman	0002-09-05	Arnette	Thurman	Flavors	0a4061fcdd3f4e6ea8db682199fee2757e8275
74	c159b6b617c53bc35627264df4b58b1f	NY	R0001	RN001	1900-01-01	2016-01-13	1	6DRp6GlmW7bu4tn1PhckfVhL9MHH85oXIaSfRJep	M	Native Hawaiian or other Pacific Islander	N	N	N	Y	N	N	\N	\N	Y	Y	Y	Y	Y	N	SLI	n1Yqb9qw1NhnALHkEpjpkyqea3XGgt6UxI0mqNDp	uZhrK9csJYrSv4OHgyLwMmaLWRMpeuVirwLiR6yi	Flavors, Arnette Thurman	0009-02-05	Arnette	Thurman	Flavors	0a4061fcdd3f4e6ea8db682199fee2757e8275
114	e379af3e84e3580ddd7a742e40db7740	NY	R0001	RN001	2016-01-13	2199-12-31	2	IWr9RrKwQAe5iXf7W8xFiTf4fxYeWjWvoRJB7s45	M	Two or more races	N	N	N	Y	N	N	\N	\N	N	Y	Y	N	N	N	OHI	kh2OFlFgeEwhUf8by4js3JylAc5mcBS6vYVPJDXh	Fn2wBPPV04m38Cn50laxjbqYZqYpnghFjHRfP9ev	Perla, Roman Debby	0002-09-05	Roman	Debby	Perla	0a4061fcdd3f4e6ea8db682199fee2757e8275
76	e379af3e84e3580ddd7a742e40db7740	NY	R0001	RN001	1900-01-01	2016-01-13	1	IWr9RrKwQAe5iXf7W8xFiTf4fxYeWjWvoRJB7s45	M	Two or more races	N	N	N	Y	N	N	\N	\N	N	Y	Y	N	N	N	OHI	kh2OFlFgeEwhUf8by4js3JylAc5mcBS6vYVPJDXh	Fn2wBPPV04m38Cn50laxjbqYZqYpnghFjHRfP9ev	Perla, Roman Debby	0009-02-05	Roman	Debby	Perla	0a4061fcdd3f4e6ea8db682199fee2757e8275
115	554a604225114a5a52a868df68119c35	NY	R0001	RN001	2016-01-13	2199-12-31	2	p42AqWSux32Vo3RQzM34aioB8DbTdzDqfLRsAnhF	F	Hispanic or Latino	Y	N	N	N	N	N	\N	\N	N	Y	Y	N	N	Y	ID	ktBL9v7hQF8A4LKbVnPxbQlILxTAm7G9BKGNXNdj	XeUALaoza0bC7hYQ4UFDNhJlLD3QeO0lSJAe23o0	Lawlor, Devon Jacquline	0002-09-05	Devon	Jacquline	Lawlor	0a4061fcdd3f4e6ea8db682199fee2757e8275
78	554a604225114a5a52a868df68119c35	NY	R0001	RN001	1900-01-01	2016-01-13	1	p42AqWSux32Vo3RQzM34aioB8DbTdzDqfLRsAnhF	F	Hispanic or Latino	Y	N	N	N	N	N	\N	\N	N	Y	Y	N	N	Y	ID	ktBL9v7hQF8A4LKbVnPxbQlILxTAm7G9BKGNXNdj	XeUALaoza0bC7hYQ4UFDNhJlLD3QeO0lSJAe23o0	Lawlor, Devon Jacquline	0009-02-05	Devon	Jacquline	Lawlor	0a4061fcdd3f4e6ea8db682199fee2757e8275
116	38092508961eea20ad1fcfe30a40653e	NY	R0003	RP001	2016-01-13	2199-12-31	2	sYbJYuJUBNDgRcwDUOr3LhFnvsw424uSYhmE0rx7	M	Hispanic or Latino	N	N	Y	N	N	N	\N	\N	N	N	Y	N	N	Y	EMN	dqp9Tw8rMvkHeZpVEkNuZJlmwtPBZM0tsrjCSGSw	QjJfMmSIbCyPW4EDF8d69mZeFxubIylIGkMteFkr	Cabello, Roman Lois	0002-09-05	Roman	Lois	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275
60	38092508961eea20ad1fcfe30a40653e	NY	R0003	RP001	1900-01-01	2016-01-13	1	sYbJYuJUBNDgRcwDUOr3LhFnvsw424uSYhmE0rx7	M	Hispanic or Latino	N	N	Y	N	N	N	\N	\N	N	N	Y	N	N	Y	EMN	dqp9Tw8rMvkHeZpVEkNuZJlmwtPBZM0tsrjCSGSw	QjJfMmSIbCyPW4EDF8d69mZeFxubIylIGkMteFkr	Cabello, Roman Lois	0009-02-05	Roman	Lois	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275
117	5fa4fe485eaac732eeec3b83c621c9e4	NY	R0003	RP002	2016-01-13	2199-12-31	2	GuxIDl2Fjyjs81hW6n9gmufC4ruRxwcV8DCuKqbi	F	Native Hawaiian or other Pacific Islander	N	N	N	N	N	Y	\N	\N	Y	N	Y	Y	Y	Y	OI	li2nUdkotubCxOOEsm1b2Av0kZdGmPlcLWtH0vea	0UHiuCA5N0TcBPrjTDu2BiBF5MzwGq8XL4DOOqAf	Pane, Debby Zofia	0002-09-05	Debby	Zofia	Pane	0a4061fcdd3f4e6ea8db682199fee2757e8275
61	5fa4fe485eaac732eeec3b83c621c9e4	NY	R0003	RP002	1900-01-01	2016-01-13	1	GuxIDl2Fjyjs81hW6n9gmufC4ruRxwcV8DCuKqbi	F	Native Hawaiian or other Pacific Islander	N	N	N	N	N	Y	\N	\N	Y	N	Y	Y	Y	Y	OI	li2nUdkotubCxOOEsm1b2Av0kZdGmPlcLWtH0vea	0UHiuCA5N0TcBPrjTDu2BiBF5MzwGq8XL4DOOqAf	Pane, Debby Zofia	0009-02-05	Debby	Zofia	Pane	0a4061fcdd3f4e6ea8db682199fee2757e8275
118	4560baa5373fc196d557510317120454	NY	R0003	RP002	2016-01-13	2199-12-31	2	k1kUAyTitX2QYeasKtOPdOxbPCSIet5I00G6vQON	F	White	N	N	N	Y	N	N	\N	\N	N	Y	Y	N	N	N	SLD	t8ivqpZI9goOjdx2lezvRQdwlupLushf7wh9ofib	s8R7rPtONccd6m7XMWFxyQzoMTa1UqvZoScl6wLI	Ung, Verena Vincent	0002-09-05	Verena	Vincent	Ung	0a4061fcdd3f4e6ea8db682199fee2757e8275
62	4560baa5373fc196d557510317120454	NY	R0003	RP002	1900-01-01	2016-01-13	1	k1kUAyTitX2QYeasKtOPdOxbPCSIet5I00G6vQON	F	White	N	N	N	Y	N	N	\N	\N	N	Y	Y	N	N	N	SLD	t8ivqpZI9goOjdx2lezvRQdwlupLushf7wh9ofib	s8R7rPtONccd6m7XMWFxyQzoMTa1UqvZoScl6wLI	Ung, Verena Vincent	0009-02-05	Verena	Vincent	Ung	0a4061fcdd3f4e6ea8db682199fee2757e8275
119	f1d393c4aa2f7521cf391cecd13500da	NY	R0003	RP002	2016-01-13	2199-12-31	2	OjeIJyOY5Qyogd8ZPoAT4Lj6E1LmudgZcIrEeAvb	M	Two or more races	Y	N	N	N	N	N	\N	\N	N	N	Y	N	N	Y	DB	jdcbqSVD8gl67mTC7aGcW7JT0dPnD1lNZTzyxvQL	Upb3hmWN4WMQI5ZYWOGI72NmBc4e8vTYV4aVmnHp	Gatti, Roman Christin	0002-09-05	Roman	Christin	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275
63	f1d393c4aa2f7521cf391cecd13500da	NY	R0003	RP002	1900-01-01	2016-01-13	1	OjeIJyOY5Qyogd8ZPoAT4Lj6E1LmudgZcIrEeAvb	M	Two or more races	Y	N	N	N	N	N	\N	\N	N	N	Y	N	N	Y	DB	jdcbqSVD8gl67mTC7aGcW7JT0dPnD1lNZTzyxvQL	Upb3hmWN4WMQI5ZYWOGI72NmBc4e8vTYV4aVmnHp	Gatti, Roman Christin	0009-02-05	Roman	Christin	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275
120	feb6249d84f91c2501612ece2667aced	NY	R0003	RP002	2016-01-13	2199-12-31	2	KDGutaOLUnOe1nGxReYkTttxEhi1iLsEL5CE8CPa	M	Two or more races	N	N	N	N	N	Y	\N	\N	Y	N	N	N	N	N	SLD	WLp5Q8kUkjBxoTH4pxHKGU6fAkUl0habx6kAsiwz	2GhcKHiErNzGJsOm9FDKroVSv5ri83VjIXhv4Egp	Heckman, Loni Senaida	0002-09-05	Loni	Senaida	Heckman	0a4061fcdd3f4e6ea8db682199fee2757e8275
64	feb6249d84f91c2501612ece2667aced	NY	R0003	RP002	1900-01-01	2016-01-13	1	KDGutaOLUnOe1nGxReYkTttxEhi1iLsEL5CE8CPa	M	Two or more races	N	N	N	N	N	Y	\N	\N	Y	N	N	N	N	N	SLD	WLp5Q8kUkjBxoTH4pxHKGU6fAkUl0habx6kAsiwz	2GhcKHiErNzGJsOm9FDKroVSv5ri83VjIXhv4Egp	Heckman, Loni Senaida	0009-02-05	Loni	Senaida	Heckman	0a4061fcdd3f4e6ea8db682199fee2757e8275
121	98244181d65cd41134bfef4702b4d51d	NY	R0003	RP002	2016-01-13	2199-12-31	2	p2Sf5xBg8rAuTjy9XPGuFJaoYhdKIHISnVuRqoUd	M	American Indian or Alaska Native	Y	N	N	N	N	N	\N	\N	Y	Y	Y	N	N	Y	EMN	aSSWKV1Drm6iwJED1qSv44z7HA8TCeXRrwcNoWVm	MePGxAdmGOsSQaCHZEDqxbcD3VCOFLTwlyCgcVrL	Buckwalter, Shane Autumn	0002-09-05	Shane	Autumn	Buckwalter	0a4061fcdd3f4e6ea8db682199fee2757e8275
15	98244181d65cd41134bfef4702b4d51d	NY	R0003	RP002	1900-01-01	2016-01-13	1	p2Sf5xBg8rAuTjy9XPGuFJaoYhdKIHISnVuRqoUd	M	American Indian or Alaska Native	Y	N	N	N	N	N	\N	\N	Y	Y	Y	N	N	Y	EMN	aSSWKV1Drm6iwJED1qSv44z7HA8TCeXRrwcNoWVm	MePGxAdmGOsSQaCHZEDqxbcD3VCOFLTwlyCgcVrL	Buckwalter, Shane Autumn	0009-02-05	Shane	Autumn	Buckwalter	0a4061fcdd3f4e6ea8db682199fee2757e8275
122	a46a4035909434471c1db9ceb7c2acc5	NY	R0003	RP002	2016-01-13	2199-12-31	2	sEorUakkZr2QwQgeQ1wCHxo8NwkksVvRlPXQV7cL	F	Asian	N	N	N	N	N	Y	\N	\N	Y	N	Y	Y	Y	Y	SLD	ajzDzGnYVOFZLxW64MxyWOCwj5ic8JqCYaeSSXQC	sOAfqeN7P0mdsSSlvrhX8tGJTyhKVtWE6ulKuGV3	Cartlidge, Joella Cornelius	0002-09-05	Joella	Cornelius	Cartlidge	0a4061fcdd3f4e6ea8db682199fee2757e8275
16	a46a4035909434471c1db9ceb7c2acc5	NY	R0003	RP002	1900-01-01	2016-01-13	1	sEorUakkZr2QwQgeQ1wCHxo8NwkksVvRlPXQV7cL	F	Asian	N	N	N	N	N	Y	\N	\N	Y	N	Y	Y	Y	Y	SLD	ajzDzGnYVOFZLxW64MxyWOCwj5ic8JqCYaeSSXQC	sOAfqeN7P0mdsSSlvrhX8tGJTyhKVtWE6ulKuGV3	Cartlidge, Joella Cornelius	0009-02-05	Joella	Cornelius	Cartlidge	0a4061fcdd3f4e6ea8db682199fee2757e8275
123	534784e43098b35a643e57f47288c6f5	NY	R0003	RP002	2016-01-13	2199-12-31	2	Sc0cLMiRhBfkJsHF7Dwz1ozIPzc1k3s6aOgAD2IT	F	Black or African American	N	N	N	N	N	Y	\N	\N	Y	N	Y	Y	Y	N	SLI	vHUc6IqR2eA7oqn479WZO8P0v6bm0qzaqNYBXqro	UYuxGmofxNxWDnuXTWFNS4rviLEy7CTfdApVoVzU	Monks, Joella Erasmo	0002-09-05	Joella	Erasmo	Monks	0a4061fcdd3f4e6ea8db682199fee2757e8275
17	534784e43098b35a643e57f47288c6f5	NY	R0003	RP002	1900-01-01	2016-01-13	1	Sc0cLMiRhBfkJsHF7Dwz1ozIPzc1k3s6aOgAD2IT	F	Black or African American	N	N	N	N	N	Y	\N	\N	Y	N	Y	Y	Y	N	SLI	vHUc6IqR2eA7oqn479WZO8P0v6bm0qzaqNYBXqro	UYuxGmofxNxWDnuXTWFNS4rviLEy7CTfdApVoVzU	Monks, Joella Erasmo	0009-02-05	Joella	Erasmo	Monks	0a4061fcdd3f4e6ea8db682199fee2757e8275
124	1ec26c48aeab93aa7ea050b3a39e1891	NY	R0003	RP002	2016-01-13	2199-12-31	2	loKBQQ7EnGEJKKoK39nYLCNWtoJIDC2yNW9cPcmh	F	Hispanic or Latino	N	N	N	N	N	Y	\N	\N	N	N	Y	N	Y	N	SLI	x1Gi3Q1cKlhWvsthoxJSHWSnAsFkXOiW8VJivQ6e	LAkGZnolk1llJQO3UvrPZ8Ze4YveAohGbYjb5edQ	Pedone, Debby Betty	0002-09-05	Debby	Betty	Pedone	0a4061fcdd3f4e6ea8db682199fee2757e8275
125	6dba96c9c966d3bb891a9a9cd78957cb	NY	R0003	RP002	2016-01-13	2199-12-31	2	ZmrZFBiOmI2H2QuEsZm59MMu7bIAhKHUjge92f6E	M	White	N	N	N	N	N	Y	\N	\N	Y	Y	Y	N	N	Y	AUT	swmmMy3yINRax8X2lZgSXb49caxsdhJrC5SGGO4L	UbYZjnIcBk3y9qIpq7QvOusuqcc7Iw0s6S0z4281	Matsuura, Arnette Sandy	0002-09-05	Arnette	Sandy	Matsuura	0a4061fcdd3f4e6ea8db682199fee2757e8275
18	6dba96c9c966d3bb891a9a9cd78957cb	NY	R0003	RP002	1900-01-01	2016-01-13	1	ZmrZFBiOmI2H2QuEsZm59MMu7bIAhKHUjge92f6E	M	White	N	N	N	N	N	Y	\N	\N	Y	Y	Y	N	N	Y	AUT	swmmMy3yINRax8X2lZgSXb49caxsdhJrC5SGGO4L	UbYZjnIcBk3y9qIpq7QvOusuqcc7Iw0s6S0z4281	Matsuura, Arnette Sandy	0009-02-05	Arnette	Sandy	Matsuura	0a4061fcdd3f4e6ea8db682199fee2757e8275
126	c291ad0d70cf66f37ed40bba3c27a0a3	NY	R0003	RP001	2016-01-13	2199-12-31	2	JRAWX24sIH3Bh2KcmpYNn6ZyGr8e6p6ctkAMFRw5	F	Asian	N	N	N	N	N	Y	\N	\N	Y	Y	Y	Y	N	N	MD	VxchSUMcdsVWHvFbAU1dUZQHymEGClKRHOucFsCD	UusU7BtgH5knogqWxkGqKMssirKkeuWZIcfN9ljv	Gatti, Shane Senaida	0002-09-05	Shane	Senaida	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275
79	c291ad0d70cf66f37ed40bba3c27a0a3	NY	R0003	RP001	1900-01-01	2016-01-13	1	JRAWX24sIH3Bh2KcmpYNn6ZyGr8e6p6ctkAMFRw5	F	Asian	N	N	N	N	N	Y	\N	\N	Y	Y	Y	Y	N	N	MD	VxchSUMcdsVWHvFbAU1dUZQHymEGClKRHOucFsCD	UusU7BtgH5knogqWxkGqKMssirKkeuWZIcfN9ljv	Gatti, Shane Senaida	0009-02-05	Shane	Senaida	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275
127	61c04a698500f3b6d2346792d85d2f45	NY	R0003	RP001	2016-01-13	2199-12-31	2	hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ	F	Black or African American	N	N	N	N	N	Y	\N	\N	Y	N	N	N	Y	N	DB	Tvw1NS4wwtZ4tkwb0iVisHEi7Ry715pRRjGtbL1r	mvxrV6SfCFq11d05bN01KupSnan42G0QEJyFwwO5	Forst, Jacquline Zofia	0002-09-05	Jacquline	Zofia	Forst	0a4061fcdd3f4e6ea8db682199fee2757e8275
128	3cbf0e282797dc6f16bc6ca57add4985	NY	R0003	RP001	2016-01-13	2199-12-31	2	zxITiM66QUVOcTyhRaooB0UIOWemGFP8eQE7xPSx	F	Hispanic or Latino	N	N	N	N	N	N	\N	\N	Y	N	Y	N	N	Y	SLI	6oZAVILFTfpG9poJ4qFAJOvw0XYa9suCqZPc2Jkf	UZJGBAKBhDPPLpfVCc225qzGY4J1Ei8vjBnnFwh6	Mccreery, Zofia Keshia	0002-09-05	Zofia	Keshia	Mccreery	0a4061fcdd3f4e6ea8db682199fee2757e8275
129	0318c6ed6af7bb05f505ef257596eb97	NY	R0003	RP001	2016-01-13	2199-12-31	2	nHaADPpFPr1tpkcoS2Xj5mhmvdViLrumXuJMSox3	M	White	N	N	N	N	N	Y	\N	\N	Y	Y	N	N	N	Y	SLD	7TgDktADgMDZW4j8udHFtT7QuZo8Qxf0wzTBfd2c	Je3HLTfOW7w8SPm63XuBFsHbXUAaFxz23AljKYpi	Gillett, Betty Orval	0002-09-05	Betty	Orval	Gillett	0a4061fcdd3f4e6ea8db682199fee2757e8275
81	0318c6ed6af7bb05f505ef257596eb97	NY	R0003	RP001	1900-01-01	2016-01-13	1	nHaADPpFPr1tpkcoS2Xj5mhmvdViLrumXuJMSox3	M	White	N	N	N	N	N	Y	\N	\N	Y	Y	N	N	N	Y	SLD	7TgDktADgMDZW4j8udHFtT7QuZo8Qxf0wzTBfd2c	Je3HLTfOW7w8SPm63XuBFsHbXUAaFxz23AljKYpi	Gillett, Betty Orval	0009-02-05	Betty	Orval	Gillett	0a4061fcdd3f4e6ea8db682199fee2757e8275
130	7620d8da9acde5eddd245c63c3ea541d	NY	R0003	RP001	2016-01-13	2199-12-31	2	LaPd7vVIU9G9NvGr44WMWgwNwvupwIyWtC5kGetF	M	Hispanic or Latino	N	N	N	N	N	Y	\N	\N	N	Y	Y	Y	Y	N	SLD	IH43WH7R7yvt5qtr3W3Y66qKyaxQkPM7vfaNyiCO	Ac1iIcJEDgScVsfJn7e61o19LhuxU15Rt8jSY6OT	Troy, Verena Winona	0002-09-05	Verena	Winona	Troy	0a4061fcdd3f4e6ea8db682199fee2757e8275
83	7620d8da9acde5eddd245c63c3ea541d	NY	R0003	RP001	1900-01-01	2016-01-13	1	LaPd7vVIU9G9NvGr44WMWgwNwvupwIyWtC5kGetF	M	Hispanic or Latino	N	N	N	N	N	Y	\N	\N	N	Y	Y	Y	Y	N	SLD	IH43WH7R7yvt5qtr3W3Y66qKyaxQkPM7vfaNyiCO	Ac1iIcJEDgScVsfJn7e61o19LhuxU15Rt8jSY6OT	Troy, Verena Winona	0009-02-05	Verena	Winona	Troy	0a4061fcdd3f4e6ea8db682199fee2757e8275
131	1ef5cb0c6e2e459dfae09639524c3d72	NY	R0003	RP001	2016-01-13	2199-12-31	2	6xduCZW2oSUGSLzpiFpJQQK8YSAzmBrMjAh4WEkn	M	White	Y	N	N	N	N	N	\N	\N	N	N	Y	N	N	N	HI	K0e1FlOXuZVSpMaTr2EUHQLrGDQSoSmZWfuY1MED	nb43B38TZ8h5SZPLTc6GMdXWSZxi8O6xGOUaLZvS	Gatti, Shane Kimberely	0002-09-05	Shane	Kimberely	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275
35	1ef5cb0c6e2e459dfae09639524c3d72	NY	R0003	RP001	1900-01-01	2016-01-13	1	6xduCZW2oSUGSLzpiFpJQQK8YSAzmBrMjAh4WEkn	M	White	Y	N	N	N	N	N	\N	\N	N	N	Y	N	N	N	HI	K0e1FlOXuZVSpMaTr2EUHQLrGDQSoSmZWfuY1MED	nb43B38TZ8h5SZPLTc6GMdXWSZxi8O6xGOUaLZvS	Gatti, Shane Kimberely	9999-01-01	Shane	Kimberely	Gatti	3c6b9327b0484f5f84741b204a6c9f02208bc1
132	2d07847c4ee837b55c26a55dcb45879a	NY	R0003	RP001	2016-01-13	2199-12-31	2	cS2xAK3xDPCBR7ZWbAh9FS08IqjzWxgbZeitAa40	F	White	N	N	N	Y	N	N	\N	\N	N	N	N	Y	Y	N	OI	9eyBPJS9K5X0kdpskmD5HIugskbIZeJ3dSbvlKD1	vwE73aeUtObNKrRIx3Nt1mP4iimz7BQyDjITVYm8	Gillett, Verena Becki	0002-09-05	Verena	Becki	Gillett	0a4061fcdd3f4e6ea8db682199fee2757e8275
85	2d07847c4ee837b55c26a55dcb45879a	NY	R0003	RP001	1900-01-01	2016-01-13	1	cS2xAK3xDPCBR7ZWbAh9FS08IqjzWxgbZeitAa40	F	White	N	N	N	Y	N	N	\N	\N	N	N	N	Y	Y	N	OI	9eyBPJS9K5X0kdpskmD5HIugskbIZeJ3dSbvlKD1	vwE73aeUtObNKrRIx3Nt1mP4iimz7BQyDjITVYm8	Gillett, Verena Becki	0009-02-05	Verena	Becki	Gillett	0a4061fcdd3f4e6ea8db682199fee2757e8275
133	7a29e75af7d6eba2638b9cb88e1b65ce	NY	R0003	RP001	2016-01-13	2199-12-31	2	wUfuK1C1vpevOq8cV6oqQxNNwJV1TbzeraL8fC4v	M	White	N	N	N	N	Y	N	\N	\N	N	Y	N	Y	Y	N	SLD	SYKyRcyh4S0npwfeCEDSXEcNeevVBGyI1cUZ7JBa	QA9WHJjldT89J8LHuhwDFqG4aVG9MKz4pBxkHNKQ	Mccreery, Sandy Devon	0002-09-05	Sandy	Devon	Mccreery	0a4061fcdd3f4e6ea8db682199fee2757e8275
36	7a29e75af7d6eba2638b9cb88e1b65ce	NY	R0003	RP001	1900-01-01	2016-01-13	1	wUfuK1C1vpevOq8cV6oqQxNNwJV1TbzeraL8fC4v	M	White	N	N	N	N	Y	N	\N	\N	N	Y	N	Y	Y	N	SLD	SYKyRcyh4S0npwfeCEDSXEcNeevVBGyI1cUZ7JBa	QA9WHJjldT89J8LHuhwDFqG4aVG9MKz4pBxkHNKQ	Mccreery, Sandy Devon	9999-01-01	Sandy	Devon	Mccreery	3c6b9327b0484f5f84741b204a6c9f02208bc1
134	561025793b3990508b17d3f53b23ea16	NY	R0003	RP001	2016-01-13	2199-12-31	2	UmOkwomla6wQuMu8rde3f3OkVwf23neTGCISpB28	F	Native Hawaiian or other Pacific Islander	Y	N	N	N	N	N	\N	\N	N	Y	Y	N	N	N	EMN	DvEgss4U4elcqiVQXhluwT1sUwSkpzKaEZiX647Z	QCj5a15vfO9wRiN1GSCQi3hYaEDSJQyaSAnHdU12	Cabello, Barbar Senaida	0002-09-05	Barbar	Senaida	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275
87	561025793b3990508b17d3f53b23ea16	NY	R0003	RP001	1900-01-01	2016-01-13	1	UmOkwomla6wQuMu8rde3f3OkVwf23neTGCISpB28	F	Native Hawaiian or other Pacific Islander	Y	N	N	N	N	N	\N	\N	N	Y	Y	N	N	N	EMN	DvEgss4U4elcqiVQXhluwT1sUwSkpzKaEZiX647Z	QCj5a15vfO9wRiN1GSCQi3hYaEDSJQyaSAnHdU12	Cabello, Barbar Senaida	0009-02-05	Barbar	Senaida	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275
135	b657950b3c933670c5fb03993b32bd7a	NY	R0003	RP001	2016-01-13	2199-12-31	2	Avrxze2AmndYm8dxmLJxDxhLsKpRgxaISvx3KS6F	M	Two or more races	Y	N	N	N	N	N	\N	\N	N	Y	Y	N	N	Y	EMN	9H6XnHLSpEfdilMrWU1xrxDGllt0BlK8y25GOAXf	8c6zW5FYxyREirYImWd75l0oh72XUsWtHFmnx7qW	Buckwalter, Keshia Debby	0002-09-05	Keshia	Debby	Buckwalter	0a4061fcdd3f4e6ea8db682199fee2757e8275
59	b657950b3c933670c5fb03993b32bd7a	NY	R0003	RP001	1900-01-01	2016-01-13	1	Avrxze2AmndYm8dxmLJxDxhLsKpRgxaISvx3KS6F	M	Two or more races	Y	N	N	N	N	N	\N	\N	N	Y	Y	N	N	Y	EMN	9H6XnHLSpEfdilMrWU1xrxDGllt0BlK8y25GOAXf	8c6zW5FYxyREirYImWd75l0oh72XUsWtHFmnx7qW	Buckwalter, Keshia Debby	0009-02-05	Keshia	Debby	Buckwalter	0a4061fcdd3f4e6ea8db682199fee2757e8275
136	368ac07435a0414a630b02836d76ecc8	NY	R0003	RP002	2016-01-13	2199-12-31	2	ZHBxgfZVT3KsM8I8iun3007MOqKZxIvz5HZht1dR	F	Native Hawaiian or other Pacific Islander	N	N	N	N	N	N	\N	\N	Y	N	Y	N	Y	Y	DB	ctmnlflHqEhxo8WAg7Q5hXXWMDDlnDUjxSnImvfb	UFUbTvBK41IVxzQEYj710qfAyhHSaq1YVRV9DxD2	Mantle, Kathe Sandy	0002-09-05	Kathe	Sandy	Mantle	0a4061fcdd3f4e6ea8db682199fee2757e8275
89	368ac07435a0414a630b02836d76ecc8	NY	R0003	RP002	1900-01-01	2016-01-13	1	ZHBxgfZVT3KsM8I8iun3007MOqKZxIvz5HZht1dR	F	Native Hawaiian or other Pacific Islander	N	N	N	N	N	N	\N	\N	Y	N	Y	N	Y	Y	DB	ctmnlflHqEhxo8WAg7Q5hXXWMDDlnDUjxSnImvfb	UFUbTvBK41IVxzQEYj710qfAyhHSaq1YVRV9DxD2	Mantle, Kathe Sandy	0009-02-05	Kathe	Sandy	Mantle	0a4061fcdd3f4e6ea8db682199fee2757e8275
137	418bdace6969c485d85e7fd3d7e10a24	NY	R0003	RP002	2016-01-13	2199-12-31	2	4mzU3y7OgIchC8Rz9tfOk7KI8AcXlVtFPVOO1XFH	M	Two or more races	N	N	N	N	N	Y	\N	\N	Y	N	N	Y	N	Y	MD	Vv4pFgpYYUhRET3hz9ODQ5STADstVH3TwhjDME1K	vo8zyU6ApIaNtviaTL0qtWIYIeJAHnijIqVVN3t4	Matsuura, Cornelius Loni	0002-09-05	Cornelius	Loni	Matsuura	0a4061fcdd3f4e6ea8db682199fee2757e8275
90	418bdace6969c485d85e7fd3d7e10a24	NY	R0003	RP002	1900-01-01	2016-01-13	1	4mzU3y7OgIchC8Rz9tfOk7KI8AcXlVtFPVOO1XFH	M	Two or more races	N	N	N	N	N	Y	\N	\N	Y	N	N	Y	N	Y	MD	Vv4pFgpYYUhRET3hz9ODQ5STADstVH3TwhjDME1K	vo8zyU6ApIaNtviaTL0qtWIYIeJAHnijIqVVN3t4	Matsuura, Cornelius Loni	0009-02-05	Cornelius	Loni	Matsuura	0a4061fcdd3f4e6ea8db682199fee2757e8275
138	5f652785e92ef918843dbf68a9cdc92a	NY	R0003	RP002	2016-01-13	2199-12-31	2	iI34kG6uH1mQ83tbh5PEVVKoQjV1r96laDb4x10n	M	American Indian or Alaska Native	N	N	N	N	N	Y	\N	\N	Y	N	Y	Y	N	Y	EMN	76PpHVVXrz4XmEJJCUkNRBPoHtBigoSOtVVTx2Z1	LOoSbNRoiqF7cmOGjcR9Z3KYZRaDkqRx7ygMsygZ	Buckwalter, Autumn Kathe	0002-09-05	Autumn	Kathe	Buckwalter	0a4061fcdd3f4e6ea8db682199fee2757e8275
91	5f652785e92ef918843dbf68a9cdc92a	NY	R0003	RP002	1900-01-01	2016-01-13	1	iI34kG6uH1mQ83tbh5PEVVKoQjV1r96laDb4x10n	M	American Indian or Alaska Native	N	N	N	N	N	Y	\N	\N	Y	N	Y	Y	N	Y	EMN	76PpHVVXrz4XmEJJCUkNRBPoHtBigoSOtVVTx2Z1	LOoSbNRoiqF7cmOGjcR9Z3KYZRaDkqRx7ygMsygZ	Buckwalter, Autumn Kathe	0009-02-05	Autumn	Kathe	Buckwalter	0a4061fcdd3f4e6ea8db682199fee2757e8275
139	636b98f9b58b09cd1c86b8f233d9ad56	NY	R0003	RP002	2016-01-13	2199-12-31	2	QitKZjIhO0SovQYzrO9PfV1o1b0P0xRtKwJwFRqc	F	Black or African American	Y	N	N	N	N	N	\N	\N	N	Y	N	N	Y	N	SLI	CZIdF9IWXP4VxKMRmyhuMH3RrbLuzlUQHcNpeiui	RAwF4ar58LF7U1lAoUhrunhyGSrKowNmH5IHEaVX	Pedone, Winona Darci	0002-09-05	Winona	Darci	Pedone	0a4061fcdd3f4e6ea8db682199fee2757e8275
37	636b98f9b58b09cd1c86b8f233d9ad56	NY	R0003	RP002	1900-01-01	2016-01-13	1	QitKZjIhO0SovQYzrO9PfV1o1b0P0xRtKwJwFRqc	F	Black or African American	Y	N	N	N	N	N	\N	\N	N	Y	N	N	Y	N	SLI	CZIdF9IWXP4VxKMRmyhuMH3RrbLuzlUQHcNpeiui	RAwF4ar58LF7U1lAoUhrunhyGSrKowNmH5IHEaVX	Pedone, Winona Darci	9999-01-01	Winona	Darci	Pedone	343fe3a084b2422796dbd7533a68de13c3cf37
140	2484bff9dd5f01bc272ddd0c11c3461b	NY	R0003	RP002	2016-01-13	2199-12-31	2	4xNcr39DqN64ORBLFyIG322oypneHyCBYVXKZZPm	M	White	N	N	N	N	N	Y	\N	\N	Y	N	N	N	N	N	ID	FEF5lXrReycYbCcr51NDoKDpjMlcswSMK3sBPH82	3Vdt1dUAmoNAKaHVOgELSeBJKEEyAabfHzYv7c78	Benevides, Shane Thurman	0002-09-05	Shane	Thurman	Benevides	0a4061fcdd3f4e6ea8db682199fee2757e8275
65	2484bff9dd5f01bc272ddd0c11c3461b	NY	R0003	RP002	1900-01-01	2016-01-13	1	4xNcr39DqN64ORBLFyIG322oypneHyCBYVXKZZPm	M	White	N	N	N	N	N	Y	\N	\N	Y	N	N	N	N	N	ID	FEF5lXrReycYbCcr51NDoKDpjMlcswSMK3sBPH82	3Vdt1dUAmoNAKaHVOgELSeBJKEEyAabfHzYv7c78	Benevides, Shane Thurman	0009-02-05	Shane	Thurman	Benevides	0a4061fcdd3f4e6ea8db682199fee2757e8275
141	f9f602589e908286160309d6be274b6b	NY	R0003	RP002	2016-01-13	2199-12-31	2	GxZdYAWx8b0sf9CbdWYT8HsXYqBnNikEkDMikPkd	M	Native Hawaiian or other Pacific Islander	N	N	N	N	N	Y	\N	\N	N	N	Y	Y	N	Y	MD	ZZRDwuqMsOWqhqTccuae5rg6ShNIc0OxClU9SJAN	8iThk59GvZdrOTkGLfgjkpY67nUJ9u9qPIUSJmRC	Cabello, Keshia Barbar	0002-09-05	Keshia	Barbar	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275
66	f9f602589e908286160309d6be274b6b	NY	R0003	RP002	1900-01-01	2016-01-13	1	GxZdYAWx8b0sf9CbdWYT8HsXYqBnNikEkDMikPkd	M	Native Hawaiian or other Pacific Islander	N	N	N	N	N	Y	\N	\N	N	N	Y	Y	N	Y	MD	ZZRDwuqMsOWqhqTccuae5rg6ShNIc0OxClU9SJAN	8iThk59GvZdrOTkGLfgjkpY67nUJ9u9qPIUSJmRC	Cabello, Keshia Barbar	0009-02-05	Keshia	Barbar	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275
142	f34f45d243c27f3563e9c23bc3357ec6	NY	R0003	RP002	2016-01-13	2199-12-31	2	sB2VXSoaEMtiSg4WjFUXNNdh3rDSEvULHmGXvAnG	M	Two or more races	N	N	N	N	N	Y	\N	\N	N	Y	Y	N	Y	Y	TBI	eReDWqEHPWW1uS7mUBVoj9Ov4xXm2PSqabTbMIsL	DtZyvWYo4Ht95HPqZeRQjuVIkn83TBbp9kiASvjN	Jewell, Senaida Loni	0002-09-05	Senaida	Loni	Jewell	0a4061fcdd3f4e6ea8db682199fee2757e8275
19	f34f45d243c27f3563e9c23bc3357ec6	NY	R0003	RP002	1900-01-01	2016-01-13	1	sB2VXSoaEMtiSg4WjFUXNNdh3rDSEvULHmGXvAnG	M	Two or more races	N	N	N	N	N	Y	\N	\N	N	Y	Y	N	Y	Y	TBI	eReDWqEHPWW1uS7mUBVoj9Ov4xXm2PSqabTbMIsL	DtZyvWYo4Ht95HPqZeRQjuVIkn83TBbp9kiASvjN	Jewell, Senaida Loni	0009-02-05	Senaida	Loni	Jewell	0a4061fcdd3f4e6ea8db682199fee2757e8275
143	265246e312042e3d65278286de600c5e	NY	R0003	RP002	2016-01-13	2199-12-31	2	c0pT5dPt1QuTicyE2IWo13jRf8kh2ep7ZI8flg9g	F	Asian	N	N	N	Y	N	N	\N	\N	Y	Y	Y	N	Y	Y	EMN	UeDU72FXhOsN87hmLVrvg2axM0ew777BXwUfOMQi	3ZnniDJxEkJJud7n9G1N5inYdTaqvPSqp60jxBHx	Augustus, Betty Senaida	0002-09-05	Betty	Senaida	Augustus	0a4061fcdd3f4e6ea8db682199fee2757e8275
38	265246e312042e3d65278286de600c5e	NY	R0003	RP002	1900-01-01	2016-01-13	1	c0pT5dPt1QuTicyE2IWo13jRf8kh2ep7ZI8flg9g	F	Asian	N	N	N	Y	N	N	\N	\N	Y	Y	Y	N	Y	Y	EMN	UeDU72FXhOsN87hmLVrvg2axM0ew777BXwUfOMQi	3ZnniDJxEkJJud7n9G1N5inYdTaqvPSqp60jxBHx	Augustus, Betty Senaida	9999-01-01	Betty	Senaida	Augustus	5f3bcce2cac34dd38578b9d423801dc3b5caf6
144	ce450e5d18b1b6e09edfd4239ff78a20	NY	R0003	RP003	2016-01-13	2199-12-31	2	urU46yAVop0SQHyO35ugSyxZHrWM1NDDyJY3g4Uz	M	Black or African American	N	N	N	N	N	N	\N	\N	N	Y	Y	N	N	N	DB	5KQ3XwmwcnQ7zvKKJlQFP6cjN3xmBway9UDfImcH	hK7kngeulxuN2vTCqeTXdBTPhmKJ0a7u2pxirjrV	Perla, Autumn Verena	0002-09-05	Autumn	Verena	Perla	0a4061fcdd3f4e6ea8db682199fee2757e8275
67	ce450e5d18b1b6e09edfd4239ff78a20	NY	R0003	RP003	1900-01-01	2016-01-13	1	urU46yAVop0SQHyO35ugSyxZHrWM1NDDyJY3g4Uz	M	Black or African American	N	N	N	N	N	N	\N	\N	N	Y	Y	N	N	N	DB	5KQ3XwmwcnQ7zvKKJlQFP6cjN3xmBway9UDfImcH	hK7kngeulxuN2vTCqeTXdBTPhmKJ0a7u2pxirjrV	Perla, Autumn Verena	0009-02-05	Autumn	Verena	Perla	0a4061fcdd3f4e6ea8db682199fee2757e8275
145	29a3892763c07d4bc7942dcdc7f82ccc	NY	R0003	RP003	2016-01-13	2199-12-31	2	VfwdANZxA7Ak4zCPTWDwK9Zd47uiFTJp4rbBTmMs	M	Hispanic or Latino	N	Y	N	N	N	N	\N	\N	N	N	N	Y	N	N	ID	xRJ2endDR593Ciqb5pcc4pp5P2xgPCYDnPfH6kSK	lONb1ELxhNmgld1Bht9x5ytDZ3QPk3TontWpMcBi	Pando, Darci Joella	0002-09-05	Darci	Joella	Pando	0a4061fcdd3f4e6ea8db682199fee2757e8275
68	29a3892763c07d4bc7942dcdc7f82ccc	NY	R0003	RP003	1900-01-01	2016-01-13	1	VfwdANZxA7Ak4zCPTWDwK9Zd47uiFTJp4rbBTmMs	M	Hispanic or Latino	N	Y	N	N	N	N	\N	\N	N	N	N	Y	N	N	ID	xRJ2endDR593Ciqb5pcc4pp5P2xgPCYDnPfH6kSK	lONb1ELxhNmgld1Bht9x5ytDZ3QPk3TontWpMcBi	Pando, Darci Joella	0009-02-05	Darci	Joella	Pando	0a4061fcdd3f4e6ea8db682199fee2757e8275
146	13139e7138f083ae579a55bf6acd3add	NY	R0003	RP003	2016-01-13	2199-12-31	2	xhHUuTmLdRjylYGiO2ZU54h2J04u9BJSn0exbdX2	M	White	Y	N	N	N	N	N	\N	\N	Y	N	Y	N	N	Y	AUT	XbmxoMlq3d94wmpWt4Fi3CK5vv1hloH2kJD0XDlS	guu3TA086qniuwgGkytIo49LMHk9PGO6EoU09vwY	Augustus, Joella Devon	0002-09-05	Joella	Devon	Augustus	0a4061fcdd3f4e6ea8db682199fee2757e8275
20	13139e7138f083ae579a55bf6acd3add	NY	R0003	RP003	1900-01-01	2016-01-13	1	xhHUuTmLdRjylYGiO2ZU54h2J04u9BJSn0exbdX2	M	White	Y	N	N	N	N	N	\N	\N	Y	N	Y	N	N	Y	AUT	XbmxoMlq3d94wmpWt4Fi3CK5vv1hloH2kJD0XDlS	guu3TA086qniuwgGkytIo49LMHk9PGO6EoU09vwY	Augustus, Joella Devon	0009-02-05	Joella	Devon	Augustus	0a4061fcdd3f4e6ea8db682199fee2757e8275
147	6468877113ecf89d9f9515373c175eb6	NY	R0003	RP003	2016-01-13	2199-12-31	2	8Kjp09Mfair9T4YbPnMaq7t35stJce29GjDxDHzD	F	Native Hawaiian or other Pacific Islander	N	N	N	N	N	Y	\N	\N	Y	N	Y	Y	N	N	SLI	r8jgDU58UHF7RDnN9xrOJIH5Xdh1uR5Lb4ffbUzd	tRdbAcKiyDNrWNbQroDe8FWDFJDgRaGcUUm2gqMp	Heckman, Orval Barbar	0002-09-05	Orval	Barbar	Heckman	0a4061fcdd3f4e6ea8db682199fee2757e8275
69	6468877113ecf89d9f9515373c175eb6	NY	R0003	RP003	1900-01-01	2016-01-13	1	8Kjp09Mfair9T4YbPnMaq7t35stJce29GjDxDHzD	F	Native Hawaiian or other Pacific Islander	N	N	N	N	N	Y	\N	\N	Y	N	Y	Y	N	N	SLI	r8jgDU58UHF7RDnN9xrOJIH5Xdh1uR5Lb4ffbUzd	tRdbAcKiyDNrWNbQroDe8FWDFJDgRaGcUUm2gqMp	Heckman, Orval Barbar	0009-02-05	Orval	Barbar	Heckman	0a4061fcdd3f4e6ea8db682199fee2757e8275
148	3c14eeab39ed4d304c1a24693993f5a4	NY	R0003	RP003	2016-01-13	2199-12-31	2	xro1vzHRQAemvh8tb1iV4F2Yr9Tf5BmZI5F1UzlJ	F	Two or more races	Y	N	N	N	N	N	\N	\N	N	Y	Y	N	N	Y	ID	GGmeXdW19rMZdUjKO7JmNuaDAk5GiejNN9KJvzsS	oaCkXIjHi49EZBU7ZeOtCfuGW9daniTsyw48CL6y	Gatti, Lois Cornelius	0002-09-05	Lois	Cornelius	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275
70	3c14eeab39ed4d304c1a24693993f5a4	NY	R0003	RP003	1900-01-01	2016-01-13	1	xro1vzHRQAemvh8tb1iV4F2Yr9Tf5BmZI5F1UzlJ	F	Two or more races	Y	N	N	N	N	N	\N	\N	N	Y	Y	N	N	Y	ID	GGmeXdW19rMZdUjKO7JmNuaDAk5GiejNN9KJvzsS	oaCkXIjHi49EZBU7ZeOtCfuGW9daniTsyw48CL6y	Gatti, Lois Cornelius	0009-02-05	Lois	Cornelius	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275
149	354b252c24f5621a8073ede1d41a0a58	NY	R0003	RP003	2016-01-13	2199-12-31	2	OLbCzYnv9jHAVbFnfaD1AtFb2FzNSLjhmkjdFdbJ	F	Two or more races	N	N	N	N	N	Y	\N	\N	Y	Y	Y	N	N	Y	MD	ODhXzHmDct2rcJJevvJL96yx8AB6ZB04D2Tp99nA	cIKKR1yH9U4lKU0XNX0teg8iZCsCwpumt2fYGtWu	Neveu, Betty Senaida	0002-09-05	Betty	Senaida	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275
21	354b252c24f5621a8073ede1d41a0a58	NY	R0003	RP003	1900-01-01	2016-01-13	1	OLbCzYnv9jHAVbFnfaD1AtFb2FzNSLjhmkjdFdbJ	F	Two or more races	N	N	N	N	N	Y	\N	\N	Y	Y	Y	N	N	Y	MD	ODhXzHmDct2rcJJevvJL96yx8AB6ZB04D2Tp99nA	cIKKR1yH9U4lKU0XNX0teg8iZCsCwpumt2fYGtWu	Neveu, Betty Senaida	0009-02-05	Betty	Senaida	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275
150	b3285718815b42e49a4ae84ffd936819	NY	R0003	RP003	2016-01-13	2199-12-31	2	8qadBD3xrsvpfExXtJw6dHilQMcdZHjWg60GuV8D	M	Native Hawaiian or other Pacific Islander	N	N	N	N	N	Y	\N	\N	Y	Y	N	Y	Y	Y	SLD	MLHUZp0zt5A8IIbVmPeEhEEUOldtM83bChwva4uA	uxf0AjpWiBmhHs4bZ9XKbyIoXOynyNYi7LlU7ir6	Mccreery, Orval Arnette	0002-09-05	Orval	Arnette	Mccreery	0a4061fcdd3f4e6ea8db682199fee2757e8275
22	b3285718815b42e49a4ae84ffd936819	NY	R0003	RP003	1900-01-01	2016-01-13	1	8qadBD3xrsvpfExXtJw6dHilQMcdZHjWg60GuV8D	M	Native Hawaiian or other Pacific Islander	N	N	N	N	N	Y	\N	\N	Y	Y	N	Y	Y	Y	SLD	MLHUZp0zt5A8IIbVmPeEhEEUOldtM83bChwva4uA	uxf0AjpWiBmhHs4bZ9XKbyIoXOynyNYi7LlU7ir6	Mccreery, Orval Arnette	0009-02-05	Orval	Arnette	Mccreery	0a4061fcdd3f4e6ea8db682199fee2757e8275
151	009e3087bef0f593fac20266c1e11d91	NY	R0003	RP003	2016-01-13	2199-12-31	2	xkO5Umv7TjxiZY8CD926MY27uoz1edQFcaWU76Ef	M	White	N	N	N	N	N	N	\N	\N	N	Y	Y	N	Y	N	TBI	XnZIh0HNVZlrvHTGqktw65fQoXE0vhqx9Fuw6m5c	luBxIsccfBXZHL8L0uVIQbUARWMPF2lmP7Wrphsw	Benevides, Roman Chase	0002-09-05	Roman	Chase	Benevides	0a4061fcdd3f4e6ea8db682199fee2757e8275
92	009e3087bef0f593fac20266c1e11d91	NY	R0003	RP003	1900-01-01	2016-01-13	1	xkO5Umv7TjxiZY8CD926MY27uoz1edQFcaWU76Ef	M	White	N	N	N	N	N	N	\N	\N	N	Y	Y	N	Y	N	TBI	XnZIh0HNVZlrvHTGqktw65fQoXE0vhqx9Fuw6m5c	luBxIsccfBXZHL8L0uVIQbUARWMPF2lmP7Wrphsw	Benevides, Roman Chase	0009-02-05	Roman	Chase	Benevides	0a4061fcdd3f4e6ea8db682199fee2757e8275
152	ab4d7295cb28da0112ffd22b6cc383b1	NY	R0003	RP003	2016-01-13	2199-12-31	2	DSoDwPOg0kZQ5YPnMJ0SLmBaCeGes4a1qjkRUQfd	F	Hispanic or Latino	N	N	N	Y	N	N	\N	\N	N	Y	Y	Y	N	Y	EMN	C4vKy987HmZkDVVIhnTBMCuQbXCqW1Qgq6aj6t1n	wdF0ClC7Z8qvAnYH090pWLDDC1mhQSg1ZqEtV3hW	Pando, Joella Zofia	0002-09-05	Joella	Zofia	Pando	0a4061fcdd3f4e6ea8db682199fee2757e8275
23	ab4d7295cb28da0112ffd22b6cc383b1	NY	R0003	RP003	1900-01-01	2016-01-13	1	DSoDwPOg0kZQ5YPnMJ0SLmBaCeGes4a1qjkRUQfd	F	Hispanic or Latino	N	N	N	Y	N	N	\N	\N	N	Y	Y	Y	N	Y	EMN	C4vKy987HmZkDVVIhnTBMCuQbXCqW1Qgq6aj6t1n	wdF0ClC7Z8qvAnYH090pWLDDC1mhQSg1ZqEtV3hW	Pando, Joella Zofia	0009-02-05	Joella	Zofia	Pando	0a4061fcdd3f4e6ea8db682199fee2757e8275
153	e8551349177c5d99595f7905e9e752b0	NY	R0003	RP003	2016-01-13	2199-12-31	2	wTpuVRx1CP4eXjjQiCKXjjvvLaZpRAAujc0QB30k	M	Black or African American	Y	N	N	N	N	N	\N	\N	Y	N	Y	Y	Y	N	SLI	ebSrKVKDGaaPL3mlUJcqbme0wl3kxMv2HlTPriG1	QR1uPT4gYiQVWWfRfcaR6woqvvuUmJxHUtcOqKoL	Matsuura, Debby Betty	0002-09-05	Debby	Betty	Matsuura	0a4061fcdd3f4e6ea8db682199fee2757e8275
93	e8551349177c5d99595f7905e9e752b0	NY	R0003	RP003	1900-01-01	2016-01-13	1	wTpuVRx1CP4eXjjQiCKXjjvvLaZpRAAujc0QB30k	M	Black or African American	Y	N	N	N	N	N	\N	\N	Y	N	Y	Y	Y	N	SLI	ebSrKVKDGaaPL3mlUJcqbme0wl3kxMv2HlTPriG1	QR1uPT4gYiQVWWfRfcaR6woqvvuUmJxHUtcOqKoL	Matsuura, Debby Betty	0009-02-05	Debby	Betty	Matsuura	0a4061fcdd3f4e6ea8db682199fee2757e8275
154	1bb20322df11065cc75f7909e7dfabb0	NY	R0003	RP003	2016-01-13	2199-12-31	2	w7TmiOs6CWJsXS2l1crivMsf42ZlQ5WAr6Xri2rd	M	Black or African American	N	N	N	N	N	Y	\N	\N	N	Y	N	Y	Y	N	VI	D1AXqIoaXx8AQTaQoumsfu5xeX4aGzCRrecFKecG	JaT2z61Ejzjz9rFhwN0ePthYx9cLsIJJPGYoHeLY	Cabello, Becki Lois	0002-09-05	Becki	Lois	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275
39	1bb20322df11065cc75f7909e7dfabb0	NY	R0003	RP003	1900-01-01	2016-01-13	1	w7TmiOs6CWJsXS2l1crivMsf42ZlQ5WAr6Xri2rd	M	Black or African American	N	N	N	N	N	Y	\N	\N	N	Y	N	Y	Y	N	VI	D1AXqIoaXx8AQTaQoumsfu5xeX4aGzCRrecFKecG	JaT2z61Ejzjz9rFhwN0ePthYx9cLsIJJPGYoHeLY	Cabello, Becki Lois	9999-01-01	Becki	Lois	Cabello	0a4d255f40644147aaffbcb10fef0d908e8bd9
155	6b4d3fe4dae280ffcdb8b898d96d5c5f	NY	R0003	RP003	2016-01-13	2199-12-31	2	e3OGNkIbM4h2skSI4nPuESDwrF1EMmjIfrprGMHz	M	Asian	N	Y	N	N	N	N	\N	\N	Y	Y	Y	N	Y	Y	AUT	TKmqHN7XOP5W3NPKNiDZZEdusLdlWLO8Ggpiwvh7	KQp8atYRrBdUzeyGT0DbWG8VMX9vwOZh357WUTZV	Pedone, Verena Sandy	0002-09-05	Verena	Sandy	Pedone	0a4061fcdd3f4e6ea8db682199fee2757e8275
40	6b4d3fe4dae280ffcdb8b898d96d5c5f	NY	R0003	RP003	1900-01-01	2016-01-13	1	e3OGNkIbM4h2skSI4nPuESDwrF1EMmjIfrprGMHz	M	Asian	N	Y	N	N	N	N	\N	\N	Y	Y	Y	N	Y	Y	AUT	TKmqHN7XOP5W3NPKNiDZZEdusLdlWLO8Ggpiwvh7	KQp8atYRrBdUzeyGT0DbWG8VMX9vwOZh357WUTZV	Pedone, Verena Sandy	9999-01-01	Verena	Sandy	Pedone	ded3479fc4504921ac1cf878b8c5d015efffc1
156	8af03f118f631883572e0f63a6831af9	NY	R0003	RP003	2016-01-13	2199-12-31	2	jgliMmu6B15PbLy7sq7uRlv40qiIF3liNI3ySpru	F	American Indian or Alaska Native	Y	N	N	N	N	N	\N	\N	Y	N	N	Y	N	N	MD	GvPr5sZMwbioUajA89GVJsZlclfYuL7suORDYyA7	UYZAdmVcmhGvyIWhC2Gn2AUP9Z55dXPSHb7OCSQl	Carriere, Zofia Autumn	0002-09-05	Zofia	Autumn	Carriere	0a4061fcdd3f4e6ea8db682199fee2757e8275
94	8af03f118f631883572e0f63a6831af9	NY	R0003	RP003	1900-01-01	2016-01-13	1	jgliMmu6B15PbLy7sq7uRlv40qiIF3liNI3ySpru	F	American Indian or Alaska Native	Y	N	N	N	N	N	\N	\N	Y	N	N	Y	N	N	MD	GvPr5sZMwbioUajA89GVJsZlclfYuL7suORDYyA7	UYZAdmVcmhGvyIWhC2Gn2AUP9Z55dXPSHb7OCSQl	Carriere, Zofia Autumn	0009-02-05	Zofia	Autumn	Carriere	0a4061fcdd3f4e6ea8db682199fee2757e8275
157	c08021aff975fe42e968144478d6296c	NY	R0003	RP003	2016-01-13	2199-12-31	2	o557yJ1Kx4YrfB8oMHfoaDVLMTFj3DJXWrKMm9zT	M	Asian	N	N	N	Y	N	N	\N	\N	Y	Y	N	N	Y	N	HI	s7VUQCuVTAzFIGpKVlmJe4taEyilIVd7kQDnTcle	sNY082Rah4osJ4DLD0UCRho0MvycGnGwlkM4SKZX	Gatti, Thurman Erasmo	0002-09-05	Thurman	Erasmo	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275
71	c08021aff975fe42e968144478d6296c	NY	R0003	RP003	1900-01-01	2016-01-13	1	o557yJ1Kx4YrfB8oMHfoaDVLMTFj3DJXWrKMm9zT	M	Asian	N	N	N	Y	N	N	\N	\N	Y	Y	N	N	Y	N	HI	s7VUQCuVTAzFIGpKVlmJe4taEyilIVd7kQDnTcle	sNY082Rah4osJ4DLD0UCRho0MvycGnGwlkM4SKZX	Gatti, Thurman Erasmo	0009-02-05	Thurman	Erasmo	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275
158	af7ce9083695923b0a451b0a9eb2f66f	NY	R0003	RP003	2016-01-13	2199-12-31	2	WW9iCRpiTricUbWoelchVevYIJHSjhAUMpLVYt6D	M	Black or African American	Y	N	N	N	N	N	\N	\N	N	Y	Y	Y	Y	N	MD	1TZNscNyH36p1d7XN1zadRihrKpSt4i87QzWaWD7	3clJx8gZokw3qKF4R9c8MRGtMI1jDxRqrhjm1Aph	Parr, Erasmo Barbar	0002-09-05	Erasmo	Barbar	Parr	0a4061fcdd3f4e6ea8db682199fee2757e8275
24	af7ce9083695923b0a451b0a9eb2f66f	NY	R0003	RP003	1900-01-01	2016-01-13	1	WW9iCRpiTricUbWoelchVevYIJHSjhAUMpLVYt6D	M	Black or African American	Y	N	N	N	N	N	\N	\N	N	Y	Y	Y	Y	N	MD	1TZNscNyH36p1d7XN1zadRihrKpSt4i87QzWaWD7	3clJx8gZokw3qKF4R9c8MRGtMI1jDxRqrhjm1Aph	Parr, Erasmo Barbar	0009-02-05	Erasmo	Barbar	Parr	0a4061fcdd3f4e6ea8db682199fee2757e8275
159	ffc6f8d2704cd12adb4e62e9e30f6776	NY	R0003	RP003	2016-01-13	2199-12-31	2	Uk2E8MeExKfyl6XHBLKpNJe6na8bi3nj8yMCgeFz	F	Hispanic or Latino	N	N	N	N	N	Y	\N	\N	Y	N	N	N	N	Y	AUT	mQUKZ7qYBovBIcS9sBQJ34Snvr3JwVr2yoyy8MzD	iWnsfdzQdhUYFlXze7BehXrUvCtCn6HtRyvgPwBn	Augustus, Zofia Orval	0002-09-05	Zofia	Orval	Augustus	0a4061fcdd3f4e6ea8db682199fee2757e8275
95	ffc6f8d2704cd12adb4e62e9e30f6776	NY	R0003	RP003	1900-01-01	2016-01-13	1	Uk2E8MeExKfyl6XHBLKpNJe6na8bi3nj8yMCgeFz	F	Hispanic or Latino	N	N	N	N	N	Y	\N	\N	Y	N	N	N	N	Y	AUT	mQUKZ7qYBovBIcS9sBQJ34Snvr3JwVr2yoyy8MzD	iWnsfdzQdhUYFlXze7BehXrUvCtCn6HtRyvgPwBn	Augustus, Zofia Orval	0009-02-05	Zofia	Orval	Augustus	0a4061fcdd3f4e6ea8db682199fee2757e8275
160	a1c0bbac44a6b67b193e48d140c69164	NY	R0003	RP003	2016-01-13	2199-12-31	2	oBPTLwB89Yetjpa88VqxOvlIENOqe4XKGKnOrJfM	F	White	N	N	N	Y	N	N	\N	\N	N	Y	N	N	N	N	SLD	yLxhxvhJpqKtF1L4Ofz0cSs4BPJh4abTqcZbQyqd	RQOAUp2l6bdfAKIPIfnyW5DxXbFJxeLqRDTCHepM	Matsuura, Orval Barbar	0002-09-05	Orval	Barbar	Matsuura	0a4061fcdd3f4e6ea8db682199fee2757e8275
41	a1c0bbac44a6b67b193e48d140c69164	NY	R0003	RP003	1900-01-01	2016-01-13	1	oBPTLwB89Yetjpa88VqxOvlIENOqe4XKGKnOrJfM	F	White	N	N	N	Y	N	N	\N	\N	N	Y	N	N	N	N	SLD	yLxhxvhJpqKtF1L4Ofz0cSs4BPJh4abTqcZbQyqd	RQOAUp2l6bdfAKIPIfnyW5DxXbFJxeLqRDTCHepM	Matsuura, Orval Barbar	9999-01-01	Orval	Barbar	Matsuura	0a4061fcdd3f4e6ea8db682199fee2757e8275
161	13052b7a1b3376e95eb5458da3afaa61	NY	R0003	RP003	2016-01-13	2199-12-31	3	6boPVVoO1Vuyg8VJ5AqV0Zha4rVGrZt3uPrM7xgw	F	Native Hawaiian or other Pacific Islander	N	N	N	Y	N	N	\N	\N	Y	N	Y	Y	N	N	EMN	qZL2LXxVKg8Sz8vOKANWLueLW3V5giAyz2jpg4D0	L5wQxkNvAhoDiwvT3jKyOEYyA25RaEL0WVIPOH0Y	Parr, Betty Erasmo	0002-09-05	Betty	Erasmo	Parr	0a4061fcdd3f4e6ea8db682199fee2757e8275
97	13052b7a1b3376e95eb5458da3afaa61	NY	R0003	RP003	2016-01-13	2016-01-13	2	6boPVVoO1Vuyg8VJ5AqV0Zha4rVGrZt3uPrM7xgw	F	Native Hawaiian or other Pacific Islander	N	N	N	Y	N	N	\N	\N	Y	N	Y	Y	N	N	EMN	qZL2LXxVKg8Sz8vOKANWLueLW3V5giAyz2jpg4D0	L5wQxkNvAhoDiwvT3jKyOEYyA25RaEL0WVIPOH0Y	Parr, Betty Erasmo	0009-02-05	Betty	Erasmo	Parr	0a4061fcdd3f4e6ea8db682199fee2757e8275
162	c4e48fce2c23536a19b6ae7d1e229eb2	NY	R0003	RP003	2016-01-13	2199-12-31	3	KL3R8L1EaO8MgymDcMI59fe7DnjLk0ZuDZiwO2J2	F	Two or more races	N	N	Y	N	N	N	\N	\N	Y	N	Y	Y	Y	Y	AUT	YF1IdV4ztKcjrlJlJkwf1fqIg8yuj2xn9ESdUdsV	WX95D3DMB8lxa2WOd0oFWiUvcMsat1t5gLC8yJTU	Gatti, Sandy Verena	0002-09-05	Sandy	Verena	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275
98	c4e48fce2c23536a19b6ae7d1e229eb2	NY	R0003	RP003	2016-01-13	2016-01-13	2	KL3R8L1EaO8MgymDcMI59fe7DnjLk0ZuDZiwO2J2	F	Two or more races	N	N	Y	N	N	N	\N	\N	Y	N	Y	Y	Y	Y	AUT	YF1IdV4ztKcjrlJlJkwf1fqIg8yuj2xn9ESdUdsV	WX95D3DMB8lxa2WOd0oFWiUvcMsat1t5gLC8yJTU	Gatti, Sandy Verena	0009-02-05	Sandy	Verena	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275
163	a506e6e4c2fedd0801be08bbdb646e1c	NY	R0003	RP003	1900-01-01	2199-12-31	1	LQWpnLVTSTAXEFoit9Q2RlZNibkOFcpmSYgXMHyo	F	Hispanic or Latino	N	N	N	N	N	N	\N	\N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Augustus, Orval Marisela	0002-09-05	Orval	Marisela	Augustus	0a4061fcdd3f4e6ea8db682199fee2757e8275
166	9c3ddcaa19df23cc12d94f2f71d6e4a1	NY	R0002	RE001	2016-01-13	2199-12-31	2	JskCum0I6ZULH064xsFeFcW4eZVTIEj5m6zExxG2	M	Black or African American	N	N	N	N	N	N	\N	\N	Y	N	Y	Y	Y	N	DD	HeZKMFcuhusK4OvtDLzrrmI79Gdes7RaJKoe9B5d	Jrg0oaGGMSXGLpjKbx1UyVueXXk7hAmN7EnYyIC0	Cabello, Debby Keshia	0002-09-05	Debby	Keshia	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275
7	9c3ddcaa19df23cc12d94f2f71d6e4a1	NY	R0002	RE001	1900-01-01	2016-01-13	1	JskCum0I6ZULH064xsFeFcW4eZVTIEj5m6zExxG2	M	Black or African American	N	N	N	N	N	N	\N	\N	Y	N	Y	Y	Y	N	DD	HeZKMFcuhusK4OvtDLzrrmI79Gdes7RaJKoe9B5d	Jrg0oaGGMSXGLpjKbx1UyVueXXk7hAmN7EnYyIC0	Cabello, Debby Keshia	0009-02-05	Debby	Keshia	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275
167	1d6f34149c5565e378f1c0ac79799f1b	NY	R0002	RE001	2016-01-13	2199-12-31	2	p69xZhxSMQuWmke62h3j1K7izaj1evXeklkHFuMg	M	Black or African American	Y	N	N	N	N	N	\N	\N	Y	N	Y	Y	Y	Y	SLI	uTP6iHeeGja0N6JtNGfst5Wm1LpAcL5AzKtMnEPS	llv5nV4RRVH4AT3X9Rg4x0CPyoFl2TGBfavZtjSP	Aispuro, Cornelius Becki	0002-09-05	Cornelius	Becki	Aispuro	0a4061fcdd3f4e6ea8db682199fee2757e8275
82	1d6f34149c5565e378f1c0ac79799f1b	NY	R0002	RE001	1900-01-01	2016-01-13	1	p69xZhxSMQuWmke62h3j1K7izaj1evXeklkHFuMg	M	Black or African American	Y	N	N	N	N	N	\N	\N	Y	N	Y	Y	Y	Y	SLI	uTP6iHeeGja0N6JtNGfst5Wm1LpAcL5AzKtMnEPS	llv5nV4RRVH4AT3X9Rg4x0CPyoFl2TGBfavZtjSP	Aispuro, Cornelius Becki	0009-02-05	Cornelius	Becki	Aispuro	0a4061fcdd3f4e6ea8db682199fee2757e8275
168	4fc5fddb5da3a6f9ebd017ff8c41bcfb	NY	R0002	RE001	2016-01-13	2199-12-31	2	3i8VkcJCs5QffVgj2QOmZr3MLv40I0KJc5k5DcWe	F	American Indian or Alaska Native	Y	N	N	N	N	N	\N	\N	Y	Y	Y	N	Y	N	SLD	sPKVH1LPyxMta9HHY4pOUwYc2hgvAQXRb4Nn3vVH	uYMntLy65YFYZooxdD3tmjcfOzdAfnbOURxtw5uL	Mantle, Keshia Arnette	0002-09-05	Keshia	Arnette	Mantle	0a4061fcdd3f4e6ea8db682199fee2757e8275
30	4fc5fddb5da3a6f9ebd017ff8c41bcfb	NY	R0002	RE001	1900-01-01	2016-01-13	1	3i8VkcJCs5QffVgj2QOmZr3MLv40I0KJc5k5DcWe	F	American Indian or Alaska Native	Y	N	N	N	N	N	\N	\N	Y	Y	Y	N	Y	N	SLD	sPKVH1LPyxMta9HHY4pOUwYc2hgvAQXRb4Nn3vVH	uYMntLy65YFYZooxdD3tmjcfOzdAfnbOURxtw5uL	Mantle, Keshia Arnette	9999-01-01	Keshia	Arnette	Mantle	dcdaa08b9b9046beb3e77419481c860bc6598e
169	334427b93ac522c0a6cc8ea1d217a2fe	NY	R0002	RE001	2016-01-13	2199-12-31	2	OimZrYW80W7xl7QUa4Hg9Q9knjdvys1Z8wRIVHuG	F	American Indian or Alaska Native	N	N	N	Y	N	N	\N	\N	N	Y	N	N	N	N	ID	LVDJ5pmKkbouYqlTqLGFL37ZoQe3oh4zisvYXKNF	sIqnzxAVbEnEyQ5EhHNskz5gSJZ2zjnzHfZ6GCZ0	Cabello, Chase Jacquline	0002-09-05	Chase	Jacquline	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275
84	334427b93ac522c0a6cc8ea1d217a2fe	NY	R0002	RE001	1900-01-01	2016-01-13	1	OimZrYW80W7xl7QUa4Hg9Q9knjdvys1Z8wRIVHuG	F	American Indian or Alaska Native	N	N	N	Y	N	N	\N	\N	N	Y	N	N	N	N	ID	LVDJ5pmKkbouYqlTqLGFL37ZoQe3oh4zisvYXKNF	sIqnzxAVbEnEyQ5EhHNskz5gSJZ2zjnzHfZ6GCZ0	Cabello, Chase Jacquline	0009-02-05	Chase	Jacquline	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275
170	e0cdf54795b59b5dc924f729dfeb0ed6	NY	R0002	RE001	2016-01-13	2199-12-31	2	N1eKk2eGo15Htuapk7zDnIIftLz18aVE1PMPSktf	M	Black or African American	N	N	N	N	N	Y	\N	\N	N	N	Y	N	Y	Y	DB	1MuydaJPqVwL73BlUbInGkDx4AOijXeulKfdfP6v	zkx46I7G2Nd1CipPzPAOYgC92lGvNZYvOHU0wbW2	Pedone, Barbar Erasmo	0002-09-05	Barbar	Erasmo	Pedone	0a4061fcdd3f4e6ea8db682199fee2757e8275
31	e0cdf54795b59b5dc924f729dfeb0ed6	NY	R0002	RE001	1900-01-01	2016-01-13	1	N1eKk2eGo15Htuapk7zDnIIftLz18aVE1PMPSktf	M	Black or African American	N	N	N	N	N	Y	\N	\N	N	N	Y	N	Y	Y	DB	1MuydaJPqVwL73BlUbInGkDx4AOijXeulKfdfP6v	zkx46I7G2Nd1CipPzPAOYgC92lGvNZYvOHU0wbW2	Pedone, Barbar Erasmo	9999-01-01	Barbar	Erasmo	Pedone	343fe3a084b2422796dbd7533a68de13c3cf37
171	ba0a932484d5410c3dbb8ee7fccd409b	NY	R0002	RE001	2016-01-13	2199-12-31	2	oGYUvFLatZjgEBWy3LV9NwMdmc9GT1GTerNuABS9	M	Hispanic or Latino	N	N	N	N	N	Y	\N	\N	N	N	N	N	N	Y	TBI	QgAwuCsMxGtAVrDNYsagNJWvjWFZQ7kIa9R7h3Sj	02i9bpKnZsr2LP5mQ5qCaReYMb8pzxH0JPS6bMW1	Lawlor, Kimberely Joella	0002-09-05	Kimberely	Joella	Lawlor	0a4061fcdd3f4e6ea8db682199fee2757e8275
86	ba0a932484d5410c3dbb8ee7fccd409b	NY	R0002	RE001	1900-01-01	2016-01-13	1	oGYUvFLatZjgEBWy3LV9NwMdmc9GT1GTerNuABS9	M	Hispanic or Latino	N	N	N	N	N	Y	\N	\N	N	N	N	N	N	Y	TBI	QgAwuCsMxGtAVrDNYsagNJWvjWFZQ7kIa9R7h3Sj	02i9bpKnZsr2LP5mQ5qCaReYMb8pzxH0JPS6bMW1	Lawlor, Kimberely Joella	0009-02-05	Kimberely	Joella	Lawlor	0a4061fcdd3f4e6ea8db682199fee2757e8275
172	4a11522d5b07fd4f3ebd02e07dec1266	NY	R0002	RE001	2016-01-13	2199-12-31	2	sstpR8WRh7effxahZuK1ZPaQJmLH10nFDTSm23am	F	Asian	N	N	Y	N	N	N	\N	\N	Y	N	N	Y	Y	Y	DB	lJNWeBPTH8pWTldllW5CSVxN1E8b41usZSxxGxcc	bXwzYWFtuW0OvjYDxXMlg3pFoMDwDzALTPg457AY	Gatti, Darci Devon	0002-09-05	Darci	Devon	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275
73	4a11522d5b07fd4f3ebd02e07dec1266	NY	R0002	RE001	1900-01-01	2016-01-13	1	sstpR8WRh7effxahZuK1ZPaQJmLH10nFDTSm23am	F	Asian	N	N	Y	N	N	N	\N	\N	Y	N	N	Y	Y	Y	DB	lJNWeBPTH8pWTldllW5CSVxN1E8b41usZSxxGxcc	bXwzYWFtuW0OvjYDxXMlg3pFoMDwDzALTPg457AY	Gatti, Darci Devon	0009-02-05	Darci	Devon	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275
173	67623efe9c24836f070f6f15d9d95223	NY	R0002	RE001	2016-01-13	2199-12-31	2	Uetza1KA3EqTldkdiq0w92KFYhVqBL6zLWinhXI9	F	Black or African American	N	N	N	Y	N	N	\N	\N	N	N	N	Y	Y	N	OI	DTdpU08KJSO0JBj3SmXaYdkerjGFRXcgjwryaonp	HTqQ01LuKWOiZo7FuuYOqwPO1A4y0vnSR12vyzaz	Perla, Orval Kimberely	0002-09-05	Orval	Kimberely	Perla	0a4061fcdd3f4e6ea8db682199fee2757e8275
75	67623efe9c24836f070f6f15d9d95223	NY	R0002	RE001	1900-01-01	2016-01-13	1	Uetza1KA3EqTldkdiq0w92KFYhVqBL6zLWinhXI9	F	Black or African American	N	N	N	Y	N	N	\N	\N	N	N	N	Y	Y	N	OI	DTdpU08KJSO0JBj3SmXaYdkerjGFRXcgjwryaonp	HTqQ01LuKWOiZo7FuuYOqwPO1A4y0vnSR12vyzaz	Perla, Orval Kimberely	0009-02-05	Orval	Kimberely	Perla	0a4061fcdd3f4e6ea8db682199fee2757e8275
174	d50baff508749f30b594eb230cbc7fc3	NY	R0002	RE001	2016-01-13	2199-12-31	2	pB7SBPI9whDTX6jqMTvlCa5POI7wiii3ik8YSqUN	M	Hispanic or Latino	N	N	Y	N	N	N	\N	\N	N	N	N	N	N	N	DD	71L3VffWOE0M4VWkRnc68I3jHb2fq71R9guPY9JJ	sfC58DkrG3bkJ2rcGRb8SGgBjmvLuVlT0KGZ0jLL	Neveu, Jacquline Kimberely	0002-09-05	Jacquline	Kimberely	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275
32	d50baff508749f30b594eb230cbc7fc3	NY	R0002	RE001	1900-01-01	2016-01-13	1	pB7SBPI9whDTX6jqMTvlCa5POI7wiii3ik8YSqUN	M	Hispanic or Latino	N	N	Y	N	N	N	\N	\N	N	N	N	N	N	N	DD	71L3VffWOE0M4VWkRnc68I3jHb2fq71R9guPY9JJ	sfC58DkrG3bkJ2rcGRb8SGgBjmvLuVlT0KGZ0jLL	Neveu, Jacquline Kimberely	9999-01-01	Jacquline	Kimberely	Neveu	cbae066d8cec4492bcbb66c5dccd0965689b3b
175	abee700c3f98e310e82a402935d287e2	NY	R0002	RE001	2016-01-13	2199-12-31	2	EOHTQnkhMw4mzKP8Rpaq42vOlOBZzsZZkKU6zpll	F	Asian	N	N	N	N	N	Y	\N	\N	Y	N	Y	N	Y	Y	ID	nt8NWGO5kNNBdXBS8atgidVWhyFSQFoonEXSPpT4	5zGEfa0eUFpkE9cRcTtRbbbKvBIlhHGkHbe07K9E	Aispuro, Devon Marisela	0002-09-05	Devon	Marisela	Aispuro	0a4061fcdd3f4e6ea8db682199fee2757e8275
8	abee700c3f98e310e82a402935d287e2	NY	R0002	RE001	1900-01-01	2016-01-13	1	EOHTQnkhMw4mzKP8Rpaq42vOlOBZzsZZkKU6zpll	F	Asian	N	N	N	N	N	Y	\N	\N	Y	N	Y	N	Y	Y	ID	nt8NWGO5kNNBdXBS8atgidVWhyFSQFoonEXSPpT4	5zGEfa0eUFpkE9cRcTtRbbbKvBIlhHGkHbe07K9E	Aispuro, Devon Marisela	0009-02-05	Devon	Marisela	Aispuro	0a4061fcdd3f4e6ea8db682199fee2757e8275
176	01304c8fd46b80a36d5e9f91f66318fe	NY	R0002	RE001	2016-01-13	2199-12-31	2	UH69Q4IEQrjnQGvI9xhrNRTWUtmihpX8hlQ4r61X	M	Black or African American	N	N	N	Y	N	N	\N	\N	N	N	Y	N	Y	N	SLI	EZ54dXWHp5YonTwHJGBO7OwfrH5qgGQXUZrQcINr	OcXwDqvPR5sGfzoIcohmBFriXTnx5rO6H6mrSY9d	Parr, Winona Loni	0002-09-05	Winona	Loni	Parr	0a4061fcdd3f4e6ea8db682199fee2757e8275
9	01304c8fd46b80a36d5e9f91f66318fe	NY	R0002	RE001	1900-01-01	2016-01-13	1	UH69Q4IEQrjnQGvI9xhrNRTWUtmihpX8hlQ4r61X	M	Black or African American	N	N	N	Y	N	N	\N	\N	N	N	Y	N	Y	N	SLI	EZ54dXWHp5YonTwHJGBO7OwfrH5qgGQXUZrQcINr	OcXwDqvPR5sGfzoIcohmBFriXTnx5rO6H6mrSY9d	Parr, Winona Loni	0009-02-05	Winona	Loni	Parr	0a4061fcdd3f4e6ea8db682199fee2757e8275
177	621d8de66bc3f55b66893f163627571c	NY	R0002	RE001	2016-01-13	2199-12-31	2	Fg5kGWmyfjDA600mHsVyZJALKlRwmg8pe1LtPdSb	F	Hispanic or Latino	N	N	N	N	N	Y	\N	\N	N	N	N	N	Y	N	TBI	Ua0kQ0waLEMyOkNDhrxiSKBFDfThzxGua8m4XG7S	R7llKZfwafKEIz3IjpgcQBxaTQTGjMuresr9R6te	Neveu, Cornelius Autumn	0002-09-05	Cornelius	Autumn	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275
10	621d8de66bc3f55b66893f163627571c	NY	R0002	RE001	1900-01-01	2016-01-13	1	Fg5kGWmyfjDA600mHsVyZJALKlRwmg8pe1LtPdSb	F	Hispanic or Latino	N	N	N	N	N	Y	\N	\N	N	N	N	N	Y	N	TBI	Ua0kQ0waLEMyOkNDhrxiSKBFDfThzxGua8m4XG7S	R7llKZfwafKEIz3IjpgcQBxaTQTGjMuresr9R6te	Neveu, Cornelius Autumn	0009-02-05	Cornelius	Autumn	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275
178	ef445d872026bf7bd42ee2cb836a6160	NY	R0002	RE001	2016-01-13	2199-12-31	2	uJkYBTpWyQhQPsBC4PFyGIFqcoyKQiHxv4f3IYsu	F	Asian	N	N	N	N	N	Y	\N	\N	N	Y	N	N	Y	N	MD	LWBhsVv7yWdjNlIGtlUQzsPY1qVaygSUKSDHTKDW	ZVeK3Mo3PCiRKga5acczo6phYZQKkFjkBTUH1cTi	Neveu, Chase Keshia	0002-09-05	Chase	Keshia	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275
52	ef445d872026bf7bd42ee2cb836a6160	NY	R0002	RE001	1900-01-01	2016-01-13	1	uJkYBTpWyQhQPsBC4PFyGIFqcoyKQiHxv4f3IYsu	F	Asian	N	N	N	N	N	Y	\N	\N	N	Y	N	N	Y	N	MD	LWBhsVv7yWdjNlIGtlUQzsPY1qVaygSUKSDHTKDW	ZVeK3Mo3PCiRKga5acczo6phYZQKkFjkBTUH1cTi	Neveu, Chase Keshia	0009-02-05	Chase	Keshia	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275
179	5e00240b44c06af13a7fe3094d08304f	NY	R0002	RE001	2016-01-13	2199-12-31	2	0YrEK4Y3PihDRZiZnFMinOcTdkNYwublx9Z8lCAp	F	Black or African American	N	N	N	N	N	Y	\N	\N	N	N	Y	N	N	Y	OI	cneCAQyANr172ywJd3n3MAl1Z3fVHQetNeVQeNn3	111XTD0Ya5bojkRrCgASmcHwlx51DERXxbKWRgld	Mcgowin, Marisela Marisela	0002-09-05	Marisela	Marisela	Mcgowin	0a4061fcdd3f4e6ea8db682199fee2757e8275
53	5e00240b44c06af13a7fe3094d08304f	NY	R0002	RE001	1900-01-01	2016-01-13	1	0YrEK4Y3PihDRZiZnFMinOcTdkNYwublx9Z8lCAp	F	Black or African American	N	N	N	N	N	Y	\N	\N	N	N	Y	N	N	Y	OI	cneCAQyANr172ywJd3n3MAl1Z3fVHQetNeVQeNn3	111XTD0Ya5bojkRrCgASmcHwlx51DERXxbKWRgld	Mcgowin, Marisela Marisela	0009-02-05	Marisela	Marisela	Mcgowin	0a4061fcdd3f4e6ea8db682199fee2757e8275
180	f34be81d07be97fa20b34697fd25688a	NY	R0001	RN001	2016-01-13	2199-12-31	2	SBFMQrkvsLNLQWnEXVVeN6ZMQXqemPhTY44hgO2M	M	Black or African American	N	N	N	Y	N	N	\N	\N	N	Y	Y	N	N	N	SLI	S6RWxM4aZT5T28sUstXnoV75JDnsTtmYXcgTv1k0	IdBEiFs4LSIhyz2lQ29b7WM6IxS2z5SXOYAMfDfy	Mccreery, Jacquline Becki	0002-09-05	Jacquline	Becki	Mccreery	5f3bcce2cac34dd38578b9d423801dc3b5caf6
3	f34be81d07be97fa20b34697fd25688a	NY	R0001	RN001	1900-01-01	2016-01-13	1	SBFMQrkvsLNLQWnEXVVeN6ZMQXqemPhTY44hgO2M	M	Black or African American	N	N	N	Y	N	N	\N	\N	N	Y	Y	N	N	N	SLI	S6RWxM4aZT5T28sUstXnoV75JDnsTtmYXcgTv1k0	IdBEiFs4LSIhyz2lQ29b7WM6IxS2z5SXOYAMfDfy	Mccreery, Jacquline Becki	0009-02-05	Jacquline	Becki	Mccreery	0a4061fcdd3f4e6ea8db682199fee2757e8275
181	6632024fec983a92415e94806238cdd5	NY	R0001	RN001	2016-01-13	2199-12-31	2	OIeWicvTvk5kcbEc0qj7vPo2nh3Vr8XS9e2Vz57b	F	Hispanic or Latino	N	N	Y	N	N	N	\N	\N	N	N	Y	N	N	N	TBI	rjmYkBa0yZxWg60UvrOgMyxiQYCWQVd8bXaeCNeS	t1lMZ0W1BM0RoHefhKPLNdUr38uYXTo4yOqxU10F	Perla, Barbar Orval	0002-09-05	Barbar	Orval	Perla	dcdaa08b9b9046beb3e77419481c860bc6598e
4	6632024fec983a92415e94806238cdd5	NY	R0001	RN001	1900-01-01	2016-01-13	1	OIeWicvTvk5kcbEc0qj7vPo2nh3Vr8XS9e2Vz57b	F	Hispanic or Latino	N	N	Y	N	N	N	\N	\N	N	N	Y	N	N	N	TBI	rjmYkBa0yZxWg60UvrOgMyxiQYCWQVd8bXaeCNeS	t1lMZ0W1BM0RoHefhKPLNdUr38uYXTo4yOqxU10F	Perla, Barbar Orval	0009-02-05	Barbar	Orval	Perla	0a4061fcdd3f4e6ea8db682199fee2757e8275
182	751602abb985d0a2bdeb84ead87b41e9	NY	R0003	RP003	1900-01-01	2199-12-31	1	FkgQ5OyZAMUDgnC8f6cJHnpLkzOlGV6xnArV3Bhf	F	Hispanic or Latino	N	N	N	N	N	N	\N	\N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Augustus, Orval Marisela	0002-09-05	Orval	Marisela	Augustus	0a4061fcdd3f4e6ea8db682199fee2757e8275
183	7aa3640ba52837815c3b3d435695442b	NY	R0003	RP003	2016-01-13	2199-12-31	2	5RsVRnISVN942m5YIo50ucTxyB8cYNObQ1tnrORJ	F	Hispanic or Latino	N	N	N	N	N	N	\N	\N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Mccreery, Sandy Marisela	0002-09-05	Sandy	Marisela	Mccreery	5f3bcce2cac34dd38578b9d423801dc3b5caf6
164	7aa3640ba52837815c3b3d435695442b	NY	R0003	RP003	1900-01-01	2016-01-13	1	5RsVRnISVN942m5YIo50ucTxyB8cYNObQ1tnrORJ	F	Hispanic or Latino	N	N	N	N	N	N	\N	\N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Mccreery, Sandy Marisela	0002-09-05	Sandy	Marisela	Mccreery	0a4061fcdd3f4e6ea8db682199fee2757e8275
184	fb550ff5bf5ff80999374791323dbce7	NY	R0003	RP003	2016-01-13	2199-12-31	2	GXr7X73TjgGmTEm62Q3cGYSMBHE34YWvhwJEalyy	F	Hispanic or Latino	N	N	N	N	N	N	\N	\N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Pedone, Shane Marisela	0002-09-05	Shane	Marisela	Pedone	dcdaa08b9b9046beb3e77419481c860bc6598e
165	fb550ff5bf5ff80999374791323dbce7	NY	R0003	RP003	1900-01-01	2016-01-13	1	GXr7X73TjgGmTEm62Q3cGYSMBHE34YWvhwJEalyy	F	Hispanic or Latino	N	N	N	N	N	N	\N	\N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Pedone, Shane Marisela	0002-09-05	Shane	Marisela	Pedone	0a4061fcdd3f4e6ea8db682199fee2757e8275
185	de927f94b686710f8141596e35ae37fb	NY	R0003	RP003	1900-01-01	2199-12-31	1	GFWLlMJ65UE7pSrpAPrt2Tu5hoZkaeJdY0mrbTTU	F	Hispanic or Latino	N	N	N	N	N	N	\N	\N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Cabello, Darci Marisela	0002-09-05	Darci	Marisela	Cabello	343fe3a084b2422796dbd7533a68de13c3cf37
186	663f2f2f24a8f790f0bab57e441ad29e	NY	R0003	RP003	1900-01-01	2199-12-31	1	DuncASxHMXlhtve8RRDsuUQ83LaR8gIZ7rFEP0zs	F	Hispanic or Latino	N	N	N	N	N	N	\N	\N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Pando, Joella Marisela	0002-09-05	Joella	Marisela	Pando	5f3bcce2cac34dd38578b9d423801dc3b5caf6
187	4264e4ce519e167c0cdd10cb21d5af4f	NY	R0001	RN001	2016-01-13	2199-12-31	2	5336b7258eb545b3ae7178aeb0cfd72428a36fdb	M	American Indian or Alaska Native	Y	N	N	N	N	N	\N	\N	Y	Y	Y	N	Y	N	OI	a64dac1816204195a4759761cf1b2715eaa26359	a91eb32222844401a74296ae3b692b71b536d8f8	Brule, Steve Jan	0001-09-05	Steve	Jan	Brule	dcdaa08b9b9046beb3e77419481c860bc6598e
44	4264e4ce519e167c0cdd10cb21d5af4f	NY	R0001	RN001	1900-01-01	2016-01-13	1	5336b7258eb545b3ae7178aeb0cfd72428a36fdb	M	American Indian or Alaska Native	Y	N	N	N	N	N	\N	\N	Y	Y	Y	N	Y	N	OI	a64dac1816204195a4759761cf1b2715eaa26359	a91eb32222844401a74296ae3b692b71b536d8f8	Brule, Steve Jan	9999-01-01	Steve	Jan	Brule	5f3bcce2cac34dd38578b9d423801dc3b5caf6
188	cb2d4e7f463e6a85d8099e24dda69755	NY	R0001	RN001	2016-01-13	2199-12-31	2	UMaYGd8hto4TYc7EiWKtivzti8YAWg5xozomFI8q	F	Hispanic or Latino	N	N	N	N	N	Y	\N	\N	N	N	N	Y	N	N	DD	GZHpQpxaeap0uLPtsMSNT525rjOaTQiKeyj3YBnK	DGsfQaDDfXEMog1xErfGsJeWSIn7QDNTYHwbxcx1	Jewell, Lois Vincent	0002-09-05	Lois	Vincent	Jewell	0a4061fcdd3f4e6ea8db682199fee2757e8275
25	cb2d4e7f463e6a85d8099e24dda69755	NY	R0001	RN001	1900-01-01	2016-01-13	1	UMaYGd8hto4TYc7EiWKtivzti8YAWg5xozomFI8q	F	Hispanic or Latino	N	N	N	N	N	Y	\N	\N	N	N	N	Y	N	N	DD	GZHpQpxaeap0uLPtsMSNT525rjOaTQiKeyj3YBnK	DGsfQaDDfXEMog1xErfGsJeWSIn7QDNTYHwbxcx1	Jewell, Lois Vincent	9999-01-01	Lois	Vincent	Jewell	dcdaa08b9b9046beb3e77419481c860bc6598e
189	7fe3deadaa56f13a515f23ca45399bbe	NY	R0001	RN001	2016-01-13	2199-12-31	2	qEy737ea1Emx97d0RTxk7QpDWecnUu2eEBWvQXkK	M	Hispanic or Latino	N	N	N	Y	N	N	\N	\N	N	N	N	Y	Y	N	OI	PHSqW51HGoDx5DDlrf1AZ8MQhRTnYqmp5hasPeAL	YFTPFKP33wUilNIhHyFcJLPpbnx8U0xSrtxBffCq	Neveu, Roman Devon	0002-09-05	Roman	Devon	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275
26	7fe3deadaa56f13a515f23ca45399bbe	NY	R0001	RN001	1900-01-01	2016-01-13	1	qEy737ea1Emx97d0RTxk7QpDWecnUu2eEBWvQXkK	M	Hispanic or Latino	N	N	N	Y	N	N	\N	\N	N	N	N	Y	Y	N	OI	PHSqW51HGoDx5DDlrf1AZ8MQhRTnYqmp5hasPeAL	YFTPFKP33wUilNIhHyFcJLPpbnx8U0xSrtxBffCq	Neveu, Roman Devon	9999-01-01	Roman	Devon	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275
190	35eea408b9c01f63a22585d53148c9e9	NY	R0001	RN001	2016-01-13	2199-12-31	2	MXGplB4M232z2j4FNTStjIAKRER2ujJ8Z2OlV7Yn	F	Asian	N	N	N	N	N	Y	\N	\N	Y	Y	N	Y	N	Y	OI	xK2Faci3f8jeHK0T5ctKCG9m8qtFs0CUS92459TJ	rBQcB1GLoy5fdMUfjBX7cmKz3xKluOBGdsJL5DDn	Parr, Joella Betty	0002-09-05	Joella	Betty	Parr	0a4061fcdd3f4e6ea8db682199fee2757e8275
27	35eea408b9c01f63a22585d53148c9e9	NY	R0001	RN001	1900-01-01	2016-01-13	1	MXGplB4M232z2j4FNTStjIAKRER2ujJ8Z2OlV7Yn	F	Asian	N	N	N	N	N	Y	\N	\N	Y	Y	N	Y	N	Y	OI	xK2Faci3f8jeHK0T5ctKCG9m8qtFs0CUS92459TJ	rBQcB1GLoy5fdMUfjBX7cmKz3xKluOBGdsJL5DDn	Parr, Joella Betty	9999-01-01	Joella	Betty	Parr	5f3bcce2cac34dd38578b9d423801dc3b5caf6
191	516dcc2059465dc6105ccd8e898237cd	NY	R0001	RN001	2016-01-13	2199-12-31	2	ITh1jmueUE6G8rMT9oKqLXZKZMPKahZUoAUuvOQE	M	Asian	N	N	N	N	N	N	\N	\N	N	N	Y	N	Y	N	OI	OimvDBNrhNjuKD5x99zg5Oqsi2Cht2WucDis0K1O	gJJP5383nfHxtkqDy2vCWxdPSmqDrQNSLFKxFzlP	Gillett, Sandy Sandy	0002-09-05	Sandy	Sandy	Gillett	0a4061fcdd3f4e6ea8db682199fee2757e8275
49	516dcc2059465dc6105ccd8e898237cd	NY	R0001	RN001	1900-01-01	2016-01-13	1	ITh1jmueUE6G8rMT9oKqLXZKZMPKahZUoAUuvOQE	M	Asian	N	N	N	N	N	N	\N	\N	N	N	Y	N	Y	N	OI	OimvDBNrhNjuKD5x99zg5Oqsi2Cht2WucDis0K1O	gJJP5383nfHxtkqDy2vCWxdPSmqDrQNSLFKxFzlP	Gillett, Sandy Sandy	0009-02-05	Sandy	Sandy	Gillett	0a4061fcdd3f4e6ea8db682199fee2757e8275
192	078a250aa8712a24923e0f4c13a96c05	NY	R0001	RN001	2016-01-13	2199-12-31	2	RqHsCUovoRLSAhySU0AnRx0LY8YKin9uDhSFcOMD	F	Asian	N	Y	N	N	N	N	\N	\N	N	N	N	Y	N	N	EMN	IC2TX2WC8IeessJCg0nggokNgi0sNXvaACRXBUQd	tkBpd9dbEgFKhkjmrCJ5FINSpLk34XW3PajKwo56	Augustus, Verena Lois	0002-09-05	Verena	Lois	Augustus	0a4061fcdd3f4e6ea8db682199fee2757e8275
6	078a250aa8712a24923e0f4c13a96c05	NY	R0001	RN001	1900-01-01	2016-01-13	1	RqHsCUovoRLSAhySU0AnRx0LY8YKin9uDhSFcOMD	F	Asian	N	Y	N	N	N	N	\N	\N	N	N	N	Y	N	N	EMN	IC2TX2WC8IeessJCg0nggokNgi0sNXvaACRXBUQd	tkBpd9dbEgFKhkjmrCJ5FINSpLk34XW3PajKwo56	Augustus, Verena Lois	0009-02-05	Verena	Lois	Augustus	0a4061fcdd3f4e6ea8db682199fee2757e8275
193	3aea140ce97b0509911dfe6d51e2886b	NY	R0001	RN001	2016-01-13	2199-12-31	2	JL6uVW6OSVaYqpvndl0foBKAFrf4bdz6HACTTcLd	M	American Indian or Alaska Native	N	N	N	N	N	Y	\N	\N	Y	N	N	Y	Y	Y	DB	8WDT6bZI50W2Zl59D3UOS2Cxf9tJQQn8bAU686c3	Tqz1FfcBuMegwffE1XzgED6VVM3sys8KTvkjQoLC	Buckwalter, Arnette Jacquline	0002-09-05	Arnette	Jacquline	Buckwalter	0a4061fcdd3f4e6ea8db682199fee2757e8275
80	3aea140ce97b0509911dfe6d51e2886b	NY	R0001	RN001	1900-01-01	2016-01-13	1	JL6uVW6OSVaYqpvndl0foBKAFrf4bdz6HACTTcLd	M	American Indian or Alaska Native	N	N	N	N	N	Y	\N	\N	Y	N	N	Y	Y	Y	DB	8WDT6bZI50W2Zl59D3UOS2Cxf9tJQQn8bAU686c3	Tqz1FfcBuMegwffE1XzgED6VVM3sys8KTvkjQoLC	Buckwalter, Arnette Jacquline	0009-02-05	Arnette	Jacquline	Buckwalter	0a4061fcdd3f4e6ea8db682199fee2757e8275
194	237e6ee6981b783c5eec5d46a7eeb5f6	NY	R0001	RN001	2016-01-13	2199-12-31	2	vwkr0R9sEgA872YY02LOWZFwoTLq0ATfnEvZ0i17	F	American Indian or Alaska Native	N	N	N	N	N	Y	\N	\N	Y	N	N	Y	Y	Y	DB	wErx2MhJgnsv4KQMeSoJh2dVSn198lOyLnxjtomP	D73On2W5jdPNO4dLrU1dMpZPxnVxtwcfGzdHgy8S	Cartlidge, Shane Becki	0002-09-05	Shane	Becki	Cartlidge	0a4061fcdd3f4e6ea8db682199fee2757e8275
28	237e6ee6981b783c5eec5d46a7eeb5f6	NY	R0001	RN001	1900-01-01	2016-01-13	1	vwkr0R9sEgA872YY02LOWZFwoTLq0ATfnEvZ0i17	F	American Indian or Alaska Native	N	N	N	N	N	Y	\N	\N	Y	N	N	Y	Y	Y	DB	wErx2MhJgnsv4KQMeSoJh2dVSn198lOyLnxjtomP	D73On2W5jdPNO4dLrU1dMpZPxnVxtwcfGzdHgy8S	Cartlidge, Shane Becki	9999-01-01	Shane	Becki	Cartlidge	dcdaa08b9b9046beb3e77419481c860bc6598e
195	84d1a4ab7f21b6dc08e6c475e035fcaa	NY	R0002	RE001	2016-01-13	2199-12-31	2	mqZIVP2jMSn1VHrJ6nLaEHLblTiXW3eCC0BZDA6d	M	American Indian or Alaska Native	N	N	N	N	N	Y	\N	\N	Y	N	Y	Y	N	N	OHI	qtw4nQ44m7Si2kuCZjwq8PmEkxQcPDMvZIjrzTdM	4jeLurTXTIAGXc9kY5uuhOAbSIbYp4nxfzlhOw3g	Borkowski, Devon Jacquline	0002-09-05	Devon	Jacquline	Borkowski	0a4061fcdd3f4e6ea8db682199fee2757e8275
50	84d1a4ab7f21b6dc08e6c475e035fcaa	NY	R0002	RE001	1900-01-01	2016-01-13	1	mqZIVP2jMSn1VHrJ6nLaEHLblTiXW3eCC0BZDA6d	M	American Indian or Alaska Native	N	N	N	N	N	Y	\N	\N	Y	N	Y	Y	N	N	OHI	qtw4nQ44m7Si2kuCZjwq8PmEkxQcPDMvZIjrzTdM	4jeLurTXTIAGXc9kY5uuhOAbSIbYp4nxfzlhOw3g	Borkowski, Devon Jacquline	0009-02-05	Devon	Jacquline	Borkowski	0a4061fcdd3f4e6ea8db682199fee2757e8275
196	2e4cdc6db63c447ce6a1d925ad8dc137	NY	R0002	RE001	2016-01-13	2199-12-31	2	Ew6FoKuAKYas7HNCC2qpWTLldXJFPKY6WuKGaoCK	M	American Indian or Alaska Native	Y	N	N	N	N	N	\N	\N	N	N	N	Y	Y	Y	SLD	SxOSlOn6gZI9a31bOijbKvqGnk1VQQSi8ri9Pjom	lLMJd89DcDvbKY8VejtkyU53eyuH2iT3Tded2q1D	Borkowski, Loni Shane	0002-09-05	Loni	Shane	Borkowski	0a4061fcdd3f4e6ea8db682199fee2757e8275
51	2e4cdc6db63c447ce6a1d925ad8dc137	NY	R0002	RE001	1900-01-01	2016-01-13	1	Ew6FoKuAKYas7HNCC2qpWTLldXJFPKY6WuKGaoCK	M	American Indian or Alaska Native	Y	N	N	N	N	N	\N	\N	N	N	N	Y	Y	Y	SLD	SxOSlOn6gZI9a31bOijbKvqGnk1VQQSi8ri9Pjom	lLMJd89DcDvbKY8VejtkyU53eyuH2iT3Tded2q1D	Borkowski, Loni Shane	0009-02-05	Loni	Shane	Borkowski	0a4061fcdd3f4e6ea8db682199fee2757e8275
197	75b28ffe72c6dd5d462e235d6c1b00e1	NY	R0002	RE001	2016-01-13	2199-12-31	2	uB6VLZuP4JnB0aZW3u9Ol9pUS9ux9VdAuAaQ19fX	M	American Indian or Alaska Native	N	N	N	Y	N	N	\N	\N	N	Y	Y	N	Y	N	OHI	tVFAA1vX9gS5Phorf9YtS7oMWjLtIAH367W3757O	uZrTDiEVUCAffMQoGo3TwqCzna3YuvdSdTAq8lwW	Borkowski, Becki Lois	0002-09-05	Becki	Lois	Borkowski	0a4061fcdd3f4e6ea8db682199fee2757e8275
5	75b28ffe72c6dd5d462e235d6c1b00e1	NY	R0002	RE001	1900-01-01	2016-01-13	1	uB6VLZuP4JnB0aZW3u9Ol9pUS9ux9VdAuAaQ19fX	M	American Indian or Alaska Native	N	N	N	Y	N	N	\N	\N	N	Y	Y	N	Y	N	OHI	tVFAA1vX9gS5Phorf9YtS7oMWjLtIAH367W3757O	uZrTDiEVUCAffMQoGo3TwqCzna3YuvdSdTAq8lwW	Borkowski, Becki Lois	0009-02-05	Becki	Lois	Borkowski	0a4061fcdd3f4e6ea8db682199fee2757e8275
198	310ec16120315b4ce58601a1e9e5d8c8	NY	R0002	RE001	2016-01-13	2199-12-31	2	rMB447lffsMQCZOQPIUTPusiGmZolvpxn7jMAsiT	F	American Indian or Alaska Native	N	N	N	N	N	Y	\N	\N	Y	Y	Y	Y	N	N	SLD	w3eZOegFEIUSKulQeCRbbgDbj9WbHE1cOvn69gPO	5vlNJePaDUMPzuw3C6SIV6wXpD0TXMjGrn1rIkME	Benevides, Debby Orval	0002-09-05	Debby	Orval	Benevides	0a4061fcdd3f4e6ea8db682199fee2757e8275
29	310ec16120315b4ce58601a1e9e5d8c8	NY	R0002	RE001	1900-01-01	2016-01-13	1	rMB447lffsMQCZOQPIUTPusiGmZolvpxn7jMAsiT	F	American Indian or Alaska Native	N	N	N	N	N	Y	\N	\N	Y	Y	Y	Y	N	N	SLD	w3eZOegFEIUSKulQeCRbbgDbj9WbHE1cOvn69gPO	5vlNJePaDUMPzuw3C6SIV6wXpD0TXMjGrn1rIkME	Benevides, Debby Orval	9999-01-01	Debby	Orval	Benevides	343fe3a084b2422796dbd7533a68de13c3cf37
199	e4a5f77e63b8f8c8c588b7ca996a05e9	NY	R0001	RN001	2016-01-13	2199-12-31	2	EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q	M	American Indian or Alaska Native	Y	N	N	N	N	N	\N	\N	Y	Y	Y	N	Y	N	OI	QG0iC7ffrwqWkMtDvQc5YLANyEkjhrq9OWn6Ajov	tJnsdsJVfv8euA4CZzZxDy9ILxhjqB1jV57Bv4bJ	Forst, Darci Betty	0001-09-05	Darci	Betty	Forst	0a4061fcdd3f4e6ea8db682199fee2757e8275
45	e4a5f77e63b8f8c8c588b7ca996a05e9	NY	R0001	RN001	1900-01-01	2016-01-13	1	EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q	M	American Indian or Alaska Native	Y	N	N	N	N	N	\N	\N	Y	Y	Y	N	Y	N	OI	QG0iC7ffrwqWkMtDvQc5YLANyEkjhrq9OWn6Ajov	tJnsdsJVfv8euA4CZzZxDy9ILxhjqB1jV57Bv4bJ	Forst, Darci Betty	0009-02-05	Darci	Betty	Forst	0a4061fcdd3f4e6ea8db682199fee2757e8275
200	f3da5d889630e9697c0939b57b943e9d	NY	R0001	RN001	2016-01-13	2199-12-31	2	2vM9yk86Q3OXKckaeaUjcSTHf1TaO5LCT2DHaeU7	M	American Indian or Alaska Native	N	N	N	N	N	Y	\N	\N	N	Y	N	N	N	Y	ID	Du1Sh4DTO3nLwT357QZPIzYJJfJX2FLFRMUI2hHC	itBQNt3L5ZuR734wODV8UCZOkAImL1QQv3TEbsOZ	Neveu, Betty Cornelius	0002-09-05	Betty	Cornelius	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275
46	f3da5d889630e9697c0939b57b943e9d	NY	R0001	RN001	1900-01-01	2016-01-13	1	2vM9yk86Q3OXKckaeaUjcSTHf1TaO5LCT2DHaeU7	M	American Indian or Alaska Native	N	N	N	N	N	Y	\N	\N	N	Y	N	N	N	Y	ID	Du1Sh4DTO3nLwT357QZPIzYJJfJX2FLFRMUI2hHC	itBQNt3L5ZuR734wODV8UCZOkAImL1QQv3TEbsOZ	Neveu, Betty Cornelius	0009-02-05	Betty	Cornelius	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275
201	76cecc342534d454a99d60ca204e2eeb	NY	R0001	RN001	2016-01-13	2199-12-31	2	dxpojnotJFaxas2YDJMiJoBPHkVCIc4lptuj24Dy	M	American Indian or Alaska Native	N	N	N	N	N	N	\N	\N	N	N	Y	Y	Y	N	SLI	LX9Rwl1Eg9LP92kzBpe5idYStEwlkffCQQY929Su	j8JpKBopcMtBNZwz3JGyhplE018kxwJscvq2xACn	Matsuura, Thurman Kathe	0002-09-05	Thurman	Kathe	Matsuura	0a4061fcdd3f4e6ea8db682199fee2757e8275
47	76cecc342534d454a99d60ca204e2eeb	NY	R0001	RN001	1900-01-01	2016-01-13	1	dxpojnotJFaxas2YDJMiJoBPHkVCIc4lptuj24Dy	M	American Indian or Alaska Native	N	N	N	N	N	N	\N	\N	N	N	Y	Y	Y	N	SLI	LX9Rwl1Eg9LP92kzBpe5idYStEwlkffCQQY929Su	j8JpKBopcMtBNZwz3JGyhplE018kxwJscvq2xACn	Matsuura, Thurman Kathe	0009-02-05	Thurman	Kathe	Matsuura	0a4061fcdd3f4e6ea8db682199fee2757e8275
202	50ed991cd14e3840bbe67034440fdd8d	NY	R0001	RN001	2016-01-13	2199-12-31	2	MyXTFn1S8qz5lQJAjw11CeaMjoaIiQbo8KihC5ix	M	American Indian or Alaska Native	N	N	N	Y	N	N	\N	\N	Y	Y	Y	Y	N	Y	HI	7Jde6xDUfRO1fBo41AFgFQb8WHq7nowhL3abbVeX	3l2uzb7OYZRxJL9Mtkhpopb1ifSlpIOHM7OjH2uA	Gatti, Christin Barbar	0002-09-05	Christin	Barbar	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275
48	50ed991cd14e3840bbe67034440fdd8d	NY	R0001	RN001	1900-01-01	2016-01-13	1	MyXTFn1S8qz5lQJAjw11CeaMjoaIiQbo8KihC5ix	M	American Indian or Alaska Native	N	N	N	Y	N	N	\N	\N	Y	Y	Y	Y	N	Y	HI	7Jde6xDUfRO1fBo41AFgFQb8WHq7nowhL3abbVeX	3l2uzb7OYZRxJL9Mtkhpopb1ifSlpIOHM7OjH2uA	Gatti, Christin Barbar	0009-02-05	Christin	Barbar	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275
203	30f481f254102105b899ed6327fe9ad8	NY	R0001	RN001	2016-01-13	2199-12-31	2	jEEReGK9NLZr2teqi8dFBUhxaozkY3uwy5IB7hUx	M	American Indian or Alaska Native	N	N	N	N	N	Y	\N	\N	Y	Y	N	Y	Y	N	VI	g8QWxVFSTYLQZhxYnK6oinNBz03lbpYTlCnxQPmD	wRTfjzpzwMmHWQDTwwvL3BWXR0PgnLdHkUPqK9lo	Troy, Christin Orval	0002-09-05	Christin	Orval	Troy	0a4061fcdd3f4e6ea8db682199fee2757e8275
1	30f481f254102105b899ed6327fe9ad8	NY	R0001	RN001	1900-01-01	2016-01-13	1	jEEReGK9NLZr2teqi8dFBUhxaozkY3uwy5IB7hUx	M	American Indian or Alaska Native	N	N	N	N	N	Y	\N	\N	Y	Y	N	Y	Y	N	VI	g8QWxVFSTYLQZhxYnK6oinNBz03lbpYTlCnxQPmD	wRTfjzpzwMmHWQDTwwvL3BWXR0PgnLdHkUPqK9lo	Troy, Christin Orval	0009-02-05	Christin	Orval	Troy	0a4061fcdd3f4e6ea8db682199fee2757e8275
204	b43c979e95fba2139ec4749c3530a0d9	NY	R0001	RN001	2016-01-13	2199-12-31	2	1hIns0clANFCb9Rq3Cj5vEUUhMzryD9monuIwTFq	M	Asian	N	N	N	Y	N	N	\N	\N	Y	Y	Y	Y	Y	Y	MD	smSgplUtVjGGlqKHH2bTuPQPpNZZPujM6BpDjMEG	XuYGOYmVHGbw4S2xqjiwPXWiAOtRgtpWswqJFwiU	Monks, Zofia Betty	0002-09-05	Zofia	Betty	Monks	0a4061fcdd3f4e6ea8db682199fee2757e8275
2	b43c979e95fba2139ec4749c3530a0d9	NY	R0001	RN001	1900-01-01	2016-01-13	1	1hIns0clANFCb9Rq3Cj5vEUUhMzryD9monuIwTFq	M	Asian	N	N	N	Y	N	N	\N	\N	Y	Y	Y	Y	Y	Y	MD	smSgplUtVjGGlqKHH2bTuPQPpNZZPujM6BpDjMEG	XuYGOYmVHGbw4S2xqjiwPXWiAOtRgtpWswqJFwiU	Monks, Zofia Betty	0009-02-05	Zofia	Betty	Monks	0a4061fcdd3f4e6ea8db682199fee2757e8275
0	0	N 	UNKNOWN	UNKNOWN	1900-01-01	2199-12-31	0	UNKNOWN	\N	could not resolve	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	UNKNOWN	UNKNOWN	UNKNOWN	UNKNOWN	\N	\N	\N	\N
\.


--
-- Name: dim_student__key_student_seq; Type: SEQUENCE SET; Schema: analytics; Owner: edware
--

SELECT pg_catalog.setval('dim_student__key_student_seq', 204, true);


--
-- Data for Name: dim_test; Type: TABLE DATA; Schema: analytics; Owner: edware
--

COPY dim_test (_key_test, _hash_test, state_id, district_id, _valid_from, _valid_to, _record_version, form_id, test_category, test_subject, asmt_grade, test_code, pba_form_id, eoy_form_id, pba_category, eoy_category) FROM stdin;
0	0	N 	UNKNOWN	1900-01-01	2199-12-31	0	NA	NA	UNKNOWN	UNKNOWN	UNKNOWN	UNKNOWN	UNKNOWN	-	-
\.


--
-- Name: dim_test__key_test_seq; Type: SEQUENCE SET; Schema: analytics; Owner: edware
--

SELECT pg_catalog.setval('dim_test__key_test_seq', 1, false);


--
-- Data for Name: dim_unique_test; Type: TABLE DATA; Schema: analytics; Owner: edware
--

COPY dim_unique_test (_key_unique_test, _valid_from, _valid_to, _record_version, test_code, test_subject, asmt_grade) FROM stdin;
1	1900-01-01	2199-12-31	1	ELA10	English Language Arts/Literacy	Grade 10
2	1900-01-01	2199-12-31	1	ELA03	English Language Arts/Literacy	Grade 3
3	1900-01-01	2199-12-31	1	ELA09	English Language Arts/Literacy	Grade 9
4	1900-01-01	2199-12-31	1	ELA11	English Language Arts/Literacy	Grade 11
5	1900-01-01	2199-12-31	1	MAT03	Mathematics	Grade 3
6	1900-01-01	2199-12-31	1	GEO01	Geometry	\N
7	1900-01-01	2199-12-31	1	GEO01	Geometry	\N
8	1900-01-01	2199-12-31	1	GEO01	Geometry	\N
9	1900-01-01	2199-12-31	1	GEO01	Geometry	\N
10	1900-01-01	2199-12-31	1	GEO01	Geometry	\N
11	1900-01-01	2199-12-31	1	GEO01	Geometry	\N
12	1900-01-01	2199-12-31	1	GEO01	Geometry	\N
13	1900-01-01	2199-12-31	1	GEO01	Geometry	\N
14	1900-01-01	2199-12-31	1	GEO01	Geometry	\N
15	1900-01-01	2199-12-31	1	GEO01	Geometry	\N
16	1900-01-01	2199-12-31	1	GEO01	Geometry	\N
17	1900-01-01	2199-12-31	1	GEO01	Geometry	\N
18	1900-01-01	2199-12-31	1	GEO01	Geometry	\N
19	1900-01-01	2199-12-31	1	GEO01	Geometry	\N
20	1900-01-01	2199-12-31	1	MAT2I	Integrated Mathematics II	\N
21	1900-01-01	2199-12-31	1	MAT2I	Integrated Mathematics II	\N
22	1900-01-01	2199-12-31	1	MAT2I	Integrated Mathematics II	\N
23	1900-01-01	2199-12-31	1	MAT2I	Integrated Mathematics II	\N
24	1900-01-01	2199-12-31	1	MAT2I	Integrated Mathematics II	\N
25	1900-01-01	2199-12-31	1	MAT2I	Integrated Mathematics II	\N
26	1900-01-01	2199-12-31	1	MAT2I	Integrated Mathematics II	\N
27	1900-01-01	2199-12-31	1	MAT2I	Integrated Mathematics II	\N
28	1900-01-01	2199-12-31	1	MAT2I	Integrated Mathematics II	\N
29	1900-01-01	2199-12-31	1	MAT2I	Integrated Mathematics II	\N
30	1900-01-01	2199-12-31	1	MAT1I	Integrated Mathematics I	\N
31	1900-01-01	2199-12-31	1	MAT1I	Integrated Mathematics I	\N
32	1900-01-01	2199-12-31	1	MAT1I	Integrated Mathematics I	\N
33	1900-01-01	2199-12-31	1	MAT1I	Integrated Mathematics I	\N
34	1900-01-01	2199-12-31	1	MAT1I	Integrated Mathematics I	\N
35	1900-01-01	2199-12-31	1	MAT1I	Integrated Mathematics I	\N
36	1900-01-01	2199-12-31	1	MAT1I	Integrated Mathematics I	\N
37	1900-01-01	2199-12-31	1	MAT1I	Integrated Mathematics I	\N
38	1900-01-01	2199-12-31	1	MAT1I	Integrated Mathematics I	\N
39	1900-01-01	2199-12-31	1	MAT1I	Integrated Mathematics I	\N
40	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
41	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
42	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
43	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
44	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
45	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
46	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
47	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
48	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
49	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
50	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
51	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
52	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
53	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
54	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
55	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
56	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
57	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
58	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
59	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
60	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
61	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
62	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
63	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
64	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
65	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
66	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
67	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
68	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
69	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
70	1900-01-01	2199-12-31	1	MAT3I	Integrated Mathematics III	\N
71	1900-01-01	2199-12-31	1	ALG02	Algebra II	\N
72	1900-01-01	2199-12-31	1	ALG02	Algebra II	\N
73	1900-01-01	2199-12-31	1	ALG02	Algebra II	\N
74	1900-01-01	2199-12-31	1	ALG02	Algebra II	\N
75	1900-01-01	2199-12-31	1	ALG02	Algebra II	\N
76	1900-01-01	2199-12-31	1	ALG02	Algebra II	\N
77	1900-01-01	2199-12-31	1	ALG02	Algebra II	\N
78	1900-01-01	2199-12-31	1	ALG02	Algebra II	\N
79	1900-01-01	2199-12-31	1	ALG02	Algebra II	\N
80	1900-01-01	2199-12-31	1	ALG02	Algebra II	\N
81	1900-01-01	2199-12-31	1	ALG02	Algebra II	\N
82	1900-01-01	2199-12-31	1	ALG02	Algebra II	\N
83	1900-01-01	2199-12-31	1	ALG02	Algebra II	\N
84	1900-01-01	2199-12-31	1	ALG02	Algebra II	\N
85	1900-01-01	2199-12-31	1	MAT08	Mathematics	Grade 8
86	1900-01-01	2199-12-31	1	MAT05	Mathematics	Grade 5
87	1900-01-01	2199-12-31	1	ALG01	Algebra I	\N
88	1900-01-01	2199-12-31	1	ALG01	Algebra I	\N
89	1900-01-01	2199-12-31	1	ALG01	Algebra I	\N
90	1900-01-01	2199-12-31	1	ALG01	Algebra I	\N
91	1900-01-01	2199-12-31	1	ALG01	Algebra I	\N
92	1900-01-01	2199-12-31	1	ALG01	Algebra I	\N
93	1900-01-01	2199-12-31	1	ALG01	Algebra I	\N
94	1900-01-01	2199-12-31	1	ALG01	Algebra I	\N
95	1900-01-01	2199-12-31	1	ALG01	Algebra I	\N
96	1900-01-01	2199-12-31	1	ALG01	Algebra I	\N
97	1900-01-01	2199-12-31	1	ALG01	Algebra I	\N
98	1900-01-01	2199-12-31	1	ALG01	Algebra I	\N
99	1900-01-01	2199-12-31	1	ALG01	Algebra I	\N
100	1900-01-01	2199-12-31	1	ALG01	Algebra I	\N
101	1900-01-01	2199-12-31	1	ALG01	Algebra I	\N
102	1900-01-01	2199-12-31	1	ALG01	Algebra I	\N
103	1900-01-01	2199-12-31	1	ALG01	Algebra I	\N
0	1900-01-01	2199-12-31	0	UNKNOWN	UNKNOWN	UNKNOWN
\.


--
-- Name: dim_unique_test__key_unique_test_seq; Type: SEQUENCE SET; Schema: analytics; Owner: edware
--

SELECT pg_catalog.setval('dim_unique_test__key_unique_test_seq', 103, true);


--
-- Data for Name: fact_sum; Type: TABLE DATA; Schema: analytics; Owner: edware
--

COPY fact_sum (rec_id, state_id, district_id, school_id, _key_opt_state_data, _key_poy, _key_resp_school, _key_student, _key_accomod, _key_eoy_test_school, _key_pba_test_school, _key_pba_test, _key_eoy_test, _key_unique_test, student_grade, sum_scale_score, sum_csem, sum_perf_lvl, sum_read_scale_score, sum_read_csem, sum_write_scale_score, sum_write_csem, subclaim1_category, subclaim2_category, subclaim3_category, subclaim4_category, subclaim5_category, subclaim6_category, state_growth_percent, district_growth_percent, parcc_growth_percent, multirecord_flag, result_type, record_type, reported_score_flag, report_suppression_code, report_suppression_action, reported_roster_flag, include_in_parcc, include_in_state, include_in_district, include_in_school, include_in_roster, create_date, me_flag, form_category, pba_test_uuid, eoy_test_uuid, sum_score_rec_uuid, pba_total_items, eoy_total_items, pba_attempt_flag, eoy_attempt_flag, pba_total_items_attempt, eoy_total_items_attempt, pba_total_items_unit1, eoy_total_items_unit1, pba_total_items_unit2, eoy_total_items_unit2, pba_total_items_unit3, eoy_total_items_unit3, pba_total_items_unit4, eoy_total_items_unit4, pba_total_items_unit5, eoy_total_items_unit5, pba_unit1_items_attempt, eoy_unit1_items_attempt, pba_unit2_items_attempt, eoy_unit2_items_attempt, pba_unit3_items_attempt, eoy_unit3_items_attempt, pba_unit4_items_attempt, eoy_unit4_items_attempt, pba_unit5_items_attempt, eoy_unit5_items_attempt, pba_not_tested_reason, eoy_not_tested_reason, pba_void_reason, eoy_void_reason, student_parcc_id) FROM stdin;
275	N 	R0003	UNKNOWN	21	1	5	149	21	0	0	0	0	53	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	N 	1	03	N       	2 	96	Y	N	N	N	N	W	2016-01-13	math	\N	\N	2bc49b90-c05e-46df-92f2-75d41e09c4ee	7823823c-1ef0-4993-b00a-61389b4e1463	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1 	1 	69	69	OLbCzYnv9jHAVbFnfaD1AtFb2FzNSLjhmkjdFdbJ
207	N 	R0001	UNKNOWN	1	1	1	1	1	0	0	0	0	1	10	332	3	4	41	3	42	3	3	3	2	3	3	\N	50.00	52.00	46.00	- 	1	01	Y       	3 	3	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	bf21bb13-abd7-4ac3-89c9-8c3c7214ca4c	52846912-1b9d-4f00-962a-92dd043ac63f	100	\N	\N	\N	94	\N	8	8	20	20	0	0	15	15	45	45	80	80	89	89	42	42	56	56	68	68	4 	4 	1 	1 	jEEReGK9NLZr2teqi8dFBUhxaozkY3uwy5IB7hUx
208	N 	R0001	UNKNOWN	2	1	1	2	2	0	0	0	0	1	10	333	7	4	42	4	43	1	2	1	2	3	2	\N	86.00	67.00	85.00	- 	1	02	N       	\N	0	Y	N	N	N	N	W	2016-01-13	ela	\N	\N	9c74587a-e7c9-4d30-af66-15399b941ca7	0469613b-39e9-43dc-80e5-84fdd693296a	33	\N	\N	\N	21	\N	38	38	84	84	94	94	86	86	59	59	31	31	35	35	52	52	73	73	49	49	5 	5 	34	34	1hIns0clANFCb9Rq3Cj5vEUUhMzryD9monuIwTFq
209	N 	R0001	UNKNOWN	3	1	1	3	3	0	0	0	0	1	10	219	9	3	23	2	22	1	2	4	2	2	2	\N	47.00	95.00	81.00	- 	1	01	Y       	2 	2	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	cdae1117-12b9-4077-be15-af2eb5db0e7a	40ab40d0-1b80-4552-81ae-baa0502c5ff6	76	\N	\N	\N	26	\N	36	36	5	5	31	31	24	24	70	70	58	58	38	38	81	81	45	45	8	8	6 	6 	57	57	SBFMQrkvsLNLQWnEXVVeN6ZMQXqemPhTY44hgO2M
210	N 	R0001	UNKNOWN	4	1	1	4	4	0	0	0	0	1	10	467	1	5	46	1	50	1	2	1	3	2	2	\N	77.00	61.00	75.00	- 	1	01	N       	\N	0	N	N	N	N	N	N	2016-01-13	ela	\N	\N	89450f20-5621-4cc8-bd71-709834a8631e	abe70eed-50ad-4ea0-9143-03724edfa770	1	\N	\N	\N	41	\N	22	22	53	53	24	24	44	44	60	60	80	80	7	7	60	60	94	94	48	48	10	10	48	48	OIeWicvTvk5kcbEc0qj7vPo2nh3Vr8XS9e2Vz57b
211	N 	R0002	UNKNOWN	5	1	2	5	5	0	0	0	0	1	10	182	2	2	14	1	15	1	2	1	3	3	1	\N	76.00	23.00	62.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	80770855-70a5-432b-921d-74930d96679d	ebc63927-bd2a-48b5-9c4d-0c40d0655f85	57	\N	\N	\N	45	\N	13	13	97	97	40	40	39	39	1	1	82	82	37	37	35	35	5	5	14	14	2 	2 	48	48	uB6VLZuP4JnB0aZW3u9Ol9pUS9ux9VdAuAaQ19fX
212	N 	R0001	UNKNOWN	6	1	1	6	6	0	0	0	0	1	10	451	6	5	50	6	47	6	1	2	3	2	3	\N	64.00	11.00	25.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	39d27e35-2285-4c06-83ab-a00f52537471	3063220f-8693-4b43-8d75-69408e06447e	26	\N	\N	\N	86	\N	46	46	84	84	37	37	27	27	19	19	0	0	42	42	60	60	48	48	37	37	6 	6 	91	91	RqHsCUovoRLSAhySU0AnRx0LY8YKin9uDhSFcOMD
213	N 	R0002	UNKNOWN	7	1	2	7	7	0	0	0	0	1	10	223	8	3	30	8	29	2	3	3	3	3	1	\N	70.00	30.00	46.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	28e46644-56b0-439d-aece-4a88f147b218	b22f03bf-189f-432a-ad69-247bfddbb66d	75	\N	\N	\N	26	\N	96	96	14	14	42	42	50	50	46	46	65	65	90	90	19	19	70	70	17	17	2 	2 	43	43	JskCum0I6ZULH064xsFeFcW4eZVTIEj5m6zExxG2
214	N 	R0002	UNKNOWN	8	1	2	8	8	0	0	0	0	1	10	202	5	3	29	4	30	2	2	3	2	2	3	\N	66.00	97.00	45.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	4243c86e-837a-4255-b667-7b4b8195862f	d0e1956e-5335-4025-b912-59e7fe5dcd82	24	\N	\N	\N	99	\N	13	13	34	34	40	40	14	14	59	59	55	55	98	98	80	80	89	89	78	78	2 	2 	16	16	EOHTQnkhMw4mzKP8Rpaq42vOlOBZzsZZkKU6zpll
215	N 	R0002	UNKNOWN	9	1	2	9	9	0	0	0	0	1	10	206	4	3	30	1	31	1	2	2	2	2	3	\N	13.00	99.00	84.00	- 	1	01	Y       	3 	3	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	1ec80e05-8f1c-4a65-9349-14694bbbac78	81e942ae-af3c-40ae-9f38-1e72f10ea569	94	\N	\N	\N	39	\N	17	17	13	13	22	22	82	82	76	76	78	78	1	1	67	67	30	30	67	67	1 	1 	62	62	UH69Q4IEQrjnQGvI9xhrNRTWUtmihpX8hlQ4r61X
216	N 	R0002	UNKNOWN	10	1	2	10	10	0	0	0	0	1	10	212	4	3	31	1	32	1	2	2	2	2	3	\N	51.00	39.00	36.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	307a76ba-cda8-43fc-8953-55795759032e	511b0c83-dce0-4ff7-bbac-776a5b3d7e7e	37	\N	\N	\N	54	\N	11	11	26	26	80	80	18	18	97	97	54	54	54	54	5	5	98	98	50	50	1 	1 	76	76	Fg5kGWmyfjDA600mHsVyZJALKlRwmg8pe1LtPdSb
217	N 	R0003	UNKNOWN	11	1	3	11	11	0	0	0	0	1	10	248	1	3	35	1	38	1	3	3	3	3	3	\N	98.00	58.00	55.00	- 	1	03	N       	\N	0	N	N	N	N	N	N	2016-01-13	ela	\N	\N	94a06c65-474b-423f-9eb8-59ac8cf496db	cfd67a8e-0659-4ddb-aa65-348d6027541c	78	\N	\N	\N	56	\N	88	88	2	2	71	71	57	57	29	29	94	94	44	44	39	39	43	43	15	15	3 	3 	78	78	E9fHez5Rp8SfdyyhzYgAlyLyIE1JYmpYL8d9Up4W
218	N 	R0003	UNKNOWN	12	1	3	12	12	0	0	0	0	1	10	377	10	4	41	9	44	5	2	3	2	2	2	\N	18.00	62.00	32.00	- 	1	02	N       	\N	0	Y	N	N	N	N	W	2016-01-13	ela	\N	\N	cf537852-5ba0-4af8-ad17-996d07c0a040	ae14d043-fbb7-414a-9836-89e6328cb0ae	38	\N	\N	\N	68	\N	16	16	58	58	13	13	10	10	89	89	43	43	42	42	96	96	37	37	82	82	3 	3 	16	16	haGegI5PgD6OzaQ1Mu478Og0zw693ZmLAgQTbjLb
219	N 	R0003	UNKNOWN	13	1	3	13	13	0	0	0	0	1	10	456	10	5	50	9	48	8	2	2	2	2	2	\N	11.00	29.00	56.00	- 	1	03	N       	\N	0	N	N	N	N	N	N	2016-01-13	ela	\N	\N	81b4561e-6e70-47b4-8dfe-2898e550de7f	0a6965e4-9ee6-4fc8-82db-17403876830f	13	\N	\N	\N	1	\N	56	56	44	44	79	79	9	9	64	64	22	22	68	68	74	74	57	57	40	40	3 	3 	30	30	U49Ivq4Y5D2NTB0EAuHv0sKhVA90oVkCwOgJErDW
220	N 	R0003	UNKNOWN	14	1	3	14	14	0	0	0	0	1	10	471	6	5	47	3	49	3	2	1	2	2	2	\N	58.00	90.00	84.00	- 	1	01	N       	\N	0	N	N	N	N	N	N	2016-01-13	ela	\N	\N	7fec724b-1594-4e0e-9d08-a539a77b3c99	2b3db56d-98b3-47f4-9324-73d4733c781a	34	\N	\N	\N	40	\N	66	66	63	63	26	26	61	61	69	69	87	87	64	64	34	34	58	58	78	78	3 	3 	13	13	Cnt3veDS2HW94jTAnU4bUASdLgIbP9y0lvQpQqer
221	N 	R0003	UNKNOWN	15	1	4	15	15	0	0	0	0	1	10	137	7	2	17	5	15	5	3	2	3	3	3	\N	35.00	86.00	61.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	0483d072-9b85-49b5-b542-95dbcfca1a06	6d0d60f1-4370-46f2-863e-d913ec3af726	52	\N	\N	\N	88	\N	61	61	38	38	47	47	33	33	18	18	82	82	60	60	72	72	73	73	98	98	4 	4 	58	58	p2Sf5xBg8rAuTjy9XPGuFJaoYhdKIHISnVuRqoUd
222	N 	R0003	UNKNOWN	16	1	4	16	16	0	0	0	0	1	10	198	9	2	18	2	16	2	3	2	3	3	3	\N	40.00	42.00	68.00	- 	1	01	Y       	5 	5	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	1e71359d-b18d-4b83-a613-1a9f52f89d9e	8a4eae54-8561-4bdc-b69c-cbca486cb465	25	\N	\N	\N	80	\N	54	54	8	8	13	13	33	33	71	71	40	40	22	22	97	97	54	54	72	72	4 	4 	80	80	sEorUakkZr2QwQgeQ1wCHxo8NwkksVvRlPXQV7cL
223	N 	R0003	UNKNOWN	17	1	4	17	17	0	0	0	0	1	10	102	7	2	19	7	17	6	3	2	3	3	3	\N	66.00	88.00	26.00	- 	1	02	N       	\N	0	N	N	N	N	N	N	2016-01-13	ela	\N	\N	95a45098-3643-421f-9ef0-99e7e683b52a	53b7291d-5875-47d8-b4e4-e572eccebdf5	8	\N	\N	\N	42	\N	61	61	72	72	45	45	22	22	14	14	15	15	31	31	3	3	87	87	79	79	5 	5 	72	72	Sc0cLMiRhBfkJsHF7Dwz1ozIPzc1k3s6aOgAD2IT
224	N 	R0003	UNKNOWN	18	1	4	18	18	0	0	0	0	1	10	202	9	3	33	9	37	1	1	1	1	1	1	\N	66.00	82.00	34.00	- 	1	03	N       	\N	0	N	N	N	N	N	N	2016-01-13	ela	\N	\N	6a996b2d-7fe2-40ad-a34b-facf7b9034db	31ec13ad-d195-435a-b060-d99c60b46bf1	81	\N	\N	\N	65	\N	36	36	65	65	14	14	67	67	4	4	32	32	75	75	71	71	88	88	90	90	5 	5 	10	10	ZmrZFBiOmI2H2QuEsZm59MMu7bIAhKHUjge92f6E
225	N 	R0003	UNKNOWN	19	1	4	19	19	0	0	0	0	1	10	204	7	3	36	1	32	1	3	3	3	3	3	\N	12.00	80.00	74.00	- 	1	02	N       	\N	0	Y	N	N	N	N	W	2016-01-13	ela	\N	\N	028647c5-df78-46dc-b0b9-760586f55fc8	22a51fde-87cf-40d9-884f-cb58d92a7aae	44	\N	\N	\N	62	\N	7	7	84	84	82	82	11	11	20	20	25	25	12	12	41	41	56	56	41	41	5 	5 	94	94	sB2VXSoaEMtiSg4WjFUXNNdh3rDSEvULHmGXvAnG
226	N 	R0003	UNKNOWN	20	1	5	20	20	0	0	0	0	1	10	194	2	2	13	2	16	2	1	2	1	1	1	\N	70.00	11.00	84.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	77c6d6a6-9b59-4189-b11b-75aedde7511c	c829124c-0544-4340-8ae3-e1f0eaf7f586	45	\N	\N	\N	35	\N	23	23	4	4	22	22	43	43	74	74	54	54	82	82	90	90	97	97	27	27	1 	1 	52	52	xhHUuTmLdRjylYGiO2ZU54h2J04u9BJSn0exbdX2
227	N 	R0003	UNKNOWN	21	1	5	21	21	0	0	0	0	1	10	287	2	3	36	2	37	1	2	3	2	2	2	\N	12.00	59.00	62.00	- 	1	03	N       	\N	0	Y	N	N	N	N	W	2016-01-13	ela	\N	\N	2bc49b90-c05e-46df-92f2-75d41e09c4ee	e90f3dbd-bb0e-487f-b5ea-9fdf51446e27	60	\N	\N	\N	15	\N	1	1	71	71	35	35	2	2	29	29	39	39	40	40	40	40	32	32	17	17	1 	1 	69	69	OLbCzYnv9jHAVbFnfaD1AtFb2FzNSLjhmkjdFdbJ
228	N 	R0003	UNKNOWN	22	1	5	22	22	0	0	0	0	1	10	216	3	3	37	2	33	2	3	3	3	3	3	\N	10.00	81.00	45.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	76040d10-ba95-444b-a29e-da65a3679d01	7fce8cc3-59cc-4559-ae49-8e490448e90a	57	\N	\N	\N	19	\N	48	48	80	80	89	89	92	92	64	64	15	15	75	75	91	91	50	50	98	98	1 	1 	97	97	8qadBD3xrsvpfExXtJw6dHilQMcdZHjWg60GuV8D
229	N 	R0003	UNKNOWN	23	1	5	23	23	0	0	0	0	1	10	340	3	4	45	3	44	1	1	2	1	1	1	\N	62.00	38.00	17.00	- 	1	02	N       	\N	0	Y	N	N	N	N	W	2016-01-13	ela	\N	\N	c2043a85-fb32-440a-8ca1-9f1f657aa4a2	76a9f327-a761-4d46-bdeb-d1b06f942509	78	\N	\N	\N	70	\N	64	64	47	47	59	59	5	5	65	65	78	78	61	61	1	1	91	91	92	92	4 	4 	50	50	DSoDwPOg0kZQ5YPnMJ0SLmBaCeGes4a1qjkRUQfd
230	N 	R0003	UNKNOWN	24	1	5	24	24	0	0	0	0	1	10	278	3	3	38	3	35	2	2	3	2	2	2	\N	34.00	10.00	17.00	- 	1	03	N       	\N	0	Y	N	N	N	N	W	2016-01-13	ela	\N	\N	1101d194-08b9-4632-b655-513319ed9a07	401cc9b4-9c05-4ca2-bed2-90ce3f2d5427	55	\N	\N	\N	19	\N	84	84	70	70	92	92	59	59	62	62	92	92	24	24	89	89	24	24	97	97	8 	8 	29	29	WW9iCRpiTricUbWoelchVevYIJHSjhAUMpLVYt6D
232	N 	R0001	UNKNOWN	25	1	1	25	25	0	0	0	0	2	03	227	2	3	27	1	26	1	2	2	2	3	2	\N	39.00	16.00	86.00	- 	1	03	N       	\N	0	Y	N	N	N	N	W	2016-01-13	ela	\N	\N	771d6032-621f-47b3-8113-2aeffcb7bffa	f6eaeed1-e2d1-4b2d-8c64-ffe01a832b07	53	\N	\N	\N	4	\N	70	70	84	84	37	37	66	66	28	28	86	86	50	50	69	69	65	65	18	18	5 	5 	87	87	UMaYGd8hto4TYc7EiWKtivzti8YAWg5xozomFI8q
233	N 	R0001	UNKNOWN	26	1	1	26	26	0	0	0	0	2	03	355	2	4	42	2	41	1	3	3	1	1	2	\N	63.00	59.00	46.00	- 	1	02	N       	\N	0	Y	N	N	N	N	W	2016-01-13	ela	\N	\N	bb63c6c4-07db-4b0a-974f-59fcaf94c189	1d14762b-ae4b-4332-8045-6b9c51dd7880	3	\N	\N	\N	31	\N	71	71	96	96	11	11	72	72	21	21	92	92	34	34	64	64	45	45	81	81	3 	3 	25	25	qEy737ea1Emx97d0RTxk7QpDWecnUu2eEBWvQXkK
234	N 	R0001	UNKNOWN	27	1	1	27	27	0	0	0	0	2	03	360	8	4	43	3	42	2	3	2	2	2	2	\N	88.00	69.00	87.00	- 	1	01	N       	\N	0	N	N	N	N	N	N	2016-01-13	ela	\N	\N	92349e8e-73bc-46cf-9ed2-5025cb2c4857	50c6dc1b-634f-4561-9271-7122ddf9390c	52	\N	\N	\N	64	\N	56	56	44	44	67	67	24	24	93	93	95	95	70	70	85	85	10	10	34	34	7 	7 	91	91	MXGplB4M232z2j4FNTStjIAKRER2ujJ8Z2OlV7Yn
235	N 	R0001	UNKNOWN	28	1	1	28	28	0	0	0	0	2	03	448	8	5	47	7	50	5	2	1	2	3	1	\N	10.00	43.00	36.00	- 	1	01	Y       	01	6	N	Y	Y	Y	N	Y	2016-01-13	ela	\N	\N	447eef05-927f-4a94-baaa-593c538366d0	deaf9408-c943-46bd-bbe0-1a79df79f227	17	\N	\N	\N	41	\N	30	30	16	16	57	57	98	98	15	15	41	41	78	78	8	8	3	3	10	10	20	20	98	98	vwkr0R9sEgA872YY02LOWZFwoTLq0ATfnEvZ0i17
236	N 	R0002	UNKNOWN	29	1	2	29	29	0	0	0	0	2	03	128	3	2	15	3	16	3	1	3	3	3	2	\N	25.00	23.00	18.00	- 	1	01	N       	01	1	N	N	N	N	N	N	2016-01-13	ela	\N	\N	48544429-c044-413a-8d7e-cbf933e6128e	dadb7cf2-ac24-4e07-96ad-9599c55d5a89	92	\N	\N	\N	88	\N	86	86	35	35	98	98	89	89	3	3	52	52	75	75	44	44	20	20	34	34	3 	3 	75	75	rMB447lffsMQCZOQPIUTPusiGmZolvpxn7jMAsiT
237	N 	R0002	UNKNOWN	30	1	2	30	30	0	0	0	0	2	03	246	3	3	32	3	31	1	3	3	3	3	3	\N	53.00	28.00	27.00	- 	1	01	Y       	01	2	N	N	N	N	N	Y	2016-01-13	ela	\N	\N	54488102-dc6f-45fb-8941-14b9dc51c977	1d3c68bb-dbc5-40f3-be4f-086a1b239f52	18	\N	\N	\N	53	\N	63	63	23	23	96	96	26	26	5	5	63	63	4	4	39	39	14	14	22	22	4 	4 	20	20	3i8VkcJCs5QffVgj2QOmZr3MLv40I0KJc5k5DcWe
239	N 	R0002	UNKNOWN	31	1	2	31	31	0	0	0	0	2	03	285	3	3	33	2	29	1	3	2	1	3	2	\N	26.00	58.00	16.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	93e5e92a-27bb-4683-b84f-6f661c9e47a2	5a5c3cb1-4038-4519-b1a4-a2808840afc1	50	\N	\N	\N	63	\N	23	23	72	72	44	44	80	80	98	98	62	62	1	1	95	95	41	41	1	1	40	40	88	88	N1eKk2eGo15Htuapk7zDnIIftLz18aVE1PMPSktf
240	N 	R0002	UNKNOWN	32	1	2	32	32	0	0	0	0	2	03	132	10	2	16	2	15	1	1	1	1	1	3	\N	46.00	60.00	13.00	- 	1	01	N       	01	3	N	N	N	N	N	N	2016-01-13	ela	\N	\N	debff429-22ea-48a9-b5f0-feeddefc2e38	1bdd6b6e-9b36-47b0-8502-ac842e86dc80	27	\N	\N	\N	44	\N	89	89	29	29	92	92	43	43	27	27	56	56	20	20	53	53	97	97	18	18	3 	3 	36	36	pB7SBPI9whDTX6jqMTvlCa5POI7wiii3ik8YSqUN
241	N 	R0003	UNKNOWN	33	1	3	33	33	0	0	0	0	2	03	248	2	3	37	1	35	1	1	1	1	1	1	\N	14.00	36.00	25.00	- 	1	02	N       	\N	0	Y	N	N	N	N	W	2016-01-13	ela	\N	\N	7b12d2d9-0b6d-4f75-9d8c-398f80f58062	e3c74b24-345f-4177-907c-191804fab597	86	\N	\N	\N	53	\N	67	67	35	35	89	89	56	56	56	56	44	44	44	44	51	51	37	37	83	83	3 	3 	68	68	hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ
242	N 	R0003	UNKNOWN	34	1	3	34	34	0	0	0	0	2	03	254	5	3	38	4	36	2	2	2	2	2	2	\N	91.00	74.00	51.00	- 	1	01	Y       	01	5	N	N	N	N	N	N	2016-01-13	ela	\N	\N	614e4fd1-5442-402f-82c8-3d2ea5f50e7b	4dd864b9-d6d4-4778-832c-2c005e31ba4a	87	\N	\N	\N	7	\N	32	32	52	52	97	97	49	49	26	26	43	43	49	49	38	38	33	33	67	67	44	44	57	57	zxITiM66QUVOcTyhRaooB0UIOWemGFP8eQE7xPSx
243	N 	R0003	UNKNOWN	35	1	3	35	35	0	0	0	0	2	03	319	10	4	44	3	41	2	2	2	2	2	2	\N	16.00	67.00	78.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	96ba894d-7749-48f9-9ef5-3019f562f963	c1cb032a-7b58-499a-9999-3d273c9dcccd	71	\N	\N	\N	73	\N	27	27	2	2	83	83	46	46	22	22	3	3	97	97	20	20	29	29	57	57	4 	4 	98	98	6xduCZW2oSUGSLzpiFpJQQK8YSAzmBrMjAh4WEkn
244	N 	R0003	UNKNOWN	36	1	3	36	36	0	0	0	0	2	03	466	3	5	47	2	48	2	1	3	1	1	1	\N	64.00	17.00	62.00	- 	1	02	N       	\N	0	N	N	N	N	N	N	2016-01-13	ela	\N	\N	8d54686a-ff28-415a-8ee5-95cb999600f0	b30bcb85-5e27-4268-bccc-914312dc90c3	98	\N	\N	\N	67	\N	35	35	89	89	27	27	73	73	10	10	69	69	40	40	57	57	89	89	86	86	4 	4 	68	68	wUfuK1C1vpevOq8cV6oqQxNNwJV1TbzeraL8fC4v
246	N 	R0003	UNKNOWN	37	1	4	37	37	0	0	0	0	2	03	53	5	1	2	2	2	2	2	3	2	2	2	\N	22.00	90.00	25.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	57c8411c-3380-418e-b4b6-9da2dbccb6fb	dccdc883-cf47-49a4-9d74-0d0526e9be5d	22	\N	\N	\N	78	\N	70	70	36	36	45	45	30	30	20	20	34	34	46	46	32	32	92	92	68	68	5 	5 	24	24	QitKZjIhO0SovQYzrO9PfV1o1b0P0xRtKwJwFRqc
247	N 	R0003	UNKNOWN	38	1	4	38	38	0	0	0	0	2	03	222	2	3	37	2	33	2	1	3	1	1	1	\N	25.00	74.00	43.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	af618355-c475-4f32-a6da-ca83fc385dab	70883c09-7020-4d4c-81f8-bfb99f363517	25	\N	\N	\N	15	\N	47	47	61	61	6	6	73	73	85	85	91	91	93	93	42	42	29	29	69	69	22	22	58	58	c0pT5dPt1QuTicyE2IWo13jRf8kh2ep7ZI8flg9g
248	N 	R0003	UNKNOWN	39	1	5	39	39	0	0	0	0	2	03	124	4	2	14	1	11	1	3	1	3	3	3	\N	18.00	23.00	35.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	e13c29ed-0577-46bd-b6c0-86092c68a12b	7357ff31-682b-4136-ae3d-0ee55ac492b6	22	\N	\N	\N	5	\N	82	82	97	97	74	74	6	6	66	66	34	34	92	92	36	36	38	38	30	30	5 	5 	24	24	w7TmiOs6CWJsXS2l1crivMsf42ZlQ5WAr6Xri2rd
249	N 	R0003	UNKNOWN	40	1	5	40	40	0	0	0	0	2	03	191	9	2	15	6	12	4	3	2	3	3	3	\N	57.00	90.00	65.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	eed942f3-d554-40f2-b547-446a9e1165bf	db651bc6-76a5-4ef2-992d-be10881592cc	1	\N	\N	\N	3	\N	30	30	78	78	28	28	16	16	61	61	2	2	63	63	39	39	72	72	58	58	5 	5 	59	59	e3OGNkIbM4h2skSI4nPuESDwrF1EMmjIfrprGMHz
250	N 	R0003	UNKNOWN	41	1	5	41	41	0	0	0	0	2	03	294	10	3	40	6	37	6	2	3	2	2	2	\N	56.00	49.00	99.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	a16f02a7-151a-4b29-8203-8568a8746591	ed5311ed-17fb-4b5c-bfda-c006a01e9cff	56	\N	\N	\N	100	\N	88	88	21	21	91	91	51	51	58	58	64	64	36	36	50	50	67	67	84	84	8 	8 	86	86	oBPTLwB89Yetjpa88VqxOvlIENOqe4XKGKnOrJfM
251	N 	R0003	UNKNOWN	42	1	5	42	42	0	0	0	0	2	03	305	1	4	44	1	42	1	3	2	3	3	3	\N	57.00	41.00	87.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	22b58ccc-2418-4508-bc73-12b3ff68dac3	d6bea7d9-d90d-4f99-ab64-080e3c5ee53a	96	\N	\N	\N	68	\N	63	63	82	82	63	63	80	80	46	46	11	11	46	46	39	39	80	80	43	43	8 	8 	3 	3 	6boPVVoO1Vuyg8VJ5AqV0Zha4rVGrZt3uPrM7xgw
252	N 	R0003	UNKNOWN	43	1	5	43	43	0	0	0	0	2	03	365	10	4	45	3	44	1	3	3	3	3	3	\N	68.00	41.00	54.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	0b674c27-ad68-4597-91bc-01fe8e4d57d2	08dde853-be20-4883-b263-ab8ed18cc3ec	71	\N	\N	\N	72	\N	72	72	71	71	28	28	31	31	40	40	49	49	99	99	65	65	11	11	2	2	8 	8 	94	94	KL3R8L1EaO8MgymDcMI59fe7DnjLk0ZuDZiwO2J2
253	N 	R0001	UNKNOWN	44	1	1	44	44	0	0	0	0	2	03	335	6	4	43	1	44	1	2	2	3	1	3	\N	86.00	93.00	77.00	Y 	1	01	Y       	01	1	N	N	N	N	N	N	2016-01-13	ela	\N	\N	13162932-2ee6-4d59-b595-fb23ff0b8efd	94c13427-7897-4e76-8fa7-26cdf3ee6e5b	46	\N	\N	\N	94	\N	38	38	92	92	12	12	59	59	55	55	4	4	62	62	46	46	56	56	75	75	0 	0 	39	39	5336b7258eb545b3ae7178aeb0cfd72428a36fdb
254	N 	R0001	UNKNOWN	44	1	1	45	44	0	0	0	0	3	09	254	5	3	21	2	24	2	1	1	3	2	1	\N	44.00	63.00	14.00	- 	1	01	Y       	1 	1	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	13162932-2ee6-4d59-b595-fb23ff0b8efd	9dab4c6f-856e-4b76-93dd-bdffef2204fa	46	\N	\N	\N	94	\N	38	38	92	92	12	12	59	59	55	55	4	4	62	62	46	46	56	56	75	75	0 	0 	39	39	EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q
255	N 	R0001	UNKNOWN	45	1	1	46	45	0	0	0	0	3	09	277	2	3	22	1	25	1	1	1	3	2	1	\N	25.00	54.00	16.00	- 	1	02	N       	\N	0	Y	N	N	N	N	W	2016-01-13	ela	\N	\N	d21f2726-9b67-4e37-845a-06fd98459c55	6f08734f-3e70-4c60-a13c-018cb909000f	7	\N	\N	\N	15	\N	26	26	45	45	70	70	49	49	45	45	15	15	96	96	53	53	4	4	38	38	1 	1 	83	83	2vM9yk86Q3OXKckaeaUjcSTHf1TaO5LCT2DHaeU7
256	N 	R0001	UNKNOWN	46	1	1	47	46	0	0	0	0	3	09	207	8	3	23	1	26	1	2	2	1	3	2	\N	88.00	16.00	38.00	- 	1	02	N       	1 	1	Y	N	N	N	N	W	2016-01-13	ela	\N	\N	1b7f6c16-8846-47fb-b925-581b4bead589	7a81b60d-d720-4cad-8e94-38320c986263	19	\N	\N	\N	45	\N	92	92	42	42	94	94	69	69	41	41	50	50	45	45	97	97	64	64	28	28	2 	2 	54	54	dxpojnotJFaxas2YDJMiJoBPHkVCIc4lptuj24Dy
257	N 	R0001	UNKNOWN	47	1	1	48	47	0	0	0	0	3	09	212	7	3	24	5	27	4	3	3	2	3	3	\N	96.00	84.00	95.00	- 	1	01	Y       	2 	2	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	1f4e4ff7-80f1-4fb8-87c6-4a6502bb24d7	c3b1df9b-9c45-40c0-a776-41d35e3591ae	85	\N	\N	\N	32	\N	21	21	89	89	76	76	98	98	97	97	3	3	68	68	72	72	38	38	35	35	3 	3 	96	96	MyXTFn1S8qz5lQJAjw11CeaMjoaIiQbo8KihC5ix
258	N 	R0001	UNKNOWN	48	1	1	49	48	0	0	0	0	3	09	228	7	3	37	5	40	3	2	2	3	1	2	\N	34.00	52.00	53.00	- 	1	02	N       	\N	0	N	N	N	N	N	N	2016-01-13	ela	\N	\N	d0af2f64-adec-4ab8-b9e0-ce0f2e9631cf	1d6381ec-e35c-4f1d-a535-b4768b020907	32	\N	\N	\N	90	\N	69	69	64	64	38	38	78	78	54	54	89	89	76	76	45	45	71	71	41	41	8 	8 	34	34	ITh1jmueUE6G8rMT9oKqLXZKZMPKahZUoAUuvOQE
259	N 	R0002	UNKNOWN	49	1	2	50	49	0	0	0	0	3	09	168	1	2	12	1	13	1	3	1	3	3	2	\N	40.00	81.00	25.00	- 	1	01	N       	5 	5	N	N	N	N	N	N	2016-01-13	ela	\N	\N	ba04ad27-ce18-42d8-ba7f-75bebee4ca6e	624ef560-63a9-42cd-a731-50132009fd89	37	\N	\N	\N	69	\N	59	59	96	96	60	60	98	98	87	87	74	74	35	35	99	99	40	40	84	84	10	10	55	55	mqZIVP2jMSn1VHrJ6nLaEHLblTiXW3eCC0BZDA6d
260	N 	R0002	UNKNOWN	50	1	2	51	50	0	0	0	0	3	09	154	2	2	13	1	14	1	3	2	3	2	3	\N	44.00	15.00	28.00	- 	1	03	N       	\N	0	N	N	N	N	N	N	2016-01-13	ela	\N	\N	207191be-0e63-4879-b5af-ec0d5b5c6426	8e1239c5-4363-44e2-b415-9c13558deecb	21	\N	\N	\N	79	\N	47	47	83	83	12	12	32	32	27	27	62	62	58	58	7	7	83	83	18	18	11	11	2 	2 	Ew6FoKuAKYas7HNCC2qpWTLldXJFPKY6WuKGaoCK
261	N 	R0002	UNKNOWN	51	1	2	52	51	0	0	0	0	3	09	331	2	4	44	1	41	1	3	2	3	3	3	\N	19.00	93.00	38.00	- 	1	01	Y       	2 	2	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	67c4ce05-a8cd-43f8-af8e-075fb70b3ecb	d0111504-71b2-44a7-b543-fd813b5e1369	25	\N	\N	\N	93	\N	73	73	85	85	18	18	4	4	13	13	9	9	64	64	2	2	57	57	6	6	1 	1 	26	26	uJkYBTpWyQhQPsBC4PFyGIFqcoyKQiHxv4f3IYsu
262	N 	R0002	UNKNOWN	52	1	2	53	52	0	0	0	0	3	09	391	4	4	45	3	42	2	1	2	1	1	1	\N	50.00	76.00	61.00	- 	1	03	N       	\N	0	N	N	N	N	N	N	2016-01-13	ela	\N	\N	91b0e6a7-e546-4c7f-8d51-d1890957b3c8	65a4a1c4-9409-4d2d-80c0-6a8abe0e28a9	41	\N	\N	\N	18	\N	68	68	61	61	12	12	31	31	54	54	57	57	59	59	83	83	55	55	14	14	1 	1 	33	33	0YrEK4Y3PihDRZiZnFMinOcTdkNYwublx9Z8lCAp
263	N 	R0002	UNKNOWN	53	1	2	54	53	0	0	0	0	3	09	449	6	5	47	6	49	1	1	2	1	1	1	\N	72.00	73.00	59.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	b681b620-edd4-4a50-affa-47774eff0914	a32523b7-52e4-4d51-98d9-ec5a3552020f	33	\N	\N	\N	7	\N	1	1	60	60	55	55	67	67	1	1	2	2	25	25	28	28	33	33	22	22	1 	1 	22	22	1SWd4Oky5wnWUNuH078iTM1reO6FjlIYmYkVHcQw
264	N 	R0003	UNKNOWN	54	1	3	55	54	0	0	0	0	3	09	261	1	3	28	1	29	1	2	2	2	2	1	\N	98.00	28.00	33.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	905fdbd3-2337-457b-a38b-90fc87f615c2	02f5f8ca-ee0d-495e-bb2f-b7e4b6b938aa	25	\N	\N	\N	77	\N	47	47	65	65	88	88	7	7	3	3	23	23	26	26	16	16	22	22	37	37	1 	1 	64	64	Z94Y0ynyVyq3VCwLTjIRZHTZSYkFHT0lBN8meOgb
265	N 	R0003	UNKNOWN	55	1	3	56	55	0	0	0	0	3	09	237	3	3	29	3	27	2	3	1	3	3	1	\N	90.00	62.00	55.00	- 	1	01	N       	\N	0	N	N	N	N	N	N	2016-01-13	ela	\N	\N	805d1318-86f8-49d7-aee4-527a22808b36	b1290688-a0ff-43fa-a825-e7d7ed05ba6a	97	\N	\N	\N	63	\N	65	65	94	94	11	11	20	20	98	98	2	2	59	59	95	95	99	99	47	47	3 	3 	69	69	maHNc5BQaVj9ddANVpBvFluBikLI3H06gqH3P1zi
266	N 	R0003	UNKNOWN	56	1	3	57	56	0	0	0	0	3	09	229	7	3	36	1	35	1	2	2	2	2	2	\N	77.00	82.00	79.00	- 	1	01	Y       	01	3	N	N	N	N	N	W	2016-01-13	ela	\N	\N	00737d05-b0bd-46b9-80a6-20bf79cf761a	8baac5ea-2056-4afd-9b7a-2cb16ea5ee61	52	\N	\N	\N	34	\N	42	42	54	54	73	73	42	42	67	67	69	69	26	26	91	91	13	13	12	12	3 	3 	79	79	r1s4BnsoqFMAxUnLv1q3GbBD9SWWz0yUXPaFp0Nd
267	N 	R0003	UNKNOWN	57	1	3	58	57	0	0	0	0	3	09	397	1	4	42	1	45	1	2	2	2	2	2	\N	17.00	67.00	23.00	- 	1	02	N       	\N	0	Y	N	N	N	N	W	2016-01-13	ela	\N	\N	c8d02a33-f11e-4f47-8bd8-7767951d2036	4e8d749a-316a-440d-91f2-d4887a853e70	71	\N	\N	\N	41	\N	29	29	28	28	19	19	60	60	89	89	31	31	8	8	83	83	12	12	49	49	3 	3 	17	17	zoxqZgWkKoS9mT7YODBFwUZS48qNkoXzGPZjXUHi
268	N 	R0003	UNKNOWN	58	1	3	59	58	0	0	0	0	3	09	404	10	5	49	1	47	1	3	3	3	3	3	\N	70.00	96.00	62.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	55b43c51-c038-41b3-aa6a-f6760c80afb4	d486facf-afe0-407f-bdc8-09545ed980e9	7	\N	\N	\N	48	\N	26	26	79	79	1	1	88	88	24	24	35	35	32	32	30	30	17	17	30	30	4 	4 	42	42	Avrxze2AmndYm8dxmLJxDxhLsKpRgxaISvx3KS6F
269	N 	R0003	UNKNOWN	59	1	3	60	59	0	0	0	0	3	09	463	5	5	46	1	49	1	3	1	3	3	3	\N	72.00	21.00	24.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	2ff2eeef-b266-4240-8dca-d90ad4718be0	1cc0e77d-71a4-4907-9cf6-d44acffee7ea	72	\N	\N	\N	96	\N	57	57	38	38	70	70	16	16	66	66	59	59	69	69	23	23	73	73	83	83	4 	4 	73	73	sYbJYuJUBNDgRcwDUOr3LhFnvsw424uSYhmE0rx7
270	N 	R0003	UNKNOWN	60	1	4	61	60	0	0	0	0	3	09	88	6	1	9	1	10	1	2	1	2	2	2	\N	10.00	40.00	20.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	fdfc61cd-c3ee-4b3a-b40e-fc47283a9e60	553c6e87-f655-4437-8133-63ddb6288577	48	\N	\N	\N	84	\N	76	76	98	98	30	30	32	32	52	52	8	8	4	4	8	8	28	28	36	36	4 	4 	18	18	GuxIDl2Fjyjs81hW6n9gmufC4ruRxwcV8DCuKqbi
271	N 	R0003	UNKNOWN	61	1	4	62	61	0	0	0	0	3	09	80	8	1	6	2	8	2	1	3	1	1	1	\N	59.00	35.00	28.00	- 	1	01	N       	1 	1	N	N	N	N	N	N	2016-01-13	ela	\N	\N	93133aef-f229-4574-98c6-fcc5189ebf93	54c315cf-e9d1-4a5c-9ae2-db386e051a32	4	\N	\N	\N	21	\N	91	91	43	43	2	2	32	32	79	79	37	37	87	87	68	68	42	42	40	40	4 	4 	6 	6 	k1kUAyTitX2QYeasKtOPdOxbPCSIet5I00G6vQON
272	N 	R0003	UNKNOWN	62	1	4	63	59	0	0	0	0	3	09	44	2	1	5	1	3	1	3	3	3	3	3	\N	34.00	58.00	86.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	4b310271-405c-4367-8cd6-7b8fe5a082aa	976046e6-066a-490e-ab19-1486f9bee4d2	57	\N	\N	\N	28	\N	33	33	0	0	40	40	85	85	20	20	87	87	20	20	71	71	90	90	70	70	4 	4 	13	13	OjeIJyOY5Qyogd8ZPoAT4Lj6E1LmudgZcIrEeAvb
273	N 	R0003	UNKNOWN	63	1	4	64	62	0	0	0	0	3	09	21	2	1	4	1	2	1	3	3	3	3	3	\N	97.00	38.00	27.00	- 	1	03	N       	\N	0	N	N	N	N	N	N	2016-01-13	ela	\N	\N	9569b28d-62ee-479a-8328-fd9850cbd779	c9a689b9-9ba8-4acd-b1d1-5bb241ba255e	35	\N	\N	\N	22	\N	35	35	57	57	60	60	64	64	88	88	87	87	20	20	3	3	40	40	80	80	4 	4 	4 	4 	KDGutaOLUnOe1nGxReYkTttxEhi1iLsEL5CE8CPa
274	N 	R0003	UNKNOWN	64	1	4	65	63	0	0	0	0	3	09	174	10	2	16	8	18	3	3	2	3	3	3	\N	48.00	57.00	72.00	- 	1	03	N       	\N	0	Y	N	N	N	N	W	2016-01-13	ela	\N	\N	6e0b03a7-fb69-4025-b91e-c076e171f1c3	c12af48e-50b2-4a88-bfa2-f2d8738f59e8	49	\N	\N	\N	17	\N	76	76	6	6	10	10	71	71	13	13	6	6	84	84	58	58	4	4	46	46	33	33	20	20	4xNcr39DqN64ORBLFyIG322oypneHyCBYVXKZZPm
275	N 	R0003	UNKNOWN	65	1	4	66	59	0	0	0	0	3	09	147	7	2	17	3	19	3	3	2	3	3	3	\N	94.00	68.00	48.00	- 	1	02	N       	\N	0	N	N	N	N	N	N	2016-01-13	ela	\N	\N	30bb2e2c-1aec-4ea4-9069-f3a947ac13cd	22766bdb-69c9-41d1-bf9c-a6f1354d465c	27	\N	\N	\N	35	\N	97	97	71	71	46	46	91	91	13	13	20	20	19	19	11	11	16	16	72	72	5 	5 	52	52	GxZdYAWx8b0sf9CbdWYT8HsXYqBnNikEkDMikPkd
276	N 	R0003	UNKNOWN	66	1	5	67	58	0	0	0	0	3	09	110	6	2	11	3	14	2	1	3	1	1	1	\N	54.00	95.00	91.00	- 	1	03	N       	\N	0	N	N	N	N	N	N	2016-01-13	ela	\N	\N	87546bfd-3a03-4365-ae75-c05ccb1258cc	a0008ec3-c74f-4a0a-8bca-35107e22e131	52	\N	\N	\N	37	\N	44	44	66	66	99	99	91	91	81	81	87	87	31	31	63	63	42	42	29	29	1 	1 	62	62	urU46yAVop0SQHyO35ugSyxZHrWM1NDDyJY3g4Uz
277	N 	R0003	UNKNOWN	67	1	5	68	63	0	0	0	0	3	09	118	8	2	12	7	15	3	1	3	1	1	1	\N	60.00	48.00	41.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	6d279ae3-e49a-4dcf-84f5-25374dc74213	8412cd07-b01e-433f-97da-b9358137c485	23	\N	\N	\N	79	\N	89	89	73	73	34	34	34	34	41	41	9	9	92	92	37	37	1	1	16	16	1 	1 	70	70	VfwdANZxA7Ak4zCPTWDwK9Zd47uiFTJp4rbBTmMs
278	N 	R0003	UNKNOWN	68	1	5	69	60	0	0	0	0	3	09	172	9	2	14	1	17	1	2	2	2	2	2	\N	60.00	58.00	84.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	1694db1e-07a8-44a0-9bd6-92afa9204d8d	354def07-0710-4ef3-94f4-811f95b83f1d	14	\N	\N	\N	71	\N	46	46	32	32	97	97	12	12	71	71	6	6	5	5	61	61	17	17	25	25	1 	1 	83	83	8Kjp09Mfair9T4YbPnMaq7t35stJce29GjDxDHzD
279	N 	R0003	UNKNOWN	69	1	5	70	64	0	0	0	0	3	09	299	1	3	32	1	36	1	2	2	2	2	2	\N	51.00	53.00	74.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	3c3e4b7e-61e1-49dc-80d6-d29745ae2db3	0ecbf011-0ab0-4c78-a521-4ec617d44c7e	7	\N	\N	\N	3	\N	82	82	56	56	87	87	90	90	0	0	31	31	57	57	83	83	74	74	50	50	1 	1 	67	67	xro1vzHRQAemvh8tb1iV4F2Yr9Tf5BmZI5F1UzlJ
280	N 	R0003	UNKNOWN	70	1	5	71	65	0	0	0	0	3	09	162	3	2	17	2	14	2	2	2	2	2	2	\N	11.00	27.00	89.00	- 	1	01	N       	\N	0	N	N	N	N	N	N	2016-01-13	ela	\N	\N	382b343d-1b02-4b96-b1cb-2644db2e9f09	c2ef9249-45e6-4535-95bb-53653f2aa855	97	\N	\N	\N	5	\N	7	7	55	55	75	75	7	7	91	91	55	55	15	15	6	6	39	39	78	78	8 	8 	91	91	o557yJ1Kx4YrfB8oMHfoaDVLMTFj3DJXWrKMm9zT
281	N 	R0001	UNKNOWN	71	1	1	72	66	0	0	0	0	4	11	297	9	3	25	6	21	3	2	2	3	2	2	\N	34.00	93.00	16.00	- 	1	01	Y       	1 	1	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	472bef1a-828a-48ef-9c46-8f3a3c235d0d	e3d7ef8e-3fd5-4497-ab99-1bcb39f7eed6	53	\N	\N	\N	26	\N	0	0	83	83	9	9	28	28	22	22	28	28	99	99	94	94	83	83	88	88	33	33	57	57	RQaWFAiKM91B9AbbwRXpJzOo302G4qKWtzbpRano
282	N 	R0002	UNKNOWN	72	1	2	73	67	0	0	0	0	4	11	175	2	2	14	2	13	2	1	1	1	1	3	\N	95.00	46.00	95.00	- 	1	01	Y       	5 	5	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	e56959c5-c419-45b3-a1ab-327e78b10aeb	b8340e9c-33ae-4a21-869d-7291c87af210	53	\N	\N	\N	34	\N	67	67	54	54	87	87	64	64	30	30	43	43	89	89	98	98	21	21	18	18	55	55	12	12	sstpR8WRh7effxahZuK1ZPaQJmLH10nFDTSm23am
283	N 	R0001	UNKNOWN	73	1	1	74	68	0	0	0	0	4	11	421	4	5	47	3	49	2	2	2	1	3	2	\N	56.00	20.00	19.00	- 	1	03	N       	\N	0	Y	N	N	N	N	W	2016-01-13	ela	\N	\N	17c77770-7dde-4bfd-8ed8-2d24ffb665ee	6c26d2d4-870f-4e2d-8f6d-233aacc9f86b	7	\N	\N	\N	22	\N	39	39	51	51	62	62	68	68	94	94	32	32	5	5	77	77	4	4	36	36	23	23	10	10	6DRp6GlmW7bu4tn1PhckfVhL9MHH85oXIaSfRJep
284	N 	R0002	UNKNOWN	74	1	2	75	69	0	0	0	0	4	11	197	8	2	15	5	14	5	1	3	3	1	3	\N	90.00	84.00	51.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	ce42c551-d724-4718-b769-31a796be9581	bb1910b9-5631-486a-aeb3-29434dc9e081	26	\N	\N	\N	46	\N	11	11	85	85	74	74	85	85	16	16	70	70	41	41	85	85	84	84	58	58	44	44	16	16	Uetza1KA3EqTldkdiq0w92KFYhVqBL6zLWinhXI9
285	N 	R0001	UNKNOWN	75	1	1	76	70	0	0	0	0	4	11	202	4	3	24	4	23	3	3	3	2	3	3	\N	99.00	31.00	93.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	35c65034-b5b4-4129-b850-8d1fbf9f58ee	b24a821b-6d9e-4854-8af0-df5184affc21	64	\N	\N	\N	69	\N	45	45	77	77	20	20	3	3	42	42	15	15	56	56	75	75	21	21	1	1	22	22	57	57	IWr9RrKwQAe5iXf7W8xFiTf4fxYeWjWvoRJB7s45
286	N 	R0003	UNKNOWN	76	1	3	77	71	0	0	0	0	4	11	441	10	5	48	4	47	4	3	1	3	3	3	\N	58.00	99.00	36.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	b2de9140-4fec-4ba0-807c-655d41d0a161	8a933cdb-15aa-4b06-81bc-65e97eb1a715	57	\N	\N	\N	13	\N	49	49	27	27	25	25	39	39	40	40	74	74	74	74	49	49	41	41	48	48	3 	3 	71	71	wD6y2AQPgV3HtqM23RFtegCe56oenrPa6DdbElzI
287	N 	R0001	UNKNOWN	77	1	1	78	72	0	0	0	0	4	11	262	8	3	25	1	24	1	3	2	3	1	3	\N	84.00	78.00	72.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	fa72a9f8-a5bc-4ed2-bffb-1817f65c3069	6bc4e31f-d8bd-4d59-bf08-5222b3a96293	88	\N	\N	\N	13	\N	89	89	39	39	68	68	24	24	74	74	6	6	74	74	56	56	82	82	24	24	1 	1 	7 	7 	p42AqWSux32Vo3RQzM34aioB8DbTdzDqfLRsAnhF
288	N 	R0003	UNKNOWN	78	1	3	79	73	0	0	0	0	4	11	455	3	5	49	3	46	2	3	1	3	3	3	\N	99.00	84.00	52.00	- 	1	02	N       	\N	0	Y	N	N	N	N	W	2016-01-13	ela	\N	\N	e5c1fa54-2abd-4b9a-98e8-b3e13fa8f754	915071ce-3b15-4788-9bb8-6c07b1deec33	42	\N	\N	\N	87	\N	44	44	39	39	51	51	50	50	47	47	24	24	94	94	75	75	87	87	31	31	3 	3 	89	89	JRAWX24sIH3Bh2KcmpYNn6ZyGr8e6p6ctkAMFRw5
289	N 	R0001	UNKNOWN	79	1	1	80	74	0	0	0	0	4	11	246	8	3	33	6	37	2	1	2	3	3	3	\N	90.00	97.00	12.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	291c9cf8-bc4d-4af1-a900-2fa77efddd1b	c36be487-e6d2-4d72-a442-93279973b819	2	\N	\N	\N	8	\N	80	80	21	21	60	60	98	98	54	54	42	42	69	69	19	19	62	62	69	69	9 	9 	17	17	JL6uVW6OSVaYqpvndl0foBKAFrf4bdz6HACTTcLd
290	N 	R0003	UNKNOWN	80	1	3	81	75	0	0	0	0	4	11	218	4	3	35	1	33	1	3	3	3	3	3	\N	13.00	67.00	57.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	4d9d3bfc-6a13-4ebd-8c6c-8cf4581c3c5d	b43f1880-193b-4118-9271-3dc383d2f59e	54	\N	\N	\N	69	\N	57	57	48	48	97	97	92	92	1	1	86	86	63	63	5	5	16	16	6	6	4 	4 	9 	9 	nHaADPpFPr1tpkcoS2Xj5mhmvdViLrumXuJMSox3
291	N 	R0002	UNKNOWN	81	1	2	82	76	0	0	0	0	4	11	295	3	3	31	2	30	2	3	3	3	3	2	\N	63.00	50.00	42.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	db94ba6c-3abe-4e9a-8bbb-b680531efbab	64b2f62f-c5d4-49c9-be9a-762e5e161e2c	15	\N	\N	\N	26	\N	96	96	58	58	15	15	47	47	10	10	35	35	88	88	27	27	67	67	39	39	3 	3 	80	80	p69xZhxSMQuWmke62h3j1K7izaj1evXeklkHFuMg
292	N 	R0003	UNKNOWN	82	1	3	83	77	0	0	0	0	4	11	207	5	3	36	4	34	4	3	2	3	3	3	\N	67.00	36.00	74.00	- 	1	02	N       	\N	0	Y	N	N	N	N	W	2016-01-13	ela	\N	\N	5f6a86cc-94e7-453a-ae49-622325b6244a	07e5465d-9a4c-4c73-8fa7-f97ab0d469fb	28	\N	\N	\N	4	\N	5	5	53	53	81	81	2	2	32	32	41	41	67	67	64	64	9	9	93	93	4 	4 	31	31	LaPd7vVIU9G9NvGr44WMWgwNwvupwIyWtC5kGetF
293	N 	R0002	UNKNOWN	83	1	2	84	78	0	0	0	0	4	11	382	2	4	41	1	44	1	3	3	3	3	3	\N	44.00	65.00	99.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	30942862-4d08-4505-a85e-798d8065de42	cdb5e9f7-8259-46e1-8dea-e005ff9d8ff4	5	\N	\N	\N	81	\N	32	32	22	22	80	80	73	73	4	4	67	67	27	27	29	29	39	39	43	43	5 	5 	27	27	OimZrYW80W7xl7QUa4Hg9Q9knjdvys1Z8wRIVHuG
294	N 	R0003	UNKNOWN	84	1	3	85	79	0	0	0	0	4	11	342	7	4	45	1	42	1	1	2	1	1	1	\N	61.00	31.00	13.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	7ea982c0-a412-49d6-a6b5-fe4d05c94eb3	de01c9b7-1b53-4174-a91c-0a8f6c70c819	48	\N	\N	\N	68	\N	21	21	76	76	76	76	37	37	45	45	30	30	68	68	10	10	89	89	11	11	4 	4 	60	60	cS2xAK3xDPCBR7ZWbAh9FS08IqjzWxgbZeitAa40
295	N 	R0002	UNKNOWN	85	1	2	86	80	0	0	0	0	4	11	138	8	2	13	5	12	5	1	1	1	1	3	\N	89.00	30.00	79.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	8559ab78-3be5-4ff9-8966-e8c4c6f3b8df	d104f8e3-fc52-44db-95bf-6315b1f0d298	86	\N	\N	\N	19	\N	19	19	84	84	62	62	2	2	58	58	92	92	83	83	57	57	92	92	22	22	6 	6 	72	72	oGYUvFLatZjgEBWy3LV9NwMdmc9GT1GTerNuABS9
296	N 	R0003	UNKNOWN	86	1	3	87	81	0	0	0	0	4	11	449	10	5	47	10	49	1	2	3	2	2	2	\N	35.00	71.00	38.00	- 	1	01	Y       	2 	2	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	be331185-b72d-48bf-9d36-e90ecee72e92	d1706d64-ffaa-4dfb-ad33-7de052568519	68	\N	\N	\N	99	\N	61	61	83	83	85	85	90	90	5	5	4	4	8	8	99	99	57	57	63	63	4 	4 	32	32	UmOkwomla6wQuMu8rde3f3OkVwf23neTGCISpB28
297	N 	R0003	UNKNOWN	87	1	4	88	82	0	0	0	0	4	11	271	6	3	32	3	36	2	3	1	3	3	3	\N	69.00	50.00	49.00	- 	1	01	N       	\N	0	N	N	N	N	N	N	2016-01-13	ela	\N	\N	35b4c08c-f392-4c2e-94b2-2472ac6909b8	89921cd5-c987-468f-b362-d5c6c17856f4	74	\N	\N	\N	80	\N	28	28	97	97	42	42	26	26	34	34	64	64	89	89	60	60	58	58	11	11	5 	5 	48	48	loKBQQ7EnGEJKKoK39nYLCNWtoJIDC2yNW9cPcmh
298	N 	R0003	UNKNOWN	88	1	4	89	83	0	0	0	0	4	11	358	3	4	45	2	40	2	1	3	1	1	1	\N	27.00	81.00	60.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	d5f2e34b-e706-402c-bc10-1e7bfa707f5f	b73cc90c-b450-4def-acb2-423268f8c07f	3	\N	\N	\N	14	\N	70	70	57	57	28	28	45	45	12	12	61	61	88	88	44	44	28	28	15	15	5 	5 	87	87	ZHBxgfZVT3KsM8I8iun3007MOqKZxIvz5HZht1dR
299	N 	R0003	UNKNOWN	89	1	4	90	84	0	0	0	0	4	11	10	6	1	5	1	7	1	1	3	1	1	1	\N	74.00	34.00	29.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	4d767508-38b1-4927-9048-0a6fb4b5c2a5	c5007fd7-93fa-4458-bfd9-44ae9de391f2	4	\N	\N	\N	67	\N	54	54	86	86	21	21	65	65	18	18	60	60	51	51	52	52	68	68	99	99	5 	5 	66	66	4mzU3y7OgIchC8Rz9tfOk7KI8AcXlVtFPVOO1XFH
300	N 	R0003	UNKNOWN	90	1	4	91	85	0	0	0	0	4	11	16	8	1	4	3	6	1	1	3	1	1	1	\N	83.00	57.00	77.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	0e9d8042-698b-4046-ba1b-34997540bdbc	bd14470c-edd7-479e-9ca4-82da5383df54	67	\N	\N	\N	17	\N	2	2	14	14	35	35	7	7	8	8	3	3	56	56	43	43	10	10	53	53	5 	5 	54	54	iI34kG6uH1mQ83tbh5PEVVKoQjV1r96laDb4x10n
301	N 	R0003	UNKNOWN	91	1	5	92	86	0	0	0	0	4	11	341	10	4	44	1	42	1	1	1	1	1	1	\N	59.00	41.00	45.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	2aff6dcf-e2de-4ec4-9212-a761f29e0e6f	0055b1fc-2bc9-4747-91f6-6388c0e282dd	89	\N	\N	\N	97	\N	55	55	4	4	16	16	74	74	75	75	59	59	33	33	59	59	32	32	82	82	1 	1 	33	33	xkO5Umv7TjxiZY8CD926MY27uoz1edQFcaWU76Ef
302	N 	R0003	UNKNOWN	92	1	5	93	87	0	0	0	0	4	11	445	8	5	48	5	47	1	2	3	2	2	2	\N	95.00	58.00	27.00	- 	1	03	N       	\N	0	Y	N	N	N	N	W	2016-01-13	ela	\N	\N	66e13a15-faaa-47d2-b082-a8d5e8aaea6c	3e3e3ce0-1cf8-4e4c-ab5e-bffd7ab0db2b	39	\N	\N	\N	35	\N	4	4	74	74	68	68	45	45	49	49	61	61	69	69	21	21	47	47	50	50	5 	5 	94	94	wTpuVRx1CP4eXjjQiCKXjjvvLaZpRAAujc0QB30k
303	N 	R0003	UNKNOWN	93	1	5	94	88	0	0	0	0	4	11	184	5	2	16	1	13	1	2	1	2	2	2	\N	78.00	40.00	82.00	- 	1	02	N       	\N	0	Y	N	N	N	N	W	2016-01-13	ela	\N	\N	1b03efd2-1d1e-4110-9350-22185753b6ba	f317e79f-1b79-4463-bdfd-4700232f8cb9	79	\N	\N	\N	46	\N	46	46	17	17	56	56	92	92	66	66	50	50	21	21	28	28	5	5	25	25	58	58	66	66	jgliMmu6B15PbLy7sq7uRlv40qiIF3liNI3ySpru
304	N 	R0003	UNKNOWN	94	1	5	95	89	0	0	0	0	4	11	213	9	3	39	1	36	1	2	3	2	2	2	\N	57.00	84.00	66.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	f620a09f-9103-4446-b234-1c736f15500c	18f97d35-021e-410e-a8cc-c95fc8bbf981	42	\N	\N	\N	73	\N	1	1	8	8	74	74	8	8	0	0	68	68	83	83	7	7	20	20	17	17	8 	8 	21	21	Uk2E8MeExKfyl6XHBLKpNJe6na8bi3nj8yMCgeFz
305	N 	R0003	UNKNOWN	95	1	5	96	90	0	0	0	0	4	11	483	2	5	47	1	49	1	1	3	1	1	1	\N	47.00	17.00	50.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	45d46b86-9771-47c4-9205-ed5df2a743d7	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	nQXqvAHQ0W1FOGH2IFhG68Aa590JN9bpNQtSXdiw
307	N 	R0003	UNKNOWN	42	2	6	97	42	0	0	0	0	4	11	301	1	4	44	1	42	1	3	2	3	3	3	\N	57.00	41.00	87.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	22b58ccc-2418-4508-bc73-12b3ff68dac3	c1082a31-3843-4932-9334-3b967dacaca3	96	\N	\N	\N	68	\N	63	63	82	82	63	63	80	80	46	46	11	11	46	46	39	39	80	80	43	43	8 	8 	3 	3 	6boPVVoO1Vuyg8VJ5AqV0Zha4rVGrZt3uPrM7xgw
308	N 	R0003	UNKNOWN	43	2	6	98	43	0	0	0	0	4	11	369	10	4	45	3	44	1	3	3	3	3	3	\N	68.00	41.00	54.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	0b674c27-ad68-4597-91bc-01fe8e4d57d2	8fb46bb9-280b-4584-9a0f-7e00f4ab7cd7	71	\N	\N	\N	72	\N	72	72	71	71	28	28	31	31	40	40	49	49	99	99	65	65	11	11	2	2	8 	8 	94	94	KL3R8L1EaO8MgymDcMI59fe7DnjLk0ZuDZiwO2J2
309	N 	R0003	UNKNOWN	95	2	6	96	90	0	0	0	0	4	11	490	2	5	47	1	49	1	1	3	1	1	1	\N	47.00	17.00	50.00	- 	1	01	Y       	\N	0	N	Y	Y	Y	Y	Y	2016-01-13	ela	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	14bea448-99d3-45e6-ba13-162d4cc0d0a8	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	nQXqvAHQ0W1FOGH2IFhG68Aa590JN9bpNQtSXdiw
223	N 	R0003	UNKNOWN	95	1	5	99	90	0	0	0	0	5	11	146	3	2	\N	3	\N	1	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y       	2 	4	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	506a05b5-c8c9-4170-a12c-e2542eb4a0c3	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	D3de8PGASi2QhIZ0zHLSNJXZI0okSm1VqPJMeJAw
224	N 	R0003	UNKNOWN	95	1	5	100	90	0	0	0	0	5	11	175	7	2	\N	1	\N	1	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y       	2 	4	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	4eb74f59-53d1-41fa-bf2b-33031e9fd851	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	CYh72rsxfT7A4jmupxUkrb3S76bTra2BmlgrR57F
225	N 	R0003	UNKNOWN	95	1	5	101	90	0	0	0	0	5	11	116	4	2	\N	3	\N	1	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y       	2 	4	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	a4f811f7-1919-43d5-98ac-78b89f5fcf53	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	m5i2mSxYFwrFTnW09BEYlrXBvQIw3bTmo2zm1GaI
226	N 	R0002	UNKNOWN	53	1	2	102	53	0	0	0	0	6	09	202	10	3	\N	5	\N	1	1	2	3	2	1	\N	48.00	36.00	58.00	Y 	1	01	Y       	3 	22	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	b681b620-edd4-4a50-affa-47774eff0914	c118b57c-4e79-4ff5-8833-4a85f227ad32	33	\N	\N	\N	7	\N	1	1	60	60	55	55	67	67	1	1	2	2	25	25	28	28	33	33	22	22	1 	1 	22	22	1SWd4Oky5wnWUNuH078iTM1reO6FjlIYmYkVHcQw
227	N 	R0003	UNKNOWN	54	1	3	103	54	0	0	0	0	7	09	236	7	3	\N	7	\N	1	1	1	1	2	1	\N	93.00	90.00	54.00	Y 	1	01	Y       	4 	9	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	905fdbd3-2337-457b-a38b-90fc87f615c2	415dd8d5-6dd7-4d53-a8a2-af044ecb3a94	25	\N	\N	\N	77	\N	47	47	65	65	88	88	7	7	3	3	23	23	26	26	16	16	22	22	37	37	1 	1 	64	64	Z94Y0ynyVyq3VCwLTjIRZHTZSYkFHT0lBN8meOgb
228	N 	R0003	UNKNOWN	55	1	3	104	55	0	0	0	0	8	10	398	4	4	\N	4	\N	1	1	3	3	3	2	\N	88.00	40.00	48.00	Y 	1	01	N       	5 	89	N	N	N	N	N	N	2016-01-13	math	\N	\N	805d1318-86f8-49d7-aee4-527a22808b36	47ff60a1-ae5a-4683-9ae9-556f0ed9da14	97	\N	\N	\N	63	\N	65	65	94	94	11	11	20	20	98	98	2	2	59	59	95	95	99	99	47	47	3 	3 	69	69	maHNc5BQaVj9ddANVpBvFluBikLI3H06gqH3P1zi
229	N 	R0003	UNKNOWN	11	1	3	105	11	0	0	0	0	9	11	106	6	2	\N	5	\N	5	2	2	3	3	2	\N	46.00	67.00	27.00	Y 	1	03	N       	1 	51	N	N	N	N	N	N	2016-01-13	math	\N	\N	94a06c65-474b-423f-9eb8-59ac8cf496db	aae2b43b-2be1-42bc-98cd-4528d6e86927	78	\N	\N	\N	56	\N	88	88	2	2	71	71	57	57	29	29	94	94	44	44	39	39	43	43	15	15	3 	3 	78	78	E9fHez5Rp8SfdyyhzYgAlyLyIE1JYmpYL8d9Up4W
230	N 	R0003	UNKNOWN	56	1	3	106	56	0	0	0	0	10	12	276	9	3	\N	5	\N	5	3	3	2	1	1	\N	14.00	71.00	81.00	Y 	1	01	Y       	2 	9	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	00737d05-b0bd-46b9-80a6-20bf79cf761a	64d8cf91-094c-458b-b450-8fb0afa1bb66	52	\N	\N	\N	34	\N	42	42	54	54	73	73	42	42	67	67	69	69	26	26	91	91	13	13	12	12	3 	3 	79	79	r1s4BnsoqFMAxUnLv1q3GbBD9SWWz0yUXPaFp0Nd
231	N 	R0003	UNKNOWN	12	1	3	107	12	0	0	0	0	11	11	156	4	2	\N	2	\N	1	1	2	3	3	3	\N	28.00	48.00	85.00	Y 	1	02	N       	3 	5	Y	N	N	N	N	W	2016-01-13	math	\N	\N	cf537852-5ba0-4af8-ad17-996d07c0a040	adb48129-8f51-4e89-a439-6a5a40f5c824	38	\N	\N	\N	68	\N	16	16	58	58	13	13	10	10	89	89	43	43	42	42	96	96	37	37	82	82	3 	3 	16	16	haGegI5PgD6OzaQ1Mu478Og0zw693ZmLAgQTbjLb
232	N 	R0003	UNKNOWN	14	1	3	108	14	0	0	0	0	12	12	58	1	1	\N	1	\N	1	2	3	2	1	1	\N	56.00	63.00	3.00	Y 	1	01	N       	1 	7	N	N	N	N	N	N	2016-01-13	math	\N	\N	7fec724b-1594-4e0e-9d08-a539a77b3c99	536c0db4-f91c-42df-b1eb-e6ce236fef92	34	\N	\N	\N	40	\N	66	66	63	63	26	26	61	61	69	69	87	87	64	64	34	34	58	58	78	78	3 	3 	13	13	Cnt3veDS2HW94jTAnU4bUASdLgIbP9y0lvQpQqer
233	N 	R0003	UNKNOWN	57	1	3	109	57	0	0	0	0	13	12	22	2	1	\N	1	\N	1	2	3	2	1	1	\N	45.00	15.00	50.00	Y 	1	02	N       	4 	37	Y	N	N	N	N	W	2016-01-13	math	\N	\N	c8d02a33-f11e-4f47-8bd8-7767951d2036	4f781890-fde1-4e57-9903-1789d63178e5	71	\N	\N	\N	41	\N	29	29	28	28	19	19	60	60	89	89	31	31	8	8	83	83	12	12	49	49	3 	3 	17	17	zoxqZgWkKoS9mT7YODBFwUZS48qNkoXzGPZjXUHi
234	N 	R0003	UNKNOWN	13	1	3	110	13	0	0	0	0	14	11	7	1	1	\N	1	\N	1	3	3	3	3	1	\N	64.00	15.00	11.00	Y 	1	03	N       	5 	23	N	N	N	N	N	N	2016-01-13	math	\N	\N	81b4561e-6e70-47b4-8dfe-2898e550de7f	bf6f4e3d-e0da-4b22-b992-7e29f307aa30	13	\N	\N	\N	1	\N	56	56	44	44	79	79	9	9	64	64	22	22	68	68	74	74	57	57	40	40	3 	3 	30	30	U49Ivq4Y5D2NTB0EAuHv0sKhVA90oVkCwOgJErDW
235	N 	R0003	UNKNOWN	76	1	3	111	71	0	0	0	0	15	11	121	2	2	\N	1	\N	1	2	2	3	2	3	\N	9.00	26.00	26.00	Y 	1	01	Y       	2 	42	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	b2de9140-4fec-4ba0-807c-655d41d0a161	a9dfaaff-3136-4587-a763-05cb1360080f	57	\N	\N	\N	13	\N	49	49	27	27	25	25	39	39	40	40	74	74	74	74	49	49	41	41	48	48	3 	3 	71	71	wD6y2AQPgV3HtqM23RFtegCe56oenrPa6DdbElzI
236	N 	R0001	UNKNOWN	71	1	1	112	66	0	0	0	0	16	11	330	2	4	\N	2	\N	1	1	1	1	3	3	\N	61.00	71.00	78.00	Y 	1	01	Y       	4 	29	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	472bef1a-828a-48ef-9c46-8f3a3c235d0d	75893fb2-5cf5-4f94-8f25-d81aa058410e	53	\N	\N	\N	26	\N	0	0	83	83	9	9	28	28	22	22	28	28	99	99	94	94	83	83	88	88	33	33	57	57	RQaWFAiKM91B9AbbwRXpJzOo302G4qKWtzbpRano
237	N 	R0001	UNKNOWN	73	1	1	113	68	0	0	0	0	17	11	340	6	4	\N	1	\N	1	2	2	3	3	2	\N	38.00	60.00	59.00	Y 	1	03	N       	4 	12	Y	N	N	N	N	W	2016-01-13	math	\N	\N	17c77770-7dde-4bfd-8ed8-2d24ffb665ee	b2439cf2-f045-404b-8386-ddef5969cf9e	7	\N	\N	\N	22	\N	39	39	51	51	62	62	68	68	94	94	32	32	5	5	77	77	4	4	36	36	23	23	10	10	6DRp6GlmW7bu4tn1PhckfVhL9MHH85oXIaSfRJep
238	N 	R0001	UNKNOWN	75	1	1	114	70	0	0	0	0	18	11	223	4	3	\N	4	\N	3	3	3	3	1	1	\N	0.00	19.00	52.00	Y 	1	01	Y       	1 	13	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	35c65034-b5b4-4129-b850-8d1fbf9f58ee	93e908b1-ee24-4e93-95bc-8debeb323b2e	64	\N	\N	\N	69	\N	45	45	77	77	20	20	3	3	42	42	15	15	56	56	75	75	21	21	1	1	22	22	57	57	IWr9RrKwQAe5iXf7W8xFiTf4fxYeWjWvoRJB7s45
239	N 	R0001	UNKNOWN	77	1	1	115	72	0	0	0	0	19	11	315	2	4	\N	2	\N	1	3	2	2	2	3	\N	64.00	95.00	11.00	Y 	1	01	Y       	1 	75	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	fa72a9f8-a5bc-4ed2-bffb-1817f65c3069	bc6202f4-1f8b-42dc-a10d-e2310948cd8e	88	\N	\N	\N	13	\N	89	89	39	39	68	68	24	24	74	74	6	6	74	74	56	56	82	82	24	24	1 	1 	7 	7 	p42AqWSux32Vo3RQzM34aioB8DbTdzDqfLRsAnhF
240	N 	R0003	UNKNOWN	59	1	3	116	59	0	0	0	0	20	09	230	2	3	\N	2	\N	1	1	3	3	2	1	\N	4.00	55.00	31.00	Y 	1	01	Y       	2 	16	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	2ff2eeef-b266-4240-8dca-d90ad4718be0	5ba7a545-46f3-4927-b2bf-2d77dc7ad021	72	\N	\N	\N	96	\N	57	57	38	38	70	70	16	16	66	66	59	59	69	69	23	23	73	73	83	83	4 	4 	73	73	sYbJYuJUBNDgRcwDUOr3LhFnvsw424uSYhmE0rx7
241	N 	R0003	UNKNOWN	60	1	4	117	60	0	0	0	0	21	09	221	7	3	\N	2	\N	1	3	3	1	3	1	\N	99.00	87.00	35.00	Y 	1	01	Y       	2 	66	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	fdfc61cd-c3ee-4b3a-b40e-fc47283a9e60	f3cc5821-2925-479c-b4ec-5f7d55b2ecf6	48	\N	\N	\N	84	\N	76	76	98	98	30	30	32	32	52	52	8	8	4	4	8	8	28	28	36	36	4 	4 	18	18	GuxIDl2Fjyjs81hW6n9gmufC4ruRxwcV8DCuKqbi
242	N 	R0003	UNKNOWN	61	1	4	118	61	0	0	0	0	22	09	355	5	4	\N	2	\N	2	3	2	1	2	3	\N	99.00	85.00	40.00	Y 	1	01	N       	2 	9	N	N	N	N	N	N	2016-01-13	math	\N	\N	93133aef-f229-4574-98c6-fcc5189ebf93	48c25d83-21fb-4387-a6de-31c0a435e593	4	\N	\N	\N	21	\N	91	91	43	43	2	2	32	32	79	79	37	37	87	87	68	68	42	42	40	40	4 	4 	6 	6 	k1kUAyTitX2QYeasKtOPdOxbPCSIet5I00G6vQON
243	N 	R0003	UNKNOWN	62	1	4	119	59	0	0	0	0	23	09	256	7	3	\N	3	\N	3	2	2	1	1	2	\N	63.00	71.00	58.00	Y 	1	01	Y       	2 	45	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	4b310271-405c-4367-8cd6-7b8fe5a082aa	832507e9-fa2e-487f-a78d-43eb684d10cd	57	\N	\N	\N	28	\N	33	33	0	0	40	40	85	85	20	20	87	87	20	20	71	71	90	90	70	70	4 	4 	13	13	OjeIJyOY5Qyogd8ZPoAT4Lj6E1LmudgZcIrEeAvb
244	N 	R0003	UNKNOWN	63	1	4	120	62	0	0	0	0	24	09	327	5	4	\N	1	\N	1	3	2	3	1	1	\N	43.00	77.00	11.00	Y 	1	03	N       	3 	18	N	N	N	N	N	N	2016-01-13	math	\N	\N	9569b28d-62ee-479a-8328-fd9850cbd779	610dadca-2794-4d8a-b720-282303787b6a	35	\N	\N	\N	22	\N	35	35	57	57	60	60	64	64	88	88	87	87	20	20	3	3	40	40	80	80	4 	4 	4 	4 	KDGutaOLUnOe1nGxReYkTttxEhi1iLsEL5CE8CPa
245	N 	R0003	UNKNOWN	15	1	4	121	15	0	0	0	0	25	10	270	5	3	\N	3	\N	1	2	1	1	3	1	\N	22.00	2.00	74.00	Y 	1	01	Y       	3 	38	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	0483d072-9b85-49b5-b542-95dbcfca1a06	8765f885-3a06-4ba1-9c72-3bcbdcbc8e5d	52	\N	\N	\N	88	\N	61	61	38	38	47	47	33	33	18	18	82	82	60	60	72	72	73	73	98	98	4 	4 	58	58	p2Sf5xBg8rAuTjy9XPGuFJaoYhdKIHISnVuRqoUd
246	N 	R0003	UNKNOWN	16	1	4	122	16	0	0	0	0	26	10	485	10	5	\N	5	\N	1	1	1	1	2	1	\N	79.00	44.00	98.00	Y 	1	01	Y       	3 	57	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	1e71359d-b18d-4b83-a613-1a9f52f89d9e	79fcce2f-960e-4f41-a1bf-a91e7c5f1c70	25	\N	\N	\N	80	\N	54	54	8	8	13	13	33	33	71	71	40	40	22	22	97	97	54	54	72	72	4 	4 	80	80	sEorUakkZr2QwQgeQ1wCHxo8NwkksVvRlPXQV7cL
247	N 	R0003	UNKNOWN	17	1	4	123	17	0	0	0	0	27	10	318	8	4	\N	2	\N	2	2	1	3	3	2	\N	43.00	69.00	5.00	Y 	1	02	N       	4 	98	N	N	N	N	N	N	2016-01-13	math	\N	\N	95a45098-3643-421f-9ef0-99e7e683b52a	eb09bcc2-155d-4396-935a-ed58e06d6c7b	8	\N	\N	\N	42	\N	61	61	72	72	45	45	22	22	14	14	15	15	31	31	3	3	87	87	79	79	5 	5 	72	72	Sc0cLMiRhBfkJsHF7Dwz1ozIPzc1k3s6aOgAD2IT
248	N 	R0003	UNKNOWN	87	1	4	124	82	0	0	0	0	28	11	172	5	2	\N	1	\N	1	3	3	1	1	2	\N	56.00	25.00	40.00	Y 	1	01	N       	4 	74	N	N	N	N	N	N	2016-01-13	math	\N	\N	35b4c08c-f392-4c2e-94b2-2472ac6909b8	43393e57-9a6e-48f6-9c75-8912777d0682	74	\N	\N	\N	80	\N	28	28	97	97	42	42	26	26	34	34	64	64	89	89	60	60	58	58	11	11	5 	5 	48	48	loKBQQ7EnGEJKKoK39nYLCNWtoJIDC2yNW9cPcmh
249	N 	R0003	UNKNOWN	18	1	4	125	18	0	0	0	0	29	10	119	7	2	\N	6	\N	1	3	3	2	3	1	\N	66.00	22.00	28.00	Y 	1	03	N       	3 	64	N	N	N	N	N	N	2016-01-13	math	\N	\N	6a996b2d-7fe2-40ad-a34b-facf7b9034db	e80a8289-7d7f-4b9c-bcd5-30adfe364a8c	81	\N	\N	\N	65	\N	36	36	65	65	14	14	67	67	4	4	32	32	75	75	71	71	88	88	90	90	5 	5 	10	10	ZmrZFBiOmI2H2QuEsZm59MMu7bIAhKHUjge92f6E
250	N 	R0003	UNKNOWN	78	1	3	126	73	0	0	0	0	30	11	420	7	5	\N	5	\N	5	1	2	2	3	3	\N	10.00	88.00	78.00	Y 	1	02	N       	3 	59	Y	N	N	N	N	W	2016-01-13	math	\N	\N	e5c1fa54-2abd-4b9a-98e8-b3e13fa8f754	d98abfc9-21b2-4b59-8fc5-86c9a2ba7c7f	42	\N	\N	\N	87	\N	44	44	39	39	51	51	50	50	47	47	24	24	94	94	75	75	87	87	31	31	3 	3 	89	89	JRAWX24sIH3Bh2KcmpYNn6ZyGr8e6p6ctkAMFRw5
251	N 	R0003	UNKNOWN	33	1	3	127	33	0	0	0	0	31	12	418	2	5	\N	2	\N	2	3	1	3	1	3	\N	81.00	10.00	77.00	Y 	1	02	N       	3 	9	Y	N	N	N	N	W	2016-01-13	math	\N	\N	7b12d2d9-0b6d-4f75-9d8c-398f80f58062	adce9869-85b7-442e-a54f-f3d5c254b979	86	\N	\N	\N	53	\N	67	67	35	35	89	89	56	56	56	56	44	44	44	44	51	51	37	37	83	83	3 	3 	68	68	hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ
252	N 	R0003	UNKNOWN	34	1	3	128	34	0	0	0	0	32	12	430	1	5	\N	1	\N	1	1	3	2	3	1	\N	61.00	48.00	4.00	Y 	1	01	Y       	4 	81	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	614e4fd1-5442-402f-82c8-3d2ea5f50e7b	426fac1d-394d-486e-b372-46b81d365b84	87	\N	\N	\N	7	\N	32	32	52	52	97	97	49	49	26	26	43	43	49	49	38	38	33	33	67	67	44	44	57	57	zxITiM66QUVOcTyhRaooB0UIOWemGFP8eQE7xPSx
253	N 	R0003	UNKNOWN	80	1	3	129	75	0	0	0	0	33	11	321	9	4	\N	4	\N	3	1	2	2	2	1	\N	98.00	95.00	75.00	Y 	1	01	Y       	4 	59	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	4d9d3bfc-6a13-4ebd-8c6c-8cf4581c3c5d	3fe51140-add4-4e0d-94cf-a13582cec21c	54	\N	\N	\N	69	\N	57	57	48	48	97	97	92	92	1	1	86	86	63	63	5	5	16	16	6	6	4 	4 	9 	9 	nHaADPpFPr1tpkcoS2Xj5mhmvdViLrumXuJMSox3
254	N 	R0003	UNKNOWN	82	1	3	130	77	0	0	0	0	34	11	302	7	4	\N	5	\N	1	1	1	3	2	2	\N	21.00	82.00	82.00	Y 	1	02	N       	5 	30	Y	N	N	N	N	W	2016-01-13	math	\N	\N	5f6a86cc-94e7-453a-ae49-622325b6244a	fc9bec22-3451-4d41-b99e-20f56b5065ec	28	\N	\N	\N	4	\N	5	5	53	53	81	81	2	2	32	32	41	41	67	67	64	64	9	9	93	93	4 	4 	31	31	LaPd7vVIU9G9NvGr44WMWgwNwvupwIyWtC5kGetF
255	N 	R0003	UNKNOWN	35	1	3	131	35	0	0	0	0	35	12	399	6	4	\N	5	\N	5	2	3	1	2	2	\N	6.00	83.00	53.00	Y 	1	01	Y       	5 	62	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	96ba894d-7749-48f9-9ef5-3019f562f963	e4da7acc-cfb8-4ac7-91de-fde4641129c8	71	\N	\N	\N	73	\N	27	27	2	2	83	83	46	46	22	22	3	3	97	97	20	20	29	29	57	57	4 	4 	98	98	6xduCZW2oSUGSLzpiFpJQQK8YSAzmBrMjAh4WEkn
256	N 	R0003	UNKNOWN	84	1	3	132	79	0	0	0	0	36	11	474	4	5	\N	1	\N	1	1	3	2	3	3	\N	48.00	72.00	50.00	Y 	1	01	Y       	1 	78	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	7ea982c0-a412-49d6-a6b5-fe4d05c94eb3	8548789d-90de-42c4-b2a9-58710c1d56f8	48	\N	\N	\N	68	\N	21	21	76	76	76	76	37	37	45	45	30	30	68	68	10	10	89	89	11	11	4 	4 	60	60	cS2xAK3xDPCBR7ZWbAh9FS08IqjzWxgbZeitAa40
257	N 	R0003	UNKNOWN	36	1	3	133	36	0	0	0	0	37	12	193	9	2	\N	5	\N	2	1	3	1	3	3	\N	84.00	26.00	87.00	Y 	1	02	N       	1 	98	N	N	N	N	N	N	2016-01-13	math	\N	\N	8d54686a-ff28-415a-8ee5-95cb999600f0	22373a08-96d1-480f-803f-0e12684416fa	98	\N	\N	\N	67	\N	35	35	89	89	27	27	73	73	10	10	69	69	40	40	57	57	89	89	86	86	4 	4 	68	68	wUfuK1C1vpevOq8cV6oqQxNNwJV1TbzeraL8fC4v
258	N 	R0003	UNKNOWN	86	1	3	134	81	0	0	0	0	38	11	352	5	4	\N	3	\N	3	1	1	2	1	3	\N	98.00	48.00	71.00	Y 	1	01	Y       	1 	70	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	be331185-b72d-48bf-9d36-e90ecee72e92	5c8ece25-8b9e-4578-a6f4-ed669986b9b5	68	\N	\N	\N	99	\N	61	61	83	83	85	85	90	90	5	5	4	4	8	8	99	99	57	57	63	63	4 	4 	32	32	UmOkwomla6wQuMu8rde3f3OkVwf23neTGCISpB28
259	N 	R0003	UNKNOWN	58	1	3	135	58	0	0	0	0	39	09	230	5	3	\N	2	\N	1	1	1	2	2	2	\N	51.00	53.00	17.00	Y 	1	01	Y       	1 	40	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	55b43c51-c038-41b3-aa6a-f6760c80afb4	8c42c4d8-ee1e-4a4b-90d6-37cf41e00154	7	\N	\N	\N	48	\N	26	26	79	79	1	1	88	88	24	24	35	35	32	32	30	30	17	17	30	30	4 	4 	42	42	Avrxze2AmndYm8dxmLJxDxhLsKpRgxaISvx3KS6F
260	N 	R0003	UNKNOWN	88	1	4	136	83	0	0	0	0	40	11	164	5	2	\N	2	\N	2	2	3	1	3	2	\N	100.00	93.00	73.00	Y 	1	01	Y       	3 	35	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	d5f2e34b-e706-402c-bc10-1e7bfa707f5f	0e3112ea-3aa6-46f6-b55b-7b2b4e58aae4	3	\N	\N	\N	14	\N	70	70	57	57	28	28	45	45	12	12	61	61	88	88	44	44	28	28	15	15	5 	5 	87	87	ZHBxgfZVT3KsM8I8iun3007MOqKZxIvz5HZht1dR
261	N 	R0003	UNKNOWN	89	1	4	137	84	0	0	0	0	41	11	167	2	2	\N	1	\N	1	3	3	2	2	1	\N	92.00	14.00	49.00	Y 	1	01	Y       	3 	46	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	4d767508-38b1-4927-9048-0a6fb4b5c2a5	f1bd3c77-4968-493b-af2e-64af7acf7160	4	\N	\N	\N	67	\N	54	54	86	86	21	21	65	65	18	18	60	60	51	51	52	52	68	68	99	99	5 	5 	66	66	4mzU3y7OgIchC8Rz9tfOk7KI8AcXlVtFPVOO1XFH
262	N 	R0003	UNKNOWN	90	1	4	138	85	0	0	0	0	42	11	276	4	3	\N	4	\N	4	1	3	2	2	1	\N	59.00	58.00	1.00	Y 	1	01	Y       	3 	47	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	0e9d8042-698b-4046-ba1b-34997540bdbc	fdf0ae08-2b04-4ab4-9389-a2f46aaf7b9d	67	\N	\N	\N	17	\N	2	2	14	14	35	35	7	7	8	8	3	3	56	56	43	43	10	10	53	53	5 	5 	54	54	iI34kG6uH1mQ83tbh5PEVVKoQjV1r96laDb4x10n
264	N 	R0003	UNKNOWN	37	1	4	139	37	0	0	0	0	43	12	55	5	1	\N	4	\N	3	1	1	1	3	3	\N	46.00	17.00	41.00	Y 	1	01	Y       	4 	92	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	57c8411c-3380-418e-b4b6-9da2dbccb6fb	a849f26a-943e-4d82-8b6f-9d2fba044e23	22	\N	\N	\N	78	\N	70	70	36	36	45	45	30	30	20	20	34	34	46	46	32	32	92	92	68	68	5 	5 	24	24	QitKZjIhO0SovQYzrO9PfV1o1b0P0xRtKwJwFRqc
266	N 	R0003	UNKNOWN	64	1	4	140	63	0	0	0	0	44	09	65	1	1	\N	1	\N	1	3	3	3	3	2	\N	80.00	86.00	77.00	Y 	1	03	N       	4 	46	Y	N	N	N	N	W	2016-01-13	math	\N	\N	6e0b03a7-fb69-4025-b91e-c076e171f1c3	dc642ad5-1a8a-481d-9b62-13cbf7d55262	49	\N	\N	\N	17	\N	76	76	6	6	10	10	71	71	13	13	6	6	84	84	58	58	4	4	46	46	33	33	20	20	4xNcr39DqN64ORBLFyIG322oypneHyCBYVXKZZPm
267	N 	R0003	UNKNOWN	65	1	4	141	59	0	0	0	0	45	09	38	1	1	\N	1	\N	1	3	1	3	2	3	\N	8.00	5.00	64.00	Y 	1	02	N       	5 	93	N	N	N	N	N	N	2016-01-13	math	\N	\N	30bb2e2c-1aec-4ea4-9069-f3a947ac13cd	24851366-6aca-47f7-9918-a1bf41203ad2	27	\N	\N	\N	35	\N	97	97	71	71	46	46	91	91	13	13	20	20	19	19	11	11	16	16	72	72	5 	5 	52	52	GxZdYAWx8b0sf9CbdWYT8HsXYqBnNikEkDMikPkd
268	N 	R0003	UNKNOWN	19	1	4	142	19	0	0	0	0	46	10	127	2	2	\N	1	\N	1	2	2	2	3	3	\N	8.00	16.00	28.00	Y 	1	02	N       	5 	2	Y	N	N	N	N	W	2016-01-13	math	\N	\N	028647c5-df78-46dc-b0b9-760586f55fc8	946c7229-b0c2-4adc-8655-21ac5a0f483e	44	\N	\N	\N	62	\N	7	7	84	84	82	82	11	11	20	20	25	25	12	12	41	41	56	56	41	41	5 	5 	94	94	sB2VXSoaEMtiSg4WjFUXNNdh3rDSEvULHmGXvAnG
269	N 	R0003	UNKNOWN	38	1	4	143	38	0	0	0	0	47	12	439	7	5	\N	3	\N	1	1	1	2	3	1	\N	33.00	41.00	34.00	N 	1	01	Y       	5 	21	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	af618355-c475-4f32-a6da-ca83fc385dab	25cdc95e-7b0d-4be0-be0a-fa9f6622c643	25	\N	\N	\N	15	\N	47	47	61	61	6	6	73	73	85	85	91	91	93	93	42	42	29	29	69	69	22	22	58	58	c0pT5dPt1QuTicyE2IWo13jRf8kh2ep7ZI8flg9g
270	N 	R0003	UNKNOWN	66	1	5	144	58	0	0	0	0	48	09	349	8	4	\N	5	\N	1	1	2	2	1	3	\N	80.00	27.00	37.00	N 	1	03	N       	5 	62	N	N	N	N	N	N	2016-01-13	math	\N	\N	87546bfd-3a03-4365-ae75-c05ccb1258cc	0a5663b1-e04a-4d9b-b301-5799013cc00e	52	\N	\N	\N	37	\N	44	44	66	66	99	99	91	91	81	81	87	87	31	31	63	63	42	42	29	29	1 	1 	62	62	urU46yAVop0SQHyO35ugSyxZHrWM1NDDyJY3g4Uz
271	N 	R0003	UNKNOWN	67	1	5	145	63	0	0	0	0	49	09	28	8	1	\N	1	\N	1	2	2	3	1	3	\N	25.00	85.00	31.00	N 	1	01	Y       	1 	80	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	6d279ae3-e49a-4dcf-84f5-25374dc74213	2ee928e7-16cd-41e8-a410-424c1f1b389d	23	\N	\N	\N	79	\N	89	89	73	73	34	34	34	34	41	41	9	9	92	92	37	37	1	1	16	16	1 	1 	70	70	VfwdANZxA7Ak4zCPTWDwK9Zd47uiFTJp4rbBTmMs
272	N 	R0003	UNKNOWN	20	1	5	146	20	0	0	0	0	50	10	253	9	3	\N	5	\N	4	1	3	3	1	2	\N	67.00	46.00	21.00	N 	1	01	Y       	1 	58	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	77c6d6a6-9b59-4189-b11b-75aedde7511c	84c7bec7-19f5-4c4b-b76a-da685a216f4e	45	\N	\N	\N	35	\N	23	23	4	4	22	22	43	43	74	74	54	54	82	82	90	90	97	97	27	27	1 	1 	52	52	xhHUuTmLdRjylYGiO2ZU54h2J04u9BJSn0exbdX2
273	N 	R0003	UNKNOWN	68	1	5	147	60	0	0	0	0	51	09	222	8	3	\N	5	\N	4	1	3	1	3	3	\N	7.00	46.00	50.00	N 	1	01	Y       	1 	43	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	1694db1e-07a8-44a0-9bd6-92afa9204d8d	18a52a80-f329-4510-832c-7e4e6f731d45	14	\N	\N	\N	71	\N	46	46	32	32	97	97	12	12	71	71	6	6	5	5	61	61	17	17	25	25	1 	1 	83	83	8Kjp09Mfair9T4YbPnMaq7t35stJce29GjDxDHzD
274	N 	R0003	UNKNOWN	69	1	5	148	64	0	0	0	0	52	09	202	10	3	\N	6	\N	6	2	3	2	1	2	\N	49.00	41.00	68.00	N 	1	01	Y       	1 	60	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	3c3e4b7e-61e1-49dc-80d6-d29745ae2db3	84f9b073-9500-4065-a812-b98dd61081fd	7	\N	\N	\N	3	\N	82	82	56	56	87	87	90	90	0	0	31	31	57	57	83	83	74	74	50	50	1 	1 	67	67	xro1vzHRQAemvh8tb1iV4F2Yr9Tf5BmZI5F1UzlJ
276	N 	R0003	UNKNOWN	22	1	5	150	22	0	0	0	0	54	10	114	5	2	\N	2	\N	1	1	1	1	3	2	\N	33.00	24.00	13.00	N 	1	01	Y       	2 	16	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	76040d10-ba95-444b-a29e-da65a3679d01	4a0491b6-99cc-49e0-947b-c1729ba19299	57	\N	\N	\N	19	\N	48	48	80	80	89	89	92	92	64	64	15	15	75	75	91	91	50	50	98	98	1 	1 	97	97	8qadBD3xrsvpfExXtJw6dHilQMcdZHjWg60GuV8D
277	N 	R0003	UNKNOWN	91	1	5	151	86	0	0	0	0	55	11	451	9	5	\N	1	\N	1	3	3	2	2	2	\N	62.00	50.00	17.00	N 	1	01	Y       	2 	94	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	2aff6dcf-e2de-4ec4-9212-a761f29e0e6f	2cee117a-37dc-4fd5-a76c-e7d39112ae8c	89	\N	\N	\N	97	\N	55	55	4	4	16	16	74	74	75	75	59	59	33	33	59	59	32	32	82	82	1 	1 	33	33	xkO5Umv7TjxiZY8CD926MY27uoz1edQFcaWU76Ef
278	N 	R0003	UNKNOWN	23	1	5	152	23	0	0	0	0	56	10	55	4	1	\N	4	\N	4	3	3	1	1	2	\N	37.00	86.00	60.00	N 	1	02	N       	3 	33	Y	N	N	N	N	W	2016-01-13	math	\N	\N	c2043a85-fb32-440a-8ca1-9f1f657aa4a2	fe9d4719-6154-4bdb-b453-70a638f2713d	78	\N	\N	\N	70	\N	64	64	47	47	59	59	5	5	65	65	78	78	61	61	1	1	91	91	92	92	4 	4 	50	50	DSoDwPOg0kZQ5YPnMJ0SLmBaCeGes4a1qjkRUQfd
279	N 	R0003	UNKNOWN	92	1	5	153	87	0	0	0	0	57	11	345	8	4	\N	5	\N	1	2	3	1	2	2	\N	80.00	20.00	44.00	N 	1	03	N       	3 	21	Y	N	N	N	N	W	2016-01-13	math	\N	\N	66e13a15-faaa-47d2-b082-a8d5e8aaea6c	d258c1aa-1e15-4a35-82d5-3626eba44e72	39	\N	\N	\N	35	\N	4	4	74	74	68	68	45	45	49	49	61	61	69	69	21	21	47	47	50	50	5 	5 	94	94	wTpuVRx1CP4eXjjQiCKXjjvvLaZpRAAujc0QB30k
280	N 	R0003	UNKNOWN	39	1	5	154	39	0	0	0	0	58	12	2	3	1	\N	3	\N	1	2	1	2	1	2	\N	24.00	60.00	37.00	N 	1	01	Y       	4 	76	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	e13c29ed-0577-46bd-b6c0-86092c68a12b	cc01e107-9239-43ae-95bd-96c8414578de	22	\N	\N	\N	5	\N	82	82	97	97	74	74	6	6	66	66	34	34	92	92	36	36	38	38	30	30	5 	5 	24	24	w7TmiOs6CWJsXS2l1crivMsf42ZlQ5WAr6Xri2rd
281	N 	R0003	UNKNOWN	40	1	5	155	40	0	0	0	0	59	12	456	7	5	\N	7	\N	1	1	3	1	3	1	\N	31.00	43.00	55.00	N 	1	01	Y       	4 	90	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	eed942f3-d554-40f2-b547-446a9e1165bf	0f1cc7c9-72fb-4e1a-b5ed-babe3e67e731	1	\N	\N	\N	3	\N	30	30	78	78	28	28	16	16	61	61	2	2	63	63	39	39	72	72	58	58	5 	5 	59	59	e3OGNkIbM4h2skSI4nPuESDwrF1EMmjIfrprGMHz
282	N 	R0003	UNKNOWN	93	1	5	156	88	0	0	0	0	60	11	47	10	1	\N	9	\N	8	1	2	1	3	3	\N	1.00	36.00	51.00	N 	1	02	N       	4 	59	Y	N	N	N	N	W	2016-01-13	math	\N	\N	1b03efd2-1d1e-4110-9350-22185753b6ba	bcb27126-7164-4c1e-aa16-5a267adc202b	79	\N	\N	\N	46	\N	46	46	17	17	56	56	92	92	66	66	50	50	21	21	28	28	5	5	25	25	58	58	66	66	jgliMmu6B15PbLy7sq7uRlv40qiIF3liNI3ySpru
283	N 	R0003	UNKNOWN	70	1	5	157	65	0	0	0	0	61	09	494	9	5	\N	5	\N	5	2	3	2	1	2	\N	41.00	13.00	32.00	N 	1	01	N       	1 	84	N	N	N	N	N	N	2016-01-13	math	\N	\N	382b343d-1b02-4b96-b1cb-2644db2e9f09	df50c3b5-a7b7-4232-b5f9-b12e6d6b2d5d	97	\N	\N	\N	5	\N	7	7	55	55	75	75	7	7	91	91	55	55	15	15	6	6	39	39	78	78	8 	8 	91	91	o557yJ1Kx4YrfB8oMHfoaDVLMTFj3DJXWrKMm9zT
284	N 	R0003	UNKNOWN	24	1	5	158	24	0	0	0	0	62	10	281	3	3	\N	3	\N	3	2	3	3	1	1	\N	18.00	9.00	19.00	N 	1	03	N       	2 	67	Y	N	N	N	N	W	2016-01-13	math	\N	\N	1101d194-08b9-4632-b655-513319ed9a07	29c7e4cb-901c-4dfb-9fb2-b02470a1e261	55	\N	\N	\N	19	\N	84	84	70	70	92	92	59	59	62	62	92	92	24	24	89	89	24	24	97	97	8 	8 	29	29	WW9iCRpiTricUbWoelchVevYIJHSjhAUMpLVYt6D
285	N 	R0003	UNKNOWN	94	1	5	159	89	0	0	0	0	63	11	299	4	3	\N	2	\N	2	1	1	3	3	3	\N	68.00	74.00	33.00	N 	1	01	Y       	3 	21	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	f620a09f-9103-4446-b234-1c736f15500c	ad2f4799-f9ad-42a8-b72b-571d7ae93e4f	42	\N	\N	\N	73	\N	1	1	8	8	74	74	8	8	0	0	68	68	83	83	7	7	20	20	17	17	8 	8 	21	21	Uk2E8MeExKfyl6XHBLKpNJe6na8bi3nj8yMCgeFz
286	N 	R0003	UNKNOWN	41	1	5	160	41	0	0	0	0	64	12	121	1	2	\N	1	\N	1	2	2	3	2	2	\N	78.00	27.00	23.00	N 	1	01	Y       	4 	49	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	a16f02a7-151a-4b29-8203-8568a8746591	796e9f05-298a-436f-8c92-3539c4789c78	56	\N	\N	\N	100	\N	88	88	21	21	91	91	51	51	58	58	64	64	36	36	50	50	67	67	84	84	8 	8 	86	86	oBPTLwB89Yetjpa88VqxOvlIENOqe4XKGKnOrJfM
287	N 	R0003	UNKNOWN	42	1	5	161	42	0	0	0	0	65	12	441	10	5	\N	9	\N	3	3	2	2	3	1	\N	94.00	8.00	66.00	N 	1	01	Y       	5 	8	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	22b58ccc-2418-4508-bc73-12b3ff68dac3	682893a2-d4e3-48fc-8988-a39a7fadd847	96	\N	\N	\N	68	\N	63	63	82	82	63	63	80	80	46	46	11	11	46	46	39	39	80	80	43	43	8 	8 	3 	3 	6boPVVoO1Vuyg8VJ5AqV0Zha4rVGrZt3uPrM7xgw
288	N 	R0003	UNKNOWN	43	1	5	162	43	0	0	0	0	66	12	22	7	1	\N	3	\N	1	1	2	2	1	2	\N	31.00	34.00	62.00	N 	1	01	Y       	1 	80	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	0b674c27-ad68-4597-91bc-01fe8e4d57d2	26e7906a-23f8-4e70-b869-a1a9703623ee	71	\N	\N	\N	72	\N	72	72	71	71	28	28	31	31	40	40	49	49	99	99	65	65	11	11	2	2	8 	8 	94	94	KL3R8L1EaO8MgymDcMI59fe7DnjLk0ZuDZiwO2J2
289	N 	R0003	UNKNOWN	95	1	5	163	90	0	0	0	0	67	11	119	9	2	\N	8	\N	2	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y       	2 	4	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	29316d10-43dc-4203-b882-4eb9d9806f44	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	LQWpnLVTSTAXEFoit9Q2RlZNibkOFcpmSYgXMHyo
290	N 	R0003	UNKNOWN	95	2	6	164	90	0	0	0	0	68	11	185	3	2	\N	1	\N	1	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y       	2 	4	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	f11989d2-8ab6-4d39-a946-827c4fc69474	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	5RsVRnISVN942m5YIo50ucTxyB8cYNObQ1tnrORJ
291	N 	R0003	UNKNOWN	95	2	6	165	90	0	0	0	0	69	11	199	5	2	\N	2	\N	2	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y       	2 	4	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	9c4c9b2f-52c5-474e-87e8-89120e4863af	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	GXr7X73TjgGmTEm62Q3cGYSMBHE34YWvhwJEalyy
292	N 	R0003	UNKNOWN	95	2	6	101	90	0	0	0	0	70	11	113	4	2	\N	3	\N	1	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y       	2 	4	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	d566e123-c622-4a3b-8e3e-1f7b5f21ee58	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	m5i2mSxYFwrFTnW09BEYlrXBvQIw3bTmo2zm1GaI
293	N 	R0002	UNKNOWN	7	1	2	166	7	0	0	0	0	71	10	309	1	4	\N	1	\N	1	2	2	1	1	3	\N	31.00	73.00	77.00	Y 	1	01	Y       	4 	23	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	28e46644-56b0-439d-aece-4a88f147b218	d3525cec-7727-4fd8-b961-16a1a592ffad	75	\N	\N	\N	26	\N	96	96	14	14	42	42	50	50	46	46	65	65	90	90	19	19	70	70	17	17	2 	2 	43	43	JskCum0I6ZULH064xsFeFcW4eZVTIEj5m6zExxG2
294	N 	R0002	UNKNOWN	81	1	2	167	76	0	0	0	0	72	11	106	5	2	\N	1	\N	1	1	2	1	1	1	\N	31.00	0.00	77.00	Y 	1	01	Y       	4 	8	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	db94ba6c-3abe-4e9a-8bbb-b680531efbab	03f78eb8-5818-4e39-b320-b1a0743696dc	15	\N	\N	\N	26	\N	96	96	58	58	15	15	47	47	10	10	35	35	88	88	27	27	67	67	39	39	3 	3 	80	80	p69xZhxSMQuWmke62h3j1K7izaj1evXeklkHFuMg
295	N 	R0002	UNKNOWN	30	1	2	168	30	0	0	0	0	73	12	447	5	5	\N	3	\N	1	3	1	3	2	2	\N	78.00	71.00	77.00	Y 	1	01	Y       	1 	70	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	54488102-dc6f-45fb-8941-14b9dc51c977	0a166472-d22d-4d7f-985f-1c70b5920ac4	18	\N	\N	\N	53	\N	63	63	23	23	96	96	26	26	5	5	63	63	4	4	39	39	14	14	22	22	4 	4 	20	20	3i8VkcJCs5QffVgj2QOmZr3MLv40I0KJc5k5DcWe
296	N 	R0002	UNKNOWN	83	1	2	169	78	0	0	0	0	74	11	352	4	4	\N	1	\N	1	2	3	1	3	1	\N	73.00	6.00	12.00	Y 	1	01	Y       	2 	35	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	30942862-4d08-4505-a85e-798d8065de42	b8683f57-f7b0-49e0-9d18-5ae7910fe323	5	\N	\N	\N	81	\N	32	32	22	22	80	80	73	73	4	4	67	67	27	27	29	29	39	39	43	43	5 	5 	27	27	OimZrYW80W7xl7QUa4Hg9Q9knjdvys1Z8wRIVHuG
298	N 	R0002	UNKNOWN	31	1	2	170	31	0	0	0	0	75	12	352	9	4	\N	9	\N	3	2	3	1	1	3	\N	98.00	4.00	41.00	Y 	1	01	Y       	3 	17	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	93e5e92a-27bb-4683-b84f-6f661c9e47a2	47b9a539-930d-4371-91ab-4a123c81765b	50	\N	\N	\N	63	\N	23	23	72	72	44	44	80	80	98	98	62	62	1	1	95	95	41	41	1	1	40	40	88	88	N1eKk2eGo15Htuapk7zDnIIftLz18aVE1PMPSktf
299	N 	R0002	UNKNOWN	85	1	2	171	80	0	0	0	0	76	11	451	5	5	\N	2	\N	1	2	3	1	3	2	\N	4.00	68.00	18.00	Y 	1	01	Y       	3 	28	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	8559ab78-3be5-4ff9-8966-e8c4c6f3b8df	f771a54c-ba15-4724-b81e-e03ce7e257c2	86	\N	\N	\N	19	\N	19	19	84	84	62	62	2	2	58	58	92	92	83	83	57	57	92	92	22	22	6 	6 	72	72	oGYUvFLatZjgEBWy3LV9NwMdmc9GT1GTerNuABS9
300	N 	R0002	UNKNOWN	72	1	2	172	67	0	0	0	0	77	11	131	8	2	\N	8	\N	3	3	1	2	1	1	\N	56.00	20.00	55.00	Y 	1	01	Y       	4 	84	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	e56959c5-c419-45b3-a1ab-327e78b10aeb	1b488539-2999-44bb-9b84-e3152e387b71	53	\N	\N	\N	34	\N	67	67	54	54	87	87	64	64	30	30	43	43	89	89	98	98	21	21	18	18	55	55	12	12	sstpR8WRh7effxahZuK1ZPaQJmLH10nFDTSm23am
301	N 	R0002	UNKNOWN	74	1	2	173	69	0	0	0	0	78	11	403	3	5	\N	2	\N	2	1	3	2	2	3	\N	47.00	37.00	0.00	Y 	1	01	Y       	4 	57	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	ce42c551-d724-4718-b769-31a796be9581	e9a4761f-f9b6-4147-bb0e-2e6c21ed3815	26	\N	\N	\N	46	\N	11	11	85	85	74	74	85	85	16	16	70	70	41	41	85	85	84	84	58	58	44	44	16	16	Uetza1KA3EqTldkdiq0w92KFYhVqBL6zLWinhXI9
302	N 	R0002	UNKNOWN	32	1	2	174	32	0	0	0	0	79	12	318	8	4	\N	1	\N	1	2	1	1	3	2	\N	97.00	69.00	26.00	Y 	1	01	N       	5 	64	N	N	N	N	N	N	2016-01-13	math	\N	\N	debff429-22ea-48a9-b5f0-feeddefc2e38	4d064a91-3016-400d-be28-4d30dff82a3e	27	\N	\N	\N	44	\N	89	89	29	29	92	92	43	43	27	27	56	56	20	20	53	53	97	97	18	18	3 	3 	36	36	pB7SBPI9whDTX6jqMTvlCa5POI7wiii3ik8YSqUN
303	N 	R0002	UNKNOWN	8	1	2	175	8	0	0	0	0	80	10	273	3	3	\N	1	\N	1	1	3	2	3	1	\N	88.00	85.00	58.00	Y 	1	01	Y       	5 	22	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	4243c86e-837a-4255-b667-7b4b8195862f	1113ce38-64b9-4bfb-a5c4-0d76034b85bc	24	\N	\N	\N	99	\N	13	13	34	34	40	40	14	14	59	59	55	55	98	98	80	80	89	89	78	78	2 	2 	16	16	EOHTQnkhMw4mzKP8Rpaq42vOlOBZzsZZkKU6zpll
304	N 	R0002	UNKNOWN	9	1	2	176	9	0	0	0	0	81	10	312	7	4	\N	3	\N	1	2	3	1	1	2	\N	98.00	4.00	99.00	Y 	1	01	Y       	5 	42	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	1ec80e05-8f1c-4a65-9349-14694bbbac78	e48992d5-d8b8-4025-82fd-8a2422b74106	94	\N	\N	\N	39	\N	17	17	13	13	22	22	82	82	76	76	78	78	1	1	67	67	30	30	67	67	1 	1 	62	62	UH69Q4IEQrjnQGvI9xhrNRTWUtmihpX8hlQ4r61X
305	N 	R0002	UNKNOWN	10	1	2	177	10	0	0	0	0	82	10	78	7	1	\N	3	\N	1	3	2	2	2	2	\N	1.00	71.00	95.00	Y 	1	01	Y       	5 	84	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	307a76ba-cda8-43fc-8953-55795759032e	2b7d84f3-08b8-4a7c-b05b-285dcbfd1eb2	37	\N	\N	\N	54	\N	11	11	26	26	80	80	18	18	97	97	54	54	54	54	5	5	98	98	50	50	1 	1 	76	76	Fg5kGWmyfjDA600mHsVyZJALKlRwmg8pe1LtPdSb
306	N 	R0002	UNKNOWN	51	1	2	178	51	0	0	0	0	83	09	324	9	4	\N	4	\N	4	2	1	3	1	3	\N	5.00	60.00	8.00	Y 	1	01	Y       	1 	61	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	67c4ce05-a8cd-43f8-af8e-075fb70b3ecb	efcd37fa-0da7-4e2c-9647-3018df45354b	25	\N	\N	\N	93	\N	73	73	85	85	18	18	4	4	13	13	9	9	64	64	2	2	57	57	6	6	1 	1 	26	26	uJkYBTpWyQhQPsBC4PFyGIFqcoyKQiHxv4f3IYsu
307	N 	R0002	UNKNOWN	52	1	2	179	52	0	0	0	0	84	09	118	3	2	\N	2	\N	1	1	2	2	2	3	\N	80.00	63.00	42.00	Y 	1	03	N       	2 	55	N	N	N	N	N	N	2016-01-13	math	\N	\N	91b0e6a7-e546-4c7f-8d51-d1890957b3c8	fe1fd316-c11b-4a80-9770-6db56d3a755d	41	\N	\N	\N	18	\N	68	68	61	61	12	12	31	31	54	54	57	57	59	59	83	83	55	55	14	14	1 	1 	33	33	0YrEK4Y3PihDRZiZnFMinOcTdkNYwublx9Z8lCAp
308	N 	R0001	UNKNOWN	3	1	1	180	3	0	0	0	0	85	10	270	5	3	\N	1	\N	1	3	3	1	1	2	\N	98.00	37.00	62.00	Y 	1	01	Y       	4 	41	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	cdae1117-12b9-4077-be15-af2eb5db0e7a	6790bcd5-b5a3-470c-a715-3918d86b0418	76	\N	\N	\N	26	\N	36	36	5	5	31	31	24	24	70	70	58	58	38	38	81	81	45	45	8	8	6 	6 	57	57	SBFMQrkvsLNLQWnEXVVeN6ZMQXqemPhTY44hgO2M
309	N 	R0001	UNKNOWN	4	1	1	181	4	0	0	0	0	85	10	53	5	1	\N	4	\N	1	3	2	3	1	1	\N	59.00	22.00	99.00	Y 	1	01	N       	4 	73	N	N	N	N	N	N	2016-01-13	math	\N	\N	89450f20-5621-4cc8-bd71-709834a8631e	5e07c335-586b-4de1-926d-9f6e5bcbf36d	1	\N	\N	\N	41	\N	22	22	53	53	24	24	44	44	60	60	80	80	7	7	60	60	94	94	48	48	10	10	48	48	OIeWicvTvk5kcbEc0qj7vPo2nh3Vr8XS9e2Vz57b
310	N 	R0003	UNKNOWN	95	1	5	182	90	0	0	0	0	85	11	164	3	2	\N	1	\N	1	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y       	2 	4	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	dfc56f94-6e12-4a87-971d-c564c3500050	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	FkgQ5OyZAMUDgnC8f6cJHnpLkzOlGV6xnArV3Bhf
311	N 	R0003	UNKNOWN	95	1	5	183	90	0	0	0	0	85	11	191	3	2	\N	1	\N	1	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y       	2 	4	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	3a892c83-2897-4303-9308-a7b060643eb0	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	5RsVRnISVN942m5YIo50ucTxyB8cYNObQ1tnrORJ
312	N 	R0003	UNKNOWN	95	1	5	184	90	0	0	0	0	85	11	193	5	2	\N	2	\N	2	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y       	2 	4	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	9081f4ad-1e35-4b9d-a6e1-669e07d7e727	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	GXr7X73TjgGmTEm62Q3cGYSMBHE34YWvhwJEalyy
313	N 	R0003	UNKNOWN	95	1	5	185	90	0	0	0	0	85	11	129	6	2	\N	3	\N	1	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y       	2 	4	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	9d9d732f-d5bc-4c91-aa5a-308663dfa606	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	GFWLlMJ65UE7pSrpAPrt2Tu5hoZkaeJdY0mrbTTU
314	N 	R0003	UNKNOWN	95	1	5	186	90	0	0	0	0	86	11	110	8	2	\N	6	\N	2	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y       	2 	4	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	11341062-4858-4bc4-a301-f94f95adeeae	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	DuncASxHMXlhtve8RRDsuUQ83LaR8gIZ7rFEP0zs
315	N 	R0001	UNKNOWN	44	1	1	187	44	0	0	0	0	86	05	335	6	4	\N	1	\N	1	2	2	3	1	3	\N	86.00	93.00	77.00	Y 	1	01	Y       	1 	1	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	13162932-2ee6-4d59-b595-fb23ff0b8efd	5d2f2e17-69d4-4f19-918e-5feeecdd18dc	46	\N	\N	\N	94	\N	38	38	92	92	12	12	59	59	55	55	4	4	62	62	46	46	56	56	75	75	0 	0 	39	39	5336b7258eb545b3ae7178aeb0cfd72428a36fdb
316	N 	R0001	UNKNOWN	25	1	1	188	25	0	0	0	0	87	12	379	1	4	\N	1	\N	1	2	3	1	2	1	\N	52.00	75.00	34.00	Y 	1	03	N       	1 	99	Y	N	N	N	N	W	2016-01-13	math	\N	\N	771d6032-621f-47b3-8113-2aeffcb7bffa	aeb7e402-410b-4cc6-b4d0-0cb82510a292	53	\N	\N	\N	4	\N	70	70	84	84	37	37	66	66	28	28	86	86	50	50	69	69	65	65	18	18	5 	5 	87	87	UMaYGd8hto4TYc7EiWKtivzti8YAWg5xozomFI8q
317	N 	R0001	UNKNOWN	26	1	1	189	26	0	0	0	0	88	12	298	7	3	\N	2	\N	1	1	2	2	3	3	\N	3.00	58.00	8.00	Y 	1	02	N       	1 	78	Y	N	N	N	N	W	2016-01-13	math	\N	\N	bb63c6c4-07db-4b0a-974f-59fcaf94c189	a8889163-df6e-4493-8c62-eaab736a2e9e	3	\N	\N	\N	31	\N	71	71	96	96	11	11	72	72	21	21	92	92	34	34	64	64	45	45	81	81	3 	3 	25	25	qEy737ea1Emx97d0RTxk7QpDWecnUu2eEBWvQXkK
318	N 	R0001	UNKNOWN	27	1	1	190	27	0	0	0	0	89	12	358	2	4	\N	2	\N	1	2	2	2	1	1	\N	25.00	22.00	34.00	Y 	1	01	N       	1 	26	N	N	N	N	N	N	2016-01-13	math	\N	\N	92349e8e-73bc-46cf-9ed2-5025cb2c4857	3b1e928f-e7d6-4730-b142-b27ba87c8942	52	\N	\N	\N	64	\N	56	56	44	44	67	67	24	24	93	93	95	95	70	70	85	85	10	10	34	34	7 	7 	91	91	MXGplB4M232z2j4FNTStjIAKRER2ujJ8Z2OlV7Yn
319	N 	R0001	UNKNOWN	48	1	1	191	48	0	0	0	0	90	09	439	10	5	\N	7	\N	1	3	3	1	1	1	\N	73.00	22.00	3.00	Y 	1	02	N       	1 	7	N	N	N	N	N	N	2016-01-13	math	\N	\N	d0af2f64-adec-4ab8-b9e0-ce0f2e9631cf	2315ba50-22e9-4c45-9b26-a4b6f6808a2c	32	\N	\N	\N	90	\N	69	69	64	64	38	38	78	78	54	54	89	89	76	76	45	45	71	71	41	41	8 	8 	34	34	ITh1jmueUE6G8rMT9oKqLXZKZMPKahZUoAUuvOQE
320	N 	R0001	UNKNOWN	6	1	1	192	6	0	0	0	0	91	10	299	5	3	\N	5	\N	4	1	1	2	2	3	\N	25.00	83.00	42.00	Y 	1	01	Y       	2 	97	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	39d27e35-2285-4c06-83ab-a00f52537471	26226e3a-6b61-422a-9892-75753c89149e	26	\N	\N	\N	86	\N	46	46	84	84	37	37	27	27	19	19	0	0	42	42	60	60	48	48	37	37	6 	6 	91	91	RqHsCUovoRLSAhySU0AnRx0LY8YKin9uDhSFcOMD
321	N 	R0001	UNKNOWN	79	1	1	193	74	0	0	0	0	92	11	118	2	2	\N	2	\N	2	2	3	3	3	1	\N	16.00	60.00	69.00	Y 	1	01	Y       	2 	52	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	291c9cf8-bc4d-4af1-a900-2fa77efddd1b	05864e9c-4e09-4522-b1be-ab6b10683287	2	\N	\N	\N	8	\N	80	80	21	21	60	60	98	98	54	54	42	42	69	69	19	19	62	62	69	69	9 	9 	17	17	JL6uVW6OSVaYqpvndl0foBKAFrf4bdz6HACTTcLd
322	N 	R0001	UNKNOWN	28	1	1	194	28	0	0	0	0	93	12	363	8	4	\N	7	\N	6	1	1	1	2	2	\N	34.00	96.00	87.00	Y 	1	01	Y       	2 	40	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	447eef05-927f-4a94-baaa-593c538366d0	25be4c66-e8ae-40bf-836d-473b9e1f9572	17	\N	\N	\N	41	\N	30	30	16	16	57	57	98	98	15	15	41	41	78	78	8	8	3	3	10	10	20	20	98	98	vwkr0R9sEgA872YY02LOWZFwoTLq0ATfnEvZ0i17
323	N 	R0002	UNKNOWN	49	1	2	195	49	0	0	0	0	94	09	131	7	2	\N	4	\N	3	2	3	3	2	1	\N	33.00	64.00	39.00	Y 	1	01	N       	2 	36	N	N	N	N	N	N	2016-01-13	math	\N	\N	ba04ad27-ce18-42d8-ba7f-75bebee4ca6e	721d0233-ca2e-4561-b003-a26a30a592cd	37	\N	\N	\N	69	\N	59	59	96	96	60	60	98	98	87	87	74	74	35	35	99	99	40	40	84	84	10	10	55	55	mqZIVP2jMSn1VHrJ6nLaEHLblTiXW3eCC0BZDA6d
324	N 	R0002	UNKNOWN	50	1	2	196	50	0	0	0	0	95	09	146	4	2	\N	4	\N	4	3	3	3	3	1	\N	58.00	64.00	10.00	Y 	1	03	N       	2 	87	N	N	N	N	N	N	2016-01-13	math	\N	\N	207191be-0e63-4879-b5af-ec0d5b5c6426	85b0d5d9-b646-4881-a913-c887dc9a6112	21	\N	\N	\N	79	\N	47	47	83	83	12	12	32	32	27	27	62	62	58	58	7	7	83	83	18	18	11	11	2 	2 	Ew6FoKuAKYas7HNCC2qpWTLldXJFPKY6WuKGaoCK
325	N 	R0002	UNKNOWN	5	1	2	197	5	0	0	0	0	96	10	398	1	4	\N	1	\N	1	2	2	2	2	2	\N	83.00	2.00	25.00	Y 	1	01	Y       	2 	60	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	80770855-70a5-432b-921d-74930d96679d	8fa85f04-f1a7-4282-97b1-9d2e5c60afe7	57	\N	\N	\N	45	\N	13	13	97	97	40	40	39	39	1	1	82	82	37	37	35	35	5	5	14	14	2 	2 	48	48	uB6VLZuP4JnB0aZW3u9Ol9pUS9ux9VdAuAaQ19fX
326	N 	R0002	UNKNOWN	29	1	2	198	29	0	0	0	0	97	12	293	9	3	\N	3	\N	3	3	1	3	3	2	\N	37.00	72.00	91.00	Y 	1	01	N       	3 	86	N	N	N	N	N	N	2016-01-13	math	\N	\N	48544429-c044-413a-8d7e-cbf933e6128e	617f992f-b740-46a2-8cef-51deaa61e4dd	92	\N	\N	\N	88	\N	86	86	35	35	98	98	89	89	3	3	52	52	75	75	44	44	20	20	34	34	3 	3 	75	75	rMB447lffsMQCZOQPIUTPusiGmZolvpxn7jMAsiT
327	N 	R0001	UNKNOWN	44	1	1	199	44	0	0	0	0	98	09	335	6	4	\N	1	\N	1	2	2	3	1	3	\N	86.00	93.00	77.00	Y 	1	01	Y       	1 	1	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	13162932-2ee6-4d59-b595-fb23ff0b8efd	5fce8c4c-4bfd-4384-b213-b020e14cd99a	46	\N	\N	\N	94	\N	38	38	92	92	12	12	59	59	55	55	4	4	62	62	46	46	56	56	75	75	0 	0 	39	39	EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q
328	N 	R0001	UNKNOWN	45	1	1	200	45	0	0	0	0	99	09	225	8	3	\N	3	\N	1	3	1	1	1	1	\N	37.00	76.00	65.00	Y 	1	02	N       	2 	54	Y	N	N	N	N	W	2016-01-13	math	\N	\N	d21f2726-9b67-4e37-845a-06fd98459c55	acb10f83-d802-4780-91b4-97a656528f11	7	\N	\N	\N	15	\N	26	26	45	45	70	70	49	49	45	45	15	15	96	96	53	53	4	4	38	38	1 	1 	83	83	2vM9yk86Q3OXKckaeaUjcSTHf1TaO5LCT2DHaeU7
329	N 	R0001	UNKNOWN	46	1	1	201	46	0	0	0	0	100	09	164	1	2	\N	1	\N	1	2	2	3	3	1	\N	62.00	33.00	30.00	Y 	1	02	N       	3 	54	Y	N	N	N	N	W	2016-01-13	math	\N	\N	1b7f6c16-8846-47fb-b925-581b4bead589	062e5472-346e-4c95-a732-1cfe373e1683	19	\N	\N	\N	45	\N	92	92	42	42	94	94	69	69	41	41	50	50	45	45	97	97	64	64	28	28	2 	2 	54	54	dxpojnotJFaxas2YDJMiJoBPHkVCIc4lptuj24Dy
330	N 	R0001	UNKNOWN	47	1	1	202	47	0	0	0	0	101	09	381	10	4	\N	6	\N	3	1	3	1	2	3	\N	42.00	6.00	8.00	Y 	1	01	Y       	4 	26	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	1f4e4ff7-80f1-4fb8-87c6-4a6502bb24d7	f7091995-0540-4f7f-be3f-7467572b04cf	85	\N	\N	\N	32	\N	21	21	89	89	76	76	98	98	97	97	3	3	68	68	72	72	38	38	35	35	3 	3 	96	96	MyXTFn1S8qz5lQJAjw11CeaMjoaIiQbo8KihC5ix
331	N 	R0001	UNKNOWN	1	1	1	203	1	0	0	0	0	102	10	66	7	1	\N	2	\N	2	1	3	3	1	2	\N	90.00	76.00	61.00	Y 	1	01	Y       	5 	26	N	Y	Y	Y	Y	Y	2016-01-13	math	\N	\N	bf21bb13-abd7-4ac3-89c9-8c3c7214ca4c	37919430-cf2a-48a0-bcc0-fda383f55269	100	\N	\N	\N	94	\N	8	8	20	20	0	0	15	15	45	45	80	80	89	89	42	42	56	56	68	68	4 	4 	1 	1 	jEEReGK9NLZr2teqi8dFBUhxaozkY3uwy5IB7hUx
332	N 	R0001	UNKNOWN	2	1	1	204	2	0	0	0	0	103	10	273	5	3	\N	3	\N	2	3	3	1	3	3	\N	55.00	49.00	61.00	Y 	1	02	N       	4 	28	Y	N	N	N	N	W	2016-01-13	math	\N	\N	9c74587a-e7c9-4d30-af66-15399b941ca7	c6aee337-ef60-4227-8769-c4e8f25842e6	33	\N	\N	\N	21	\N	38	38	84	84	94	94	86	86	59	59	31	31	35	35	52	52	73	73	49	49	5 	5 	34	34	1hIns0clANFCb9Rq3Cj5vEUUhMzryD9monuIwTFq
\.


--
-- Name: dim_accomod_pkey; Type: CONSTRAINT; Schema: analytics; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_accomod
    ADD CONSTRAINT dim_accomod_pkey PRIMARY KEY (_key_accomod);


--
-- Name: dim_opt_state_data_pkey; Type: CONSTRAINT; Schema: analytics; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_opt_state_data
    ADD CONSTRAINT dim_opt_state_data_pkey PRIMARY KEY (_key_opt_state_data);


--
-- Name: dim_poy_pkey; Type: CONSTRAINT; Schema: analytics; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_poy
    ADD CONSTRAINT dim_poy_pkey PRIMARY KEY (_key_poy);


--
-- Name: dim_school_pkey; Type: CONSTRAINT; Schema: analytics; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_school
    ADD CONSTRAINT dim_school_pkey PRIMARY KEY (_key_school);


--
-- Name: dim_student_pkey; Type: CONSTRAINT; Schema: analytics; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_student
    ADD CONSTRAINT dim_student_pkey PRIMARY KEY (_key_student);


--
-- Name: dim_test_pkey; Type: CONSTRAINT; Schema: analytics; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_test
    ADD CONSTRAINT dim_test_pkey PRIMARY KEY (_key_test);


--
-- Name: dim_unique_test_pkey; Type: CONSTRAINT; Schema: analytics; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_unique_test
    ADD CONSTRAINT dim_unique_test_pkey PRIMARY KEY (_key_unique_test);


--
-- Name: idx_dim_accomod; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX idx_dim_accomod ON dim_accomod USING hash (_hash_accomod);


--
-- Name: idx_dim_opt_state_data; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX idx_dim_opt_state_data ON dim_opt_state_data USING hash (_hash_opt_state_data);


--
-- Name: idx_dim_school; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX idx_dim_school ON dim_school USING hash (_hash_school);


--
-- Name: idx_dim_student; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX idx_dim_student ON dim_student USING hash (_hash_student);


--
-- Name: idx_dim_test; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX idx_dim_test ON dim_test USING hash (_hash_test);


--
-- Name: idx_fact_accomod; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX idx_fact_accomod ON fact_sum USING hash (_key_accomod);


--
-- Name: idx_fact_eoy_school; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX idx_fact_eoy_school ON fact_sum USING hash (_key_eoy_test_school);


--
-- Name: idx_fact_eoy_test; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX idx_fact_eoy_test ON fact_sum USING hash (_key_eoy_test);


--
-- Name: idx_fact_opt_state_data; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX idx_fact_opt_state_data ON fact_sum USING hash (_key_opt_state_data);


--
-- Name: idx_fact_pba_school; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX idx_fact_pba_school ON fact_sum USING hash (_key_pba_test_school);


--
-- Name: idx_fact_pba_test; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX idx_fact_pba_test ON fact_sum USING hash (_key_pba_test);


--
-- Name: idx_fact_poy; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX idx_fact_poy ON fact_sum USING hash (_key_poy);


--
-- Name: idx_fact_resp_school; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX idx_fact_resp_school ON fact_sum USING hash (_key_resp_school);


--
-- Name: idx_fact_student; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX idx_fact_student ON fact_sum USING hash (_key_student);


--
-- Name: idx_fact_unique_test; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX idx_fact_unique_test ON fact_sum USING hash (_key_unique_test);


--
-- Name: fk_fact_accomod; Type: FK CONSTRAINT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fk_fact_accomod FOREIGN KEY (_key_accomod) REFERENCES dim_accomod(_key_accomod);


--
-- Name: fk_fact_eoy_school; Type: FK CONSTRAINT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fk_fact_eoy_school FOREIGN KEY (_key_eoy_test_school) REFERENCES dim_school(_key_school);


--
-- Name: fk_fact_eoy_test; Type: FK CONSTRAINT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fk_fact_eoy_test FOREIGN KEY (_key_eoy_test) REFERENCES dim_test(_key_test);


--
-- Name: fk_fact_opt_state_data; Type: FK CONSTRAINT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fk_fact_opt_state_data FOREIGN KEY (_key_opt_state_data) REFERENCES dim_opt_state_data(_key_opt_state_data);


--
-- Name: fk_fact_pba_school; Type: FK CONSTRAINT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fk_fact_pba_school FOREIGN KEY (_key_pba_test_school) REFERENCES dim_school(_key_school);


--
-- Name: fk_fact_pba_test; Type: FK CONSTRAINT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fk_fact_pba_test FOREIGN KEY (_key_pba_test) REFERENCES dim_test(_key_test);


--
-- Name: fk_fact_poy; Type: FK CONSTRAINT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fk_fact_poy FOREIGN KEY (_key_poy) REFERENCES dim_poy(_key_poy);


--
-- Name: fk_fact_resp_school; Type: FK CONSTRAINT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fk_fact_resp_school FOREIGN KEY (_key_resp_school) REFERENCES dim_school(_key_school);


--
-- Name: fk_fact_student; Type: FK CONSTRAINT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fk_fact_student FOREIGN KEY (_key_student) REFERENCES dim_student(_key_student);


--
-- Name: fk_fact_unique_test; Type: FK CONSTRAINT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fk_fact_unique_test FOREIGN KEY (_key_unique_test) REFERENCES dim_unique_test(_key_unique_test);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

