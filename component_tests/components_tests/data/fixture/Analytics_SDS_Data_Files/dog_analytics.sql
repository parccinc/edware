--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: analytics; Type: SCHEMA; Schema: -; Owner: edware
--

CREATE SCHEMA analytics;


ALTER SCHEMA analytics OWNER TO edware;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = analytics, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: dim_accomod; Type: TABLE; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE TABLE dim_accomod (
    _key_accomod bigint NOT NULL,
    accomod_ell character(1),
    accomod_504 character(1),
    accomod_ind_ed character(1),
    accomod_freq_breacks character(1),
    accomod_alt_location character(1),
    accomod_small_group character(1),
    accomod_special_equip character(1),
    accomod_spec_area character(1),
    accomod_time_day character(1),
    accomod_answer_mask character(1),
    accomod_color_contrast character varying(19),
    accomod_asl_video character(1),
    accomod_screen_reader character(1),
    accomod_close_capt_ela character(1),
    accomod_braille_ela character(1),
    accomod_tactile_graph character(1),
    accomod_answer_rec character(1),
    accomod_braille_resp character varying(16),
    accomod_construct_resp_ela character varying(20),
    accomod_select_resp_ela character varying(20),
    accomod_monitor_test_resp character(1),
    accomod_word_predict character(1),
    accomod_native_lang character(1),
    accomod_loud_native_lang character varying(40),
    accomod_w_2_w_dict character(1),
    accomod_extend_time character varying(6),
    accomod_alt_paper_test character(1),
    accomod_human_read_sign character varying(14),
    accomod_large_print character(1),
    accomod_braille_tactile character(1),
    accomod_math_resp_el character varying(20),
    accomod_calculator character(1),
    accomod_math_trans_online character(3),
    accomod_paper_trans_math character(3),
    accomod_math_resp character varying(20),
    _valid_from date DEFAULT '1900-01-01'::date NOT NULL,
    _valid_to date DEFAULT '2199-12-31'::date NOT NULL,
    _record_version smallint DEFAULT 0 NOT NULL,
    accomod_text_2_speech_ela character(1),
    accomod_text_2_speech_math character(1),
    accomod_read_ela character varying(50),
    accomod_read_math character varying(50),
    _hash_accomod character varying(32)
);


ALTER TABLE analytics.dim_accomod OWNER TO edware;

--
-- Name: COLUMN dim_accomod.accomod_ell; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_ell IS 'Assessment Accommodation:  English learner (EL)';


--
-- Name: COLUMN dim_accomod.accomod_504; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_504 IS 'Assessment Accommodation: 504. 504 accommodations needed for a given assessment. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_ind_ed; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_ind_ed IS 'Assessment Accommodation: Individualized Educational Plan (IEP)';


--
-- Name: COLUMN dim_accomod.accomod_freq_breacks; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_freq_breacks IS 'Frequent Breaks. Y, Blank. Personal Needs Profile field. Student is allowed to take breaks, at their request, during the testing session. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_alt_location; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_alt_location IS 'Additional Breaks; Separate/Alternate Location';


--
-- Name: COLUMN dim_accomod.accomod_small_group; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_small_group IS 'Small Testing Group';


--
-- Name: COLUMN dim_accomod.accomod_special_equip; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_special_equip IS 'Specialized Equipment or Furniture';


--
-- Name: COLUMN dim_accomod.accomod_spec_area; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_spec_area IS 'Specified Area or Setting';


--
-- Name: COLUMN dim_accomod.accomod_time_day; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_time_day IS 'Time Of Day';


--
-- Name: COLUMN dim_accomod.accomod_answer_mask; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_answer_mask IS 'Answer Masking. Y, blank. Personal Needs Profile field. Specifies as part of an Assessment Personal Needs Profile the type of masks the user is able to create to cover portions of the question until needed. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_color_contrast; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_color_contrast IS 'Color Contrast. black-cream black-light blue black-light magenta white-black light blue-dark blue gray-green Color Overlay blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field. Defines as part of an Assessment Personal Needs Profile the access for preference to invert the foreground and background colors.';


--
-- Name: COLUMN dim_accomod.accomod_asl_video; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_asl_video IS 'ASL Video. y , blank. Personal Needs Profile field. American Sign Language content is provided to the student by a human signer through a video.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_screen_reader; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_screen_reader IS 'Screen Reader OR other Assistive Technology (AT) Application Y blank. Personal Needs Profile field. Screen Reader OR other Assistive Technology (AT) Application used to deliver online test form for ELA/L and Math. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_braille_ela; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_braille_ela IS 'Refreshable Braille Display for ELA/L. Y blank. Personal Needs Profile field. Student uses external device which converts the text from the Screen Reader into Braille.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_tactile_graph; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_tactile_graph IS 'Tactile Graphics. Y blank, Personal Needs Profile field. A tactile representation of the graphic, charts and images information is available outside of the computer testing system. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_answer_rec; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_answer_rec IS 'Answers Recorded in Test Book. Y blank. Personal Needs Profile field. The student records answers directly in the test book. Responses must be transcribed verbatim by a test administrator in a student answer book or answer sheet. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_braille_resp; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_braille_resp IS 'Braille Response. BrailleWriter, BrailleNotetaker, blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.A student who is blind or visually impaired and their response are captured by a Braille Writer or Note taker.';


--
-- Name: COLUMN dim_accomod.accomod_construct_resp_ela; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_construct_resp_ela IS 'ELA/L Constructed Response. SpeechToText HumanScribe HumanSigner ExternalATDevice blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer for Constructed Response item types';


--
-- Name: COLUMN dim_accomod.accomod_select_resp_ela; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_select_resp_ela IS 'ELA/L Selected Response or Technology Enhanced Items. SpeechToText, HumanScribe, HumanSigner, ExternalATDevice, blank.  Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer for Selected Response or Technology Enhanced items types.Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_monitor_test_resp; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_monitor_test_resp IS 'Monitor Test Response. Y blank. Personal Needs Profile field. The test administrator or assigned accommodator monitors proper placement of student responses on a test book/answer sheet. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_word_predict; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_word_predict IS 'Word Prediction. Y blank. Personal Needs Profile field. The student uses a word prediction external device that provides a bank of frequently -or recently -used words as a result of the student entering the first few letters of a word. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_native_lang; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_native_lang IS 'General Administration Directions Clarified in Student\92s Native Language (by test administrator)';


--
-- Name: COLUMN dim_accomod.accomod_loud_native_lang; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_loud_native_lang IS 'General Administration Directions Read Aloud and Repeated as Needed in Student\92s Native Language
(by test administrator)';


--
-- Name: COLUMN dim_accomod.accomod_w_2_w_dict; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_w_2_w_dict IS 'Word to Word Dictionary (English/Native Language). Y, blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.';


--
-- Name: COLUMN dim_accomod.accomod_extend_time; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_extend_time IS 'Extended Time';


--
-- Name: COLUMN dim_accomod.accomod_alt_paper_test; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_alt_paper_test IS 'Alternate Representation - Paper Test';


--
-- Name: COLUMN dim_accomod.accomod_human_read_sign; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_human_read_sign IS 'Human Reader or Human Signer. HumanSigner, HumanReadAloud ,blank. The paper test is read aloud or signed to the student by the test administrator (Human Reader). Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_calculator; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_calculator IS 'Calculation Device and Mathematics Tools.Y blank. Personal Needs Profile field. The student is allowed to use a calculator as an accommodation, including for items in test sections designated as non-calculator sections. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: COLUMN dim_accomod.accomod_math_trans_online; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_math_trans_online IS 'TranslationoftheMathematicsAssessmentOnline.SPA = Spanish, Blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.';


--
-- Name: COLUMN dim_accomod.accomod_paper_trans_math; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_paper_trans_math IS 'TranslationoftheMathematicsAssessmentOnline.SPA = Spanish, Blank. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank. Personal Needs Profile field.';


--
-- Name: COLUMN dim_accomod.accomod_math_resp; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_accomod.accomod_math_resp IS 'Mathematics Response. SpeechToText, HumanScribe, HumanSigner ,ExternalATDevice, blank. Personal Needs Profile field. A student''s response is captured by an external Speech to Text device, external AT device, Human Scribe or Signer. Rule - If either PBA or EOY has Y/true populated then report Y/true. If both EOY and PBA are blank, then use blank.';


--
-- Name: dim_school; Type: TABLE; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE TABLE dim_school (
    _key_school numeric(4,0) NOT NULL,
    dist_name character varying(60) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    district_id character varying(15) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    school_name character varying(60) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    school_id character varying(15) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    _valid_from date DEFAULT '1900-01-01'::date NOT NULL,
    _valid_to date DEFAULT '2199-12-31'::date NOT NULL,
    _record_version smallint NOT NULL,
    state_id character(3) DEFAULT 'UNK'::bpchar,
    school_year character varying(9),
    _hash_school character varying(32),
    institution_role character varying(4)
);


ALTER TABLE analytics.dim_school OWNER TO edware;

--
-- Name: COLUMN dim_school.dist_name; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_school.dist_name IS 'Name of Responsible district. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';


--
-- Name: COLUMN dim_school.district_id; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_school.district_id IS 'The district responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.  If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';


--
-- Name: COLUMN dim_school.school_name; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_school.school_name IS 'Responsible School/Institution Name - Name of Responsible school. If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';


--
-- Name: COLUMN dim_school.school_id; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_school.school_id IS 'The school responsible for specific educational services and/or instruction of the student. Also known as the Reporting organization.If this field is blank in PearsonAccessnext then it will be auto filled with EOY Testing District. If there is no EOY component then default to PBA.';


--
-- Name: dim_institution_school_key_seq; Type: SEQUENCE; Schema: analytics; Owner: edware
--

CREATE SEQUENCE dim_institution_school_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE analytics.dim_institution_school_key_seq OWNER TO edware;

--
-- Name: dim_institution_school_key_seq; Type: SEQUENCE OWNED BY; Schema: analytics; Owner: edware
--

ALTER SEQUENCE dim_institution_school_key_seq OWNED BY dim_school._key_school;


--
-- Name: dim_opt_state_data; Type: TABLE; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE TABLE dim_opt_state_data (
    _key_opt_state_data bigint NOT NULL,
    opt_state_data8 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data7 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data6 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data5 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data4 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data3 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data2 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    opt_state_data1 character varying(20) DEFAULT 'NA'::character varying NOT NULL,
    _valid_from date,
    _valid_to date,
    _record_version smallint NOT NULL,
    district_id character varying(25),
    _hash_opt_state_data character varying(32),
    state_id character(2)
);


ALTER TABLE analytics.dim_opt_state_data OWNER TO edware;

--
-- Name: dim_poy; Type: TABLE; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE TABLE dim_poy (
    _key_poy numeric(4,0) DEFAULT (-1) NOT NULL,
    poy character varying(20) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    end_year character varying(9) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    school_year character(9) DEFAULT 'UNKNOWN'::bpchar NOT NULL,
    _valid_from date DEFAULT '1900-01-01'::date NOT NULL,
    _valid_to date DEFAULT '2199-12-31'::date NOT NULL,
    _record_version smallint NOT NULL
);


ALTER TABLE analytics.dim_poy OWNER TO edware;

--
-- Name: COLUMN dim_poy._key_poy; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_poy._key_poy IS 'YEAR || 01 OR YEAR || 02';


--
-- Name: COLUMN dim_poy.poy; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_poy.poy IS 'Fall, Spring';


--
-- Name: COLUMN dim_poy.end_year; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_poy.end_year IS 'Academioc year end year,example 2014 for 2013-2014';


--
-- Name: dim_student; Type: TABLE; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE TABLE dim_student (
    _key_student numeric(7,0) NOT NULL,
    student_parcc_id character varying(40) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    student_sex character(1),
    ethnicity character varying(150) DEFAULT 'could not resolve'::character varying NOT NULL,
    ethn_hisp_latino character(1),
    ethn_indian_alaska character(1),
    ethn_asian character(1),
    ethn_black character(1),
    ethn_hawai character(1),
    ethn_white character(1),
    ethn_filler character(1),
    ethn_2_more character(1),
    ell character(1),
    lep_status character(1),
    gift_talent character(1),
    migrant_status character(1),
    econo_disadvantage character(1),
    disabil_student character(1),
    primary_disabil_type text,
    student_state_id character varying(40) DEFAULT 'UNKNOWN'::character varying,
    student_local_id character varying(40) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    student_name character varying(105) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    student_dob character varying(10) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    _record_version smallint NOT NULL,
    _valid_from timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    _valid_to timestamp without time zone DEFAULT '2199-12-31 23:59:59.999'::timestamp without time zone,
    student_first_name character varying(150),
    student_middle_name character varying(150),
    student_last_name character varying(150),
    staff_id character varying(150),
    district_id character varying(25),
    _hash_student character varying(32),
    state_id character(2),
    school_id character varying(15)
);


ALTER TABLE analytics.dim_student OWNER TO edware;

--
-- Name: COLUMN dim_student.student_parcc_id; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.student_parcc_id IS ' PARCC name:PARCC Student Identifier, An unique number or alphanumeric code assigned to a student by a school, school system, a state, or other agency or entity. This does not need to be the code associated with the student''s educational record.';


--
-- Name: COLUMN dim_student.ethn_hisp_latino; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_hisp_latino IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_indian_alaska; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_indian_alaska IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_asian; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_asian IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_black; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_black IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_hawai; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_hawai IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_white; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_white IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_filler; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_filler IS 'Parcc name:Hispanic/Latino Ethnicity. An indication that the person traces his or her origin or descent to Mexico, Puerto Rico, Cuba, Central and South America, and other Spanish cultures, regardless of race.
https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20765';


--
-- Name: COLUMN dim_student.ethn_2_more; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.ethn_2_more IS 'Parcc name: Demographic Race Two or More Races.A person having origins in any of more than one of the racial groups. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20425';


--
-- Name: COLUMN dim_student.ell; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.ell IS 'Parcc name:English Language Learner ELL. English Language Learner
 (ELL).';


--
-- Name: COLUMN dim_student.lep_status; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.lep_status IS 'Ampliofy name:LEPStatus, Parcc name:Limited English Proficient (LEP)   sed to indicate persons (A) who are ages 3 through 21; (B) who are enrolled or preparing to enroll in an elementary school or a secondary school; (C ) (who are I, ii, or iii) (i) who were not born in the United States or whose native languages are languages other than English; (ii) (who are I and II) (I) who are a Native American or Alaska Native, or a native resident of the outlying areas; and (II) who come from an environment where languages other than English have a significant impact on their level of language proficiency; or (iii) who are migratory, whose native languages are languages other than English, and who come from an environment where languages other than English are dominant; and (D) whose difficulties in speaking, reading, writing, or understanding the English language may be sufficient to deny the individuals (who are denied I or ii or iii) (i) the ability to meet the state''s proficient level of achievement on state assessments described in section 1111(b)(3); (ii) the ability to successfully achieve in classrooms where the language of instruction is English; or (iii) the opportunity to participate fully in society.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20783';


--
-- Name: COLUMN dim_student.gift_talent; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.gift_talent IS 'Gifted and Talented. Y,N,Blank. An indication that the student is participating in and served by a Gifted/Talented program. Rule: Test Administration Level Student Data: If either PBA or EOY has Y/true populated then Y/true must be reported on 01 - Summative Score Record.';


--
-- Name: COLUMN dim_student.migrant_status; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.migrant_status IS 'Parcc name:Migrant Status. Persons who are, or whose parents or spouses are, migratory agricultural workers, including migratory dairy workers, or migratory fishers, and who, in the preceding 36 months, in order to obtain, or accompany such parents or spouses, in order to obtain, temporary or seasonal employment in agricultural or fishing work (A) have moved from one LEA to another; (B) in a state that comprises a single LEA, have moved from one administrative area to another within such LEA; or (C) reside in an LEA of more than 15,000 square miles, and migrate a distance of 20 miles or more to a temporary residence to engage in a fishing activity. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20789';


--
-- Name: COLUMN dim_student.econo_disadvantage; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.econo_disadvantage IS 'Parcc field: Economic Disadvantage Status. An indication that the student met the State criteria for classification as having an economic disadvantage. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20741';


--
-- Name: COLUMN dim_student.disabil_student; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.disabil_student IS 'Parcc name:Student With Disability. An indication of whether a person is classified as disabled under the American''s with Disability Act (ADA).https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=3569\A0';


--
-- Name: COLUMN dim_student.student_state_id; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.student_state_id IS 'Parcc name: State Student Identifier. A State assigned student Identifier which is unique within that state. This identifier is used by states that do not wish to share student personal identifying information outside of state-deployed systems. Components sending data to Consortium-deployed systems would use this alternate identifier rather than the student''s SSID.';


--
-- Name: COLUMN dim_student.student_local_id; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.student_local_id IS 'Parcc name: LocalStudentIdentifier. ';


--
-- Name: COLUMN dim_student.student_name; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.student_name IS 'Last name||", "|| First name ||", "||Middle Name';


--
-- Name: COLUMN dim_student.student_dob; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_student.student_dob IS 'PARCC name:Birthdate, format:YYYY-MM-DD. The year, month and day on which a person was born.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=19995';


--
-- Name: dim_test; Type: TABLE; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE TABLE dim_test (
    _key_test numeric(4,0) NOT NULL,
    form_id character varying(14) DEFAULT 'NA'::character varying NOT NULL,
    test_category character varying(2) DEFAULT 'NA'::character varying NOT NULL,
    test_subject character varying(35) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    asmt_grade character varying(12) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    test_code character varying(20) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    _valid_from date DEFAULT '1900-01-01'::date NOT NULL,
    _valid_to date DEFAULT '2199-12-31'::date NOT NULL,
    _record_version smallint NOT NULL,
    pba_form_id character varying(50),
    eoy_form_id character varying(50),
    pba_category character(1),
    eoy_category character(1),
    district_id character varying(25),
    _hash_test character varying(32),
    state_id character(2)
);


ALTER TABLE analytics.dim_test OWNER TO edware;

--
-- Name: TABLE dim_test; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON TABLE dim_test IS 'removed course, course_key fields';


--
-- Name: COLUMN dim_test._key_test; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_test._key_test IS 'NA for summative different for different test_codes';


--
-- Name: COLUMN dim_test.form_id; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_test.form_id IS 'PBA Form ID or  EOY Form ID.  form student tested, NA for Summative';


--
-- Name: COLUMN dim_test.test_category; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_test.test_category IS 'A =  Test Attemptedness flag is blank for both PBA and EOY components test attempts;B = Test attempts in both PBA and EOY components are Y for Voided PBA/EOY Score Code field ;C =  Test attempts in either PBA and EOY ;omponents are Y for Voided PBA/EOY Score Code field and there are no test assignements or test attempts in the other component;D = Test attempts in either PBA and EOY components have the Test Attemptedness flag as blank  and there are no test assignements or test attempts in the other component;E =  Test Assignments exist in both  PBA and EOY components  ;F =  Test Assignment exist in one component (either PBA or EOY) and the other Component has a test attempt that did not meet attemptedness rules ;G =  Test Assignment exist in one component (either PBA or EOY) and the other component has a test attempt that has a Y for Voided PBA/EOY Score Code field ; H =  Test Assignment exist in one component (either PBA or EOY) and no test assignment or test attempt is present in the other component ';


--
-- Name: COLUMN dim_test.test_subject; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_test.test_subject IS 'PARCC name: AssessmentAcademicSubject. scription of the academic content or subject area being evaluated. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21356';


--
-- Name: COLUMN dim_test.asmt_grade; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN dim_test.asmt_grade IS '2 - 02 = Second grade;3 - 03 = Third grade;04 = Fourth grade;05 = Fifth grade; 06 = Sixth grade; 07 = Seventh grade; 08 = Eighth grade; 09 = Ninth grade; 10 = Tenth grade; 11 = Eleventh grade';


--
-- Name: dim_unique_test; Type: TABLE; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE TABLE dim_unique_test (
    _key_unique_test numeric NOT NULL,
    test_code character varying(20),
    test_subject character varying(35),
    asmt_grade character varying(12),
    _valid_from date,
    _valid_to date,
    _record_version smallint
);


ALTER TABLE analytics.dim_unique_test OWNER TO edware;

--
-- Name: fact_sum; Type: TABLE; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE TABLE fact_sum (
    rec_id bigint NOT NULL,
    _key_accomod bigint NOT NULL,
    _key_opt_state_data bigint NOT NULL,
    _key_poy numeric(4,0) DEFAULT (-1) NOT NULL,
    _key_resp_school integer NOT NULL,
    _key_student integer NOT NULL,
    student_grade character varying(12) NOT NULL,
    sum_scale_score integer,
    sum_csem integer,
    sum_perf_lvl smallint,
    sum_read_scale_score integer,
    sum_read_csem integer,
    sum_write_scale_score integer,
    sum_write_csem integer,
    subclaim1_category smallint,
    subclaim2_category smallint,
    subclaim3_category smallint,
    subclaim4_category smallint,
    subclaim5_category smallint,
    subclaim6_category smallint,
    state_growth_percent numeric(5,2),
    district_growth_percent numeric(5,2),
    parcc_growth_percent numeric(5,2),
    multirecord_flag character(2) DEFAULT (-1) NOT NULL,
    result_type character varying(10) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    record_type character varying(20) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    reported_score_flag character(1) DEFAULT 'UNKNOWN'::bpchar NOT NULL,
    report_suppression_code character(2),
    report_suppression_action smallint,
    reported_roster_flag character varying(2) DEFAULT 'NA'::character varying NOT NULL,
    include_in_parcc character varying(5) DEFAULT false NOT NULL,
    include_in_state character varying(5) DEFAULT false NOT NULL,
    include_in_district character varying(5) DEFAULT false NOT NULL,
    include_in_school character varying(5) DEFAULT false NOT NULL,
    include_in_roster character varying(5) DEFAULT 'N'::bpchar NOT NULL,
    create_date date DEFAULT ('now'::text)::date NOT NULL,
    me_flag character varying(8),
    form_category character varying(2),
    pba_test_uuid character varying(36),
    eoy_test_uuid character varying(36),
    sum_score_rec_uuid character varying(36),
    _key_eoy_test_school integer,
    _key_pba_test_school integer,
    pba_total_items integer,
    eoy_total_items integer,
    pba_attempt_flag character(1),
    eoy_attempt_flag character(1),
    pba_total_items_attempt integer,
    eoy_total_items_attempt integer,
    pba_total_items_unit1 integer,
    eoy_total_items_unit1 integer,
    pba_total_items_unit2 integer,
    eoy_total_items_unit2 integer,
    pba_total_items_unit3 integer,
    eoy_total_items_unit3 integer,
    pba_total_items_unit4 integer,
    eoy_total_items_unit4 integer,
    pba_total_items_unit5 integer,
    eoy_total_items_unit5 integer,
    pba_unit1_items_attempt integer,
    eoy_unit1_items_attempt integer,
    pba_unit2_items_attempt integer,
    eoy_unit2_items_attempt integer,
    pba_unit3_items_attempt integer,
    eoy_unit3_items_attempt integer,
    pba_unit4_items_attempt integer,
    eoy_unit4_items_attempt integer,
    pba_unit5_items_attempt integer,
    eoy_unit5_items_attempt integer,
    pba_not_tested_reason character varying(150),
    eoy_not_tested_reason character varying(150),
    pba_void_reason character varying(150),
    eoy_void_reason character varying(150),
    _key_pba_test integer,
    _key_eoy_test integer,
    _key_unique_test integer,
    student_parcc_id character varying(40),
    district_id character varying(25),
    state_id character(2),
    school_id character varying(15)
);


ALTER TABLE analytics.fact_sum OWNER TO edware;

--
-- Name: COLUMN fact_sum._key_poy; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum._key_poy IS 'YEAR || 01 OR YEAR || 02';


--
-- Name: COLUMN fact_sum.student_grade; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.student_grade IS 'The typical grade or combination of grade-levels, developmental levels, or age-levels for which an assessment is designed. (multiple selections supported) . KG, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, PS.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21362';


--
-- Name: COLUMN fact_sum.sum_scale_score; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_scale_score IS 'PARCC name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score';


--
-- Name: COLUMN fact_sum.sum_csem; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_csem IS 'Summative CSEM';


--
-- Name: COLUMN fact_sum.sum_perf_lvl; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_perf_lvl IS 'Summative Performance Level';


--
-- Name: COLUMN fact_sum.sum_read_scale_score; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_read_scale_score IS 'Summative Reading Scale Score';


--
-- Name: COLUMN fact_sum.sum_read_csem; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_read_csem IS 'Summative Reading CSEM';


--
-- Name: COLUMN fact_sum.sum_write_scale_score; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_write_scale_score IS 'PARCC name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score';


--
-- Name: COLUMN fact_sum.sum_write_csem; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_write_csem IS 'Summative Writing CSEM';


--
-- Name: COLUMN fact_sum.subclaim1_category; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim1_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- Name: COLUMN fact_sum.subclaim2_category; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim2_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- Name: COLUMN fact_sum.subclaim3_category; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim3_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- Name: COLUMN fact_sum.subclaim4_category; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim4_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- Name: COLUMN fact_sum.subclaim5_category; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim5_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- Name: COLUMN fact_sum.subclaim6_category; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim6_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- Name: COLUMN fact_sum.state_growth_percent; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.state_growth_percent IS 'Student Growth Percentile Compared to State. Blank yr 1 (2014-2015) Yr 2+, student level summative and state level performance from previous year will be extracted from the data warehouse, compared to current year and student growth calculation will be applied Always Blank in PearsonAccessnext - Field placeholder for data warehouse';


--
-- Name: COLUMN fact_sum.district_growth_percent; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.district_growth_percent IS 'Student Growth Percentile Compared to District';


--
-- Name: COLUMN fact_sum.parcc_growth_percent; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.parcc_growth_percent IS 'Student Growth Percentile Compared to PARCC';


--
-- Name: COLUMN fact_sum.multirecord_flag; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.multirecord_flag IS '1 - Y, 0 - N, -1 = blank';


--
-- Name: COLUMN fact_sum.result_type; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.result_type IS 'Summative, PBA, EOY';


--
-- Name: COLUMN fact_sum.record_type; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.record_type IS ' Summative Score, Single Component, No Component';


--
-- Name: COLUMN fact_sum.reported_score_flag; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.reported_score_flag IS 'Y,N,balnk';


--
-- Name: COLUMN fact_sum.reported_roster_flag; Type: COMMENT; Schema: analytics; Owner: edware
--

COMMENT ON COLUMN fact_sum.reported_roster_flag IS 'Y, N, NA';


--
-- Name: fact_sum_resp_school_key_seq; Type: SEQUENCE; Schema: analytics; Owner: edware
--

CREATE SEQUENCE fact_sum_resp_school_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE analytics.fact_sum_resp_school_key_seq OWNER TO edware;

--
-- Name: fact_sum_resp_school_key_seq; Type: SEQUENCE OWNED BY; Schema: analytics; Owner: edware
--

ALTER SEQUENCE fact_sum_resp_school_key_seq OWNED BY fact_sum._key_resp_school;


--
-- Name: _key_resp_school; Type: DEFAULT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY fact_sum ALTER COLUMN _key_resp_school SET DEFAULT nextval('fact_sum_resp_school_key_seq'::regclass);


--
-- Data for Name: dim_accomod; Type: TABLE DATA; Schema: analytics; Owner: edware
--

COPY dim_accomod (_key_accomod, accomod_ell, accomod_504, accomod_ind_ed, accomod_freq_breacks, accomod_alt_location, accomod_small_group, accomod_special_equip, accomod_spec_area, accomod_time_day, accomod_answer_mask, accomod_color_contrast, accomod_asl_video, accomod_screen_reader, accomod_close_capt_ela, accomod_braille_ela, accomod_tactile_graph, accomod_answer_rec, accomod_braille_resp, accomod_construct_resp_ela, accomod_select_resp_ela, accomod_monitor_test_resp, accomod_word_predict, accomod_native_lang, accomod_loud_native_lang, accomod_w_2_w_dict, accomod_extend_time, accomod_alt_paper_test, accomod_human_read_sign, accomod_large_print, accomod_braille_tactile, accomod_math_resp_el, accomod_calculator, accomod_math_trans_online, accomod_paper_trans_math, accomod_math_resp, _valid_from, _valid_to, _record_version, accomod_text_2_speech_ela, accomod_text_2_speech_math, accomod_read_ela, accomod_read_math, _hash_accomod) FROM stdin;
0	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	N  	N	2015-11-02	2015-11-02	1	N	N	N	N	\N
1	N	N	N	N	N	N	Y	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
2	N	N	N	N	N	N	N	N	N	N	N	Y	Y	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
3	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	SelectedResponse1	Y	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	HumanSigner	N	\N
4	N	Y	N	N	N	N	Y	N	N	N	N	Y	N	N	Y	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
5	N	Y	Y	N	Y	N	N	N	N	N	N	Y	N	Y	N	N	N	N	N	SelectedResponse1	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
6	N	Y	Y	N	N	Y	N	N	N	N	N	Y	Y	N	Y	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	Spa	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
7	N	Y	Y	N	N	N	Y	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
8	N	Y	Y	N	N	N	N	N	N	N	N	Y	N	N	Y	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
9	N	Y	Y	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
10	Y	N	N	N	Y	N	N	Y	N	N	N	Y	Y	Y	N	N	N	N	oralresponse	SelectedResponse1	N	N	N	N	N	N	N	N	N	Y	\N	Y	Spa	\N	N	1900-01-01	2199-12-31	1	N	N	N	HumanReadAloud	\N
11	Y	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	HumanSigner	N	\N
12	Y	N	N	N	N	Y	N	N	N	N	N	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	HumanReadAloud	N	\N
13	Y	N	N	N	N	Y	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	Spa	\N	N	1900-01-01	2199-12-31	1	N	N	HumanSigner	N	\N
14	Y	N	N	N	N	N	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
15	Y	N	Y	Y	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
16	Y	N	Y	N	N	N	Y	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
17	Y	Y	N	Y	N	N	N	N	N	N	N	Y	N	N	Y	N	N	N	N	N	N	N	Y	N	N	N	Y	N	N	N	\N	N	Spa	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
18	Y	Y	N	N	Y	N	N	N	N	N	N	N	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
19	Y	Y	Y	N	N	N	Y	N	Y	N	N	Y	Y	N	Y	N	N	N	oralresponse	N	N	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
20	Y	Y	Y	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	SelectedResponse1	N	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
21	N	N	N	N	N	N	Y	N	Y	N	N	Y	N	N	Y	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	Y	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	Y	N	N	N	\N
22	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	Y	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
23	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
24	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	Y	Y	N	Y	N	N	N	N	Y	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
25	N	N	Y	N	N	Y	N	N	N	N	N	Y	Y	Y	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	HumanReadAloud	HumanReadAloud	\N
26	N	N	Y	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	Y	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
27	N	N	Y	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
28	N	Y	N	Y	N	Y	Y	N	N	N	N	N	Y	N	N	N	N	N	N	N	Y	N	N	N	N	Y	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
29	N	Y	N	N	N	Y	N	N	N	N	N	N	Y	Y	N	N	Y	BrailleNotetaker	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	Y	N	N	HumanReadAloud	\N
30	N	Y	N	N	N	N	Y	N	N	N	N	Y	N	N	N	N	N	N	oralresponse	N	N	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
31	N	Y	N	N	N	N	Y	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
32	N	Y	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
33	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	BrailleNotetaker	N	N	N	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	HumanReadAloud	\N
34	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	HumanSigner	\N
35	N	Y	Y	N	N	N	Y	N	N	N	N	Y	Y	N	Y	N	N	N	N	N	N	N	Y	OralScriptReadbyTestAdministratorARA	N	Y	N	N	N	Y	\N	Y	N  	\N	SpeechToText	1900-01-01	2199-12-31	1	Y	N	HumanReadAloud	N	\N
36	N	Y	Y	N	N	N	N	N	N	N	N	Y	Y	Y	N	N	Y	BrailleWriter	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
37	Y	Y	N	Y	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
38	Y	Y	N	N	N	N	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
39	Y	Y	Y	N	N	N	Y	N	N	N	N	Y	Y	N	N	N	N	N	N	SelectedResponse1	N	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
40	Y	Y	Y	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
41	N	N	N	N	Y	N	Y	N	N	Y	N	Y	N	N	N	N	N	BrailleNotetaker	N	N	N	N	Y	N	Y	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	HumanSigner	N	\N
42	N	N	N	N	N	N	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	Spa	\N	SpeechToText	1900-01-01	2199-12-31	1	N	N	N	N	\N
43	N	N	N	N	N	N	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	Spa	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
44	N	N	N	N	N	N	Y	N	N	N	Y	N	Y	N	N	N	Y	N	N	N	N	N	Y	N	N	N	N	N	N	Y	\N	Y	N  	\N	SpeechToText	1900-01-01	2199-12-31	1	N	N	N	N	\N
45	N	N	N	N	N	N	Y	N	N	N	N	Y	Y	N	N	N	N	N	N	N	Y	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
46	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	oralresponse	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
47	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	Y	N	HumanSigner	HumanSigner	\N
48	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	BrailleWriter	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
49	N	N	Y	N	Y	N	Y	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	Y	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	Y	Y	N	HumanReadAloud	\N
50	N	N	Y	N	N	Y	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
51	N	N	Y	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
52	N	N	Y	N	N	N	N	N	N	N	N	N	N	Y	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	Y	N	N	N	\N
53	N	N	Y	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
54	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	oralresponse	N	N	N	Y	N	N	Y	N	N	N	N	\N	Y	N  	\N	SpeechToText	1900-01-01	2199-12-31	1	N	N	N	N	\N
55	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	Y	N	N	N	N	\N	N	N  	\N	SpeechToText	1900-01-01	2199-12-31	1	N	N	N	N	\N
56	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	OralScriptReadbyTestAdministratorARA	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
57	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
58	N	Y	N	Y	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	Spa	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
59	N	Y	N	N	Y	N	Y	N	N	Y	N	N	Y	N	N	N	Y	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	HumanReadAloud	HumanSigner	\N
60	N	Y	N	N	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
61	N	Y	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	Spa	\N	N	1900-01-01	2199-12-31	1	N	Y	N	N	\N
62	N	Y	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
63	N	Y	N	N	N	N	Y	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	Y	OralScriptReadbyTestAdministratorARA	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
64	N	Y	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
65	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
66	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
67	N	Y	Y	N	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	Spa	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
68	N	Y	Y	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
69	N	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
70	N	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
71	Y	N	N	N	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	Spa	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
72	Y	N	N	N	N	Y	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
73	Y	N	N	N	N	N	Y	N	N	N	Y	N	Y	Y	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	Y	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
74	Y	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
75	Y	N	Y	N	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	Spa	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
76	Y	N	Y	N	N	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	Y	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
77	Y	N	Y	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	SelectedResponse1	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
78	Y	N	Y	N	N	N	N	Y	Y	N	N	N	Y	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
79	Y	N	Y	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	SelectedResponse1	N	N	Y	N	N	Y	N	N	N	N	\N	N	N  	\N	SpeechToText	1900-01-01	2199-12-31	1	N	N	N	N	\N
80	Y	N	Y	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
81	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	HumanSigner	N	\N
82	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	oralresponse	N	N	N	Y	N	N	Y	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
83	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	OralScriptReadbyTestAdministratorARA	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
84	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
85	Y	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
86	Y	Y	N	Y	N	N	Y	N	N	N	N	N	N	N	Y	N	Y	N	N	N	N	N	Y	N	N	Y	N	N	Y	Y	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	HumanSigner	\N
87	Y	Y	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	N  	\N	SpeechToText	1900-01-01	2199-12-31	1	N	N	N	N	\N
88	Y	Y	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	OralScriptReadbyTestAdministratorARA	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
89	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
90	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
91	Y	Y	Y	N	N	N	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	Y	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
92	Y	Y	Y	N	N	N	Y	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	N	N	OralScriptReadbyTestAdministratorARA	N	N	N	N	N	N	\N	Y	Spa	\N	SpeechToText	1900-01-01	2199-12-31	1	N	N	N	N	\N
93	Y	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	\N	N	Spa	\N	N	1900-01-01	2199-12-31	1	Y	N	N	N	\N
94	Y	Y	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	N	Y	N	N	N	N	N	N	N	\N	N	N  	\N	N	1900-01-01	2199-12-31	1	N	N	N	N	\N
\.


--
-- Name: dim_institution_school_key_seq; Type: SEQUENCE SET; Schema: analytics; Owner: edware
--

SELECT pg_catalog.setval('dim_institution_school_key_seq', 1, false);


--
-- Data for Name: dim_opt_state_data; Type: TABLE DATA; Schema: analytics; Owner: edware
--

COPY dim_opt_state_data (_key_opt_state_data, opt_state_data8, opt_state_data7, opt_state_data6, opt_state_data5, opt_state_data4, opt_state_data3, opt_state_data2, opt_state_data1, _valid_from, _valid_to, _record_version, district_id, _hash_opt_state_data, state_id) FROM stdin;
0	NA	NA	NA	NA	NA	NA	NA	NA	\N	\N	1	\N	\N	\N
1	qx	qx	qx	qx	qx	qx	qx	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
2	WKrYyOXjojbf0o8kl	WKrYyOXjojbf0o8kl	WKrYyOXjojbf0o8kl	WKrYyOXjojbf0o8kl	WKrYyOXjojbf0o8kl	WKrYyOXjojbf0o8kl	WKrYyOXjojbf0o8kl	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
3	JPLuCUHgPC7B	JPLuCUHgPC7B	JPLuCUHgPC7B	JPLuCUHgPC7B	JPLuCUHgPC7B	JPLuCUHgPC7B	JPLuCUHgPC7B	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
4	Z4cKN	Z4cKN	Z4cKN	Z4cKN	Z4cKN	Z4cKN	Z4cKN	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
5	S6i	S6i	S6i	S6i	S6i	S6i	S6i	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
6	pp14le3Zr1B7pxKKGdM	pp14le3Zr1B7pxKKGdM	pp14le3Zr1B7pxKKGdM	pp14le3Zr1B7pxKKGdM	pp14le3Zr1B7pxKKGdM	pp14le3Zr1B7pxKKGdM	pp14le3Zr1B7pxKKGdM	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
7	hGaOD5arW64LWlk2	hGaOD5arW64LWlk2	hGaOD5arW64LWlk2	hGaOD5arW64LWlk2	hGaOD5arW64LWlk2	hGaOD5arW64LWlk2	hGaOD5arW64LWlk2	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
8	jg	jg	jg	jg	jg	jg	jg	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
9	gelByZdArm	gelByZdArm	gelByZdArm	gelByZdArm	gelByZdArm	gelByZdArm	gelByZdArm	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
10	idlDCzYl45cO7	idlDCzYl45cO7	idlDCzYl45cO7	idlDCzYl45cO7	idlDCzYl45cO7	idlDCzYl45cO7	idlDCzYl45cO7	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
11	9opCz4520wIrA5pt	9opCz4520wIrA5pt	9opCz4520wIrA5pt	9opCz4520wIrA5pt	9opCz4520wIrA5pt	9opCz4520wIrA5pt	9opCz4520wIrA5pt	UNKNOWN	1900-01-01	2199-12-31	1	R0002	\N	\N
12	rtURxgiDqHd5TD	rtURxgiDqHd5TD	rtURxgiDqHd5TD	rtURxgiDqHd5TD	rtURxgiDqHd5TD	rtURxgiDqHd5TD	rtURxgiDqHd5TD	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
13	GPnC0vRqe9lMILMaE	GPnC0vRqe9lMILMaE	GPnC0vRqe9lMILMaE	GPnC0vRqe9lMILMaE	GPnC0vRqe9lMILMaE	GPnC0vRqe9lMILMaE	GPnC0vRqe9lMILMaE	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
14	MWzV6dFs4TRRo	MWzV6dFs4TRRo	MWzV6dFs4TRRo	MWzV6dFs4TRRo	MWzV6dFs4TRRo	MWzV6dFs4TRRo	MWzV6dFs4TRRo	UNKNOWN	1900-01-01	2199-12-31	1	R0002	\N	\N
15	aKzeeht9E	aKzeeht9E	aKzeeht9E	aKzeeht9E	aKzeeht9E	aKzeeht9E	aKzeeht9E	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
16	MxOvcIRQv	MxOvcIRQv	MxOvcIRQv	MxOvcIRQv	MxOvcIRQv	MxOvcIRQv	MxOvcIRQv	UNKNOWN	1900-01-01	2199-12-31	1	R0002	\N	\N
17	2R48ezkfuLr21YkRr	2R48ezkfuLr21YkRr	2R48ezkfuLr21YkRr	2R48ezkfuLr21YkRr	2R48ezkfuLr21YkRr	2R48ezkfuLr21YkRr	2R48ezkfuLr21YkRr	UNKNOWN	1900-01-01	2199-12-31	1	R0002	\N	\N
18	Nd8aL6OZHpTzAK	Nd8aL6OZHpTzAK	Nd8aL6OZHpTzAK	Nd8aL6OZHpTzAK	Nd8aL6OZHpTzAK	Nd8aL6OZHpTzAK	Nd8aL6OZHpTzAK	UNKNOWN	1900-01-01	2199-12-31	1	R0002	\N	\N
19	0HiUbZx60MdcK036GyKn	0HiUbZx60MdcK036GyKn	0HiUbZx60MdcK036GyKn	0HiUbZx60MdcK036GyKn	0HiUbZx60MdcK036GyKn	0HiUbZx60MdcK036GyKn	0HiUbZx60MdcK036GyKn	UNKNOWN	1900-01-01	2199-12-31	1	R0002	\N	\N
20	GL7fec1XzKzib	GL7fec1XzKzib	GL7fec1XzKzib	GL7fec1XzKzib	GL7fec1XzKzib	GL7fec1XzKzib	GL7fec1XzKzib	UNKNOWN	1900-01-01	2199-12-31	1	R0001	\N	\N
21	LnWxz	LnWxz	LnWxz	LnWxz	LnWxz	LnWxz	LnWxz	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
22	Dak7I0au0HGetJ8ldU9	Dak7I0au0HGetJ8ldU9	Dak7I0au0HGetJ8ldU9	Dak7I0au0HGetJ8ldU9	Dak7I0au0HGetJ8ldU9	Dak7I0au0HGetJ8ldU9	Dak7I0au0HGetJ8ldU9	UNKNOWN	1900-01-01	2199-12-31	1	R0002	\N	\N
23	ZUbaT2XarRNdc71	ZUbaT2XarRNdc71	ZUbaT2XarRNdc71	ZUbaT2XarRNdc71	ZUbaT2XarRNdc71	ZUbaT2XarRNdc71	ZUbaT2XarRNdc71	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
24	5VeHNBZEifdweJB	5VeHNBZEifdweJB	5VeHNBZEifdweJB	5VeHNBZEifdweJB	5VeHNBZEifdweJB	5VeHNBZEifdweJB	5VeHNBZEifdweJB	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
25	3jjXcxXj9	3jjXcxXj9	3jjXcxXj9	3jjXcxXj9	3jjXcxXj9	3jjXcxXj9	3jjXcxXj9	UNKNOWN	1900-01-01	2199-12-31	1	R0001	\N	\N
26	VGQWB6vNRZh4xhV4VF	VGQWB6vNRZh4xhV4VF	VGQWB6vNRZh4xhV4VF	VGQWB6vNRZh4xhV4VF	VGQWB6vNRZh4xhV4VF	VGQWB6vNRZh4xhV4VF	VGQWB6vNRZh4xhV4VF	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
27	eQTOG9VhE3	eQTOG9VhE3	eQTOG9VhE3	eQTOG9VhE3	eQTOG9VhE3	eQTOG9VhE3	eQTOG9VhE3	UNKNOWN	1900-01-01	2199-12-31	1	R0001	\N	\N
28	Rx3hHFwPUfma3ln	Rx3hHFwPUfma3ln	Rx3hHFwPUfma3ln	Rx3hHFwPUfma3ln	Rx3hHFwPUfma3ln	Rx3hHFwPUfma3ln	Rx3hHFwPUfma3ln	UNKNOWN	1900-01-01	2199-12-31	1	R0002	\N	\N
29	E8Ecxsy2JttginOv	E8Ecxsy2JttginOv	E8Ecxsy2JttginOv	E8Ecxsy2JttginOv	E8Ecxsy2JttginOv	E8Ecxsy2JttginOv	E8Ecxsy2JttginOv	UNKNOWN	1900-01-01	2199-12-31	1	R0001	\N	\N
30	zfCkJ5pIBilv	zfCkJ5pIBilv	zfCkJ5pIBilv	zfCkJ5pIBilv	zfCkJ5pIBilv	zfCkJ5pIBilv	zfCkJ5pIBilv	UNKNOWN	1900-01-01	2199-12-31	1	R0001	\N	\N
31	EFK3uCDr	EFK3uCDr	EFK3uCDr	EFK3uCDr	EFK3uCDr	EFK3uCDr	EFK3uCDr	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
32	91a5	91a5	91a5	91a5	91a5	91a5	91a5	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
33	37g	37g	37g	37g	37g	37g	37g	UNKNOWN	1900-01-01	2199-12-31	1	R0001	\N	\N
34	Le	Le	Le	Le	Le	Le	Le	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
35	N	N	N	N	N	N	N	UNKNOWN	1900-01-01	2199-12-31	1	R0002	\N	\N
36	aHF7OVhLeElfq	aHF7OVhLeElfq	aHF7OVhLeElfq	aHF7OVhLeElfq	aHF7OVhLeElfq	aHF7OVhLeElfq	aHF7OVhLeElfq	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
37	0qj4Q7MIVmMeP	0qj4Q7MIVmMeP	0qj4Q7MIVmMeP	0qj4Q7MIVmMeP	0qj4Q7MIVmMeP	0qj4Q7MIVmMeP	0qj4Q7MIVmMeP	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
38	YKNI8ZHfGL	YKNI8ZHfGL	YKNI8ZHfGL	YKNI8ZHfGL	YKNI8ZHfGL	YKNI8ZHfGL	YKNI8ZHfGL	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
39	BdqzqT0LhEzTiIdRbVw	BdqzqT0LhEzTiIdRbVw	BdqzqT0LhEzTiIdRbVw	BdqzqT0LhEzTiIdRbVw	BdqzqT0LhEzTiIdRbVw	BdqzqT0LhEzTiIdRbVw	BdqzqT0LhEzTiIdRbVw	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
40	AofVP2mMC41Yt1	AofVP2mMC41Yt1	AofVP2mMC41Yt1	AofVP2mMC41Yt1	AofVP2mMC41Yt1	AofVP2mMC41Yt1	AofVP2mMC41Yt1	UNKNOWN	1900-01-01	2199-12-31	1	R0001	\N	\N
41	FzI	FzI	FzI	FzI	FzI	FzI	FzI	UNKNOWN	1900-01-01	2199-12-31	1	R0001	\N	\N
42	v2j	v2j	v2j	v2j	v2j	v2j	v2j	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
43	ZZdBBLmmXhKr2Z	ZZdBBLmmXhKr2Z	ZZdBBLmmXhKr2Z	ZZdBBLmmXhKr2Z	ZZdBBLmmXhKr2Z	ZZdBBLmmXhKr2Z	ZZdBBLmmXhKr2Z	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
44	spwemE1dqM	spwemE1dqM	spwemE1dqM	spwemE1dqM	spwemE1dqM	spwemE1dqM	spwemE1dqM	UNKNOWN	1900-01-01	2199-12-31	1	R0002	\N	\N
45	qAoIfWmsm1U4VL	qAoIfWmsm1U4VL	qAoIfWmsm1U4VL	qAoIfWmsm1U4VL	qAoIfWmsm1U4VL	qAoIfWmsm1U4VL	qAoIfWmsm1U4VL	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
46	RltLCEWj8P9nwO	RltLCEWj8P9nwO	RltLCEWj8P9nwO	RltLCEWj8P9nwO	RltLCEWj8P9nwO	RltLCEWj8P9nwO	RltLCEWj8P9nwO	UNKNOWN	1900-01-01	2199-12-31	1	R0002	\N	\N
47	gv7WsQJmtXpiAVwhd	gv7WsQJmtXpiAVwhd	gv7WsQJmtXpiAVwhd	gv7WsQJmtXpiAVwhd	gv7WsQJmtXpiAVwhd	gv7WsQJmtXpiAVwhd	gv7WsQJmtXpiAVwhd	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
48	SamCuvOAmoCIEUh	SamCuvOAmoCIEUh	SamCuvOAmoCIEUh	SamCuvOAmoCIEUh	SamCuvOAmoCIEUh	SamCuvOAmoCIEUh	SamCuvOAmoCIEUh	UNKNOWN	1900-01-01	2199-12-31	1	R0002	\N	\N
49	NY0nKXcg2ZoH6GfGM	NY0nKXcg2ZoH6GfGM	NY0nKXcg2ZoH6GfGM	NY0nKXcg2ZoH6GfGM	NY0nKXcg2ZoH6GfGM	NY0nKXcg2ZoH6GfGM	NY0nKXcg2ZoH6GfGM	UNKNOWN	1900-01-01	2199-12-31	1	R0001	\N	\N
50	YxmBdJ387mTd	YxmBdJ387mTd	YxmBdJ387mTd	YxmBdJ387mTd	YxmBdJ387mTd	YxmBdJ387mTd	YxmBdJ387mTd	UNKNOWN	1900-01-01	2199-12-31	1	R0001	\N	\N
51	mJuy6a3GcGHbwR	mJuy6a3GcGHbwR	mJuy6a3GcGHbwR	mJuy6a3GcGHbwR	mJuy6a3GcGHbwR	mJuy6a3GcGHbwR	mJuy6a3GcGHbwR	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
52	Wd03DTPLXjY4	Wd03DTPLXjY4	Wd03DTPLXjY4	Wd03DTPLXjY4	Wd03DTPLXjY4	Wd03DTPLXjY4	Wd03DTPLXjY4	UNKNOWN	1900-01-01	2199-12-31	1	R0001	\N	\N
53	SAmjSHZtr0CeCRXFz	SAmjSHZtr0CeCRXFz	SAmjSHZtr0CeCRXFz	SAmjSHZtr0CeCRXFz	SAmjSHZtr0CeCRXFz	SAmjSHZtr0CeCRXFz	SAmjSHZtr0CeCRXFz	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
54	a0AURmkAHWweqOwiOH	a0AURmkAHWweqOwiOH	a0AURmkAHWweqOwiOH	a0AURmkAHWweqOwiOH	a0AURmkAHWweqOwiOH	a0AURmkAHWweqOwiOH	a0AURmkAHWweqOwiOH	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
55	5SaXfXdCL4zarJCqnY3	5SaXfXdCL4zarJCqnY3	5SaXfXdCL4zarJCqnY3	5SaXfXdCL4zarJCqnY3	5SaXfXdCL4zarJCqnY3	5SaXfXdCL4zarJCqnY3	5SaXfXdCL4zarJCqnY3	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
56	kYw0	kYw0	kYw0	kYw0	kYw0	kYw0	kYw0	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
57	yIBbjZTSidZf	yIBbjZTSidZf	yIBbjZTSidZf	yIBbjZTSidZf	yIBbjZTSidZf	yIBbjZTSidZf	yIBbjZTSidZf	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
58	vEcx9C9wPnkP8BRD	vEcx9C9wPnkP8BRD	vEcx9C9wPnkP8BRD	vEcx9C9wPnkP8BRD	vEcx9C9wPnkP8BRD	vEcx9C9wPnkP8BRD	vEcx9C9wPnkP8BRD	UNKNOWN	1900-01-01	2199-12-31	1	R0002	\N	\N
59	vHmP2m0wkmCluGkz	vHmP2m0wkmCluGkz	vHmP2m0wkmCluGkz	vHmP2m0wkmCluGkz	vHmP2m0wkmCluGkz	vHmP2m0wkmCluGkz	vHmP2m0wkmCluGkz	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
60	hvLjm	hvLjm	hvLjm	hvLjm	hvLjm	hvLjm	hvLjm	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
61	dxQLUs	dxQLUs	dxQLUs	dxQLUs	dxQLUs	dxQLUs	dxQLUs	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
62	sDW3tntyGwrqMVwTL7Yr	sDW3tntyGwrqMVwTL7Yr	sDW3tntyGwrqMVwTL7Yr	sDW3tntyGwrqMVwTL7Yr	sDW3tntyGwrqMVwTL7Yr	sDW3tntyGwrqMVwTL7Yr	sDW3tntyGwrqMVwTL7Yr	UNKNOWN	1900-01-01	2199-12-31	1	R0001	\N	\N
63	PemxLX8i5cOCkNKhO	PemxLX8i5cOCkNKhO	PemxLX8i5cOCkNKhO	PemxLX8i5cOCkNKhO	PemxLX8i5cOCkNKhO	PemxLX8i5cOCkNKhO	PemxLX8i5cOCkNKhO	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
64	vtnA2m	vtnA2m	vtnA2m	vtnA2m	vtnA2m	vtnA2m	vtnA2m	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
65	XDqx0NRSSlODiFCNLIPO	XDqx0NRSSlODiFCNLIPO	XDqx0NRSSlODiFCNLIPO	XDqx0NRSSlODiFCNLIPO	XDqx0NRSSlODiFCNLIPO	XDqx0NRSSlODiFCNLIPO	XDqx0NRSSlODiFCNLIPO	UNKNOWN	1900-01-01	2199-12-31	1	R0002	\N	\N
66	f8xX7D9KWP6ArNd8	f8xX7D9KWP6ArNd8	f8xX7D9KWP6ArNd8	f8xX7D9KWP6ArNd8	f8xX7D9KWP6ArNd8	f8xX7D9KWP6ArNd8	f8xX7D9KWP6ArNd8	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
67	cUHfZBYYqD4kDSMdO	cUHfZBYYqD4kDSMdO	cUHfZBYYqD4kDSMdO	cUHfZBYYqD4kDSMdO	cUHfZBYYqD4kDSMdO	cUHfZBYYqD4kDSMdO	cUHfZBYYqD4kDSMdO	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
68	8zyJiQcSCrTg5H	8zyJiQcSCrTg5H	8zyJiQcSCrTg5H	8zyJiQcSCrTg5H	8zyJiQcSCrTg5H	8zyJiQcSCrTg5H	8zyJiQcSCrTg5H	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
69	xVGmYF	xVGmYF	xVGmYF	xVGmYF	xVGmYF	xVGmYF	xVGmYF	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
70	KM08fO	KM08fO	KM08fO	KM08fO	KM08fO	KM08fO	KM08fO	UNKNOWN	1900-01-01	2199-12-31	1	R0001	\N	\N
71	D6OsMGC3sUXby0e60	D6OsMGC3sUXby0e60	D6OsMGC3sUXby0e60	D6OsMGC3sUXby0e60	D6OsMGC3sUXby0e60	D6OsMGC3sUXby0e60	D6OsMGC3sUXby0e60	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
72	5dj9tnUtD0BaIWR	5dj9tnUtD0BaIWR	5dj9tnUtD0BaIWR	5dj9tnUtD0BaIWR	5dj9tnUtD0BaIWR	5dj9tnUtD0BaIWR	5dj9tnUtD0BaIWR	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
73	WdC3qlWLnLqd3	WdC3qlWLnLqd3	WdC3qlWLnLqd3	WdC3qlWLnLqd3	WdC3qlWLnLqd3	WdC3qlWLnLqd3	WdC3qlWLnLqd3	UNKNOWN	1900-01-01	2199-12-31	1	R0001	\N	\N
74	b	b	b	b	b	b	b	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
75	PBX1kyfKLuDPY9	PBX1kyfKLuDPY9	PBX1kyfKLuDPY9	PBX1kyfKLuDPY9	PBX1kyfKLuDPY9	PBX1kyfKLuDPY9	PBX1kyfKLuDPY9	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
76	LNjtpdOtesj1c4amNTk	LNjtpdOtesj1c4amNTk	LNjtpdOtesj1c4amNTk	LNjtpdOtesj1c4amNTk	LNjtpdOtesj1c4amNTk	LNjtpdOtesj1c4amNTk	LNjtpdOtesj1c4amNTk	UNKNOWN	1900-01-01	2199-12-31	1	R0002	\N	\N
77	ABHIjAtBdXJYnOVKx	ABHIjAtBdXJYnOVKx	ABHIjAtBdXJYnOVKx	ABHIjAtBdXJYnOVKx	ABHIjAtBdXJYnOVKx	ABHIjAtBdXJYnOVKx	ABHIjAtBdXJYnOVKx	UNKNOWN	1900-01-01	2199-12-31	1	R0001	\N	\N
78	9mGeugc3KNKzBWDQoe	9mGeugc3KNKzBWDQoe	9mGeugc3KNKzBWDQoe	9mGeugc3KNKzBWDQoe	9mGeugc3KNKzBWDQoe	9mGeugc3KNKzBWDQoe	9mGeugc3KNKzBWDQoe	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
79	dyDjumLSDXXCb0W	dyDjumLSDXXCb0W	dyDjumLSDXXCb0W	dyDjumLSDXXCb0W	dyDjumLSDXXCb0W	dyDjumLSDXXCb0W	dyDjumLSDXXCb0W	UNKNOWN	1900-01-01	2199-12-31	1	R0001	\N	\N
80	nKmBv46Ei8mejG	nKmBv46Ei8mejG	nKmBv46Ei8mejG	nKmBv46Ei8mejG	nKmBv46Ei8mejG	nKmBv46Ei8mejG	nKmBv46Ei8mejG	UNKNOWN	1900-01-01	2199-12-31	1	R0002	\N	\N
81	te3e	te3e	te3e	te3e	te3e	te3e	te3e	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
82	NADBOT1uvz5R	NADBOT1uvz5R	NADBOT1uvz5R	NADBOT1uvz5R	NADBOT1uvz5R	NADBOT1uvz5R	NADBOT1uvz5R	UNKNOWN	1900-01-01	2199-12-31	1	R0001	\N	\N
83	zzCOsCpPTKTDl6b	zzCOsCpPTKTDl6b	zzCOsCpPTKTDl6b	zzCOsCpPTKTDl6b	zzCOsCpPTKTDl6b	zzCOsCpPTKTDl6b	zzCOsCpPTKTDl6b	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
84	w3Cme6NvN7UxsU4	w3Cme6NvN7UxsU4	w3Cme6NvN7UxsU4	w3Cme6NvN7UxsU4	w3Cme6NvN7UxsU4	w3Cme6NvN7UxsU4	w3Cme6NvN7UxsU4	UNKNOWN	1900-01-01	2199-12-31	1	R0001	\N	\N
85	W	W	W	W	W	W	W	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
86	bEcar4ZudgE7	bEcar4ZudgE7	bEcar4ZudgE7	bEcar4ZudgE7	bEcar4ZudgE7	bEcar4ZudgE7	bEcar4ZudgE7	UNKNOWN	1900-01-01	2199-12-31	1	R0002	\N	\N
87	v3eq4j2WKz3Xj	v3eq4j2WKz3Xj	v3eq4j2WKz3Xj	v3eq4j2WKz3Xj	v3eq4j2WKz3Xj	v3eq4j2WKz3Xj	v3eq4j2WKz3Xj	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
88	IWu1IFLDr1GBos5kOyz0	IWu1IFLDr1GBos5kOyz0	IWu1IFLDr1GBos5kOyz0	IWu1IFLDr1GBos5kOyz0	IWu1IFLDr1GBos5kOyz0	IWu1IFLDr1GBos5kOyz0	IWu1IFLDr1GBos5kOyz0	UNKNOWN	1900-01-01	2199-12-31	1	R0002	\N	\N
89	B80ISk2pkFJDa0	B80ISk2pkFJDa0	B80ISk2pkFJDa0	B80ISk2pkFJDa0	B80ISk2pkFJDa0	B80ISk2pkFJDa0	B80ISk2pkFJDa0	UNKNOWN	1900-01-01	2199-12-31	1	R0002	\N	\N
90	1UbnSd2QJMmJ0Y7J	1UbnSd2QJMmJ0Y7J	1UbnSd2QJMmJ0Y7J	1UbnSd2QJMmJ0Y7J	1UbnSd2QJMmJ0Y7J	1UbnSd2QJMmJ0Y7J	1UbnSd2QJMmJ0Y7J	UNKNOWN	1900-01-01	2199-12-31	1	R0002	\N	\N
91	9CdyHvSWO2C2uNrDmP	9CdyHvSWO2C2uNrDmP	9CdyHvSWO2C2uNrDmP	9CdyHvSWO2C2uNrDmP	9CdyHvSWO2C2uNrDmP	9CdyHvSWO2C2uNrDmP	9CdyHvSWO2C2uNrDmP	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
92	wHtmUDzScbdN7QHw	wHtmUDzScbdN7QHw	wHtmUDzScbdN7QHw	wHtmUDzScbdN7QHw	wHtmUDzScbdN7QHw	wHtmUDzScbdN7QHw	wHtmUDzScbdN7QHw	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
93	837	837	837	837	837	837	837	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
94	LdOLqMh0IDHixZZ	LdOLqMh0IDHixZZ	LdOLqMh0IDHixZZ	LdOLqMh0IDHixZZ	LdOLqMh0IDHixZZ	LdOLqMh0IDHixZZ	LdOLqMh0IDHixZZ	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
95	LrOZusXVqM8	LrOZusXVqM8	LrOZusXVqM8	LrOZusXVqM8	LrOZusXVqM8	LrOZusXVqM8	LrOZusXVqM8	UNKNOWN	1900-01-01	2199-12-31	1	R0001	\N	\N
96	J6XVOOZ7YZCeO	J6XVOOZ7YZCeO	J6XVOOZ7YZCeO	J6XVOOZ7YZCeO	J6XVOOZ7YZCeO	J6XVOOZ7YZCeO	J6XVOOZ7YZCeO	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
97	1gqQM8R2ELM1W	1gqQM8R2ELM1W	1gqQM8R2ELM1W	1gqQM8R2ELM1W	1gqQM8R2ELM1W	1gqQM8R2ELM1W	1gqQM8R2ELM1W	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
98	Kn1dcUynMwWGO6yw	Kn1dcUynMwWGO6yw	Kn1dcUynMwWGO6yw	Kn1dcUynMwWGO6yw	Kn1dcUynMwWGO6yw	Kn1dcUynMwWGO6yw	Kn1dcUynMwWGO6yw	UNKNOWN	1900-01-01	2199-12-31	1	R0003	\N	\N
99	lrYyyzuoIeeUodL	lrYyyzuoIeeUodL	lrYyyzuoIeeUodL	lrYyyzuoIeeUodL	lrYyyzuoIeeUodL	lrYyyzuoIeeUodL	lrYyyzuoIeeUodL	UNKNOWN	1900-01-01	2199-12-31	1	R0001	\N	\N
\.


--
-- Data for Name: dim_poy; Type: TABLE DATA; Schema: analytics; Owner: edware
--

COPY dim_poy (_key_poy, poy, end_year, school_year, _valid_from, _valid_to, _record_version) FROM stdin;
0	UNKNOWN	UNKNOWN	UNKNOWN  	1900-01-01	2199-12-31	1
1	Spring	2013-2014	2013-2014	1900-01-01	2199-12-31	1
2	Spring	2014-2015	2014-2015	1900-01-01	2199-12-31	1
\.


--
-- Data for Name: dim_school; Type: TABLE DATA; Schema: analytics; Owner: edware
--

COPY dim_school (_key_school, dist_name, district_id, school_name, school_id, _valid_from, _valid_to, _record_version, state_id, school_year, _hash_school, institution_role) FROM stdin;
0	NA	NA	NA	NA	1900-01-01	2199-12-31	1	UNK	\N	\N	\N
1	NA	NA	NA	NA	1900-01-01	2199-12-31	1	RI 	2013-2014	\N	\N
3	NA	NA	NA	NA	1900-01-01	2199-12-31	1	RI 	2014-2015	\N	\N
2	Providence	R0003	Hope High School	RP003	1900-01-01	2199-12-31	1	RI 	2013-2014	\N	\N
4	Newport	R0001	Rogers High School	RN001	1900-01-01	2199-12-31	1	RI 	2014-2015	\N	\N
5	East Greenwich	R0002	East Greenwich High School	RE001	1900-01-01	2199-12-31	1	RI 	2014-2015	\N	\N
6	Providence	R0003	Alvarez High School	RP001	1900-01-01	2199-12-31	1	RI 	2014-2015	\N	\N
7	Providence	R0003	Mount Pleasant High School	RP002	1900-01-01	2199-12-31	1	RI 	2014-2015	\N	\N
8	Providence	R0003	Hope High School	RP003	1900-01-01	2199-12-31	1	RI 	2014-2015	\N	\N
\.


--
-- Data for Name: dim_student; Type: TABLE DATA; Schema: analytics; Owner: edware
--

COPY dim_student (_key_student, student_parcc_id, student_sex, ethnicity, ethn_hisp_latino, ethn_indian_alaska, ethn_asian, ethn_black, ethn_hawai, ethn_white, ethn_filler, ethn_2_more, ell, lep_status, gift_talent, migrant_status, econo_disadvantage, disabil_student, primary_disabil_type, student_state_id, student_local_id, student_name, student_dob, _record_version, _valid_from, _valid_to, student_first_name, student_middle_name, student_last_name, staff_id, district_id, _hash_student, state_id, school_id) FROM stdin;
0	NA	\N	could not resolve	N	N	N	N	N	N	Y	N	\N	\N	\N	\N	\N	\N	\N	UNKNOWN	UNKNOWN	UNKNOWN	UNKOWN	1	1900-01-01 00:00:00	2199-12-31 23:59:59.999	\N	\N	\N	\N	\N	\N	\N	\N
10	ZmrZFBiOmI2H2QuEsZm59MMu7bIAhKHUjge92f6E	M	White	N	N	N	N	N	Y	N	N	Y	Y	Y	N	N	Y	AUT	swmmMy3yINRax8X2lZgSXb49caxsdhJrC5SGGO4L	UbYZjnIcBk3y9qIpq7QvOusuqcc7Iw0s6S0z4281	Matsuura, Arnette Sandy	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:09.937508	Arnette	Sandy	Matsuura	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
11	2vM9yk86Q3OXKckaeaUjcSTHf1TaO5LCT2DHaeU7	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	N	Y	N	N	N	Y	ID	Du1Sh4DTO3nLwT357QZPIzYJJfJX2FLFRMUI2hHC	itBQNt3L5ZuR734wODV8UCZOkAImL1QQv3TEbsOZ	Neveu, Betty Cornelius	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:13.895692	Betty	Cornelius	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
12	dxpojnotJFaxas2YDJMiJoBPHkVCIc4lptuj24Dy	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	N	N	Y	Y	Y	N	SLI	LX9Rwl1Eg9LP92kzBpe5idYStEwlkffCQQY929Su	j8JpKBopcMtBNZwz3JGyhplE018kxwJscvq2xACn	Matsuura, Thurman Kathe	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:13.895692	Thurman	Kathe	Matsuura	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
24	GuxIDl2Fjyjs81hW6n9gmufC4ruRxwcV8DCuKqbi	F	Native Hawaiian or other Pacific Islander	N	N	N	N	Y	N	N	N	Y	N	Y	Y	Y	Y	OI	li2nUdkotubCxOOEsm1b2Av0kZdGmPlcLWtH0vea	0UHiuCA5N0TcBPrjTDu2BiBF5MzwGq8XL4DOOqAf	Pane, Debby Zofia	0009-02-05	2	2015-09-15 07:55:58.174923	2199-12-31 23:59:59.999	Debby	Zofia	Pane	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
1	GuxIDl2Fjyjs81hW6n9gmufC4ruRxwcV8DCuKqbi	F	Native Hawaiian or other Pacific Islander	N	N	N	N	Y	N	N	N	Y	N	Y	Y	Y	Y	OI	li2nUdkotubCxOOEsm1b2Av0kZdGmPlcLWtH0vea	0UHiuCA5N0TcBPrjTDu2BiBF5MzwGq8XL4DOOqAf	Pane, Debby Zofia	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:55:58.174923	Debby	Zofia	Pane	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
26	k1kUAyTitX2QYeasKtOPdOxbPCSIet5I00G6vQON	F	White	N	N	N	N	N	Y	N	N	N	Y	Y	N	N	N	SLD	t8ivqpZI9goOjdx2lezvRQdwlupLushf7wh9ofib	s8R7rPtONccd6m7XMWFxyQzoMTa1UqvZoScl6wLI	Ung, Verena Vincent	0009-02-05	2	2015-09-15 07:55:58.174923	2199-12-31 23:59:59.999	Verena	Vincent	Ung	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
17	1SWd4Oky5wnWUNuH078iTM1reO6FjlIYmYkVHcQw	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	N	Y	N	Y	Y	HI	MnQn4XYlZppmKAIik2GbO6qo7UUtxyJT8EAQvFod	Kqm3TmmLkcC9V8uszQcM50BuMlfquTl5a9TW363O	Mantle, Darci Roman	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:02.050598	Darci	Roman	Mantle	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
46	maHNc5BQaVj9ddANVpBvFluBikLI3H06gqH3P1zi	F	Black or African American	N	N	N	Y	N	N	N	N	Y	Y	Y	Y	N	Y	SLI	NuR72lVSYLmdau6elNP0mugSv2GnuJVHM6MKDPGb	WapHLkZfW6VL8yTizh6k8TM6YWe4SImeCUW5u4Rz	Carriere, Vincent Kimberely	0002-09-05	2	2015-09-15 07:56:02.050598	2199-12-31 23:59:59.999	Vincent	Kimberely	Carriere	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
28	maHNc5BQaVj9ddANVpBvFluBikLI3H06gqH3P1zi	F	Black or African American	N	N	N	Y	N	N	N	N	Y	Y	Y	Y	N	Y	SLI	NuR72lVSYLmdau6elNP0mugSv2GnuJVHM6MKDPGb	WapHLkZfW6VL8yTizh6k8TM6YWe4SImeCUW5u4Rz	Carriere, Vincent Kimberely	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:02.050598	Vincent	Kimberely	Carriere	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
73	Cnt3veDS2HW94jTAnU4bUASdLgIbP9y0lvQpQqer	M	Black or African American	N	N	N	Y	N	N	N	N	N	Y	Y	Y	Y	N	SLD	XddKt8voU4Ws8Q3RPZNSJsf8ZMPzzEeI8OM1pxZt	WIpn1enuYlVIe53JejMJqSJSCeKWxc36LGWyZekf	Mcgowin, Cornelius Joella	0009-02-05	2	2015-09-15 07:56:09.937508	2199-12-31 23:59:59.999	Cornelius	Joella	Mcgowin	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
43	Cnt3veDS2HW94jTAnU4bUASdLgIbP9y0lvQpQqer	M	Black or African American	N	N	N	Y	N	N	N	N	N	Y	Y	Y	Y	N	SLD	XddKt8voU4Ws8Q3RPZNSJsf8ZMPzzEeI8OM1pxZt	WIpn1enuYlVIe53JejMJqSJSCeKWxc36LGWyZekf	Mcgowin, Cornelius Joella	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:09.937508	Cornelius	Joella	Mcgowin	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
75	E9fHez5Rp8SfdyyhzYgAlyLyIE1JYmpYL8d9Up4W	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	Y	N	Y	N	N	ID	zR4uPNZtt7KQSSb1OtijkI8Z3lDaRazHsiO03wGY	OUjRHF0b6p41lL5GwuVzXyXQpk2yqduCxsp3eZ3P	Gillett, Debby Senaida	0009-02-05	2	2015-09-15 07:56:09.937508	2199-12-31 23:59:59.999	Debby	Senaida	Gillett	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
7	Sc0cLMiRhBfkJsHF7Dwz1ozIPzc1k3s6aOgAD2IT	F	Black or African American	N	N	N	Y	N	N	N	N	Y	N	Y	Y	Y	N	SLI	vHUc6IqR2eA7oqn479WZO8P0v6bm0qzaqNYBXqro	UYuxGmofxNxWDnuXTWFNS4rviLEy7CTfdApVoVzU	Monks, Joella Erasmo	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:09.937508	Joella	Erasmo	Monks	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
81	sEorUakkZr2QwQgeQ1wCHxo8NwkksVvRlPXQV7cL	F	Asian	N	N	Y	N	N	N	N	N	Y	N	Y	Y	Y	Y	SLD	ajzDzGnYVOFZLxW64MxyWOCwj5ic8JqCYaeSSXQC	sOAfqeN7P0mdsSSlvrhX8tGJTyhKVtWE6ulKuGV3	Cartlidge, Joella Cornelius	0009-02-05	2	2015-09-15 07:56:09.937508	2199-12-31 23:59:59.999	Joella	Cornelius	Cartlidge	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
8	sEorUakkZr2QwQgeQ1wCHxo8NwkksVvRlPXQV7cL	F	Asian	N	N	Y	N	N	N	N	N	Y	N	Y	Y	Y	Y	SLD	ajzDzGnYVOFZLxW64MxyWOCwj5ic8JqCYaeSSXQC	sOAfqeN7P0mdsSSlvrhX8tGJTyhKVtWE6ulKuGV3	Cartlidge, Joella Cornelius	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:09.937508	Joella	Cornelius	Cartlidge	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
82	U49Ivq4Y5D2NTB0EAuHv0sKhVA90oVkCwOgJErDW	M	Asian	N	N	Y	N	N	N	N	N	N	Y	N	Y	Y	Y	SLD	upsehex48pDKkcKQZC0mj8VAI6H7uFKlfpnxQRhz	Aax8ihQ82nR88np1G1SxRB7uYY74BtDGIJkW40Us	Mcgowin, Betty Vincent	0009-02-05	2	2015-09-15 07:56:09.937508	2199-12-31 23:59:59.999	Betty	Vincent	Mcgowin	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
48	U49Ivq4Y5D2NTB0EAuHv0sKhVA90oVkCwOgJErDW	M	Asian	N	N	Y	N	N	N	N	N	N	Y	N	Y	Y	Y	SLD	upsehex48pDKkcKQZC0mj8VAI6H7uFKlfpnxQRhz	Aax8ihQ82nR88np1G1SxRB7uYY74BtDGIJkW40Us	Mcgowin, Betty Vincent	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:09.937508	Betty	Vincent	Mcgowin	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
100	Ew6FoKuAKYas7HNCC2qpWTLldXJFPKY6WuKGaoCK	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	N	N	N	Y	Y	Y	SLD	SxOSlOn6gZI9a31bOijbKvqGnk1VQQSi8ri9Pjom	lLMJd89DcDvbKY8VejtkyU53eyuH2iT3Tded2q1D	Borkowski, Loni Shane	0002-09-05	2	2015-09-15 07:56:13.895692	2199-12-31 23:59:59.999	Loni	Shane	Borkowski	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
101	mqZIVP2jMSn1VHrJ6nLaEHLblTiXW3eCC0BZDA6d	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	N	Y	Y	N	N	OHI	qtw4nQ44m7Si2kuCZjwq8PmEkxQcPDMvZIjrzTdM	4jeLurTXTIAGXc9kY5uuhOAbSIbYp4nxfzlhOw3g	Borkowski, Devon Jacquline	0002-09-05	2	2015-09-15 07:56:13.895692	2199-12-31 23:59:59.999	Devon	Jacquline	Borkowski	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
103	uB6VLZuP4JnB0aZW3u9Ol9pUS9ux9VdAuAaQ19fX	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	N	Y	Y	N	Y	N	OHI	tVFAA1vX9gS5Phorf9YtS7oMWjLtIAH367W3757O	uZrTDiEVUCAffMQoGo3TwqCzna3YuvdSdTAq8lwW	Borkowski, Becki Lois	0002-09-05	2	2015-09-15 07:56:13.895692	2199-12-31 23:59:59.999	Becki	Lois	Borkowski	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
70	uB6VLZuP4JnB0aZW3u9Ol9pUS9ux9VdAuAaQ19fX	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	N	Y	Y	N	Y	N	OHI	tVFAA1vX9gS5Phorf9YtS7oMWjLtIAH367W3757O	uZrTDiEVUCAffMQoGo3TwqCzna3YuvdSdTAq8lwW	Borkowski, Becki Lois	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:13.895692	Becki	Lois	Borkowski	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
121	JRAWX24sIH3Bh2KcmpYNn6ZyGr8e6p6ctkAMFRw5	F	Asian	N	N	Y	N	N	N	N	N	Y	Y	Y	Y	N	N	MD	VxchSUMcdsVWHvFbAU1dUZQHymEGClKRHOucFsCD	UusU7BtgH5knogqWxkGqKMssirKkeuWZIcfN9ljv	Gatti, Shane Senaida	0009-02-05	2	2015-09-15 07:56:21.782214	2199-12-31 23:59:59.999	Shane	Senaida	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
123	LaPd7vVIU9G9NvGr44WMWgwNwvupwIyWtC5kGetF	M	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	Y	Y	Y	Y	N	SLD	IH43WH7R7yvt5qtr3W3Y66qKyaxQkPM7vfaNyiCO	Ac1iIcJEDgScVsfJn7e61o19LhuxU15Rt8jSY6OT	Troy, Verena Winona	0009-02-05	2	2015-09-15 07:56:21.782214	2199-12-31 23:59:59.999	Verena	Winona	Troy	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
140	oGYUvFLatZjgEBWy3LV9NwMdmc9GT1GTerNuABS9	M	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	N	N	N	N	Y	TBI	QgAwuCsMxGtAVrDNYsagNJWvjWFZQ7kIa9R7h3Sj	02i9bpKnZsr2LP5mQ5qCaReYMb8pzxH0JPS6bMW1	Lawlor, Kimberely Joella	0002-09-05	2	2015-09-15 07:56:25.615271	2199-12-31 23:59:59.999	Kimberely	Joella	Lawlor	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
111	oGYUvFLatZjgEBWy3LV9NwMdmc9GT1GTerNuABS9	M	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	N	N	N	N	Y	TBI	QgAwuCsMxGtAVrDNYsagNJWvjWFZQ7kIa9R7h3Sj	02i9bpKnZsr2LP5mQ5qCaReYMb8pzxH0JPS6bMW1	Lawlor, Kimberely Joella	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:25.615271	Kimberely	Joella	Lawlor	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
141	OimZrYW80W7xl7QUa4Hg9Q9knjdvys1Z8wRIVHuG	F	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	N	Y	N	N	N	N	ID	LVDJ5pmKkbouYqlTqLGFL37ZoQe3oh4zisvYXKNF	sIqnzxAVbEnEyQ5EhHNskz5gSJZ2zjnzHfZ6GCZ0	Cabello, Chase Jacquline	0002-09-05	2	2015-09-15 07:56:25.615271	2199-12-31 23:59:59.999	Chase	Jacquline	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
156	DSoDwPOg0kZQ5YPnMJ0SLmBaCeGes4a1qjkRUQfd	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	Y	Y	Y	N	Y	EMN	C4vKy987HmZkDVVIhnTBMCuQbXCqW1Qgq6aj6t1n	wdF0ClC7Z8qvAnYH090pWLDDC1mhQSg1ZqEtV3hW	Pando, Joella Zofia	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Joella	Zofia	Pando	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
159	GxZdYAWx8b0sf9CbdWYT8HsXYqBnNikEkDMikPkd	M	Native Hawaiian or other Pacific Islander	N	N	N	N	Y	N	N	N	N	N	Y	Y	N	Y	MD	ZZRDwuqMsOWqhqTccuae5rg6ShNIc0OxClU9SJAN	8iThk59GvZdrOTkGLfgjkpY67nUJ9u9qPIUSJmRC	Cabello, Keshia Barbar	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Keshia	Barbar	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
160	iI34kG6uH1mQ83tbh5PEVVKoQjV1r96laDb4x10n	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	N	Y	Y	N	Y	EMN	76PpHVVXrz4XmEJJCUkNRBPoHtBigoSOtVVTx2Z1	LOoSbNRoiqF7cmOGjcR9Z3KYZRaDkqRx7ygMsygZ	Buckwalter, Autumn Kathe	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Autumn	Kathe	Buckwalter	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
4	loKBQQ7EnGEJKKoK39nYLCNWtoJIDC2yNW9cPcmh	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	N	Y	N	Y	N	SLI	x1Gi3Q1cKlhWvsthoxJSHWSnAsFkXOiW8VJivQ6e	LAkGZnolk1llJQO3UvrPZ8Ze4YveAohGbYjb5edQ	Pedone, Debby Betty	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:21.782214	Debby	Betty	Pedone	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
16	0YrEK4Y3PihDRZiZnFMinOcTdkNYwublx9Z8lCAp	F	Black or African American	N	N	N	Y	N	N	N	N	N	N	Y	N	N	Y	OI	cneCAQyANr172ywJd3n3MAl1Z3fVHQetNeVQeNn3	111XTD0Ya5bojkRrCgASmcHwlx51DERXxbKWRgld	Mcgowin, Marisela Marisela	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:25.615271	Marisela	Marisela	Mcgowin	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
21	4xNcr39DqN64ORBLFyIG322oypneHyCBYVXKZZPm	M	White	N	N	N	N	N	Y	N	N	Y	N	N	N	N	N	ID	FEF5lXrReycYbCcr51NDoKDpjMlcswSMK3sBPH82	3Vdt1dUAmoNAKaHVOgELSeBJKEEyAabfHzYv7c78	Benevides, Shane Thurman	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Shane	Thurman	Benevides	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
42	1SWd4Oky5wnWUNuH078iTM1reO6FjlIYmYkVHcQw	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	N	Y	N	Y	Y	HI	MnQn4XYlZppmKAIik2GbO6qo7UUtxyJT8EAQvFod	Kqm3TmmLkcC9V8uszQcM50BuMlfquTl5a9TW363O	Mantle, Darci Roman	0002-09-05	2	2015-09-15 07:56:02.050598	2199-12-31 23:59:59.999	Darci	Roman	Mantle	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
38	6DRp6GlmW7bu4tn1PhckfVhL9MHH85oXIaSfRJep	M	Native Hawaiian or other Pacific Islander	N	N	N	N	Y	N	N	N	Y	Y	Y	Y	Y	N	SLI	n1Yqb9qw1NhnALHkEpjpkyqea3XGgt6UxI0mqNDp	uZhrK9csJYrSv4OHgyLwMmaLWRMpeuVirwLiR6yi	Flavors, Arnette Thurman	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:21.782214	Arnette	Thurman	Flavors	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
18	Ew6FoKuAKYas7HNCC2qpWTLldXJFPKY6WuKGaoCK	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	N	N	N	Y	Y	Y	SLD	SxOSlOn6gZI9a31bOijbKvqGnk1VQQSi8ri9Pjom	lLMJd89DcDvbKY8VejtkyU53eyuH2iT3Tded2q1D	Borkowski, Loni Shane	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:13.895692	Loni	Shane	Borkowski	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
147	uJkYBTpWyQhQPsBC4PFyGIFqcoyKQiHxv4f3IYsu	F	Asian	N	N	Y	N	N	N	N	N	N	Y	N	N	Y	N	MD	LWBhsVv7yWdjNlIGtlUQzsPY1qVaygSUKSDHTKDW	ZVeK3Mo3PCiRKga5acczo6phYZQKkFjkBTUH1cTi	Neveu, Chase Keshia	0002-09-05	2	2015-09-15 07:56:25.615271	2199-12-31 23:59:59.999	Chase	Keshia	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
105	DuncASxHMXlhtve8RRDsuUQ83LaR8gIZ7rFEP0zs	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Pando, Joella Marisela	0002-09-05	1	1900-01-01 00:00:00	2199-12-31 23:59:59.999	Joella	Marisela	Pando	5f3bcce2cac34dd38578b9d423801dc3b5caf6	R0003	\N	\N	\N
106	6DRp6GlmW7bu4tn1PhckfVhL9MHH85oXIaSfRJep	M	Native Hawaiian or other Pacific Islander	N	N	N	N	Y	N	N	N	Y	Y	Y	Y	Y	N	SLI	n1Yqb9qw1NhnALHkEpjpkyqea3XGgt6UxI0mqNDp	uZhrK9csJYrSv4OHgyLwMmaLWRMpeuVirwLiR6yi	Flavors, Arnette Thurman	0009-02-05	2	2015-09-15 07:56:21.782214	2199-12-31 23:59:59.999	Arnette	Thurman	Flavors	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
30	OjeIJyOY5Qyogd8ZPoAT4Lj6E1LmudgZcIrEeAvb	M	Two or more races	N	N	N	N	N	N	N	Y	N	N	Y	N	N	Y	DB	jdcbqSVD8gl67mTC7aGcW7JT0dPnD1lNZTzyxvQL	Upb3hmWN4WMQI5ZYWOGI72NmBc4e8vTYV4aVmnHp	Gatti, Roman Christin	0009-02-05	2	2015-09-15 07:55:58.174923	2199-12-31 23:59:59.999	Roman	Christin	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
5	OjeIJyOY5Qyogd8ZPoAT4Lj6E1LmudgZcIrEeAvb	M	Two or more races	N	N	N	N	N	N	N	Y	N	N	Y	N	N	Y	DB	jdcbqSVD8gl67mTC7aGcW7JT0dPnD1lNZTzyxvQL	Upb3hmWN4WMQI5ZYWOGI72NmBc4e8vTYV4aVmnHp	Gatti, Roman Christin	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:55:58.174923	Roman	Christin	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
32	sYbJYuJUBNDgRcwDUOr3LhFnvsw424uSYhmE0rx7	M	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	N	Y	N	N	Y	EMN	dqp9Tw8rMvkHeZpVEkNuZJlmwtPBZM0tsrjCSGSw	QjJfMmSIbCyPW4EDF8d69mZeFxubIylIGkMteFkr	Cabello, Roman Lois	0009-02-05	2	2015-09-15 07:55:58.174923	2199-12-31 23:59:59.999	Roman	Lois	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
9	sYbJYuJUBNDgRcwDUOr3LhFnvsw424uSYhmE0rx7	M	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	N	Y	N	N	Y	EMN	dqp9Tw8rMvkHeZpVEkNuZJlmwtPBZM0tsrjCSGSw	QjJfMmSIbCyPW4EDF8d69mZeFxubIylIGkMteFkr	Cabello, Roman Lois	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:55:58.174923	Roman	Lois	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
19	mqZIVP2jMSn1VHrJ6nLaEHLblTiXW3eCC0BZDA6d	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	N	Y	Y	N	N	OHI	qtw4nQ44m7Si2kuCZjwq8PmEkxQcPDMvZIjrzTdM	4jeLurTXTIAGXc9kY5uuhOAbSIbYp4nxfzlhOw3g	Borkowski, Devon Jacquline	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:13.895692	Devon	Jacquline	Borkowski	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
129	wD6y2AQPgV3HtqM23RFtegCe56oenrPa6DdbElzI	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	N	N	Y	N	N	VI	zXNJatxmXL0LbQYd3ANwEsjGbQkCOIeoy0B5vVXG	pSU02h0m7dKGePY7gH3JrcROBfeTqXMoVJAqxvT0	Carriere, Erasmo Orval	0009-02-05	2	2015-09-15 07:56:21.782214	2199-12-31 23:59:59.999	Erasmo	Orval	Carriere	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
33	urU46yAVop0SQHyO35ugSyxZHrWM1NDDyJY3g4Uz	M	Black or African American	N	N	N	Y	N	N	N	N	N	Y	Y	N	N	N	DB	5KQ3XwmwcnQ7zvKKJlQFP6cjN3xmBway9UDfImcH	hK7kngeulxuN2vTCqeTXdBTPhmKJ0a7u2pxirjrV	Perla, Autumn Verena	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Autumn	Verena	Perla	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
34	VfwdANZxA7Ak4zCPTWDwK9Zd47uiFTJp4rbBTmMs	M	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	N	N	Y	N	N	ID	xRJ2endDR593Ciqb5pcc4pp5P2xgPCYDnPfH6kSK	lONb1ELxhNmgld1Bht9x5ytDZ3QPk3TontWpMcBi	Pando, Darci Joella	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Darci	Joella	Pando	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
47	r1s4BnsoqFMAxUnLv1q3GbBD9SWWz0yUXPaFp0Nd	M	Asian	N	N	Y	N	N	N	N	N	Y	Y	Y	Y	Y	N	DD	troNcSA0WmW7Z5fiOOibDVRaR3DYhxpYzHklOou4	DdzkB0orkdmRQVp9nbhO7tbzjspzWKCOfIO788v0	Cabello, Kimberely Chase	0002-09-05	2	2015-09-15 07:56:02.050598	2199-12-31 23:59:59.999	Kimberely	Chase	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
194	WAhvqzYnESXaMgGYTK04IH1RPLeTatzuOubPg8U7	M	Asian	N	N	Y	N	N	N	N	N	N	N	Y	N	N	Y	TBI	eCPPkTke1VjCEQGR7fv75sCaUOpKB6Ux4uubwxmp	0JvlHOeCIPlXfvoQBcaq88zuhe4VC3CMqSMIewNf	Heckman, Arnette Roman	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Arnette	Roman	Heckman	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
2	k1kUAyTitX2QYeasKtOPdOxbPCSIet5I00G6vQON	F	White	N	N	N	N	N	Y	N	N	N	Y	Y	N	N	N	SLD	t8ivqpZI9goOjdx2lezvRQdwlupLushf7wh9ofib	s8R7rPtONccd6m7XMWFxyQzoMTa1UqvZoScl6wLI	Ung, Verena Vincent	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:55:58.174923	Verena	Vincent	Ung	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
27	KDGutaOLUnOe1nGxReYkTttxEhi1iLsEL5CE8CPa	M	Two or more races	N	N	N	N	N	N	N	Y	Y	N	N	N	N	N	SLD	WLp5Q8kUkjBxoTH4pxHKGU6fAkUl0habx6kAsiwz	2GhcKHiErNzGJsOm9FDKroVSv5ri83VjIXhv4Egp	Heckman, Loni Senaida	0009-02-05	2	2015-09-15 07:55:58.174923	2199-12-31 23:59:59.999	Loni	Senaida	Heckman	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
31	r1s4BnsoqFMAxUnLv1q3GbBD9SWWz0yUXPaFp0Nd	M	Asian	N	N	Y	N	N	N	N	N	Y	Y	Y	Y	Y	N	DD	troNcSA0WmW7Z5fiOOibDVRaR3DYhxpYzHklOou4	DdzkB0orkdmRQVp9nbhO7tbzjspzWKCOfIO788v0	Cabello, Kimberely Chase	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:02.050598	Kimberely	Chase	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
50	Z94Y0ynyVyq3VCwLTjIRZHTZSYkFHT0lBN8meOgb	F	Asian	N	N	Y	N	N	N	N	N	N	N	Y	Y	Y	Y	SLD	Vqp8InuVS0EmPH5SGuynzzrxnZLVYrRCQRH1EIzQ	N0pp23Z9iEtPwPGjuQXLEiii0xy2ohcTWVkffgjg	Cartlidge, Keshia Vincent	0002-09-05	2	2015-09-15 07:56:02.050598	2199-12-31 23:59:59.999	Keshia	Vincent	Cartlidge	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
36	Z94Y0ynyVyq3VCwLTjIRZHTZSYkFHT0lBN8meOgb	F	Asian	N	N	Y	N	N	N	N	N	N	N	Y	Y	Y	Y	SLD	Vqp8InuVS0EmPH5SGuynzzrxnZLVYrRCQRH1EIzQ	N0pp23Z9iEtPwPGjuQXLEiii0xy2ohcTWVkffgjg	Cartlidge, Keshia Vincent	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:02.050598	Keshia	Vincent	Cartlidge	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
51	zoxqZgWkKoS9mT7YODBFwUZS48qNkoXzGPZjXUHi	M	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	N	Y	Y	N	N	MD	3QSWsRpOpp9qwGRb7174DczCwDiIuk9mrbM0k1qI	sPKaMbmrXE2w80ZQ4SIcRhQNZ1HZeTolaWaDw2k6	Jewell, Betty Senaida	0002-09-05	2	2015-09-15 07:56:02.050598	2199-12-31 23:59:59.999	Betty	Senaida	Jewell	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
37	zoxqZgWkKoS9mT7YODBFwUZS48qNkoXzGPZjXUHi	M	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	N	Y	Y	N	N	MD	3QSWsRpOpp9qwGRb7174DczCwDiIuk9mrbM0k1qI	sPKaMbmrXE2w80ZQ4SIcRhQNZ1HZeTolaWaDw2k6	Jewell, Betty Senaida	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:02.050598	Betty	Senaida	Jewell	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
53	Avrxze2AmndYm8dxmLJxDxhLsKpRgxaISvx3KS6F	M	Two or more races	N	N	N	N	N	N	N	Y	N	Y	Y	N	N	Y	EMN	9H6XnHLSpEfdilMrWU1xrxDGllt0BlK8y25GOAXf	8c6zW5FYxyREirYImWd75l0oh72XUsWtHFmnx7qW	Buckwalter, Keshia Debby	0002-09-05	2	2015-09-15 07:56:05.991977	2199-12-31 23:59:59.999	Keshia	Debby	Buckwalter	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
13	EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	Y	Y	N	Y	N	OI	QG0iC7ffrwqWkMtDvQc5YLANyEkjhrq9OWn6Ajov	tJnsdsJVfv8euA4CZzZxDy9ILxhjqB1jV57Bv4bJ	Forst, Darci Betty	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:13.895692	Darci	Betty	Forst	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
14	ITh1jmueUE6G8rMT9oKqLXZKZMPKahZUoAUuvOQE	M	Asian	N	N	Y	N	N	N	N	N	N	N	Y	N	Y	N	OI	OimvDBNrhNjuKD5x99zg5Oqsi2Cht2WucDis0K1O	gJJP5383nfHxtkqDy2vCWxdPSmqDrQNSLFKxFzlP	Gillett, Sandy Sandy	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:13.895692	Sandy	Sandy	Gillett	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
15	MyXTFn1S8qz5lQJAjw11CeaMjoaIiQbo8KihC5ix	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	Y	Y	Y	N	Y	HI	7Jde6xDUfRO1fBo41AFgFQb8WHq7nowhL3abbVeX	3l2uzb7OYZRxJL9Mtkhpopb1ifSlpIOHM7OjH2uA	Gatti, Christin Barbar	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:13.895692	Christin	Barbar	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
44	E9fHez5Rp8SfdyyhzYgAlyLyIE1JYmpYL8d9Up4W	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	Y	N	Y	N	N	ID	zR4uPNZtt7KQSSb1OtijkI8Z3lDaRazHsiO03wGY	OUjRHF0b6p41lL5GwuVzXyXQpk2yqduCxsp3eZ3P	Gillett, Debby Senaida	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:09.937508	Debby	Senaida	Gillett	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
144	sstpR8WRh7effxahZuK1ZPaQJmLH10nFDTSm23am	F	Asian	N	N	Y	N	N	N	N	N	Y	N	N	Y	Y	Y	DB	lJNWeBPTH8pWTldllW5CSVxN1E8b41usZSxxGxcc	bXwzYWFtuW0OvjYDxXMlg3pFoMDwDzALTPg457AY	Gatti, Darci Devon	0002-09-05	2	2015-09-15 07:56:25.615271	2199-12-31 23:59:59.999	Darci	Devon	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
114	sstpR8WRh7effxahZuK1ZPaQJmLH10nFDTSm23am	F	Asian	N	N	Y	N	N	N	N	N	Y	N	N	Y	Y	Y	DB	lJNWeBPTH8pWTldllW5CSVxN1E8b41usZSxxGxcc	bXwzYWFtuW0OvjYDxXMlg3pFoMDwDzALTPg457AY	Gatti, Darci Devon	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:25.615271	Darci	Devon	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
145	Uetza1KA3EqTldkdiq0w92KFYhVqBL6zLWinhXI9	F	Black or African American	N	N	N	Y	N	N	N	N	N	N	N	Y	Y	N	OI	DTdpU08KJSO0JBj3SmXaYdkerjGFRXcgjwryaonp	HTqQ01LuKWOiZo7FuuYOqwPO1A4y0vnSR12vyzaz	Perla, Orval Kimberely	0002-09-05	2	2015-09-15 07:56:25.615271	2199-12-31 23:59:59.999	Orval	Kimberely	Perla	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
85	ZmrZFBiOmI2H2QuEsZm59MMu7bIAhKHUjge92f6E	M	White	N	N	N	N	N	Y	N	N	Y	Y	Y	N	N	Y	AUT	swmmMy3yINRax8X2lZgSXb49caxsdhJrC5SGGO4L	UbYZjnIcBk3y9qIpq7QvOusuqcc7Iw0s6S0z4281	Matsuura, Arnette Sandy	0009-02-05	2	2015-09-15 07:56:09.937508	2199-12-31 23:59:59.999	Arnette	Sandy	Matsuura	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
93	MXGplB4M232z2j4FNTStjIAKRER2ujJ8Z2OlV7Yn	F	Asian	N	N	Y	N	N	N	N	N	Y	Y	N	Y	N	Y	OI	xK2Faci3f8jeHK0T5ctKCG9m8qtFs0CUS92459TJ	rBQcB1GLoy5fdMUfjBX7cmKz3xKluOBGdsJL5DDn	Parr, Joella Betty	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Joella	Betty	Parr	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
186	qEy737ea1Emx97d0RTxk7QpDWecnUu2eEBWvQXkK	M	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	N	N	Y	Y	N	OI	PHSqW51HGoDx5DDlrf1AZ8MQhRTnYqmp5hasPeAL	YFTPFKP33wUilNIhHyFcJLPpbnx8U0xSrtxBffCq	Neveu, Roman Devon	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Roman	Devon	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
124	loKBQQ7EnGEJKKoK39nYLCNWtoJIDC2yNW9cPcmh	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	N	Y	N	Y	N	SLI	x1Gi3Q1cKlhWvsthoxJSHWSnAsFkXOiW8VJivQ6e	LAkGZnolk1llJQO3UvrPZ8Ze4YveAohGbYjb5edQ	Pedone, Debby Betty	0009-02-05	2	2015-09-15 07:56:21.782214	2199-12-31 23:59:59.999	Debby	Betty	Pedone	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
125	nHaADPpFPr1tpkcoS2Xj5mhmvdViLrumXuJMSox3	M	White	N	N	N	N	N	Y	N	N	Y	Y	N	N	N	Y	SLD	7TgDktADgMDZW4j8udHFtT7QuZo8Qxf0wzTBfd2c	Je3HLTfOW7w8SPm63XuBFsHbXUAaFxz23AljKYpi	Gillett, Betty Orval	0009-02-05	2	2015-09-15 07:56:21.782214	2199-12-31 23:59:59.999	Betty	Orval	Gillett	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
126	nQXqvAHQ0W1FOGH2IFhG68Aa590JN9bpNQtSXdiw	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Augustus, Orval Marisela	0009-02-05	1	1900-01-01 00:00:00	2199-12-31 23:59:59.999	Orval	Marisela	Augustus	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
128	UmOkwomla6wQuMu8rde3f3OkVwf23neTGCISpB28	F	Native Hawaiian or other Pacific Islander	N	N	N	N	Y	N	N	N	N	Y	Y	N	N	N	EMN	DvEgss4U4elcqiVQXhluwT1sUwSkpzKaEZiX647Z	QCj5a15vfO9wRiN1GSCQi3hYaEDSJQyaSAnHdU12	Cabello, Barbar Senaida	0009-02-05	2	2015-09-15 07:56:21.782214	2199-12-31 23:59:59.999	Barbar	Senaida	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
59	UmOkwomla6wQuMu8rde3f3OkVwf23neTGCISpB28	F	Native Hawaiian or other Pacific Islander	N	N	N	N	Y	N	N	N	N	Y	Y	N	N	N	EMN	DvEgss4U4elcqiVQXhluwT1sUwSkpzKaEZiX647Z	QCj5a15vfO9wRiN1GSCQi3hYaEDSJQyaSAnHdU12	Cabello, Barbar Senaida	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:21.782214	Barbar	Senaida	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
22	8Kjp09Mfair9T4YbPnMaq7t35stJce29GjDxDHzD	F	Native Hawaiian or other Pacific Islander	N	N	N	N	Y	N	N	N	Y	N	Y	Y	N	N	SLI	r8jgDU58UHF7RDnN9xrOJIH5Xdh1uR5Lb4ffbUzd	tRdbAcKiyDNrWNbQroDe8FWDFJDgRaGcUUm2gqMp	Heckman, Orval Barbar	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Orval	Barbar	Heckman	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
200	KL3R8L1EaO8MgymDcMI59fe7DnjLk0ZuDZiwO2J2	F	Two or more races	N	N	N	N	N	N	N	Y	Y	N	Y	Y	Y	Y	AUT	YF1IdV4ztKcjrlJlJkwf1fqIg8yuj2xn9ESdUdsV	WX95D3DMB8lxa2WOd0oFWiUvcMsat1t5gLC8yJTU	Gatti, Sandy Verena	9999-01-01	3	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Sandy	Verena	Gatti	43e0d272a0c245bfb568ee9e944a12c9436f17	R0003	\N	\N	\N
3	KDGutaOLUnOe1nGxReYkTttxEhi1iLsEL5CE8CPa	M	Two or more races	N	N	N	N	N	N	N	Y	Y	N	N	N	N	N	SLD	WLp5Q8kUkjBxoTH4pxHKGU6fAkUl0habx6kAsiwz	2GhcKHiErNzGJsOm9FDKroVSv5ri83VjIXhv4Egp	Heckman, Loni Senaida	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:55:58.174923	Loni	Senaida	Heckman	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
39	IWr9RrKwQAe5iXf7W8xFiTf4fxYeWjWvoRJB7s45	M	Two or more races	N	N	N	N	N	N	N	Y	N	Y	Y	N	N	N	OHI	kh2OFlFgeEwhUf8by4js3JylAc5mcBS6vYVPJDXh	Fn2wBPPV04m38Cn50laxjbqYZqYpnghFjHRfP9ev	Perla, Roman Debby	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:21.782214	Roman	Debby	Perla	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
40	p42AqWSux32Vo3RQzM34aioB8DbTdzDqfLRsAnhF	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	Y	Y	N	N	Y	ID	ktBL9v7hQF8A4LKbVnPxbQlILxTAm7G9BKGNXNdj	XeUALaoza0bC7hYQ4UFDNhJlLD3QeO0lSJAe23o0	Lawlor, Devon Jacquline	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:21.782214	Devon	Jacquline	Lawlor	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
41	RQaWFAiKM91B9AbbwRXpJzOo302G4qKWtzbpRano	F	White	N	N	N	N	N	Y	N	N	Y	Y	N	Y	N	Y	HI	fnBllhnGFCbGz3Ifmcapa13xiR6NaWJlqqbGO6iD	9BKPvX6AfXTVdEXx473rHEXEPeMfxkAmYRJgq5lI	Perla, Thurman Betty	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:21.782214	Thurman	Betty	Perla	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
25	GxZdYAWx8b0sf9CbdWYT8HsXYqBnNikEkDMikPkd	M	Native Hawaiian or other Pacific Islander	N	N	N	N	Y	N	N	N	N	N	Y	Y	N	Y	MD	ZZRDwuqMsOWqhqTccuae5rg6ShNIc0OxClU9SJAN	8iThk59GvZdrOTkGLfgjkpY67nUJ9u9qPIUSJmRC	Cabello, Keshia Barbar	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Keshia	Barbar	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
29	o557yJ1Kx4YrfB8oMHfoaDVLMTFj3DJXWrKMm9zT	M	Asian	N	N	Y	N	N	N	N	N	Y	Y	N	N	Y	N	HI	s7VUQCuVTAzFIGpKVlmJe4taEyilIVd7kQDnTcle	sNY082Rah4osJ4DLD0UCRho0MvycGnGwlkM4SKZX	Gatti, Thurman Erasmo	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Thurman	Erasmo	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
95	qEy737ea1Emx97d0RTxk7QpDWecnUu2eEBWvQXkK	M	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	N	N	Y	Y	N	OI	PHSqW51HGoDx5DDlrf1AZ8MQhRTnYqmp5hasPeAL	YFTPFKP33wUilNIhHyFcJLPpbnx8U0xSrtxBffCq	Neveu, Roman Devon	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Roman	Devon	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
187	UMaYGd8hto4TYc7EiWKtivzti8YAWg5xozomFI8q	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	N	N	Y	N	N	DD	GZHpQpxaeap0uLPtsMSNT525rjOaTQiKeyj3YBnK	DGsfQaDDfXEMog1xErfGsJeWSIn7QDNTYHwbxcx1	Jewell, Lois Vincent	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Lois	Vincent	Jewell	dcdaa08b9b9046beb3e77419481c860bc6598e	R0001	\N	\N	\N
97	UMaYGd8hto4TYc7EiWKtivzti8YAWg5xozomFI8q	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	N	N	Y	N	N	DD	GZHpQpxaeap0uLPtsMSNT525rjOaTQiKeyj3YBnK	DGsfQaDDfXEMog1xErfGsJeWSIn7QDNTYHwbxcx1	Jewell, Lois Vincent	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Lois	Vincent	Jewell	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
188	vwkr0R9sEgA872YY02LOWZFwoTLq0ATfnEvZ0i17	F	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	N	N	Y	Y	Y	DB	wErx2MhJgnsv4KQMeSoJh2dVSn198lOyLnxjtomP	D73On2W5jdPNO4dLrU1dMpZPxnVxtwcfGzdHgy8S	Cartlidge, Shane Becki	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Shane	Becki	Cartlidge	dcdaa08b9b9046beb3e77419481c860bc6598e	R0001	\N	\N	\N
98	vwkr0R9sEgA872YY02LOWZFwoTLq0ATfnEvZ0i17	F	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	N	N	Y	Y	Y	DB	wErx2MhJgnsv4KQMeSoJh2dVSn198lOyLnxjtomP	D73On2W5jdPNO4dLrU1dMpZPxnVxtwcfGzdHgy8S	Cartlidge, Shane Becki	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Shane	Becki	Cartlidge	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
189	Xwk5McYQSIRVCwDpzpWzMfHYetWqy5BUNy6Z2ogV	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	Y	Y	N	N	N	DD	MlmSUBgk2YxCyyfo6OptZmHBC02LlOScVKkBPyQO	xCgjFCvTnUHSEh54cJnkjTZVMEtIczPiAxvh5CrU	Lawlor, Chase Chase	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Chase	Chase	Lawlor	5f3bcce2cac34dd38578b9d423801dc3b5caf6	R0001	\N	\N	\N
190	3i8VkcJCs5QffVgj2QOmZr3MLv40I0KJc5k5DcWe	F	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	Y	Y	N	Y	N	SLD	sPKVH1LPyxMta9HHY4pOUwYc2hgvAQXRb4Nn3vVH	uYMntLy65YFYZooxdD3tmjcfOzdAfnbOURxtw5uL	Mantle, Keshia Arnette	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Keshia	Arnette	Mantle	dcdaa08b9b9046beb3e77419481c860bc6598e	R0002	\N	\N	\N
135	3i8VkcJCs5QffVgj2QOmZr3MLv40I0KJc5k5DcWe	F	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	Y	Y	N	Y	N	SLD	sPKVH1LPyxMta9HHY4pOUwYc2hgvAQXRb4Nn3vVH	uYMntLy65YFYZooxdD3tmjcfOzdAfnbOURxtw5uL	Mantle, Keshia Arnette	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Keshia	Arnette	Mantle	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
191	N1eKk2eGo15Htuapk7zDnIIftLz18aVE1PMPSktf	M	Black or African American	N	N	N	Y	N	N	N	N	N	N	Y	N	Y	Y	DB	1MuydaJPqVwL73BlUbInGkDx4AOijXeulKfdfP6v	zkx46I7G2Nd1CipPzPAOYgC92lGvNZYvOHU0wbW2	Pedone, Barbar Erasmo	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Barbar	Erasmo	Pedone	343fe3a084b2422796dbd7533a68de13c3cf37	R0002	\N	\N	\N
139	N1eKk2eGo15Htuapk7zDnIIftLz18aVE1PMPSktf	M	Black or African American	N	N	N	Y	N	N	N	N	N	N	Y	N	Y	Y	DB	1MuydaJPqVwL73BlUbInGkDx4AOijXeulKfdfP6v	zkx46I7G2Nd1CipPzPAOYgC92lGvNZYvOHU0wbW2	Pedone, Barbar Erasmo	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Barbar	Erasmo	Pedone	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
192	pB7SBPI9whDTX6jqMTvlCa5POI7wiii3ik8YSqUN	M	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	DD	71L3VffWOE0M4VWkRnc68I3jHb2fq71R9guPY9JJ	sfC58DkrG3bkJ2rcGRb8SGgBjmvLuVlT0KGZ0jLL	Neveu, Jacquline Kimberely	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Jacquline	Kimberely	Neveu	cbae066d8cec4492bcbb66c5dccd0965689b3b	R0002	\N	\N	\N
143	pB7SBPI9whDTX6jqMTvlCa5POI7wiii3ik8YSqUN	M	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	N	N	N	N	N	DD	71L3VffWOE0M4VWkRnc68I3jHb2fq71R9guPY9JJ	sfC58DkrG3bkJ2rcGRb8SGgBjmvLuVlT0KGZ0jLL	Neveu, Jacquline Kimberely	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Jacquline	Kimberely	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
193	rMB447lffsMQCZOQPIUTPusiGmZolvpxn7jMAsiT	F	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	Y	Y	Y	N	N	SLD	w3eZOegFEIUSKulQeCRbbgDbj9WbHE1cOvn69gPO	5vlNJePaDUMPzuw3C6SIV6wXpD0TXMjGrn1rIkME	Benevides, Debby Orval	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Debby	Orval	Benevides	343fe3a084b2422796dbd7533a68de13c3cf37	R0002	\N	\N	\N
102	rMB447lffsMQCZOQPIUTPusiGmZolvpxn7jMAsiT	F	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	Y	Y	Y	N	N	SLD	w3eZOegFEIUSKulQeCRbbgDbj9WbHE1cOvn69gPO	5vlNJePaDUMPzuw3C6SIV6wXpD0TXMjGrn1rIkME	Benevides, Debby Orval	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Debby	Orval	Benevides	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
148	WAhvqzYnESXaMgGYTK04IH1RPLeTatzuOubPg8U7	M	Asian	N	N	Y	N	N	N	N	N	N	N	Y	N	N	Y	TBI	eCPPkTke1VjCEQGR7fv75sCaUOpKB6Ux4uubwxmp	0JvlHOeCIPlXfvoQBcaq88zuhe4VC3CMqSMIewNf	Heckman, Arnette Roman	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Arnette	Roman	Heckman	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
195	6boPVVoO1Vuyg8VJ5AqV0Zha4rVGrZt3uPrM7xgw	F	Native Hawaiian or other Pacific Islander	N	N	N	N	Y	N	N	N	Y	N	Y	Y	N	N	EMN	qZL2LXxVKg8Sz8vOKANWLueLW3V5giAyz2jpg4D0	L5wQxkNvAhoDiwvT3jKyOEYyA25RaEL0WVIPOH0Y	Parr, Betty Erasmo	9999-01-01	3	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Betty	Erasmo	Parr	43e0d272a0c245bfb568ee9e944a12c9436f17	R0003	\N	\N	\N
152	6boPVVoO1Vuyg8VJ5AqV0Zha4rVGrZt3uPrM7xgw	F	Native Hawaiian or other Pacific Islander	N	N	N	N	Y	N	N	N	Y	N	Y	Y	N	N	EMN	qZL2LXxVKg8Sz8vOKANWLueLW3V5giAyz2jpg4D0	L5wQxkNvAhoDiwvT3jKyOEYyA25RaEL0WVIPOH0Y	Parr, Betty Erasmo	0002-09-05	2	2015-09-15 07:56:29.534211	2015-09-15 07:56:37.259892	Betty	Erasmo	Parr	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
196	6xduCZW2oSUGSLzpiFpJQQK8YSAzmBrMjAh4WEkn	M	White	N	N	N	N	N	Y	N	N	N	N	Y	N	N	N	HI	K0e1FlOXuZVSpMaTr2EUHQLrGDQSoSmZWfuY1MED	nb43B38TZ8h5SZPLTc6GMdXWSZxi8O6xGOUaLZvS	Gatti, Shane Kimberely	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Shane	Kimberely	Gatti	3c6b9327b0484f5f84741b204a6c9f02208bc1	R0003	\N	\N	\N
197	c0pT5dPt1QuTicyE2IWo13jRf8kh2ep7ZI8flg9g	F	Asian	N	N	Y	N	N	N	N	N	Y	Y	Y	N	Y	Y	EMN	UeDU72FXhOsN87hmLVrvg2axM0ew777BXwUfOMQi	3ZnniDJxEkJJud7n9G1N5inYdTaqvPSqp60jxBHx	Augustus, Betty Senaida	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Betty	Senaida	Augustus	5f3bcce2cac34dd38578b9d423801dc3b5caf6	R0003	\N	\N	\N
115	Uetza1KA3EqTldkdiq0w92KFYhVqBL6zLWinhXI9	F	Black or African American	N	N	N	Y	N	N	N	N	N	N	N	Y	Y	N	OI	DTdpU08KJSO0JBj3SmXaYdkerjGFRXcgjwryaonp	HTqQ01LuKWOiZo7FuuYOqwPO1A4y0vnSR12vyzaz	Perla, Orval Kimberely	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:25.615271	Orval	Kimberely	Perla	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
146	UH69Q4IEQrjnQGvI9xhrNRTWUtmihpX8hlQ4r61X	M	Black or African American	N	N	N	Y	N	N	N	N	N	N	Y	N	Y	N	SLI	EZ54dXWHp5YonTwHJGBO7OwfrH5qgGQXUZrQcINr	OcXwDqvPR5sGfzoIcohmBFriXTnx5rO6H6mrSY9d	Parr, Winona Loni	0002-09-05	2	2015-09-15 07:56:25.615271	2199-12-31 23:59:59.999	Winona	Loni	Parr	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
71	UH69Q4IEQrjnQGvI9xhrNRTWUtmihpX8hlQ4r61X	M	Black or African American	N	N	N	Y	N	N	N	N	N	N	Y	N	Y	N	SLI	EZ54dXWHp5YonTwHJGBO7OwfrH5qgGQXUZrQcINr	OcXwDqvPR5sGfzoIcohmBFriXTnx5rO6H6mrSY9d	Parr, Winona Loni	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:25.615271	Winona	Loni	Parr	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
20	uJkYBTpWyQhQPsBC4PFyGIFqcoyKQiHxv4f3IYsu	F	Asian	N	N	Y	N	N	N	N	N	N	Y	N	N	Y	N	MD	LWBhsVv7yWdjNlIGtlUQzsPY1qVaygSUKSDHTKDW	ZVeK3Mo3PCiRKga5acczo6phYZQKkFjkBTUH1cTi	Neveu, Chase Keshia	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:25.615271	Chase	Keshia	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
149	4mzU3y7OgIchC8Rz9tfOk7KI8AcXlVtFPVOO1XFH	M	Two or more races	N	N	N	N	N	N	N	Y	Y	N	N	Y	N	Y	MD	Vv4pFgpYYUhRET3hz9ODQ5STADstVH3TwhjDME1K	vo8zyU6ApIaNtviaTL0qtWIYIeJAHnijIqVVN3t4	Matsuura, Cornelius Loni	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Cornelius	Loni	Matsuura	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
150	4xNcr39DqN64ORBLFyIG322oypneHyCBYVXKZZPm	M	White	N	N	N	N	N	Y	N	N	Y	N	N	N	N	N	ID	FEF5lXrReycYbCcr51NDoKDpjMlcswSMK3sBPH82	3Vdt1dUAmoNAKaHVOgELSeBJKEEyAabfHzYv7c78	Benevides, Shane Thurman	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Shane	Thurman	Benevides	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
153	8Kjp09Mfair9T4YbPnMaq7t35stJce29GjDxDHzD	F	Native Hawaiian or other Pacific Islander	N	N	N	N	Y	N	N	N	Y	N	Y	Y	N	N	SLI	r8jgDU58UHF7RDnN9xrOJIH5Xdh1uR5Lb4ffbUzd	tRdbAcKiyDNrWNbQroDe8FWDFJDgRaGcUUm2gqMp	Heckman, Orval Barbar	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Orval	Barbar	Heckman	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
154	8qadBD3xrsvpfExXtJw6dHilQMcdZHjWg60GuV8D	M	Native Hawaiian or other Pacific Islander	N	N	N	N	Y	N	N	N	Y	Y	N	Y	Y	Y	SLD	MLHUZp0zt5A8IIbVmPeEhEEUOldtM83bChwva4uA	uxf0AjpWiBmhHs4bZ9XKbyIoXOynyNYi7LlU7ir6	Mccreery, Orval Arnette	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Orval	Arnette	Mccreery	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
151	5RsVRnISVN942m5YIo50ucTxyB8cYNObQ1tnrORJ	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Mccreery, Sandy Marisela	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:41.133869	Sandy	Marisela	Mccreery	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
161	jgliMmu6B15PbLy7sq7uRlv40qiIF3liNI3ySpru	F	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	N	N	Y	N	N	MD	GvPr5sZMwbioUajA89GVJsZlclfYuL7suORDYyA7	UYZAdmVcmhGvyIWhC2Gn2AUP9Z55dXPSHb7OCSQl	Carriere, Zofia Autumn	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Zofia	Autumn	Carriere	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
120	jgliMmu6B15PbLy7sq7uRlv40qiIF3liNI3ySpru	F	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	N	N	Y	N	N	MD	GvPr5sZMwbioUajA89GVJsZlclfYuL7suORDYyA7	UYZAdmVcmhGvyIWhC2Gn2AUP9Z55dXPSHb7OCSQl	Carriere, Zofia Autumn	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Zofia	Autumn	Carriere	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
122	KL3R8L1EaO8MgymDcMI59fe7DnjLk0ZuDZiwO2J2	F	Two or more races	N	N	N	N	N	N	N	Y	Y	N	Y	Y	Y	Y	AUT	YF1IdV4ztKcjrlJlJkwf1fqIg8yuj2xn9ESdUdsV	WX95D3DMB8lxa2WOd0oFWiUvcMsat1t5gLC8yJTU	Gatti, Sandy Verena	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Sandy	Verena	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
163	LQWpnLVTSTAXEFoit9Q2RlZNibkOFcpmSYgXMHyo	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Augustus, Orval Marisela	0002-09-05	1	1900-01-01 00:00:00	2199-12-31 23:59:59.999	Orval	Marisela	Augustus	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
164	m5i2mSxYFwrFTnW09BEYlrXBvQIw3bTmo2zm1GaI	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Perla, Orval Marisela	0002-09-05	1	1900-01-01 00:00:00	2199-12-31 23:59:59.999	Orval	Marisela	Perla	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
165	o557yJ1Kx4YrfB8oMHfoaDVLMTFj3DJXWrKMm9zT	M	Asian	N	N	Y	N	N	N	N	N	Y	Y	N	N	Y	N	HI	s7VUQCuVTAzFIGpKVlmJe4taEyilIVd7kQDnTcle	sNY082Rah4osJ4DLD0UCRho0MvycGnGwlkM4SKZX	Gatti, Thurman Erasmo	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Thurman	Erasmo	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
167	OLbCzYnv9jHAVbFnfaD1AtFb2FzNSLjhmkjdFdbJ	F	Two or more races	N	N	N	N	N	N	N	Y	Y	Y	Y	N	N	Y	MD	ODhXzHmDct2rcJJevvJL96yx8AB6ZB04D2Tp99nA	cIKKR1yH9U4lKU0XNX0teg8iZCsCwpumt2fYGtWu	Neveu, Betty Senaida	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Betty	Senaida	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
169	sB2VXSoaEMtiSg4WjFUXNNdh3rDSEvULHmGXvAnG	M	Two or more races	N	N	N	N	N	N	N	Y	N	Y	Y	N	Y	Y	TBI	eReDWqEHPWW1uS7mUBVoj9Ov4xXm2PSqabTbMIsL	DtZyvWYo4Ht95HPqZeRQjuVIkn83TBbp9kiASvjN	Jewell, Senaida Loni	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Senaida	Loni	Jewell	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
171	Uk2E8MeExKfyl6XHBLKpNJe6na8bi3nj8yMCgeFz	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	N	N	N	N	Y	AUT	mQUKZ7qYBovBIcS9sBQJ34Snvr3JwVr2yoyy8MzD	iWnsfdzQdhUYFlXze7BehXrUvCtCn6HtRyvgPwBn	Augustus, Zofia Orval	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Zofia	Orval	Augustus	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
127	Uk2E8MeExKfyl6XHBLKpNJe6na8bi3nj8yMCgeFz	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	N	N	N	N	Y	AUT	mQUKZ7qYBovBIcS9sBQJ34Snvr3JwVr2yoyy8MzD	iWnsfdzQdhUYFlXze7BehXrUvCtCn6HtRyvgPwBn	Augustus, Zofia Orval	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Zofia	Orval	Augustus	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
23	Avrxze2AmndYm8dxmLJxDxhLsKpRgxaISvx3KS6F	M	Two or more races	N	N	N	N	N	N	N	Y	N	Y	Y	N	N	Y	EMN	9H6XnHLSpEfdilMrWU1xrxDGllt0BlK8y25GOAXf	8c6zW5FYxyREirYImWd75l0oh72XUsWtHFmnx7qW	Buckwalter, Keshia Debby	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:05.991977	Keshia	Debby	Buckwalter	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
54	cS2xAK3xDPCBR7ZWbAh9FS08IqjzWxgbZeitAa40	F	White	N	N	N	N	N	Y	N	N	N	N	N	Y	Y	N	OI	9eyBPJS9K5X0kdpskmD5HIugskbIZeJ3dSbvlKD1	vwE73aeUtObNKrRIx3Nt1mP4iimz7BQyDjITVYm8	Gillett, Verena Becki	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:21.782214	Verena	Becki	Gillett	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
56	JRAWX24sIH3Bh2KcmpYNn6ZyGr8e6p6ctkAMFRw5	F	Asian	N	N	Y	N	N	N	N	N	Y	Y	Y	Y	N	N	MD	VxchSUMcdsVWHvFbAU1dUZQHymEGClKRHOucFsCD	UusU7BtgH5knogqWxkGqKMssirKkeuWZIcfN9ljv	Gatti, Shane Senaida	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:21.782214	Shane	Senaida	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
57	LaPd7vVIU9G9NvGr44WMWgwNwvupwIyWtC5kGetF	M	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	Y	Y	Y	Y	N	SLD	IH43WH7R7yvt5qtr3W3Y66qKyaxQkPM7vfaNyiCO	Ac1iIcJEDgScVsfJn7e61o19LhuxU15Rt8jSY6OT	Troy, Verena Winona	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:21.782214	Verena	Winona	Troy	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
58	nHaADPpFPr1tpkcoS2Xj5mhmvdViLrumXuJMSox3	M	White	N	N	N	N	N	Y	N	N	Y	Y	N	N	N	Y	SLD	7TgDktADgMDZW4j8udHFtT7QuZo8Qxf0wzTBfd2c	Je3HLTfOW7w8SPm63XuBFsHbXUAaFxz23AljKYpi	Gillett, Betty Orval	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:21.782214	Betty	Orval	Gillett	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
52	6xduCZW2oSUGSLzpiFpJQQK8YSAzmBrMjAh4WEkn	M	White	N	N	N	N	N	Y	N	N	N	N	Y	N	N	N	HI	K0e1FlOXuZVSpMaTr2EUHQLrGDQSoSmZWfuY1MED	nb43B38TZ8h5SZPLTc6GMdXWSZxi8O6xGOUaLZvS	Gatti, Shane Kimberely	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Shane	Kimberely	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
55	hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ	F	Black or African American	N	N	N	Y	N	N	N	N	Y	N	N	N	Y	N	DB	Tvw1NS4wwtZ4tkwb0iVisHEi7Ry715pRRjGtbL1r	mvxrV6SfCFq11d05bN01KupSnan42G0QEJyFwwO5	Forst, Jacquline Zofia	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Jacquline	Zofia	Forst	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
60	wUfuK1C1vpevOq8cV6oqQxNNwJV1TbzeraL8fC4v	M	White	N	N	N	N	N	Y	N	N	N	Y	N	Y	Y	N	SLD	SYKyRcyh4S0npwfeCEDSXEcNeevVBGyI1cUZ7JBa	QA9WHJjldT89J8LHuhwDFqG4aVG9MKz4pBxkHNKQ	Mccreery, Sandy Devon	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Sandy	Devon	Mccreery	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
76	haGegI5PgD6OzaQ1Mu478Og0zw693ZmLAgQTbjLb	F	Black or African American	N	N	N	Y	N	N	N	N	N	N	N	N	N	Y	MD	ECFaO3SYBBsvZJk11tbA0G4gSO1mOKoTq7rDRXw1	RJSgbORr4aXV4WGkMDLihE6pHnpbe4tp9JAX56wE	Cabello, Jacquline Orval	0009-02-05	2	2015-09-15 07:56:09.937508	2199-12-31 23:59:59.999	Jacquline	Orval	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
45	haGegI5PgD6OzaQ1Mu478Og0zw693ZmLAgQTbjLb	F	Black or African American	N	N	N	Y	N	N	N	N	N	N	N	N	N	Y	MD	ECFaO3SYBBsvZJk11tbA0G4gSO1mOKoTq7rDRXw1	RJSgbORr4aXV4WGkMDLihE6pHnpbe4tp9JAX56wE	Cabello, Jacquline Orval	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:09.937508	Jacquline	Orval	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
78	p2Sf5xBg8rAuTjy9XPGuFJaoYhdKIHISnVuRqoUd	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	Y	Y	N	N	Y	EMN	aSSWKV1Drm6iwJED1qSv44z7HA8TCeXRrwcNoWVm	MePGxAdmGOsSQaCHZEDqxbcD3VCOFLTwlyCgcVrL	Buckwalter, Shane Autumn	0009-02-05	2	2015-09-15 07:56:09.937508	2199-12-31 23:59:59.999	Shane	Autumn	Buckwalter	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
6	p2Sf5xBg8rAuTjy9XPGuFJaoYhdKIHISnVuRqoUd	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	Y	Y	N	N	Y	EMN	aSSWKV1Drm6iwJED1qSv44z7HA8TCeXRrwcNoWVm	MePGxAdmGOsSQaCHZEDqxbcD3VCOFLTwlyCgcVrL	Buckwalter, Shane Autumn	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:09.937508	Shane	Autumn	Buckwalter	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
80	Sc0cLMiRhBfkJsHF7Dwz1ozIPzc1k3s6aOgAD2IT	F	Black or African American	N	N	N	Y	N	N	N	N	Y	N	Y	Y	Y	N	SLI	vHUc6IqR2eA7oqn479WZO8P0v6bm0qzaqNYBXqro	UYuxGmofxNxWDnuXTWFNS4rviLEy7CTfdApVoVzU	Monks, Joella Erasmo	0009-02-05	2	2015-09-15 07:56:09.937508	2199-12-31 23:59:59.999	Joella	Erasmo	Monks	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
72	8qadBD3xrsvpfExXtJw6dHilQMcdZHjWg60GuV8D	M	Native Hawaiian or other Pacific Islander	N	N	N	N	Y	N	N	N	Y	Y	N	Y	Y	Y	SLD	MLHUZp0zt5A8IIbVmPeEhEEUOldtM83bChwva4uA	uxf0AjpWiBmhHs4bZ9XKbyIoXOynyNYi7LlU7ir6	Mccreery, Orval Arnette	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Orval	Arnette	Mccreery	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
74	DSoDwPOg0kZQ5YPnMJ0SLmBaCeGes4a1qjkRUQfd	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	Y	Y	Y	N	Y	EMN	C4vKy987HmZkDVVIhnTBMCuQbXCqW1Qgq6aj6t1n	wdF0ClC7Z8qvAnYH090pWLDDC1mhQSg1ZqEtV3hW	Pando, Joella Zofia	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Joella	Zofia	Pando	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
77	OLbCzYnv9jHAVbFnfaD1AtFb2FzNSLjhmkjdFdbJ	F	Two or more races	N	N	N	N	N	N	N	Y	Y	Y	Y	N	N	Y	MD	ODhXzHmDct2rcJJevvJL96yx8AB6ZB04D2Tp99nA	cIKKR1yH9U4lKU0XNX0teg8iZCsCwpumt2fYGtWu	Neveu, Betty Senaida	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Betty	Senaida	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
79	sB2VXSoaEMtiSg4WjFUXNNdh3rDSEvULHmGXvAnG	M	Two or more races	N	N	N	N	N	N	N	Y	N	Y	Y	N	Y	Y	TBI	eReDWqEHPWW1uS7mUBVoj9Ov4xXm2PSqabTbMIsL	DtZyvWYo4Ht95HPqZeRQjuVIkn83TBbp9kiASvjN	Jewell, Senaida Loni	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Senaida	Loni	Jewell	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
61	zxITiM66QUVOcTyhRaooB0UIOWemGFP8eQE7xPSx	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	N	Y	N	N	Y	SLI	6oZAVILFTfpG9poJ4qFAJOvw0XYa9suCqZPc2Jkf	UZJGBAKBhDPPLpfVCc225qzGY4J1Ei8vjBnnFwh6	Mccreery, Zofia Keshia	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Zofia	Keshia	Mccreery	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
64	OIeWicvTvk5kcbEc0qj7vPo2nh3Vr8XS9e2Vz57b	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	N	Y	N	N	N	TBI	rjmYkBa0yZxWg60UvrOgMyxiQYCWQVd8bXaeCNeS	t1lMZ0W1BM0RoHefhKPLNdUr38uYXTo4yOqxU10F	Perla, Barbar Orval	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:41.133869	Barbar	Orval	Perla	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
66	SBFMQrkvsLNLQWnEXVVeN6ZMQXqemPhTY44hgO2M	M	Black or African American	N	N	N	Y	N	N	N	N	N	Y	Y	N	N	N	SLI	S6RWxM4aZT5T28sUstXnoV75JDnsTtmYXcgTv1k0	IdBEiFs4LSIhyz2lQ29b7WM6IxS2z5SXOYAMfDfy	Mccreery, Jacquline Becki	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:41.133869	Jacquline	Becki	Mccreery	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
86	1hIns0clANFCb9Rq3Cj5vEUUhMzryD9monuIwTFq	M	Asian	N	N	Y	N	N	N	N	N	Y	Y	Y	Y	Y	Y	MD	smSgplUtVjGGlqKHH2bTuPQPpNZZPujM6BpDjMEG	XuYGOYmVHGbw4S2xqjiwPXWiAOtRgtpWswqJFwiU	Monks, Zofia Betty	0002-09-05	2	2015-09-15 07:56:13.895692	2199-12-31 23:59:59.999	Zofia	Betty	Monks	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
62	1hIns0clANFCb9Rq3Cj5vEUUhMzryD9monuIwTFq	M	Asian	N	N	Y	N	N	N	N	N	Y	Y	Y	Y	Y	Y	MD	smSgplUtVjGGlqKHH2bTuPQPpNZZPujM6BpDjMEG	XuYGOYmVHGbw4S2xqjiwPXWiAOtRgtpWswqJFwiU	Monks, Zofia Betty	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:13.895692	Zofia	Betty	Monks	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
87	2vM9yk86Q3OXKckaeaUjcSTHf1TaO5LCT2DHaeU7	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	N	Y	N	N	N	Y	ID	Du1Sh4DTO3nLwT357QZPIzYJJfJX2FLFRMUI2hHC	itBQNt3L5ZuR734wODV8UCZOkAImL1QQv3TEbsOZ	Neveu, Betty Cornelius	0002-09-05	2	2015-09-15 07:56:13.895692	2199-12-31 23:59:59.999	Betty	Cornelius	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
88	dxpojnotJFaxas2YDJMiJoBPHkVCIc4lptuj24Dy	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	N	N	Y	Y	Y	N	SLI	LX9Rwl1Eg9LP92kzBpe5idYStEwlkffCQQY929Su	j8JpKBopcMtBNZwz3JGyhplE018kxwJscvq2xACn	Matsuura, Thurman Kathe	0002-09-05	2	2015-09-15 07:56:13.895692	2199-12-31 23:59:59.999	Thurman	Kathe	Matsuura	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
89	EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	Y	Y	N	Y	N	OI	QG0iC7ffrwqWkMtDvQc5YLANyEkjhrq9OWn6Ajov	tJnsdsJVfv8euA4CZzZxDy9ILxhjqB1jV57Bv4bJ	Forst, Darci Betty	0001-09-05	2	2015-09-15 07:56:13.895692	2199-12-31 23:59:59.999	Darci	Betty	Forst	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
90	ITh1jmueUE6G8rMT9oKqLXZKZMPKahZUoAUuvOQE	M	Asian	N	N	Y	N	N	N	N	N	N	N	Y	N	Y	N	OI	OimvDBNrhNjuKD5x99zg5Oqsi2Cht2WucDis0K1O	gJJP5383nfHxtkqDy2vCWxdPSmqDrQNSLFKxFzlP	Gillett, Sandy Sandy	0002-09-05	2	2015-09-15 07:56:13.895692	2199-12-31 23:59:59.999	Sandy	Sandy	Gillett	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
91	jEEReGK9NLZr2teqi8dFBUhxaozkY3uwy5IB7hUx	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	Y	N	Y	Y	N	VI	g8QWxVFSTYLQZhxYnK6oinNBz03lbpYTlCnxQPmD	wRTfjzpzwMmHWQDTwwvL3BWXR0PgnLdHkUPqK9lo	Troy, Christin Orval	0002-09-05	2	2015-09-15 07:56:13.895692	2199-12-31 23:59:59.999	Christin	Orval	Troy	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
63	jEEReGK9NLZr2teqi8dFBUhxaozkY3uwy5IB7hUx	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	Y	N	Y	Y	N	VI	g8QWxVFSTYLQZhxYnK6oinNBz03lbpYTlCnxQPmD	wRTfjzpzwMmHWQDTwwvL3BWXR0PgnLdHkUPqK9lo	Troy, Christin Orval	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:13.895692	Christin	Orval	Troy	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
94	MyXTFn1S8qz5lQJAjw11CeaMjoaIiQbo8KihC5ix	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	Y	Y	Y	N	Y	HI	7Jde6xDUfRO1fBo41AFgFQb8WHq7nowhL3abbVeX	3l2uzb7OYZRxJL9Mtkhpopb1ifSlpIOHM7OjH2uA	Gatti, Christin Barbar	0002-09-05	2	2015-09-15 07:56:13.895692	2199-12-31 23:59:59.999	Christin	Barbar	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
96	RqHsCUovoRLSAhySU0AnRx0LY8YKin9uDhSFcOMD	F	Asian	N	N	Y	N	N	N	N	N	N	N	N	Y	N	N	EMN	IC2TX2WC8IeessJCg0nggokNgi0sNXvaACRXBUQd	tkBpd9dbEgFKhkjmrCJ5FINSpLk34XW3PajKwo56	Augustus, Verena Lois	0002-09-05	2	2015-09-15 07:56:13.895692	2199-12-31 23:59:59.999	Verena	Lois	Augustus	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
65	RqHsCUovoRLSAhySU0AnRx0LY8YKin9uDhSFcOMD	F	Asian	N	N	Y	N	N	N	N	N	N	N	N	Y	N	N	EMN	IC2TX2WC8IeessJCg0nggokNgi0sNXvaACRXBUQd	tkBpd9dbEgFKhkjmrCJ5FINSpLk34XW3PajKwo56	Augustus, Verena Lois	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:13.895692	Verena	Lois	Augustus	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
83	WW9iCRpiTricUbWoelchVevYIJHSjhAUMpLVYt6D	M	Black or African American	N	N	N	Y	N	N	N	N	N	Y	Y	Y	Y	N	MD	1TZNscNyH36p1d7XN1zadRihrKpSt4i87QzWaWD7	3clJx8gZokw3qKF4R9c8MRGtMI1jDxRqrhjm1Aph	Parr, Erasmo Barbar	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Erasmo	Barbar	Parr	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
107	IWr9RrKwQAe5iXf7W8xFiTf4fxYeWjWvoRJB7s45	M	Two or more races	N	N	N	N	N	N	N	Y	N	Y	Y	N	N	N	OHI	kh2OFlFgeEwhUf8by4js3JylAc5mcBS6vYVPJDXh	Fn2wBPPV04m38Cn50laxjbqYZqYpnghFjHRfP9ev	Perla, Roman Debby	0009-02-05	2	2015-09-15 07:56:21.782214	2199-12-31 23:59:59.999	Roman	Debby	Perla	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
108	JL6uVW6OSVaYqpvndl0foBKAFrf4bdz6HACTTcLd	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	N	N	Y	Y	Y	DB	8WDT6bZI50W2Zl59D3UOS2Cxf9tJQQn8bAU686c3	Tqz1FfcBuMegwffE1XzgED6VVM3sys8KTvkjQoLC	Buckwalter, Arnette Jacquline	0009-02-05	2	2015-09-15 07:56:21.782214	2199-12-31 23:59:59.999	Arnette	Jacquline	Buckwalter	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
92	JL6uVW6OSVaYqpvndl0foBKAFrf4bdz6HACTTcLd	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	N	N	Y	Y	Y	DB	8WDT6bZI50W2Zl59D3UOS2Cxf9tJQQn8bAU686c3	Tqz1FfcBuMegwffE1XzgED6VVM3sys8KTvkjQoLC	Buckwalter, Arnette Jacquline	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:21.782214	Arnette	Jacquline	Buckwalter	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
109	p42AqWSux32Vo3RQzM34aioB8DbTdzDqfLRsAnhF	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	Y	Y	N	N	Y	ID	ktBL9v7hQF8A4LKbVnPxbQlILxTAm7G9BKGNXNdj	XeUALaoza0bC7hYQ4UFDNhJlLD3QeO0lSJAe23o0	Lawlor, Devon Jacquline	0009-02-05	2	2015-09-15 07:56:21.782214	2199-12-31 23:59:59.999	Devon	Jacquline	Lawlor	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
110	RQaWFAiKM91B9AbbwRXpJzOo302G4qKWtzbpRano	F	White	N	N	N	N	N	Y	N	N	Y	Y	N	Y	N	Y	HI	fnBllhnGFCbGz3Ifmcapa13xiR6NaWJlqqbGO6iD	9BKPvX6AfXTVdEXx473rHEXEPeMfxkAmYRJgq5lI	Perla, Thurman Betty	0009-02-05	2	2015-09-15 07:56:21.782214	2199-12-31 23:59:59.999	Thurman	Betty	Perla	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
118	cS2xAK3xDPCBR7ZWbAh9FS08IqjzWxgbZeitAa40	F	White	N	N	N	N	N	Y	N	N	N	N	N	Y	Y	N	OI	9eyBPJS9K5X0kdpskmD5HIugskbIZeJ3dSbvlKD1	vwE73aeUtObNKrRIx3Nt1mP4iimz7BQyDjITVYm8	Gillett, Verena Becki	0009-02-05	2	2015-09-15 07:56:21.782214	2199-12-31 23:59:59.999	Verena	Becki	Gillett	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
116	4mzU3y7OgIchC8Rz9tfOk7KI8AcXlVtFPVOO1XFH	M	Two or more races	N	N	N	N	N	N	N	Y	Y	N	N	Y	N	Y	MD	Vv4pFgpYYUhRET3hz9ODQ5STADstVH3TwhjDME1K	vo8zyU6ApIaNtviaTL0qtWIYIeJAHnijIqVVN3t4	Matsuura, Cornelius Loni	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Cornelius	Loni	Matsuura	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
117	6boPVVoO1Vuyg8VJ5AqV0Zha4rVGrZt3uPrM7xgw	F	Native Hawaiian or other Pacific Islander	N	N	N	N	Y	N	N	N	Y	N	Y	Y	N	N	EMN	qZL2LXxVKg8Sz8vOKANWLueLW3V5giAyz2jpg4D0	L5wQxkNvAhoDiwvT3jKyOEYyA25RaEL0WVIPOH0Y	Parr, Betty Erasmo	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Betty	Erasmo	Parr	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
119	iI34kG6uH1mQ83tbh5PEVVKoQjV1r96laDb4x10n	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	N	Y	Y	N	Y	EMN	76PpHVVXrz4XmEJJCUkNRBPoHtBigoSOtVVTx2Z1	LOoSbNRoiqF7cmOGjcR9Z3KYZRaDkqRx7ygMsygZ	Buckwalter, Autumn Kathe	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Autumn	Kathe	Buckwalter	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
104	5336b7258eb545b3ae7178aeb0cfd72428a36fdb	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	Y	Y	N	Y	N	OI	a64dac1816204195a4759761cf1b2715eaa26359	a91eb32222844401a74296ae3b692b71b536d8f8	Brule, Steve Jan	0001-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Steve	Jan	Brule	dcdaa08b9b9046beb3e77419481c860bc6598e	R0001	\N	\N	\N
99	Xwk5McYQSIRVCwDpzpWzMfHYetWqy5BUNy6Z2ogV	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	Y	Y	N	N	N	DD	MlmSUBgk2YxCyyfo6OptZmHBC02LlOScVKkBPyQO	xCgjFCvTnUHSEh54cJnkjTZVMEtIczPiAxvh5CrU	Lawlor, Chase Chase	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Chase	Chase	Lawlor	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0001	\N	\N	\N
49	wD6y2AQPgV3HtqM23RFtegCe56oenrPa6DdbElzI	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	N	N	Y	N	N	VI	zXNJatxmXL0LbQYd3ANwEsjGbQkCOIeoy0B5vVXG	pSU02h0m7dKGePY7gH3JrcROBfeTqXMoVJAqxvT0	Carriere, Erasmo Orval	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:21.782214	Erasmo	Orval	Carriere	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
134	0YrEK4Y3PihDRZiZnFMinOcTdkNYwublx9Z8lCAp	F	Black or African American	N	N	N	Y	N	N	N	N	N	N	Y	N	N	Y	OI	cneCAQyANr172ywJd3n3MAl1Z3fVHQetNeVQeNn3	111XTD0Ya5bojkRrCgASmcHwlx51DERXxbKWRgld	Mcgowin, Marisela Marisela	0002-09-05	2	2015-09-15 07:56:25.615271	2199-12-31 23:59:59.999	Marisela	Marisela	Mcgowin	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
136	EOHTQnkhMw4mzKP8Rpaq42vOlOBZzsZZkKU6zpll	F	Asian	N	N	Y	N	N	N	N	N	Y	N	Y	N	Y	Y	ID	nt8NWGO5kNNBdXBS8atgidVWhyFSQFoonEXSPpT4	5zGEfa0eUFpkE9cRcTtRbbbKvBIlhHGkHbe07K9E	Aispuro, Devon Marisela	0002-09-05	2	2015-09-15 07:56:25.615271	2199-12-31 23:59:59.999	Devon	Marisela	Aispuro	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
67	EOHTQnkhMw4mzKP8Rpaq42vOlOBZzsZZkKU6zpll	F	Asian	N	N	Y	N	N	N	N	N	Y	N	Y	N	Y	Y	ID	nt8NWGO5kNNBdXBS8atgidVWhyFSQFoonEXSPpT4	5zGEfa0eUFpkE9cRcTtRbbbKvBIlhHGkHbe07K9E	Aispuro, Devon Marisela	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:25.615271	Devon	Marisela	Aispuro	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
137	Fg5kGWmyfjDA600mHsVyZJALKlRwmg8pe1LtPdSb	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	N	N	N	Y	N	TBI	Ua0kQ0waLEMyOkNDhrxiSKBFDfThzxGua8m4XG7S	R7llKZfwafKEIz3IjpgcQBxaTQTGjMuresr9R6te	Neveu, Cornelius Autumn	0002-09-05	2	2015-09-15 07:56:25.615271	2199-12-31 23:59:59.999	Cornelius	Autumn	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
68	Fg5kGWmyfjDA600mHsVyZJALKlRwmg8pe1LtPdSb	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	N	N	N	Y	N	TBI	Ua0kQ0waLEMyOkNDhrxiSKBFDfThzxGua8m4XG7S	R7llKZfwafKEIz3IjpgcQBxaTQTGjMuresr9R6te	Neveu, Cornelius Autumn	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:25.615271	Cornelius	Autumn	Neveu	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
138	JskCum0I6ZULH064xsFeFcW4eZVTIEj5m6zExxG2	M	Black or African American	N	N	N	Y	N	N	N	N	Y	N	Y	Y	Y	N	DD	HeZKMFcuhusK4OvtDLzrrmI79Gdes7RaJKoe9B5d	Jrg0oaGGMSXGLpjKbx1UyVueXXk7hAmN7EnYyIC0	Cabello, Debby Keshia	0002-09-05	2	2015-09-15 07:56:25.615271	2199-12-31 23:59:59.999	Debby	Keshia	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
69	JskCum0I6ZULH064xsFeFcW4eZVTIEj5m6zExxG2	M	Black or African American	N	N	N	Y	N	N	N	N	Y	N	Y	Y	Y	N	DD	HeZKMFcuhusK4OvtDLzrrmI79Gdes7RaJKoe9B5d	Jrg0oaGGMSXGLpjKbx1UyVueXXk7hAmN7EnYyIC0	Cabello, Debby Keshia	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:25.615271	Debby	Keshia	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
112	OimZrYW80W7xl7QUa4Hg9Q9knjdvys1Z8wRIVHuG	F	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	N	Y	N	N	N	N	ID	LVDJ5pmKkbouYqlTqLGFL37ZoQe3oh4zisvYXKNF	sIqnzxAVbEnEyQ5EhHNskz5gSJZ2zjnzHfZ6GCZ0	Cabello, Chase Jacquline	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:25.615271	Chase	Jacquline	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
142	p69xZhxSMQuWmke62h3j1K7izaj1evXeklkHFuMg	M	Black or African American	N	N	N	Y	N	N	N	N	Y	N	Y	Y	Y	Y	SLI	uTP6iHeeGja0N6JtNGfst5Wm1LpAcL5AzKtMnEPS	llv5nV4RRVH4AT3X9Rg4x0CPyoFl2TGBfavZtjSP	Aispuro, Cornelius Becki	0002-09-05	2	2015-09-15 07:56:25.615271	2199-12-31 23:59:59.999	Cornelius	Becki	Aispuro	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
113	p69xZhxSMQuWmke62h3j1K7izaj1evXeklkHFuMg	M	Black or African American	N	N	N	Y	N	N	N	N	Y	N	Y	Y	Y	Y	SLI	uTP6iHeeGja0N6JtNGfst5Wm1LpAcL5AzKtMnEPS	llv5nV4RRVH4AT3X9Rg4x0CPyoFl2TGBfavZtjSP	Aispuro, Cornelius Becki	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:25.615271	Cornelius	Becki	Aispuro	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0002	\N	\N	\N
172	urU46yAVop0SQHyO35ugSyxZHrWM1NDDyJY3g4Uz	M	Black or African American	N	N	N	Y	N	N	N	N	N	Y	Y	N	N	N	DB	5KQ3XwmwcnQ7zvKKJlQFP6cjN3xmBway9UDfImcH	hK7kngeulxuN2vTCqeTXdBTPhmKJ0a7u2pxirjrV	Perla, Autumn Verena	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Autumn	Verena	Perla	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
173	VfwdANZxA7Ak4zCPTWDwK9Zd47uiFTJp4rbBTmMs	M	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	N	N	Y	N	N	ID	xRJ2endDR593Ciqb5pcc4pp5P2xgPCYDnPfH6kSK	lONb1ELxhNmgld1Bht9x5ytDZ3QPk3TontWpMcBi	Pando, Darci Joella	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Darci	Joella	Pando	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
175	WQSZZ0TnQLwHd1VBV596z6mqD7T07BZiUgXDa2o2	M	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	Y	N	N	Y	Y	AUT	8HWDuDNE3MWRqG3L083McUPmamnln4GqT22b7xvO	ONcaaz0WyoDQHryVWS8ocmbdzx43wMjRlPobkbad	Parr, Sandy Senaida	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Sandy	Senaida	Parr	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
130	WQSZZ0TnQLwHd1VBV596z6mqD7T07BZiUgXDa2o2	M	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	Y	N	N	Y	Y	AUT	8HWDuDNE3MWRqG3L083McUPmamnln4GqT22b7xvO	ONcaaz0WyoDQHryVWS8ocmbdzx43wMjRlPobkbad	Parr, Sandy Senaida	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Sandy	Senaida	Parr	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
174	w7TmiOs6CWJsXS2l1crivMsf42ZlQ5WAr6Xri2rd	M	Black or African American	N	N	N	Y	N	N	N	N	N	Y	N	Y	Y	N	VI	D1AXqIoaXx8AQTaQoumsfu5xeX4aGzCRrecFKecG	JaT2z61Ejzjz9rFhwN0ePthYx9cLsIJJPGYoHeLY	Cabello, Becki Lois	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Becki	Lois	Cabello	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
158	GXr7X73TjgGmTEm62Q3cGYSMBHE34YWvhwJEalyy	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Pedone, Shane Marisela	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:41.133869	Shane	Marisela	Pedone	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
176	wTpuVRx1CP4eXjjQiCKXjjvvLaZpRAAujc0QB30k	M	Black or African American	N	N	N	Y	N	N	N	N	Y	N	Y	Y	Y	N	SLI	ebSrKVKDGaaPL3mlUJcqbme0wl3kxMv2HlTPriG1	QR1uPT4gYiQVWWfRfcaR6woqvvuUmJxHUtcOqKoL	Matsuura, Debby Betty	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Debby	Betty	Matsuura	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
131	wTpuVRx1CP4eXjjQiCKXjjvvLaZpRAAujc0QB30k	M	Black or African American	N	N	N	Y	N	N	N	N	Y	N	Y	Y	Y	N	SLI	ebSrKVKDGaaPL3mlUJcqbme0wl3kxMv2HlTPriG1	QR1uPT4gYiQVWWfRfcaR6woqvvuUmJxHUtcOqKoL	Matsuura, Debby Betty	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Debby	Betty	Matsuura	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
177	WW9iCRpiTricUbWoelchVevYIJHSjhAUMpLVYt6D	M	Black or African American	N	N	N	Y	N	N	N	N	N	Y	Y	Y	Y	N	MD	1TZNscNyH36p1d7XN1zadRihrKpSt4i87QzWaWD7	3clJx8gZokw3qKF4R9c8MRGtMI1jDxRqrhjm1Aph	Parr, Erasmo Barbar	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Erasmo	Barbar	Parr	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
178	xhHUuTmLdRjylYGiO2ZU54h2J04u9BJSn0exbdX2	M	White	N	N	N	N	N	Y	N	N	Y	N	Y	N	N	Y	AUT	XbmxoMlq3d94wmpWt4Fi3CK5vv1hloH2kJD0XDlS	guu3TA086qniuwgGkytIo49LMHk9PGO6EoU09vwY	Augustus, Joella Devon	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Joella	Devon	Augustus	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
84	xhHUuTmLdRjylYGiO2ZU54h2J04u9BJSn0exbdX2	M	White	N	N	N	N	N	Y	N	N	Y	N	Y	N	N	Y	AUT	XbmxoMlq3d94wmpWt4Fi3CK5vv1hloH2kJD0XDlS	guu3TA086qniuwgGkytIo49LMHk9PGO6EoU09vwY	Augustus, Joella Devon	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Joella	Devon	Augustus	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
179	xkO5Umv7TjxiZY8CD926MY27uoz1edQFcaWU76Ef	M	White	N	N	N	N	N	Y	N	N	N	Y	Y	N	Y	N	TBI	XnZIh0HNVZlrvHTGqktw65fQoXE0vhqx9Fuw6m5c	luBxIsccfBXZHL8L0uVIQbUARWMPF2lmP7Wrphsw	Benevides, Roman Chase	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Roman	Chase	Benevides	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
132	xkO5Umv7TjxiZY8CD926MY27uoz1edQFcaWU76Ef	M	White	N	N	N	N	N	Y	N	N	N	Y	Y	N	Y	N	TBI	XnZIh0HNVZlrvHTGqktw65fQoXE0vhqx9Fuw6m5c	luBxIsccfBXZHL8L0uVIQbUARWMPF2lmP7Wrphsw	Benevides, Roman Chase	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Roman	Chase	Benevides	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
180	xro1vzHRQAemvh8tb1iV4F2Yr9Tf5BmZI5F1UzlJ	F	Two or more races	N	N	N	N	N	N	N	Y	N	Y	Y	N	N	Y	ID	GGmeXdW19rMZdUjKO7JmNuaDAk5GiejNN9KJvzsS	oaCkXIjHi49EZBU7ZeOtCfuGW9daniTsyw48CL6y	Gatti, Lois Cornelius	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Lois	Cornelius	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
35	xro1vzHRQAemvh8tb1iV4F2Yr9Tf5BmZI5F1UzlJ	F	Two or more races	N	N	N	N	N	N	N	Y	N	Y	Y	N	N	Y	ID	GGmeXdW19rMZdUjKO7JmNuaDAk5GiejNN9KJvzsS	oaCkXIjHi49EZBU7ZeOtCfuGW9daniTsyw48CL6y	Gatti, Lois Cornelius	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Lois	Cornelius	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
181	ZHBxgfZVT3KsM8I8iun3007MOqKZxIvz5HZht1dR	F	Native Hawaiian or other Pacific Islander	N	N	N	N	Y	N	N	N	Y	N	Y	N	Y	Y	DB	ctmnlflHqEhxo8WAg7Q5hXXWMDDlnDUjxSnImvfb	UFUbTvBK41IVxzQEYj710qfAyhHSaq1YVRV9DxD2	Mantle, Kathe Sandy	0002-09-05	2	2015-09-15 07:56:29.534211	2199-12-31 23:59:59.999	Kathe	Sandy	Mantle	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
133	ZHBxgfZVT3KsM8I8iun3007MOqKZxIvz5HZht1dR	F	Native Hawaiian or other Pacific Islander	N	N	N	N	Y	N	N	N	Y	N	Y	N	Y	Y	DB	ctmnlflHqEhxo8WAg7Q5hXXWMDDlnDUjxSnImvfb	UFUbTvBK41IVxzQEYj710qfAyhHSaq1YVRV9DxD2	Mantle, Kathe Sandy	0009-02-05	1	1900-01-01 00:00:00	2015-09-15 07:56:29.534211	Kathe	Sandy	Mantle	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
182	CYh72rsxfT7A4jmupxUkrb3S76bTra2BmlgrR57F	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Pando, Joella Marisela	0002-09-05	1	1900-01-01 00:00:00	2199-12-31 23:59:59.999	Joella	Marisela	Pando	dcdaa08b9b9046beb3e77419481c860bc6598e	R0003	\N	\N	\N
183	D3de8PGASi2QhIZ0zHLSNJXZI0okSm1VqPJMeJAw	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Matsuura, Roman Marisela	0002-09-05	1	1900-01-01 00:00:00	2199-12-31 23:59:59.999	Roman	Marisela	Matsuura	5f3bcce2cac34dd38578b9d423801dc3b5caf6	R0003	\N	\N	\N
184	5336b7258eb545b3ae7178aeb0cfd72428a36fdb	M	American Indian or Alaska Native	N	Y	N	N	N	N	N	N	Y	Y	Y	N	Y	N	OI	a64dac1816204195a4759761cf1b2715eaa26359	a91eb32222844401a74296ae3b692b71b536d8f8	Brule, Steve Jan	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Steve	Jan	Brule	5f3bcce2cac34dd38578b9d423801dc3b5caf6	R0001	\N	\N	\N
185	MXGplB4M232z2j4FNTStjIAKRER2ujJ8Z2OlV7Yn	F	Asian	N	N	Y	N	N	N	N	N	Y	Y	N	Y	N	Y	OI	xK2Faci3f8jeHK0T5ctKCG9m8qtFs0CUS92459TJ	rBQcB1GLoy5fdMUfjBX7cmKz3xKluOBGdsJL5DDn	Parr, Joella Betty	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Joella	Betty	Parr	5f3bcce2cac34dd38578b9d423801dc3b5caf6	R0001	\N	\N	\N
155	c0pT5dPt1QuTicyE2IWo13jRf8kh2ep7ZI8flg9g	F	Asian	N	N	Y	N	N	N	N	N	Y	Y	Y	N	Y	Y	EMN	UeDU72FXhOsN87hmLVrvg2axM0ew777BXwUfOMQi	3ZnniDJxEkJJud7n9G1N5inYdTaqvPSqp60jxBHx	Augustus, Betty Senaida	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Betty	Senaida	Augustus	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
198	e3OGNkIbM4h2skSI4nPuESDwrF1EMmjIfrprGMHz	M	Asian	N	N	Y	N	N	N	N	N	Y	Y	Y	N	Y	Y	AUT	TKmqHN7XOP5W3NPKNiDZZEdusLdlWLO8Ggpiwvh7	KQp8atYRrBdUzeyGT0DbWG8VMX9vwOZh357WUTZV	Pedone, Verena Sandy	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Verena	Sandy	Pedone	ded3479fc4504921ac1cf878b8c5d015efffc1	R0003	\N	\N	\N
157	e3OGNkIbM4h2skSI4nPuESDwrF1EMmjIfrprGMHz	M	Asian	N	N	Y	N	N	N	N	N	Y	Y	Y	N	Y	Y	AUT	TKmqHN7XOP5W3NPKNiDZZEdusLdlWLO8Ggpiwvh7	KQp8atYRrBdUzeyGT0DbWG8VMX9vwOZh357WUTZV	Pedone, Verena Sandy	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Verena	Sandy	Pedone	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
199	hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ	F	Black or African American	N	N	N	Y	N	N	N	N	Y	N	N	N	Y	N	DB	Tvw1NS4wwtZ4tkwb0iVisHEi7Ry715pRRjGtbL1r	mvxrV6SfCFq11d05bN01KupSnan42G0QEJyFwwO5	Forst, Jacquline Zofia	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Jacquline	Zofia	Forst	3c6b9327b0484f5f84741b204a6c9f02208bc1	R0003	\N	\N	\N
162	KL3R8L1EaO8MgymDcMI59fe7DnjLk0ZuDZiwO2J2	F	Two or more races	N	N	N	N	N	N	N	Y	Y	N	Y	Y	Y	Y	AUT	YF1IdV4ztKcjrlJlJkwf1fqIg8yuj2xn9ESdUdsV	WX95D3DMB8lxa2WOd0oFWiUvcMsat1t5gLC8yJTU	Gatti, Sandy Verena	0002-09-05	2	2015-09-15 07:56:29.534211	2015-09-15 07:56:37.259892	Sandy	Verena	Gatti	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
201	oBPTLwB89Yetjpa88VqxOvlIENOqe4XKGKnOrJfM	F	White	N	N	N	N	N	Y	N	N	N	Y	N	N	N	N	SLD	yLxhxvhJpqKtF1L4Ofz0cSs4BPJh4abTqcZbQyqd	RQOAUp2l6bdfAKIPIfnyW5DxXbFJxeLqRDTCHepM	Matsuura, Orval Barbar	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Orval	Barbar	Matsuura	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
166	oBPTLwB89Yetjpa88VqxOvlIENOqe4XKGKnOrJfM	F	White	N	N	N	N	N	Y	N	N	N	Y	N	N	N	N	SLD	yLxhxvhJpqKtF1L4Ofz0cSs4BPJh4abTqcZbQyqd	RQOAUp2l6bdfAKIPIfnyW5DxXbFJxeLqRDTCHepM	Matsuura, Orval Barbar	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Orval	Barbar	Matsuura	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
202	QitKZjIhO0SovQYzrO9PfV1o1b0P0xRtKwJwFRqc	F	Black or African American	N	N	N	Y	N	N	N	N	N	Y	N	N	Y	N	SLI	CZIdF9IWXP4VxKMRmyhuMH3RrbLuzlUQHcNpeiui	RAwF4ar58LF7U1lAoUhrunhyGSrKowNmH5IHEaVX	Pedone, Winona Darci	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Winona	Darci	Pedone	343fe3a084b2422796dbd7533a68de13c3cf37	R0003	\N	\N	\N
168	QitKZjIhO0SovQYzrO9PfV1o1b0P0xRtKwJwFRqc	F	Black or African American	N	N	N	Y	N	N	N	N	N	Y	N	N	Y	N	SLI	CZIdF9IWXP4VxKMRmyhuMH3RrbLuzlUQHcNpeiui	RAwF4ar58LF7U1lAoUhrunhyGSrKowNmH5IHEaVX	Pedone, Winona Darci	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Winona	Darci	Pedone	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
203	SY0CEikzlomxMRW6NsjHhNuLpK17yruc5pjGavX8	M	Asian	N	N	Y	N	N	N	N	N	N	Y	N	N	N	N	AUT	XFnox5kaJRbysJtAmQHxOLaKWJvPvzQgkz3pv0KK	CaYqEwjoL1rAeX2BVmrVwhjXiORRRnxWWYbgKINv	Pando, Joella Vincent	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Joella	Vincent	Pando	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
170	SY0CEikzlomxMRW6NsjHhNuLpK17yruc5pjGavX8	M	Asian	N	N	Y	N	N	N	N	N	N	Y	N	N	N	N	AUT	XFnox5kaJRbysJtAmQHxOLaKWJvPvzQgkz3pv0KK	CaYqEwjoL1rAeX2BVmrVwhjXiORRRnxWWYbgKINv	Pando, Joella Vincent	0002-09-05	1	1900-01-01 00:00:00	2015-09-15 07:56:37.259892	Joella	Vincent	Pando	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
204	w7TmiOs6CWJsXS2l1crivMsf42ZlQ5WAr6Xri2rd	M	Black or African American	N	N	N	Y	N	N	N	N	N	Y	N	Y	Y	N	VI	D1AXqIoaXx8AQTaQoumsfu5xeX4aGzCRrecFKecG	JaT2z61Ejzjz9rFhwN0ePthYx9cLsIJJPGYoHeLY	Cabello, Becki Lois	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Becki	Lois	Cabello	0a4d255f40644147aaffbcb10fef0d908e8bd9	R0003	\N	\N	\N
205	wUfuK1C1vpevOq8cV6oqQxNNwJV1TbzeraL8fC4v	M	White	N	N	N	N	N	Y	N	N	N	Y	N	Y	Y	N	SLD	SYKyRcyh4S0npwfeCEDSXEcNeevVBGyI1cUZ7JBa	QA9WHJjldT89J8LHuhwDFqG4aVG9MKz4pBxkHNKQ	Mccreery, Sandy Devon	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Sandy	Devon	Mccreery	3c6b9327b0484f5f84741b204a6c9f02208bc1	R0003	\N	\N	\N
206	zxITiM66QUVOcTyhRaooB0UIOWemGFP8eQE7xPSx	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	N	Y	N	N	Y	SLI	6oZAVILFTfpG9poJ4qFAJOvw0XYa9suCqZPc2Jkf	UZJGBAKBhDPPLpfVCc225qzGY4J1Ei8vjBnnFwh6	Mccreery, Zofia Keshia	9999-01-01	2	2015-09-15 07:56:37.259892	2199-12-31 23:59:59.999	Zofia	Keshia	Mccreery	5f3bcce2cac34dd38578b9d423801dc3b5caf6	R0003	\N	\N	\N
207	OIeWicvTvk5kcbEc0qj7vPo2nh3Vr8XS9e2Vz57b	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	N	N	Y	N	N	N	TBI	rjmYkBa0yZxWg60UvrOgMyxiQYCWQVd8bXaeCNeS	t1lMZ0W1BM0RoHefhKPLNdUr38uYXTo4yOqxU10F	Perla, Barbar Orval	0002-09-05	2	2015-09-15 07:56:41.133869	2199-12-31 23:59:59.999	Barbar	Orval	Perla	dcdaa08b9b9046beb3e77419481c860bc6598e	R0001	\N	\N	\N
208	SBFMQrkvsLNLQWnEXVVeN6ZMQXqemPhTY44hgO2M	M	Black or African American	N	N	N	Y	N	N	N	N	N	Y	Y	N	N	N	SLI	S6RWxM4aZT5T28sUstXnoV75JDnsTtmYXcgTv1k0	IdBEiFs4LSIhyz2lQ29b7WM6IxS2z5SXOYAMfDfy	Mccreery, Jacquline Becki	0002-09-05	2	2015-09-15 07:56:41.133869	2199-12-31 23:59:59.999	Jacquline	Becki	Mccreery	5f3bcce2cac34dd38578b9d423801dc3b5caf6	R0001	\N	\N	\N
209	5RsVRnISVN942m5YIo50ucTxyB8cYNObQ1tnrORJ	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Mccreery, Sandy Marisela	0002-09-05	2	2015-09-15 07:56:41.133869	2199-12-31 23:59:59.999	Sandy	Marisela	Mccreery	5f3bcce2cac34dd38578b9d423801dc3b5caf6	R0003	\N	\N	\N
210	FkgQ5OyZAMUDgnC8f6cJHnpLkzOlGV6xnArV3Bhf	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Augustus, Orval Marisela	0002-09-05	1	1900-01-01 00:00:00	2199-12-31 23:59:59.999	Orval	Marisela	Augustus	0a4061fcdd3f4e6ea8db682199fee2757e8275	R0003	\N	\N	\N
211	GFWLlMJ65UE7pSrpAPrt2Tu5hoZkaeJdY0mrbTTU	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Cabello, Darci Marisela	0002-09-05	1	1900-01-01 00:00:00	2199-12-31 23:59:59.999	Darci	Marisela	Cabello	343fe3a084b2422796dbd7533a68de13c3cf37	R0003	\N	\N	\N
212	GXr7X73TjgGmTEm62Q3cGYSMBHE34YWvhwJEalyy	F	Hispanic or Latino	Y	N	N	N	N	N	N	N	Y	N	N	N	Y	N	VI	yLuqbEc8yrkg3ThKQgze4Am0jF7OXhnJZCFNbw8q	obEJXPqoQ1I2Eo8TD3G1vEPa5JoHuUpua7gIzMe1	Pedone, Shane Marisela	0002-09-05	2	2015-09-15 07:56:41.133869	2199-12-31 23:59:59.999	Shane	Marisela	Pedone	dcdaa08b9b9046beb3e77419481c860bc6598e	R0003	\N	\N	\N
\.


--
-- Data for Name: dim_test; Type: TABLE DATA; Schema: analytics; Owner: edware
--

COPY dim_test (_key_test, form_id, test_category, test_subject, asmt_grade, test_code, _valid_from, _valid_to, _record_version, pba_form_id, eoy_form_id, pba_category, eoy_category, district_id, _hash_test, state_id) FROM stdin;
0	NA	NA	UNKNOWN	UNKNOWN	UNKNOWN	1900-01-01	2199-12-31	1	\N	\N	\N	\N	\N	\N	\N
1	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	dgIVb9Wq9odHcW	-	A	R0003	\N	\N
2	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	fYO3TpPiFUUiTO	-	G	R0001	\N	\N
3	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	JeuRkUYEfSuHmx	-	A	R0003	\N	\N
4	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	4xWJsoJUwiPGPf	-	-	R0003	\N	\N
5	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	99HW9Xo91d10zv	-	-	R0002	\N	\N
6	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	X95m0tcJUSH84v	-	-	R0003	\N	\N
7	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	juNh333YKgldJG	-	-	R0002	\N	\N
8	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	y00dd73aY0qhrW	A	-	R0003	\N	\N
9	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	h2gwsqqE7MIPUV	-	-	R0002	\N	\N
10	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	8eUxb3zcwHy99N	-	-	R0003	\N	\N
11	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	VYpRCNdHzNTPpz	-	-	R0003	\N	\N
12	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	0KTO8622azLJFE	-	-	R0002	\N	\N
13	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	m35FZzH2KHEiJX	-	-	R0001	\N	\N
14	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	HAd8BlYQGitg8U	-	-	R0003	\N	\N
15	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	yZDhUH2pTTE84i	-	B	R0002	\N	\N
16	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	CkNKqug4Mg6eDO	-	-	R0003	\N	\N
17	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	XIcAeP7q4vw1b2	D	-	R0003	\N	\N
18	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	vid5XD74AUpgys	-	-	R0003	\N	\N
19	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	2ubQweDBHNXqdn	-	-	R0003	\N	\N
20	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	uEYgB8WD2tFK1d	A	-	R0003	\N	\N
21	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	YtieYGWwKeBVpg	-	A	R0003	\N	\N
22	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	DntjhMHdfJ9nnw	-	-	R0003	\N	\N
23	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	wMf8DWYieBflI6	-	-	R0003	\N	\N
24	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	dfLGkXcrVdl1M0	-	-	R0003	\N	\N
25	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	78ZCEiQxcZC9bS	-	-	R0003	\N	\N
26	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	igOBSdxwH5ANr4	-	F	R0003	\N	\N
27	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	NjHIDXqnbah9ve	-	-	R0002	\N	\N
28	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	VklM6gM8JHFsFa	A	-	R0002	\N	\N
29	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	Stw9UUc8h816xD	-	-	R0001	\N	\N
30	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	6Yt24ZBy7naqGh	E	-	R0003	\N	\N
31	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	4lAGHJHnE5fQtH	-	-	R0003	\N	\N
32	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	v7WbCtVelPK6II	-	D	R0003	\N	\N
33	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	WNcvkVWBJi3ryg	-	-	R0003	\N	\N
34	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	HxNaJqf5gIe436	A	-	R0003	\N	\N
35	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	n3nADGgGpc9piT	D	-	R0001	\N	\N
36	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	ObSSHpCkdfCRHN	D	-	R0003	\N	\N
37	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	6DmamWynStLmln	-	-	R0001	\N	\N
38	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	rMPpBMUSE9aC8p	-	-	R0001	\N	\N
39	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	RZx5igwCzVBDDc	F	-	R0003	\N	\N
40	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	xpKDslwonfX1FY	A	-	R0003	\N	\N
41	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	wMf8DWYieBflI6	-	-	R0003	\N	\N
42	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	dfLGkXcrVdl1M0	-	-	R0003	\N	\N
43	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	2BXMMHcWQfG0xm	-	-	R0003	\N	\N
44	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	XTnbnfj14uiA7W	B	-	R0002	\N	\N
45	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	rBd8kkGe5TjMj3	-	B	R0001	\N	\N
46	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	RhPb6HcQH8TAj1	-	-	R0002	\N	\N
47	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	iBfO8koraM3ZXw	-	-	R0003	\N	\N
48	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	ooGWuBoxjHUQXZ	-	-	R0003	\N	\N
49	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	YH4dwgUYBUswsT	A	-	R0003	\N	\N
50	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	fGegQ2t8WJsYaa	-	-	R0002	\N	\N
51	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	FzaEWlQ6hoUp3y	-	-	R0001	\N	\N
52	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	KDVzGBvh8ZmKie	-	-	R0003	\N	\N
53	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	EEia4QrPiFHKBu	F	-	R0003	\N	\N
54	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	Q0tVkYBC9M3yRK	C	-	R0002	\N	\N
55	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	ubzXHZx99oLB9A	-	-	R0003	\N	\N
56	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	NhVWgd3y4uitUo	-	-	R0001	\N	\N
57	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	2NZOBxAbkkmJYh	-	-	R0002	\N	\N
58	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	6Pn5kCRK42YQhi	-	-	R0003	\N	\N
59	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	SSwvnInaL4W5Zl	-	-	R0002	\N	\N
60	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	S84rkBC7IlGalJ	A	-	R0003	\N	\N
61	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	SNFAoTmQVAcPlX	-	-	R0001	\N	\N
62	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	bmzzDt44Kjctdj	-	E	R0001	\N	\N
63	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	ho4E8YYU72zMsS	-	-	R0003	\N	\N
64	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	ivz8VxujtbLFz7	-	E	R0003	\N	\N
65	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	sITIkaJdbq32oy	-	-	R0003	\N	\N
66	NA	NA	English Language Arts/Literacy	11	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	NFwIta5foeLUse	A	-	R0003	\N	\N
67	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	YM1VtBJ7AxJpZY	-	-	R0001	\N	\N
68	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	NkEOfEaOoenQTL	-	-	R0003	\N	\N
69	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	oZNn0gw72lwDLD	-	-	R0003	\N	\N
70	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	1yum7wlYtgEza0	-	-	R0001	\N	\N
71	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	XptuHcZNTUJpBF	-	-	R0001	\N	\N
72	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	ikWoOoyJZIxIsd	-	-	R0002	\N	\N
73	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	f2DBdiAoCdacEf	A	-	R0002	\N	\N
74	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	GyEt1xFmawzM7N	-	-	R0001	\N	\N
75	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	QDW5k4mAqA7jVn	-	-	R0002	\N	\N
76	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	1rsDnBUUyYCOAU	-	C	R0003	\N	\N
77	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	eZGuWpesLwOYPR	-	-	R0003	\N	\N
78	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	FRBpRbJEp0zI8s	A	-	R0001	\N	\N
79	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	KuE963evgXBHzE	-	-	R0002	\N	\N
80	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	QO80Y8Jm4LKCWZ	-	-	R0003	\N	\N
81	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	2v1EFEXijCesqg	-	-	R0003	\N	\N
82	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	hA8JYMjU1tJjtu	-	-	R0003	\N	\N
83	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	hP1OwWKiVDcfHj	B	-	R0002	\N	\N
84	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	7wfBudLKnThOjR	-	-	R0003	\N	\N
85	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	441Ab8l1eKHw0K	-	-	R0001	\N	\N
86	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	3HzSa2cH648Vvd	-	-	R0003	\N	\N
87	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	e6z7FjVMf1ok4O	A	-	R0003	\N	\N
88	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	JqEIPV2TiEjKbD	-	-	R0003	\N	\N
89	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	Hc6RedkXlpduBi	-	F	R0003	\N	\N
90	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	wuHZcgBB9vDqNK	H	-	R0001	\N	\N
91	NA	NA	English Language Arts/Literacy	09	ELA09	1900-01-01	2199-12-31	1	UNKNOWN	RgDEVYZCMMgOgp	E	-	R0003	\N	\N
92	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	K9JzaGPABAyBIX	-	-	R0003	\N	\N
93	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	XdyFVicnC7pKvY	-	B	R0002	\N	\N
94	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	OfvRXhwXjea3ML	-	F	R0001	\N	\N
95	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	4OHlYZTfZeefES	-	-	R0003	\N	\N
96	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	1EA63i1ZVCXIYD	-	A	R0003	\N	\N
97	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	tNL2RskJunn1qo	C	-	R0003	\N	\N
98	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	7CktgeMR2WPbPf	C	-	R0001	\N	\N
99	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	LHNkKbXoSCTxpt	-	-	R0003	\N	\N
100	NA	NA	English Language Arts/Literacy	10	ELA10	1900-01-01	2199-12-31	1	UNKNOWN	pjCfEb4D1TobqR	-	-	R0003	\N	\N
101	NA	NA	English Language Arts/Literacy	03	ELA03	1900-01-01	2199-12-31	1	UNKNOWN	1yum7wlYtgEza0	-	-	R0001	\N	\N
102	NA	NA	English Language Arts/Literacy	11	ELA11	1900-01-01	2199-12-31	1	UNKNOWN	LznOLa9CLZFs5v	A	-	R0002	\N	\N
103	NA	NA	Algebra I	UNKNOWN	ALG01	1900-01-01	2199-12-31	1	UNKNOWN	f2DBdiAoCdacEf	A	-	R0002	\N	\N
104	NA	NA	Mathematics	03	MAT03	1900-01-01	2199-12-31	1	UNKNOWN	12TIkaJdbq32oy	-	-	R0003	\N	\N
105	NA	NA	Integrated Mathematics II	UNKNOWN	MAT2I	1900-01-01	2199-12-31	1	UNKNOWN	RgDEVYZCMMgOgp	E	-	R0003	\N	\N
106	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	xpKDslwonfX1FY	A	-	R0003	\N	\N
107	NA	NA	Mathematics	05	MAT05	1900-01-01	2199-12-31	1	UNKNOWN	6ITIkaJdbq32oy	-	-	R0003	\N	\N
108	NA	NA	Algebra II	UNKNOWN	ALG02	1900-01-01	2199-12-31	1	UNKNOWN	RhPb6HcQH8TAj1	-	-	R0002	\N	\N
109	NA	NA	Geometry	UNKNOWN	GEO01	1900-01-01	2199-12-31	1	UNKNOWN	NkEOfEaOoenQTL	-	-	R0003	\N	\N
110	NA	NA	Integrated Mathematics I	UNKNOWN	MAT1I	1900-01-01	2199-12-31	1	UNKNOWN	6Pn5kCRK42YQhi	-	-	R0003	\N	\N
111	NA	NA	Algebra I	UNKNOWN	ALG01	1900-01-01	2199-12-31	1	UNKNOWN	YM1VtBJ7AxJpZY	-	-	R0001	\N	\N
112	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	4lAGHJHnE5fQtH	-	-	R0003	\N	\N
113	NA	NA	Algebra II	UNKNOWN	ALG02	1900-01-01	2199-12-31	1	UNKNOWN	yZDhUH2pTTE84i	-	B	R0002	\N	\N
114	NA	NA	Geometry	UNKNOWN	GEO01	1900-01-01	2199-12-31	1	UNKNOWN	8eUxb3zcwHy99N	-	-	R0003	\N	\N
115	NA	NA	Algebra II	UNKNOWN	ALG02	1900-01-01	2199-12-31	1	UNKNOWN	2NZOBxAbkkmJYh	-	-	R0002	\N	\N
116	NA	NA	Integrated Mathematics I	UNKNOWN	MAT1I	1900-01-01	2199-12-31	1	UNKNOWN	iBfO8koraM3ZXw	-	-	R0003	\N	\N
117	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	dgIVb9Wq9odHcW	-	A	R0003	\N	\N
118	NA	NA	Algebra I	UNKNOWN	ALG01	1900-01-01	2199-12-31	1	UNKNOWN	NjHIDXqnbah9ve	-	-	R0002	\N	\N
119	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	4xWJsoJUwiPGPf	-	-	R0003	\N	\N
120	NA	NA	Algebra I	UNKNOWN	ALG01	1900-01-01	2199-12-31	1	UNKNOWN	rMPpBMUSE9aC8p	-	-	R0001	\N	\N
121	NA	NA	Mathematics	08	MAT08	1900-01-01	2199-12-31	1	UNKNOWN	14TIkaJdbq32oy	-	-	R0003	\N	\N
122	NA	NA	Algebra I	UNKNOWN	ALG01	1900-01-01	2199-12-31	1	UNKNOWN	SNFAoTmQVAcPlX	-	-	R0001	\N	\N
123	NA	NA	Integrated Mathematics I	UNKNOWN	MAT1I	1900-01-01	2199-12-31	1	UNKNOWN	ooGWuBoxjHUQXZ	-	-	R0003	\N	\N
124	NA	NA	Algebra I	UNKNOWN	ALG01	1900-01-01	2199-12-31	1	UNKNOWN	wuHZcgBB9vDqNK	H	-	R0001	\N	\N
125	NA	NA	Algebra II	UNKNOWN	ALG02	1900-01-01	2199-12-31	1	UNKNOWN	QDW5k4mAqA7jVn	-	-	R0002	\N	\N
126	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	HAd8BlYQGitg8U	-	-	R0003	\N	\N
127	NA	NA	Mathematics	03	MAT03	1900-01-01	2199-12-31	1	UNKNOWN	s43IkaJdbq32oy	-	-	R0003	\N	\N
128	NA	NA	Integrated Mathematics II	UNKNOWN	MAT2I	1900-01-01	2199-12-31	1	UNKNOWN	6Yt24ZBy7naqGh	E	-	R0003	\N	\N
129	NA	NA	Geometry	UNKNOWN	GEO01	1900-01-01	2199-12-31	1	UNKNOWN	v7WbCtVelPK6II	-	D	R0003	\N	\N
130	NA	NA	Algebra I	UNKNOWN	ALG01	1900-01-01	2199-12-31	1	UNKNOWN	VklM6gM8JHFsFa	A	-	R0002	\N	\N
131	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	JqEIPV2TiEjKbD	-	-	R0003	\N	\N
132	NA	NA	Mathematics	08	MAT08	1900-01-01	2199-12-31	1	UNKNOWN	nITIkaJdbq32oy	-	-	R0003	\N	\N
133	NA	NA	Algebra II	UNKNOWN	ALG02	1900-01-01	2199-12-31	1	UNKNOWN	99HW9Xo91d10zv	-	-	R0002	\N	\N
134	NA	NA	Mathematics	03	MAT03	1900-01-01	2199-12-31	1	UNKNOWN	yITIkaJdbq32oy	-	-	R0003	\N	\N
135	NA	NA	Geometry	UNKNOWN	GEO01	1900-01-01	2199-12-31	1	UNKNOWN	1rsDnBUUyYCOAU	-	C	R0003	\N	\N
136	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	y00dd73aY0qhrW	A	-	R0003	\N	\N
137	NA	NA	Algebra I	UNKNOWN	ALG01	1900-01-01	2199-12-31	1	UNKNOWN	FRBpRbJEp0zI8s	A	-	R0001	\N	\N
138	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	1EA63i1ZVCXIYD	-	A	R0003	\N	\N
139	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	YH4dwgUYBUswsT	A	-	R0003	\N	\N
140	NA	NA	Integrated Mathematics I	UNKNOWN	MAT1I	1900-01-01	2199-12-31	1	UNKNOWN	hA8JYMjU1tJjtu	-	-	R0003	\N	\N
141	NA	NA	Algebra I	UNKNOWN	ALG01	1900-01-01	2199-12-31	1	UNKNOWN	fYO3TpPiFUUiTO	-	G	R0001	\N	\N
142	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	wMf8DWYieBflI6	-	-	R0003	\N	\N
143	NA	NA	Geometry	UNKNOWN	GEO01	1900-01-01	2199-12-31	1	UNKNOWN	FzaEWlQ6hoUp3y	-	-	R0001	\N	\N
144	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	dfLGkXcrVdl1M0	-	-	R0003	\N	\N
145	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	WNcvkVWBJi3ryg	-	-	R0003	\N	\N
146	NA	NA	Algebra I	UNKNOWN	ALG01	1900-01-01	2199-12-31	1	UNKNOWN	m35FZzH2KHEiJX	-	-	R0001	\N	\N
147	NA	NA	Algebra II	UNKNOWN	ALG02	1900-01-01	2199-12-31	1	UNKNOWN	ikWoOoyJZIxIsd	-	-	R0002	\N	\N
148	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	igOBSdxwH5ANr4	-	F	R0003	\N	\N
149	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	DntjhMHdfJ9nnw	-	-	R0003	\N	\N
150	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	NFwIta5foeLUse	A	-	R0003	\N	\N
151	NA	NA	Algebra II	UNKNOWN	ALG02	1900-01-01	2199-12-31	1	UNKNOWN	Q0tVkYBC9M3yRK	C	-	R0002	\N	\N
152	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	HxNaJqf5gIe436	A	-	R0003	\N	\N
153	NA	NA	Mathematics	08	MAT08	1900-01-01	2199-12-31	1	UNKNOWN	rBd8kkGe5TjMj3	-	B	R0001	\N	\N
154	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	2BXMMHcWQfG0xm	-	-	R0003	\N	\N
155	NA	NA	Algebra I	UNKNOWN	ALG01	1900-01-01	2199-12-31	1	UNKNOWN	Stw9UUc8h816xD	-	-	R0001	\N	\N
156	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	S84rkBC7IlGalJ	A	-	R0003	\N	\N
157	NA	NA	Algebra II	UNKNOWN	ALG02	1900-01-01	2199-12-31	1	UNKNOWN	SSwvnInaL4W5Zl	-	-	R0002	\N	\N
158	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	78ZCEiQxcZC9bS	-	-	R0003	\N	\N
159	NA	NA	Integrated Mathematics I	UNKNOWN	MAT1I	1900-01-01	2199-12-31	1	UNKNOWN	K9JzaGPABAyBIX	-	-	R0003	\N	\N
160	NA	NA	Integrated Mathematics II	UNKNOWN	MAT2I	1900-01-01	2199-12-31	1	UNKNOWN	ObSSHpCkdfCRHN	D	-	R0003	\N	\N
161	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	uEYgB8WD2tFK1d	A	-	R0003	\N	\N
162	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	zITIkaJdbq32oy	-	-	R0003	\N	\N
163	NA	NA	Mathematics	08	MAT08	1900-01-01	2199-12-31	1	UNKNOWN	7ITIkaJdbq32oy	-	-	R0003	\N	\N
164	NA	NA	Integrated Mathematics II	UNKNOWN	MAT2I	1900-01-01	2199-12-31	1	UNKNOWN	eZGuWpesLwOYPR	-	-	R0003	\N	\N
165	NA	NA	Algebra II	UNKNOWN	ALG02	1900-01-01	2199-12-31	1	UNKNOWN	LznOLa9CLZFs5v	A	-	R0002	\N	\N
166	NA	NA	Algebra II	UNKNOWN	ALG02	1900-01-01	2199-12-31	1	UNKNOWN	XTnbnfj14uiA7W	B	-	R0002	\N	\N
167	NA	NA	Geometry	UNKNOWN	GEO01	1900-01-01	2199-12-31	1	UNKNOWN	2ubQweDBHNXqdn	-	-	R0003	\N	\N
168	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	sITIkaJdbq32oy	-	-	R0003	\N	\N
169	NA	NA	Algebra I	UNKNOWN	ALG01	1900-01-01	2199-12-31	1	UNKNOWN	OfvRXhwXjea3ML	-	F	R0001	\N	\N
170	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	EEia4QrPiFHKBu	F	-	R0003	\N	\N
171	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	e6z7FjVMf1ok4O	A	-	R0003	\N	\N
172	NA	NA	Algebra II	UNKNOWN	ALG02	1900-01-01	2199-12-31	1	UNKNOWN	fGegQ2t8WJsYaa	-	-	R0002	\N	\N
173	NA	NA	Algebra II	UNKNOWN	ALG02	1900-01-01	2199-12-31	1	UNKNOWN	h2gwsqqE7MIPUV	-	-	R0002	\N	\N
174	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	yITIkaJdbq32oy	-	-	R0003	\N	\N
175	NA	NA	Geometry	UNKNOWN	GEO01	1900-01-01	2199-12-31	1	UNKNOWN	n3nADGgGpc9piT	D	-	R0001	\N	\N
176	NA	NA	Integrated Mathematics I	UNKNOWN	MAT1I	1900-01-01	2199-12-31	1	UNKNOWN	7wfBudLKnThOjR	-	-	R0003	\N	\N
177	NA	NA	Algebra I	UNKNOWN	ALG01	1900-01-01	2199-12-31	1	UNKNOWN	1yum7wlYtgEza0	-	-	R0001	\N	\N
178	NA	NA	Algebra I	UNKNOWN	ALG01	1900-01-01	2199-12-31	1	UNKNOWN	XptuHcZNTUJpBF	-	-	R0001	\N	\N
179	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	2v1EFEXijCesqg	-	-	R0003	\N	\N
180	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	JeuRkUYEfSuHmx	-	A	R0003	\N	\N
181	NA	NA	Integrated Mathematics II	UNKNOWN	MAT2I	1900-01-01	2199-12-31	1	UNKNOWN	vid5XD74AUpgys	-	-	R0003	\N	\N
182	NA	NA	Geometry	UNKNOWN	GEO01	1900-01-01	2199-12-31	1	UNKNOWN	XIcAeP7q4vw1b2	D	-	R0003	\N	\N
183	NA	NA	Geometry	UNKNOWN	GEO01	1900-01-01	2199-12-31	1	UNKNOWN	CkNKqug4Mg6eDO	-	-	R0003	\N	\N
184	NA	NA	Mathematics	08	MAT08	1900-01-01	2199-12-31	1	UNKNOWN	zITIkaJdbq32oy	-	-	R0003	\N	\N
185	NA	NA	Integrated Mathematics I	UNKNOWN	MAT1I	1900-01-01	2199-12-31	1	UNKNOWN	oZNn0gw72lwDLD	-	-	R0003	\N	\N
186	NA	NA	Integrated Mathematics I	UNKNOWN	MAT1I	1900-01-01	2199-12-31	1	UNKNOWN	ubzXHZx99oLB9A	-	-	R0003	\N	\N
187	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	4OHlYZTfZeefES	-	-	R0003	\N	\N
188	NA	NA	Geometry	UNKNOWN	GEO01	1900-01-01	2199-12-31	1	UNKNOWN	7CktgeMR2WPbPf	C	-	R0001	\N	\N
189	NA	NA	Algebra II	UNKNOWN	ALG02	1900-01-01	2199-12-31	1	UNKNOWN	hP1OwWKiVDcfHj	B	-	R0002	\N	\N
190	NA	NA	Integrated Mathematics II	UNKNOWN	MAT2I	1900-01-01	2199-12-31	1	UNKNOWN	LHNkKbXoSCTxpt	-	-	R0003	\N	\N
191	NA	NA	Algebra I	UNKNOWN	ALG01	1900-01-01	2199-12-31	1	UNKNOWN	6DmamWynStLmln	-	-	R0001	\N	\N
192	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	YtieYGWwKeBVpg	-	A	R0003	\N	\N
193	NA	NA	Integrated Mathematics II	UNKNOWN	MAT2I	1900-01-01	2199-12-31	1	UNKNOWN	VYpRCNdHzNTPpz	-	-	R0003	\N	\N
194	NA	NA	Algebra I	UNKNOWN	ALG01	1900-01-01	2199-12-31	1	UNKNOWN	0KTO8622azLJFE	-	-	R0002	\N	\N
195	NA	NA	Integrated Mathematics II	UNKNOWN	MAT2I	1900-01-01	2199-12-31	1	UNKNOWN	RZx5igwCzVBDDc	F	-	R0003	\N	\N
196	NA	NA	Geometry	UNKNOWN	GEO01	1900-01-01	2199-12-31	1	UNKNOWN	juNh333YKgldJG	-	-	R0002	\N	\N
197	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	KDVzGBvh8ZmKie	-	-	R0003	\N	\N
198	NA	NA	Geometry	UNKNOWN	GEO01	1900-01-01	2199-12-31	1	UNKNOWN	GyEt1xFmawzM7N	-	-	R0001	\N	\N
199	NA	NA	Integrated Mathematics I	UNKNOWN	MAT1I	1900-01-01	2199-12-31	1	UNKNOWN	ho4E8YYU72zMsS	-	-	R0003	\N	\N
200	NA	NA	Algebra I	UNKNOWN	ALG01	1900-01-01	2199-12-31	1	UNKNOWN	bmzzDt44Kjctdj	-	E	R0001	\N	\N
201	NA	NA	Algebra II	UNKNOWN	ALG02	1900-01-01	2199-12-31	1	UNKNOWN	KuE963evgXBHzE	-	-	R0002	\N	\N
202	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	QO80Y8Jm4LKCWZ	-	-	R0003	\N	\N
203	NA	NA	Geometry	UNKNOWN	GEO01	1900-01-01	2199-12-31	1	UNKNOWN	pjCfEb4D1TobqR	-	-	R0003	\N	\N
204	NA	NA	Geometry	UNKNOWN	GEO01	1900-01-01	2199-12-31	1	UNKNOWN	tNL2RskJunn1qo	C	-	R0003	\N	\N
205	NA	NA	Integrated Mathematics II	UNKNOWN	MAT2I	1900-01-01	2199-12-31	1	UNKNOWN	ivz8VxujtbLFz7	-	E	R0003	\N	\N
206	NA	NA	Mathematics	05	MAT05	1900-01-01	2199-12-31	1	UNKNOWN	1yum7wlYtgEza0	-	-	R0001	\N	\N
207	NA	NA	Mathematics	08	MAT08	1900-01-01	2199-12-31	1	UNKNOWN	NhVWgd3y4uitUo	-	-	R0001	\N	\N
208	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	X95m0tcJUSH84v	-	-	R0003	\N	\N
209	NA	NA	Integrated Mathematics II	UNKNOWN	MAT2I	1900-01-01	2199-12-31	1	UNKNOWN	Hc6RedkXlpduBi	-	F	R0003	\N	\N
210	NA	NA	Integrated Mathematics I	UNKNOWN	MAT1I	1900-01-01	2199-12-31	1	UNKNOWN	3HzSa2cH648Vvd	-	-	R0003	\N	\N
211	NA	NA	Integrated Mathematics III	UNKNOWN	MAT3I	1900-01-01	2199-12-31	1	UNKNOWN	nITIkaJdbq32oy	-	-	R0003	\N	\N
212	NA	NA	Algebra II	UNKNOWN	ALG02	1900-01-01	2199-12-31	1	UNKNOWN	XdyFVicnC7pKvY	-	B	R0002	\N	\N
213	NA	NA	Algebra I	UNKNOWN	ALG01	1900-01-01	2199-12-31	1	UNKNOWN	441Ab8l1eKHw0K	-	-	R0001	\N	\N
\.


--
-- Data for Name: dim_unique_test; Type: TABLE DATA; Schema: analytics; Owner: edware
--

COPY dim_unique_test (_key_unique_test, test_code, test_subject, asmt_grade, _valid_from, _valid_to, _record_version) FROM stdin;
13	ELA09	English Language Arts/Literacy	09	\N	\N	\N
110	MAT1I	Integrated Mathematics I	UNKNOWN	\N	\N	\N
1	ELA09	English Language Arts/Literacy	09	\N	\N	\N
106	MAT3I	Integrated Mathematics III	UNKNOWN	\N	\N	\N
9	ELA10	English Language Arts/Literacy	10	\N	\N	\N
103	ALG01	Algebra I	UNKNOWN	\N	\N	\N
2	ELA03	English Language Arts/Literacy	03	\N	\N	\N
8	ELA03	English Language Arts/Literacy	03	\N	\N	\N
121	MAT08	Mathematics	08	\N	\N	\N
66	ELA03	English Language Arts/Literacy	11	\N	\N	\N
153	MAT08	Mathematics	08	\N	\N	\N
196	GEO01	Geometry	UNKNOWN	\N	\N	\N
3	ELA11	English Language Arts/Literacy	11	\N	\N	\N
35	ELA11	English Language Arts/Literacy	11	\N	\N	\N
105	MAT2I	Integrated Mathematics II	UNKNOWN	\N	\N	\N
104	MAT03	Mathematics	03	\N	\N	\N
27	ELA03	English Language Arts/Literacy	03	\N	\N	\N
7	ELA09	English Language Arts/Literacy	09	\N	\N	\N
6	ELA10	English Language Arts/Literacy	10	\N	\N	\N
108	ALG02	Algebra II	UNKNOWN	\N	\N	\N
29	ELA10	English Language Arts/Literacy	10	\N	\N	\N
111	ALG01	Algebra I	UNKNOWN	\N	\N	\N
206	MAT05	Mathematics	05	\N	\N	\N
107	MAT05	Mathematics	05	\N	\N	\N
5	ELA11	English Language Arts/Literacy	11	\N	\N	\N
0	UNKNOWN	UNKNOWN	UNKNOWN	\N	\N	\N
109	GEO01	Geometry	UNKNOWN	\N	\N	\N
143	GEO01	Geometry	UNKNOWN	\N	\N	\N
\.


--
-- Data for Name: fact_sum; Type: TABLE DATA; Schema: analytics; Owner: edware
--

COPY fact_sum (rec_id, _key_accomod, _key_opt_state_data, _key_poy, _key_resp_school, _key_student, student_grade, sum_scale_score, sum_csem, sum_perf_lvl, sum_read_scale_score, sum_read_csem, sum_write_scale_score, sum_write_csem, subclaim1_category, subclaim2_category, subclaim3_category, subclaim4_category, subclaim5_category, subclaim6_category, state_growth_percent, district_growth_percent, parcc_growth_percent, multirecord_flag, result_type, record_type, reported_score_flag, report_suppression_code, report_suppression_action, reported_roster_flag, include_in_parcc, include_in_state, include_in_district, include_in_school, include_in_roster, create_date, me_flag, form_category, pba_test_uuid, eoy_test_uuid, sum_score_rec_uuid, _key_eoy_test_school, _key_pba_test_school, pba_total_items, eoy_total_items, pba_attempt_flag, eoy_attempt_flag, pba_total_items_attempt, eoy_total_items_attempt, pba_total_items_unit1, eoy_total_items_unit1, pba_total_items_unit2, eoy_total_items_unit2, pba_total_items_unit3, eoy_total_items_unit3, pba_total_items_unit4, eoy_total_items_unit4, pba_total_items_unit5, eoy_total_items_unit5, pba_unit1_items_attempt, eoy_unit1_items_attempt, pba_unit2_items_attempt, eoy_unit2_items_attempt, pba_unit3_items_attempt, eoy_unit3_items_attempt, pba_unit4_items_attempt, eoy_unit4_items_attempt, pba_unit5_items_attempt, eoy_unit5_items_attempt, pba_not_tested_reason, eoy_not_tested_reason, pba_void_reason, eoy_void_reason, _key_pba_test, _key_eoy_test, _key_unique_test, student_parcc_id, district_id, state_id, school_id) FROM stdin;
207	10	0	2	4	13	09	254	5	3	21	2	24	2	1	1	3	2	1	\N	44.00	63.00	14.00	- 	1	01	Y	1 	1	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	13162932-2ee6-4d59-b595-fb23ff0b8efd	9dab4c6f-856e-4b76-93dd-bdffef2204fa	0	0	46	\N	\N	\N	94	\N	38	38	92	92	12	12	59	59	55	55	4	4	62	62	46	46	56	56	75	75	0 	0 	39	39	85	70	13	EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q	R0001	\N	\N
208	5	0	2	4	11	09	277	2	3	22	1	25	1	1	1	3	2	1	\N	25.00	54.00	16.00	- 	1	02	N	\N	0	Y	N	N	N	N	W	2015-11-02	English	\N	\N	d21f2726-9b67-4e37-845a-06fd98459c55	6f08734f-3e70-4c60-a13c-018cb909000f	0	0	7	\N	\N	\N	15	\N	26	26	45	45	70	70	49	49	45	45	15	15	96	96	53	53	4	4	38	38	1 	1 	83	83	85	71	13	2vM9yk86Q3OXKckaeaUjcSTHf1TaO5LCT2DHaeU7	R0001	\N	\N
210	18	0	2	4	15	09	212	7	3	24	5	27	4	3	3	2	3	3	\N	96.00	84.00	95.00	- 	1	01	Y	2 	2	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	1f4e4ff7-80f1-4fb8-87c6-4a6502bb24d7	c3b1df9b-9c45-40c0-a776-41d35e3591ae	0	0	85	\N	\N	\N	32	\N	21	21	89	89	76	76	98	98	97	97	3	3	68	68	72	72	38	38	35	35	3 	3 	96	96	85	13	13	MyXTFn1S8qz5lQJAjw11CeaMjoaIiQbo8KihC5ix	R0001	\N	\N
315	73	0	2	8	167	10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	N 	1	03	N	2 	96	Y	N	N	N	N	W	2015-11-02	Math	\N	\N	2bc49b90-c05e-46df-92f2-75d41e09c4ee	7823823c-1ef0-4993-b00a-61389b4e1463	0	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1 	1 	69	69	211	192	106	OLbCzYnv9jHAVbFnfaD1AtFb2FzNSLjhmkjdFdbJ	R0003	\N	\N
216	37	0	2	5	17	09	449	6	5	47	6	49	1	1	2	1	1	1	\N	72.00	73.00	59.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	b681b620-edd4-4a50-affa-47774eff0914	a32523b7-52e4-4d51-98d9-ec5a3552020f	0	0	33	\N	\N	\N	7	\N	1	1	60	60	55	55	67	67	1	1	2	2	25	25	28	28	33	33	22	22	1 	1 	22	22	72	7	13	1SWd4Oky5wnWUNuH078iTM1reO6FjlIYmYkVHcQw	R0002	\N	\N
217	60	0	2	6	36	09	261	1	3	28	1	29	1	2	2	2	2	1	\N	98.00	28.00	33.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	905fdbd3-2337-457b-a38b-90fc87f615c2	02f5f8ca-ee0d-495e-bb2f-b7e4b6b938aa	0	0	25	\N	\N	\N	77	\N	47	47	65	65	88	88	7	7	3	3	23	23	26	26	16	16	22	22	37	37	1 	1 	64	64	81	76	13	Z94Y0ynyVyq3VCwLTjIRZHTZSYkFHT0lBN8meOgb	R0003	\N	\N
218	58	0	2	6	28	09	237	3	3	29	3	27	2	3	1	3	3	1	\N	90.00	62.00	55.00	- 	1	01	N	\N	0	N	N	N	N	N	N	2015-11-02	English	\N	\N	805d1318-86f8-49d7-aee4-527a22808b36	b1290688-a0ff-43fa-a825-e7d7ed05ba6a	0	0	97	\N	\N	\N	63	\N	65	65	94	94	11	11	20	20	98	98	2	2	59	59	95	95	99	99	47	47	3 	3 	69	69	81	16	13	maHNc5BQaVj9ddANVpBvFluBikLI3H06gqH3P1zi	R0003	\N	\N
219	62	0	2	6	31	09	229	7	3	36	1	35	1	2	2	2	2	2	\N	77.00	82.00	79.00	- 	1	01	Y	01	3	N	N	N	N	N	W	2015-11-02	English	\N	\N	00737d05-b0bd-46b9-80a6-20bf79cf761a	8baac5ea-2056-4afd-9b7a-2cb16ea5ee61	0	0	52	\N	\N	\N	34	\N	42	42	54	54	73	73	42	42	67	67	69	69	26	26	91	91	13	13	12	12	3 	3 	79	79	36	17	13	r1s4BnsoqFMAxUnLv1q3GbBD9SWWz0yUXPaFp0Nd	R0003	\N	\N
235	62	0	2	6	47	12	276	9	3	\N	5	\N	5	3	3	2	1	1	\N	14.00	71.00	81.00	Y 	1	01	Y	2 	9	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	00737d05-b0bd-46b9-80a6-20bf79cf761a	64d8cf91-094c-458b-b450-8fb0afa1bb66	0	0	52	\N	\N	\N	34	\N	42	42	54	54	73	73	42	42	67	67	69	69	26	26	91	91	13	13	12	12	3 	3 	79	79	182	182	196	r1s4BnsoqFMAxUnLv1q3GbBD9SWWz0yUXPaFp0Nd	R0003	\N	\N
215	26	0	2	5	16	09	391	4	4	45	3	42	2	1	2	1	1	1	\N	50.00	76.00	61.00	- 	1	03	N	\N	0	N	N	N	N	N	N	2015-11-02	English	\N	\N	91b0e6a7-e546-4c7f-8d51-d1890957b3c8	65a4a1c4-9409-4d2d-80c0-6a8abe0e28a9	0	0	41	\N	\N	\N	18	\N	68	68	61	61	12	12	31	31	54	54	57	57	59	59	83	83	55	55	14	14	1 	1 	33	33	54	54	13	0YrEK4Y3PihDRZiZnFMinOcTdkNYwublx9Z8lCAp	R0002	\N	\N
222	70	0	2	6	32	09	463	5	5	46	1	49	1	3	1	3	3	3	\N	72.00	21.00	24.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	2ff2eeef-b266-4240-8dca-d90ad4718be0	1cc0e77d-71a4-4907-9cf6-d44acffee7ea	0	0	72	\N	\N	\N	96	\N	57	57	38	38	70	70	16	16	66	66	59	59	69	69	23	23	73	73	83	83	4 	4 	73	73	36	36	13	sYbJYuJUBNDgRcwDUOr3LhFnvsw424uSYhmE0rx7	R0003	\N	\N
223	70	0	2	7	25	09	147	7	2	17	3	19	3	3	2	3	3	3	\N	94.00	68.00	48.00	- 	1	02	N	\N	0	N	N	N	N	N	N	2015-11-02	English	\N	\N	30bb2e2c-1aec-4ea4-9069-f3a947ac13cd	22766bdb-69c9-41d1-bf9c-a6f1354d465c	0	0	27	\N	\N	\N	35	\N	97	97	71	71	46	46	91	91	13	13	20	20	19	19	11	11	16	16	72	72	5 	5 	52	52	81	14	13	GxZdYAWx8b0sf9CbdWYT8HsXYqBnNikEkDMikPkd	R0003	\N	\N
224	75	0	2	7	24	09	88	6	1	9	1	10	1	2	1	2	2	2	\N	10.00	40.00	20.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	fdfc61cd-c3ee-4b3a-b40e-fc47283a9e60	553c6e87-f655-4437-8133-63ddb6288577	0	0	48	\N	\N	\N	84	\N	76	76	98	98	30	30	32	32	52	52	8	8	4	4	8	8	28	28	36	36	4 	4 	18	18	81	18	13	GuxIDl2Fjyjs81hW6n9gmufC4ruRxwcV8DCuKqbi	R0003	\N	\N
225	57	0	2	8	33	09	110	6	2	11	3	14	2	1	3	1	1	1	\N	54.00	95.00	91.00	- 	1	03	N	\N	0	N	N	N	N	N	N	2015-11-02	English	\N	\N	87546bfd-3a03-4365-ae75-c05ccb1258cc	a0008ec3-c74f-4a0a-8bca-35107e22e131	0	0	52	\N	\N	\N	37	\N	44	44	66	66	99	99	91	91	81	81	87	87	31	31	63	63	42	42	29	29	1 	1 	62	62	60	60	13	urU46yAVop0SQHyO35ugSyxZHrWM1NDDyJY3g4Uz	R0003	\N	\N
226	71	0	2	7	26	09	80	8	1	6	2	8	2	1	3	1	1	1	\N	59.00	35.00	28.00	- 	1	01	N	1 	1	N	N	N	N	N	N	2015-11-02	English	\N	\N	93133aef-f229-4574-98c6-fcc5189ebf93	54c315cf-e9d1-4a5c-9ae2-db386e051a32	0	0	4	\N	\N	\N	21	\N	91	91	43	43	2	2	32	32	79	79	37	37	87	87	68	68	42	42	40	40	4 	4 	6 	6 	81	11	13	k1kUAyTitX2QYeasKtOPdOxbPCSIet5I00G6vQON	R0003	\N	\N
227	85	0	2	8	34	09	118	8	2	12	7	15	3	1	3	1	1	1	\N	60.00	48.00	41.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	6d279ae3-e49a-4dcf-84f5-25374dc74213	8412cd07-b01e-433f-97da-b9358137c485	0	0	23	\N	\N	\N	79	\N	89	89	73	73	34	34	34	34	41	41	9	9	92	92	37	37	1	1	16	16	1 	1 	70	70	81	81	13	VfwdANZxA7Ak4zCPTWDwK9Zd47uiFTJp4rbBTmMs	R0003	\N	\N
209	11	0	2	4	12	09	207	8	3	23	1	26	1	2	2	1	3	2	\N	88.00	16.00	38.00	- 	1	02	N	1 	1	Y	N	N	N	N	W	2015-11-02	English	\N	\N	1b7f6c16-8846-47fb-b925-581b4bead589	7a81b60d-d720-4cad-8e94-38320c986263	0	0	19	\N	\N	\N	45	\N	92	92	42	42	94	94	69	69	41	41	50	50	45	45	97	97	64	64	28	28	2 	2 	54	54	85	67	13	dxpojnotJFaxas2YDJMiJoBPHkVCIc4lptuj24Dy	R0001	\N	\N
220	64	0	2	6	37	09	397	1	4	42	1	45	1	2	2	2	2	2	\N	17.00	67.00	23.00	- 	1	02	N	\N	0	Y	N	N	N	N	W	2015-11-02	English	\N	\N	c8d02a33-f11e-4f47-8bd8-7767951d2036	4e8d749a-316a-440d-91f2-d4887a853e70	0	0	71	\N	\N	\N	41	\N	29	29	28	28	19	19	60	60	89	89	31	31	8	8	83	83	12	12	49	49	3 	3 	17	17	81	19	13	zoxqZgWkKoS9mT7YODBFwUZS48qNkoXzGPZjXUHi	R0003	\N	\N
221	57	0	2	6	23	09	404	10	5	49	1	47	1	3	3	3	3	3	\N	70.00	96.00	62.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	55b43c51-c038-41b3-aa6a-f6760c80afb4	d486facf-afe0-407f-bdc8-09545ed980e9	0	0	7	\N	\N	\N	48	\N	26	26	79	79	1	1	88	88	24	24	35	35	32	32	30	30	17	17	30	30	4 	4 	42	42	81	55	13	Avrxze2AmndYm8dxmLJxDxhLsKpRgxaISvx3KS6F	R0003	\N	\N
228	70	0	2	7	30	09	44	2	1	5	1	3	1	3	3	3	3	3	\N	34.00	58.00	86.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	4b310271-405c-4367-8cd6-7b8fe5a082aa	976046e6-066a-490e-ab19-1486f9bee4d2	0	0	57	\N	\N	\N	28	\N	33	33	0	0	40	40	85	85	20	20	87	87	20	20	71	71	90	90	70	70	4 	4 	13	13	81	64	13	OjeIJyOY5Qyogd8ZPoAT4Lj6E1LmudgZcIrEeAvb	R0003	\N	\N
229	75	0	2	8	22	09	172	9	2	14	1	17	1	2	2	2	2	2	\N	60.00	58.00	84.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	1694db1e-07a8-44a0-9bd6-92afa9204d8d	354def07-0710-4ef3-94f4-811f95b83f1d	0	0	14	\N	\N	\N	71	\N	46	46	32	32	97	97	12	12	71	71	6	6	5	5	61	61	17	17	25	25	1 	1 	83	83	81	33	13	8Kjp09Mfair9T4YbPnMaq7t35stJce29GjDxDHzD	R0003	\N	\N
230	90	0	2	7	27	09	21	2	1	4	1	2	1	3	3	3	3	3	\N	97.00	38.00	27.00	- 	1	03	N	\N	0	N	N	N	N	N	N	2015-11-02	English	\N	\N	9569b28d-62ee-479a-8328-fd9850cbd779	c9a689b9-9ba8-4acd-b1d1-5bb241ba255e	0	0	35	\N	\N	\N	22	\N	35	35	57	57	60	60	64	64	88	88	87	87	20	20	3	3	40	40	80	80	4 	4 	4 	4 	91	91	13	KDGutaOLUnOe1nGxReYkTttxEhi1iLsEL5CE8CPa	R0003	\N	\N
231	67	0	2	8	35	09	299	1	3	32	1	36	1	2	2	2	2	2	\N	51.00	53.00	74.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	3c3e4b7e-61e1-49dc-80d6-d29745ae2db3	0ecbf011-0ab0-4c78-a521-4ec617d44c7e	0	0	7	\N	\N	\N	3	\N	82	82	56	56	87	87	90	90	0	0	31	31	57	57	83	83	74	74	50	50	1 	1 	67	67	81	4	13	xro1vzHRQAemvh8tb1iV4F2Yr9Tf5BmZI5F1UzlJ	R0003	\N	\N
232	85	0	2	7	21	09	174	10	2	16	8	18	3	3	2	3	3	3	\N	48.00	57.00	72.00	- 	1	03	N	\N	0	Y	N	N	N	N	W	2015-11-02	English	\N	\N	6e0b03a7-fb69-4025-b91e-c076e171f1c3	c12af48e-50b2-4a88-bfa2-f2d8738f59e8	0	0	49	\N	\N	\N	17	\N	76	76	6	6	10	10	71	71	13	13	6	6	84	84	58	58	4	4	46	46	33	33	20	20	81	1	13	4xNcr39DqN64ORBLFyIG322oypneHyCBYVXKZZPm	R0003	\N	\N
211	1	0	2	4	14	09	228	7	3	37	5	40	3	2	2	3	1	2	\N	34.00	52.00	53.00	- 	1	02	N	\N	0	N	N	N	N	N	N	2015-11-02	English	\N	\N	d0af2f64-adec-4ab8-b9e0-ce0f2e9631cf	1d6381ec-e35c-4f1d-a535-b4768b020907	0	0	32	\N	\N	\N	90	\N	69	69	64	64	38	38	78	78	54	54	89	89	76	76	45	45	71	71	41	41	8 	8 	34	34	85	85	13	ITh1jmueUE6G8rMT9oKqLXZKZMPKahZUoAUuvOQE	R0001	\N	\N
212	31	0	2	5	19	09	168	1	2	12	1	13	1	3	1	3	3	2	\N	40.00	81.00	25.00	- 	1	01	N	5 	5	N	N	N	N	N	N	2015-11-02	English	\N	\N	ba04ad27-ce18-42d8-ba7f-75bebee4ca6e	624ef560-63a9-42cd-a731-50132009fd89	0	0	37	\N	\N	\N	69	\N	59	59	96	96	60	60	98	98	87	87	74	74	35	35	99	99	40	40	84	84	10	10	55	55	73	28	13	mqZIVP2jMSn1VHrJ6nLaEHLblTiXW3eCC0BZDA6d	R0002	\N	\N
213	27	0	2	5	18	09	154	2	2	13	1	14	1	3	2	3	2	3	\N	44.00	15.00	28.00	- 	1	03	N	\N	0	N	N	N	N	N	N	2015-11-02	English	\N	\N	207191be-0e63-4879-b5af-ec0d5b5c6426	8e1239c5-4363-44e2-b415-9c13558deecb	0	0	21	\N	\N	\N	79	\N	47	47	83	83	12	12	32	32	27	27	62	62	58	58	7	7	83	83	18	18	11	11	2 	2 	73	73	13	Ew6FoKuAKYas7HNCC2qpWTLldXJFPKY6WuKGaoCK	R0002	\N	\N
214	22	0	2	5	20	09	331	2	4	44	1	41	1	3	2	3	3	3	\N	19.00	93.00	38.00	- 	1	01	Y	2 	2	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	67c4ce05-a8cd-43f8-af8e-075fb70b3ecb	d0111504-71b2-44a7-b543-fd813b5e1369	0	0	25	\N	\N	\N	93	\N	73	73	85	85	18	18	4	4	13	13	9	9	64	64	2	2	57	57	6	6	1 	1 	26	26	72	72	13	uJkYBTpWyQhQPsBC4PFyGIFqcoyKQiHxv4f3IYsu	R0002	\N	\N
233	66	0	2	8	29	09	162	3	2	17	2	14	2	2	2	2	2	2	\N	11.00	27.00	89.00	- 	1	01	N	\N	0	N	N	N	N	N	N	2015-11-02	English	\N	\N	382b343d-1b02-4b96-b1cb-2644db2e9f09	c2ef9249-45e6-4535-95bb-53653f2aa855	0	0	97	\N	\N	\N	5	\N	7	7	55	55	75	75	7	7	91	91	55	55	15	15	6	6	39	39	78	78	8 	8 	91	91	81	22	13	o557yJ1Kx4YrfB8oMHfoaDVLMTFj3DJXWrKMm9zT	R0003	\N	\N
234	15	0	2	4	63	10	332	3	4	41	3	42	3	3	3	2	3	3	\N	50.00	52.00	46.00	- 	1	01	Y	3 	3	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	bf21bb13-abd7-4ac3-89c9-8c3c7214ca4c	52846912-1b9d-4f00-962a-92dd043ac63f	0	0	100	\N	\N	\N	94	\N	8	8	20	20	0	0	15	15	45	45	80	80	89	89	42	42	56	56	68	68	4 	4 	1 	1 	78	78	9	jEEReGK9NLZr2teqi8dFBUhxaozkY3uwy5IB7hUx	R0001	\N	\N
235	16	0	2	4	62	10	333	7	4	42	4	43	1	2	1	2	3	2	\N	86.00	67.00	85.00	- 	1	02	N	\N	0	Y	N	N	N	N	W	2015-11-02	English	\N	\N	9c74587a-e7c9-4d30-af66-15399b941ca7	0469613b-39e9-43dc-80e5-84fdd693296a	0	0	33	\N	\N	\N	21	\N	38	38	84	84	94	94	86	86	59	59	31	31	35	35	52	52	73	73	49	49	5 	5 	34	34	56	29	9	1hIns0clANFCb9Rq3Cj5vEUUhMzryD9monuIwTFq	R0001	\N	\N
236	14	0	2	4	66	10	219	9	3	23	2	22	1	2	4	2	2	2	\N	47.00	95.00	81.00	- 	1	01	Y	2 	2	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	cdae1117-12b9-4077-be15-af2eb5db0e7a	40ab40d0-1b80-4552-81ae-baa0502c5ff6	0	0	76	\N	\N	\N	26	\N	36	36	5	5	31	31	24	24	70	70	58	58	38	38	81	81	45	45	8	8	6 	6 	57	57	56	45	9	SBFMQrkvsLNLQWnEXVVeN6ZMQXqemPhTY44hgO2M	R0001	\N	\N
237	19	0	2	4	64	10	467	1	5	46	1	50	1	2	1	3	2	2	\N	77.00	61.00	75.00	- 	1	01	N	\N	0	N	N	N	N	N	N	2015-11-02	English	\N	\N	89450f20-5621-4cc8-bd71-709834a8631e	abe70eed-50ad-4ea0-9143-03724edfa770	0	0	1	\N	\N	\N	41	\N	22	22	53	53	24	24	44	44	60	60	80	80	7	7	60	60	94	94	48	48	10	10	48	48	56	56	9	OIeWicvTvk5kcbEc0qj7vPo2nh3Vr8XS9e2Vz57b	R0001	\N	\N
238	23	0	2	5	70	10	182	2	2	14	1	15	1	2	1	3	3	1	\N	76.00	23.00	62.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	80770855-70a5-432b-921d-74930d96679d	ebc63927-bd2a-48b5-9c4d-0c40d0655f85	0	0	57	\N	\N	\N	45	\N	13	13	97	97	40	40	39	39	1	1	82	82	37	37	35	35	5	5	14	14	2 	2 	48	48	93	12	9	uB6VLZuP4JnB0aZW3u9Ol9pUS9ux9VdAuAaQ19fX	R0002	\N	\N
239	4	0	2	4	65	10	451	6	5	50	6	47	6	1	2	3	2	3	\N	64.00	11.00	25.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	39d27e35-2285-4c06-83ab-a00f52537471	3063220f-8693-4b43-8d75-69408e06447e	0	0	26	\N	\N	\N	86	\N	46	46	84	84	37	37	27	27	19	19	0	0	42	42	60	60	48	48	37	37	6 	6 	91	91	90	90	9	RqHsCUovoRLSAhySU0AnRx0LY8YKin9uDhSFcOMD	R0001	\N	\N
240	40	0	2	5	69	10	223	8	3	30	8	29	2	3	3	3	3	1	\N	70.00	30.00	46.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	28e46644-56b0-439d-aece-4a88f147b218	b22f03bf-189f-432a-ad69-247bfddbb66d	0	0	75	\N	\N	\N	26	\N	96	96	14	14	42	42	50	50	46	46	65	65	90	90	19	19	70	70	17	17	2 	2 	43	43	93	57	9	JskCum0I6ZULH064xsFeFcW4eZVTIEj5m6zExxG2	R0002	\N	\N
241	39	0	2	5	67	10	202	5	3	29	4	30	2	2	3	2	2	3	\N	66.00	97.00	45.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	4243c86e-837a-4255-b667-7b4b8195862f	d0e1956e-5335-4025-b912-59e7fe5dcd82	0	0	24	\N	\N	\N	99	\N	13	13	34	34	40	40	14	14	59	59	55	55	98	98	80	80	89	89	78	78	2 	2 	16	16	93	46	9	EOHTQnkhMw4mzKP8Rpaq42vOlOBZzsZZkKU6zpll	R0002	\N	\N
242	38	0	2	5	71	10	206	4	3	30	1	31	1	2	2	2	2	3	\N	13.00	99.00	84.00	- 	1	01	Y	3 	3	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	1ec80e05-8f1c-4a65-9349-14694bbbac78	81e942ae-af3c-40ae-9f38-1e72f10ea569	0	0	94	\N	\N	\N	39	\N	17	17	13	13	22	22	82	82	76	76	78	78	1	1	67	67	30	30	67	67	1 	1 	62	62	93	93	9	UH69Q4IEQrjnQGvI9xhrNRTWUtmihpX8hlQ4r61X	R0002	\N	\N
243	42	0	2	6	82	10	456	10	5	50	9	48	8	2	2	2	2	2	\N	11.00	29.00	56.00	- 	1	03	N	\N	0	N	N	N	N	N	N	2015-11-02	English	\N	\N	81b4561e-6e70-47b4-8dfe-2898e550de7f	0a6965e4-9ee6-4fc8-82db-17403876830f	0	0	13	\N	\N	\N	1	\N	56	56	44	44	79	79	9	9	64	64	22	22	68	68	74	74	57	57	40	40	3 	3 	30	30	100	32	9	U49Ivq4Y5D2NTB0EAuHv0sKhVA90oVkCwOgJErDW	R0003	\N	\N
244	30	0	2	5	68	10	212	4	3	31	1	32	1	2	2	2	2	3	\N	51.00	39.00	36.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	307a76ba-cda8-43fc-8953-55795759032e	511b0c83-dce0-4ff7-bbac-776a5b3d7e7e	0	0	37	\N	\N	\N	54	\N	11	11	26	26	80	80	18	18	97	97	54	54	54	54	5	5	98	98	50	50	1 	1 	76	76	93	9	9	Fg5kGWmyfjDA600mHsVyZJALKlRwmg8pe1LtPdSb	R0002	\N	\N
245	61	0	2	6	73	10	471	6	5	47	3	49	3	2	1	2	2	2	\N	58.00	90.00	84.00	- 	1	01	N	\N	0	N	N	N	N	N	N	2015-11-02	English	\N	\N	7fec724b-1594-4e0e-9d08-a539a77b3c99	2b3db56d-98b3-47f4-9324-73d4733c781a	0	0	34	\N	\N	\N	40	\N	66	66	63	63	26	26	61	61	69	69	87	87	64	64	34	34	58	58	78	78	3 	3 	13	13	100	68	9	Cnt3veDS2HW94jTAnU4bUASdLgIbP9y0lvQpQqer	R0003	\N	\N
246	45	0	2	6	75	10	248	1	3	35	1	38	1	3	3	3	3	3	\N	98.00	58.00	55.00	- 	1	03	N	\N	0	N	N	N	N	N	N	2015-11-02	English	\N	\N	94a06c65-474b-423f-9eb8-59ac8cf496db	cfd67a8e-0659-4ddb-aa65-348d6027541c	0	0	78	\N	\N	\N	56	\N	88	88	2	2	71	71	57	57	29	29	94	94	44	44	39	39	43	43	15	15	3 	3 	78	78	97	97	9	E9fHez5Rp8SfdyyhzYgAlyLyIE1JYmpYL8d9Up4W	R0003	\N	\N
247	48	0	2	7	78	10	137	7	2	17	5	15	5	3	2	3	3	3	\N	35.00	86.00	61.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	0483d072-9b85-49b5-b542-95dbcfca1a06	6d0d60f1-4370-46f2-863e-d913ec3af726	0	0	52	\N	\N	\N	88	\N	61	61	38	38	47	47	33	33	18	18	82	82	60	60	72	72	73	73	98	98	4 	4 	58	58	30	30	9	p2Sf5xBg8rAuTjy9XPGuFJaoYhdKIHISnVuRqoUd	R0003	\N	\N
248	41	0	2	6	76	10	377	10	4	41	9	44	5	2	3	2	2	2	\N	18.00	62.00	32.00	- 	1	02	N	\N	0	Y	N	N	N	N	W	2015-11-02	English	\N	\N	cf537852-5ba0-4af8-ad17-996d07c0a040	ae14d043-fbb7-414a-9836-89e6328cb0ae	0	0	38	\N	\N	\N	68	\N	16	16	58	58	13	13	10	10	89	89	43	43	42	42	96	96	37	37	82	82	3 	3 	16	16	100	100	9	haGegI5PgD6OzaQ1Mu478Og0zw693ZmLAgQTbjLb	R0003	\N	\N
249	59	0	2	7	81	10	198	9	2	18	2	16	2	3	2	3	3	3	\N	40.00	42.00	68.00	- 	1	01	Y	5 	5	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	1e71359d-b18d-4b83-a613-1a9f52f89d9e	8a4eae54-8561-4bdc-b69c-cbca486cb465	0	0	25	\N	\N	\N	80	\N	54	54	8	8	13	13	33	33	71	71	40	40	22	22	97	97	54	54	72	72	4 	4 	80	80	39	39	9	sEorUakkZr2QwQgeQ1wCHxo8NwkksVvRlPXQV7cL	R0003	\N	\N
250	49	0	2	7	80	10	102	7	2	19	7	17	6	3	2	3	3	3	\N	66.00	88.00	26.00	- 	1	02	N	\N	0	N	N	N	N	N	N	2015-11-02	English	\N	\N	95a45098-3643-421f-9ef0-99e7e683b52a	53b7291d-5875-47d8-b4e4-e572eccebdf5	0	0	8	\N	\N	\N	42	\N	61	61	72	72	45	45	22	22	14	14	15	15	31	31	3	3	87	87	79	79	5 	5 	72	72	100	77	9	Sc0cLMiRhBfkJsHF7Dwz1ozIPzc1k3s6aOgAD2IT	R0003	\N	\N
251	52	0	2	7	85	10	202	9	3	33	9	37	1	1	1	1	1	1	\N	66.00	82.00	34.00	- 	1	03	N	\N	0	N	N	N	N	N	N	2015-11-02	English	\N	\N	6a996b2d-7fe2-40ad-a34b-facf7b9034db	31ec13ad-d195-435a-b060-d99c60b46bf1	0	0	81	\N	\N	\N	65	\N	36	36	65	65	14	14	67	67	4	4	32	32	75	75	71	71	88	88	90	90	5 	5 	10	10	100	89	9	ZmrZFBiOmI2H2QuEsZm59MMu7bIAhKHUjge92f6E	R0003	\N	\N
252	53	0	2	7	79	10	204	7	3	36	1	32	1	3	3	3	3	3	\N	12.00	80.00	74.00	- 	1	02	N	\N	0	Y	N	N	N	N	W	2015-11-02	English	\N	\N	028647c5-df78-46dc-b0b9-760586f55fc8	22a51fde-87cf-40d9-884f-cb58d92a7aae	0	0	44	\N	\N	\N	62	\N	7	7	84	84	82	82	11	11	20	20	25	25	12	12	41	41	56	56	41	41	5 	5 	94	94	100	6	9	sB2VXSoaEMtiSg4WjFUXNNdh3rDSEvULHmGXvAnG	R0003	\N	\N
253	68	0	2	8	84	10	194	2	2	13	2	16	2	1	2	1	1	1	\N	70.00	11.00	84.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	77c6d6a6-9b59-4189-b11b-75aedde7511c	c829124c-0544-4340-8ae3-e1f0eaf7f586	0	0	45	\N	\N	\N	35	\N	23	23	4	4	22	22	43	43	74	74	54	54	82	82	90	90	97	97	27	27	1 	1 	52	52	87	40	9	xhHUuTmLdRjylYGiO2ZU54h2J04u9BJSn0exbdX2	R0003	\N	\N
254	73	0	2	8	77	10	287	2	3	36	2	37	1	2	3	2	2	2	\N	12.00	59.00	62.00	- 	1	03	N	\N	0	Y	N	N	N	N	W	2015-11-02	English	\N	\N	2bc49b90-c05e-46df-92f2-75d41e09c4ee	e90f3dbd-bb0e-487f-b5ea-9fdf51446e27	0	0	60	\N	\N	\N	15	\N	1	1	71	71	35	35	2	2	29	29	39	39	40	40	40	40	32	32	17	17	1 	1 	69	69	100	21	9	OLbCzYnv9jHAVbFnfaD1AtFb2FzNSLjhmkjdFdbJ	R0003	\N	\N
255	81	0	2	8	72	10	216	3	3	37	2	33	2	3	3	3	3	3	\N	10.00	81.00	45.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	76040d10-ba95-444b-a29e-da65a3679d01	7fce8cc3-59cc-4559-ae49-8e490448e90a	0	0	57	\N	\N	\N	19	\N	48	48	80	80	89	89	92	92	64	64	15	15	75	75	91	91	50	50	98	98	1 	1 	97	97	100	31	9	8qadBD3xrsvpfExXtJw6dHilQMcdZHjWg60GuV8D	R0003	\N	\N
256	92	0	2	8	74	10	340	3	4	45	3	44	1	1	2	1	1	1	\N	62.00	38.00	17.00	- 	1	02	N	\N	0	Y	N	N	N	N	W	2015-11-02	English	\N	\N	c2043a85-fb32-440a-8ca1-9f1f657aa4a2	76a9f327-a761-4d46-bdeb-d1b06f942509	0	0	78	\N	\N	\N	70	\N	64	64	47	47	59	59	5	5	65	65	78	78	61	61	1	1	91	91	92	92	4 	4 	50	50	100	52	9	DSoDwPOg0kZQ5YPnMJ0SLmBaCeGes4a1qjkRUQfd	R0003	\N	\N
257	47	0	2	8	83	10	278	3	3	38	3	35	2	2	3	2	2	2	\N	34.00	10.00	17.00	- 	1	03	N	\N	0	Y	N	N	N	N	W	2015-11-02	English	\N	\N	1101d194-08b9-4632-b655-513319ed9a07	401cc9b4-9c05-4ca2-bed2-90ce3f2d5427	0	0	55	\N	\N	\N	19	\N	84	84	70	70	92	92	59	59	62	62	92	92	24	24	89	89	24	24	97	97	8 	8 	29	29	87	87	9	WW9iCRpiTricUbWoelchVevYIJHSjhAUMpLVYt6D	R0003	\N	\N
258	7	0	2	4	110	11	297	9	3	25	6	21	3	2	2	3	2	2	\N	34.00	93.00	16.00	- 	1	01	Y	1 	1	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	472bef1a-828a-48ef-9c46-8f3a3c235d0d	e3d7ef8e-3fd5-4497-ab99-1bcb39f7eed6	0	0	53	\N	\N	\N	26	\N	0	0	83	83	9	9	28	28	22	22	28	28	99	99	94	94	83	83	88	88	33	33	57	57	35	35	3	RQaWFAiKM91B9AbbwRXpJzOo302G4qKWtzbpRano	R0001	\N	\N
259	17	0	2	4	106	11	421	4	5	47	3	49	2	2	2	1	3	2	\N	56.00	20.00	19.00	- 	1	03	N	\N	0	Y	N	N	N	N	W	2015-11-02	English	\N	\N	17c77770-7dde-4bfd-8ed8-2d24ffb665ee	6c26d2d4-870f-4e2d-8f6d-233aacc9f86b	0	0	7	\N	\N	\N	22	\N	39	39	51	51	62	62	68	68	94	94	32	32	5	5	77	77	4	4	36	36	23	23	10	10	98	98	3	6DRp6GlmW7bu4tn1PhckfVhL9MHH85oXIaSfRJep	R0001	\N	\N
260	6	0	2	4	107	11	202	4	3	24	4	23	3	3	3	2	3	3	\N	99.00	31.00	93.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	35c65034-b5b4-4129-b850-8d1fbf9f58ee	b24a821b-6d9e-4854-8af0-df5184affc21	0	0	64	\N	\N	\N	69	\N	45	45	77	77	20	20	3	3	42	42	15	15	56	56	75	75	21	21	1	1	22	22	57	57	74	74	3	IWr9RrKwQAe5iXf7W8xFiTf4fxYeWjWvoRJB7s45	R0001	\N	\N
261	13	0	2	4	109	11	262	8	3	25	1	24	1	3	2	3	1	3	\N	84.00	78.00	72.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	fa72a9f8-a5bc-4ed2-bffb-1817f65c3069	6bc4e31f-d8bd-4d59-bf08-5222b3a96293	0	0	88	\N	\N	\N	13	\N	89	89	39	39	68	68	24	24	74	74	6	6	74	74	56	56	82	82	24	24	1 	1 	7 	7 	74	51	3	p42AqWSux32Vo3RQzM34aioB8DbTdzDqfLRsAnhF	R0001	\N	\N
262	21	0	2	5	114	11	175	2	2	14	2	13	2	1	1	1	1	3	\N	95.00	46.00	95.00	- 	1	01	Y	5 	5	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	e56959c5-c419-45b3-a1ab-327e78b10aeb	b8340e9c-33ae-4a21-869d-7291c87af210	0	0	53	\N	\N	\N	34	\N	67	67	54	54	87	87	64	64	30	30	43	43	89	89	98	98	21	21	18	18	55	55	12	12	79	5	3	sstpR8WRh7effxahZuK1ZPaQJmLH10nFDTSm23am	R0002	\N	\N
263	76	0	2	7	124	11	271	6	3	32	3	36	2	3	1	3	3	3	\N	69.00	50.00	49.00	- 	1	01	N	\N	0	N	N	N	N	N	N	2015-11-02	English	\N	\N	35b4c08c-f392-4c2e-94b2-2472ac6909b8	89921cd5-c987-468f-b362-d5c6c17856f4	0	0	74	\N	\N	\N	80	\N	28	28	97	97	42	42	26	26	34	34	64	64	89	89	60	60	58	58	11	11	5 	5 	48	48	99	99	3	loKBQQ7EnGEJKKoK39nYLCNWtoJIDC2yNW9cPcmh	R0003	\N	\N
264	12	0	2	4	108	11	246	8	3	33	6	37	2	1	2	3	3	3	\N	90.00	97.00	12.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	291c9cf8-bc4d-4af1-a900-2fa77efddd1b	c36be487-e6d2-4d72-a442-93279973b819	0	0	2	\N	\N	\N	8	\N	80	80	21	21	60	60	98	98	54	54	42	42	69	69	19	19	62	62	69	69	9 	9 	17	17	74	61	3	JL6uVW6OSVaYqpvndl0foBKAFrf4bdz6HACTTcLd	R0001	\N	\N
265	35	0	2	5	115	11	197	8	2	15	5	14	5	1	3	3	1	3	\N	90.00	84.00	51.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	ce42c551-d724-4718-b769-31a796be9581	bb1910b9-5631-486a-aeb3-29434dc9e081	0	0	26	\N	\N	\N	46	\N	11	11	85	85	74	74	85	85	16	16	70	70	41	41	85	85	84	84	58	58	44	44	16	16	83	83	3	Uetza1KA3EqTldkdiq0w92KFYhVqBL6zLWinhXI9	R0002	\N	\N
266	91	0	2	7	133	11	358	3	4	45	2	40	2	1	3	1	1	1	\N	27.00	81.00	60.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	d5f2e34b-e706-402c-bc10-1e7bfa707f5f	b73cc90c-b450-4def-acb2-423268f8c07f	0	0	3	\N	\N	\N	14	\N	70	70	57	57	28	28	45	45	12	12	61	61	88	88	44	44	28	28	15	15	5 	5 	87	87	53	53	3	ZHBxgfZVT3KsM8I8iun3007MOqKZxIvz5HZht1dR	R0003	\N	\N
267	25	0	2	5	113	11	295	3	3	31	2	30	2	3	3	3	3	2	\N	63.00	50.00	42.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	db94ba6c-3abe-4e9a-8bbb-b680531efbab	64b2f62f-c5d4-49c9-be9a-762e5e161e2c	0	0	15	\N	\N	\N	26	\N	96	96	58	58	15	15	47	47	10	10	35	35	88	88	27	27	67	67	39	39	3 	3 	80	80	102	102	3	p69xZhxSMQuWmke62h3j1K7izaj1evXeklkHFuMg	R0002	\N	\N
268	79	0	2	7	116	11	10	6	1	5	1	7	1	1	3	1	1	1	\N	74.00	34.00	29.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	4d767508-38b1-4927-9048-0a6fb4b5c2a5	c5007fd7-93fa-4458-bfd9-44ae9de391f2	0	0	4	\N	\N	\N	67	\N	54	54	86	86	21	21	65	65	18	18	60	60	51	51	52	52	68	68	99	99	5 	5 	66	66	99	26	3	4mzU3y7OgIchC8Rz9tfOk7KI8AcXlVtFPVOO1XFH	R0003	\N	\N
269	86	0	2	6	129	11	441	10	5	48	4	47	4	3	1	3	3	3	\N	58.00	99.00	36.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	b2de9140-4fec-4ba0-807c-655d41d0a161	8a933cdb-15aa-4b06-81bc-65e97eb1a715	0	0	57	\N	\N	\N	13	\N	49	49	27	27	25	25	39	39	40	40	74	74	74	74	49	49	41	41	48	48	3 	3 	71	71	99	10	3	wD6y2AQPgV3HtqM23RFtegCe56oenrPa6DdbElzI	R0003	\N	\N
270	29	0	2	5	112	11	382	2	4	41	1	44	1	3	3	3	3	3	\N	44.00	65.00	99.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	30942862-4d08-4505-a85e-798d8065de42	cdb5e9f7-8259-46e1-8dea-e005ff9d8ff4	0	0	5	\N	\N	\N	81	\N	32	32	22	22	80	80	73	73	4	4	67	67	27	27	29	29	39	39	43	43	5 	5 	27	27	79	15	3	OimZrYW80W7xl7QUa4Hg9Q9knjdvys1Z8wRIVHuG	R0002	\N	\N
271	46	0	2	8	126	11	483	2	5	47	1	49	1	1	3	1	1	1	\N	47.00	17.00	50.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	45d46b86-9771-47c4-9205-ed5df2a743d7	0	0	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	99	65	3	nQXqvAHQ0W1FOGH2IFhG68Aa590JN9bpNQtSXdiw	R0003	\N	\N
272	88	0	2	7	119	11	16	8	1	4	3	6	1	1	3	1	1	1	\N	83.00	57.00	77.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	0e9d8042-698b-4046-ba1b-34997540bdbc	bd14470c-edd7-479e-9ca4-82da5383df54	0	0	67	\N	\N	\N	17	\N	2	2	14	14	35	35	7	7	8	8	3	3	56	56	43	43	10	10	53	53	5 	5 	54	54	99	88	3	iI34kG6uH1mQ83tbh5PEVVKoQjV1r96laDb4x10n	R0003	\N	\N
273	33	0	2	5	111	11	138	8	2	13	5	12	5	1	1	1	1	3	\N	89.00	30.00	79.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	8559ab78-3be5-4ff9-8966-e8c4c6f3b8df	d104f8e3-fc52-44db-95bf-6315b1f0d298	0	0	86	\N	\N	\N	19	\N	19	19	84	84	62	62	2	2	58	58	92	92	83	83	57	57	92	92	22	22	6 	6 	72	72	79	79	3	oGYUvFLatZjgEBWy3LV9NwMdmc9GT1GTerNuABS9	R0002	\N	\N
274	44	0	2	6	121	11	455	3	5	49	3	46	2	3	1	3	3	3	\N	99.00	84.00	52.00	- 	1	02	N	\N	0	Y	N	N	N	N	W	2015-11-02	English	\N	\N	e5c1fa54-2abd-4b9a-98e8-b3e13fa8f754	915071ce-3b15-4788-9bb8-6c07b1deec33	0	0	42	\N	\N	\N	87	\N	44	44	39	39	51	51	50	50	47	47	24	24	94	94	75	75	87	87	31	31	3 	3 	89	89	99	47	3	JRAWX24sIH3Bh2KcmpYNn6ZyGr8e6p6ctkAMFRw5	R0003	\N	\N
275	65	0	2	7	130	03	187	8	2	15	2	17	1	2	2	2	2	2	\N	91.00	27.00	93.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	8e2e1208-b860-4723-975e-c167831dce5c	3e46b648-2717-4f95-bda6-285e8bf67278	0	0	2	\N	\N	\N	38	\N	69	69	48	48	5	5	17	17	88	88	98	98	27	27	20	20	77	77	10	10	22	22	80	80	66	66	2	WQSZZ0TnQLwHd1VBV596z6mqD7T07BZiUgXDa2o2	R0003	\N	\N
276	83	0	2	8	132	11	341	10	4	44	1	42	1	1	1	1	1	1	\N	59.00	41.00	45.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	2aff6dcf-e2de-4ec4-9212-a761f29e0e6f	0055b1fc-2bc9-4747-91f6-6388c0e282dd	0	0	89	\N	\N	\N	97	\N	55	55	4	4	16	16	74	74	75	75	59	59	33	33	59	59	32	32	82	82	1 	1 	33	33	34	34	3	xkO5Umv7TjxiZY8CD926MY27uoz1edQFcaWU76Ef	R0003	\N	\N
277	82	0	2	6	125	11	218	4	3	35	1	33	1	3	3	3	3	3	\N	13.00	67.00	57.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	4d9d3bfc-6a13-4ebd-8c6c-8cf4581c3c5d	b43f1880-193b-4118-9271-3dc383d2f59e	0	0	54	\N	\N	\N	69	\N	57	57	48	48	97	97	92	92	1	1	86	86	63	63	5	5	16	16	6	6	4 	4 	9 	9 	99	86	3	nHaADPpFPr1tpkcoS2Xj5mhmvdViLrumXuJMSox3	R0003	\N	\N
278	94	0	1	2	117	11	301	1	4	44	1	42	1	3	2	3	3	3	\N	57.00	41.00	87.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	22b58ccc-2418-4508-bc73-12b3ff68dac3	c1082a31-3843-4932-9334-3b967dacaca3	0	0	96	\N	\N	\N	68	\N	63	63	82	82	63	63	80	80	46	46	11	11	46	46	39	39	80	80	43	43	8 	8 	3 	3 	99	23	3	6boPVVoO1Vuyg8VJ5AqV0Zha4rVGrZt3uPrM7xgw	R0003	\N	\N
279	56	0	2	8	131	11	445	8	5	48	5	47	1	2	3	2	2	2	\N	95.00	58.00	27.00	- 	1	03	N	\N	0	Y	N	N	N	N	W	2015-11-02	English	\N	\N	66e13a15-faaa-47d2-b082-a8d5e8aaea6c	3e3e3ce0-1cf8-4e4c-ab5e-bffd7ab0db2b	0	0	39	\N	\N	\N	35	\N	4	4	74	74	68	68	45	45	49	49	61	61	69	69	21	21	47	47	50	50	5 	5 	94	94	99	3	3	wTpuVRx1CP4eXjjQiCKXjjvvLaZpRAAujc0QB30k	R0003	\N	\N
280	54	0	2	6	123	11	207	5	3	36	4	34	4	3	2	3	3	3	\N	67.00	36.00	74.00	- 	1	02	N	\N	0	Y	N	N	N	N	W	2015-11-02	English	\N	\N	5f6a86cc-94e7-453a-ae49-622325b6244a	07e5465d-9a4c-4c73-8fa7-f97ab0d469fb	0	0	28	\N	\N	\N	4	\N	5	5	53	53	81	81	2	2	32	32	41	41	67	67	64	64	9	9	93	93	4 	4 	31	31	99	58	3	LaPd7vVIU9G9NvGr44WMWgwNwvupwIyWtC5kGetF	R0003	\N	\N
281	69	0	1	2	122	11	369	10	4	45	3	44	1	3	3	3	3	3	\N	68.00	41.00	54.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	0b674c27-ad68-4597-91bc-01fe8e4d57d2	8fb46bb9-280b-4584-9a0f-7e00f4ab7cd7	0	0	71	\N	\N	\N	72	\N	72	72	71	71	28	28	31	31	40	40	49	49	99	99	65	65	11	11	2	2	8 	8 	94	94	99	24	3	KL3R8L1EaO8MgymDcMI59fe7DnjLk0ZuDZiwO2J2	R0003	\N	\N
282	43	0	2	8	120	11	184	5	2	16	1	13	1	2	1	2	2	2	\N	78.00	40.00	82.00	- 	1	02	N	\N	0	Y	N	N	N	N	W	2015-11-02	English	\N	\N	1b03efd2-1d1e-4110-9350-22185753b6ba	f317e79f-1b79-4463-bdfd-4700232f8cb9	0	0	79	\N	\N	\N	46	\N	46	46	17	17	56	56	92	92	66	66	50	50	21	21	28	28	5	5	25	25	58	58	66	66	99	43	3	jgliMmu6B15PbLy7sq7uRlv40qiIF3liNI3ySpru	R0003	\N	\N
283	63	0	2	6	118	11	342	7	4	45	1	42	1	1	2	1	1	1	\N	61.00	31.00	13.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	7ea982c0-a412-49d6-a6b5-fe4d05c94eb3	de01c9b7-1b53-4174-a91c-0a8f6c70c819	0	0	48	\N	\N	\N	68	\N	21	21	76	76	76	76	37	37	45	45	30	30	68	68	10	10	89	89	11	11	4 	4 	60	60	99	69	3	cS2xAK3xDPCBR7ZWbAh9FS08IqjzWxgbZeitAa40	R0003	\N	\N
284	46	0	1	2	126	11	490	2	5	47	1	49	1	1	3	1	1	1	\N	47.00	17.00	50.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	14bea448-99d3-45e6-ba13-162d4cc0d0a8	0	0	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	99	65	3	nQXqvAHQ0W1FOGH2IFhG68Aa590JN9bpNQtSXdiw	R0003	\N	\N
285	77	0	2	8	127	11	213	9	3	39	1	36	1	2	3	2	2	2	\N	57.00	84.00	66.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	f620a09f-9103-4446-b234-1c736f15500c	18f97d35-021e-410e-a8cc-c95fc8bbf981	0	0	42	\N	\N	\N	73	\N	1	1	8	8	74	74	8	8	0	0	68	68	83	83	7	7	20	20	17	17	8 	8 	21	21	99	96	3	Uk2E8MeExKfyl6XHBLKpNJe6na8bi3nj8yMCgeFz	R0003	\N	\N
286	72	0	2	6	128	11	449	10	5	47	10	49	1	2	3	2	2	2	\N	35.00	71.00	38.00	- 	1	01	Y	2 	2	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	be331185-b72d-48bf-9d36-e90ecee72e92	d1706d64-ffaa-4dfb-ad33-7de052568519	0	0	68	\N	\N	\N	99	\N	61	61	83	83	85	85	90	90	5	5	4	4	8	8	99	99	57	57	63	63	4 	4 	32	32	99	48	3	UmOkwomla6wQuMu8rde3f3OkVwf23neTGCISpB28	R0003	\N	\N
287	74	0	2	8	198	03	191	9	2	15	6	12	4	3	2	3	3	3	\N	57.00	90.00	65.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	eed942f3-d554-40f2-b547-446a9e1165bf	db651bc6-76a5-4ef2-992d-be10881592cc	0	0	1	\N	\N	\N	3	\N	30	30	78	78	28	28	16	16	61	61	2	2	63	63	39	39	72	72	58	58	5 	5 	59	59	66	8	2	e3OGNkIbM4h2skSI4nPuESDwrF1EMmjIfrprGMHz	R0003	\N	\N
288	2	0	2	4	189	03	297	4	3	26	4	25	1	1	1	1	2	1	\N	56.00	70.00	13.00	- 	1	01	Y	01	1	N	N	N	N	N	N	2015-11-02	English	\N	\N	96cf2a1e-6a7b-4d81-bf20-3600c0e59e58	a44997e3-53f6-4848-815b-7257163ff542	0	0	22	\N	\N	\N	83	\N	88	88	65	65	6	6	11	11	34	34	15	15	93	93	49	49	1	1	81	81	2 	2 	60	60	101	62	2	Xwk5McYQSIRVCwDpzpWzMfHYetWqy5BUNy6Z2ogV	R0001	\N	\N
289	51	0	2	6	196	03	319	10	4	44	3	41	2	2	2	2	2	2	\N	16.00	67.00	78.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	96ba894d-7749-48f9-9ef5-3019f562f963	c1cb032a-7b58-499a-9999-3d273c9dcccd	0	0	71	\N	\N	\N	73	\N	27	27	2	2	83	83	46	46	22	22	3	3	97	97	20	20	29	29	57	57	4 	4 	98	98	95	92	2	6xduCZW2oSUGSLzpiFpJQQK8YSAzmBrMjAh4WEkn	R0003	\N	\N
290	80	0	2	8	201	03	294	10	3	40	6	37	6	2	3	2	2	2	\N	56.00	49.00	99.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	a16f02a7-151a-4b29-8203-8568a8746591	ed5311ed-17fb-4b5c-bfda-c006a01e9cff	0	0	56	\N	\N	\N	100	\N	88	88	21	21	91	91	51	51	58	58	64	64	36	36	50	50	67	67	84	84	8 	8 	86	86	95	25	2	oBPTLwB89Yetjpa88VqxOvlIENOqe4XKGKnOrJfM	R0003	\N	\N
291	9	0	2	4	187	03	227	2	3	27	1	26	1	2	2	2	3	2	\N	39.00	16.00	86.00	- 	1	03	N	\N	0	Y	N	N	N	N	W	2015-11-02	English	\N	\N	771d6032-621f-47b3-8113-2aeffcb7bffa	f6eaeed1-e2d1-4b2d-8c64-ffe01a832b07	0	0	53	\N	\N	\N	4	\N	70	70	84	84	37	37	66	66	28	28	86	86	50	50	69	69	65	65	18	18	5 	5 	87	87	101	94	2	UMaYGd8hto4TYc7EiWKtivzti8YAWg5xozomFI8q	R0001	\N	\N
292	34	0	2	5	190	03	246	3	3	32	3	31	1	3	3	3	3	3	\N	53.00	28.00	27.00	- 	1	01	Y	01	2	N	N	N	N	N	Y	2015-11-02	English	\N	\N	54488102-dc6f-45fb-8941-14b9dc51c977	1d3c68bb-dbc5-40f3-be4f-086a1b239f52	0	0	18	\N	\N	\N	53	\N	63	63	23	23	96	96	26	26	5	5	63	63	4	4	39	39	14	14	22	22	4 	4 	20	20	75	75	2	3i8VkcJCs5QffVgj2QOmZr3MLv40I0KJc5k5DcWe	R0002	\N	\N
293	94	0	2	8	195	03	305	1	4	44	1	42	1	3	2	3	3	3	\N	57.00	41.00	87.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	22b58ccc-2418-4508-bc73-12b3ff68dac3	d6bea7d9-d90d-4f99-ab64-080e3c5ee53a	0	0	96	\N	\N	\N	68	\N	63	63	82	82	63	63	80	80	46	46	11	11	46	46	39	39	80	80	43	43	8 	8 	3 	3 	95	41	2	6boPVVoO1Vuyg8VJ5AqV0Zha4rVGrZt3uPrM7xgw	R0003	\N	\N
294	20	0	2	4	186	03	355	2	4	42	2	41	1	3	3	1	1	2	\N	63.00	59.00	46.00	- 	1	02	N	\N	0	Y	N	N	N	N	W	2015-11-02	English	\N	\N	bb63c6c4-07db-4b0a-974f-59fcaf94c189	1d14762b-ae4b-4332-8045-6b9c51dd7880	0	0	3	\N	\N	\N	31	\N	71	71	96	96	11	11	72	72	21	21	92	92	34	34	64	64	45	45	81	81	3 	3 	25	25	101	38	2	qEy737ea1Emx97d0RTxk7QpDWecnUu2eEBWvQXkK	R0001	\N	\N
295	84	0	2	6	205	03	466	3	5	47	2	48	2	1	3	1	1	1	\N	64.00	17.00	62.00	- 	1	02	N	\N	0	N	N	N	N	N	N	2015-11-02	English	\N	\N	8d54686a-ff28-415a-8ee5-95cb999600f0	b30bcb85-5e27-4268-bccc-914312dc90c3	0	0	98	\N	\N	\N	67	\N	35	35	89	89	27	27	73	73	10	10	69	69	40	40	57	57	89	89	86	86	4 	4 	68	68	95	63	2	wUfuK1C1vpevOq8cV6oqQxNNwJV1TbzeraL8fC4v	R0003	\N	\N
296	32	0	2	5	194	03	385	9	4	42	8	45	5	3	2	1	3	3	\N	61.00	38.00	12.00	- 	1	01	N	\N	0	N	N	N	N	N	N	2015-11-02	English	\N	\N	75c44166-926f-4c5b-a208-42231bf5cd01	eff2bc68-deaa-453c-9d95-3be42a202dd9	0	0	89	\N	\N	\N	44	\N	16	16	37	37	3	3	10	10	53	53	62	62	7	7	72	72	60	60	93	93	6 	6 	58	58	75	50	2	WAhvqzYnESXaMgGYTK04IH1RPLeTatzuOubPg8U7	R0002	\N	\N
297	69	0	2	8	200	03	365	10	4	45	3	44	1	3	3	3	3	3	\N	68.00	41.00	54.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	0b674c27-ad68-4597-91bc-01fe8e4d57d2	08dde853-be20-4883-b263-ab8ed18cc3ec	0	0	71	\N	\N	\N	72	\N	72	72	71	71	28	28	31	31	40	40	49	49	99	99	65	65	11	11	2	2	8 	8 	94	94	95	42	2	KL3R8L1EaO8MgymDcMI59fe7DnjLk0ZuDZiwO2J2	R0003	\N	\N
298	8	0	2	4	185	03	360	8	4	43	3	42	2	3	2	2	2	2	\N	88.00	69.00	87.00	- 	1	01	N	\N	0	N	N	N	N	N	N	2015-11-02	English	\N	\N	92349e8e-73bc-46cf-9ed2-5025cb2c4857	50c6dc1b-634f-4561-9271-7122ddf9390c	0	0	52	\N	\N	\N	64	\N	56	56	44	44	67	67	24	24	93	93	95	95	70	70	85	85	10	10	34	34	7 	7 	91	91	101	2	2	MXGplB4M232z2j4FNTStjIAKRER2ujJ8Z2OlV7Yn	R0001	\N	\N
299	55	0	2	7	203	03	38	10	1	3	2	5	2	2	3	2	2	2	\N	67.00	22.00	10.00	- 	1	01	Y	01	4	N	Y	Y	Y	Y	N	2015-11-02	English	\N	\N	ca37a6b7-71ae-43c1-8840-489b6badbfb5	dfb576c1-6916-41e2-a728-9320d200980c	0	0	41	\N	\N	\N	41	\N	46	46	7	7	23	23	62	62	15	15	0	0	22	22	98	98	88	88	6	6	6 	6 	64	64	66	49	2	SY0CEikzlomxMRW6NsjHhNuLpK17yruc5pjGavX8	R0003	\N	\N
300	36	0	2	5	191	03	285	3	3	33	2	29	1	3	2	1	3	2	\N	26.00	58.00	16.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	93e5e92a-27bb-4683-b84f-6f661c9e47a2	5a5c3cb1-4038-4519-b1a4-a2808840afc1	0	0	50	\N	\N	\N	63	\N	23	23	72	72	44	44	80	80	98	98	62	62	1	1	95	95	41	41	1	1	40	40	88	88	44	44	2	N1eKk2eGo15Htuapk7zDnIIftLz18aVE1PMPSktf	R0002	\N	\N
301	10	0	2	4	184	03	335	6	4	43	1	44	1	2	2	3	1	3	\N	86.00	93.00	77.00	Y 	1	01	Y	01	1	N	N	N	N	N	N	2015-11-02	English	\N	\N	13162932-2ee6-4d59-b595-fb23ff0b8efd	94c13427-7897-4e76-8fa7-26cdf3ee6e5b	0	0	46	\N	\N	\N	94	\N	38	38	92	92	12	12	59	59	55	55	4	4	62	62	46	46	56	56	75	75	0 	0 	39	39	101	101	2	5336b7258eb545b3ae7178aeb0cfd72428a36fdb	R0001	\N	\N
302	50	0	2	7	202	03	53	5	1	2	2	2	2	2	3	2	2	2	\N	22.00	90.00	25.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	57c8411c-3380-418e-b4b6-9da2dbccb6fb	dccdc883-cf47-49a4-9d74-0d0526e9be5d	0	0	22	\N	\N	\N	78	\N	70	70	36	36	45	45	30	30	20	20	34	34	46	46	32	32	92	92	68	68	5 	5 	24	24	66	20	2	QitKZjIhO0SovQYzrO9PfV1o1b0P0xRtKwJwFRqc	R0003	\N	\N
303	3	0	2	4	188	03	448	8	5	47	7	50	5	2	1	2	3	1	\N	10.00	43.00	36.00	- 	1	01	Y	01	6	N	Y	Y	Y	N	Y	2015-11-02	English	\N	\N	447eef05-927f-4a94-baaa-593c538366d0	deaf9408-c943-46bd-bbe0-1a79df79f227	0	0	17	\N	\N	\N	41	\N	30	30	16	16	57	57	98	98	15	15	41	41	78	78	8	8	3	3	10	10	20	20	98	98	101	37	2	vwkr0R9sEgA872YY02LOWZFwoTLq0ATfnEvZ0i17	R0001	\N	\N
304	28	0	2	5	192	03	132	10	2	16	2	15	1	1	1	1	1	3	\N	46.00	60.00	13.00	- 	1	01	N	01	3	N	N	N	N	N	N	2015-11-02	English	\N	\N	debff429-22ea-48a9-b5f0-feeddefc2e38	1bdd6b6e-9b36-47b0-8502-ac842e86dc80	0	0	27	\N	\N	\N	44	\N	89	89	29	29	92	92	43	43	27	27	56	56	20	20	53	53	97	97	18	18	3 	3 	36	36	75	59	2	pB7SBPI9whDTX6jqMTvlCa5POI7wiii3ik8YSqUN	R0002	\N	\N
305	89	0	2	7	197	03	222	2	3	37	2	33	2	1	3	1	1	1	\N	25.00	74.00	43.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	af618355-c475-4f32-a6da-ca83fc385dab	70883c09-7020-4d4c-81f8-bfb99f363517	0	0	25	\N	\N	\N	15	\N	47	47	61	61	6	6	73	73	85	85	91	91	93	93	42	42	29	29	69	69	22	22	58	58	95	95	2	c0pT5dPt1QuTicyE2IWo13jRf8kh2ep7ZI8flg9g	R0003	\N	\N
306	24	0	2	5	193	03	128	3	2	15	3	16	3	1	3	3	3	2	\N	25.00	23.00	18.00	- 	1	01	N	01	1	N	N	N	N	N	N	2015-11-02	English	\N	\N	48544429-c044-413a-8d7e-cbf933e6128e	dadb7cf2-ac24-4e07-96ad-9599c55d5a89	0	0	92	\N	\N	\N	88	\N	86	86	35	35	98	98	89	89	3	3	52	52	75	75	44	44	20	20	34	34	3 	3 	75	75	75	27	2	rMB447lffsMQCZOQPIUTPusiGmZolvpxn7jMAsiT	R0002	\N	\N
307	93	0	2	6	199	03	248	2	3	37	1	35	1	1	1	1	1	1	\N	14.00	36.00	25.00	- 	1	02	N	\N	0	Y	N	N	N	N	W	2015-11-02	English	\N	\N	7b12d2d9-0b6d-4f75-9d8c-398f80f58062	e3c74b24-345f-4177-907c-191804fab597	0	0	86	\N	\N	\N	53	\N	67	67	35	35	89	89	56	56	56	56	44	44	44	44	51	51	37	37	83	83	3 	3 	68	68	95	82	2	hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ	R0003	\N	\N
308	78	0	2	8	204	03	124	4	2	14	1	11	1	3	1	3	3	3	\N	18.00	23.00	35.00	- 	1	01	Y	\N	0	N	Y	Y	Y	Y	Y	2015-11-02	English	\N	\N	e13c29ed-0577-46bd-b6c0-86092c68a12b	7357ff31-682b-4136-ae3d-0ee55ac492b6	0	0	22	\N	\N	\N	5	\N	82	82	97	97	74	74	6	6	66	66	34	34	92	92	36	36	38	38	30	30	5 	5 	24	24	95	80	2	w7TmiOs6CWJsXS2l1crivMsf42ZlQ5WAr6Xri2rd	R0003	\N	\N
309	87	0	2	6	206	03	254	5	3	38	4	36	2	2	2	2	2	2	\N	91.00	74.00	51.00	- 	1	01	Y	01	5	N	N	N	N	N	N	2015-11-02	English	\N	\N	614e4fd1-5442-402f-82c8-3d2ea5f50e7b	4dd864b9-d6d4-4778-832c-2c005e31ba4a	0	0	87	\N	\N	\N	7	\N	32	32	52	52	97	97	49	49	26	26	43	43	49	49	38	38	33	33	67	67	44	44	57	57	95	84	2	zxITiM66QUVOcTyhRaooB0UIOWemGFP8eQE7xPSx	R0003	\N	\N
223	70	0	2	6	9	09	230	2	3	\N	2	\N	1	1	3	3	2	1	\N	4.00	55.00	31.00	Y 	1	01	Y	2 	16	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	2ff2eeef-b266-4240-8dca-d90ad4718be0	5ba7a545-46f3-4927-b2bf-2d77dc7ad021	0	0	72	\N	\N	\N	96	\N	57	57	38	38	70	70	16	16	66	66	59	59	69	69	23	23	73	73	83	83	4 	4 	73	73	160	160	105	sYbJYuJUBNDgRcwDUOr3LhFnvsw424uSYhmE0rx7	R0003	\N	\N
224	70	0	2	7	5	09	256	7	3	\N	3	\N	3	2	2	1	1	2	\N	63.00	71.00	58.00	Y 	1	01	Y	2 	45	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	4b310271-405c-4367-8cd6-7b8fe5a082aa	832507e9-fa2e-487f-a78d-43eb684d10cd	0	0	57	\N	\N	\N	28	\N	33	33	0	0	40	40	85	85	20	20	87	87	20	20	71	71	90	90	70	70	4 	4 	13	13	209	205	105	OjeIJyOY5Qyogd8ZPoAT4Lj6E1LmudgZcIrEeAvb	R0003	\N	\N
225	75	0	2	7	1	09	221	7	3	\N	2	\N	1	3	3	1	3	1	\N	99.00	87.00	35.00	Y 	1	01	Y	2 	66	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	fdfc61cd-c3ee-4b3a-b40e-fc47283a9e60	f3cc5821-2925-479c-b4ec-5f7d55b2ecf6	0	0	48	\N	\N	\N	84	\N	76	76	98	98	30	30	32	32	52	52	8	8	4	4	8	8	28	28	36	36	4 	4 	18	18	209	181	105	GuxIDl2Fjyjs81hW6n9gmufC4ruRxwcV8DCuKqbi	R0003	\N	\N
226	59	0	2	7	8	10	485	10	5	\N	5	\N	1	1	1	1	2	1	\N	79.00	44.00	98.00	Y 	1	01	Y	3 	57	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	1e71359d-b18d-4b83-a613-1a9f52f89d9e	79fcce2f-960e-4f41-a1bf-a91e7c5f1c70	0	0	25	\N	\N	\N	80	\N	54	54	8	8	13	13	33	33	71	71	40	40	22	22	97	97	54	54	72	72	4 	4 	80	80	195	195	105	sEorUakkZr2QwQgeQ1wCHxo8NwkksVvRlPXQV7cL	R0003	\N	\N
227	90	0	2	7	3	09	327	5	4	\N	1	\N	1	3	2	3	1	1	\N	43.00	77.00	11.00	Y 	1	03	N	3 	18	N	N	N	N	N	N	2015-11-02	Math	\N	\N	9569b28d-62ee-479a-8328-fd9850cbd779	610dadca-2794-4d8a-b720-282303787b6a	0	0	35	\N	\N	\N	22	\N	35	35	57	57	60	60	64	64	88	88	87	87	20	20	3	3	40	40	80	80	4 	4 	4 	4 	128	105	105	KDGutaOLUnOe1nGxReYkTttxEhi1iLsEL5CE8CPa	R0003	\N	\N
228	49	0	2	7	7	10	318	8	4	\N	2	\N	2	2	1	3	3	2	\N	43.00	69.00	5.00	Y 	1	02	N	4 	98	N	N	N	N	N	N	2015-11-02	Math	\N	\N	95a45098-3643-421f-9ef0-99e7e683b52a	eb09bcc2-155d-4396-935a-ed58e06d6c7b	0	0	8	\N	\N	\N	42	\N	61	61	72	72	45	45	22	22	14	14	15	15	31	31	3	3	87	87	79	79	5 	5 	72	72	209	164	105	Sc0cLMiRhBfkJsHF7Dwz1ozIPzc1k3s6aOgAD2IT	R0003	\N	\N
229	71	0	2	7	2	09	355	5	4	\N	2	\N	2	3	2	1	2	3	\N	99.00	85.00	40.00	Y 	1	01	N	2 	9	N	N	N	N	N	N	2015-11-02	Math	\N	\N	93133aef-f229-4574-98c6-fcc5189ebf93	48c25d83-21fb-4387-a6de-31c0a435e593	0	0	4	\N	\N	\N	21	\N	91	91	43	43	2	2	32	32	79	79	37	37	87	87	68	68	42	42	40	40	4 	4 	6 	6 	209	193	105	k1kUAyTitX2QYeasKtOPdOxbPCSIet5I00G6vQON	R0003	\N	\N
230	48	0	2	7	6	10	270	5	3	\N	3	\N	1	2	1	1	3	1	\N	22.00	2.00	74.00	Y 	1	01	Y	3 	38	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	0483d072-9b85-49b5-b542-95dbcfca1a06	8765f885-3a06-4ba1-9c72-3bcbdcbc8e5d	0	0	52	\N	\N	\N	88	\N	61	61	38	38	47	47	33	33	18	18	82	82	60	60	72	72	73	73	98	98	4 	4 	58	58	128	128	105	p2Sf5xBg8rAuTjy9XPGuFJaoYhdKIHISnVuRqoUd	R0003	\N	\N
231	76	0	2	7	4	11	172	5	2	\N	1	\N	1	3	3	1	1	2	\N	56.00	25.00	40.00	Y 	1	01	N	4 	74	N	N	N	N	N	N	2015-11-02	Math	\N	\N	35b4c08c-f392-4c2e-94b2-2472ac6909b8	43393e57-9a6e-48f6-9c75-8912777d0682	0	0	74	\N	\N	\N	80	\N	28	28	97	97	42	42	26	26	34	34	64	64	89	89	60	60	58	58	11	11	5 	5 	48	48	209	190	105	loKBQQ7EnGEJKKoK39nYLCNWtoJIDC2yNW9cPcmh	R0003	\N	\N
232	52	0	2	7	10	10	119	7	2	\N	6	\N	1	3	3	2	3	1	\N	66.00	22.00	28.00	Y 	1	03	N	3 	64	N	N	N	N	N	N	2015-11-02	Math	\N	\N	6a996b2d-7fe2-40ad-a34b-facf7b9034db	e80a8289-7d7f-4b9c-bcd5-30adfe364a8c	0	0	81	\N	\N	\N	65	\N	36	36	65	65	14	14	67	67	4	4	32	32	75	75	71	71	88	88	90	90	5 	5 	10	10	209	209	105	ZmrZFBiOmI2H2QuEsZm59MMu7bIAhKHUjge92f6E	R0003	\N	\N
233	37	0	2	5	42	09	202	10	3	\N	5	\N	1	1	2	3	2	1	\N	48.00	36.00	58.00	Y 	1	01	Y	3 	22	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	b681b620-edd4-4a50-affa-47774eff0914	c118b57c-4e79-4ff5-8833-4a85f227ad32	0	0	33	\N	\N	\N	7	\N	1	1	60	60	55	55	67	67	1	1	2	2	25	25	28	28	33	33	22	22	1 	1 	22	22	196	196	196	1SWd4Oky5wnWUNuH078iTM1reO6FjlIYmYkVHcQw	R0002	\N	\N
234	60	0	2	6	50	09	236	7	3	\N	7	\N	1	1	1	1	2	1	\N	93.00	90.00	54.00	Y 	1	01	Y	4 	9	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	905fdbd3-2337-457b-a38b-90fc87f615c2	415dd8d5-6dd7-4d53-a8a2-af044ecb3a94	0	0	25	\N	\N	\N	77	\N	47	47	65	65	88	88	7	7	3	3	23	23	26	26	16	16	22	22	37	37	1 	1 	64	64	203	135	196	Z94Y0ynyVyq3VCwLTjIRZHTZSYkFHT0lBN8meOgb	R0003	\N	\N
236	58	0	2	6	46	10	398	4	4	\N	4	\N	1	1	3	3	3	2	\N	88.00	40.00	48.00	Y 	1	01	N	5 	89	N	N	N	N	N	N	2015-11-02	Math	\N	\N	805d1318-86f8-49d7-aee4-527a22808b36	47ff60a1-ae5a-4683-9ae9-556f0ed9da14	0	0	97	\N	\N	\N	63	\N	65	65	94	94	11	11	20	20	98	98	2	2	59	59	95	95	99	99	47	47	3 	3 	69	69	203	183	196	maHNc5BQaVj9ddANVpBvFluBikLI3H06gqH3P1zi	R0003	\N	\N
237	41	0	2	6	45	11	156	4	2	\N	2	\N	1	1	2	3	3	3	\N	28.00	48.00	85.00	Y 	1	02	N	3 	5	Y	N	N	N	N	W	2015-11-02	Math	\N	\N	cf537852-5ba0-4af8-ad17-996d07c0a040	adb48129-8f51-4e89-a439-6a5a40f5c824	0	0	38	\N	\N	\N	68	\N	16	16	58	58	13	13	10	10	89	89	43	43	42	42	96	96	37	37	82	82	3 	3 	16	16	203	203	196	haGegI5PgD6OzaQ1Mu478Og0zw693ZmLAgQTbjLb	R0003	\N	\N
238	45	0	2	6	44	11	106	6	2	\N	5	\N	5	2	2	3	3	2	\N	46.00	67.00	27.00	Y 	1	03	N	1 	51	N	N	N	N	N	N	2015-11-02	Math	\N	\N	94a06c65-474b-423f-9eb8-59ac8cf496db	aae2b43b-2be1-42bc-98cd-4528d6e86927	0	0	78	\N	\N	\N	56	\N	88	88	2	2	71	71	57	57	29	29	94	94	44	44	39	39	43	43	15	15	3 	3 	78	78	204	204	196	E9fHez5Rp8SfdyyhzYgAlyLyIE1JYmpYL8d9Up4W	R0003	\N	\N
239	64	0	2	6	51	12	22	2	1	\N	1	\N	1	2	3	2	1	1	\N	45.00	15.00	50.00	Y 	1	02	N	4 	37	Y	N	N	N	N	W	2015-11-02	Math	\N	\N	c8d02a33-f11e-4f47-8bd8-7767951d2036	4f781890-fde1-4e57-9903-1789d63178e5	0	0	71	\N	\N	\N	41	\N	29	29	28	28	19	19	60	60	89	89	31	31	8	8	83	83	12	12	49	49	3 	3 	17	17	203	167	196	zoxqZgWkKoS9mT7YODBFwUZS48qNkoXzGPZjXUHi	R0003	\N	\N
240	61	0	2	6	43	12	58	1	1	\N	1	\N	1	2	3	2	1	1	\N	56.00	63.00	3.00	Y 	1	01	N	1 	7	N	N	N	N	N	N	2015-11-02	Math	\N	\N	7fec724b-1594-4e0e-9d08-a539a77b3c99	536c0db4-f91c-42df-b1eb-e6ce236fef92	0	0	34	\N	\N	\N	40	\N	66	66	63	63	26	26	61	61	69	69	87	87	64	64	34	34	58	58	78	78	3 	3 	13	13	203	109	196	Cnt3veDS2HW94jTAnU4bUASdLgIbP9y0lvQpQqer	R0003	\N	\N
241	42	0	2	6	48	11	7	1	1	\N	1	\N	1	3	3	3	3	1	\N	64.00	15.00	11.00	Y 	1	03	N	5 	23	N	N	N	N	N	N	2015-11-02	Math	\N	\N	81b4561e-6e70-47b4-8dfe-2898e550de7f	bf6f4e3d-e0da-4b22-b992-7e29f307aa30	0	0	13	\N	\N	\N	1	\N	56	56	44	44	79	79	9	9	64	64	22	22	68	68	74	74	57	57	40	40	3 	3 	30	30	203	129	196	U49Ivq4Y5D2NTB0EAuHv0sKhVA90oVkCwOgJErDW	R0003	\N	\N
242	86	0	2	6	49	11	121	2	2	\N	1	\N	1	2	2	3	2	3	\N	9.00	26.00	26.00	Y 	1	01	Y	2 	42	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	b2de9140-4fec-4ba0-807c-655d41d0a161	a9dfaaff-3136-4587-a763-05cb1360080f	0	0	57	\N	\N	\N	13	\N	49	49	27	27	25	25	39	39	40	40	74	74	74	74	49	49	41	41	48	48	3 	3 	71	71	203	114	196	wD6y2AQPgV3HtqM23RFtegCe56oenrPa6DdbElzI	R0003	\N	\N
243	7	0	2	4	41	11	330	2	4	\N	2	\N	1	1	1	1	3	3	\N	61.00	71.00	78.00	Y 	1	01	Y	4 	29	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	472bef1a-828a-48ef-9c46-8f3a3c235d0d	75893fb2-5cf5-4f94-8f25-d81aa058410e	0	0	53	\N	\N	\N	26	\N	0	0	83	83	9	9	28	28	22	22	28	28	99	99	94	94	83	83	88	88	33	33	57	57	175	175	196	RQaWFAiKM91B9AbbwRXpJzOo302G4qKWtzbpRano	R0001	\N	\N
244	17	0	2	4	38	11	340	6	4	\N	1	\N	1	2	2	3	3	2	\N	38.00	60.00	59.00	Y 	1	03	N	4 	12	Y	N	N	N	N	W	2015-11-02	Math	\N	\N	17c77770-7dde-4bfd-8ed8-2d24ffb665ee	b2439cf2-f045-404b-8386-ddef5969cf9e	0	0	7	\N	\N	\N	22	\N	39	39	51	51	62	62	68	68	94	94	32	32	5	5	77	77	4	4	36	36	23	23	10	10	188	188	196	6DRp6GlmW7bu4tn1PhckfVhL9MHH85oXIaSfRJep	R0001	\N	\N
245	6	0	2	4	39	11	223	4	3	\N	4	\N	3	3	3	3	1	1	\N	0.00	19.00	52.00	Y 	1	01	Y	1 	13	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	35c65034-b5b4-4129-b850-8d1fbf9f58ee	93e908b1-ee24-4e93-95bc-8debeb323b2e	0	0	64	\N	\N	\N	69	\N	45	45	77	77	20	20	3	3	42	42	15	15	56	56	75	75	21	21	1	1	22	22	57	57	198	198	196	IWr9RrKwQAe5iXf7W8xFiTf4fxYeWjWvoRJB7s45	R0001	\N	\N
246	13	0	2	4	40	11	315	2	4	\N	2	\N	1	3	2	2	2	3	\N	64.00	95.00	11.00	Y 	1	01	Y	1 	75	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	fa72a9f8-a5bc-4ed2-bffb-1817f65c3069	bc6202f4-1f8b-42dc-a10d-e2310948cd8e	0	0	88	\N	\N	\N	13	\N	89	89	39	39	68	68	24	24	74	74	6	6	74	74	56	56	82	82	24	24	1 	1 	7 	7 	198	143	196	p42AqWSux32Vo3RQzM34aioB8DbTdzDqfLRsAnhF	R0001	\N	\N
247	44	0	2	6	56	11	420	7	5	\N	5	\N	5	1	2	2	3	3	\N	10.00	88.00	78.00	Y 	1	02	N	3 	59	Y	N	N	N	N	W	2015-11-02	Math	\N	\N	e5c1fa54-2abd-4b9a-98e8-b3e13fa8f754	d98abfc9-21b2-4b59-8fc5-86c9a2ba7c7f	0	0	42	\N	\N	\N	87	\N	44	44	39	39	51	51	50	50	47	47	24	24	94	94	75	75	87	87	31	31	3 	3 	89	89	210	116	110	JRAWX24sIH3Bh2KcmpYNn6ZyGr8e6p6ctkAMFRw5	R0003	\N	\N
248	93	0	2	6	55	12	418	2	5	\N	2	\N	2	3	1	3	1	3	\N	81.00	10.00	77.00	Y 	1	02	N	3 	9	Y	N	N	N	N	W	2015-11-02	Math	\N	\N	7b12d2d9-0b6d-4f75-9d8c-398f80f58062	adce9869-85b7-442e-a54f-f3d5c254b979	0	0	86	\N	\N	\N	53	\N	67	67	35	35	89	89	56	56	56	56	44	44	44	44	51	51	37	37	83	83	3 	3 	68	68	210	140	110	hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ	R0003	\N	\N
249	87	0	2	6	61	12	430	1	5	\N	1	\N	1	1	3	2	3	1	\N	61.00	48.00	4.00	Y 	1	01	Y	4 	81	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	614e4fd1-5442-402f-82c8-3d2ea5f50e7b	426fac1d-394d-486e-b372-46b81d365b84	0	0	87	\N	\N	\N	7	\N	32	32	52	52	97	97	49	49	26	26	43	43	49	49	38	38	33	33	67	67	44	44	57	57	210	176	110	zxITiM66QUVOcTyhRaooB0UIOWemGFP8eQE7xPSx	R0003	\N	\N
250	82	0	2	6	58	11	321	9	4	\N	4	\N	3	1	2	2	2	1	\N	98.00	95.00	75.00	Y 	1	01	Y	4 	59	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	4d9d3bfc-6a13-4ebd-8c6c-8cf4581c3c5d	3fe51140-add4-4e0d-94cf-a13582cec21c	0	0	54	\N	\N	\N	69	\N	57	57	48	48	97	97	92	92	1	1	86	86	63	63	5	5	16	16	6	6	4 	4 	9 	9 	210	210	110	nHaADPpFPr1tpkcoS2Xj5mhmvdViLrumXuJMSox3	R0003	\N	\N
251	54	0	2	6	57	11	302	7	4	\N	5	\N	1	1	1	3	2	2	\N	21.00	82.00	82.00	Y 	1	02	N	5 	30	Y	N	N	N	N	W	2015-11-02	Math	\N	\N	5f6a86cc-94e7-453a-ae49-622325b6244a	fc9bec22-3451-4d41-b99e-20f56b5065ec	0	0	28	\N	\N	\N	4	\N	5	5	53	53	81	81	2	2	32	32	41	41	67	67	64	64	9	9	93	93	4 	4 	31	31	210	110	110	LaPd7vVIU9G9NvGr44WMWgwNwvupwIyWtC5kGetF	R0003	\N	\N
252	63	0	2	6	54	11	474	4	5	\N	1	\N	1	1	3	2	3	3	\N	48.00	72.00	50.00	Y 	1	01	Y	1 	78	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	7ea982c0-a412-49d6-a6b5-fe4d05c94eb3	8548789d-90de-42c4-b2a9-58710c1d56f8	0	0	48	\N	\N	\N	68	\N	21	21	76	76	76	76	37	37	45	45	30	30	68	68	10	10	89	89	11	11	4 	4 	60	60	210	185	110	cS2xAK3xDPCBR7ZWbAh9FS08IqjzWxgbZeitAa40	R0003	\N	\N
253	51	0	2	6	52	12	399	6	4	\N	5	\N	5	2	3	1	2	2	\N	6.00	83.00	53.00	Y 	1	01	Y	5 	62	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	96ba894d-7749-48f9-9ef5-3019f562f963	e4da7acc-cfb8-4ac7-91de-fde4641129c8	0	0	71	\N	\N	\N	73	\N	27	27	2	2	83	83	46	46	22	22	3	3	97	97	20	20	29	29	57	57	4 	4 	98	98	210	159	110	6xduCZW2oSUGSLzpiFpJQQK8YSAzmBrMjAh4WEkn	R0003	\N	\N
254	84	0	2	6	60	12	193	9	2	\N	5	\N	2	1	3	1	3	3	\N	84.00	26.00	87.00	Y 	1	02	N	1 	98	N	N	N	N	N	N	2015-11-02	Math	\N	\N	8d54686a-ff28-415a-8ee5-95cb999600f0	22373a08-96d1-480f-803f-0e12684416fa	0	0	98	\N	\N	\N	67	\N	35	35	89	89	27	27	73	73	10	10	69	69	40	40	57	57	89	89	86	86	4 	4 	68	68	210	199	110	wUfuK1C1vpevOq8cV6oqQxNNwJV1TbzeraL8fC4v	R0003	\N	\N
255	72	0	2	6	59	11	352	5	4	\N	3	\N	3	1	1	2	1	3	\N	98.00	48.00	71.00	Y 	1	01	Y	1 	70	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	be331185-b72d-48bf-9d36-e90ecee72e92	5c8ece25-8b9e-4578-a6f4-ed669986b9b5	0	0	68	\N	\N	\N	99	\N	61	61	83	83	85	85	90	90	5	5	4	4	8	8	99	99	57	57	63	63	4 	4 	32	32	210	123	110	UmOkwomla6wQuMu8rde3f3OkVwf23neTGCISpB28	R0003	\N	\N
256	57	0	2	6	53	09	230	5	3	\N	2	\N	1	1	1	2	2	2	\N	51.00	53.00	17.00	Y 	1	01	Y	1 	40	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	55b43c51-c038-41b3-aa6a-f6760c80afb4	8c42c4d8-ee1e-4a4b-90d6-37cf41e00154	0	0	7	\N	\N	\N	48	\N	26	26	79	79	1	1	88	88	24	24	35	35	32	32	30	30	17	17	30	30	4 	4 	42	42	210	186	110	Avrxze2AmndYm8dxmLJxDxhLsKpRgxaISvx3KS6F	R0003	\N	\N
257	9	0	2	4	97	12	379	1	4	\N	1	\N	1	2	3	1	2	1	\N	52.00	75.00	34.00	Y 	1	03	N	1 	99	Y	N	N	N	N	W	2015-11-02	Math	\N	\N	771d6032-621f-47b3-8113-2aeffcb7bffa	aeb7e402-410b-4cc6-b4d0-0cb82510a292	0	0	53	\N	\N	\N	4	\N	70	70	84	84	37	37	66	66	28	28	86	86	50	50	69	69	65	65	18	18	5 	5 	87	87	213	169	103	UMaYGd8hto4TYc7EiWKtivzti8YAWg5xozomFI8q	R0001	\N	\N
258	20	0	2	4	95	12	298	7	3	\N	2	\N	1	1	2	2	3	3	\N	3.00	58.00	8.00	Y 	1	02	N	1 	78	Y	N	N	N	N	W	2015-11-02	Math	\N	\N	bb63c6c4-07db-4b0a-974f-59fcaf94c189	a8889163-df6e-4493-8c62-eaab736a2e9e	0	0	3	\N	\N	\N	31	\N	71	71	96	96	11	11	72	72	21	21	92	92	34	34	64	64	45	45	81	81	3 	3 	25	25	213	120	103	qEy737ea1Emx97d0RTxk7QpDWecnUu2eEBWvQXkK	R0001	\N	\N
259	8	0	2	4	93	12	358	2	4	\N	2	\N	1	2	2	2	1	1	\N	25.00	22.00	34.00	Y 	1	01	N	1 	26	N	N	N	N	N	N	2015-11-02	Math	\N	\N	92349e8e-73bc-46cf-9ed2-5025cb2c4857	3b1e928f-e7d6-4730-b142-b27ba87c8942	0	0	52	\N	\N	\N	64	\N	56	56	44	44	67	67	24	24	93	93	95	95	70	70	85	85	10	10	34	34	7 	7 	91	91	213	141	103	MXGplB4M232z2j4FNTStjIAKRER2ujJ8Z2OlV7Yn	R0001	\N	\N
260	1	0	2	4	90	09	439	10	5	\N	7	\N	1	3	3	1	1	1	\N	73.00	22.00	3.00	Y 	1	02	N	1 	7	N	N	N	N	N	N	2015-11-02	Math	\N	\N	d0af2f64-adec-4ab8-b9e0-ce0f2e9631cf	2315ba50-22e9-4c45-9b26-a4b6f6808a2c	0	0	32	\N	\N	\N	90	\N	69	69	64	64	38	38	78	78	54	54	89	89	76	76	45	45	71	71	41	41	8 	8 	34	34	213	213	103	ITh1jmueUE6G8rMT9oKqLXZKZMPKahZUoAUuvOQE	R0001	\N	\N
261	4	0	2	4	96	10	299	5	3	\N	5	\N	4	1	1	2	2	3	\N	25.00	83.00	42.00	Y 	1	01	Y	2 	97	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	39d27e35-2285-4c06-83ab-a00f52537471	26226e3a-6b61-422a-9892-75753c89149e	0	0	26	\N	\N	\N	86	\N	46	46	84	84	37	37	27	27	19	19	0	0	42	42	60	60	48	48	37	37	6 	6 	91	91	124	124	103	RqHsCUovoRLSAhySU0AnRx0LY8YKin9uDhSFcOMD	R0001	\N	\N
262	12	0	2	4	92	11	118	2	2	\N	2	\N	2	2	3	3	3	1	\N	16.00	60.00	69.00	Y 	1	01	Y	2 	52	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	291c9cf8-bc4d-4af1-a900-2fa77efddd1b	05864e9c-4e09-4522-b1be-ab6b10683287	0	0	2	\N	\N	\N	8	\N	80	80	21	21	60	60	98	98	54	54	42	42	69	69	19	19	62	62	69	69	9 	9 	17	17	213	122	103	JL6uVW6OSVaYqpvndl0foBKAFrf4bdz6HACTTcLd	R0001	\N	\N
263	24	0	2	5	102	12	293	9	3	\N	3	\N	3	3	1	3	3	2	\N	37.00	72.00	91.00	Y 	1	01	N	3 	86	N	N	N	N	N	N	2015-11-02	Math	\N	\N	48544429-c044-413a-8d7e-cbf933e6128e	617f992f-b740-46a2-8cef-51deaa61e4dd	0	0	92	\N	\N	\N	88	\N	86	86	35	35	98	98	89	89	3	3	52	52	75	75	44	44	20	20	34	34	3 	3 	75	75	194	118	103	rMB447lffsMQCZOQPIUTPusiGmZolvpxn7jMAsiT	R0002	\N	\N
264	15	0	2	4	91	10	66	7	1	\N	2	\N	2	1	3	3	1	2	\N	90.00	76.00	61.00	Y 	1	01	Y	5 	26	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	bf21bb13-abd7-4ac3-89c9-8c3c7214ca4c	37919430-cf2a-48a0-bcc0-fda383f55269	0	0	100	\N	\N	\N	94	\N	8	8	20	20	0	0	15	15	45	45	80	80	89	89	42	42	56	56	68	68	4 	4 	1 	1 	137	137	103	jEEReGK9NLZr2teqi8dFBUhxaozkY3uwy5IB7hUx	R0001	\N	\N
265	3	0	2	4	98	12	363	8	4	\N	7	\N	6	1	1	1	2	2	\N	34.00	96.00	87.00	Y 	1	01	Y	2 	40	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	447eef05-927f-4a94-baaa-593c538366d0	25be4c66-e8ae-40bf-836d-473b9e1f9572	0	0	17	\N	\N	\N	41	\N	30	30	16	16	57	57	98	98	15	15	41	41	78	78	8	8	3	3	10	10	20	20	98	98	213	191	103	vwkr0R9sEgA872YY02LOWZFwoTLq0ATfnEvZ0i17	R0001	\N	\N
266	10	0	2	4	89	09	335	6	4	\N	1	\N	1	2	2	3	1	3	\N	86.00	93.00	77.00	Y 	1	01	Y	1 	1	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	13162932-2ee6-4d59-b595-fb23ff0b8efd	5fce8c4c-4bfd-4384-b213-b020e14cd99a	0	0	46	\N	\N	\N	94	\N	38	38	92	92	12	12	59	59	55	55	4	4	62	62	46	46	56	56	75	75	0 	0 	39	39	213	177	103	EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q	R0001	\N	\N
267	16	0	2	4	86	10	273	5	3	\N	3	\N	2	3	3	1	3	3	\N	55.00	49.00	61.00	Y 	1	02	N	4 	28	Y	N	N	N	N	W	2015-11-02	Math	\N	\N	9c74587a-e7c9-4d30-af66-15399b941ca7	c6aee337-ef60-4227-8769-c4e8f25842e6	0	0	33	\N	\N	\N	21	\N	38	38	84	84	94	94	86	86	59	59	31	31	35	35	52	52	73	73	49	49	5 	5 	34	34	213	155	103	1hIns0clANFCb9Rq3Cj5vEUUhMzryD9monuIwTFq	R0001	\N	\N
268	31	0	2	5	101	09	131	7	2	\N	4	\N	3	2	3	3	2	1	\N	33.00	64.00	39.00	Y 	1	01	N	2 	36	N	N	N	N	N	N	2015-11-02	Math	\N	\N	ba04ad27-ce18-42d8-ba7f-75bebee4ca6e	721d0233-ca2e-4561-b003-a26a30a592cd	0	0	37	\N	\N	\N	69	\N	59	59	96	96	60	60	98	98	87	87	74	74	35	35	99	99	40	40	84	84	10	10	55	55	130	130	103	mqZIVP2jMSn1VHrJ6nLaEHLblTiXW3eCC0BZDA6d	R0002	\N	\N
269	5	0	2	4	87	09	225	8	3	\N	3	\N	1	3	1	1	1	1	\N	37.00	76.00	65.00	Y 	1	02	N	2 	54	Y	N	N	N	N	W	2015-11-02	Math	\N	\N	d21f2726-9b67-4e37-845a-06fd98459c55	acb10f83-d802-4780-91b4-97a656528f11	0	0	7	\N	\N	\N	15	\N	26	26	45	45	70	70	49	49	45	45	15	15	96	96	53	53	4	4	38	38	1 	1 	83	83	213	178	103	2vM9yk86Q3OXKckaeaUjcSTHf1TaO5LCT2DHaeU7	R0001	\N	\N
270	2	0	2	4	99	12	61	5	1	\N	3	\N	3	1	1	3	1	1	\N	89.00	52.00	18.00	Y 	1	01	Y	1 	42	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	96cf2a1e-6a7b-4d81-bf20-3600c0e59e58	0aeb5cb8-7cce-42d2-9213-6c046161cf15	0	0	22	\N	\N	\N	83	\N	88	88	65	65	6	6	11	11	34	34	15	15	93	93	49	49	1	1	81	81	2 	2 	60	60	213	200	103	Xwk5McYQSIRVCwDpzpWzMfHYetWqy5BUNy6Z2ogV	R0001	\N	\N
271	27	0	2	5	100	09	146	4	2	\N	4	\N	4	3	3	3	3	1	\N	58.00	64.00	10.00	Y 	1	03	N	2 	87	N	N	N	N	N	N	2015-11-02	Math	\N	\N	207191be-0e63-4879-b5af-ec0d5b5c6426	85b0d5d9-b646-4881-a913-c887dc9a6112	0	0	21	\N	\N	\N	79	\N	47	47	83	83	12	12	32	32	27	27	62	62	58	58	7	7	83	83	18	18	11	11	2 	2 	130	103	103	Ew6FoKuAKYas7HNCC2qpWTLldXJFPKY6WuKGaoCK	R0002	\N	\N
272	23	0	2	5	103	10	398	1	4	\N	1	\N	1	2	2	2	2	2	\N	83.00	2.00	25.00	Y 	1	01	Y	2 	60	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	80770855-70a5-432b-921d-74930d96679d	8fa85f04-f1a7-4282-97b1-9d2e5c60afe7	0	0	57	\N	\N	\N	45	\N	13	13	97	97	40	40	39	39	1	1	82	82	37	37	35	35	5	5	14	14	2 	2 	48	48	194	194	103	uB6VLZuP4JnB0aZW3u9Ol9pUS9ux9VdAuAaQ19fX	R0002	\N	\N
273	11	0	2	4	88	09	164	1	2	\N	1	\N	1	2	2	3	3	1	\N	62.00	33.00	30.00	Y 	1	02	N	3 	54	Y	N	N	N	N	W	2015-11-02	Math	\N	\N	1b7f6c16-8846-47fb-b925-581b4bead589	062e5472-346e-4c95-a732-1cfe373e1683	0	0	19	\N	\N	\N	45	\N	92	92	42	42	94	94	69	69	41	41	50	50	45	45	97	97	64	64	28	28	2 	2 	54	54	213	111	103	dxpojnotJFaxas2YDJMiJoBPHkVCIc4lptuj24Dy	R0001	\N	\N
274	18	0	2	4	94	09	381	10	4	\N	6	\N	3	1	3	1	2	3	\N	42.00	6.00	8.00	Y 	1	01	Y	4 	26	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	1f4e4ff7-80f1-4fb8-87c6-4a6502bb24d7	f7091995-0540-4f7f-be3f-7467572b04cf	0	0	85	\N	\N	\N	32	\N	21	21	89	89	76	76	98	98	97	97	3	3	68	68	72	72	38	38	35	35	3 	3 	96	96	213	146	103	MyXTFn1S8qz5lQJAjw11CeaMjoaIiQbo8KihC5ix	R0001	\N	\N
275	46	0	2	8	105	11	110	8	2	\N	6	\N	2	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y	2 	4	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	11341062-4858-4bc4-a301-f94f95adeeae	0	0	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	107	107	206	DuncASxHMXlhtve8RRDsuUQ83LaR8gIZ7rFEP0zs	R0003	\N	\N
276	10	0	2	4	104	05	335	6	4	\N	1	\N	1	2	2	3	1	3	\N	86.00	93.00	77.00	Y 	1	01	Y	1 	1	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	13162932-2ee6-4d59-b595-fb23ff0b8efd	5d2f2e17-69d4-4f19-918e-5feeecdd18dc	0	0	46	\N	\N	\N	94	\N	38	38	92	92	12	12	59	59	55	55	4	4	62	62	46	46	56	56	75	75	0 	0 	39	39	206	206	206	5336b7258eb545b3ae7178aeb0cfd72428a36fdb	R0001	\N	\N
277	40	0	2	5	138	10	309	1	4	\N	1	\N	1	2	2	1	1	3	\N	31.00	73.00	77.00	Y 	1	01	Y	4 	23	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	28e46644-56b0-439d-aece-4a88f147b218	d3525cec-7727-4fd8-b961-16a1a592ffad	0	0	75	\N	\N	\N	26	\N	96	96	14	14	42	42	50	50	46	46	65	65	90	90	19	19	70	70	17	17	2 	2 	43	43	212	115	108	JskCum0I6ZULH064xsFeFcW4eZVTIEj5m6zExxG2	R0002	\N	\N
278	25	0	2	5	142	11	106	5	2	\N	1	\N	1	1	2	1	1	1	\N	31.00	0.00	77.00	Y 	1	01	Y	4 	8	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	db94ba6c-3abe-4e9a-8bbb-b680531efbab	03f78eb8-5818-4e39-b320-b1a0743696dc	0	0	15	\N	\N	\N	26	\N	96	96	58	58	15	15	47	47	10	10	35	35	88	88	27	27	67	67	39	39	3 	3 	80	80	165	165	108	p69xZhxSMQuWmke62h3j1K7izaj1evXeklkHFuMg	R0002	\N	\N
279	34	0	2	5	135	12	447	5	5	\N	3	\N	1	3	1	3	2	2	\N	78.00	71.00	77.00	Y 	1	01	Y	1 	70	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	54488102-dc6f-45fb-8941-14b9dc51c977	0a166472-d22d-4d7f-985f-1c70b5920ac4	0	0	18	\N	\N	\N	53	\N	63	63	23	23	96	96	26	26	5	5	63	63	4	4	39	39	14	14	22	22	4 	4 	20	20	212	125	108	3i8VkcJCs5QffVgj2QOmZr3MLv40I0KJc5k5DcWe	R0002	\N	\N
280	29	0	2	5	141	11	352	4	4	\N	1	\N	1	2	3	1	3	1	\N	73.00	6.00	12.00	Y 	1	01	Y	2 	35	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	30942862-4d08-4505-a85e-798d8065de42	b8683f57-f7b0-49e0-9d18-5ae7910fe323	0	0	5	\N	\N	\N	81	\N	32	32	22	22	80	80	73	73	4	4	67	67	27	27	29	29	39	39	43	43	5 	5 	27	27	212	113	108	OimZrYW80W7xl7QUa4Hg9Q9knjdvys1Z8wRIVHuG	R0002	\N	\N
281	32	0	2	5	148	12	270	10	3	\N	5	\N	4	2	1	3	1	3	\N	31.00	59.00	2.00	Y 	1	01	N	2 	73	N	N	N	N	N	N	2015-11-02	Math	\N	\N	75c44166-926f-4c5b-a208-42231bf5cd01	090b8eb7-ed22-48ff-9bb8-3ea33da0e724	0	0	89	\N	\N	\N	44	\N	16	16	37	37	3	3	10	10	53	53	62	62	7	7	72	72	60	60	93	93	6 	6 	58	58	212	172	108	WAhvqzYnESXaMgGYTK04IH1RPLeTatzuOubPg8U7	R0002	\N	\N
282	36	0	2	5	139	12	352	9	4	\N	9	\N	3	2	3	1	1	3	\N	98.00	4.00	41.00	Y 	1	01	Y	3 	17	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	93e5e92a-27bb-4683-b84f-6f661c9e47a2	47b9a539-930d-4371-91ab-4a123c81765b	0	0	50	\N	\N	\N	63	\N	23	23	72	72	44	44	80	80	98	98	62	62	1	1	95	95	41	41	1	1	40	40	88	88	189	166	108	N1eKk2eGo15Htuapk7zDnIIftLz18aVE1PMPSktf	R0002	\N	\N
283	33	0	2	5	140	11	451	5	5	\N	2	\N	1	2	3	1	3	2	\N	4.00	68.00	18.00	Y 	1	01	Y	3 	28	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	8559ab78-3be5-4ff9-8966-e8c4c6f3b8df	f771a54c-ba15-4724-b81e-e03ce7e257c2	0	0	86	\N	\N	\N	19	\N	19	19	84	84	62	62	2	2	58	58	92	92	83	83	57	57	92	92	22	22	6 	6 	72	72	212	201	108	oGYUvFLatZjgEBWy3LV9NwMdmc9GT1GTerNuABS9	R0002	\N	\N
284	21	0	2	5	144	11	131	8	2	\N	8	\N	3	3	1	2	1	1	\N	56.00	20.00	55.00	Y 	1	01	Y	4 	84	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	e56959c5-c419-45b3-a1ab-327e78b10aeb	1b488539-2999-44bb-9b84-e3152e387b71	0	0	53	\N	\N	\N	34	\N	67	67	54	54	87	87	64	64	30	30	43	43	89	89	98	98	21	21	18	18	55	55	12	12	212	133	108	sstpR8WRh7effxahZuK1ZPaQJmLH10nFDTSm23am	R0002	\N	\N
285	30	0	2	5	137	10	78	7	1	\N	3	\N	1	3	2	2	2	2	\N	1.00	71.00	95.00	Y 	1	01	Y	5 	84	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	307a76ba-cda8-43fc-8953-55795759032e	2b7d84f3-08b8-4a7c-b05b-285dcbfd1eb2	0	0	37	\N	\N	\N	54	\N	11	11	26	26	80	80	18	18	97	97	54	54	54	54	5	5	98	98	50	50	1 	1 	76	76	212	173	108	Fg5kGWmyfjDA600mHsVyZJALKlRwmg8pe1LtPdSb	R0002	\N	\N
286	35	0	2	5	145	11	403	3	5	\N	2	\N	2	1	3	2	2	3	\N	47.00	37.00	0.00	Y 	1	01	Y	4 	57	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	ce42c551-d724-4718-b769-31a796be9581	e9a4761f-f9b6-4147-bb0e-2e6c21ed3815	0	0	26	\N	\N	\N	46	\N	11	11	85	85	74	74	85	85	16	16	70	70	41	41	85	85	84	84	58	58	44	44	16	16	189	189	108	Uetza1KA3EqTldkdiq0w92KFYhVqBL6zLWinhXI9	R0002	\N	\N
287	22	0	2	5	147	09	324	9	4	\N	4	\N	4	2	1	3	1	3	\N	5.00	60.00	8.00	Y 	1	01	Y	1 	61	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	67c4ce05-a8cd-43f8-af8e-075fb70b3ecb	efcd37fa-0da7-4e2c-9647-3018df45354b	0	0	25	\N	\N	\N	93	\N	73	73	85	85	18	18	4	4	13	13	9	9	64	64	2	2	57	57	6	6	1 	1 	26	26	212	147	108	uJkYBTpWyQhQPsBC4PFyGIFqcoyKQiHxv4f3IYsu	R0002	\N	\N
288	28	0	2	5	143	12	318	8	4	\N	1	\N	1	2	1	1	3	2	\N	97.00	69.00	26.00	Y 	1	01	N	5 	64	N	N	N	N	N	N	2015-11-02	Math	\N	\N	debff429-22ea-48a9-b5f0-feeddefc2e38	4d064a91-3016-400d-be28-4d30dff82a3e	0	0	27	\N	\N	\N	44	\N	89	89	29	29	92	92	43	43	27	27	56	56	20	20	53	53	97	97	18	18	3 	3 	36	36	212	157	108	pB7SBPI9whDTX6jqMTvlCa5POI7wiii3ik8YSqUN	R0002	\N	\N
289	26	0	2	5	134	09	118	3	2	\N	2	\N	1	1	2	2	2	3	\N	80.00	63.00	42.00	Y 	1	03	N	2 	55	N	N	N	N	N	N	2015-11-02	Math	\N	\N	91b0e6a7-e546-4c7f-8d51-d1890957b3c8	fe1fd316-c11b-4a80-9770-6db56d3a755d	0	0	41	\N	\N	\N	18	\N	68	68	61	61	12	12	31	31	54	54	57	57	59	59	83	83	55	55	14	14	1 	1 	33	33	151	151	108	0YrEK4Y3PihDRZiZnFMinOcTdkNYwublx9Z8lCAp	R0002	\N	\N
290	39	0	2	5	136	10	273	3	3	\N	1	\N	1	1	3	2	3	1	\N	88.00	85.00	58.00	Y 	1	01	Y	5 	22	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	4243c86e-837a-4255-b667-7b4b8195862f	1113ce38-64b9-4bfb-a5c4-0d76034b85bc	0	0	24	\N	\N	\N	99	\N	13	13	34	34	40	40	14	14	59	59	55	55	98	98	80	80	89	89	78	78	2 	2 	16	16	212	108	108	EOHTQnkhMw4mzKP8Rpaq42vOlOBZzsZZkKU6zpll	R0002	\N	\N
291	38	0	2	5	146	10	312	7	4	\N	3	\N	1	2	3	1	1	2	\N	98.00	4.00	99.00	Y 	1	01	Y	5 	42	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	1ec80e05-8f1c-4a65-9349-14694bbbac78	e48992d5-d8b8-4025-82fd-8a2422b74106	0	0	94	\N	\N	\N	39	\N	17	17	13	13	22	22	82	82	76	76	78	78	1	1	67	67	30	30	67	67	1 	1 	62	62	212	212	108	UH69Q4IEQrjnQGvI9xhrNRTWUtmihpX8hlQ4r61X	R0002	\N	\N
292	89	0	2	7	155	12	439	7	5	\N	3	\N	1	1	1	2	3	1	\N	33.00	41.00	34.00	N 	1	01	Y	5 	21	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	af618355-c475-4f32-a6da-ca83fc385dab	25cdc95e-7b0d-4be0-be0a-fa9f6622c643	0	0	25	\N	\N	\N	15	\N	47	47	61	61	6	6	73	73	85	85	91	91	93	93	42	42	29	29	69	69	22	22	58	58	211	187	106	c0pT5dPt1QuTicyE2IWo13jRf8kh2ep7ZI8flg9g	R0003	\N	\N
293	91	0	2	7	181	11	164	5	2	\N	2	\N	2	2	3	1	3	2	\N	100.00	93.00	73.00	Y 	1	01	Y	3 	35	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	d5f2e34b-e706-402c-bc10-1e7bfa707f5f	0e3112ea-3aa6-46f6-b55b-7b2b4e58aae4	0	0	3	\N	\N	\N	14	\N	70	70	57	57	28	28	45	45	12	12	61	61	88	88	44	44	28	28	15	15	5 	5 	87	87	170	170	106	ZHBxgfZVT3KsM8I8iun3007MOqKZxIvz5HZht1dR	R0003	\N	\N
294	92	0	2	8	156	10	55	4	1	\N	4	\N	4	3	3	1	1	2	\N	37.00	86.00	60.00	N 	1	02	N	3 	33	Y	N	N	N	N	W	2015-11-02	Math	\N	\N	c2043a85-fb32-440a-8ca1-9f1f657aa4a2	fe9d4719-6154-4bdb-b453-70a638f2713d	0	0	78	\N	\N	\N	70	\N	64	64	47	47	59	59	5	5	65	65	78	78	61	61	1	1	91	91	92	92	4 	4 	50	50	211	197	106	DSoDwPOg0kZQ5YPnMJ0SLmBaCeGes4a1qjkRUQfd	R0003	\N	\N
295	57	0	2	8	172	09	349	8	4	\N	5	\N	1	1	2	2	1	3	\N	80.00	27.00	37.00	N 	1	03	N	5 	62	N	N	N	N	N	N	2015-11-02	Math	\N	\N	87546bfd-3a03-4365-ae75-c05ccb1258cc	0a5663b1-e04a-4d9b-b301-5799013cc00e	0	0	52	\N	\N	\N	37	\N	44	44	66	66	99	99	91	91	81	81	87	87	31	31	63	63	42	42	29	29	1 	1 	62	62	171	156	106	urU46yAVop0SQHyO35ugSyxZHrWM1NDDyJY3g4Uz	R0003	\N	\N
296	79	0	2	7	149	11	167	2	2	\N	1	\N	1	3	3	2	2	1	\N	92.00	14.00	49.00	Y 	1	01	Y	3 	46	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	4d767508-38b1-4927-9048-0a6fb4b5c2a5	f1bd3c77-4968-493b-af2e-64af7acf7160	0	0	4	\N	\N	\N	67	\N	54	54	86	86	21	21	65	65	18	18	60	60	51	51	52	52	68	68	99	99	5 	5 	66	66	211	148	106	4mzU3y7OgIchC8Rz9tfOk7KI8AcXlVtFPVOO1XFH	R0003	\N	\N
297	56	0	2	8	176	11	345	8	4	\N	5	\N	1	2	3	1	2	2	\N	80.00	20.00	44.00	N 	1	03	N	3 	21	Y	N	N	N	N	W	2015-11-02	Math	\N	\N	66e13a15-faaa-47d2-b082-a8d5e8aaea6c	d258c1aa-1e15-4a35-82d5-3626eba44e72	0	0	39	\N	\N	\N	35	\N	4	4	74	74	68	68	45	45	49	49	61	61	69	69	21	21	47	47	50	50	5 	5 	94	94	211	180	106	wTpuVRx1CP4eXjjQiCKXjjvvLaZpRAAujc0QB30k	R0003	\N	\N
298	94	0	2	8	152	12	441	10	5	\N	9	\N	3	3	2	2	3	1	\N	94.00	8.00	66.00	N 	1	01	Y	5 	8	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	22b58ccc-2418-4508-bc73-12b3ff68dac3	682893a2-d4e3-48fc-8988-a39a7fadd847	0	0	96	\N	\N	\N	68	\N	63	63	82	82	63	63	80	80	46	46	11	11	46	46	39	39	80	80	43	43	8 	8 	3 	3 	211	142	106	6boPVVoO1Vuyg8VJ5AqV0Zha4rVGrZt3uPrM7xgw	R0003	\N	\N
299	85	0	2	8	173	09	28	8	1	\N	1	\N	1	2	2	3	1	3	\N	25.00	85.00	31.00	N 	1	01	Y	1 	80	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	6d279ae3-e49a-4dcf-84f5-25374dc74213	2ee928e7-16cd-41e8-a410-424c1f1b389d	0	0	23	\N	\N	\N	79	\N	89	89	73	73	34	34	34	34	41	41	9	9	92	92	37	37	1	1	16	16	1 	1 	70	70	211	179	106	VfwdANZxA7Ak4zCPTWDwK9Zd47uiFTJp4rbBTmMs	R0003	\N	\N
300	78	0	2	8	174	12	2	3	1	\N	3	\N	1	2	1	2	1	2	\N	24.00	60.00	37.00	N 	1	01	Y	4 	76	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	e13c29ed-0577-46bd-b6c0-86092c68a12b	cc01e107-9239-43ae-95bd-96c8414578de	0	0	22	\N	\N	\N	5	\N	82	82	97	97	74	74	6	6	66	66	34	34	92	92	36	36	38	38	30	30	5 	5 	24	24	211	202	106	w7TmiOs6CWJsXS2l1crivMsf42ZlQ5WAr6Xri2rd	R0003	\N	\N
301	88	0	2	7	160	11	276	4	3	\N	4	\N	4	1	3	2	2	1	\N	59.00	58.00	1.00	Y 	1	01	Y	3 	47	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	0e9d8042-698b-4046-ba1b-34997540bdbc	fdf0ae08-2b04-4ab4-9389-a2f46aaf7b9d	0	0	67	\N	\N	\N	17	\N	2	2	14	14	35	35	7	7	8	8	3	3	56	56	43	43	10	10	53	53	5 	5 	54	54	211	131	106	iI34kG6uH1mQ83tbh5PEVVKoQjV1r96laDb4x10n	R0003	\N	\N
302	69	0	2	8	162	12	22	7	1	\N	3	\N	1	1	2	2	1	2	\N	31.00	34.00	62.00	N 	1	01	Y	1 	80	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	0b674c27-ad68-4597-91bc-01fe8e4d57d2	26e7906a-23f8-4e70-b869-a1a9703623ee	0	0	71	\N	\N	\N	72	\N	72	72	71	71	28	28	31	31	40	40	49	49	99	99	65	65	11	11	2	2	8 	8 	94	94	211	144	106	KL3R8L1EaO8MgymDcMI59fe7DnjLk0ZuDZiwO2J2	R0003	\N	\N
303	68	0	2	8	178	10	253	9	3	\N	5	\N	4	1	3	3	1	2	\N	67.00	46.00	21.00	N 	1	01	Y	1 	58	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	77c6d6a6-9b59-4189-b11b-75aedde7511c	84c7bec7-19f5-4c4b-b76a-da685a216f4e	0	0	45	\N	\N	\N	35	\N	23	23	4	4	22	22	43	43	74	74	54	54	82	82	90	90	97	97	27	27	1 	1 	52	52	171	106	106	xhHUuTmLdRjylYGiO2ZU54h2J04u9BJSn0exbdX2	R0003	\N	\N
304	74	0	2	8	157	12	456	7	5	\N	7	\N	1	1	3	1	3	1	\N	31.00	43.00	55.00	N 	1	01	Y	4 	90	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	eed942f3-d554-40f2-b547-446a9e1165bf	0f1cc7c9-72fb-4e1a-b5ed-babe3e67e731	0	0	1	\N	\N	\N	3	\N	30	30	78	78	28	28	16	16	61	61	2	2	63	63	39	39	72	72	58	58	5 	5 	59	59	171	136	106	e3OGNkIbM4h2skSI4nPuESDwrF1EMmjIfrprGMHz	R0003	\N	\N
305	55	0	2	7	170	12	22	10	1	\N	5	\N	3	3	3	3	2	1	\N	90.00	10.00	98.00	Y 	1	01	Y	4 	82	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	ca37a6b7-71ae-43c1-8840-489b6badbfb5	4e4cd9a1-3508-4e92-be17-06a078587a0d	0	0	41	\N	\N	\N	41	\N	46	46	7	7	23	23	62	62	15	15	0	0	22	22	98	98	88	88	6	6	6 	6 	64	64	171	139	106	SY0CEikzlomxMRW6NsjHhNuLpK17yruc5pjGavX8	R0003	\N	\N
306	46	0	2	8	163	11	119	9	2	\N	8	\N	2	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y	2 	4	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	29316d10-43dc-4203-b882-4eb9d9806f44	0	0	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	211	168	106	LQWpnLVTSTAXEFoit9Q2RlZNibkOFcpmSYgXMHyo	R0003	\N	\N
307	75	0	2	8	153	09	222	8	3	\N	5	\N	4	1	3	1	3	3	\N	7.00	46.00	50.00	N 	1	01	Y	1 	43	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	1694db1e-07a8-44a0-9bd6-92afa9204d8d	18a52a80-f329-4510-832c-7e4e6f731d45	0	0	14	\N	\N	\N	71	\N	46	46	32	32	97	97	12	12	71	71	6	6	5	5	61	61	17	17	25	25	1 	1 	83	83	211	145	106	8Kjp09Mfair9T4YbPnMaq7t35stJce29GjDxDHzD	R0003	\N	\N
308	43	0	2	8	161	11	47	10	1	\N	9	\N	8	1	2	1	3	3	\N	1.00	36.00	51.00	N 	1	02	N	4 	59	Y	N	N	N	N	W	2015-11-02	Math	\N	\N	1b03efd2-1d1e-4110-9350-22185753b6ba	bcb27126-7164-4c1e-aa16-5a267adc202b	0	0	79	\N	\N	\N	46	\N	46	46	17	17	56	56	92	92	66	66	50	50	21	21	28	28	5	5	25	25	58	58	66	66	211	154	106	jgliMmu6B15PbLy7sq7uRlv40qiIF3liNI3ySpru	R0003	\N	\N
309	50	0	2	7	168	12	55	5	1	\N	4	\N	3	1	1	1	3	3	\N	46.00	17.00	41.00	Y 	1	01	Y	4 	92	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	57c8411c-3380-418e-b4b6-9da2dbccb6fb	a849f26a-943e-4d82-8b6f-9d2fba044e23	0	0	22	\N	\N	\N	78	\N	70	70	36	36	45	45	30	30	20	20	34	34	46	46	32	32	92	92	68	68	5 	5 	24	24	171	161	106	QitKZjIhO0SovQYzrO9PfV1o1b0P0xRtKwJwFRqc	R0003	\N	\N
310	46	0	1	2	151	11	185	3	2	\N	1	\N	1	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y	2 	4	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	f11989d2-8ab6-4d39-a946-827c4fc69474	0	0	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	211	162	106	5RsVRnISVN942m5YIo50ucTxyB8cYNObQ1tnrORJ	R0003	\N	\N
311	67	0	2	8	180	09	202	10	3	\N	6	\N	6	2	3	2	1	2	\N	49.00	41.00	68.00	N 	1	01	Y	1 	60	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	3c3e4b7e-61e1-49dc-80d6-d29745ae2db3	84f9b073-9500-4065-a812-b98dd61081fd	0	0	7	\N	\N	\N	3	\N	82	82	56	56	87	87	90	90	0	0	31	31	57	57	83	83	74	74	50	50	1 	1 	67	67	211	119	106	xro1vzHRQAemvh8tb1iV4F2Yr9Tf5BmZI5F1UzlJ	R0003	\N	\N
312	66	0	2	8	165	09	494	9	5	\N	5	\N	5	2	3	2	1	2	\N	41.00	13.00	32.00	N 	1	01	N	1 	84	N	N	N	N	N	N	2015-11-02	Math	\N	\N	382b343d-1b02-4b96-b1cb-2644db2e9f09	df50c3b5-a7b7-4232-b5f9-b12e6d6b2d5d	0	0	97	\N	\N	\N	5	\N	7	7	55	55	75	75	7	7	91	91	55	55	15	15	6	6	39	39	78	78	8 	8 	91	91	211	149	106	o557yJ1Kx4YrfB8oMHfoaDVLMTFj3DJXWrKMm9zT	R0003	\N	\N
313	65	0	2	7	175	12	149	1	2	\N	1	\N	1	1	2	2	2	2	\N	42.00	42.00	92.00	Y 	1	01	Y	4 	80	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	8e2e1208-b860-4723-975e-c167831dce5c	acf55eaa-2755-4541-8eed-3096da068c7f	0	0	2	\N	\N	\N	38	\N	69	69	48	48	5	5	17	17	88	88	98	98	27	27	20	20	77	77	10	10	22	22	80	80	171	150	106	WQSZZ0TnQLwHd1VBV596z6mqD7T07BZiUgXDa2o2	R0003	\N	\N
314	46	0	1	2	158	11	199	5	2	\N	2	\N	2	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y	2 	4	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	9c4c9b2f-52c5-474e-87e8-89120e4863af	0	0	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	211	211	106	GXr7X73TjgGmTEm62Q3cGYSMBHE34YWvhwJEalyy	R0003	\N	\N
316	47	0	2	8	177	10	281	3	3	\N	3	\N	3	2	3	3	1	1	\N	18.00	9.00	19.00	N 	1	03	N	2 	67	Y	N	N	N	N	W	2015-11-02	Math	\N	\N	1101d194-08b9-4632-b655-513319ed9a07	29c7e4cb-901c-4dfb-9fb2-b02470a1e261	0	0	55	\N	\N	\N	19	\N	84	84	70	70	92	92	59	59	62	62	92	92	24	24	89	89	24	24	97	97	8 	8 	29	29	171	171	106	WW9iCRpiTricUbWoelchVevYIJHSjhAUMpLVYt6D	R0003	\N	\N
317	85	0	2	7	150	09	65	1	1	\N	1	\N	1	3	3	3	3	2	\N	80.00	86.00	77.00	Y 	1	03	N	4 	46	Y	N	N	N	N	W	2015-11-02	Math	\N	\N	6e0b03a7-fb69-4025-b91e-c076e171f1c3	dc642ad5-1a8a-481d-9b62-13cbf7d55262	0	0	49	\N	\N	\N	17	\N	76	76	6	6	10	10	71	71	13	13	6	6	84	84	58	58	4	4	46	46	33	33	20	20	211	117	106	4xNcr39DqN64ORBLFyIG322oypneHyCBYVXKZZPm	R0003	\N	\N
318	46	0	1	2	164	11	113	4	2	\N	3	\N	1	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y	2 	4	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	d566e123-c622-4a3b-8e3e-1f7b5f21ee58	0	0	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	211	174	106	m5i2mSxYFwrFTnW09BEYlrXBvQIw3bTmo2zm1GaI	R0003	\N	\N
319	81	0	2	8	154	10	114	5	2	\N	2	\N	1	1	1	1	3	2	\N	33.00	24.00	13.00	N 	1	01	Y	2 	16	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	76040d10-ba95-444b-a29e-da65a3679d01	4a0491b6-99cc-49e0-947b-c1729ba19299	0	0	57	\N	\N	\N	19	\N	48	48	80	80	89	89	92	92	64	64	15	15	75	75	91	91	50	50	98	98	1 	1 	97	97	211	112	106	8qadBD3xrsvpfExXtJw6dHilQMcdZHjWg60GuV8D	R0003	\N	\N
320	70	0	2	7	159	09	38	1	1	\N	1	\N	1	3	1	3	2	3	\N	8.00	5.00	64.00	Y 	1	02	N	5 	93	N	N	N	N	N	N	2015-11-02	Math	\N	\N	30bb2e2c-1aec-4ea4-9069-f3a947ac13cd	24851366-6aca-47f7-9918-a1bf41203ad2	0	0	27	\N	\N	\N	35	\N	97	97	71	71	46	46	91	91	13	13	20	20	19	19	11	11	16	16	72	72	5 	5 	52	52	211	126	106	GxZdYAWx8b0sf9CbdWYT8HsXYqBnNikEkDMikPkd	R0003	\N	\N
321	77	0	2	8	171	11	299	4	3	\N	2	\N	2	1	1	3	3	3	\N	68.00	74.00	33.00	N 	1	01	Y	3 	21	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	f620a09f-9103-4446-b234-1c736f15500c	ad2f4799-f9ad-42a8-b72b-571d7ae93e4f	0	0	42	\N	\N	\N	73	\N	1	1	8	8	74	74	8	8	0	0	68	68	83	83	7	7	20	20	17	17	8 	8 	21	21	211	138	106	Uk2E8MeExKfyl6XHBLKpNJe6na8bi3nj8yMCgeFz	R0003	\N	\N
322	83	0	2	8	179	11	451	9	5	\N	1	\N	1	3	3	2	2	2	\N	62.00	50.00	17.00	N 	1	01	Y	2 	94	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	2aff6dcf-e2de-4ec4-9212-a761f29e0e6f	2cee117a-37dc-4fd5-a76c-e7d39112ae8c	0	0	89	\N	\N	\N	97	\N	55	55	4	4	16	16	74	74	75	75	59	59	33	33	59	59	32	32	82	82	1 	1 	33	33	171	152	106	xkO5Umv7TjxiZY8CD926MY27uoz1edQFcaWU76Ef	R0003	\N	\N
323	53	0	2	7	169	10	127	2	2	\N	1	\N	1	2	2	2	3	3	\N	8.00	16.00	28.00	Y 	1	02	N	5 	2	Y	N	N	N	N	W	2015-11-02	Math	\N	\N	028647c5-df78-46dc-b0b9-760586f55fc8	946c7229-b0c2-4adc-8655-21ac5a0f483e	0	0	44	\N	\N	\N	62	\N	7	7	84	84	82	82	11	11	20	20	25	25	12	12	41	41	56	56	41	41	5 	5 	94	94	211	208	106	sB2VXSoaEMtiSg4WjFUXNNdh3rDSEvULHmGXvAnG	R0003	\N	\N
324	80	0	2	8	166	12	121	1	2	\N	1	\N	1	2	2	3	2	2	\N	78.00	27.00	23.00	N 	1	01	Y	4 	49	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	a16f02a7-151a-4b29-8203-8568a8746591	796e9f05-298a-436f-8c92-3539c4789c78	0	0	56	\N	\N	\N	100	\N	88	88	21	21	91	91	51	51	58	58	64	64	36	36	50	50	67	67	84	84	8 	8 	86	86	211	158	106	oBPTLwB89Yetjpa88VqxOvlIENOqe4XKGKnOrJfM	R0003	\N	\N
325	46	0	2	8	164	11	116	4	2	\N	3	\N	1	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y	2 	4	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	a4f811f7-1919-43d5-98ac-78b89f5fcf53	0	0	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	134	134	104	m5i2mSxYFwrFTnW09BEYlrXBvQIw3bTmo2zm1GaI	R0003	\N	\N
326	46	0	2	8	183	11	146	3	2	\N	3	\N	1	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y	2 	4	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	506a05b5-c8c9-4170-a12c-e2542eb4a0c3	0	0	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	134	127	104	D3de8PGASi2QhIZ0zHLSNJXZI0okSm1VqPJMeJAw	R0003	\N	\N
327	46	0	2	8	182	11	175	7	2	\N	1	\N	1	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y	2 	4	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	4eb74f59-53d1-41fa-bf2b-33031e9fd851	0	0	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	134	104	104	CYh72rsxfT7A4jmupxUkrb3S76bTra2BmlgrR57F	R0003	\N	\N
328	14	0	2	4	208	10	270	5	3	\N	1	\N	1	3	3	1	1	2	\N	98.00	37.00	62.00	Y 	1	01	Y	4 	41	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	cdae1117-12b9-4077-be15-af2eb5db0e7a	6790bcd5-b5a3-470c-a715-3918d86b0418	0	0	76	\N	\N	\N	26	\N	36	36	5	5	31	31	24	24	70	70	58	58	38	38	81	81	45	45	8	8	6 	6 	57	57	207	153	121	SBFMQrkvsLNLQWnEXVVeN6ZMQXqemPhTY44hgO2M	R0001	\N	\N
329	19	0	2	4	207	10	53	5	1	\N	4	\N	1	3	2	3	1	1	\N	59.00	22.00	99.00	Y 	1	01	N	4 	73	N	N	N	N	N	N	2015-11-02	Math	\N	\N	89450f20-5621-4cc8-bd71-709834a8631e	5e07c335-586b-4de1-926d-9f6e5bcbf36d	0	0	1	\N	\N	\N	41	\N	22	22	53	53	24	24	44	44	60	60	80	80	7	7	60	60	94	94	48	48	10	10	48	48	207	207	121	OIeWicvTvk5kcbEc0qj7vPo2nh3Vr8XS9e2Vz57b	R0001	\N	\N
330	46	0	2	8	212	11	193	5	2	\N	2	\N	2	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y	2 	4	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	9081f4ad-1e35-4b9d-a6e1-669e07d7e727	0	0	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	184	132	121	GXr7X73TjgGmTEm62Q3cGYSMBHE34YWvhwJEalyy	R0003	\N	\N
331	46	0	2	8	210	11	164	3	2	\N	1	\N	1	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y	2 	4	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	dfc56f94-6e12-4a87-971d-c564c3500050	0	0	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	184	163	121	FkgQ5OyZAMUDgnC8f6cJHnpLkzOlGV6xnArV3Bhf	R0003	\N	\N
332	46	0	2	8	209	11	191	3	2	\N	1	\N	1	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y	2 	4	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	3a892c83-2897-4303-9308-a7b060643eb0	0	0	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	184	184	121	5RsVRnISVN942m5YIo50ucTxyB8cYNObQ1tnrORJ	R0003	\N	\N
333	46	0	2	8	211	11	129	6	2	\N	3	\N	1	2	1	2	3	1	\N	74.00	92.00	5.00	N 	1	01	Y	2 	4	N	Y	Y	Y	Y	Y	2015-11-02	Math	\N	\N	088b45b9-5cdf-4b9c-b414-44b971a00dd3	9d9d732f-d5bc-4c91-aa5a-308663dfa606	0	0	54	\N	\N	\N	68	\N	21	21	87	87	78	78	38	38	2	2	26	26	66	66	69	69	44	44	69	69	9 	9 	51	51	184	121	121	GFWLlMJ65UE7pSrpAPrt2Tu5hoZkaeJdY0mrbTTU	R0003	\N	\N
\.


--
-- Name: fact_sum_resp_school_key_seq; Type: SEQUENCE SET; Schema: analytics; Owner: edware
--

SELECT pg_catalog.setval('fact_sum_resp_school_key_seq', 1, false);


--
-- Name: dim_ela_accomod_pk; Type: CONSTRAINT; Schema: analytics; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_accomod
    ADD CONSTRAINT dim_ela_accomod_pk PRIMARY KEY (_key_accomod);


--
-- Name: dim_institution_pk; Type: CONSTRAINT; Schema: analytics; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_school
    ADD CONSTRAINT dim_institution_pk PRIMARY KEY (_key_school);


--
-- Name: dim_opt_state_data_pk; Type: CONSTRAINT; Schema: analytics; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_opt_state_data
    ADD CONSTRAINT dim_opt_state_data_pk PRIMARY KEY (_key_opt_state_data);


--
-- Name: dim_poy_pk; Type: CONSTRAINT; Schema: analytics; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_poy
    ADD CONSTRAINT dim_poy_pk PRIMARY KEY (_key_poy);


--
-- Name: dim_student_pk; Type: CONSTRAINT; Schema: analytics; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_student
    ADD CONSTRAINT dim_student_pk PRIMARY KEY (_key_student);


--
-- Name: dim_test_pk; Type: CONSTRAINT; Schema: analytics; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_test
    ADD CONSTRAINT dim_test_pk PRIMARY KEY (_key_test);


--
-- Name: dim_unique_test_pkey; Type: CONSTRAINT; Schema: analytics; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_unique_test
    ADD CONSTRAINT dim_unique_test_pkey PRIMARY KEY (_key_unique_test);


--
-- Name: eoy_attempt_idx; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX eoy_attempt_idx ON fact_sum USING btree (eoy_attempt_flag);


--
-- Name: eoy_test_idx; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX eoy_test_idx ON fact_sum USING btree (_key_eoy_test);


--
-- Name: eoy_test_school_idx; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX eoy_test_school_idx ON fact_sum USING btree (_key_eoy_test_school);


--
-- Name: fact_sum_ix1; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX fact_sum_ix1 ON fact_sum USING btree (_key_accomod);


--
-- Name: fact_sum_ix2; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX fact_sum_ix2 ON fact_sum USING btree (_key_poy);


--
-- Name: fact_sum_ix8; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX fact_sum_ix8 ON fact_sum USING btree (_key_resp_school);


--
-- Name: fact_sum_ix9; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX fact_sum_ix9 ON fact_sum USING btree (_key_student);


--
-- Name: idx_dim_institution_lookup; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX idx_dim_institution_lookup ON dim_school USING btree (school_id);


--
-- Name: idx_dim_poy_lookup; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX idx_dim_poy_lookup ON dim_poy USING btree (poy, school_year);


--
-- Name: idx_dim_student_lookup; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX idx_dim_student_lookup ON dim_student USING btree (student_parcc_id);


--
-- Name: idx_dim_test_lookup; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX idx_dim_test_lookup ON dim_test USING btree (test_code, test_category);


--
-- Name: me_idx; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX me_idx ON fact_sum USING btree (me_flag);


--
-- Name: multirecord_idx; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX multirecord_idx ON fact_sum USING btree (multirecord_flag);


--
-- Name: pba_attempt_idx; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX pba_attempt_idx ON fact_sum USING btree (pba_attempt_flag);


--
-- Name: pba_test_idx; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX pba_test_idx ON fact_sum USING btree (_key_pba_test);


--
-- Name: pba_test_school_idx; Type: INDEX; Schema: analytics; Owner: edware; Tablespace: 
--

CREATE INDEX pba_test_school_idx ON fact_sum USING btree (_key_pba_test_school);


--
-- Name: fact_sum_acc; Type: FK CONSTRAINT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fact_sum_acc FOREIGN KEY (_key_accomod) REFERENCES dim_accomod(_key_accomod);


--
-- Name: fact_sum_eoy_test; Type: FK CONSTRAINT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fact_sum_eoy_test FOREIGN KEY (_key_eoy_test) REFERENCES dim_test(_key_test);


--
-- Name: fact_sum_fk4; Type: FK CONSTRAINT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fact_sum_fk4 FOREIGN KEY (_key_student) REFERENCES dim_student(_key_student);


--
-- Name: fact_sum_fk6; Type: FK CONSTRAINT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fact_sum_fk6 FOREIGN KEY (_key_poy) REFERENCES dim_poy(_key_poy);


--
-- Name: fact_sum_fk8; Type: FK CONSTRAINT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fact_sum_fk8 FOREIGN KEY (_key_resp_school) REFERENCES dim_school(_key_school);


--
-- Name: fact_sum_pba_test; Type: FK CONSTRAINT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fact_sum_pba_test FOREIGN KEY (_key_pba_test) REFERENCES dim_test(_key_test);


--
-- Name: fk_unique_test; Type: FK CONSTRAINT; Schema: analytics; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fk_unique_test FOREIGN KEY (_key_unique_test) REFERENCES dim_unique_test(_key_unique_test);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

