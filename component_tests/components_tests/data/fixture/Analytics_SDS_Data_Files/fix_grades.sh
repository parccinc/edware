# this script is needed to alter the generated analytics SDS data until we can fix the sorting issues in Pentaho (DE1358)

for file in cat_analytics.sql dog_analytics.sql
do
    for grade in 3 4 5 6 7 8 9
    do
        sed -i.bak "s/Grade ${grade}/0${grade}/g" $file
        rm $file.bak
    done
    for grade in 10 11
    do
        sed -i.bak "s/Grade ${grade}/${grade}/g" $file
        rm $file.bak
    done
done
