CREATE TABLE ***schema_name***.int_form_group(
    asmt_guid            varchar(50)     NOT NULL,
    form_guid            varchar(50)     NOT NULL,
    group_guid           varchar(50)     NOT NULL,
    group_code           varchar(50),
    group_name           varchar(60),
    group_desc           varchar(256),
    group_type           varchar(30),
    parent_group_guid    varchar(50),
    group_ref_id         varchar(60),
    batch_guid           varchar(50)     NOT NULL,
    record_num           bigint          NOT NULL,
    rec_status           varchar(1)      DEFAULT 'C' NOT NULL,
    create_date          date            DEFAULT CURRENT_DATE NOT NULL
)
