CREATE TABLE ***schema_name***.int_wrong_table(
    some_field         varchar(50)    NOT NULL,
    other_field        varchar(60),
    batch_guid         varchar(50)    NOT NULL,
    rec_status         varchar(1)     DEFAULT 'C' NOT NULL,
    create_date        date           DEFAULT CURRENT_DATE NOT NULL
)
