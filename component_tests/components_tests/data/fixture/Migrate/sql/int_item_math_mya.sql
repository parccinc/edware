CREATE TABLE ***schema_name***.int_item_math_mya(
    asmt_attempt_guid     varchar(36)      NOT NULL,
    item_guid             varchar(50)      NOT NULL,
    student_parcc_id      varchar(50),
    student_item_score    numeric(4, 0),
    batch_guid            varchar(50)      NOT NULL,
    record_num            int4             NOT NULL,
    rec_status            varchar(1)       DEFAULT 'C' NOT NULL,
    create_date           date             DEFAULT CURRENT_DATE NOT NULL
)
