CREATE TABLE ***schema_name***.int_file_store(
    admin_guid     varchar(50)     NOT NULL,
    asmt_guid      varchar(50)     NOT NULL,
    file_type      varchar(10)     NOT NULL,
    page_no        bigint          NOT NULL,
    file_name      varchar(150)    NOT NULL,
    file_data      bytea           NOT NULL,
    batch_guid     varchar(50)     NOT NULL,
    rec_status     varchar(1)      DEFAULT 'C' NOT NULL,
    create_date    date            DEFAULT CURRENT_DATE NOT NULL
)
