CREATE TABLE ***schema_name***.int_asmt_lvl(
    asmt_guid          varchar(50)    NOT NULL,
    asmt_lvl_design    char(2)        NOT NULL,
    batch_guid         varchar(50)    NOT NULL,
    rec_status         varchar(1)     DEFAULT 'C' NOT NULL,
    create_date        date           DEFAULT CURRENT_DATE NOT NULL
)
