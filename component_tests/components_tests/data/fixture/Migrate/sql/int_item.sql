CREATE TABLE ***schema_name***.int_item(
    item_guid           varchar(50)     NOT NULL,
    item_name           varchar(150)    NOT NULL,
    item_descript       varchar(250),
    item_ref_id         varchar(60)     NOT NULL,
    item_max_points     varchar(30),
    item_version        varchar(30),
    test_item_type      varchar(60),
    is_item_omitted     varchar(30),
    item_status         varchar(30),
    parent_item_guid    varchar(50)     NOT NULL,
    batch_guid          varchar(50)     NOT NULL,
    rec_status          varchar(1)      DEFAULT 'C' NOT NULL,
    create_date         date            DEFAULT CURRENT_DATE NOT NULL
)
