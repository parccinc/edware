CREATE TABLE ***schema_name***.int_item_ela_decod(
    batch_guid            varchar(50)      NOT NULL,
    record_num            int4             NOT NULL,
    asmt_attempt_guid     varchar(36)      NOT NULL,
    item_guid             varchar(50)      NOT NULL,
    student_parcc_id      varchar(40),
    student_item_score    numeric(4, 0),
    select_key            varchar(250),
    item_seq              int2,
    rec_status            varchar(1)       DEFAULT 'C' NOT NULL,
    create_date           date             DEFAULT CURRENT_DATE NOT NULL
)
