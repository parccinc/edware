CREATE TABLE ***schema_name***.int_asmt(
    asmt_guid       varchar(50)    NOT NULL,
    admin_guid      varchar(50)    NOT NULL,
    asmt_title      varchar(60),
    asmt_subject    varchar(15),
    batch_guid      varchar(50)    NOT NULL,
    rec_status      varchar(1)     DEFAULT 'C' NOT NULL,
    create_date     date           DEFAULT CURRENT_DATE NOT NULL
)
