CREATE TABLE ***schema_name***.int_sum_test_score_m(
    test_code        varchar(20)    NOT NULL,
    sum_period       varchar(20)    NOT NULL,
    batch_guid       varchar(50)    NOT NULL,
    rec_status       varchar(1)     DEFAULT 'C' NOT NULL,
    create_date      date           DEFAULT CURRENT_DATE NOT NULL
)