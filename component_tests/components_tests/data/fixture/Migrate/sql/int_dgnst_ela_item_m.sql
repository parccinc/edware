CREATE TABLE ***schema_name***.int_dgnst_ela_item_m(
    item_guid             varchar(50)     NOT NULL,
    asmt_guid             varchar(50)     NOT NULL,
    subclaim_name         varchar(30),
    item_name             varchar(150),
    item_descript         varchar(250),
    test_item_type        varchar(60),
    passage_type          varchar(40),
    text_complexity       varchar(40),
    content_standard      varchar(40),
    evidence_stmt1        varchar(40),
    evidence_stmt2        varchar(40),
    evidence_stmt3        varchar(40),
    word_count            int2,
    ela_correct_answer    varchar(250),
    batch_guid            varchar(50)     NOT NULL,
    rec_status            varchar(1)      DEFAULT 'C' NOT NULL,
    create_date           date            DEFAULT CURRENT_DATE NOT NULL
)
