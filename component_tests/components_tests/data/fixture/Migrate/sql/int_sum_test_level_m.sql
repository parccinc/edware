CREATE TABLE ***schema_name***.int_sum_test_level_m(
    test_code         varchar(20)      NOT NULL,
    perf_lvl_id       varchar(30)      NOT NULL,
    cutpoint_label    varchar(50)      NOT NULL,
    cutpoint_low      numeric(4, 0)    NOT NULL,
    cutpoint_upper    numeric(4, 0)    NOT NULL,
    batch_guid        varchar(50)      NOT NULL,
    rec_status        varchar(1)       DEFAULT 'C' NOT NULL,
    create_date       date             DEFAULT CURRENT_DATE NOT NULL
)