CREATE TABLE ***schema_name***.int_student_task_snl(
    batch_guid             varchar(50)      NOT NULL,
    record_num             int4             NOT NULL,
    asmt_attempt_guid      varchar(36),
    student_parcc_id       varchar(50),
    task_guid              varchar(50),
    task_name              varchar(60),
    total_task_score       numeric(4, 0),
    observation            varchar(250),
    dim_score1             numeric(4, 0),
    dim_score2             numeric(4, 0),
    dim_score3             numeric(4, 0),
    dim_score4             numeric(4, 0),
    dim_score5             numeric(4, 0),
    dr_evidence_observ1    char(1),
    dr_evidence_observ2    char(1),
    dr_evidence_observ3    char(1),
    dr_evidence_observ4    char(1),
    dr_evidence_observ5    char(1),
    rec_status             char(1)          DEFAULT 'C' NOT NULL,
    create_date            date             DEFAULT CURRENT_DATE NOT NULL
)
