CREATE TABLE ***schema_name***.int_snl_asmt_mode_m(
    asmt_guid      varchar(50)    NOT NULL,
    asmt_mode      varchar(5)     NOT NULL,
    batch_guid     varchar(50)    NOT NULL,
    rec_status     varchar(1)     DEFAULT 'C' NOT NULL,
    create_date    date           DEFAULT CURRENT_DATE NOT NULL
)
