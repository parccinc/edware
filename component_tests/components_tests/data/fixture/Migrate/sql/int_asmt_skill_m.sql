CREATE TABLE ***schema_name***.int_asmt_skill_m(
    skill_guid               varchar(36)    NOT NULL,
    asmt_guid                varchar(50)    NOT NULL,
    skill_name               varchar(40)    NOT NULL,
    batch_guid               varchar(50)    NOT NULL,
    max_number_per_skill     int2,
    total_number_of_items    int4,
    rec_status               varchar(1)     DEFAULT 'C' NOT NULL,
    create_date              date           DEFAULT CURRENT_DATE NOT NULL
)
