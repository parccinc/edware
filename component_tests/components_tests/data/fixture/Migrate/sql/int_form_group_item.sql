CREATE TABLE ***schema_name***.int_form_group_item(
    asmt_guid      varchar(50)    NOT NULL,
    form_guid      varchar(50)    NOT NULL,
    group_guid     varchar(50)    NOT NULL,
    item_guid      varchar(50)    NOT NULL,
    batch_guid     varchar(50)    NOT NULL,
    record_num     bigint         NOT NULL,
    rec_status     varchar(1)     DEFAULT 'C' NOT NULL,
    create_date    date           DEFAULT CURRENT_DATE NOT NULL
)
