CREATE TABLE ***schema_name***.int_admin(
    admin_guid         varchar(50)    NOT NULL,
    file_type          varchar(50),
    admin_name         varchar(60),
    admin_code         varchar(50),
    admin_type         varchar(32),
    admin_period       varchar(32),
    date_taken_year    char(9),
    batch_guid         varchar(50)    NOT NULL,
    rec_status         varchar(1)     DEFAULT 'C' NOT NULL,
    create_date        date           DEFAULT CURRENT_DATE NOT NULL
)
