CREATE TABLE ***schema_name***.int_item_math_flu(
    batch_guid            varchar(50)      NOT NULL,
    record_num            int4             NOT NULL,
    asmt_attempt_guid     varchar(36)      NOT NULL,
    item_guid             varchar(50)      NOT NULL,
    student_parcc_id      varchar(40),
    rsp_entered           int2,
    student_item_score    numeric(3, 0),
    item_seq              int2,
    item_time             numeric(4, 0),
    rec_status            varchar(1)       DEFAULT 'C' NOT NULL,
    create_date           date             DEFAULT CURRENT_DATE NOT NULL
)
