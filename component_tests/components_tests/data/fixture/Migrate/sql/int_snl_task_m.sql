CREATE TABLE ***schema_name***.int_snl_task_m(
    asmt_guid                 varchar(50)     NOT NULL,
    task_guid                 varchar(50)     NOT NULL,
    task_descript             varchar(250),
    task_short_name           char(2),
    pt_task_mode              varchar(10),
    pt_complexity             varchar(40),
    pt_stimulus_type          varchar(30),
    pt_stimulus_complexity    varchar(40),
    batch_guid                varchar(50)     NOT NULL,
    rec_status                varchar(1)      DEFAULT 'C' NOT NULL,
    create_date               date            DEFAULT CURRENT_DATE NOT NULL
)
