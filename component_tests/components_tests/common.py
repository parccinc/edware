from sqlalchemy.engine.url import URL


def get_edware_stats_url(context):
    return get_formatted_postgres_url(context.edware_stats_db_user, context.edware_stats_db_password,
                                      context.utility_rds_server, context.utility_rds_port,
                                      context.edware_stats_db)


def get_formatted_postgres_url(user, password, server, port, db):
    return URL('postgresql', user, password, server, port, db)
