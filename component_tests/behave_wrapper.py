import argparse
from collections import OrderedDict
import json
import logging
import os
from pathlib import Path
import re
import subprocess
import sys

from components_tests.flaky_tests_handler import flaky_utils as utils


SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))


def set_logger():
    logger = logging.getLogger('rerun_logger')
    logger.setLevel(logging.INFO)
    sh = logging.StreamHandler(sys.stdout)
    sh.setLevel(logging.INFO)
    logger.addHandler(sh)
    return logger


class ErrObj(object):
    def __init__(self, feature, filename, error_type, error_data):
        self.attempt_index = 1
        self.feature = feature
        self.filename = filename
        self.error_type = error_type
        self._set_error_data(error_data)
        self.was_failed_last_time = False

    def __eq__(self, other):
        return self.title == other.title and self.line == other.line and self.step == other.step

    def _set_error_data(self, data):
        self.title = data[utils.TITLE]
        self.line = data[utils.LINE]
        self.step = data[utils.STEP]
        self.error_message = data[utils.ERROR_MESSAGE]

    def increase_attempt(self):
        self.attempt_index += 1


class BehaveRunner(object):

    MAX_ATTEMPT_INDEX = 5

    def __init__(self, args: argparse.Namespace, logger: logging.Logger):

        # command line parameters
        self.path = args.path
        self.file_mask_pattern = args.file_mask_pattern
        self.stop_on_failure = args.stop_on_failure
        self.extra_params = ' '.join(args.extra_params)

        self.logger = logger

        # current feature running info
        self.flaky_scenarios = {}
        self.initial_run_command = None
        self.last_error = None
        self.feature_error_files = set()

    def run_behave_tests(self):
        features = self._get_features_list()
        feature_status = []
        returncodes = set()
        for test_directory, feature in features:
            returncode = self._process_feature(test_directory, feature)
            self.logger.info('Feature: {} returned statuscode {}'.format(feature, returncode))
            status = 'passed' if returncode == 0 else 'failed'
            feature_status.append('{f_path}: {status}'.format(f_path=self._get_path(test_directory, feature),
                                                              status=status))
            returncodes.add(returncode)
        for status in feature_status:
            self.logger.info(status)
        return self._get_returncode(returncodes)

    def _get_features_list(self):
        if self.path.is_dir():
            features = self.path.glob('**/*.feature')
            if self.file_mask_pattern:
                file_mask_pattern = re.compile(self.file_mask_pattern)
                features = (feature for feature in features if file_mask_pattern.search(feature.name))
        elif self.path.is_file():
            features = [self.path]
        else:
            raise ("Please provide valid path to the dir or file with test")
        features_list = [(str(feature.parent), str(feature.name)) for feature in features]
        self._log_found_files_list(features_list)
        return features_list

    def _process_feature(self, test_directory, feature):
        try:
            os.chdir(test_directory)
            returncode = self._run_feature(feature)
        except Exception:
            raise
        else:
            while self.feature_error_files:
                os.remove(self.feature_error_files.pop())
            return returncode
        finally:
            os.chdir(str(SCRIPT_DIR))

    def _run_feature(self, feature):
        command = 'behave {feature} {params}'.format(feature=feature, params=self.extra_params)
        if self.stop_on_failure:
            command = ' '.join([command, '--stop'])
        self.initial_run_command = command
        returncode = self._run_command(command)
        if returncode == 0:
            return returncode

        flaky_scenarios = self._read_error_file(feature)
        self._register_scenarios_as_flaky(feature, flaky_scenarios)
        returncodes = set()
        for sc_error in self.flaky_scenarios[feature].values():
            self.last_error = sc_error
            returncode = self._rerun_flaky_scenarios(feature, sc_error)
            returncodes.add(returncode)
            if self.stop_on_failure and returncode != 0:
                break
        return self._get_returncode(returncodes)

    def _run_command(self, command: str):
        return subprocess.call(command, shell=True)

    def _register_scenarios_as_flaky(self, feature, errors):
        if not self.flaky_scenarios.get(feature):
            self.flaky_scenarios[feature] = OrderedDict()
        self.flaky_scenarios[feature].update(errors)

    def _rerun_flaky_scenarios(self, feature, sc_error: ErrObj):
        if self._max_attempt_reached(sc_error) or sc_error.was_failed_last_time:
            return 1
        command = self._get_rerun_command(sc_error)
        return self._rerun_scenario(feature, command)

    def _rerun_scenario(self, feature: str, command: str):
        returncode = self._run_command(command)
        if returncode == 0:
            return returncode
        error = self._read_error_file(feature, rerun_mode=True)
        rerun_returncodes = set()
        for error_key, error in error.items():
            self.compare_error_with_registered(feature, error_key, error)
            # continue with obj from registry (cause new error object have attempt_index = 1)
            error_obj = self.flaky_scenarios[feature][error_key]
            returncode = self._rerun_flaky_scenarios(feature, error_obj)
            rerun_returncodes.add(returncode)
        return self._get_returncode(rerun_returncodes)

    def _read_error_file(self, feature, rerun_mode: bool=False):
        filename = utils.format_filename(feature, rerun_mode)
        try:
            with open(filename) as f:
                errors = json.load(f)
                self.feature_error_files.add(filename)
        except OSError:
            raise ('There is no file "{}" in the test directory'.format(filename))

        feature_title = errors[utils.FEATURE]
        err_filename = filename
        err_type = errors[utils.ERROR_TYPE]
        failed_scenarios = errors[utils.FAILED_SCENARIOS]

        bucket = OrderedDict()
        for scenario in failed_scenarios:
            key = (err_type, scenario[utils.TITLE], scenario[utils.LINE])
            value = ErrObj(feature_title, err_filename, err_type, scenario)
            bucket[key] = value

        return bucket

    def compare_error_with_registered(self, feature: str, error_key: tuple, error: ErrObj):
        """
        Compare error object after feature rerunning with error object from registry. If objects are equal
        then increase attempt index for registered object, if there's no match with registry
        then add new error object to the registry.
        :param feature: current feature filename
        :param error_key: tuple, by which we can found error object from registry
        :param error: ErrObj instance
        """
        registered_error = self.flaky_scenarios[feature].get(error_key)
        if registered_error and registered_error == error:
            registered_error.increase_attempt()
            registered_error.was_failed_last_time = True if registered_error == self.last_error else False
        else:
            self.flaky_scenarios[feature][error_key] = error

    def _log_found_files_list(self, features: list):
        self.logger.info('Features to be run:')
        for test_dir, feature in features:
            self.logger.info(self._get_path(test_dir, feature))

    @staticmethod
    def _get_returncode(returncodes):
        return 0 if len(returncodes) == 1 and 0 in returncodes else 1

    @staticmethod
    def _get_path(directory, filename):
        return '{}/{}'.format(directory, filename)

    @property
    def set_rerun_mode(self):
        return '-D rerun_mode=True'

    @staticmethod
    def _get_scenario_name(sc_name: str) -> str:
        return '-n "%s"' % sc_name

    def _get_rerun_command(self, sc_error: ErrObj) -> str:
        """
        Create rerun command for failed scenario, add parameter rerun_mode=True to the command
        If error_type == background we rerun all feature.
        If error_type == scenario we rerun only failed scenario,
        for this add parameter -n [scenario name] to the command string
        :param sc_error: ErrObj with error details
        :return: str: command for rerun
        """
        command = self.initial_run_command
        r_mode = self.set_rerun_mode
        sc_name = self._get_scenario_name(sc_error.title) if sc_error.error_type == 'scenario' else ''
        return '{command} {r_mode} {sc_name}'.format(command=command, r_mode=r_mode, sc_name=sc_name)

    def _max_attempt_reached(self, sc_error: ErrObj) -> bool:
        """ Check that we current scenario attempt index < MAX_ATTEMPT_INDEX
        :param sc_error: Current scenario error_object
        :return: bool
        """
        return sc_error.attempt_index >= self.MAX_ATTEMPT_INDEX


def parse_args(args: list=None) -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('path', type=Path, nargs='?', default=SCRIPT_DIR)
    parser.add_argument('--include', '-i', dest='file_mask_pattern', type=str, default=None,
                        help='Behave will search and run tests by this mask')
    parser.add_argument('--stop', dest='stop_on_failure', action='store_true', default=None,
                        help='Stop tests on first failure')
    parser.add_argument('extra_params', nargs='*')

    args, unknown = parser.parse_known_args(args)
    args.extra_params.extend(unknown)
    return args


def main(args: argparse.Namespace, ):
    logger = set_logger()
    runner = BehaveRunner(args, logger)
    returncode = runner.run_behave_tests()
    sys.exit(returncode)

if __name__ == '__main__':
    main(parse_args())
