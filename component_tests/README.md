# COMPONENT_TESTS

Common tests directory

For behave tests ( **e2e, reporting** ) use `behave_wrapper.py` to avoid flaky tests errors.

usage:

    python behave_wrapper.py [path_to_the_feature]
                             [--include] {"filename1|filename2"}]
                             [--stop]
                             [extra_parameters {*}]
                             
    
If `[path_to_the_feature]` is filename then run only this feature

If `[path_to_the_feature]` is directory then script search all features in that dir and run them all
    
Optional parameters:

    --include -i    Behave will search and run tests by this mask (equal to the behave --include parameter)
    --stop          Stop running tests at the first failure (equal to the behave --stop parameter)
    *              All other parameters

