__author__ = 'smuhit'

import configparser
import os
import shutil
from unittest import TestCase
from components_tests.frontend.common import DOWNLOAD_DIR, setup_browser
from components_tests.flaky_tests_handler.flaky_tests_handler import handle_failed_feature


def before_all(context):
    config = configparser.ConfigParser()
    config_file_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', 'test.ini'))
    config.read(config_file_path)
    config_values = dict(config.items('test'))

    context.tc = TestCase()

    for key, value in config_values.items():
        setattr(context, key, value)


def after_step(context, step):
    if step.status == "failed":
        if hasattr(context, 'driver'):
            if hasattr(context, 'take_screenshot_on_failure') and context.take_screenshot_on_failure == 'yes':
                if not os.path.exists('screenshots'):
                    os.makedirs('screenshots')
                failures()
                context.driver.save_screenshot('screenshots/failure_{0}.png'.format(failures.count))


def before_scenario(context, scenario):
    os.makedirs(DOWNLOAD_DIR, exist_ok=True)
    setup_browser(context)


def after_scenario(context, scenario):
    # delete download dir
    shutil.rmtree(DOWNLOAD_DIR, ignore_errors=True)
    # Tear down web driver
    if hasattr(context, 'driver'):
        try:
            context.driver.quit()
        except Exception as e:
            pass


def after_feature(context, feature):
    if feature.status == 'failed':
        rerun_mode = context.config.userdata.getbool('rerun_mode')
        handle_failed_feature(feature, rerun_mode)


def failures():
    failures.count += 1
failures.count = 0
