@browser
Feature: Bulk Extract functions correctly

  Background:
    Given the API is set up
    And a cookie is set for user "lwoman"

  @RALLY_US35248
  @RALLY_US30720
  @RALLY_US30721
  @RALLY_US34634
  @RALLY_US31988
  Scenario: Load summative assessment results data in the database, verify an extract is created and ingestible
    Given we have a clean data warehouse database
    And the data warehouse is loaded with mock ELA summative data for the primary tenant
    When a bulk extract triggered with the following parameters
      | param        | value            |
      | year         | 2015             |
      | extractType  | summ_asmt_result |
      | stateCode    | NY               |
      | gradeCourse  | Grade 11         |
      | subject      | subject2         |
    Then the response code should be 200
    And the response body contains a url in a files list
    Given an open browser
    When I go to download url
    Then I am redirected to the login page
    When I login as lwoman
    Then I wait until I see my download file
    And the name of the file begins with "AssmtResults_Summative_ELA_Grade_11_2014-2015"
    Then I convert the file to compressed tar format
    Given an empty UDL stats table
    And an empty UDL batch table
    And we have a clean data warehouse database
    When the extract file is encrypted and copied into the UDL landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    When enough time is given for Migrate to process the UDL data
    Then the UDL stats table should have the load status for all the records as dump successful
    And the data warehouse databases should have data in the ela assessment results table
    And the data warehouse databases should have data in the Test Score table
    And the data warehouse databases should have data in the Test Level table
    And the ela assessment results table data in the primary tenant is the same as what was loaded
    And the assessment test score table data in the primary tenant is the same as what was loaded
    And the assessment test level table data in the primary tenant is the same as what was loaded

  @RALLY_US35248
  @RALLY_US35111
  Scenario: Verify sum of rows return by multiple grade/course API when sum of gradeCourse record is 17 in CSV for Math
    Given we have a clean data warehouse database
    And the data warehouse is loaded with mock Math Summative Multiple Grade/Course data for the primary tenant
    When a bulk extract triggered with the following parameters
      | param            | value             |
      | year             | 2015              |
      | extractType      | summ_asmt_result  |
      | stateCode        | NY                |
      | gradeCourse      | Grade 3           |
      | gradeCourse      | Geometry          |
      | subject          | subject1          |
    Then the response code should be 200
    And the response body contains a url in a files list
    Given an open browser
    When I go to download url
    Then I am redirected to the login page
    When I login as lwoman
    Then I wait until I see my download file
    And the name of the file begins with "AssmtResults_Summative_Math_multigrade_2014-2015"
    Then I convert the file to compressed tar format
    When I untar the bulk extract file
    Then the untarred file has only 1 file which is a CSV
    And I expect "17" entities in the untarred bulk extract csv file

  @RALLY_US35248
  @RALLY_US35111
  Scenario: Verify sum of rows return by multiple grade/course API when sum of gradeCourse record is 18 in CSV for Math
    Given we have a clean data warehouse database
    And the data warehouse is loaded with mock Math Summative Multiple Grade/Course data for the primary tenant
    When a bulk extract triggered with the following parameters
      | param            | value             |
      | year             | 2015              |
      | extractType      | summ_asmt_result  |
      | stateCode        | NY                |
      | gradeCourse      | Grade 3           |
      | gradeCourse      | Grade 8           |
      | gradeCourse      | Algebra II        |
      | gradeCourse      | Geometry          |
      | subject          | subject1          |
    Then the response code should be 200
    And the response body contains a url in a files list
    Given an open browser
    When I go to download url
    Then I am redirected to the login page
    When I login as lwoman
    Then I wait until I see my download file
    And the name of the file begins with "AssmtResults_Summative_Math_multigrade_2014-2015"
    Then I convert the file to compressed tar format
    When I untar the bulk extract file
    Then the untarred file has only 1 file which is a CSV
    Then I expect "18" entities in the untarred bulk extract csv file

  @RALLY_US35248
  @RALLY_US35111
  Scenario: Verify sum of rows return by multiple grade/course API when sum of gradeCourse record is 6 in CSV for ELA
    Given we have a clean data warehouse database
    And the data warehouse is loaded with mock ELA Summative Multiple Grade/Course data for the primary tenant
    When a bulk extract triggered with the following parameters
      | param            | value             |
      | year             | 2015              |
      | extractType      | summ_asmt_result  |
      | stateCode        | NY                |
      | gradeCourse      | Grade 3           |
      | gradeCourse      | Grade 10          |
      | subject          | subject2          |
    Then the response code should be 200
    And the response body contains a url in a files list
    Given an open browser
    When I go to download url
    Then I am redirected to the login page
    When I login as lwoman
    Then I wait until I see my download file
    And the name of the file begins with "AssmtResults_Summative_ELA_multigrade_2014-2015"
    Then I convert the file to compressed tar format
    When I untar the bulk extract file
    Then the untarred file has only 1 file which is a CSV
    Then I expect "6" entities in the untarred bulk extract csv file

  @RALLY_US35248
  @RALLY_US35111
  Scenario: Verify sum of rows return by multiple grade/course API when sum of gradeCourse record is 6 in CSV for ELA
    Given we have a clean data warehouse database
    And the data warehouse is loaded with mock ELA Summative Multiple Grade/Course data for the primary tenant
    When a bulk extract triggered with the following parameters
      | param            | value             |
      | year             | 2015              |
      | extractType      | summ_asmt_result  |
      | stateCode        | NY                |
      | gradeCourse      | Grade 3           |
      | gradeCourse      | Grade 8           |
      | gradeCourse      | Grade 9           |
      | gradeCourse      | Grade 10          |
      | subject          | subject2          |
    Then the response code should be 200
    And the response body contains a url in a files list
    Given an open browser
    When I go to download url
    Then I am redirected to the login page
    When I login as lwoman
    Then I wait until I see my download file
    And the name of the file begins with "AssmtResults_Summative_ELA_multigrade_2014-2015"
    Then I convert the file to compressed tar format
    When I untar the bulk extract file
    Then the untarred file has only 1 file which is a CSV
    Then I expect "6" entities in the untarred bulk extract csv file

  @RALLY_US38793
  @RALLY_US35248
  @RALLY_US34636
  Scenario: Load psychometric results data in the database, verify an extract is created and ingestible
    Given we have a clean data warehouse database
    And the data warehouse is loaded with mock Psychometric Item File data for the primary tenant
    When a bulk extract triggered with the following parameters
      | param        | value            |
      | year         | 2015             |
      | extractType  | p_data           |
      | stateCode    | NY               |
    Then the response code should be 200
    And the response body contains a url in a files list
    Given an open browser
    When I go to download url
    Then I am redirected to the login page
    When I login as lwoman
    Then I wait until I see my download file
    And the name of the file begins with "PsychometricItemFile_2015"
    Then I convert the file to compressed tar format
    Given an empty UDL stats table
    And an empty UDL batch table
    And we have a clean data warehouse database
    When the extract file is encrypted and copied into the UDL landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    When enough time is given for Migrate to process the UDL data
    Then the UDL stats table should have the load status for all the records as dump successful
    And the data warehouse databases should have data in the psychometric data table
    And the psychometric item table data in the primary tenant is the same as what was loaded

  @RALLY_US35248
  @RALLY_US34635
  Scenario: Load math released item file data in the database, verify an extract is created and ingestible
    Given we have a clean data warehouse database
    And the data warehouse is loaded with mock math Released Item File data for the primary tenant
    When a bulk extract triggered with the following parameters
      | param        | value              |
      | year         | 2015               |
      | extractType  | item_student_score |
      | stateCode    | NY                 |
      | gradeCourse  | Grade 3            |
      | subject      | subject1           |
    Then the response code should be 200
    And the response body contains a url in a files list
    Given an open browser
    When I go to download url
    Then I am redirected to the login page
    When I login as lwoman
    Then I wait until I see my download file
    And the name of the file begins with "ReleasedItemFile_Summative_Math_Grade_3_2014-2015"
    Then I convert the file to compressed tar format
    Given an empty UDL stats table
    And an empty UDL batch table
    And we have a clean data warehouse database
    When the extract file is encrypted and copied into the UDL landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    When enough time is given for Migrate to process the UDL data
    Then the UDL stats table should have the load status for all the records as dump successful
    And the data warehouse databases should have data in the Math RIF table
    And the math released item table data in the primary tenant is the same as what was loaded

  @RALLY_US35248
  @RALLY_US34635
  Scenario: Load ELA released item file data in the database, verify an extract is created and ingestible
    Given we have a clean data warehouse database
    And the data warehouse is loaded with mock ELA Released Item File data for the primary tenant
    When a bulk extract triggered with the following parameters
      | param        | value              |
      | year         | 2015               |
      | extractType  | item_student_score |
      | stateCode    | NY                 |
      | gradeCourse  | Grade 3            |
      | subject      | subject2           |
    Then the response code should be 200
    And the response body contains a url in a files list
    Given an open browser
    When I go to download url
    Then I am redirected to the login page
    When I login as lwoman
    Then I wait until I see my download file
    And the name of the file begins with "ReleasedItemFile_Summative_ELA_Grade_3_2014-2015"
    Then I convert the file to compressed tar format
    Given an empty UDL stats table
    And an empty UDL batch table
    And we have a clean data warehouse database
    When the extract file is encrypted and copied into the UDL landing zone
    And enough time is given for UDL to process the file
    Then the UDL stats table should have data for UDL
    When enough time is given for Migrate to process the UDL data
    Then the UDL stats table should have the load status for all the records as dump successful
    And the data warehouse databases should have data in the ELA RIF table
    And the ela released item table data in the primary tenant is the same as what was loaded

  @RALLY_US35020
  Scenario: Load summative results data in the CDS database, verify an extract is created and ingestible
    Given we have a clean cat tenant cds database
    And the data warehouse is loaded with mock CDS ELA summative data for the cds tenant
    When a bulk extract triggered with the following parameters
      | param        | value            |
      | year         | 2015             |
      | extractType  | cds_data         |
      | stateCode    | NY               |
    Then the response code should be 200
    And the response body contains a url in a files list
    Given an open browser
    When I go to download url
    Then I am redirected to the login page
    When I login as lwoman
    Then I wait until I see my download file
    And the name of the file begins with "CDSFile_2014"
    Then I convert the file to compressed tar format
    When I untar the bulk extract file
    Then the untarred file has only 4 file which is a CSV
    And I expect "25" entities in the untarred bulk extract csv file starting with "CDSFile_SUMMATIVE_ELA_2014-2015_"
