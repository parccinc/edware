__author__ = 'npandey'

from behave import given
from components_tests.steps import udl_steps, migrate_steps, bulk_extract_steps, api_steps, cds_steps, starmigrate_steps
# TODO: frontend_saiku_analytics was moved to component_tests/pentaho/features/frontend/steps/common.py
# TODO: why do we import these steps into e2e?
from pentaho.features.frontend.steps import common
from components_tests.frontend import common as components_common, support
from reporting_tests.features.frontend.steps import common, info_bar, subject_selector, hpz, data_export, grid


# Need to treat this separately from frontend reporting for now, because we "pass" this step to avoid unnecessary browser reopening
# TODO: remove "Given an open browser" from lots of tests so that we don't have to "pass" this step
@given('a open browser')
def open_browser(context):
    components_common.setup_browser(context)
