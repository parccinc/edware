@browser
Feature: E2E Tests

   Scenario: Drop a file in the landing zone and verify data is propagated up to reports
    Given an empty UDL stats table
    And an empty UDL batch table
    And we have a clean second tenant's data warehouse database
    And we have a clean second tenant's cds database
    And we have a clean parcc cds database

    Given clear backup storage for second tenant
    When multiple valid summative SDS files are dropped into the second tenant's landing zone
    And multiple valid summative SDS item files are dropped into the second tenant's landing zone
    And enough time is given for UDL to process the file
    Then the stats table should have data for 15 records
    And summative SDS backup files are created in secure location for second tenant
    And summative SDS item backup files are created in secure location for second tenant

    Given clear backup storage for second tenant
    Then no backup files for second tenant in storage

    When enough time is given for Migrate to process the UDL data for the second tenant
    Then the UDL stats table should have the load status for all the records as dump successful
    When enough time is given for the slave database servers to replicate from master
    Then the second tenant's data warehouse databases should have data in the ela assessment results table
    And the second tenant's data warehouse databases should have data in the math assessment results table

    Then I migrate the data for the second tenant cds in tenant mode for year 2015
    And I migrate the data for parcc cds in consortium mode for year 2015
    And the second tenant's cds databases should have data in the ela assessment results table
    And the second tenant's cds databases should have data in the math assessment results table
    And the parcc cds databases should have data in the ela assessment results table
    And the parcc cds databases should have data in the math assessment results table

    Given a open browser
    When I open the cpop report with the second tenant's state code and the following additional parameters
      | param        | value            |
      | year         | 2015             |
      | districtGuid | R0003            |
      | gradeCourse  | Grade 11         |
      | asmtType     | SUMMATIVE        |
      | subject      | subject2         |
    Then I am redirected to the login page
    When I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 11th Grade   |
        | subject      | ELA/L        |
    And I am redirected to the cpop district report
    Then I verify that I'm on the performance page
    And I should see the report title "Providence"
    And I should see the subtitle "3 schools"
    And I should see "SCHOOL,STUDENTS,PERFORMANCE DISTRIBUTION,≥ LVL 4,OVERALL,READING WRITING" columns for ela overall
    And I verify that the subject selected is "ELA/L"
    When I click on "Export Data" link on header
    Then I should see modal title "Data Export"
    When I click on Academic Year drop down
    When I select "2013 - 2014" from Academic Year drop down
    And I select "Summative Assessment Results" from "EXPORT TYPE" drop down
    And I select "ELA/L" from "SUBJECT" drop down
    Then I should see "Export Data" button is disabled
    When I select "4th Grade,11th Grade" from "GRADE/COURSE" drop down
    Then I should see "Export Data" button is enabled
    When I click on "Export Data" button
    Then I should see "Copy and paste the link below to access your download:" messages
    Then I should see a link for download
    When I click "Close" button
    Then I should see "Export Data" link on header
    When I go to download url
    Then I wait until I see the download file
    And the name of the file begins with "AssmtResults_Summative_ELA_multigrade_2013-2014"
    Then I convert the file to compressed tar format
    When I untar the bulk extract file
    Then the untarred file has only 1 file which is a CSV

    When enough time is given for StarMigrate to process

  Scenario: Test analytics database integrity
    Then the dim_accomod table should have 91 rows in analytics schema
    And the number of columns in dim_accomod should be 46

    And the dim_school table should have 7 rows in analytics schema
    And the number of columns in dim_school should be 12

    And the dim_opt_state_data table should have 100 rows in analytics schema
    And the number of columns in dim_opt_state_data should be 19

    And the dim_poy table should have 2 rows in analytics schema
    And the number of columns in dim_poy should be 7

    And the dim_student table should have 213 rows in analytics schema
    And the number of columns in dim_student should be 34

    And the dim_test table should have 1 rows in analytics schema
    And the number of columns in dim_test should be 14

    Then the fact_sum table should have 214 rows in analytics schema
    And the number of columns in fact_sum should be 80

    And the dim_accomod table should have 10 rows where the accomod_screen_reader column is Y and accomod_asl_video column is N
    And the dim_school table should have 2 rows where the dist_name column is Providence and school_id column is RP003
    And the dim_poy table should have 1 rows where the poy column is Spring and school_year column is 2014-2015
    And the dim_student table should have 2 rows where the student_first_name column is Arnette and student_parcc_id column is 6DRp6GlmW7bu4tn1PhckfVhL9MHH85oXIaSfRJep
    # TODO: We need check table dim_test but now we have there only 1 line with 'UNKNOWN' values
    # And the dim_test table should have 18 rows where the test_subject column is Algebra I and _record_version column is 1
    And the fact_sum table should have 14 rows where the report_suppression_code column is 5 and me_flag column is math
    And the fact_sum table should have 108 rows when joined with dim_student on _key_student and _key_student and student_sex column is M
    And the fact_sum table should have 208 rows when joined with dim_poy on _key_poy and _key_poy and school_year column is 2014-2015
    And the fact_sum table should have 0 rows when joined with dim_test on _key_eoy_test and _key_test and test_subject column is Algebra I

  Scenario: Test pentaho UI part1
    Given a open browser
    When I browse to the Pentaho URL
    And I login as "dogstate1@state.k12.dog.us" with password "innovent"
    Then I should see the "landing" page open
    When I click on the "New Report" button in analytics
    Then I should see the "report" page open
    And I should see there is a left nav bar on the page
    And I should see a dropdown box under "Cubes"
    Then I should see "Summative Records"
    When I select "Summative Records"
    And I un-click auto run
    Then I should see "Measures box" open
    When I click on "Poy" in the left nav
    And I click on "Assessment(ELA-Math)" in the left nav
    And I drag the following fields to respective categories
      | field            | category |
      | Scale Score Mean | Measures |
      | Math - ELA       | Filters  |
      | Assessment Year  | Filters  |
    And I filter on the following options
      | field           | option | label     |
      | Assessment Year | 2      | 2014-2015 |
      | Math - ELA      | 2      | math      |
    And I click on "Responsible Institution" in the left nav
    And I drag the following fields to respective categories
      | field                     | category |
      | Responsible District Name | Rows     |
      | Responsible School Name   | Rows     |
    And I click play to run the report
    Then I should see the same results from test case "2.1" query for "second" tenant in "3" columns with the following parameters
      | field       | value     |
      | state_id    | RI        |
      | school_year | 2014-2015 |
      | me_flag     | math      |

  Scenario: Test pentaho UI part2
    When I browse to the Pentaho URL
    And I login as "dogstate1@state.k12.dog.us" with password "innovent"
    Then I should see the "landing" page open
    When I click on the "New Report" button in analytics
    Then I should see the "report" page open
    And I should see there is a left nav bar on the page
    And I should see a dropdown box under "Cubes"
    Then I should see "Summative Records"
    When I select "Summative Records"
    And I un-click auto run
    When I select the following values within the category on the left-hand sidebar
      | category  | values                                 |
      | measures  | Distribution Subclaim 1: Level 1 Count |
      | measures  | Distribution Subclaim 1: Level 2 Count |
      | measures  | Distribution Subclaim 1: Level 3 Count |
    When I click on "Poy" in the left nav
    And I click on "Assessment(ELA-Math)" in the left nav
    And I drag the following fields to respective categories
      | field           | category |
      | Assessment Year | Columns  |
      | Math - ELA      | Columns  |
    And I click on "Responsible Institution" in the left nav
    And I drag the following fields to respective categories
      | field                     | category  |
      | Responsible District Name | Rows      |
      | Responsible School Name   | Rows      |
    When I click play to run the report
    Then I should see the following values in row "4" of the table
      | column_num  | value             | tag |
      | 1           |                   | th  |
      | 2           | Hope High School  | th  |
      | 1           | 1                 | td  |
      | 2           | 0                 | td  |
      | 3           | 2                 | td  |
      | 4           | 0                 | td  |
      | 5           | 3                 | td  |
      | 6           | 0                 | td  |
      | 7           | 6                 | td  |
      | 8           | 9                 | td  |
      | 9           | 5                 | td  |
      | 10          | 8                 | td  |
      | 11          | 16                | td  |
      | 12          | 3                 | td  |