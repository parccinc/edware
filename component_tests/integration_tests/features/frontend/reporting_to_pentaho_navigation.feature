@browser

Feature: Reporting to Pentaho navigation

  Background: Open a firefox browser
    Given an open browser


  @PARCC-156
  Scenario: Go to Reporting from Analytics page
    When I browse to the Pentaho URL
    And I login as "dogstate1@state.k12.dog.us" with password "innovent"
    Then I should see the "landing" page open
    Then I verify the user name is displayed as "dogstate1@state.k12.dog.us" in the header
    Then I look at the "welcome" content
    And I should see "Folder" Actions menu bar
    And I see the following items in Folder Actions list
        |item           |
        |New Folder...  |
        |Move to Trash  |
        |Rename...      |
        |Paste          |
        |Properties...  |
    Then I see the following folders in the "folders" browser
        | object_name                    |
        | Home                           |
        | dogstate1@state.k12.dog.us     |
        | Pre-Configured Reports         |
        | Share_RI                       |
        | Trash                          |
    Then I return to main content
    When I click on the "reporting" link in the header
    And I navigate to the new opened window
    And I verify that I am on the subject and grade/course initial selection page
    Then I verified that user name "dog State1" is on page header
    Then I log out
    Then I close the new opened window


  @PARCC-156
  Scenario: Go to Analytics from Reporting page
    When I open the cpop report with the following parameters
        | param        | value     | 
        | asmtType     | SUMMATIVE | 
        | year         | 2015      |
        | subject      | subject1  |
    Then I am redirected to the login page
    When I login as "dogstate1@state.k12.dog.us" with password "innovent"
    And I verify that I am on the subject and grade/course initial selection page
    And I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 9th Grade    |
        | subject      | ELA/L        |
    Then I wait for the grid to reload
    Then I verified that user name "dog State1" is on page header
    When I click on the "Analytics" link in the reporting header
    And I navigate to the new opened window
    Then I should see the "Analytics" page open
    Then I verify the user name is displayed as "dogstate1@state.k12.dog.us" in the header
    Then I look at the "welcome" content
    And I should see "Folder" Actions menu bar
    And I see the following items in Folder Actions list
        |item           |
        |New Folder...  |
        |Move to Trash  |
        |Rename...      |
        |Paste          |
        |Properties...  |
    Then I see the following folders in the "folders" browser
        | object_name                    |
        | Home                           |
        | dogstate1@state.k12.dog.us     |
        | Pre-Configured Reports         |
        | Share_RI                       |
        | Trash                          |
    Then I return to main content
    When I click on the "reporting" link in the header
    And I navigate to the new opened window
    And I verify that I am on the subject and grade/course initial selection page
    Then I verified that user name "dog State1" is on page header
    Then I log out
    Then I close the new opened window
