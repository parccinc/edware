import os
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))

install_requires = [
    'enum34 == 1.0.4',
    'behave >= 1.2.5',  # functional testing framework
    'SQLAlchemy == 0.9.9',
    'psycopg2 == 2.5.1',
    'py-postgresql == 1.1.0',
    'paramiko == 1.15.1',
    'scp == 0.10.0',
    'python-gnupg == 0.3.5',
    'boto == 2.34.0',
    'selenium',
    'Requests >= 2.2.1',  # for restful web services
    'requests-toolbelt >= 0.2.0',  # extension of Requests
    'awscli==1.7.13',
    'matchers >= 0.22',
    'pathlib',
]

setup(name='components_tests',
      version='0.1',
      description='Components tests',
      classifiers=[
          "Programming Language :: Python 3",
          "Functional Testing Framework :: behave",
          "Web Testing Framework :: Selenium",
          "Restful API Testing :: Python Requests Module"
      ],
      packages=find_packages() + ["components_tests/data"],
      include_package_data=True,
      zip_safe=False,
      install_requires=install_requires,
      )

setup(name='reporting_tests',
      version='0.1',
      description='Reporting tests',
      classifiers=[
          "Programming Language :: Python 3",
          "Functional Testing Framework :: behave",
          "Web Testing Framework :: Selenium",
          "Restful API Testing :: Python Requests Module"
      ],
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=install_requires,
      )

setup(name='integration_tests',
      version='0.1',
      description='Integration tests',
      classifiers=[
          "Programming Language :: Python 3",
          "Functional Testing Framework :: behave",
          "Web Testing Framework :: Selenium",
          "Restful API Testing :: Python Requests Module"
      ],
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=install_requires,
      )

setup(name='pentaho',
      version='0.1',
      description='Pentaho Tests',
      classifiers=[
          "Programming Language :: Python 3",
          "Functional Testing Framework :: behave",
          "Web Testing Framework :: Selenium",
          "Restful API Testing :: Python Requests Module"
      ],
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=install_requires,
      )
