@browser
Feature: Test analytics reports and permissions

  Background: Open a firefox browser and open the webpage
    When I browse to the Pentaho URL

  Scenario: Cat state user runs adhoc report
    When I login as "catstate1@state.k12.cat.us" with password "innovent"
    Then I should see the "landing" page open
    When I click on the "New Report" button in analytics
    Then I should see the "report" page open
    And I should see there is a left nav bar on the page
    And I should see the following labels
      | labels     |
      | Cubes      |
      | Measures   |
      | Dimensions |
    And I should see a dropdown box under "Cubes"
    And I should see "Summative Records"
    When I select "Summative Records"
    And I un-click auto run
    And I click on "Distinct Student Count" in the left nav
    And I click on "Responsible Institution" in the left nav
    And I click on "Responsible District Name" in the left nav
    And I click play to run the report
    Then I should see these values in row "1" of the table "body" in "custom" report
      | value 			  |
      | East Greenwich    |
      | 19     			  |
    And I should see these values in row "2" of the table "body" in "custom" report
      | value             |
      | Newport           |
      | 20                |
    And I should see these values in row "3" of the table "body" in "custom" report
      | value             |
      | Providence        |
      | 66                |

  Scenario: Cat District user runs adhoc report
    When I login as "lwoman" with password "lwoman1234"
    Then I should see the "landing" page open
    When I click on the "New Report" button in analytics
    Then I should see the "report" page open
    And I should see there is a left nav bar on the page
    And I should see the following labels
      | labels     |
      | Cubes      |
      | Measures   |
      | Dimensions |
    And I should see a dropdown box under "Cubes"
    And I should see "Summative Records"
    When I select "Summative Records"
    And I un-click auto run
    And I click on "Distinct Student Count" in the left nav
    And I click on "Responsible Institution" in the left nav
    And I click on "Responsible District Name" in the left nav
    And I click play to run the report
    Then I should see these values in row "1" of the table "body" in "custom" report
      | value       |
      | Providence  |
      | 66          |

  @RALLY_DE1226
  Scenario: District user can't see unauthorized data in filters popup
    When I login as "lwoman" with password "lwoman1234"
    Then I should see the "landing" page open
    When I click on the "New Report" button in analytics
    Then I should see the "report" page open
    And I should see there is a left nav bar on the page
    And I should see the following labels
      | labels     |
      | Cubes      |
      | Measures   |
      | Dimensions |
    And I should see a dropdown box under "Cubes"
    And I should see "Summative Records"
    When I select "Summative Records"
    And I un-click auto run
    And I click on "Student" in the left nav
    And I click on "Responsible Institution" in the left nav
    And I drag the following fields to respective categories
      | field                   | category |
      | Responsible School Name | Filters  |
      | Name                    | Filters  |
    And I open the filters popup for "Responsible School Name"
    Then I should see the following fields in the filter popup
      | field                      |
      | Alvarez High School        |
      | Hope High School           |
      | Mount Pleasant High School |
    And I should not see the following fields in the filter popup
      | field                      |
      | East Greenwich High School |
      | Rogers High School         |
    When I close the filter popup
    And I open the filters popup for "Name"
    Then I should see the following fields in the filter popup
      | field                   |
      | Forst, Jacquline Zofia  |
      | Jewell, Senaida Loni    |
      | Pando, Darci Joella     |
    And I should not see the following fields in the filter popup
      | field                   |
      | Forst, Darci Betty      |
      | Lawlor, Devon Jacquline |
      | Neveu, Chase Keshia     |
