Feature: Populate the Analytics Data with know data, which is used by the data verification tests

  @PARCC-264
  @data_import
  Scenario: Add known csv.gz data into analytics server for cat and trigger kettle
    Given that we have known archive with csv.gz dumps for the "cat" tenant
    Then we put known files into migrate_dump folder for this tenant
    And we trigger kettle

    Then the dim_accomod table should have 88 rows in analytics schema
    And the number of columns in dim_accomod should be 46
    And the dim_school table should have 7 rows in analytics schema
    And the number of columns in dim_school should be 12
    And the dim_opt_state_data table should have 96 rows in analytics schema
    And the number of columns in dim_opt_state_data should be 19
    And the dim_poy table should have 2 rows in analytics schema
    And the number of columns in dim_poy should be 7
    And the dim_student table should have 205 rows in analytics schema
    And the number of columns in dim_student should be 34
    And the dim_test table should have 1 rows in analytics schema
    And the number of columns in dim_test should be 14
    And the dim_unique_test table should have 104 rows in analytics schema
    And the number of columns in dim_unique_test should be 7
    And the fact_sum table should have 206 rows in analytics schema
    And the number of columns in fact_sum should be 80

  @PARCC-264
  @data_import
  Scenario: Add known csv.gz data into analytics server for dog and trigger kettle
    Given that we have known archive with csv.gz dumps for the "dog" tenant
    Then we put known files into migrate_dump folder for this tenant
    And we trigger kettle

    Then the dim_accomod table should have 91 rows in analytics schema
    And the number of columns in dim_accomod should be 46
    And the dim_school table should have 7 rows in analytics schema
    And the number of columns in dim_school should be 12
    And the dim_opt_state_data table should have 100 rows in analytics schema
    And the number of columns in dim_opt_state_data should be 19
    And the dim_poy table should have 2 rows in analytics schema
    And the number of columns in dim_poy should be 7
    And the dim_student table should have 213 rows in analytics schema
    And the number of columns in dim_student should be 34
    And the dim_test table should have 1 rows in analytics schema
    And the number of columns in dim_test should be 14
    Then the fact_sum table should have 214 rows in analytics schema
    And the number of columns in fact_sum should be 80
