@browser

Feature: Compare Analytics report's data against the data from the respective SQL statements

  Background:
    When I browse to the Pentaho URL
    And I login as "catstate1@state.k12.cat.us" with password "innovent"
    Then I should see the "landing" page open
    When I click on the "New Report" button in analytics
    Then I should see the "report" page open
    And I should see there is a left nav bar on the page
    And I should see a dropdown box under "Cubes"
    Then I should see "Summative Records"
    When I select "Summative Records"
    And I un-click auto run
    Then I should see "Measures box" open

  @testcase_1
  @RALLY_US37252
  Scenario: Test Case 1: Verify the report showing mean Reading, Writing and Scale scores of 2014-2015 ELA Assessment for all schools
    Then I should see the following "measures" populated in "Measures box"
      | value                                  |
      | Student Registered Count               |
      | Scale Score Mean                       |
      | Valid Score Count                      |
      | Performance Lvl 3 Count                |
      | Distribution Subclaim 4: Level 3 Count |
      | Performance Level 5 %                  |
      | Valid Score Count                      |
      | No Valid Score Count                   |
    And I should see the following "groups of Dimension" populated in "Dimensions box"
      | value                   |
      | Accommodations          |
      | Assessment(ELA-Math)    |
      | EOY Testing Institution |
      | EOY Tests               |
      | PBA Testing Institution |
      | PBA Tests               |
      | Poy                     |
      | Record Flags            |
      | Responsible Institution |
      | Summative               |
      | Student                 |
    And I should see the following boxes on the right panel of the page
      | value    |
      | Measures |
      | Columns  |
      | Rows     |
      | Filter   |

    When I click on "Responsible Institution" in the left nav
    And I click on "Test" in the left nav
    And I select the following values within the category on the left-hand sidebar
      | category | values                   |
      | measures | Scale Score Mean         |
      | measures | Reading Scale Score Mean |
      | measures | Writing Scale Score Mean |
    And I drag the following fields to respective categories
      | field                      | category |
      | Responsible District Name  | Rows     |
      | Responsible School Name    | Rows     |
      | Assessment Grade/Test Name | Rows     |
      | Subject                    | Rows     |
    And I click on "Poy" in the left nav
    And I click on "Assessment(ELA-Math)" in the left nav
    And I drag the following fields to respective categories
      | field           | category |
      | Assessment Year | Filters  |
      | Math - ELA      | Filters  |
    And I filter on the following options
      | field           | option | label     |
      | Assessment Year | 2      | 2014-2015 |
      | Math - ELA      | 1      | ela       |
    And I click play to run the report
    Then I should see the following values in row "9" of the table
      | column_num | value                          | tag |
      | 1          | Providence                     | th  |
      | 2          | Alvarez High School            | th  |
      | 3          | Grade 10                       | th  |
      | 4          | English Language Arts/Literacy | th  |
      | 1          | 388.00                         | td  |
      | 2          | 43.25                          | td  |
      | 3          | 44.75                          | td  |
    And I should see the following values in row "10" of the table
      | column_num | value                          | tag |
      | 1          |                                | th  |
      | 2          |                                | th  |
      | 3          | Grade 11                       | th  |
      | 4          | English Language Arts/Literacy | th  |
      | 1          | 352.00                         | td  |
      | 2          | 43.33                          | td  |
      | 3          | 41.83                          | td  |
    Then I should see the following values in row "11" of the table
      | column_num | value                          | tag |
      | 1          |                                | th  |
      | 2          |                                | th  |
      | 3          | Grade 3                        | th  |
      | 4          | English Language Arts/Literacy | th  |
      | 1          | 321.75                         | td  |
      | 2          | 41.50                          | td  |
      | 3          | 40.00                          | td  |
    And I should see the same results from test case "1.1" query for "first" tenant in "7" columns with the following parameters
      | field       | value     |
      | state_id    | NY        |
      | school_year | 2014-2015 |
      | me_flag     | ela       |

  @testcase_2
  @RALLY_US37258
  Scenario: Test Case 2: Verify Math assessment analysis -- User is able to perform analysis on Math assessment results
    When I click on "Poy" in the left nav
    And I click on "Assessment(ELA-Math)" in the left nav
    And I drag the following fields to respective categories
      | field            | category |
      | Scale Score Mean | Measures |
      | Math - ELA       | Filters  |
      | Assessment Year  | Filters  |
    And I filter on the following options
      | field           | option | label     |
      | Assessment Year | 2      | 2014-2015 |
      | Math - ELA      | 2      | math      |
    And I click on "Responsible Institution" in the left nav
    And I drag the following fields to respective categories
      | field                     | category |
      | Responsible District Name | Rows     |
      | Responsible School Name   | Rows     |
    And I click play to run the report
    Then I should see the same results from test case "2.1" query for "first" tenant in "3" columns with the following parameters
      | field       | value     |
      | state_id    | NY        |
      | school_year | 2014-2015 |
      | me_flag     | math      |

    When I click on "Test" in the left nav
    And I drag the following fields to respective categories
      | field                      | category |
      | Assessment Grade/Test Name | Rows     |
      | Subject                    | Rows     |
    And I click play to run the report
    Then I should see the same results from test case "2.2" query for "first" tenant in "5" columns with the following parameters
      | field       | value     |
      | state_id    | NY        |
      | school_year | 2014-2015 |
      | me_flag     | math      |

  @testcase_3
  @RALLY_US37258
  Scenario: Test Case 3: Verify ELA and Math assessment simultaneously -- User is able to perform analysis on ELA and Math assessment results side by side
    When I click on "Poy" in the left nav
    And I click on "Assessment(ELA-Math)" in the left nav
    And I click on "Responsible Institution" in the left nav
    And I click on "Test" in the left nav
    And I drag the following fields to respective categories
      | field            | category |
      | Scale Score Mean | Measures |
      | Assessment Year  | Filters  |
      | Math - ELA       | Columns  |
    And I filter on the following options
      | field           | option | label     |
      | Assessment Year | 2      | 2014-2015 |
    And I drag the following fields to respective categories
      | field                     | category |
      | Responsible District Name | Rows     |
      | Responsible School Name   | Rows     |
    And I click play to run the report
    Then I should see the same results from test case "3.1" query for "first" tenant in "4" columns with the following parameters
      | field       | value     |
      | state_id    | NY        |
      | school_year | 2014-2015 |

    When I drag the following fields to respective categories
      | field                      | category |
      | Assessment Grade/Test Name | Rows     |
      | Subject                    | Rows     |
    And I click play to run the report
    Then I should see the same results from test case "3.2" query for "first" tenant in "6" columns with the following parameters
      | field       | value     |
      | state_id    | NY        |
      | school_year | 2014-2015 |

  @testcase_4_1
  @RALLY_US37258
  Scenario: Test Case 4 - first part: Verify student profile analysis -- User is able to slice and dice assessment results by student attributes
    When I click on "Student" in the left nav
    And I drag the following fields to respective categories
      | field                       | category |
      | Proficient (>= lvl 4) %     | Measures |
      | Proficient (>= lvl 4) Count | Measures |
      | Federal Ethnicity           | Rows     |
    And I click play to run the report
    Then I should see the same results from test case "4.1" query for "first" tenant in "3" columns with the following parameters
      | field    | value |
      | state_id | NY    |

    When I drag the following fields to respective categories
      | field | category |
      | Sex   | Columns  |
    And I click play to run the report
    Then I should see the same results from test case "4.2" query for "first" tenant in "4" columns with the following parameters
      | field    | value |
      | state_id | NY    |

  @testcase_4_2
  @RALLY_US37258
  Scenario: Test Case 4 - second part: Verify student profile analysis -- User is able to slice and dice assessment results by student attributes
    When I click on "Student" in the left nav
    And I drag the following fields to respective categories
      | field                        | category |
      | Proficient (>= lvl 4) %      | Measures |
      | Proficient (>= lvl 4) Count  | Measures |
      | Federal Ethnicity            | Rows     |
      | Sex                          | Columns  |
    And I drag the following fields to respective categories
      | field                        | category |
      | Economic Disadvantage Status | Rows     |
    And I click play to run the report
    Then I should see the same results from test case "4.3" query for "first" tenant in "5" columns with the following parameters
      | field    | value |
      | state_id | NY    |

    When I click on the "Swap axis" button in the toolbar
    And I click play to run the report
    Then I should see the same results from test case "4.4" query for "first" tenant in "29" columns with the following parameters
      | field    | value |
      | state_id | NY    |

  @testcase_5_1
  @RALLY_US37258
  Scenario: Test Case 5 - second part: Verify accommodation analysis -- User is able to slice and dice assessment results by student attributes
    When I drag the following fields to respective categories
      | field                  | category  |
      | Performance Level 5 %  | Measures  |
      | Performance Level 4 %  | Measures  |
      | Performance Level 1 %  | Measures  |
      | Performance Level 3 %  | Measures  |
      | Performance Level 2 %  | Measures  |
    And I click on "Accommodations" in the left nav
    And I drag the following fields to respective categories
      | field                         | category  |
      | Assessment Accommodation: EL  | Rows      |
    And I click play to run the report
    Then I should see the same results from test case "5.1" query for "first" tenant in "6" columns with the following parameters
      | field     | value  |
      | state_id  | NY     |

    When I drag the following fields to respective categories
      | field                         | category  |
      | Assessment Accommodation: 504 | Rows      |
    And I click play to run the report
    Then I should see the same results from test case "5.2" query for "first" tenant in "5" columns with the following parameters
      | field     | value  |
      | state_id  | NY     |

  @wip
  @testcase_5_2
  @RALLY_US37258
  Scenario: Test Case 5 - first part: Verify accommodation analysis -- User is able to slice and dice assessment results by student attributes
    # NOTE: this is @wip-ed because this test consistently fails on Jenkins because
    # "Assessment Accommodation: Individualized Educational Program (IEP)" is too long.
    # See DE1306
    When I drag the following fields to respective categories
      | field                  | category  |
      | Performance Level 5 %  | Measures  |
      | Performance Level 4 %  | Measures  |
      | Performance Level 1 %  | Measures  |
      | Performance Level 3 %  | Measures  |
      | Performance Level 2 %  | Measures  |
    And I click on "Accommodations" in the left nav
    And I drag the following fields to respective categories
      | field                         | category  |
      | Assessment Accommodation: EL  | Rows      |
    And I drag the following fields to respective categories
      | field                         | category  |
      | Assessment Accommodation: 504 | Rows      |
    And I drag the following fields to respective categories
      | field                                                              | category |
      | Assessment Accommodation: Individualized Educational Program (IEP) | Rows     |
    And I click play to run the report
    Then I should see the same results from test case "5.3" query for "first" tenant in "5" columns with the following parameters
      | field     | value  |
      | state_id  | NY     |

  @testcase_6
  @RALLY_US37258
  Scenario: Test Case 6: Verify the report showing Performance level 5 counts of gifted female students for Math and ELA
    When I select the following values within the category on the left-hand sidebar
      | category | values                   |
      | measures | Performance Lvl 5 Count  |
    And I click on "Poy" in the left nav
    And I click on "Student" in the left nav
    And I drag the following fields to respective categories
      | field               | category  |
      | Assessment Year     | Filters   |
      | Gifted and Talented | Filters   |
      | Sex                 | Filters   |
    And I filter on the following options
      | field               | option | label     |
      | Assessment Year     | 2      | 2014-2015 |
      | Gifted and Talented | 2      | Y         |
      | Sex                 | 1      | F         |
    And I click on "Assessment(ELA-Math)" in the left nav
    And I drag the following fields to respective categories
      | field      | category |
      | Math - ELA | Columns  |
    And I drag the following fields to respective categories
      | field                     | category |
      | Grade Level               | Rows     |
      | PARCC Student Identifier  | Rows     |
      | Name                      | Rows     |
    And I click play to run the report
    Then I should see the following values in row "7" of the table
      | column_num  | value                                     | tag |
      | 1           | 09                                        | th  |
      | 2           | 0YrEK4Y3PihDRZiZnFMinOcTdkNYwublx9Z8lCAp  | th  |
      | 3           | Mcgowin, Marisela Marisela                | th  |
      | 1           | 0                                         | td  |
      | 2           | 0                                         | td  |
    And I should see the same results from test case "6.1" query for "first" tenant in "5" columns with the following parameters
      | field          | value     |
      | state_id       | NY        |
      | school_year    | 2014-2015 |
      | student_sex    | F         |
      | student_gifted | Y         |

  @testcase_7
  @RALLY_US37258
  Scenario: Test Case 7: Verify the report showing Scale Score Mean and counts on Students Registered, Valid and No Valid scores for district and schools.
    When I select the following values within the category on the left-hand sidebar
      | category  | values                   |
      | measures  | Scale Score Mean         |
      | measures  | Student Registered Count |
      | measures  | Valid Score Count        |
      | measures  | No Valid Score Count     |
    And I click on "Poy" in the left nav
    And I click on "Assessment(ELA-Math)" in the left nav
    And I drag the following fields to respective categories
      | field           | category |
      | Assessment Year | Filters  |
      | Math - ELA      | Filters  |
    And I filter on the following options
      | field           | option | label     |
      | Assessment Year | 2      | 2014-2015 |
      | Math - ELA      | 2      | math      |
    And I click on "Responsible Institution" in the left nav
    And I drag the following fields to respective categories
      | field                     | category |
      | Responsible District Name | Rows     |
    And I click play to run the report
    Then I should see the following values in row "2" of the table
      | column_num  | value   | tag |
      | 1           | Newport | th  |
      | 1           | 278.20  | td  |
      | 2           | 20      | td  |
      | 3           | 13      | td  |
      | 4           | 7       | td  |
    And I should see the same results from test case "7.1" query for "first" tenant in "5" columns with the following parameters
      | field       | value     |
      | state_id    | NY        |
      | school_year | 2014-2015 |
      | me_flag     | math      |

    When I drag the following fields to respective categories
      | field                   | category |
      | Responsible School Name | Rows     |
    And I click play to run the report
    Then I should see the following values in row "3" of the table
      | column_num  | value                | tag  |
      | 1           | Providence           | th   |
      | 2           | Alvarez High School  | th   |
      | 1           | 257.45               | td   |
      | 2           | 20                   | td   |
      | 3           | 12                   | td   |
      | 4           | 8                    | td   |
    And I should see the same results from test case "7.2" query for "first" tenant in "6" columns with the following parameters
      | field        | value      |
      | state_id     | NY         |
      | school_year  | 2014-2015  |
      | me_flag      | math       |

  @testcase_8
  @RALLY_US37258
  Scenario: Test Case 8: Verify like school and district comparison -- Verify ELA and Math assessment simultaneously
    When I click on "Responsible Institution" in the left nav
    And I click on "Student" in the left nav
    And I drag the following fields to respective categories
      | field                         | category                                |
      | Distinct Student Count        | Measures                                |
      | Responsible District Name     | Rows                                    |
      | Hispanic or Latino Ethnicity  | Rows                                    |
      | Migrant Status                | Rows                                    |
      | Economic Disadvantage Status  | Rows                                    |
    And I click on "Responsible District Name" from the Workspace Fields
    And I select "Sum" from the "Sub totals" dropdown and click " OK "
    And I click on "Hispanic or Latino Ethnicity" from the Workspace Fields
    And I select "Sum" from the "Sub totals" dropdown and click " OK "
    And I click on "Migrant Status" from the Workspace Fields
    And I select "Sum" from the "Sub totals" dropdown and click " OK "
    And I click play to run the report
    Then I should see the same results from test case "8.1" query for "first" tenant in "5" columns with the following parameters
      | field                               | value                             |
      | state_id                            | NY                                |

    When I click the clear axis button on the "ROWS" area
    And I click the clear axis button on the "MEASURES" area
    And I click on "Poy" in the left nav
    And I click on "Assessment(ELA-Math)" in the left nav
    And I drag the following fields to respective categories
      | field                         | category                                |
      | Scale Score Mean              | Measures                                |
      | Assessment Year               | Filters                                 |
      | Math - ELA                    | Filters                                 |
    And I filter on the following options
      | field               | option     | label                                |
      | Assessment Year     | 2          | 2014-2015                            |
      | Math - ELA          | 2          | math                                 |
    And I click on "Test" in the left nav
    And I drag the following fields to respective categories
      | field                         | category                                |
      | Responsible District Name     | Rows                                    |
      | Responsible School Name       | Rows                                    |
      | Assessment Grade/Test Name    | Rows                                    |
      | Subject                       | Rows                                    |
    And I filter on the following options
      | field                         | option     | label                      |
      | Responsible District Name     | 2          | Newport                    |
    And I click play to run the report
    Then I should see the same results from test case "8.2" query for "first" tenant in "5" columns with the following parameters
      | field                               | value                             |
      | state_id                            | NY                                |
      | school_year                         | 2014-2015                         |
      | me_flag                             | Math                              |
      | specific_district_name              | Newport                           |

  @testcase_9
  @RALLY_US37258
  Scenario: Test Case 9: Verify the report showing Distribution Subclaim 1 Levels for district and schools.
    When I select the following values within the category on the left-hand sidebar
      | category  | values                                 |
      | measures  | Distribution Subclaim 1: Level 1 Count |
      | measures  | Distribution Subclaim 1: Level 2 Count |
      | measures  | Distribution Subclaim 1: Level 3 Count |
    And I click on "Poy" in the left nav
    And I click on "Assessment(ELA-Math)" in the left nav
    And I drag the following fields to respective categories
      | field           | category |
      | Assessment Year | Columns  |
      | Math - ELA      | Columns  |
    And I click on "Responsible Institution" in the left nav
    And I drag the following fields to respective categories
      | field                     | category  |
      | Responsible District Name | Rows      |
      | Responsible School Name   | Rows      |
    And I click play to run the report
    Then I should see the following values in row "4" of the table
      | column_num  | value             | tag |
      | 1           |                   | th  |
      | 2           | Hope High School  | th  |
      | 1           | 1                 | td  |
      | 2           | 0                 | td  |
      | 3           | 2                 | td  |
      | 4           | 0                 | td  |
      | 5           | 3                 | td  |
      | 6           | 0                 | td  |
      | 7           | 6                 | td  |
      | 8           | 9                 | td  |
      | 9           | 5                 | td  |
      | 10          | 8                 | td  |
      | 11          | 16                | td  |
      | 12          | 3                 | td  |
    And I should see the same results from test case "9.1" query for "first" tenant in "14" columns with the following parameters
      | field    | value |
      | state_id | NY    |

  @wip  # wip until we don't have valid test data
  @testcase_10
  @RALLY_US37258
  Scenario: Test Case 10: Verify identification of students without valid summative records -- User is able to identify individual students who have an EOY or PBA record (or both) but have no valid summative score
    When I drag the following fields to respective categories
      | field                         | category                                |
      | Distinct Student Count        | Measures                                |
    And I click on "Poy" in the left nav
    And I click on "Record Flags" in the left nav
    And I drag the following fields to respective categories
      | field                         | category                                |
      | Assessment Year               | Filters                                 |
      | Record Type                   | Filters                                 |
    And I filter on the following options
      | field               | option     | label                                |
      | Assessment Year     | 2          | 2014-2015                            |
      | Record Type         | 3          | 03                                   |
    And I click on "Assessment(ELA-Math)" in the left nav
    And I drag the following fields to respective categories
      | field                         | category                                |
      | Math - ELA                    | Columns                                 |
    And I click on "PBA Tests" in the left nav
    And I drag the following fields to respective categories
      | field                         | category                                |
      | PBA Type 03 Category          | Rows                                    |
    And I click play to run the report
    Then I should see the same results from test case "10.1" query for "first" tenant in "3" columns with the following parameters
      | field                               | value                             |
      | state_id                            | NY                                |
      | school_year                         | 2014-2015                         |
      | record_type                         | 03                                |

    When I click on "Responsible Institution" in the left nav
    And I click on "Student" in the left nav
    And I drag the following fields to respective categories
      | field                         | category                                |
      | Name                          | Rows                                    |
      | Responsible School Name       | Rows                                    |
    And I click play to run the report
    Then I should see the same results from test case "10.2" query for "first" tenant in "5" columns with the following parameters
      | field                               | value                             |
      | state_id                            | NY                                |
      | school_year                         | 2014-2015                         |
      | record_type                         | 03                                |
