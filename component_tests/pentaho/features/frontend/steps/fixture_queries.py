QUERY_1_1 = """
SELECT di.dist_name,
       di.school_name,
       dut.asmt_grade,
       dut.test_subject,
       Round(Avg(fs.sum_scale_score), 2) AS avg_scale_score,
       Round(Avg(fs.sum_read_scale_score), 2) AS avg_read,
       Round(Avg(fs.sum_write_scale_score), 2) AS avg_write
FROM analytics.fact_sum AS fs

JOIN analytics.dim_accomod da ON da._key_accomod = fs._key_accomod
JOIN analytics.dim_student ds ON ds._key_student = fs._key_student
JOIN analytics.dim_school di ON di._key_school = fs._key_resp_school
JOIN analytics.dim_unique_test dut ON dut._key_unique_test = fs._key_unique_test
JOIN analytics.dim_poy dp ON dp._key_poy = fs._key_poy
AND di.state_id = '{state_id}'
AND dp.school_year = '{school_year}'
WHERE me_flag = '{me_flag}'
GROUP BY di.dist_name,
         di.school_name,
         dut.asmt_grade,
         dut.test_subject
ORDER BY di.dist_name,
         di.school_name
"""

QUERY_2_1 = """
SELECT di.dist_name,
       di.school_name,
       Round(Avg(fs.sum_scale_score), 2) AS avg_scale_score
FROM analytics.fact_sum fs
JOIN analytics.dim_accomod da ON da._key_accomod = fs._key_accomod
JOIN analytics.dim_student ds ON ds._key_student = fs._key_student
JOIN analytics.dim_school di ON di._key_school = fs._key_resp_school
JOIN analytics.dim_poy dp ON dp._key_poy = fs._key_poy
AND di.state_id = '{state_id}'
AND dp.school_year = '{school_year}'
WHERE me_flag = '{me_flag}'
GROUP BY di.dist_name,
         di.school_name
ORDER BY di.dist_name,
         di.school_name,
         avg_scale_score
"""

QUERY_2_2 = """
SELECT di.dist_name,
       di.school_name,
       dut.asmt_grade,
       dut.test_subject,
       Round(Avg(fs.sum_scale_score), 2) AS avg_scale_score
FROM analytics.fact_sum fs
JOIN analytics.dim_accomod da ON da._key_accomod = fs._key_accomod
JOIN analytics.dim_student ds ON ds._key_student = fs._key_student
JOIN analytics.dim_school di ON di._key_school = fs._key_resp_school
JOIN analytics.dim_unique_test dut ON dut._key_unique_test = fs._key_unique_test
JOIN analytics.dim_poy dp ON dp._key_poy = fs._key_poy
AND dp.school_year = '{school_year}'
AND di.state_id = '{state_id}'
WHERE me_flag = '{me_flag}'
GROUP BY di.dist_name,
         di.school_name,
         dut.asmt_grade,
         dut.test_subject
ORDER BY di.dist_name,
         di.school_name,
         dut.asmt_grade NULLS FIRST,
         dut.test_subject
"""

QUERY_3_1 = """
WITH eng_sum
    AS (SELECT school_name,
               Round(Avg(sum_scale_score), 2) AS avg_eng_score
        FROM analytics.fact_sum fs1
        JOIN analytics.dim_accomod da1 ON da1._key_accomod = fs1._key_accomod
        JOIN analytics.dim_student ds1 ON ds1._key_student = fs1._key_student
        JOIN analytics.dim_school di1 ON di1._key_school = fs1._key_resp_school
        JOIN analytics.dim_poy dp1 ON dp1._key_poy = fs1._key_poy
        AND di1.state_id = '{state_id}'
        AND dp1.school_year = '{school_year}'
        WHERE fs1.me_flag = 'ela'
        GROUP BY dist_name,
                 school_name
        ORDER BY dist_name,
                 school_name)
SELECT di.dist_name,
      di.school_name,
      eng_sum.avg_eng_score,
      Round(Avg(sum_scale_score), 2) AS avg_math_score
FROM analytics.fact_sum fs
JOIN analytics.dim_accomod da ON da._key_accomod = fs._key_accomod
JOIN analytics.dim_student ds ON ds._key_student = fs._key_student
JOIN analytics.dim_school di ON di._key_school = fs._key_resp_school
JOIN analytics.dim_poy dp ON dp._key_poy = fs._key_poy
JOIN eng_sum ON di.school_name = eng_sum.school_name
AND di.state_id = '{state_id}'
AND dp.school_year = '{school_year}'
WHERE fs.me_flag = 'math'
GROUP BY dist_name,
        di.school_name,
        eng_sum.avg_eng_score
ORDER BY dist_name,
        di.school_name
"""

QUERY_3_2 = """
SELECT di.dist_name,
       di.school_name,
       dut.asmt_grade,
       dut.test_subject,
       (CASE
            WHEN me_flag = 'ela' THEN Round(Avg(fs.sum_scale_score), 2)
        END) AS avg_eng_score,
       (CASE
            WHEN me_flag = 'math' THEN Round(Avg(fs.sum_scale_score), 2)
        END) AS avg_math_score

FROM analytics.fact_sum fs
JOIN analytics.dim_poy dp ON fs._key_poy = dp._key_poy
JOIN analytics.dim_school di ON fs._key_resp_school = di._key_school
JOIN analytics.dim_unique_test dut ON fs._key_unique_test = dut._key_unique_test
AND dp.school_year = '{school_year}'
AND di.state_id = '{state_id}'
GROUP BY fs.me_flag,
         di.dist_name,
         di.school_name,
         dut.asmt_grade,
         dut.test_subject
ORDER BY di.dist_name,
         di.school_name,
         dut.asmt_grade NULLS FIRST,
         dut.test_subject,
         fs.me_flag
"""

QUERY_4_1 = """
WITH total_count AS
     (SELECT ds.ethnicity,
             SUM(CASE WHEN fs.sum_perf_lvl >= 4 THEN 1 ELSE 0 END) AS total_perf_4_ct,
             Count(*) AS total_ct
      FROM analytics.dim_student ds
      JOIN analytics.fact_sum fs ON ds._key_student = fs._key_student
      JOIN analytics.dim_school di ON di._key_school = fs._key_resp_school
      AND di.state_id = '{state_id}'
      WHERE fs.sum_perf_lvl >= 0
      AND fs.record_type = '01'
      GROUP BY ds.ethnicity
      ORDER BY ds.ethnicity,
               total_ct)
SELECT ethnicity,
       total_perf_4_ct,
       To_char(Round((total_perf_4_ct * 100.00 / total_ct * 1.00), 2), '990D99%%')
FROM total_count
"""

QUERY_4_2 = """
WITH total_count
     AS (SELECT ds.ethnicity,
         SUM(CASE WHEN ds.student_sex = 'M' THEN 1 ELSE 0 END) AS total_male,
         SUM(CASE WHEN ds.student_sex = 'F' THEN 1 ELSE 0 END) AS total_female,
         Count(*) AS total_ct

         FROM analytics.dim_student ds
         JOIN analytics.fact_sum fs ON ds._key_student = fs._key_student
         WHERE fs.sum_perf_lvl >= 0
         AND fs.record_type = '01'
         GROUP BY ds.ethnicity
         ORDER BY ds.ethnicity,
                  total_ct),

    perf_count
    AS (SELECT ds.ethnicity,
        Count(CASE WHEN fs.sum_perf_lvl >= 4 AND ds.student_sex = 'F'
                   THEN 1
                   ELSE NULL
              END) AS perf_4_F_count,
        tc.total_female AS perf_F_ct,
        Count(CASE WHEN fs.sum_perf_lvl >= 4 AND ds.student_sex = 'M'
                   THEN 1
                   ELSE NULL
              END) AS perf_4_M_count,
        tc.total_male AS perf_M_ct

        FROM analytics.fact_sum fs
        JOIN analytics.dim_student ds ON ds._key_student = fs._key_student
        JOIN analytics.dim_accomod da ON da._key_accomod = fs._key_accomod
        JOIN analytics.dim_school di ON di._key_school = fs._key_resp_school
        JOIN analytics.dim_poy dp ON dp._key_poy = fs._key_poy
        JOIN total_count tc ON tc.ethnicity = ds.ethnicity
        AND di.state_id = '{state_id}'
        WHERE fs.record_type = '01'
        AND fs.sum_perf_lvl >= 0
        GROUP BY ds.ethnicity,
                 tc.total_female,
                 tc.total_male
        ORDER BY ds.ethnicity)

SELECT perf_count.ethnicity,
       perf_4_f_count,
       To_char(Round((perf_4_f_count * 100.00 / perf_f_ct * 1.00), 2), '990D99%%') AS lvl4_percentage_F,
       perf_4_m_count,
       To_char(Round((perf_4_m_count * 100.00 / perf_m_ct * 1.00), 2), '990D99%%') AS lvl4_percentage_M
FROM perf_count
"""
QUERY_4_3 = """
WITH perf_count
   AS (SELECT ds.ethnicity,
              ds.econo_disadvantage,
              Count(CASE WHEN ds.student_sex = 'F' THEN 1 ELSE NULL END) AS perf_4_f_count_all,
              Count(CASE WHEN fs.sum_perf_lvl >= 4 AND ds.student_sex = 'F'
                          AND fs.record_type = '01' THEN 1 ELSE NULL END) AS perf_4_f_count,
              Count(CASE WHEN fs.sum_perf_lvl >= 0 AND ds.student_sex = 'F'
                          AND fs.record_type = '01' THEN 1 ELSE NULL END) AS perf_f_ct,
              Count(CASE WHEN ds.student_sex = 'M' THEN 1 ELSE NULL END) AS perf_4_m_count_all,
              Count(CASE WHEN fs.sum_perf_lvl >= 4 AND ds.student_sex = 'M'
                          AND fs.record_type = '01' THEN 1 ELSE NULL END) AS perf_4_m_count,
              Count(CASE WHEN fs.sum_perf_lvl >= 0 AND ds.student_sex = 'M'
                          AND fs.record_type = '01' THEN 1 ELSE NULL END) AS perf_m_ct
       FROM analytics.fact_sum fs
       JOIN analytics.dim_student ds ON ds._key_student = fs._key_student
       JOIN analytics.dim_accomod da ON da._key_accomod = fs._key_accomod
       JOIN analytics.dim_school di ON di._key_school = fs._key_resp_school
       JOIN analytics.dim_poy dp ON dp._key_poy = fs._key_poy
       AND di.state_id = '{state_id}'
       WHERE fs.sum_perf_lvl >= 0
       GROUP BY ds.ethnicity,
                ds.econo_disadvantage
       ORDER BY ds.ethnicity)
SELECT perf_count.ethnicity,
       perf_count.econo_disadvantage,
       CASE WHEN perf_4_f_count_all = 0 THEN NULL
            ELSE perf_4_f_count
       END,
       CASE WHEN perf_f_ct = 0 THEN NULL
            ELSE To_char(Round((perf_4_f_count * 100.00 / perf_f_ct * 1.00), 2), '990D99%%')
       END AS lvl4_percentage_F,
       CASE WHEN perf_4_m_count_all = 0 THEN NULL
            ELSE perf_4_m_count
       END,
       CASE WHEN perf_m_ct = 0 THEN NULL
            ELSE To_char(Round((perf_4_m_count * 100.00 / perf_m_ct * 1.00), 2), '990D99%%')
       END AS lvl4_percentage_M
FROM perf_count
ORDER BY perf_count.ethnicity,
         perf_count.econo_disadvantage
"""

QUERY_4_4 = """
WITH perf_count
AS (
    SELECT ds.student_sex,
        COUNT(CASE WHEN ds.ethnicity = 'American Indian or Alaska Native'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS ai_n_ct_x,
        COUNT(CASE WHEN ds.ethnicity = 'American Indian or Alaska Native'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS ai_y_ct_x,
        COUNT(CASE WHEN ds.ethnicity = 'Asian'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS a_n_ct_x,
        COUNT(CASE WHEN ds.ethnicity = 'Asian'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS a_y_ct_x,
        COUNT(CASE WHEN ds.ethnicity = 'Black or African American'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS aa_n_ct_x,
        COUNT(CASE WHEN ds.ethnicity = 'Black or African American'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS aa_y_ct_x,
        COUNT(CASE WHEN ds.ethnicity = 'Hispanic or Latino'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS h_n_ct_x,
        COUNT(CASE WHEN ds.ethnicity = 'Hispanic or Latino'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS h_y_ct_x,
        COUNT(CASE WHEN ds.ethnicity = 'Native Hawaiian or other Pacific Islander'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS nh_n_ct_x,
        COUNT(CASE WHEN ds.ethnicity = 'Native Hawaiian or other Pacific Islander'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS nh_y_ct_x,
        COUNT(CASE WHEN ds.ethnicity = 'Two or more races'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS t_n_ct_x,
        COUNT(CASE WHEN ds.ethnicity = 'Two or more races'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS t_y_ct_x,
        COUNT(CASE WHEN ds.ethnicity = 'White'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS w_n_ct_x,
        COUNT(CASE WHEN ds.ethnicity = 'White'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS w_y_ct_x,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 4
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'American Indian or Alaska Native'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS ai_n_ct,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 4
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'American Indian or Alaska Native'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS ai_y_ct,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 4
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'Asian'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS a_n_ct,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 4
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'Asian'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS a_y_ct,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 4
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'Black or African American'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS aa_n_ct,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 4
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'Black or African American'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS aa_y_ct,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 4
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'Hispanic or Latino'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS h_n_ct,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 4
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'Hispanic or Latino'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS h_y_ct,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 4
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'Native Hawaiian or other Pacific Islander'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS nh_n_ct,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 4
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'Native Hawaiian or other Pacific Islander'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS nh_y_ct,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 4
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'Two or more races'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS t_n_ct,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 4
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'Two or more races'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS t_y_ct,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 4
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'White'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS w_n_ct,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 4
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'White'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS w_y_ct,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 0
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'American Indian or Alaska Native'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS ai_n_ct_all,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 0
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'American Indian or Alaska Native'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS ai_y_ct_all,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 0
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'Asian'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS a_n_ct_all,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 0
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'Asian'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS a_y_ct_all,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 0
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'Black or African American'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS aa_n_ct_all,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 0
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'Black or African American'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS aa_y_ct_all,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 0
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'Hispanic or Latino'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS h_n_ct_all,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 0
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'Hispanic or Latino'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS h_y_ct_all,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 0
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'Native Hawaiian or other Pacific Islander'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS nh_n_ct_all,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 0
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'Native Hawaiian or other Pacific Islander'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS nh_y_ct_all,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 0
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'Two or more races'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS t_n_ct_all,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 0
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'Two or more races'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS t_y_ct_all,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 0
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'White'
                    AND ds.econo_disadvantage = 'N' THEN 1 ELSE NULL END) AS w_n_ct_all,
        COUNT(CASE WHEN fs.sum_perf_lvl >= 0
                    AND fs.record_type = '01'
                    AND ds.ethnicity = 'White'
                    AND ds.econo_disadvantage = 'Y' THEN 1 ELSE NULL END) AS w_y_ct_all
    FROM analytics.fact_sum fs
    JOIN analytics.dim_student ds ON ds._key_student = fs._key_student
     AND (ds.student_sex = 'F' OR ds.student_sex = 'M')
    JOIN analytics.dim_accomod da ON da._key_accomod = fs._key_accomod
    JOIN analytics.dim_school di ON di._key_school = fs._key_resp_school
     AND di.state_id = '{state_id}'
    JOIN analytics.dim_poy dp ON dp._key_poy = fs._key_poy
    WHERE fs.sum_perf_lvl >= 0
    GROUP BY ds.student_sex
    ORDER BY ds.student_sex)
SELECT student_sex,
       CASE
           WHEN ai_n_ct_x = 0 THEN NULL
           ELSE ai_n_ct
       END,
       CASE
           WHEN ai_n_ct_all = 0 THEN NULL
           ELSE To_char(ROUND((ai_n_ct * 100.00 / ai_n_ct_all * 1.00), 2), '990D99%%')
       END,
       CASE
           WHEN ai_y_ct_x = 0 THEN NULL
           ELSE ai_y_ct
       END,
       CASE
           WHEN ai_y_ct_all = 0 THEN NULL
           ELSE To_char(ROUND((ai_y_ct * 100.00 / ai_y_ct_all * 1.00), 2), '990D99%%')
       END,
       CASE
           WHEN a_n_ct_x = 0 THEN NULL
           ELSE a_n_ct
       END,
       CASE
           WHEN a_n_ct_all = 0 THEN NULL
           ELSE To_char(ROUND((a_n_ct * 100.00 / a_n_ct_all * 1.00), 2), '990D99%%')
       END,
       CASE
           WHEN a_y_ct_x = 0 THEN NULL
           ELSE a_y_ct
       END,
       CASE
           WHEN a_y_ct_all = 0 THEN NULL
           ELSE To_char(ROUND((a_y_ct * 100.00 / a_y_ct_all * 1.00), 2), '990D99%%')
       END,
       CASE
           WHEN aa_n_ct_x = 0 THEN NULL
           ELSE aa_n_ct
       END,
       CASE
           WHEN aa_n_ct_all = 0 THEN NULL
           ELSE To_char(ROUND((aa_n_ct * 100.00 / aa_n_ct_all * 1.00), 2), '990D99%%')
       END,
       CASE
           WHEN aa_y_ct_x = 0 THEN NULL
           ELSE aa_y_ct
       END,
       CASE
           WHEN aa_y_ct_all = 0 THEN NULL
           ELSE To_char(ROUND((aa_y_ct * 100.00 / aa_y_ct_all * 1.00), 2), '990D99%%')
       END,
       CASE
           WHEN h_n_ct_x = 0 THEN NULL
           ELSE h_n_ct
       END,
       CASE
           WHEN h_n_ct_all = 0 THEN NULL
           ELSE To_char(ROUND((h_n_ct * 100.00 / h_n_ct_all * 1.00), 2), '990D99%%')
       END,
       CASE
           WHEN h_y_ct_x = 0 THEN NULL
           ELSE h_y_ct
       END,
       CASE
           WHEN h_y_ct_all = 0 THEN NULL
           ELSE To_char(ROUND((h_y_ct * 100.00 / h_y_ct_all * 1.00), 2), '990D99%%')
       END,
       CASE
           WHEN nh_n_ct_x = 0 THEN NULL
           ELSE nh_n_ct
       END,
       CASE
           WHEN nh_n_ct_all = 0 THEN NULL
           ELSE To_char(ROUND((nh_n_ct * 100.00 / nh_n_ct_all * 1.00), 2), '990D99%%')
       END,
       CASE
           WHEN nh_y_ct_x = 0 THEN NULL
           ELSE nh_y_ct
       END,
       CASE
           WHEN nh_y_ct_all = 0 THEN NULL
           ELSE To_char(ROUND((nh_y_ct * 100.00 / nh_y_ct_all * 1.00), 2), '990D99%%')
       END,
       CASE
           WHEN t_n_ct_x = 0 THEN NULL
           ELSE t_n_ct
       END,
       CASE
           WHEN t_n_ct_all = 0 THEN NULL
           ELSE To_char(ROUND((t_n_ct * 100.00 / t_n_ct_all * 1.00), 2), '990D99%%')
       END,
       CASE
           WHEN t_y_ct_x = 0 THEN NULL
           ELSE t_y_ct
       END,
       CASE
           WHEN t_y_ct_all = 0 THEN NULL
           ELSE To_char(ROUND((t_y_ct * 100.00 / t_y_ct_all * 1.00), 2), '990D99%%')
       END,
       CASE
           WHEN w_n_ct_x = 0 THEN NULL
           ELSE w_n_ct
       END,
       CASE
           WHEN w_n_ct_all = 0 THEN NULL
           ELSE To_char(ROUND((w_n_ct * 100.00 / w_n_ct_all * 1.00), 2), '990D99%%')
       END,
       CASE
           WHEN w_y_ct_x = 0 THEN NULL
           ELSE w_y_ct
       END,
       CASE
           WHEN w_y_ct_all = 0 THEN NULL
           ELSE To_char(ROUND((w_y_ct * 100.00 / w_y_ct_all * 1.00), 2), '990D99%%')
       END
FROM perf_count
"""

QUERY_5_1 = """
WITH perf_count AS
  (SELECT accomod_ell,
          COUNT(*) AS perf_ct,
          COUNT(CASE WHEN sum_perf_lvl = 1 THEN 1 ELSE NULL END) AS perf_1_count,
          COUNT(CASE WHEN sum_perf_lvl = 2 THEN 1 ELSE NULL END) AS perf_2_count,
          COUNT(CASE WHEN sum_perf_lvl = 3 THEN 1 ELSE NULL END) AS perf_3_count,
          COUNT(CASE WHEN sum_perf_lvl = 4 THEN 1 ELSE NULL END) AS perf_4_count,
          COUNT(CASE WHEN sum_perf_lvl = 5 THEN 1 ELSE NULL END) AS perf_5_count
   FROM analytics.fact_sum fs
   JOIN analytics.dim_student ds ON ds._key_student = fs._key_student
   JOIN analytics.dim_accomod da ON da._key_accomod = fs._key_accomod
   JOIN analytics.dim_school di ON di._key_school = fs._key_resp_school
   AND di.state_id = '{state_id}'
   JOIN analytics.dim_poy dp ON dp._key_poy = fs._key_poy
   WHERE sum_perf_lvl >= 0
     AND record_type = '01'
   GROUP BY accomod_ell
   ORDER BY accomod_ell)
SELECT accomod_ell,
       TO_CHAR(ROUND((perf_1_count * 100.00 / perf_ct * 1.00), 2), '990D99%%') AS lvl1_percentage,
       TO_CHAR(ROUND((perf_2_count * 100.00 / perf_ct * 1.00), 2), '990D99%%') AS lvl2_percentage,
       TO_CHAR(ROUND((perf_3_count * 100.00 / perf_ct * 1.00), 2), '990D99%%') AS lvl3_percentage,
       TO_CHAR(ROUND((perf_4_count * 100.00 / perf_ct * 1.00), 2), '990D99%%') AS lvl4_percentage,
       TO_CHAR(ROUND((perf_5_count * 100.00 / perf_ct * 1.00), 2), '990D99%%') AS lvl5_percentage
FROM perf_count
"""

QUERY_5_2 = """
WITH perf_count AS
  (SELECT accomod_ell,
          accomod_504,
          COUNT(*) AS perf_ct,
          COUNT(CASE WHEN sum_perf_lvl = 1 THEN 1 ELSE NULL END) AS perf_1_count,
          COUNT(CASE WHEN sum_perf_lvl = 2 THEN 1 ELSE NULL END) AS perf_2_count,
          COUNT(CASE WHEN sum_perf_lvl = 3 THEN 1 ELSE NULL END) AS perf_3_count,
          COUNT(CASE WHEN sum_perf_lvl = 4 THEN 1 ELSE NULL END) AS perf_4_count,
          COUNT(CASE WHEN sum_perf_lvl = 5 THEN 1 ELSE NULL END) AS perf_5_count
   FROM analytics.fact_sum fs
   JOIN analytics.dim_student ds ON ds._key_student = fs._key_student
   JOIN analytics.dim_accomod da ON da._key_accomod = fs._key_accomod
   JOIN analytics.dim_school di ON di._key_school = fs._key_resp_school
   AND di.state_id = '{state_id}'
   JOIN analytics.dim_poy dp ON dp._key_poy = fs._key_poy
   WHERE sum_perf_lvl >= 0
   AND record_type = '01'
   GROUP BY accomod_ell,
            accomod_504
   ORDER BY accomod_ell,
            accomod_504,
            perf_ct)
SELECT accomod_ell,
       accomod_504,
       TO_CHAR(ROUND((perf_1_count * 100.00 / perf_ct * 1.00), 2), '990D99%%') AS lvl1_percentage,
       TO_CHAR(ROUND((perf_2_count * 100.00 / perf_ct * 1.00), 2), '990D99%%') AS lvl2_percentage,
       TO_CHAR(ROUND((perf_3_count * 100.00 / perf_ct * 1.00), 2), '990D99%%') AS lvl3_percentage,
       TO_CHAR(ROUND((perf_4_count * 100.00 / perf_ct * 1.00), 2), '990D99%%') AS lvl4_percentage,
       TO_CHAR(ROUND((perf_5_count * 100.00 / perf_ct * 1.00), 2), '990D99%%') AS lvl5_percentage
FROM perf_count
"""

QUERY_5_3 = """
WITH perf_count AS
  (SELECT accomod_ell,
          accomod_504,
          accomod_ind_ed,
          COUNT(*) AS perf_ct,
          COUNT(CASE WHEN sum_perf_lvl = 1 THEN 1 ELSE NULL END) AS perf_1_count,
          COUNT(CASE WHEN sum_perf_lvl = 2 THEN 1 ELSE NULL END) AS perf_2_count,
          COUNT(CASE WHEN sum_perf_lvl = 3 THEN 1 ELSE NULL END) AS perf_3_count,
          COUNT(CASE WHEN sum_perf_lvl = 4 THEN 1 ELSE NULL END) AS perf_4_count,
          COUNT(CASE WHEN sum_perf_lvl = 5 THEN 1 ELSE NULL END) AS perf_5_count
   FROM analytics.fact_sum fs
   JOIN analytics.dim_student ds ON ds._key_student = fs._key_student
   JOIN analytics.dim_accomod da ON da._key_accomod = fs._key_accomod
   JOIN analytics.dim_school di ON di._key_school = fs._key_resp_school
   AND di.state_id = '{state_id}'
   JOIN analytics.dim_poy dp ON dp._key_poy = fs._key_poy
   WHERE sum_perf_lvl >= 0
     AND record_type = '01'
   GROUP BY accomod_ell,
            accomod_504,
            accomod_ind_ed
   ORDER BY accomod_ell,
            accomod_504,
            accomod_ind_ed)
SELECT accomod_ell,
       accomod_504,
       accomod_ind_ed,
       TO_CHAR(ROUND((perf_1_count * 100.00 / perf_ct * 1.00), 2), '990D99%%') AS lvl1_percentage,
       TO_CHAR(ROUND((perf_2_count * 100.00 / perf_ct * 1.00), 2), '990D99%%') AS lvl2_percentage,
       TO_CHAR(ROUND((perf_3_count * 100.00 / perf_ct * 1.00), 2), '990D99%%') AS lvl3_percentage,
       TO_CHAR(ROUND((perf_4_count * 100.00 / perf_ct * 1.00), 2), '990D99%%') AS lvl4_percentage,
       TO_CHAR(ROUND((perf_5_count * 100.00 / perf_ct * 1.00), 2), '990D99%%') AS lvl5_percentage
FROM perf_count
"""

QUERY_6_1 = """
SELECT student_grade,
       student_parcc_id,
       student_name,
       Sum(english) AS english,
       Sum(math) AS math
FROM
  (SELECT fs.student_grade,
          ds.student_name,
          ds.student_dob,
          ds.student_parcc_id,
          CASE
              WHEN fs.sum_perf_lvl = 5
                   AND fs.me_flag = 'ela'
                   AND record_type = '01' THEN 1
              WHEN fs.me_flag = 'ela' THEN 0
          END AS english,
          CASE
              WHEN fs.sum_perf_lvl = 5
                   AND fs.me_flag = 'math'
                   AND record_type = '01' THEN 1
              WHEN fs.me_flag = 'math' THEN 0
          END AS math
   FROM analytics.dim_student ds
   JOIN analytics.fact_sum fs ON ds._key_student = fs._key_student
   JOIN analytics.dim_school di ON di._key_school = fs._key_resp_school
   AND di.state_id = '{state_id}'
   JOIN analytics.dim_poy dp ON dp._key_poy = fs._key_poy
   AND dp.school_year = '{school_year}'
   WHERE ds.student_sex = '{student_sex}'
   AND ds.gift_talent = '{student_gifted}') AS FOO
GROUP BY student_grade,
         student_parcc_id,
         student_name
ORDER BY student_grade,
         student_parcc_id,
         student_name
"""

QUERY_7_1 = """
SELECT di.dist_name,
       Round (Avg(fs.sum_scale_score), 2),
       count(fs.rec_id) AS m1,
       Sum(( CASE
              WHEN fs.record_type = '01' THEN 1
              ELSE 0
            END )),
       Sum(( CASE
              WHEN fs.record_type = '02' THEN 1
              WHEN fs.record_type = '03' THEN 1
              ELSE 0
            END ))
FROM   analytics.fact_sum AS fs
JOIN analytics.dim_poy dp ON fs._key_poy = dp._key_poy
AND dp.school_year = '{school_year}'
JOIN analytics.dim_school di ON fs._key_resp_school = di._key_school
AND di.state_id = '{state_id}'

WHERE  fs.me_flag = '{me_flag}'
GROUP  BY di.dist_name
ORDER  BY di.dist_name
"""

QUERY_7_2 = """
SELECT di.dist_name,
       di.school_name,
       Round (Avg(fs.sum_scale_score), 2),
       count(fs.rec_id),
       Sum((CASE WHEN fs.record_type = '01' THEN 1 ELSE 0 END)),
       Sum((CASE WHEN fs.record_type = '02' THEN 1 WHEN fs.record_type = '03' THEN 1 ELSE 0 END))
FROM analytics.fact_sum AS fs
JOIN analytics.dim_poy dp ON fs._key_poy = dp._key_poy
AND dp.school_year = '{school_year}'
JOIN analytics.dim_school di ON fs._key_resp_school = di._key_school
AND di.state_id = '{state_id}'
WHERE fs.me_flag = '{me_flag}'
GROUP BY di.dist_name,
         di.school_name
ORDER BY di.dist_name,
         di.school_name
"""

QUERY_8_1 = """
SELECT DISTINCT dist_name,
                ethn_hisp_latino,
                migrant_status,
                econo_disadvantage,
                COUNT (DISTINCT ds.student_parcc_id)
FROM analytics.fact_sum fs
JOIN analytics.dim_accomod da ON da._key_accomod = fs._key_accomod
JOIN analytics.dim_student ds ON ds._key_student = fs._key_student
JOIN analytics.dim_school di ON di._key_school = fs._key_resp_school
AND di.state_id = '{state_id}'
JOIN analytics.dim_poy dp ON dp._key_poy = fs._key_poy
GROUP BY dist_name,
         ethn_hisp_latino,
         migrant_status,
         econo_disadvantage
UNION
SELECT DISTINCT dist_name,
                ethn_hisp_latino,
                migrant_status,
                NULL,
                COUNT (DISTINCT ds.student_parcc_id)
FROM analytics.fact_sum fs
JOIN analytics.dim_accomod da ON da._key_accomod = fs._key_accomod
JOIN analytics.dim_student ds ON ds._key_student = fs._key_student
JOIN analytics.dim_school di ON di._key_school = fs._key_resp_school
AND di.state_id = '{state_id}'
JOIN analytics.dim_poy dp ON dp._key_poy = fs._key_poy
GROUP BY dist_name,
         ethn_hisp_latino,
         migrant_status
UNION
SELECT DISTINCT dist_name,
                ethn_hisp_latino,
                NULL,
                NULL,
                COUNT (DISTINCT ds.student_parcc_id)
FROM analytics.fact_sum fs
JOIN analytics.dim_accomod da ON da._key_accomod = fs._key_accomod
JOIN analytics.dim_student ds ON ds._key_student = fs._key_student
JOIN analytics.dim_school di ON di._key_school = fs._key_resp_school
AND di.state_id = '{state_id}'
JOIN analytics.dim_poy dp ON dp._key_poy = fs._key_poy
GROUP BY dist_name,
         ethn_hisp_latino
UNION
SELECT DISTINCT dist_name,
                NULL,
                NULL,
                NULL,
                COUNT (DISTINCT ds.student_parcc_id)
FROM analytics.fact_sum fs
JOIN analytics.dim_accomod da ON da._key_accomod = fs._key_accomod
JOIN analytics.dim_student ds ON ds._key_student = fs._key_student
JOIN analytics.dim_school di ON di._key_school = fs._key_resp_school
AND di.state_id = '{state_id}'
JOIN analytics.dim_poy dp ON dp._key_poy = fs._key_poy
GROUP BY dist_name
ORDER BY dist_name,
         ethn_hisp_latino,
         migrant_status,
         econo_disadvantage
"""

QUERY_8_2 = """
SELECT di.dist_name,
       di.school_name,
       dut.asmt_grade,
       dut.test_subject,
       Round(Avg(sum_scale_score), 2) AS avg_scale_score
FROM analytics.fact_sum fs
JOIN analytics.dim_accomod da ON da._key_accomod = fs._key_accomod
JOIN analytics.dim_student ds ON ds._key_student = fs._key_student
JOIN analytics.dim_school di ON di._key_school = fs._key_resp_school
AND di.state_id = '{state_id}'
AND di.dist_name = '{specific_district_name}'
JOIN analytics.dim_poy dp ON dp._key_poy = fs._key_poy
AND dp.school_year = '{school_year}'
JOIN analytics.dim_unique_test dut ON dut._key_unique_test = fs._key_unique_test
WHERE fs.me_flag = '{me_flag}'
GROUP BY dist_name,
         school_name,
         asmt_grade,
         test_subject
ORDER BY dist_name,
         school_name,
         test_subject,
         asmt_grade
"""

QUERY_9_1 = """
SELECT foo.dist_name,
       foo.school_name,
       Sum(prev_eng_1),
       Sum(prev_eng_2),
       Sum(prev_eng_3),
       Sum(prev_math_1),
       Sum(prev_math_2),
       Sum(prev_math_3),
       Sum(curr_eng_1),
       Sum(curr_eng_2),
       Sum(curr_eng_3),
       Sum(curr_math_1),
       Sum(curr_math_2),
       Sum(curr_math_3)
FROM
  (SELECT di.dist_name AS dist_name,
          di.school_name AS school_name,
          (CASE WHEN poy.school_year = '2013-2014'
           AND fs.me_flag = 'ela'
           AND fs.subclaim1_category = 1 THEN 1 WHEN poy.school_year = '2013-2014'
           AND fs.me_flag = 'ela' THEN 0 ELSE NULL END) AS prev_eng_1,
          (CASE WHEN poy.school_year = '2013-2014'
           AND fs.me_flag = 'ela'
           AND fs.subclaim1_category = 2 THEN 1 WHEN poy.school_year = '2013-2014'
           AND fs.me_flag = 'ela' THEN 0 ELSE NULL END) AS prev_eng_2,
          (CASE WHEN poy.school_year = '2013-2014'
           AND fs.me_flag = 'ela'
           AND fs.subclaim1_category = 3 THEN 1 WHEN poy.school_year = '2013-2014'
           AND fs.me_flag = 'ela' THEN 0 ELSE NULL END) AS prev_eng_3,
          (CASE WHEN poy.school_year = '2013-2014'
           AND fs.me_flag = 'math'
           AND fs.subclaim1_category = 1 THEN 1 WHEN poy.school_year = '2013-2014'
           AND fs.me_flag = 'math' THEN 0 ELSE NULL END) AS prev_math_1,
          (CASE WHEN poy.school_year = '2013-2014'
           AND fs.me_flag = 'math'
           AND fs.subclaim1_category = 2 THEN 1 WHEN poy.school_year = '2013-2014'
           AND fs.me_flag = 'math' THEN 0 ELSE NULL END) AS prev_math_2,
          (CASE WHEN poy.school_year = '2013-2014'
           AND fs.me_flag = 'math'
           AND fs.subclaim1_category = 3 THEN 1 WHEN poy.school_year = '2013-2014'
           AND fs.me_flag = 'math' THEN 0 ELSE NULL END) AS prev_math_3,
          (CASE WHEN poy.school_year = '2014-2015'
           AND fs.me_flag = 'ela'
           AND fs.subclaim1_category = 1 THEN 1 WHEN poy.school_year = '2014-2015'
           AND fs.me_flag = 'ela' THEN 0 ELSE NULL END) AS curr_eng_1,
          (CASE WHEN poy.school_year = '2014-2015'
           AND fs.me_flag = 'ela'
           AND fs.subclaim1_category = 2 THEN 1 WHEN poy.school_year = '2014-2015'
           AND fs.me_flag = 'ela' THEN 0 ELSE NULL END) AS curr_eng_2,
          (CASE WHEN poy.school_year = '2014-2015'
           AND fs.me_flag = 'ela'
           AND fs.subclaim1_category = 3 THEN 1 WHEN poy.school_year = '2014-2015'
           AND fs.me_flag = 'ela' THEN 0 ELSE NULL END) AS curr_eng_3,
          (CASE WHEN poy.school_year = '2014-2015'
           AND fs.me_flag = 'math'
           AND fs.subclaim1_category = 1 THEN 1 WHEN poy.school_year = '2014-2015'
           AND fs.me_flag = 'math' THEN 0 ELSE NULL END) AS curr_math_1,
          (CASE WHEN poy.school_year = '2014-2015'
           AND fs.me_flag = 'math'
           AND fs.subclaim1_category = 2 THEN 1 WHEN poy.school_year = '2014-2015'
           AND fs.me_flag = 'math' THEN 0 ELSE NULL END) AS curr_math_2,
          (CASE WHEN poy.school_year = '2014-2015'
           AND fs.me_flag = 'math'
           AND fs.subclaim1_category = 3 THEN 1 WHEN poy.school_year = '2014-2015'
           AND fs.me_flag = 'math' THEN 0 ELSE NULL END) AS curr_math_3
   FROM analytics.fact_sum fs
   JOIN analytics.dim_school di ON fs._key_resp_school = di._key_school
   AND di.state_id = '{state_id}'
   JOIN analytics.dim_poy poy ON poy._key_poy = fs._key_poy
   WHERE (fs.me_flag = 'math'
          OR fs.me_flag = 'ela')) AS foo
GROUP BY foo.dist_name,
         foo.school_name
ORDER BY foo.dist_name,
         foo.school_name
"""

QUERY_10_1 = """
SELECT dt.pba_category,
       Count(CASE WHEN me_flag = 'ela' THEN 1 ELSE NULL END) AS "Distinct Student Count (English)",
       Count(CASE WHEN me_flag = 'math' THEN 1 ELSE NULL END) AS "Distinct Student Count (Math)"
FROM analytics.fact_sum fs
JOIN analytics.dim_accomod da ON da._key_accomod = fs._key_accomod
JOIN analytics.dim_student ds ON ds._key_student = fs._key_student
JOIN analytics.dim_test dt ON dt._key_test = fs._key_pba_test
JOIN analytics.dim_poy dp ON dp._key_poy = fs._key_poy
AND dp.school_year = '{school_year}'
WHERE fs.record_type = '{record_type}'
GROUP BY dt.pba_category
ORDER BY dt.pba_category
"""

# TODO: will be implemented later
# QUERY_10_2 = """
# """
