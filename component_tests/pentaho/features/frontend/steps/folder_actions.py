from behave import when, then
from hamcrest import assert_that, equal_to, is_in
from components_tests.frontend.pentaho_widgets.folder_actions import FolderActions


@then('I should see "Folder" Actions menu bar')
def test_folder_actions(context):
    folder = FolderActions(context)
    title = folder.get_title()
    assert_that(title, equal_to("Folder Actions"))


@then('I see the following items in Folder Actions list')
def test_items(context):
    folder = FolderActions(context)
    items = []
    for i in folder.get_items():
        items.append(i.text)
    for r in context.table:
        assert_that(r['item'], is_in(items))


@then('I click on "{name}" in the Folder Actions selection')
def click_on_item(context, name):
    folder = FolderActions(context)
    folder.click_item(name)


@then('I see a new dialog box being opened')
def check_modal(context):
    folder = FolderActions(context)
    folder.switch_out_iframe()
    folder.is_modal_present()


@then('I type "{text}" in the textbox')
def send_text(context, text):
    folder = FolderActions(context)
    folder.send_text(text)


@then('I click on OK button')
def click_button(context):
    folder = FolderActions(context)
    folder.click_OK()


@then('I click on Yes, Rename button')
def click_yes(context):
    folder = FolderActions(context)
    folder.click_yes()


@then('I should see "{title}" in dialog box')
def check_modal_title(context, title):
    folder = FolderActions(context)
    folder.is_modal_title_present(title)


@then('I type "{text}" in the rename textbox')
def send_text_to_rename_textbox(context, text):
    folder = FolderActions(context)
    folder.send_text_to_rename(text)


@then('I click on OK button in the rename modal')
def click_yes_in_rename(context):
    folder = FolderActions(context)
    folder.click_yes_to_rename()


@then('I click on Yes, Move to Trash')
def click_move_to_trash(context):
    folder = FolderActions(context)
    folder.click_OK()
