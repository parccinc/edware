__author__ = 'vnatarajan'

from behave import given, when, then
from sqlalchemy import DDL
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from reporting_tests.features.frontend.steps import common
from components_tests.steps import starmigrate_steps
from components_tests.frontend.support import wait_for_condition, WebDriverHelper
from pentaho.features.frontend.steps import fixture_queries
from time import sleep
import os
import subprocess
from hamcrest import assert_that, contains_string

state_ids = {'first': 'NY',
             'cat': 'NY',
             'second': 'RI',
             'dog': 'RI'}

table_ids = {'predefined': 36,
             'custom': 13}

table_data = {'header': 'thead',
              'body': 'tbody'}


@given('we add the known analytics data to the analytics schema from "{filename}" dumpfile')
def step_impl(context, filename):
    here = os.path.dirname(__file__)
    dump_file = os.path.abspath(os.path.join(here, '..', '..', '..', '..', 'components_tests', 'data', 'fixture', 'Analytics_SDS_Data_Files', filename))
    command = "PGPASSWORD={2} psql -h {0} -U {1} -d {3} < {4}".format(context.analytics_state_master,
                                                                      context.analytics_db_user,
                                                                      context.analytics_db_password,
                                                                      context.analytics_db,
                                                                      dump_file)
    dump = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (done, fail) = dump.communicate()
    assert fail.decode() == "", "Error : {0}".format(fail.decode())
    assert dump.returncode == 0, "There were some errors while running the sql command. The process exitted with code: {0}".format(dump.returncode)


@given('that we have known archive with csv.gz dumps for the "{tenant_name}" tenant')
def step_impl(context, tenant_name):
    context.dump_tenant = tenant_name
    here = os.path.dirname(__file__)
    dump_file = os.path.abspath(os.path.join(here, '..', '..', '..', '..', 'components_tests',
                                             'data', 'fixture', 'Analytics_SDS_Data_Files',
                                             '{}.gz'.format(tenant_name)))
    if not os.path.isfile(dump_file):
        raise AssertionError("Dump file {} not exist:".format(dump_file))

    context.star_dump_file = dump_file


@when('I browse to the Pentaho URL')
def step_impl(context):
    context.driver.get(context.pentaho_url)
    try:
        alert = context.driver.switch_to.alert
        alert.accept()
    except NoAlertPresentException:
        pass


@then('I should see the "{var_page}" page open')
def wait_while_loading(context, var_page):
    """
    Instead of using the flaky, built-in waits from Selenium, roll our own. Check if the body element has the
    'body-loader' class and perform an exponential back-off if it does. We check the body's classes since it is much
    more efficient than having Selenium search the DOM for the loading mask.
    """
    # Wait for the loader to show up. It will be there for a while, don't worry ;)
    WebDriverWait(context.driver, 60).until(expected_conditions.element_to_be_clickable((By.ID, "pucHelp")))

    if var_page == "Analytics":
        context.driver.switch_to_window(context.driver.window_handles[-1])
        assert context.driver.title == "PARCC Analytics", "{0} : expected, {1} : Actual" \
            .format("PARCC Analytics", context.driver.title)
    if var_page == 'landing' or var_page == 'report':
        assert context.driver.title == "PARCC Analytics", "{0} : expected, {1} : Actual" \
            .format("PARCC Analytics", context.driver.title)
    if var_page == 'parcc':
        assert context.driver.title == "PARCC Comparing Populations Report", "{0} : expected, {1} : Actual" \
            .format("PARCC Comparing Populations Report", context.driver.title)


@then('I should see there is a left nav bar on the page')
def step_impl(context):
    context.driver.switch_to.frame("frame_0")
    try:
        WebDriverWait(context.driver, 20).until(
            expected_conditions.visibility_of_element_located((By.CLASS_NAME, "sidebar")))
    except NoSuchElementException:
        raise AssertionError("Nav bar not present")


@when('I click on "{field}" from the Workspace Fields')
def step_impl(context, field):
    element_path = "//div[@class='workspace_fields']//a[contains(text(),'{0}')]".format(field)
    context.driver.find_element_by_xpath(element_path).click()


@when('I select "{choice}" from the "{dropdown}" dropdown and click "{button}"')
def step_impl(context, choice, dropdown, button):
    WebDriverWait(context.driver, 20).until(expected_conditions.
                                            visibility_of_element_located((By.CLASS_NAME, "ui-dialog")))
    element_path = "//label[contains(text(), '{0}')]/following-sibling::select".format(dropdown)
    select = Select(context.driver.find_element_by_xpath(element_path))
    select.select_by_visible_text(choice)
    context.driver.find_element_by_link_text(button).click()


@when('I click the clear axis button on the "{area_name}" area')
def step_impl(context, area_name):
    if area_name == "MEASURES":
        area_name = "DETAILS"
    element_path = "//div[@title='{0}']//span[@title='Clear Axis']".format(area_name)
    context.driver.find_element_by_xpath(element_path).click()


@when('I click on the "{var}" button in analytics')
def step_impl(context, var):
    if var == "New Report":
        WebDriverWait(context.driver, 20).until(
            expected_conditions.invisibility_of_element_located((By.CLASS_NAME, "glasspane")))
        context.driver.switch_to.frame("home.perspective")
        context.driver.find_element_by_css_selector(
            "#buttonWrapper > div.row-fluid > div.well.sidebar > button:nth-child(2)").click()
        context.driver.switch_to.default_content()
        WebDriverWait(context.driver, 20).until(expected_conditions.
                                                visibility_of_element_located((By.CLASS_NAME, "pentaho-tab-bar")))
    else:
        context.driver.find_element_by_link_text(var).click()


@when('I click on the "{button_name}" button in the toolbar')
def step_impl(context, button_name):
    if button_name == 'Swap axis':
        context.driver.find_element_by_class_name("swap_axis").click()
        sleep(2)
    else:
        selector = "div.workspace_toolbar li a[original_title='{0}']".format(button_name)
        context.driver.find_element_by_css_selector(selector).click()


@when('I click on "{var}" in the left nav')
def step_impl(context, var):
    context.driver.find_element_by_link_text(var).click()


@then('I should see the following labels')
def step_impl(context):
    elements = context.driver.find_element_by_class_name("sidebar").find_elements_by_tag_name("h3")
    actual_labels = [element.text for element in elements]
    for row in context.table:
        assert row['labels'] in actual_labels, "{0} found in {1} labels".format(row['labels'], actual_labels)


@then('I should see a dropdown box under "Cubes"')
def step_impl(context):
    elements = context.driver.find_element_by_class_name("sidebar")
    if not elements.find_element_by_class_name("cubes"):
        raise AssertionError("drop down not present under Cubes")


@when('I drag the following fields to respective categories')
def step_impl(context):
    for row in context.table:
        source_element = context.driver.find_element_by_link_text(row['field'])
        destination_class_name = 'rows_fields'
        if row['category'] == "Filters":
            destination_class_name = 'filter_fields'
        if row['category'] == "Measures":
            destination_class_name = 'details_fields'
        if row['category'] == "Columns":
            destination_class_name = 'columns_fields'

        destination_element = context.driver.find_element_by_class_name(destination_class_name)
        ActionChains(context.driver).drag_and_drop(source_element, destination_element).perform()


@when('I filter on the following options')
def step_impl(context):
    for row in context.table:
        context.driver.find_element_by_link_text(row['field']).click()
        checkbox_id = 'selections_checker' + row['option']
        option_element = context.driver.find_element_by_id(checkbox_id)
        label = option_element.get_attribute('label')
        assert label == row['label'], "{0}: expected {1}: Actual".format(row['label'], label)
        option_element.click()
        context.driver.find_element_by_id('add_members').click()
        context.driver.find_element_by_link_text(' OK ').click()


@when('I open the filters popup for "{filter_name}"')
def open_filter(context, filter_name):
    context.driver.find_element_by_link_text(filter_name).click()
    WebDriverWait(context.driver, 20).until(expected_conditions.
                                            visibility_of_element_located((By.CLASS_NAME, "caption")))
    context.filter_fields = []


@when('I close the filter popup')
def close_filter(context):
    context.driver.find_element_by_class_name('ui-dialog-titlebar-close').click()


@then('I {state} see the following fields in the filter popup')
def check_filter(context, state):
    assert state in ['should', 'should not', "shouldn't"], 'Invalid state.'
    included = state == 'should'
    if not context.filter_fields:
        context.filter_fields = [elem.text for elem in context.driver.find_elements_by_class_name('caption')]
    for row in context.table:
        assert (row['field'] in context.filter_fields) == included, \
            "{0} {1} be in the filter fields.".format(row['field'], state)


@then('I should see "{cube_name}"')
def step_impl(context, cube_name):
    dropdown = context.driver.find_element_by_class_name("cubes")
    label = dropdown.find_element_by_tag_name("optgroup").get_attribute("label")
    assert label == "PARCC  (PARCC)", "{0}: expected {1}: Actual".format("PARCC  (PARCC)", label)
    option = dropdown.find_elements_by_tag_name("option")[1].text
    assert option == cube_name, "{0}: expected {1}: Actual".format(cube_name, option)


@when('I un-click auto run')
def step_impl(context):
    autoplay = context.driver.find_element_by_class_name("auto")
    if 'on' in autoplay.get_attribute('class').split(' '):
        autoplay.click()


@when('I click play to run the report')
def step_impl(context):
    context.driver.find_element_by_class_name("run").click()


@when('I select "{cube_name}"')
def step_impl(context, cube_name):
    WebDriverWait(context.driver, 20).until(expected_conditions.
                                            visibility_of_element_located((By.CLASS_NAME, "sidebar_inner")))
    select = Select(context.driver.find_element_by_class_name("cubes"))
    select.select_by_visible_text(cube_name)


@then('I should see "{box}" open')
def step_impl(context, box):
    if box == "Measures box":
        try:
            WebDriverWait(context.driver, 20).until(
                lambda driver: driver.find_element_by_class_name("metadata_attribute_wrapper").value_of_css_property('opacity') == '1', message="Measures box should open")
        except:
            raise AssertionError("Timeout in opening Measures box")
    if box == "Dimension box":
        assert context.driver.find_element_by_css_selector(
            ".sidebar_inner.dimension_tree"), "Dimension box list not present"


@then('I should see the following "{var_value_1}" populated in "{var_value_2}"')
def step_impl(context, var_value_1, var_value_2):
    if var_value_1 == "measures" and var_value_2 == "Measures box":
        elements = context.driver \
            .find_elements_by_css_selector("#tab_panel div.sidebar_inner.measure_tree > ul > li > ul > li a")
    elif var_value_1 == "groups of Dimension" and var_value_2 == "Dimensions box":
        elements = context.driver.find_elements_by_css_selector("#tab_panel > div > div.sidebar.ui-droppable > div.metadata_attribute_wrapper > div > div.sidebar_inner.dimension_tree > ul > li > a")
    for row in context.table:
        assert row['value'] in [element.text for element in elements], "The {0} is not present in the {1}".format(
            row['value'], var_value_2)


@then('I should see the following boxes on the right panel of the page')
def step_impl(context):
    elements = context.driver.find_elements_by_css_selector("#tab_panel .workspace_fields div .i18n")
    for row in context.table:
        assert row['value'] in [element.text for element in elements], "The {0} is not present ".format(row['value'])


@when('I select the following values within the category on the left-hand sidebar')
def step_impl(context):
    for row in context.table:
        if row['category'] == "measures":
            context.driver.find_element_by_link_text(row['values']).click()
        if row['category'] == "dimensions":
            context.driver.find_element_by_link_text(row['values']).click()


def _wait_for_table_to_load(context):
    WebDriverWait(context.driver, 20).until(expected_conditions.
                                            visibility_of_element_located((By.CLASS_NAME, "workspace_results_info")))


def _get_table_row(context, table_type, body_or_header, row_num):
    table_id = table_ids[table_type]
    table_content = table_data[body_or_header]
    selector_mask = "#table_{table_id} table {content} tr:nth-child({row})"
    selector = selector_mask.format(table_id=table_id, content=table_content, row=row_num)
    return context.driver.find_element_by_css_selector(selector)


@then('I should see the following values in row "{row_num}" of the table')
def step_impl(context, row_num):
    # TODO This method is obsolete. Should be removed after code will be checked on CI
    _wait_for_table_to_load(context)

    table_row = context.driver.find_element_by_css_selector("#table_13 table tbody tr:nth-child({0})".format(int(row_num)))
    for row in context.table:
        text_value = table_row.find_elements_by_tag_name(row['tag'])[int(row['column_num']) - 1].text.strip()
        assert row['value'] == text_value, "{0} : expected, {1} : Actual, {2} : Row".format(row['value'], text_value, table_row.text)


@then('I should see these values in row "{row_num}" of the table "{body_or_header}" in "{table_type}" report')
def check_rows(context, row_num, body_or_header, table_type):
    _wait_for_table_to_load(context)
    text_values = _get_table_row(context, table_type, body_or_header, row_num).text.splitlines()
    text_values = [val.strip() for val in text_values]
    expected_row = [row['value'] for row in context.table]
    assert expected_row == text_values, 'expected: {0}, found: {1}'.format(expected_row, text_values)


@then('I should see these values in table "{body_or_header}" of the "{table_type}" report table')
def check_data_in_table(context, body_or_header, table_type):
    _wait_for_table_to_load(context)
    for row in context.table:
        table_row = _get_table_row(context, table_type, body_or_header, row['line_num'])
        actual_text = table_row.text.replace('\n', ' ')
        assert row['value'] in actual_text, "Row {0}, Expected : '{1}', Actual : '{2}'".format(row['line_num'], row['value'], actual_text)


@then('I should see the same results from test case "{test_case}" query for "{tenant}" tenant in "{expected_columns:d}" columns with the following parameters')
def step_impl(context, test_case, tenant, expected_columns):

    values = {}
    if context.table is not None:
        values = {x['field']: x['value'] for x in context.table}

    table_rows = context.driver.find_elements_by_css_selector("#table_13 table tbody tr")

    with starmigrate_steps.get_analytics_db_conn(context, tenant) as conn:
        querystring = get_query_for_test_case(test_case)
        query = querystring.format(**values)
        result = conn.execute(DDL(query))
        count = 0
        previous = {}
        for row in result:
            if test_case == "8.1":
                #due to interface weirdness in TC8.1, we have this slower version
                ui_row = list(map(lambda x: x.text.strip(), table_rows[count].find_elements_by_tag_name("div") + table_rows[count].find_elements_by_css_selector("td.data.total")))
            else:
                ui_row = list(map(lambda x: x.text.strip(), table_rows[count].find_elements_by_tag_name("div")))

            row = list(map(lambda x: "" if x is None else str(x).strip(), row))
            for i in range(0, expected_columns):
                assert row[i] == ui_row[i] or (row[i] == previous.get(i) and ui_row[i] == "") or (row[i] == '' and ui_row[i] == "-"), \
                    "Row {0}, Expected : '{1}', Actual : '{2}', Previous : '{3}'".format(count + 1, row[i], ui_row[i], previous.get(i))
                if row[i] == ui_row[i]:
                    previous[i] = ui_row[i]
            count += 1


@then('I should see the following "{report_name}" report name in url')
def step_impl(context, report_name):
    assert_that(context.driver.current_url, (contains_string(report_name)))


def get_query_for_test_case(test_case):

    test_case_queries = {"1.1": fixture_queries.QUERY_1_1,    # Query 1.1 Fixed
                         "2.1": fixture_queries.QUERY_2_1,    # Query 2.1 Fixed
                         "2.2": fixture_queries.QUERY_2_2,    # Query 2.2 Fixed
                         "3.1": fixture_queries.QUERY_3_1,    # Query 3.1 Fixed
                         "3.2": fixture_queries.QUERY_3_2,    # Query 3.2 Fixed
                         "4.1": fixture_queries.QUERY_4_1,    # Query 4.1 Fixed
                         "4.2": fixture_queries.QUERY_4_2,    # Query 4.2 Fixed
                         "4.3": fixture_queries.QUERY_4_3,    # Query 4.3 Fixed
                         "4.4": fixture_queries.QUERY_4_4,    # Query 4.4 Fixed
                         "5.1": fixture_queries.QUERY_5_1,    # Query 5.1 Fixed
                         "5.2": fixture_queries.QUERY_5_2,    # Query 5.2 Fixed
                         "5.3": fixture_queries.QUERY_5_3,    # Query 5.3 Fixed
                         "6.1": fixture_queries.QUERY_6_1,    # Query 6.1 Fixed
                         "7.1": fixture_queries.QUERY_7_1,    # Query 7.1 Fixed
                         "7.2": fixture_queries.QUERY_7_2,    # Query 7.1 Fixed
                         "8.1": fixture_queries.QUERY_8_1,    # Query 8.1 Fixed
                         "8.2": fixture_queries.QUERY_8_2,    # Query 8.2 Fixed
                         "9.1": fixture_queries.QUERY_9_1,    # Query 9.1 Fixed
                         "10.1": fixture_queries.QUERY_10_1,  # Query 10.1 Fixed
                         "10.2": """
                                    WITH mc
                                         AS (SELECT student_name,
                                                    school_name,
                                                    Count(DISTINCT CASE
                                                                     WHEN me_flag = 'Math' THEN 1
                                                                     ELSE NULL
                                                                   END) AS math_count
                                             FROM   analytics.fact_sum fs
                                                    join analytics.dim_student ds
                                                      ON ds.student_key = fs.student_key
                                                    join analytics.dim_accomod da
                                                      ON da.rec_key = fs.rec_key
                                                    join analytics.dim_institution di
                                                      ON di.school_key = fs.resp_school_key
                                                    join analytics.dim_test dt
                                                      ON dt.test_form_key = fs.pba_test_key
                                                    join analytics.dim_poy dp
                                                      ON dp.poy_key = fs.poy_key
                                                         AND dp.school_year = '{school_year}'
                                             WHERE  fs.record_type = '{record_type}'
                                                    AND fs.me_flag = 'Math'
                                             GROUP  BY dp.school_year,
                                                       record_type,
                                                       pba_category,
                                                       me_flag,
                                                       student_name,
                                                       school_name
                                             ORDER  BY pba_category,
                                                       student_name,
                                                       school_name),
                                         ec
                                         AS (SELECT student_name,
                                                    school_name,
                                                    Count(DISTINCT CASE
                                                                     WHEN me_flag = 'English' THEN 1
                                                                     ELSE NULL
                                                                   END) AS eng_count
                                             FROM   analytics.fact_sum fs
                                                    join analytics.dim_student ds
                                                      ON ds.student_key = fs.student_key
                                                    join analytics.dim_accomod da
                                                      ON da.rec_key = fs.rec_key
                                                    join analytics.dim_institution di
                                                      ON di.school_key = fs.resp_school_key
                                                    join analytics.dim_test dt
                                                      ON dt.test_form_key = fs.pba_test_key
                                                    join analytics.dim_poy dp
                                                      ON dp.poy_key = fs.poy_key
                                                         AND dp.school_year = '{school_year}'
                                             WHERE  fs.record_type = '{record_type}'
                                                    AND fs.me_flag = 'English'
                                             GROUP  BY dp.school_year,
                                                       record_type,
                                                       pba_category,
                                                       me_flag,
                                                       student_name,
                                                       school_name
                                             ORDER  BY pba_category,
                                                       student_name,
                                                       school_name)
                                    SELECT DISTINCT pba_category,
                                                    ds.student_name,
                                                    di.school_name,
                                                    ec.eng_count  AS "Distinct Student Count (English)",
                                                    mc.math_count AS "Distinct Student Count (Math)"
                                    FROM   analytics.fact_sum fs
                                           join analytics.dim_student ds
                                             ON ds.student_key = fs.student_key
                                           join analytics.dim_accomod da
                                             ON da.rec_key = fs.rec_key
                                           join analytics.dim_institution di
                                             ON di.school_key = fs.resp_school_key
                                           join analytics.dim_test dt
                                             ON dt.test_form_key = fs.pba_test_key
                                           join analytics.dim_poy dp
                                             ON dp.poy_key = fs.poy_key
                                                AND dp.school_year = '{school_year}'
                                           join mc
                                             ON mc.school_name = di.school_name
                                                AND mc.student_name = ds.student_name
                                           join ec
                                             ON ec.school_name = mc.school_name
                                                AND ec.student_name = mc.student_name
                                    WHERE  fs.record_type = '{record_type}'
                                    GROUP  BY dp.school_year,
                                              record_type,
                                              pba_category,
                                              me_flag,
                                              ds.student_name,
                                              di.school_name,
                                              mc.student_name,
                                              mc.math_count,
                                              ec.eng_count
                                    ORDER  BY pba_category,
                                              ds.student_name,
                                              di.school_name
                               """
                         }
    query = test_case_queries.get(test_case).split('\n')
    return ' '.join(line.lstrip().rstrip() for line in query if line)


@then('I look at the "{frame_name}" content')
def go_to_iframe(context, frame_name):
    helper = WebDriverHelper(context)
    if frame_name == 'welcome':
        helper.switch_out_iframe()
        helper.navigate_to_iframe(helper.by_id('home.perspective'))
        helper.navigate_to_iframe(helper.by_class('welcome-frame'))
    elif frame_name == 'home':
        helper.switch_out_iframe()
        helper.navigate_to_iframe(helper.by_id('home.perspective'))
