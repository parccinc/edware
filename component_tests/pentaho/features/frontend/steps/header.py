from behave import when, then
from hamcrest import assert_that, equal_to
from components_tests.frontend.pentaho_widgets.header import Header
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from reporting_tests.features.frontend.environment import close_alert_if_exist


@when('I click on the "{link_name}" link in the header')
def link_click(context, link_name):
    Header(context).click_link(link_name)


@when('I navigate to "{view_name}" view via the header dropdown')
def go_to_view(context, view_name):
    Header(context).select_view(view_name)


@then('I verify the user name is displayed as "{user_name}" in the header')
def check_user_name(context, user_name):
    WebDriverWait(context.driver, 20).until(expected_conditions.visibility_of_element_located((By.ID, "pucUserDropDown")))
    displayed_name = Header(context).get_student_name()
    assert_that(displayed_name, equal_to(user_name))


@then('I log out via the logout dropdown')
def logout(context):
    Header(context).log_out()
    close_alert_if_exist(context)
