from behave import when, then
from hamcrest import assert_that, equal_to, is_in

from components_tests.frontend.pentaho_widgets.file_actions import FileActions


@then('I should see "Files" Actions menu bar')
def test_file_actions(context):
    title = FileActions(context).get_title()
    assert_that(title, equal_to("File Actions"))


@then('I see the following items in File Actions list')
def test_items(context):
    items = []
    for i in FileActions(context).get_items():
        items.append(i.text)
    for r in context.table:
        assert_that(r['item'], is_in(items))


@when('I click on "{name}" in the File Actions selection')
def click_on_item(context, name):
    FileActions(context).click_item(name)


@then('I should see the following "{report_name}" report opened')
def step_impl(context, report_name):
    assert_that(report_name in FileActions(context).return_opened_report())
