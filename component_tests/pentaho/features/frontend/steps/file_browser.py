from behave import when, then
from hamcrest import assert_that, equal_to
from components_tests.frontend.pentaho_widgets.file_browser import FileBrowser


@then('I click on the "{folder_name}" folder')
def folder_click(context, folder_name):
    FileBrowser(context).click_folder(folder_name)


@then('I click on "{file_name}" file in "{browser_name}" file browser')
def file_click(context, file_name, browser_name):
    FileBrowser(context).click_file(file_name, browser_name)


@then('I click on the favorite icon for "{file_name}" file in "{browser_name}" file browser')
def click_favorite(context, file_name, browser_name):
    FileBrowser(context).click_favorite_icon(file_name, browser_name)


@then('I click on the arrow for "{folder_name}" folder')
def arrow_click(context, folder_name):
    FileBrowser(context).click_arrow(folder_name)


@then('I verify that the arrow for "{folder_name}" is "{open_or_closed}"')
def is_folder_open(context, folder_name, open_or_closed):
    is_open = FileBrowser(context).check_open(folder_name)
    if open_or_closed == "open":
        assert is_open
    else:
        assert not is_open


@then('I get the "{file_browser_name}" file browser')
def get_file_browser(context, file_browser_name):
    el = FileBrowser(context).get_browser_with_name(file_browser_name)
    print(el.text)


@then('I see the following {object_type} in the "{browser_type}" browser')
def check_items(context, object_type, browser_type):
    objects = FileBrowser(context).get_items(browser_type)
    for row in context.table:
        assert row['object_name'].lower() in objects, "The {0} is not present in the {1}".format(
            row['object_name'], objects)


@then('I click to refresh folders')
def refresh(context):
    FileBrowser(context).click_refresh()


@then('I return to main content')
def return_to_main(context):
    FileBrowser(context).switch_out_iframe()


@then('I switch to "{frame_name}" frame')
def return_to_main(context, frame_name):
    FileBrowser(context).navigate_to_iframe(frame_name)
