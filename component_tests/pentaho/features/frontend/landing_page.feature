@browser
Feature: Verify functionality of the analytics landing page

Background: Login to Pentaho
  When I browse to the Pentaho URL
  And I login as "catstate1@state.k12.cat.us" with password "innovent"
  Then I should see the "landing" page open

@RALLY_US36608
Scenario: Verify header elements and functionality
  Then I verify the user name is displayed as "catstate1@state.k12.cat.us" in the header
  When I navigate to "Browse Files" view via the header dropdown
  When I click on the "user" link in the header
  Then I log out via the logout dropdown
  Then I am redirected to the login page

Scenario: Verify landing page functionality
  Then I look at the "welcome" content
  And I should see "Folder" Actions menu bar
  And I see the following items in Folder Actions list
    |item           |
    |New Folder...  |
    |Move to Trash  |
    |Rename...      |
    |Paste          |
    |Properties...  |
  Then I see the following folders in the "folders" browser
    | object_name                    |
    | Home                           |
    | catstate1@state.k12.cat.us     |
    | Trash                          |
  Then I click on the "catstate1@state.k12.cat.us" folder
  And I click on "New Folder..." in the Folder Actions selection
  And I see a new dialog box being opened
  And I type "Testing Folder" in the textbox
  And I click on OK button
  And I look at the "welcome" content
  And I click to refresh folders
  Then I click on the "Testing Folder" folder
  And I click on "Rename..." in the Folder Actions selection
  And I see a new dialog box being opened
  And I should see "Links Will Be Lost" in dialog box
  And I click on Yes, Rename button
  And I should see "Rename" in dialog box
  And I type " rename" in the rename textbox
  And I click on OK button in the rename modal
  And I look at the "welcome" content
  And I click to refresh folders
  Then I click on the "Testing Folder rename" folder
  And I click on "Move to Trash" in the Folder Actions selection
  And I see a new dialog box being opened
  And I click on Yes, Move to Trash
  Then I return to main content

Scenario: Verify reporting link
  When I click on the "reporting" link in the header
  And I navigate to the new opened window
  And I verify that I am on the subject and grade/course initial selection page
  Then I close the new opened window
