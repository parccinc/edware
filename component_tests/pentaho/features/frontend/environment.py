import os
from unittest import TestCase
from components_tests.frontend.common import read_config, setup_browser
from reporting_tests.features.frontend.environment import close_alert_popup
from components_tests.flaky_tests_handler.flaky_tests_handler import handle_failed_feature

# USE: behave -D BEHAVE_DEBUG_ON_ERROR         (to enable  debug-on-error)
# USE: behave -D BEHAVE_DEBUG_ON_ERROR=yes     (to enable  debug-on-error)
# USE: behave -D BEHAVE_DEBUG_ON_ERROR=no      (to disable debug-on-error)

NUM_FAILURES = 0
BEHAVE_DEBUG_ON_ERROR = False


def setup_debug_on_error(userdata):
    global BEHAVE_DEBUG_ON_ERROR
    BEHAVE_DEBUG_ON_ERROR = userdata.getbool("BEHAVE_DEBUG_ON_ERROR")


def before_all(context):
    context.tc = TestCase()
    setup_debug_on_error(context.config.userdata)
    read_config(context)
    setup_browser(context)


def after_step(context, step):
    if BEHAVE_DEBUG_ON_ERROR and step.status == "failed":
        # -- ENTER DEBUGGER: Zoom in on failure location.
        # NOTE: Use IPython debugger, same for pdb (basic python debugger).
        import ipdb
        ipdb.post_mortem(step.exc_traceback)
    elif step.status == "failed":
        if hasattr(context, 'driver'):
            if hasattr(context, 'take_screenshot_on_failure') and context.take_screenshot_on_failure == 'yes':
                if not os.path.exists('screenshots'):
                    os.makedirs('screenshots')
                another_failure()
                file_mask = 'failure_{0}.png'.format(NUM_FAILURES)
                # file_mask = 'failure_{0}.png'.format(NUM_FAILURES)
                # If failure with this number exist, create new, with latest
                if os.path.isfile(os.path.join('screenshots', file_mask)):
                    files = sorted(os.listdir('screenshots'))
                    last_error_file = files[-1].split('.')[0]
                    error_index = int(last_error_file.split('_')[-1])
                    another_failure(error_index)

                context.driver.save_screenshot('screenshots/failure_{0}.png'.format(NUM_FAILURES))


@close_alert_popup
def after_scenario(context, scenario):
    context.driver.get(context.pentaho_url + '/pentaho/Logout')
    context.driver.delete_all_cookies()


def after_feature(context, feature):
    if feature.status == 'failed':
        rerun_mode = context.config.userdata.getbool('rerun_mode')
        handle_failed_feature(feature, rerun_mode)


def after_all(context):
    # Tear down web driver
    if hasattr(context, 'driver'):
        try:
            context.driver.quit()
        except Exception:
            pass


def another_failure(last_index=None):
    global NUM_FAILURES
    if last_index:
        NUM_FAILURES = last_index
    NUM_FAILURES += 1
