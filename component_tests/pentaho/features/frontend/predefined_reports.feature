@browser

Feature: Verify predefined reports data

  Background:
    When I browse to the Pentaho URL
    And I login as "dogstate1@state.k12.dog.us" with password "innovent"
    Then I should see the "landing" page open
    Then I look at the "welcome" content
    Then I see the following folders in the "folders" browser
        | object_name                    |
        | Home                           |
        | dogstate1@state.k12.dog.us     |
        | Pre-Configured Reports         |
        | Share_RI                       |
        | Trash                          |
    Then I click on the "Pre-Configured Reports" folder
    Then I see the following files in the "files" browser
        | object_name                    |
        | Participation                  |
        | Performance Level Distribution |
        | Proficiency Percentage         |

  @PARCC_150
  Scenario: Check Participation report
    Then I click on "Participation" file in "files" file browser
    Then I should see "Files" Actions menu bar
    When I click on "open" in the File Actions selection
    Then I return to main content
    And I should see the following "Participation" report opened
    Then I switch to "frame_0" frame
    And I should see these values in row "1" of the table "header" in "predefined" report
      | column_num  | value             | tag |
      | 4           | ela               | th  |
      | 5           | math              | th  |
    And I should see these values in row "2" of the table "header" in "predefined" report
      | column_num  | value                     | tag |
      | 1           | Responsible District Name | th  |
      | 2           | Responsible School Name   | th  |
      | 3           | Test Code                 | th  |
      | 4           | Student Registered Count  | th  |
      | 5           | Valid Score Count         | th  |
      | 6           | No Valid Score Count      | th  |
      | 7           | Student Registered Count  | th  |
      | 8           | Valid Score Count         | th  |
      | 9           | No Valid Score Count      | th  |
    Then I should see these values in table "body" of the "predefined" report table
      | line_num  | value                                                 |
      | 1         | East Greenwich East Greenwich High School ALG01 4 3 1 |
      | 2         | ALG02 15 14 1                                         |
      | 3         | ELA03 5 5 0                                           |
      | 4         | ELA09 5 3 2                                           |
      | 5         | ELA10 5 5 0                                           |
      | 6         | ELA11 5 5 0                                           |
      | 7         | GEO01 1 1 0                                           |
      | 8         | 20 18 2 20 18 2                                       |

      | 9         | Newport Rogers High School ALG01 14 8 6               |
      | 10        | ELA03 6 4 2                                           |
      | 11        | ELA09 5 2 3                                           |
      | 12        | ELA10 5 4 1                                           |
      | 13        | ELA11 5 4 1                                           |
      | 14        | GEO01 4 3 1                                           |
      | 15        | MAT05 1 1 0                                           |
      | 16        | MAT08 2 2 0                                           |
      | 17        | 21 14 7 21 14 7                                       |

      | 18        | Providence Alvarez High School ELA03 4 2 2            |
      | 19        | ELA09 6 5 1                                           |
      | 20        | ELA10 4 1 3                                           |
      | 21        | ELA11 6 4 2                                           |
      | 22        | GEO01 9 5 4                                           |
      | 23        | MAT1I 10 6 4                                          |
      | 24        | MAT2I 1 1 0                                           |
      | 25        | Hope High School ELA03 5 5 0                          |
      | 26        | ELA09 5 4 1                                           |
      | 27        | ELA10 5 2 3                                           |
      | 28        | ELA11 5 3 2                                           |
      | 29        | MAT03 3 3 0                                           |
      | 30        | MAT05 1 1 0                                           |
      | 31        | MAT08 4 4 0                                           |
      | 32        | MAT3I 20 14 6                                         |
      | 33        | Mount Pleasant High School ELA03 4 4 0                |
      | 34        | ELA09 6 3 3                                           |
      | 35        | ELA10 5 2 3                                           |
      | 36        | ELA11 4 4 0                                           |
      | 37        | MAT2I 9 6 3                                           |
      | 38        | MAT3I 10 7 3                                          |

      | 39        | 59 39 20 67 47 20                                     |
      | 40        | Grand Total 100 71 29 108 79 29                       |

  @PARCC_150
  Scenario: Check Performance Level Distribution report
    Then I click on "Performance Level Distribution" file in "files" file browser
    Then I should see "Files" Actions menu bar
    When I click on "open" in the File Actions selection
    Then I return to main content
    And I should see the following "Performance Level Distribution" report opened
    Then I switch to "frame_0" frame
    And I should see these values in row "1" of the table "header" in "predefined" report
      | column_num  | value             | tag |
      | 4           | ela               | th  |
      | 5           | math              | th  |
    And I should see these values in row "2" of the table "header" in "predefined" report
      | column_num  | value                     | tag |
      | 1           | Responsible District Name | th  |
      | 2           | Responsible School Name   | th  |
      | 3           | Test Code                 | th  |
      | 4           | Performance Level 1 %     | th  |
      | 5           | Performance Level 2 %     | th  |
      | 6           | Performance Level 3 %     | th  |
      | 7           | Performance Level 4 %     | th  |
      | 8           | Performance Level 5 %     | th  |
      | 9           | Performance Level 1 %     | th  |
      | 10          | Performance Level 2 %     | th  |
      | 11          | Performance Level 3 %     | th  |
      | 12          | Performance Level 4 %     | th  |
      | 13          | Performance Level 5 %     | th  |
    Then I should see these values in table "body" of the "predefined" report table
      | line_num  | value                                                                           |
      | 1         | East Greenwich East Greenwich High School ALG01 0.00% 0.00% 0.00% 100.00% 0.00% |
      | 2         | ALG02 8.33% 16.67% 8.33% 41.67% 25.00%                                          |
      | 3         | ELA03 0.00% 0.00% 100.00% 0.00% 0.00%                                           |
      | 4         | ELA09 0.00% 0.00% 0.00% 50.00% 50.00%                                           |
      | 5         | ELA10 0.00% 20.00% 80.00% 0.00% 0.00%                                           |
      | 6         | ELA11 0.00% 60.00% 20.00% 20.00% 0.00%                                          |
      | 7         | GEO01 0.00% 0.00% 100.00% 0.00% 0.00%                                           |
      | 8         | 0.00% 20.00% 50.00% 17.50% 12.50% 2.78% 5.56% 36.11% 47.22% 8.33%               |

      | 9         | Newport Rogers High School ALG01 28.57% 14.29% 14.29% 42.86% 0.00%              |
      | 10        | ELA09 0.00% 0.00% 100.00% 0.00% 0.00%                                           |
      | 11        | ELA10 0.00% 0.00% 33.33% 33.33% 33.33%                                          |
      | 12        | ELA11 0.00% 0.00% 100.00% 0.00% 0.00%                                           |
      | 13        | GEO01 0.00% 0.00% 33.33% 66.67% 0.00%                                           |
      | 14        | MAT05 0.00% 0.00% 0.00% 100.00% 0.00%                                           |
      | 15        | MAT08 0.00% 0.00% 100.00% 0.00% 0.00%                                           |
      | 16        | 0.00% 0.00% 77.78% 11.11% 11.11% 7.14% 3.57% 36.90% 52.38% 0.00%                |

      | 17        | Providence Alvarez High School ELA03 0.00% 0.00% 0.00% 100.00% 0.00%            |
      | 18        | ELA09 0.00% 0.00% 33.33% 0.00% 66.67%                                           |
      | 19        | ELA11 0.00% 0.00% 25.00% 25.00% 50.00%                                          |
      | 20        | GEO01 0.00% 33.33% 66.67% 0.00% 0.00%                                           |
      | 21        | MAT1I 0.00% 0.00% 16.67% 50.00% 33.33%                                          |
      | 22        | MAT2I 0.00% 0.00% 100.00% 0.00% 0.00%                                           |
      | 23        | Hope High School ELA03 0.00% 40.00% 20.00% 40.00% 0.00%                         |
      | 24        | ELA09 0.00% 66.67% 33.33% 0.00% 0.00%                                           |
      | 25        | ELA10 0.00% 50.00% 50.00% 0.00% 0.00%                                           |
      | 26        | ELA11 0.00% 0.00% 33.33% 33.33% 33.33%                                          |
      | 27        | MAT03 0.00% 100.00% 0.00% 0.00% 0.00%                                           |
      | 28        | MAT05 0.00% 100.00% 0.00% 0.00% 0.00%                                           |
      | 29        | MAT08 0.00% 100.00% 0.00% 0.00% 0.00%                                           |
      | 30        | MAT3I 23.08% 23.08% 30.77% 0.00% 23.08%                                         |
      | 31        | Mount Pleasant High School ELA03 50.00% 25.00% 25.00% 0.00% 0.00%               |
      | 32        | ELA09 100.00% 0.00% 0.00% 0.00% 0.00%                                           |
      | 33        | ELA10 0.00% 100.00% 0.00% 0.00% 0.00%                                           |
      | 34        | ELA11 66.67% 0.00% 0.00% 33.33% 0.00%                                           |
      | 35        | MAT2I 0.00% 0.00% 75.00% 0.00% 25.00%                                           |
      | 36        | MAT3I 28.57% 42.86% 14.29% 0.00% 14.29%                                         |
      | 37        | 19.70% 25.61% 20.00% 21.06% 13.64% 5.74% 44.36% 33.71% 5.56% 10.63%             |
      | 38        | Grand Total 12.04% 20.09% 36.30% 18.61% 12.96% 5.53% 26.89% 34.96% 25.07% 7.54% |

  @PARCC_150
  Scenario: Check Proficiency Percentage report
    Then I click on "Proficiency Percentage" file in "files" file browser
    Then I should see "Files" Actions menu bar
    When I click on "open" in the File Actions selection
    Then I return to main content
    And I should see the following "Proficiency Percentage" report opened
    Then I switch to "frame_0" frame
    And I should see these values in row "1" of the table "header" in "predefined" report
      | column_num  | value             | tag |
      | 4           | ela               | th  |
      | 5           | math              | th  |
    And I should see these values in row "2" of the table "header" in "predefined" report
      | column_num  | value                     | tag |
      | 1           | Responsible District Name | th  |
      | 2           | Responsible School Name   | th  |
      | 3           | Test Code                 | th  |
      | 4           | Proficient (>= lvl 4) %   | th  |
      | 5           | Proficient (>= lvl 4) %   | th  |

    Then I should see these values in table "body" of the "predefined" report table
      | line_num  | value                                                   |
      | 1         | East Greenwich East Greenwich High School ALG01 100.00% |
      | 2         | ALG02 66.67%                                            |
      | 3         | ELA03 0.00%                                             |
      | 4         | ELA09 100.00%                                           |
      | 5         | ELA10 0.00%                                             |
      | 6         | ELA11 20.00%                                            |
      | 7         | GEO01 0.00%                                             |
      | 8         | 30.00% 55.56%                                           |

      | 9         | Newport Rogers High School ALG01 42.86%                 |
      | 10        | ELA09 0.00%                                             |
      | 11        | ELA10 66.67%                                            |
      | 12        | ELA11 0.00%                                             |
      | 13        | GEO01 66.67%                                            |
      | 14        | MAT05 100.00%                                           |
      | 15        | MAT08 0.00%                                             |
      | 16        | 22.22% 52.38%                                           |

      | 17        | Providence Alvarez High School ELA03 100.00%            |
      | 18        | ELA09 66.67%                                            |
      | 19        | ELA11 75.00%                                            |
      | 20        | GEO01 0.00%                                             |
      | 21        | MAT1I 83.33%                                            |
      | 22        | MAT2I 0.00%                                             |
      | 23        | Hope High School ELA03 40.00%                           |
      | 24        | ELA09 0.00%                                             |
      | 25        | ELA10 0.00%                                             |
      | 26        | ELA11 66.67%                                            |
      | 27        | MAT03 0.00%                                             |
      | 28        | MAT05 0.00%                                             |
      | 29        | MAT08 0.00%                                             |
      | 30        | MAT3I 23.08%                                            |
      | 31        | Mount Pleasant High School ELA03 0.00%                  |
      | 32        | ELA09 0.00%                                             |
      | 33        | ELA10 0.00%                                             |
      | 34        | ELA11 33.33%                                            |
      | 35        | MAT2I 25.00%                                            |
      | 36        | MAT3I 14.29%                                            |
      | 37        | 34.70% 16.19%                                           |
      | 38        | Grand Total 31.57% 32.62%                               |

  @PARCC_150
  Scenario: Check Average Scale Score report
    Then I click on "Average Scale Score" file in "files" file browser
    Then I should see "Files" Actions menu bar
    When I click on "open" in the File Actions selection
    Then I return to main content
    And I should see the following "Average Scale Score" report opened
    Then I switch to "frame_0" frame
    And I should see these values in row "1" of the table "header" in "predefined" report
      | column_num  | value             | tag |
      | 4           | ela               | th  |
      | 5           | math              | th  |
    And I should see these values in row "2" of the table "header" in "predefined" report
      | column_num  | value                     | tag |
      | 1           | Responsible District Name | th  |
      | 2           | Responsible School Name   | th  |
      | 3           | Test Code                 | th  |
      | 4           | Scale Score Mean          | th  |
      | 5           | Scale Score Mean          | th  |

    Then I should see these values in table "body" of the "predefined" report table
      | line_num  | value                                                  |
      | 1         | East Greenwich East Greenwich High School ALG01 398.00 |
      | 2         | ALG02 294.83                                           |
      | 3         | ELA03 285.00                                           |
      | 4         | ELA09 390.00                                           |
      | 5         | ELA10 205.00                                           |
      | 6         | ELA11 237.40                                           |
      | 7         | GEO01 202.00                                           |
      | 8         | 252.08 295.57                                          |

      | 9         | Newport Rogers High School ALG01 231.86                |
      | 10        | ELA09 233.00                                           |
      | 11        | ELA10 334.00                                           |
      | 12        | ELA11 251.75                                           |
      | 13        | GEO01 289.33                                           |
      | 14        | MAT05 335.00                                           |
      | 15        | MAT08 270.00                                           |
      | 16        | 275.00 258.00                                          |

      | 17        | Providence Alvarez High School ELA03 319.00            |
      | 18        | ELA09 376.00                                           |
      | 19        | ELA11 362.50                                           |
      | 20        | GEO01 211.00                                           |
      | 21        | MAT1I 367.67                                           |
      | 22        | MAT2I 230.00                                           |
      | 23        | Hope High School ELA03 255.80                          |
      | 24        | ELA09 196.33                                           |
      | 25        | ELA10 205.00                                           |
      | 26        | ELA11 345.67                                           |
      | 27        | MAT03 145.67                                           |
      | 28        | MAT05 110.00                                           |
      | 29        | MAT08 169.25                                           |
      | 30        | MAT3I 210.00                                           |
      | 31        | Mount Pleasant High School ELA03 125.00                |
      | 32        | ELA09 66.00                                            |
      | 33        | ELA10 167.50                                           |
      | 34        | ELA11 128.00                                           |
      | 35        | MAT2I 308.00                                           |
      | 36        | MAT3I 181.71                                           |
      | 37        | 236.34 226.83                                          |
      | 38        | Grand Total 246.57 246.49                              |

  @PARCC_150
  Scenario: Verify predefined report open in new window
    Then I click on "Participation" file in "files" file browser
    Then I should see "Files" Actions menu bar
    Then I see the following items in File Actions list
        |item                 |
        |Open                 |
        |Open in a new window |
        |Edit                 |
        |Cut                  |
        |Copy                 |
        |Move to Trash        |
        |Rename...            |
        |Add to Favorites     |
        |Properties...        |
    When I click on "open in a new window" in the File Actions selection
    And I navigate to the new opened window
    Then I should see the following "Participation" report name in url
    Then I close the new opened window
    Then I log out
