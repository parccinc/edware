@browser
Feature: Kibana Tests
  Background:
    Given Empty Indices

  Scenario: Open Browser and test basic Kibana functionality

    When I open the Kibana
    Then I should be redirected to "Settings" page
    And I verify that I am on "Settings" page
    And I verify that I am on indices tab
    And I see "Configure an index pattern" header

    Then I click "Index name or pattern" field
    And I erase default value in field "Index name or pattern"
    Then I enter * into field "Index name or pattern"
    And I wait when appear "Time-field name" dropdown

    Then I select "@timestamp" from the "Time-field name" dropdown
    And I click "Create" button
    Then I should see table with the following fields in the 1 column
      | fields          |
      | _index          |
      | pid             |
      | program         |
      | @version        |
      | host            |
      | timestamp       |
      | severity        |
      | _type           |
      | message         |
      | priority        |
      | logsource       |
      | HAL_CODE        |
      | tags            |
      | @timestamp      |
      | _source         |
      | _id             |
      | facility        |
      | severity_label  |
      | facility_label  |
    Then I click "Discover" page
    And I verify that I am on "Discover" page
    Then I click "timepicker" button
    And I click "Last 7 days" value
    Then I should see timestamp chart

    When I add fields
      | value     |
      | host      |
      | program   |
      | message   |
    Then I should see these columns in result table
      | value     |
      | Time      |
      | host      |
      | program   |
      | message   |
