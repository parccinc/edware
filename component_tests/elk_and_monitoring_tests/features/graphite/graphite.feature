@browser
Feature: Graphite Tests

  Scenario: Open Browser and test Graphite header buttons

    When I open the Graphite
    Then I should see header with buttons
      | button        |
      | Login         |
      | Documentation |
      | Dashboard     |
      | flot          |
      | events        |

    When I click Login button
    Then I should see login page
    And I return to the main page

    When I click Dashboard button
    Then I should see dashboard page
    And I return to the main page

    When I click flot button
    Then I should see graphlot page
    And I return to the main page

    When I click events button
    Then I should see events page
    And I return to the main page

  Scenario: Open Browser and test Graphite body elements

    When I open the Graphite
    Then I should see left column with tabs
      | tab             |
      | Tree            |
      | Search          |
      | Auto-Completer  |

    And tab Tree should be active
    And I should see folders in the Tree tab
      | folder      |
      | Graphite    |
      | User Graphs |

    Then I expand Graphite folder and see more folders with available metrics
    And I check that folder with "carbon" metrics exist

    Then I open metrics and select memory usage for reporting-app-server
    And I should see memory usage in Graphite Composer window
