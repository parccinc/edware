@browser
Feature: Uchiwa Tests

  Scenario: Open Browser and test basic Uchiwa functionality

    When I open the Uchiwa
    Then I check that I am on events tab
    Then I click tab and verify that I am on required tab
      | title       |
      | clients     |
      | events      |
      | checks      |
      | stashes     |
      | aggregates  |
      | datacenters |

    When I click "events" tab
    Then I should see < 10 elements in the table

    When I click "clients" tab
    Then I select "Show all items" in limit dropdown
    And I should see > 50 elements in the table

    When I click "checks" tab
    Then I select "Show all items" in limit dropdown
    And I should see > 50 elements in the table
