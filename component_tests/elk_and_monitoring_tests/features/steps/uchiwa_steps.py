import re
from behave import when, then
from components_tests.frontend.support import WebDriverHelper
from components_tests.frontend.common import go_to_url


@when("I open the Uchiwa")
def enter_url(context):
    _go_to_url(context)


@then('I check that I am on {sometab} tab')
def verify_tab(context, sometab):
    helper = WebDriverHelper(context)
    _verify_tab(helper, sometab)


@then('I click tab and verify that I am on required tab')
def step_impl(context):
    helper = WebDriverHelper(context)
    for element in context.table:
        value = element['title']
        _click_tab(helper, value)
        _verify_tab(helper, value)


@when('I click "{element}" tab')
def click_element(context, element):
    helper = WebDriverHelper(context)
    _click_tab(helper, element)


@then('I should see {how_much} elements in the table')
def step_impl(context, how_much):
    helper = WebDriverHelper(context)
    table = helper.by_xpath("//div[@class='panel-body']/table/tbody")
    actual = len(table.find_elements_by_css_selector('tr.ng-scope'))
    required = int(re.search(r'\d+', how_much).group())
    if '>' in how_much:
        assert actual > required, "There should be {} elements".format(how_much)
    elif '<' in how_much:
        assert actual < required, "There should be {} elements".format(how_much)
    else:
        assert actual == required, "There should be {} elements".format(how_much)


@then('I select "{value}" in limit dropdown')
def step_impl(context, value):
    helper = WebDriverHelper(context)
    selector = "panel-limit.ng-isolate-scope"
    dropdown = helper.by_css(selector)
    dropdown.click()
    helper.wait_for_text(selector, value)
    dropdown.find_element_by_link_text(value).click()


def _click_tab(helper, tab):
    selector = "//*[@id='{}']".format(tab)
    helper.by_xpath(selector).click()
    helper.wait_for_css("div.panel.panel-default>div.panel-body")
    helper.wait_for_text("p.ng-binding", tab.upper())


def _verify_tab(helper, tab):
    helper.wait_for_css(".sidebar.ng-scope")
    selector = ".sidebar.ng-scope>div.container>div>ul.nav.sidebar-nav>li.show-on-hover.selected"
    active_tab = helper.by_css(selector)
    expected = tab.lower()
    actual = active_tab.get_attribute("id").lower()
    assert actual == expected, '"{}" tab expected. Actual active tab "{}".'.format(expected, actual)


def _go_to_url(context):
    url = context.uchiwa_url
    try:
        go_to_url(context, url)
    except:
        raise AssertionError("Can't open page: {}".format(url))
