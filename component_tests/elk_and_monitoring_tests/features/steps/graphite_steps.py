import re
from behave import when, then
from time import sleep
from urllib.parse import urlparse
from components_tests.frontend.support import WebDriverHelper
from components_tests.frontend.common import go_to_url


@when("I open the Graphite")
def enter_url(context):
    _go_to_url(context)


@then('I should see header with buttons')
def step_impl(context):
    helper = WebDriverHelper(context)
    _navigate_to_frame(helper, "header")
    buttons = [el.text for el in helper.by_css("div.header>div.nav>ul>li>a", True)]
    for element in context.table:
        btn = element['button']
        assert btn in buttons, "Button {} should be in the header".format(btn)
    helper.switch_out_iframe()


@when('I click {button} button')
def step_impl(context, button):
    helper = WebDriverHelper(context)
    _navigate_to_frame(helper, "header")
    selector = "//div[@class='nav']/ul/li/a[contains(.,'{}')]".format(button)
    helper.click_element_by_xpath(selector)
    sleep(2)


@then('I should see {somepage} page')
def step_impl(context, somepage):
    WebDriverHelper(context).wait_for_css("html[webdriver='true']")
    url = urlparse(context.driver.current_url, allow_fragments=False)
    url_paths = [el for el in url.path.replace("#", "").split("/") if el]
    assert somepage.lower() in url_paths, '"{expected}" page expected. Actual page "{actual}".'.format(
        expected=somepage,
        actual=context.driver.title)


@then('I return to the main page')
def step_impl(context):
    _go_to_url(context)


@then('I should see left column with tabs')
def step_impl(context):
    helper = WebDriverHelper(context)
    _navigate_to_frame(helper, "composerFrame")
    selector = "#ext-gen15>li>a.x-tab-right>em>span>span.x-tab-strip-text"
    tabs = [el.text for el in helper.by_css(selector, True)]
    for element in context.table:
        tab = element['tab']
        assert tab in tabs, "Tab {} should be in the left column".format(tab)
    helper.switch_out_iframe()


@then('I should see Graphite Composer window')
def step_impl(context):
    helper = WebDriverHelper(context)
    _navigate_to_frame(helper, "composerFrame")
    composer_window = helper.by_css("#ext-comp-1111")
    assert composer_window.is_displayed(), "Compose window missed"
    helper.switch_out_iframe()


@then('tab {sometab} should be active')
def step_impl(context, sometab):
    helper = WebDriverHelper(context)
    _navigate_to_frame(helper, "composerFrame")
    selector = "#ext-gen15>li.x-tab-strip-active"
    active_tab = helper.by_css(selector).text
    assert sometab == active_tab, "Tab {} should be active".format(sometab)
    helper.switch_out_iframe()


@then('I should see folders in the Tree tab')
def step_impl(context):
    helper = WebDriverHelper(context)
    _navigate_to_frame(helper, "composerFrame")
    selector = "div.x-tree-root-node>li.x-tree-node"
    actual_folders = [el.text for el in helper.by_css(selector, True)]
    for fold in context.table:
        assert fold['folder'] in actual_folders, "Folder {} should exist".format(fold['folder'])
    helper.switch_out_iframe()


@then('I expand {folder} folder and see more folders with available metrics')
def step_impl(context, folder):
    helper = WebDriverHelper(context)
    _navigate_to_frame(helper, "composerFrame")
    root_selector = "//div[@class='x-tree-root-node']"
    folder_selector = root_selector + "/li/div/a[contains(.,'{}')]".format(folder)
    root = helper.by_xpath(root_selector)
    root.find_element_by_xpath(folder_selector).click()
    metrics = root.find_elements_by_css_selector("li.x-tree-node>ul>li")
    assert len(metrics) > 0, "There should be data in {} section".format(folder)
    helper.switch_out_iframe()


@then('I check that folder with "carbon" metrics exist')
def step_impl(context):
    helper = WebDriverHelper(context)
    _navigate_to_frame(helper, "composerFrame")
    root_selector = "//div[@class='x-tree-root-node']"
    sys_selector = "//li/ul/li/div/a[contains(.,'carbon')]"
    try:
        root_folder = helper.by_xpath(root_selector)
        sys_folder = root_folder.find_element_by_xpath(sys_selector)
        sys_folder.click()
    except:
        raise AssertionError('Folder "carbon" should exist')
    finally:
        helper.switch_out_iframe()


@then('I open metrics and select memory usage for reporting-app-server')
def step_impl(context):
    helper = WebDriverHelper(context)
    _navigate_to_frame(helper, "composerFrame")
    inner_lvl_mask = ".//li/div/a[contains(.,'{}')]"
    root_selector = "//div[@class='x-tree-root-node']"
    root_folder = helper.by_xpath(root_selector)
    app_folder = root_folder.find_element_by_xpath(inner_lvl_mask.format('reporting-app-server'))
    app_folder.click()
    sleep(5)
    app_folder = app_folder.find_element_by_xpath('../..')
    sleep(5)
    memory_folder = _find_and_open_folder(app_folder, 'memory')
    memory_folder.find_element_by_link_text('used').click()
    sleep(5)
    helper.switch_out_iframe()


@then('I should see memory usage in Graphite Composer window')
def step_impl(context):
    helper = WebDriverHelper(context)
    _navigate_to_frame(helper, "composerFrame")
    graph_window = helper.by_id("ext-comp-1111")
    graph = graph_window.find_element_by_id("image-viewer")
    graph_params = _parse_image_element(graph)
    displayed = graph_params.get('target')
    try:
        # So many groups in case we'll update test suite with more tests in future
        mask = "(reporting\-app\-server)(\.)(.*)+(memory\.used)"
        used_pattern = re.search(mask, displayed)
        assert 'memory.used' in used_pattern.groups(), "Memory usage should be displayed"
    except:
        raise AssertionError("Graph window is empty")
    finally:
        helper.switch_out_iframe()


def _navigate_to_frame(helper, frame):
    helper.wait_for_id(frame)
    helper.navigate_to_iframe(frame)


def _parse_image_element(element):
    img_url = element.get_attribute('src')
    url_query = urlparse(img_url).query.split('&')
    splitted_query = [el.split('=') for el in url_query]
    query_dict = {el[0]: el[1] for el in splitted_query}
    return query_dict


def _find_and_open_folder(parent, target):
    inner_lvl_mask = ".//li/div/a[contains(.,'{}')]"
    childrens_list = parent.text.split("\n")[1:]

    if target not in childrens_list:
        for child in childrens_list:
            next_parent = parent.find_element_by_xpath(inner_lvl_mask.format(child))
            next_parent.click()
            sleep(2)
            next_parent = next_parent.find_element_by_xpath('../..')
            sleep(2)
            return _find_and_open_folder(next_parent, target)
    else:
        searched = parent.find_element_by_xpath(inner_lvl_mask.format(target))
        searched.click()
        sleep(2)
        searched = searched.find_element_by_xpath('../..')
        sleep(2)
        return searched


def _go_to_url(context):
    url = context.graphite_url
    try:
        go_to_url(context, url)
    except:
        raise AssertionError("Can't open page: {}".format(url))
