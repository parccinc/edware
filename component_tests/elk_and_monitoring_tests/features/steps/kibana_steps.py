from behave import given, when, then
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import Select
from components_tests.frontend.support import WebDriverHelper
from components_tests.frontend.common import go_to_url
from urllib.parse import urlparse
from time import sleep

KIBANA_TITLE_TEMPLATE = "{} - Kibana 4"
ELEMENT_TYPE_MAP = {
    "button": {
        "Create": "button.btn.btn-success",
        "timepicker": ".navbar-timepicker-timefilter-desc"
    },
    "field": {
        "Index name or pattern": "input.form-control.ng-isolate-scope",
    },
    "page": {
        "Discover": "//li[contains(.,'Discover')]"
    },
    "value": {
        "Last 7 days": "//a[contains(.,'Last 7 days')]"
    }
}

@given("Empty Indices")
def erase_indices(context):
    _go_to_url(context)
    helper = WebDriverHelper(context)
    helper.wait_for_css(".application.ng-scope")
    helper.click_element_by_xpath("//li[contains(.,'Settings')]")
    while True:
        helper.wait_for_class("sidebar-list")
        pattern = helper.by_css("li.sidebar-item.ng-scope") or None
        if pattern:
            if "No default index pattern" in  pattern.text:
                break
            helper.wait_for_class("settings-section-container")
            ActionChains(context.driver).move_to_element(pattern).click().perform()
            helper.wait_for_css("button.btn.btn-danger.ng-scope")
            helper.click_element_by_css("button.btn.btn-danger.ng-scope")
            context.driver.switch_to_alert().accept()
            context.driver.switch_to_default_content()


@when("I open the Kibana")
def enter_url(context):
    _go_to_url(context)


@then('I should be redirected to "{somepage}" page')
def check_title(context, somepage):
    WebDriverWait(context.driver, 10).until(expected_conditions.title_contains(somepage))
    expected = KIBANA_TITLE_TEMPLATE.format(somepage)
    actual = context.driver.title
    assert actual == expected, "\"{0}\" page expected. Actual page \"{1}\"".format(expected, actual)


@then('I verify that I am on "{somepage}" page')
def verify_page(context, somepage):
    url = urlparse(context.driver.current_url, allow_fragments=False)
    url_paths = [el for el in url.path.replace("#", "").split("/") if el]
    assert somepage.lower() in url_paths, '"{expected}" page expected. Actual page "{actual}".'.format(
        expected=KIBANA_TITLE_TEMPLATE.format(somepage.capitalize()),
        actual=context.driver.title)


@then('I verify that I am on {sometab} tab')
def verify_tab(context, sometab):
    helper = WebDriverHelper(context)
    active_tab = helper.by_css(".ng-scope.active>a.navbar-link")
    expected = sometab.lower()
    actual = active_tab.text.lower()
    assert actual == expected, '"{}" tab expected. Actual active tab "{}".'.format(expected, actual)


@then('I see "{message}" header')
def check_message_in_element(context, message):
    helper = WebDriverHelper(context)
    selector = ".page-header>h1"
    try:
        actual = helper.by_css(selector).text
        assert actual == message, "Unexpected message: \"{0}\", Required: {1}".format(actual, message)
    except Exception as e:
        raise AssertionError("Message wasn't found")


@then('I erase default value in field "Index name or pattern"')
def erase_default_value(context):
    helper = WebDriverHelper(context)
    selector = "input.form-control.ng-isolate-scope"
    helper.by_css(selector).clear()


@then('I enter {value} into field "Index name or pattern"')
def enter_field(context, value):
    helper = WebDriverHelper(context)
    selector = "input.form-control.ng-isolate-scope"
    helper.by_css(selector).send_keys(value)


@then('I wait when appear "Time-field name" dropdown')
def wait_for_element(context):
    helper = WebDriverHelper(context)
    selector = "div.form-group.ng-scope>label"
    element_name = "Time-field name"
    helper.wait_for_text(selector, element_name)


@then('I select "{value}" from the "Time-field name" dropdown')
def select_element(context, value):
    helper = WebDriverHelper(context)
    selector = "select.form-control.ng-scope.ng-pristine"
    helper.click_element_by_css(selector)
    select = Select(helper.by_css(selector))
    sleep(1)
    select.select_by_visible_text(value)


@then('I click "{element}" {type}')
def click_element(context, element, type):
    helper = WebDriverHelper(context)
    selector = ELEMENT_TYPE_MAP.get(type).get(element)
    if type in ("page", "value"):
        helper.click_element_by_xpath(selector)
    else:
        helper.click_element_by_css(selector)


@then('I should see table with the following fields in the {col_num} column')
def check_table(context, col_num):
    helper = WebDriverHelper(context)
    selector = "tr>td:nth-child({child})>span.ng-binding.ng-scope".format(child=col_num)
    helper.wait_for_class("agg-table-paginated")
    sleep(1)
    fields = helper.by_css(selector, True)
    fields_text = [el.text for el in fields]
    for row in context.table:
        assert row['fields'] in fields_text, "{0} should be in {1} labels".format(row['fields'], fields_text)

@then('I should see timestamp chart')
def step_impl(context):
    helper = WebDriverHelper(context)
    selector = ".discover-timechart.ng-scope"
    helper.wait_for_css(selector)
    assert helper.by_css(selector).is_displayed(), 'timestamp chart should display'

@when('I add fields')
def step_impl(context):
    helper = WebDriverHelper(context)
    selector = "div[@class='sidebar-item-title']/field-name[contains(.,'{}')]"
    for field in context.table:
        value = field['value']
        field_xpath= "//{}".format(selector.format(value))
        button_suffix = "/following-sibling::button"
        visibility_prefix = "//ul[@class='list-unstyled discover-selected-fields']/li/{}"
        helper.move_to_element(field_xpath, 'xpath')
        helper.click_element_by_xpath(field_xpath + button_suffix)
        helper.wait_for_xpath(visibility_prefix.format(selector.format(value)))

@then('I should see these columns in result table')
def step_impl(context):
    helper = WebDriverHelper(context)
    selector = "table.kbn-table.table.ng-scope>thead.ng-isolate-scope>th.ng-scope"
    heads = helper.by_css(selector, True)
    heads_text = [el.text for el in heads]
    for row in context.table:
        assert row['value'] in heads_text, "{0} should be in table headers".format(row['value'])


def _go_to_url(context):
    url = context.kibana_url
    try:
        go_to_url(context, url)
    except:
        raise AssertionError("Can't open page: {}".format(url))
