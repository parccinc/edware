#!/bin/bash

# $1 = aws / local
# $2 = [realm] if $1 = aws, [proxy IP] if $1 = local
# $3 = cloud - Use private IP, other - use Public IP
# $4 = qa / stg

if [[ $1 == aws ]]
then
  REALM=$2
  if [[ $3 == cloud ]]
  then
    UDL_LZ_SERVER=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=udl-lz-server" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
    TAKE_SCREENSHOT_ON_FAILURE='yes'
  else
    UDL_LZ_SERVER=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=udl-lz-server" --query "Reservations[0].Instances[0].PublicIpAddress" --output=text)
    TAKE_SCREENSHOT_ON_FAILURE='no'
  fi

  AWSACCT_ENV_VAR='dev'
  if [[ $4 == 'qa' || $4 == 'stg' ]]
  then
      AWSACCT_ENV_VAR='$4'
  fi

  # HPZ #
  HPZ_APP_SERVER=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=hpz-app-server" --query 'Reservations[0].Instances[0].Tags[?Key==`Name`].Value' --output=text)

  # PARCC CDS #
  MASTER_DB_CDS_PARCC_SERVER=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=cds-master-parcc" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  SLAVE_DB_CDS_PARCC_SERVER1=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=cds-slave-parcc1" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  SLAVE_DB_CDS_PARCC_SERVER2=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=cds-slave-parcc2" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)

  # TENANT CDS #
  MASTER_DB_CDS_SECOND_TENANT_SERVER=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=cds-master-dog" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  SLAVE_DB_CDS_SECOND_TENANT_SERVER1=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=cds-slave-dog1" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  SLAVE_DB_CDS_SECOND_TENANT_SERVER2=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=cds-slave-dog2" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)

  MASTER_DB_CDS_FIRST_TENANT_SERVER=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=cds-master-cat" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  SLAVE_DB_CDS_FIRST_TENANT_SERVER1=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=cds-slave-cat1" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  SLAVE_DB_CDS_FIRST_TENANT_SERVER2=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=cds-slave-cat2" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)

  # UTILITY RDS (Among other things edware_stats lives here)
  UTILITY_RDS_SERVER=$(aws rds describe-db-instances --db-instance-identifier "$2-rds-utility-cluster" --query "DBInstances[0].Endpoint.Address" --output=text)
  UTILITY_RDS_PORT=$(aws rds describe-db-instances --db-instance-identifier "$2-rds-utility-cluster" --query "DBInstances[0].Endpoint.Port" --output=text)

  # STAGING RDS
  STAGING_RDS_SERVER=$(aws rds describe-db-instances --db-instance-identifier "$2-rds-udl-staging-cluster" --query "DBInstances[0].Endpoint.Address" --output=text)
  STAGING_RDS_PORT=$(aws rds describe-db-instances --db-instance-identifier "$2-rds-udl-staging-cluster" --query "DBInstances[0].Endpoint.Port" --output=text)

  LOADER_DB_SERVER=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=loader-db-server" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  MASTER_DB_SERVER1=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=dw-master-cat" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  MASTER_DB_SERVER2=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=dw-master-dog" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  MASTER_DB_SERVER3=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=dw-master-fish" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  SLAVE_DB_SERVER1=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=dw-slave-cat1" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  SLAVE_DB_SERVER2=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=dw-slave-cat2" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  SLAVE_DB_SERVER3=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=dw-slave-dog1" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  SLAVE_DB_SERVER4=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=dw-slave-dog2" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  SLAVE_DB_SERVER5=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=dw-slave-fish1" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  SLAVE_DB_SERVER6=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=dw-slave-fish2" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  MIGRATE_CONDUCTOR1=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:host_group,Values=*migrate-conductor*" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  MIGRATE_CONDUCTOR2=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:host_group,Values=*migrate-conductor*" --query "Reservations[1].Instances[0].PrivateIpAddress" --output=text)
  DATA_BACKUP_SERVER=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=data-backup-server" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  PENTAHO_SERVER=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=pentaho-server" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  PENTAHO_UTILITY=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=pentaho-utility" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  ANALYTICS_STATE_MASTER_DB_SERVER1=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=analytics-master-cat" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  ANALYTICS_STATE_MASTER_DB_SERVER2=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=analytics-master-dog" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  ANALYTICS_STATE_MASTER_DB_SERVER3=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=analytics-master-fish" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  ANALYTICS_STATE_SLAVE_DB_SERVER1=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=analytics-slave-cat1" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  ANALYTICS_STATE_SLAVE_DB_SERVER2=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=analytics-slave-cat2" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  ANALYTICS_STATE_SLAVE_DB_SERVER3=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=analytics-slave-dog1" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  ANALYTICS_STATE_SLAVE_DB_SERVER4=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=analytics-slave-dog2" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  ANALYTICS_STATE_SLAVE_DB_SERVER5=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=analytics-slave-fish1" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  ANALYTICS_STATE_SLAVE_DB_SERVER6=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=analytics-slave-fish2" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  TRIGGER_S3_BUCKET='s3://parcc-analytics-source-$AWSACCT_ENV_VAR/$2/'
  EXTRACT_SERVER=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=extract-worker-server" --query "Reservations[0].Instances[0].PrivateIpAddress" --output=text)
  EXTRACT_LOCATION_BASE="/opt/edware/extraction/"
  REPORTING_APP_SERVER=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=$2" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=reporting-app-server" --query 'Reservations[0].Instances[0].Tags[?Key==`Name`].Value' --output=text)
  STATIC_REPORTING_APP_SERVER=10.172.7.87
  ENVIRONMENT="AWS"

  KIBANA_URL="https://${REALM}-kibana-elb.dev.parccresults.org"
  UCHIWA_URL="https://${REALM}-sensu-elb.dev.parccresults.org/uchiwa/"
  GRAPHITE_URL="https://${REALM}-sensu-elb.dev.parccresults.org:8443/"
  GRAFANA_URL="https://${REALM}-sensu-elb.dev.parccresults.org/grafana/"

  if [[ $4 == 'qa' || $4 == 'stg' ]]; then
    REPORTING_APP_PORT="443"
    REPORTING_APP_PROTOCOL="https"
    PENTAHO_URL="https://${REALM}-pentaho-elb.${4}.parccresults.org"
  elif [[ $4 == 'dev_with_elbs' ]]; then
    REPORTING_APP_PORT="443"
    REPORTING_APP_PROTOCOL="https"
    REPORTING_ELB_ADDRESS="$2-reporting-elb.dev.parccresults.org"
    PENTAHO_URL="https://${REALM}-pentaho-elb.dev.parccresults.org"
  else
    REPORTING_APP_PORT="80"
    REPORTING_APP_PROTOCOL="http"
    PENTAHO_URL="http://${REALM}-pentaho-master.dev.ae1.parcdc.net:8080"
    UCHIWA_URL="http://${REALM}-sensu1.dev.ae1.parcdc.net/uchiwa/"
    GRAPHITE_URL="http://${REALM}-sensu1.dev.ae1.parcdc.net:8080"
    GRAFANA_URL="http://${REALM}-sensu1.dev.ae1.parcdc.net/grafana/"
    KIBANA_URL="http://${REALM}-loghost.dev.ae1.parcdc.net"
  fi

elif [[ $1 == local ]]
then
  AWSACCT_ENV_VAR='dev'
  UTILITY_RDS_SERVER='localhost'
  UTILITY_RDS_PORT='5432'
  LOADER_DB_SERVER='localhost'
  STAGING_RDS_SERVER='localhost'
  STAGING_RDS_PORT='5432'
  UDL_LZ_SERVER="none"
  # HPZ #
  HPZ_APP_SERVER=$(aws ec2 describe-instances --filters "Name=tag:realm,Values=post-uat-dev1" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=hpz-app-server" --query 'Reservations[0].Instances[0].Tags[?Key==`Name`].Value' --output=text)
  MASTER_DB_CDS_PARCC_SERVER='localhost'
  SLAVE_DB_CDS_PARCC_SERVER1='localhost'
  SLAVE_DB_CDS_PARCC_SERVER2='localhost'
  MASTER_DB_CDS_SECOND_TENANT_SERVER='localhost'
  SLAVE_DB_CDS_SECOND_TENANT_SERVER1='localhost'
  SLAVE_DB_CDS_SECOND_TENANT_SERVER2='localhost'
  MASTER_DB_CDS_FIRST_TENANT_SERVER='localhost'
  SLAVE_DB_CDS_FIRST_TENANT_SERVER1='localhost'
  SLAVE_DB_CDS_FIRST_TENANT_SERVER2='localhost'
  MASTER_DB_SERVER1='localhost'
  MASTER_DB_SERVER2='localhost'
  MASTER_DB_SERVER3='localhost'
  SLAVE_DB_SERVER1='localhost'
  SLAVE_DB_SERVER2='localhost'
  SLAVE_DB_SERVER3='localhost'
  SLAVE_DB_SERVER4='localhost'
  PENTAHO_SERVER='localhost'
  PENTAHO_UTILITY='localhost'
  ANALYTICS_STATE_MASTER_DB_SERVER1='localhost'
  ANALYTICS_STATE_MASTER_DB_SERVER2='localhost'
  ANALYTICS_STATE_MASTER_DB_SERVER3='localhost'
  ANALYTICS_STATE_SLAVE_DB_SERVER1='localhost'
  ANALYTICS_STATE_SLAVE_DB_SERVER2='localhost'
  ANALYTICS_STATE_SLAVE_DB_SERVER3='localhost'
  ANALYTICS_STATE_SLAVE_DB_SERVER4='localhost'
  ENVIRONMENT="local"
  EXTRACT_LOCATION_BASE="/tmp/"
  REPORTING_APP_SERVER="localhost"
  REPORTING_APP_PORT="6543"
  REPORTING_APP_PROTOCOL="http"
  STATIC_REPORTING_APP_SERVER="localhost"
  PENTAHO_URL="http://localhost:8080"
  KIBANA_URL="localhost"
  UCHIWA_URL="localhost"
  GRAPHITE_URL="localhost"
  PROXY_IP="$2"
  PROXY_PORT="443"
else
  echo "USAGE: "
  echo "sh $0 aws|local [realm] [cloud|other] [qa|stg]"
  exit 1
fi

ANALYTICS_SOURCE_BUCKET_PREFIX='parcc-analytics-source-dev'
BACKUP_STORAGE="s3://parcc-udl-backup-${AWSACCT_ENV_VAR}"
if [[ $4 == qa ]]
then
  FIRST_TENANT_NAME='cat'
  FIRST_TENANT_STATE_CODE='NY'
  SECOND_TENANT_NAME='dog'
  SECOND_TENANT_STATE_CODE='RI'
  THIRD_TENANT_NAME='fish'
  THIRD_TENANT_STATE_CODE='VT'

  MASTER_DB_USER='edware'
  MASTER_DB_PASSWORD='edware2013'
  MASTER_DB_SCHEMA='edware_prod'
  MASTER_DB='edware'
  MASTER_DB_USER_SECOND_TENANT='edware'
  MASTER_DB_PASSWORD_SECOND_TENANT='edware2013'
  MASTER_DB_SCHEMA_SECOND_TENANT='edware_prod'
  MASTER_DB_SECOND_TENANT='edware'
  MASTER_DB_USER_THIRD_TENANT='edware'
  MASTER_DB_PASSWORD_THIRD_TENANT='edware2013'
  MASTER_DB_SCHEMA_THIRD_TENANT='edware_prod'
  MASTER_DB_THIRD_TENANT='edware'

  # HPZ #
  HPZ_APP_SERVER="$2-hpz-elb.qa.parccresults.org"
  HPZ_PROTOCOL="https"

  # TENANT CDS #
  MASTER_DB_USER_CDS_SECOND_TENANT='edware'
  MASTER_DB_PASSWORD_CDS_SECOND_TENANT='edware2013'
  MASTER_DB_SCHEMA_CDS_SECOND_TENANT='edware_tenant_cds'
  MASTER_DB_CDS_SECOND_TENANT='edware'

  MASTER_DB_USER_CDS_FIRST_TENANT='edware'
  MASTER_DB_PASSWORD_CDS_FIRST_TENANT='edware2013'
  MASTER_DB_SCHEMA_CDS_FIRST_TENANT='edware_tenant_cds'
  MASTER_DB_CDS_FIRST_TENANT='edware'

  # PARCC CDS #
  MASTER_DB_USER_CDS_PARCC='edware'
  MASTER_DB_PASSWORD_CDS_PARCC='edware2013'
  MASTER_DB_SCHEMA_CDS_PARCC='edware_cds'
  MASTER_DB_CDS_PARCC='edware'

  STAGING_DB_USER='edware'
  STAGING_DB_PASSWORD='edware2013'
  STAGING_DB='cat'
  STAGING_DB_USER_SECOND_TENANT='edware'
  STAGING_DB_PASSWORD_SECOND_TENANT='edware2013'
  STAGING_DB_SECOND_TENANT='dog'
  LOADER_DB_USER='edware'
  LOADER_DB_PASSWORD='edware2013'
  LOADER_DB='udl2'
  EDWARE_STATS_DB_USER='edware'
  EDWARE_STATS_DB_PASSWORD='edware2013'
  EDWARE_STATS_DB='edware_stats'
  EDWARE_STATS_SCHEMA='edware_stats'
  UDL_SCHEMA='udl2'
  UDL_LZ_USER='test_qa_lz_cat_user'
  UDL_LZ_KEY='amplify_parcc_qa_ci_adminkeys.pem'
  UDL_LZ_USER_SECOND_TENANT='test_qa_lz_dog_user'
  UDL_LZ_KEY_SECOND_TENANT='amplify_parcc_qa_ci_adminkeys.pem'
  GPG_KEY_LOCATION='qa'
  GPG_RECIPIENT='sbac_data_provider@sbac.com'
  GPG_KEY='ca_user@ca.com'
  GPG_PASSPHRASE='sbac udl2'
  MIGRATE_FREQUENCY='minutely'
  EXTRACT_SERVER_USER='root'
  EXTRACT_SERVER_KEY='amplify_parcc_qa_ci_adminkeys.pem'
  ANALYTICS_SOURCE_BUCKET_PREFIX='parcc-analytics-source-qa'
  echo "[test]" > test.ini
  echo "first_tenant_name  = ${FIRST_TENANT_NAME}" >> test.ini
  echo "first_tenant_state_code  = ${FIRST_TENANT_STATE_CODE}" >> test.ini
  echo "second_tenant_name  = ${SECOND_TENANT_NAME}" >> test.ini
  echo "second_tenant_state_code  = ${SECOND_TENANT_STATE_CODE}" >> test.ini
  echo "third_tenant_name  = ${THIRD_TENANT_NAME}" >> test.ini
  echo "third_tenant_state_code  = ${THIRD_TENANT_STATE_CODE}" >> test.ini
  echo "master_db_user  = ${MASTER_DB_USER}" >> test.ini
  echo "master_db_password  = ${MASTER_DB_PASSWORD}" >> test.ini
  echo "master_db_schema  = ${MASTER_DB_SCHEMA}" >> test.ini
  echo "master_db  = ${MASTER_DB}" >> test.ini
  echo "master_db_user_second_tenant  = ${MASTER_DB_USER_SECOND_TENANT}" >> test.ini
  echo "master_db_password_second_tenant  = ${MASTER_DB_PASSWORD_SECOND_TENANT}" >> test.ini
  echo "master_db_schema_second_tenant  = ${MASTER_DB_SCHEMA_SECOND_TENANT}" >> test.ini
  echo "master_db_second_tenant  = ${MASTER_DB_SECOND_TENANT}" >> test.ini
  echo "master_db_user_third_tenant  = ${MASTER_DB_USER_THIRD_TENANT}" >> test.ini
  echo "master_db_password_third_tenant  = ${MASTER_DB_PASSWORD_THIRD_TENANT}" >> test.ini
  echo "master_db_schema_third_tenant  = ${MASTER_DB_SCHEMA_THIRD_TENANT}" >> test.ini
  echo "master_db_third_tenant  = ${MASTER_DB_THIRD_TENANT}" >> test.ini

  # CDS
  echo "master_db_user_cds_second_tenant  = ${MASTER_DB_USER_CDS_SECOND_TENANT}" >> test.ini
  echo "master_db_password_cds_second_tenant  = ${MASTER_DB_PASSWORD_CDS_SECOND_TENANT}" >> test.ini
  echo "master_db_schema_cds_second_tenant  = ${MASTER_DB_SCHEMA_CDS_SECOND_TENANT}" >> test.ini
  echo "master_db_cds_second_tenant  = ${MASTER_DB_CDS_SECOND_TENANT}" >> test.ini

  echo "master_db_user_cds_first_tenant  = ${MASTER_DB_USER_CDS_FIRST_TENANT}" >> test.ini
  echo "master_db_password_cds_first_tenant  = ${MASTER_DB_PASSWORD_CDS_FIRST_TENANT}" >> test.ini
  echo "master_db_schema_cds_first_tenant  = ${MASTER_DB_SCHEMA_CDS_FIRST_TENANT}" >> test.ini
  echo "master_db_cds_first_tenant  = ${MASTER_DB_CDS_FIRST_TENANT}" >> test.ini

  echo "master_db_user_cds_parcc  = ${MASTER_DB_USER_CDS_PARCC}" >> test.ini
  echo "master_db_password_cds_parcc  = ${MASTER_DB_PASSWORD_CDS_PARCC}" >> test.ini
  echo "master_db_schema_cds_parcc  = ${MASTER_DB_SCHEMA_CDS_PARCC}" >> test.ini
  echo "master_db_cds_parcc  = ${MASTER_DB_CDS_PARCC}" >> test.ini

  echo "staging_db_user  = ${STAGING_DB_USER}" >> test.ini
  echo "staging_db_password  = ${STAGING_DB_PASSWORD}" >> test.ini
  echo "staging_db  = ${STAGING_DB}" >> test.ini
  echo "staging_db_user_second_tenant  = ${STAGING_DB_USER_SECOND_TENANT}" >> test.ini
  echo "staging_db_password_second_tenant  = ${STAGING_DB_PASSWORD_SECOND_TENANT}" >> test.ini
  echo "staging_db_second_tenant  = ${STAGING_DB_SECOND_TENANT}" >> test.ini
  echo "loader_db_user  = ${LOADER_DB_USER}" >> test.ini
  echo "loader_db_password  = ${LOADER_DB_PASSWORD}" >> test.ini
  echo "loader_db  = ${LOADER_DB}" >> test.ini
  echo "edware_stats_db_user  = ${EDWARE_STATS_DB_USER}" >> test.ini
  echo "edware_stats_db_password  = ${EDWARE_STATS_DB_PASSWORD}" >> test.ini
  echo "edware_stats_db  = ${EDWARE_STATS_DB}" >> test.ini
  echo "edware_stats_schema  = ${EDWARE_STATS_SCHEMA}" >> test.ini
  echo "udl_schema  = ${UDL_SCHEMA}" >> test.ini
  echo "udl_lz_user  = ${UDL_LZ_USER}" >> test.ini
  echo "udl_lz_key  = ${UDL_LZ_KEY}" >> test.ini
  echo "udl_lz_user_second_tenant  = ${UDL_LZ_USER_SECOND_TENANT}" >> test.ini
  echo "udl_lz_key_second_tenant  = ${UDL_LZ_KEY_SECOND_TENANT}" >> test.ini
  echo "gpg_key_location  = ${GPG_KEY_LOCATION}" >> test.ini
  echo "gpg_recipient = ${GPG_RECIPIENT}" >> test.ini
  echo "gpg_key = ${GPG_KEY}" >> test.ini
  echo "gpg_passphrase = ${GPG_PASSPHRASE}" >> test.ini
  echo "migrate_frequency = ${MIGRATE_FREQUENCY}" >> test.ini
  echo "environment = ${ENVIRONMENT}" >> test.ini
  echo "extract_server_user = ${EXTRACT_SERVER_USER}" >> test.ini
  echo "extract_server_key = ${EXTRACT_SERVER_KEY}" >> test.ini
elif [[ $4 == stg ]]
then
  aws s3api get-object --bucket amplify-parcc-config-stg --key test.ini test.ini
  mkdir -p data_warehouse/data/keys/stg
  aws s3api get-object --bucket amplify-udl-keys-stg --key pubring.gpg data_warehouse/data/keys/stg/pubring.gpg
  aws s3api get-object --bucket amplify-udl-keys-stg --key secring.gpg data_warehouse/data/keys/stg/secring.gpg
  aws s3api get-object --bucket amplify-udl-keys-stg --key trustdb.gpg data_warehouse/data/keys/stg/trustdb.gpg
  ANALYTICS_SOURCE_BUCKET_PREFIX='parcc-analytics-source-stg'
else
  FIRST_TENANT_NAME='cat'
  FIRST_TENANT_STATE_CODE='NY'
  SECOND_TENANT_NAME='dog'
  SECOND_TENANT_STATE_CODE='RI'
  THIRD_TENANT_NAME='fish'
  THIRD_TENANT_STATE_CODE='VT'

  # PARCC CDS #
  MASTER_DB_USER_CDS_PARCC='edware'
  MASTER_DB_PASSWORD_CDS_PARCC='edware2013'
  MASTER_DB_SCHEMA_CDS_PARCC='edware_cds'
  MASTER_DB_CDS_PARCC='edware'

  # HPZ #
  if [[ $4 == 'dev_with_elbs' ]]; then
    HPZ_APP_SERVER="$2-hpz-elb.dev.parccresults.org"
    HPZ_PROTOCOL="https"
    IDP_HOSTNAME="https://gamma-openam.dev.parccresults.org/openam/UI/Login"
  else
    HPZ_PROTOCOL="http"
    IDP_HOSTNAME="http://openam-gamma-server1.dev.ae1.parcdc.net:8080/OpenAM-11.0.0/UI/Login"
  fi
  STAGING_DB_USER='edware'
  STAGING_DB_PASSWORD='edware2013'
  STAGING_DB='cat'
  STAGING_DB_USER_SECOND_TENANT='edware'
  STAGING_DB_PASSWORD_SECOND_TENANT='edware2013'
  STAGING_DB_SECOND_TENANT='dog'
  LOADER_DB_USER='edware'
  LOADER_DB_PASSWORD='edware2013'
  LOADER_DB='udl2'
  EDWARE_STATS_DB_USER='edware'
  EDWARE_STATS_DB_PASSWORD='edware2013'
  EDWARE_STATS_DB='edware_stats'
  EDWARE_STATS_SCHEMA='edware_stats'
  STARMIGRATE_SCHEMA='starmigrate'
  ANALYTICS_DB_USER='edware'
  ANALYTICS_DB_PASSWORD='edware2013'
  ANALYTICS_DB='analytics'
  ANALYTICS_DB_SCHEMA='analytics'
  ANALYTICS_DB_USER_SECOND_TENANT='edware'
  ANALYTICS_DB_PASSWORD_SECOND_TENANT='edware2013'
  ANALYTICS_DB_SECOND_TENANT='analytics'
  ANALYTICS_DB_SCHEMA_SECOND_TENANT='analytics'
  UDL_SCHEMA='udl2'
  UDL_LZ_USER='test_lz_cat_user'
  UDL_LZ_KEY='amplify_parcc_ci_adminkeys.pem'
  UDL_LZ_USER_SECOND_TENANT='test_lz_dog_user'
  UDL_LZ_KEY_SECOND_TENANT='amplify_parcc_ci_adminkeys.pem'
  GPG_KEY_LOCATION='dev'
  GPG_RECIPIENT='sbac_data_provider@sbac.com'
  GPG_KEY='ca_user@ca.com'
  GPG_PASSPHRASE='sbac udl2'
  MIGRATE_FREQUENCY='minutely'
  EXTRACT_SERVER_USER='root'
  EXTRACT_SERVER_KEY='amplify_parcc_ci_adminkeys.pem'
  if [[ $1 == local ]]
  then
    MASTER_DB_USER='edware'
    MASTER_DB_PASSWORD='edware2013'
    MASTER_DB_SCHEMA='edware_prod_cat'
    MASTER_DB='edware'
    MASTER_DB_USER_SECOND_TENANT='edware'
    MASTER_DB_PASSWORD_SECOND_TENANT='edware2013'
    MASTER_DB_SCHEMA_SECOND_TENANT='edware_prod'
    MASTER_DB_SECOND_TENANT='edware'
    MASTER_DB_USER_THIRD_TENANT='edware'
    MASTER_DB_PASSWORD_THIRD_TENANT='edware2013'
    MASTER_DB_SCHEMA_THIRD_TENANT='edware_prod_fish'
    MASTER_DB_SECOND_TENANT='edware'
    # TENANT CDS #
    MASTER_DB_USER_CDS_SECOND_TENANT='edware'
    MASTER_DB_PASSWORD_CDS_SECOND_TENANT='edware2013'
    MASTER_DB_SCHEMA_CDS_SECOND_TENANT='edware_tenant_cds_dog'
    MASTER_DB_CDS_SECOND_TENANT='edware'

    MASTER_DB_USER_CDS_FIRST_TENANT='edware'
    MASTER_DB_PASSWORD_CDS_FIRST_TENANT='edware2013'
    MASTER_DB_SCHEMA_CDS_FIRST_TENANT='edware_tenant_cds_cat'
    MASTER_DB_CDS_FIRST_TENANT='edware'
    REALM='developers'
  elif [[ $1 == aws ]]
  then
    MASTER_DB_USER='edware'
    MASTER_DB_PASSWORD='edware2013'
    MASTER_DB_SCHEMA='edware_prod'
    MASTER_DB='edware'
    MASTER_DB_USER_SECOND_TENANT='edware'
    MASTER_DB_PASSWORD_SECOND_TENANT='edware2013'
    MASTER_DB_SCHEMA_SECOND_TENANT='edware_prod'
    MASTER_DB_SECOND_TENANT='edware'
    MASTER_DB_USER_THIRD_TENANT='edware'
    MASTER_DB_PASSWORD_THIRD_TENANT='edware2013'
    MASTER_DB_SCHEMA_THIRD_TENANT='edware_prod'
    MASTER_DB_THIRD_TENANT='edware'
    # TENANT CDS #
    MASTER_DB_USER_CDS_SECOND_TENANT='edware'
    MASTER_DB_PASSWORD_CDS_SECOND_TENANT='edware2013'
    MASTER_DB_SCHEMA_CDS_SECOND_TENANT='edware_tenant_cds'
    MASTER_DB_CDS_SECOND_TENANT='edware'

    MASTER_DB_USER_CDS_FIRST_TENANT='edware'
    MASTER_DB_PASSWORD_CDS_FIRST_TENANT='edware2013'
    MASTER_DB_SCHEMA_CDS_FIRST_TENANT='edware_tenant_cds'
    MASTER_DB_CDS_FIRST_TENANT='edware'
  fi
  echo "[test]" > test.ini
  echo "first_tenant_name  = ${FIRST_TENANT_NAME}" >> test.ini
  echo "first_tenant_state_code  = ${FIRST_TENANT_STATE_CODE}" >> test.ini
  echo "second_tenant_name  = ${SECOND_TENANT_NAME}" >> test.ini
  echo "second_tenant_state_code  = ${SECOND_TENANT_STATE_CODE}" >> test.ini
  echo "third_tenant_name  = ${THIRD_TENANT_NAME}" >> test.ini
  echo "third_tenant_state_code  = ${THIRD_TENANT_STATE_CODE}" >> test.ini
  echo "master_db_user  = ${MASTER_DB_USER}" >> test.ini
  echo "master_db_password  = ${MASTER_DB_PASSWORD}" >> test.ini
  echo "master_db_schema  = ${MASTER_DB_SCHEMA}" >> test.ini
  echo "master_db  = ${MASTER_DB}" >> test.ini
  echo "master_db_user_second_tenant  = ${MASTER_DB_USER_SECOND_TENANT}" >> test.ini
  echo "master_db_password_second_tenant  = ${MASTER_DB_PASSWORD_SECOND_TENANT}" >> test.ini
  echo "master_db_schema_second_tenant  = ${MASTER_DB_SCHEMA_SECOND_TENANT}" >> test.ini
  echo "master_db_second_tenant  = ${MASTER_DB_SECOND_TENANT}" >> test.ini
  echo "master_db_user_third_tenant  = ${MASTER_DB_USER_THIRD_TENANT}" >> test.ini
  echo "master_db_password_third_tenant  = ${MASTER_DB_PASSWORD_THIRD_TENANT}" >> test.ini
  echo "master_db_schema_third_tenant  = ${MASTER_DB_SCHEMA_THIRD_TENANT}" >> test.ini
  echo "master_db_third_tenant  = ${MASTER_DB_THIRD_TENANT}" >> test.ini

  echo "staging_db_user  = ${STAGING_DB_USER}" >> test.ini
  echo "staging_db_password  = ${STAGING_DB_PASSWORD}" >> test.ini
  if [[ $1 == aws ]]
  then
    echo "staging_db  = ${STAGING_DB}" >> test.ini
  else
    echo "staging_db  = edware" >> test.ini
  fi
  echo "staging_db_user_second_tenant  = ${STAGING_DB_USER_SECOND_TENANT}" >> test.ini
  echo "staging_db_password_second_tenant  = ${STAGING_DB_PASSWORD_SECOND_TENANT}" >> test.ini
  if [[ $1 == aws ]]
  then
    echo "staging_db_second_tenant  = ${STAGING_DB_SECOND_TENANT}" >> test.ini
  else
    echo "staging_db_second_tenant  = edware" >> test.ini
  fi

  # CDS
  echo "master_db_user_cds_second_tenant  = ${MASTER_DB_USER_CDS_SECOND_TENANT}" >> test.ini
  echo "master_db_password_cds_second_tenant  = ${MASTER_DB_PASSWORD_CDS_SECOND_TENANT}" >> test.ini
  echo "master_db_schema_cds_second_tenant  = ${MASTER_DB_SCHEMA_CDS_SECOND_TENANT}" >> test.ini
  echo "master_db_cds_second_tenant  = ${MASTER_DB_CDS_SECOND_TENANT}" >> test.ini

  echo "master_db_user_cds_first_tenant  = ${MASTER_DB_USER_CDS_FIRST_TENANT}" >> test.ini
  echo "master_db_password_cds_first_tenant  = ${MASTER_DB_PASSWORD_CDS_FIRST_TENANT}" >> test.ini
  echo "master_db_schema_cds_first_tenant  = ${MASTER_DB_SCHEMA_CDS_FIRST_TENANT}" >> test.ini
  echo "master_db_cds_first_tenant  = ${MASTER_DB_CDS_FIRST_TENANT}" >> test.ini

  echo "master_db_user_cds_parcc  = ${MASTER_DB_USER_CDS_PARCC}" >> test.ini
  echo "master_db_password_cds_parcc  = ${MASTER_DB_PASSWORD_CDS_PARCC}" >> test.ini
  echo "master_db_schema_cds_parcc  = ${MASTER_DB_SCHEMA_CDS_PARCC}" >> test.ini
  echo "master_db_cds_parcc  = ${MASTER_DB_CDS_PARCC}" >> test.ini

  echo "analytics_db_user  = ${ANALYTICS_DB_USER}" >> test.ini
  echo "analytics_db_password  = ${ANALYTICS_DB_PASSWORD}" >> test.ini
  echo "analytics_db  = ${ANALYTICS_DB}" >> test.ini
  echo "analytics_db_schema  = ${ANALYTICS_DB_SCHEMA}" >> test.ini
  echo "analytics_db_user_second_tenant  = ${ANALYTICS_DB_USER_SECOND_TENANT}" >> test.ini
  echo "analytics_db_password_second_tenant  = ${ANALYTICS_DB_PASSWORD_SECOND_TENANT}" >> test.ini
  echo "analytics_db_second_tenant  = ${ANALYTICS_DB_SECOND_TENANT}" >> test.ini
  echo "analytics_db_schema_second_tenant  = ${ANALYTICS_DB_SCHEMA_SECOND_TENANT}" >> test.ini
  echo "loader_db_user  = ${LOADER_DB_USER}" >> test.ini
  echo "loader_db_password  = ${LOADER_DB_PASSWORD}" >> test.ini
  echo "loader_db  = ${LOADER_DB}" >> test.ini
  echo "edware_stats_db_user  = ${EDWARE_STATS_DB_USER}" >> test.ini
  echo "edware_stats_db_password  = ${EDWARE_STATS_DB_PASSWORD}" >> test.ini
  echo "edware_stats_db  = ${EDWARE_STATS_DB}" >> test.ini
  echo "edware_stats_schema  = ${EDWARE_STATS_SCHEMA}" >> test.ini
  echo "udl_schema  = ${UDL_SCHEMA}" >> test.ini
  echo "udl_lz_user  = ${UDL_LZ_USER}" >> test.ini
  echo "udl_lz_key  = ${UDL_LZ_KEY}" >> test.ini
  echo "udl_lz_user_second_tenant  = ${UDL_LZ_USER_SECOND_TENANT}" >> test.ini
  echo "udl_lz_key_second_tenant  = ${UDL_LZ_KEY_SECOND_TENANT}" >> test.ini
  echo "gpg_key_location  = ${GPG_KEY_LOCATION}" >> test.ini
  echo "gpg_recipient = ${GPG_RECIPIENT}" >> test.ini
  echo "gpg_key = ${GPG_KEY}" >> test.ini
  echo "gpg_passphrase = ${GPG_PASSPHRASE}" >> test.ini
  echo "migrate_frequency = ${MIGRATE_FREQUENCY}" >> test.ini
  echo "environment = ${ENVIRONMENT}" >> test.ini
  echo "extract_server_user = ${EXTRACT_SERVER_USER}" >> test.ini
  echo "extract_server_key = ${EXTRACT_SERVER_KEY}" >> test.ini
  echo "idp_hostname = ${IDP_HOSTNAME}" >> test.ini
fi

echo "udl_lz_server = ${UDL_LZ_SERVER}" >> test.ini

echo "utility_rds_server = ${UTILITY_RDS_SERVER}" >> test.ini
echo "utility_rds_port = ${UTILITY_RDS_PORT}" >> test.ini

echo "staging_rds_server = ${STAGING_RDS_SERVER}" >> test.ini
echo "staging_rds_port = ${STAGING_RDS_PORT}" >> test.ini

echo "loader_db_server = ${LOADER_DB_SERVER}" >> test.ini
echo "master_db_server1 = ${MASTER_DB_SERVER1}" >> test.ini
echo "master_db_server2 = ${MASTER_DB_SERVER2}" >> test.ini
echo "master_db_server3 = ${MASTER_DB_SERVER3}" >> test.ini
echo "slave_db_server1 = ${SLAVE_DB_SERVER1}" >> test.ini
echo "slave_db_server2 = ${SLAVE_DB_SERVER2}" >> test.ini
echo "slave_db_server3 = ${SLAVE_DB_SERVER3}" >> test.ini
echo "slave_db_server4 = ${SLAVE_DB_SERVER4}" >> test.ini

# HPZ #
echo "hpz_app_server = ${HPZ_APP_SERVER}" >> test.ini
echo "hpz_protocol = ${HPZ_PROTOCOL}" >> test.ini

# PARCC CDS
echo "master_db_cds_parcc_server = ${MASTER_DB_CDS_PARCC_SERVER}" >> test.ini
echo "slave_db_cds_parcc_server1 = ${SLAVE_DB_CDS_PARCC_SERVER1}" >> test.ini
echo "slave_db_cds_parcc_server2 = ${SLAVE_DB_CDS_PARCC_SERVER2}" >> test.ini

# TENANT CDS
echo "master_db_cds_second_tenant_server = ${MASTER_DB_CDS_SECOND_TENANT_SERVER}" >> test.ini
echo "slave_db_cds_second_tenant_server1 = ${SLAVE_DB_CDS_SECOND_TENANT_SERVER1}" >> test.ini
echo "slave_db_cds_second_tenant_server2 = ${SLAVE_DB_CDS_SECOND_TENANT_SERVER2}" >> test.ini

echo "master_db_cds_first_tenant_server = ${MASTER_DB_CDS_FIRST_TENANT_SERVER}" >> test.ini
echo "slave_db_cds_first_tenant_server1 = ${SLAVE_DB_CDS_FIRST_TENANT_SERVER1}" >> test.ini
echo "slave_db_cds_first_tenant_server2 = ${SLAVE_DB_CDS_FIRST_TENANT_SERVER2}" >> test.ini

echo "migrate_conductor1 = ${MIGRATE_CONDUCTOR1}" >> test.ini
echo "migrate_conductor2 = ${MIGRATE_CONDUCTOR2}" >> test.ini
echo "data_backup_server = ${DATA_BACKUP_SERVER}" >> test.ini
echo "pentaho_server = ${PENTAHO_SERVER}" >> test.ini
echo "pentaho_utility = ${PENTAHO_UTILITY}" >> test.ini
echo "analytics_state_master_db_server1 = ${ANALYTICS_STATE_MASTER_DB_SERVER1}" >> test.ini
echo "analytics_state_master_db_server2 = ${ANALYTICS_STATE_MASTER_DB_SERVER2}" >> test.ini
echo "analytics_state_slave_db_server1 = ${ANALYTICS_STATE_SLAVE_DB_SERVER1}" >> test.ini
echo "analytics_state_slave_db_server2 = ${ANALYTICS_STATE_SLAVE_DB_SERVER2}" >> test.ini
echo "analytics_state_slave_db_server3 = ${ANALYTICS_STATE_SLAVE_DB_SERVER3}" >> test.ini
echo "analytics_state_slave_db_server4 = ${ANALYTICS_STATE_SLAVE_DB_SERVER4}" >> test.ini
echo "analytics_source_bucket_prefix = ${ANALYTICS_SOURCE_BUCKET_PREFIX}" >> test.ini
echo "extract_server = ${EXTRACT_SERVER}" >> test.ini
echo "extract_location_base = ${EXTRACT_LOCATION_BASE}" >> test.ini
echo "reporting_app_server = ${REPORTING_APP_SERVER}" >> test.ini
echo "reporting_app_port = ${REPORTING_APP_PORT}" >> test.ini
echo "reporting_app_protocol = ${REPORTING_APP_PROTOCOL}" >> test.ini
echo "reporting_elb_address = ${REPORTING_ELB_ADDRESS}" >> test.ini
echo "static_reporting_app_server = ${STATIC_REPORTING_APP_SERVER}" >> test.ini
echo "take_screenshot_on_failure = yes" >> test.ini
echo "realm = $REALM" >> test.ini
echo "trigger_s3_bucket = $TRIGGER_S3_BUCKET" >> test.ini
echo "browser = firefox" >> test.ini
echo "pentaho_url = $PENTAHO_URL" >> test.ini
echo "kibana_url = $KIBANA_URL" >> test.ini
echo "uchiwa_url = $UCHIWA_URL" >> test.ini
echo "graphite_url = $GRAPHITE_URL" >> test.ini
echo "grafana_url = $GRAFANA_URL" >> test.ini
echo "proxy_ip = $PROXY_IP" >> test.ini
echo "proxy_port = $PROXY_PORT" >> test.ini
echo "backup_storage = $BACKUP_STORAGE" >> test.ini
