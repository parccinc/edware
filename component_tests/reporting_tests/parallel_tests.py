#!/usr/bin/env python

import os
import glob
import multiprocessing
import subprocess
import argparse
from datetime import datetime

__author__ = 'Ethan Lusterman'

'''
This script takes as input the number of desired simultaneous processes
for running functional tests and constructs bins of files based on size.
These bins of files are then run in parallel with behave.
'''

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Run feature files in parallel for quicker runtimes.')
    parser.add_argument('-p', '--proc',
                        help='Number of simultaneous processes to run. Defaults to the number of logical cores on your machine.',
                        default=multiprocessing.cpu_count())
    args = parser.parse_args()
    num_groups = int(args.proc)

    # Get frontend feature file paths
    here = os.path.dirname(__file__)
    frontend_feature_dir = os.path.join(here, 'features', 'frontend')
    fpaths = glob.glob(
        os.path.join(frontend_feature_dir, '*.feature'))
    if num_groups > len(fpaths):
        num_groups = len(fpaths)
    fpaths.sort(key=os.path.getsize, reverse=True)

    # Based on the number of processes, generate groups of files to run tests.
    # N.B. This assumes that the file size is correlated with the length of
    # time it takes to run the feature file.
    fpath_groups = [[] for _ in range(num_groups)]
    fpath_sizes = [0 for _ in range(num_groups)]
    for fpath in fpaths:
        idx = fpath_sizes.index(min(fpath_sizes))
        fpath_sizes[idx] += os.path.getsize(fpath)
        fpath_groups[idx].append(fpath)

    # delete/make log output directory
    curr_time = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    log_dir = os.path.join('/tmp', 'ft_logs', curr_time)
    os.makedirs(log_dir)

    # run commands in background
    #   e.g. behave f1.feature f2.feature > /tmp/out1.log 2>&1
    def work(group):
        logfilename = ('--'.join([os.path.basename(i) for i in group])).replace('.feature', '')
        cmd = 'behave {files} > {log_file} 2>&1'
        cmd = cmd.format(files=' '.join(group),
                         log_file=os.path.join(log_dir, 'results-{0}.log'.format(logfilename)))
        print(cmd)
        return subprocess.call(cmd, shell=True)

    p = multiprocessing.Pool(processes=num_groups)
    r = p.map(work, fpath_groups)
    p.close()
    p.join()

    # view results
    print("\nResults\n")
    subprocess.call("tail -n 5 {dir}/*.log".format(dir=log_dir), shell=True)
