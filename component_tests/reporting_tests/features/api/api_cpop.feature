Feature: Verify the Comparing Populations API for Reporting

  Background:
    Given the API is set up
    And a cookie is set for user "gman"

  @RALLY_US31194
  @RALLY_US33940
  Scenario: Verify API for Comparing Populations Report
    When the comparing populations report API is accessed with the following parameters:
      | param        | value     |
      | year         | 2015      |
      | gradeCourse  | Grade 11        |
      | asmtType     | SUMMATIVE |
      | stateCode    | RI        |
      | districtGuid | R0003     |
      | subject      | subject2  |
      | view      	 | performance|
    Then the response code should be 200
    And the response body has the json fields "assessments,context"
    And I expect "3" entities in "subject2"

  @RALLY_US33938
  @RALLY_US33940
  Scenario Outline: Verify API for Comparing Populations Report for Grade/Course selector for ELA
    When the comparing populations report API is accessed with the following parameters:
      | param        | value     |
      | year         | 2015      |
      | gradeCourse  | <grade>   |
      | asmtType     | SUMMATIVE |
      | stateCode    | RI        |
      | districtGuid | R0003     |
      | subject      | subject2  |
      | view      	 | performance|
    Then the response code should be 200
    And the response body has the json fields "assessments,context"
    And I expect "<exp_entry>" entities in "subject2"
   Examples:
      |grade        |exp_entry|
      |Grade 9      |3        |
      |Grade 11     |3        |

  @RALLY_US33938
  @RALLY_US33940
  Scenario Outline: Verify API for Comparing Populations Report for Grade/Course selector for Math
    When the comparing populations report API is accessed with the following parameters:
      | param        | value         |
      | year         | 2015          |
      | gradeCourse  | <gradeCourse> |
      | asmtType     | SUMMATIVE     |
      | stateCode    | RI            |
      | districtGuid | R0003         |
      | subject      | subject1      |
      | view      	 | performance|
    Then the response code should be 200
    And the response body has the json fields "assessments,context"
    And I expect "<exp_entry>" entities in "subject1"
   Examples:
      |gradeCourse      |exp_entry|
      |Grade 3          |1        |
      |Grade 8          |1        |
      |Integrated Mathematics III  |2        |

  @RALLY_US34456
  Scenario: Verify API for Comparing Populations Report for Minimum Cell Size
    When the comparing populations report API is accessed with the following parameters:
      | param        | value     |
      | year         | 2015      |
      | gradeCourse  | Grade 3   |
      | asmtType     | SUMMATIVE |
      | stateCode    | RI        |
      | subject      | subject2  |
      | view      	 | performance|
    Then the response code should be 200
    And the response body has the json fields "assessments,context"
    And the entity with guid "R0002" should have the is_less_than_min_cell_size flag set
    And the entity with guid "R0003" should have no flag set

  @RALLY_US33166
  @RALLY_US33940
  Scenario Outline: Verify API for Comparing Populations Report for subscores
    When the comparing populations report API is accessed with the following parameters:
      | param        | value      |
      | year         | 2015       |
      | gradeCourse  | <grade>    |
      | asmtType     | SUMMATIVE  |
      | stateCode    | RI         |
      | districtGuid | R0003      |
      | result       | <subscore> |
      | subject      | <subject>  |
      | view      	 | performance|
    Then the response code should be 200
    And the response body has the json fields "assessments,context"
    And I expect "<entity_count>" entities in "<subject>"
  Examples:
      | subscore     | grade       | entity_count | subject  |
      | literarytext | Grade 11    | 3            | subject2 |
      | infotext     | Grade 11    | 3            | subject2 |
      | vocab        | Grade 11    | 3            | subject2 |
      | writingexp   | Grade 11    | 3            | subject2 |
      | knwldconv    | Grade 11    | 3            | subject2 |
      | majorcontent | Grade 3     | 1            | subject1 |
      | addcontent   | Grade 3     | 1            | subject1 |
      | modelapp     | Grade 3     | 1            | subject1 |
      | mathreason   | Grade 3     | 1            | subject1 |

  @RALLY_US32561
  Scenario: Verify API for Breadcrumbs on Comparing Populations Report
    When the comparing populations report API is accessed with the following parameters:
      | param        | value     |
      | year         | 2015      |
      | gradeCourse  | Grade 11  |
      | asmtType     | SUMMATIVE |
      | stateCode    | RI        |
      | districtGuid | R0003     |
      | subject      | subject2  |
      | view      	 | performance|
    Then the response code should be 200
    And the response body has the json fields "assessments,context,user_info"
    And the response body has 3 items for breadcrumbs on the comparing populations report
    And the first breadcrumb type is home and display name is Home
    And the second breadcrumb type is state and display name is Rhode Island
    And the third breadcrumb type is district and display name is Providence

  @RALLY_US31839
  Scenario: Verify API request for missing params (student roster report)
    When the comparing populations report API is accessed with the following parameters:
      | param        | value      |
      | gradeCourse  | Grade 11   |
      | asmtType     | SUMMATIVE  |
      | districtGuid | R0003      |
      | subject      | subject2   |
      | view      	 | performance|
    Then the response code should be 412

  @RALLY_US34137
  Scenario: Verify API for Comparing Populations Report with Filters
    When the comparing populations report API is accessed with the following parameters:
      | param        | value     |
      | year         | 2015      |
      | gradeCourse  | Grade 11  |
      | asmtType     | SUMMATIVE |
      | districtGuid | R0003     |
      | stateCode    | RI        |
      | sex          | M         |
      | subject      | subject2  |
      | view      	 | performance|
  	Then the response code should be 200
  	Then I expect "3" entities in "subject2"
  	And the entity with guid "RP001" should have the has_filters flag set
  	And the entity with guid "RP001" should have the is_less_than_min_cell_size flag set

  @RALLY_US34137
  Scenario: Verify API for Comparing Populations Report with Ethnicity Filters
  	When the comparing populations report API is accessed with the following parameters:
      | param        | value     |
      | year         | 2015      |
      | gradeCourse  | Grade 11  |
      | asmtType     | SUMMATIVE |
      | districtGuid | R0003     |
      | stateCode    | RI        |
      | ethnicity    | 01        |
      | subject      | subject2  |
      | view      	 | performance|
  	Then the response code should be 200
  	Then I expect "1" entities in "subject2"

  @RALLY_US34137
  Scenario: Verify API for Comparing Populations Report with Disabilities Filters
  	When the comparing populations report API is accessed with the following parameters:
      | param        | value     |
      | year         | 2015      |
      | gradeCourse  | Grade 11  |
      | asmtType     | SUMMATIVE |
      | districtGuid | R0003     |
      | stateCode    | RI        |
      | disabilities | N         |
      | subject      | subject2  |
      | view      	 | performance|
  	Then the response code should be 200
  	Then I expect "2" entities in "subject2"

  @RALLY_US34137
  Scenario: Verify API for Comparing Populations Report with Economic Disadvantages Filters
  	When the comparing populations report API is accessed with the following parameters:
      | param        		| value     |
      | year         		| 2015      |
      | gradeCourse    		| Grade 11  |
      | asmtType     		| SUMMATIVE |
      | districtGuid 		| R0003     |
      | stateCode    		| RI        |
      | econoDisadvantage 	| Y			|
      | econoDisadvantage 	| N			|
      | subject             | subject2  |
      | view      	 		| performance|
  	Then the response code should be 200
  	Then I expect "3" entities in "subject2"

  @RALLY_US34137
  Scenario: Verify API for Comparing Populations Report with English Learner
  	When the comparing populations report API is accessed with the following parameters:
      | param        		| value     |
      | year         		| 2015      |
      | gradeCourse    		| Grade 11  |
      | asmtType     		| SUMMATIVE |
      | districtGuid 		| R0003     |
      | stateCode    		| RI        |
      | englishLearner	 	| ELL		|
      | subject             | subject2  |
      | view      	 		| performance|
  	Then the response code should be 200
  	Then I expect "3" entities in "subject2"
  	And the entity with guid "RP002" should have the has_filters flag set
  	And the entity with guid "RP002" should have the is_less_than_threshold flag set

  @RALLY_US34137
  Scenario: Verify API for Comparing Populations Report with English Learner
  	When the comparing populations report API is accessed with the following parameters:
      | param        		| value     |
      | year         		| 2015      |
      | gradeCourse    		| Grade 11  |
      | asmtType     		| SUMMATIVE |
      | districtGuid 		| R0003     |
      | stateCode    		| RI        |
      | englishLearner	 	| ELL		|
      | englishLearner	 	| LEP		|
      | subject             | subject2  |
      | view      	 		| performance|
  	Then the response code should be 200
  	Then I expect "3" entities in "subject2"

  @RALLY_US34137
  Scenario: Verify API for Comparing Populations Report with English Learner Both, LEP, ELL
  	When the comparing populations report API is accessed with the following parameters:
      | param        		| value     |
      | year         		| 2015      |
      | gradeCourse    		| Grade 11  |
      | asmtType     		| SUMMATIVE |
      | districtGuid 		| R0003     |
      | stateCode    		| RI        |
      | englishLearner	 	| ELL		|
      | englishLearner	 	| LEP		|
      | englishLearner	 	| BOTH		|
      | subject             | subject2  |
      | view      	 		| performance|
  	Then the response code should be 200
  	Then I expect "3" entities in "subject2"

  @RALLY_US34137
  Scenario: Verify API for Comparing Populations Report with Economic Disadvantages Filters
  	When the comparing populations report API is accessed with the following parameters:
      | param        		| value     |
      | year         		| 2015      |
      | gradeCourse    		| Grade 11  |
      | asmtType     		| SUMMATIVE |
      | districtGuid 		| R0003     |
      | stateCode    		| RI        |
      | econoDisadvantage	| Y			|
      | econoDisadvantage 	| N			|
      | subject             | subject2  |
      | view      	 		| performance|
  	Then the response code should be 200
  	Then I expect "3" entities in "subject2"

  @RALLY_US34000
  Scenario: Verify API for Comparing Populations Report Aggregation
  	When the comparing populations report API is accessed with the following parameters:
      | param               | value     |
      | year         	    | 2015      |
      | gradeCourse         | Grade 3   |
      | asmtType     	    | SUMMATIVE |
      | stateCode    	    | RI        |
      | subject             | subject1  |
      | view      	 		| performance|
  	Then the response code should be 200
  	Then I expect "1" entities in "subject1"
  	Then I expect "3" students for entity "R0003" in subject "subject1"
  	Then in summary row, I expect "3" students for entity "RI" in subject "subject1"

  @RALLY_US34000
  Scenario: Verify API for Comparing Populations Report Aggregation
  	When the comparing populations report API is accessed with the following parameters:
      | param        		| value     |
      | year         		| 2015      |
      | gradeCourse			| Grade 3   |
      | asmtType     		| SUMMATIVE |
      | stateCode    		| RI        |
      | subject             | subject1  |
      | districtGuid	    | R0003     |
      | view      	 		| performance|
  	Then the response code should be 200
  	Then I expect "1" entities in "subject1"
  	Then I expect "3" students for entity "RP003" in subject "subject1"
  	Then in summary row, I expect "3" students for entity "RI" in subject "subject1"
  	Then in summary row, I expect "3" students for entity "R0003" in subject "subject1"

  @RALLY_US34000
  Scenario: Verify API for Comparing Populations Report Aggregation
  	When the comparing populations report API is accessed with the following parameters:
      | param        		| value      |
      | year         		| 2015       |
      | gradeCourse    		| Grade 3    |
      | asmtType     		| SUMMATIVE  |
      | stateCode    		| RI         |
      | subject             | subject2   |
      | view      	 		| performance|
  	Then the response code should be 200
  	Then I expect "3" entities in "subject2"
  	Then I expect "9" students for entity "R0003" in subject "subject2"
  	Then I expect "1" students for entity "R0001" in subject "subject2"
  	Then I expect "1" students for entity "R0002" in subject "subject2"
  	Then in summary row, I expect "11" students for entity "RI" in subject "subject2"
  	Then in summary row, I expect "2" entities in "subject2"
  	Then in summary row, I expect "0" entities in "subject1"

  @RALLY_US34000
  Scenario: Verify API for Comparing Populations Report Aggregation
  	When the comparing populations report API is accessed with the following parameters:
      | param        		| value      |
      | year         		| 2015       |
      | gradeCourse    		| Grade 3    |
      | asmtType     		| SUMMATIVE  |
      | stateCode    		| RI         |
      | subject             | subject2   |
      | view      	 		| performance|
  	Then the response code should be 200
  	Then I expect "3" entities in "subject2"
  	Then in summary row, I expect "2" entities in "subject2"
  	Then I expect "9" students for entity "R0003" in subject "subject2"
  	Then I expect "1" students for entity "R0001" in subject "subject2"
  	Then I expect "1" students for entity "R0002" in subject "subject2"

  @RALLY_US35583
  Scenario: Verify API for Analytics Links
  	When the comparing populations report API is accessed with the following parameters:
      | param       | value       |
      | year        | 2015        |
      | gradeCourse | Grade 3     |
      | asmtType    | SUMMATIVE   |
      | stateCode   | RI          |
      | subject     | subject2    |
      | view        | performance |
  	Then the response code should be 200
    And the response body has the json fields "analytics"

  @RALLY_US36075
  Scenario: Verify API for Comparing Populations Report for no results
  	When the comparing populations report API is accessed with the following parameters:
      | param        		| value      |
      | year         		| 2014       |
      | gradeCourse    		| Grade 3    |
      | asmtType     		| SUMMATIVE  |
      | stateCode    		| RI         |
      | subject             | subject2   |
      | view      	 		| performance|
  	Then the response code should be 200
    And the response body has no "assessments" field

  @Rally_DE1195
  Scenario: Verify growth view chart y axis cutpoints
  	When the comparing populations report API is accessed with the following parameters:
      | param        		| value      |
      | year         		| 2014       |
      | gradeCourse    		| Grade 3    |
      | asmtType     		| SUMMATIVE  |
      | stateCode    		| RI         |
      | subject             | subject2   |
      | view      	 		| growth     |
  	Then the response code should be 200
    And the response body should have growth chart cut points
