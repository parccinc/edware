Feature: Verify the Comparing Populations API for Reporting

  Background:
    Given the API is set up
    And a cookie is set for user "gman"

  @RALLY_US31839
  Scenario: Verify API for Individual Student Report
    When the individual student report API is accessed with the following parameters:
      | param        | value                                    |
      | districtGuid | R0001                                    |
      | year         | 2015                                     |
      | studentGuid  | EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q |
      | stateCode    | RI                                       |
      | asmtType     | SUMMATIVE                                |
      | gradeCourse  | Grade 9                                       |
      | schoolGuid   | RN001                                    |
      | subject      | subject2                                 |
    Then the response code should be 200
    And the response body has the json fields "assessments,context,subjects,user_info,summary"

  @RALLY_US31839
  Scenario: Verify API for Breadcrumbs on Individual Student Report
    When the individual student report API is accessed with the following parameters:
      | param        | value                                    |
      | districtGuid | R0001                                    |
      | year         | 2015                                     |
      | studentGuid  | EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q |
      | stateCode    | RI                                       |
      | asmtType     | SUMMATIVE                                |
      | gradeCourse  | Grade 9                                       |
      | schoolGuid   | RN001                                    |
      | subject      | subject2                                 |
    Then the response code should be 200
    And the response body has the json fields "assessments,context,subjects,user_info"
    And the response body has 6 items for breadcrumbs on the individual student report
    And the first breadcrumb type is home and display name is Home
    And the second breadcrumb type is state and display name is Rhode Island
    And the third breadcrumb type is district and display name is Newport
    And the fourth breadcrumb type is school and display name is Rogers High School
    And the fifth breadcrumb type is grade and display name is Grade 9
    And the sixth breadcrumb type is student and display name is Darci Forst

    @RALLY_US31839
    Scenario: Verify API request for missing params (ISR)
      When the individual student report API is accessed with the following parameters:
        | param        | value     |
        | districtGuid | R0001     |
        | year         | 2015      |
        | stateCode    | RI        |
        | asmtType     | SUMMATIVE |
        | asmtGrade    | Grade 9        |
        | schoolGuid   | RN001     |
      Then the response code should be 412

  Scenario: Verify API for Breadcrumbs on Individual Student Report
    When the individual student report API is accessed with the following parameters:
      | param        | value                                    |
      | districtGuid | R0002                                    |
      | year         | 2015                                     |
      | studentGuid  | p69xZhxSMQuWmke62h3j1K7izaj1evXeklkHFuMg |
      | stateCode    | RI                                       |
      | asmtType     | SUMMATIVE                                |
      | gradeCourse  | Algebra II                               |
      | subject      | subject1                                 |
      | schoolGuid   | RE001                                    |
    Then the response code should be 200
    And the response body has the json fields "assessments,context,subjects,user_info"
    And the response body has 6 items for breadcrumbs on the individual student report
    And the first breadcrumb type is home and display name is Home
    And the second breadcrumb type is state and display name is Rhode Island
    And the third breadcrumb type is district and display name is East Greenwich
    And the fourth breadcrumb type is school and display name is East Greenwich High School
    And the fifth breadcrumb type is grade and display name is Algebra II
    And the sixth breadcrumb type is student and display name is Cornelius Aispuro

  @RALLY_US35321
  Scenario: Verify API for Individual Student Report for CDS opt out users
    Given the API is set up
    And a cookie is set for user "msmith"
    When the individual student report API is accessed with the following parameters:
      | param        | value                                    |
      | districtGuid | R0003                                    |
      | year         | 2015                                     |
      | studentGuid  | w7TmiOs6CWJsXS2l1crivMsf42ZlQ5WAr6Xri2rd |
      | stateCode    | VT                                       |
      | asmtType     | SUMMATIVE                                |
      | gradeCourse  | Grade 3                                  |
      | schoolGuid   | RP003                                    |
      | subject      | subject2                                 |
    Then the response code should be 200
    And the response body has the json fields "assessments,context,subjects,user_info,summary"
    And the parcc level summary data is suppressed for subject2
