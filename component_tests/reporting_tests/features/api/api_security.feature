Feature: Verify the API Endpoints are protected
 
  @RALLY_US35548
  Scenario: Verify an user with school pii permission cannot access another school
  	Given the API is set up
    And a cookie is set for user "dumbridge"
    When the school report API is accessed with the following parameters:
      | param        | value      |
      | subject      | subject2   |
      | year         | 2015       |
      | asmtType     | DIAGNOSTIC |
      | stateCode    | RI         |
      | districtGuid | R0003      |
      | schoolGuid   | RP001      |
      | view         | vocab      |
	Then the response code should be 403

  @RALLY_US35548
  Scenario: Verify an user with state pii permission cannot access another state
  	Given the API is set up
	And a cookie is set for user "lwoman"
	When the school report API is accessed with the following parameters:
      | param        | value      |
      | subject      | subject2   |
      | year         | 2015       |
      | asmtType     | DIAGNOSTIC |
      | stateCode    | RI         |
      | districtGuid | R0003      |
      | schoolGuid   | RP001      |
      | view         | read_flu   |
	Then the response code should be 403

  @RALLY_US35548
  Scenario: Verify an user with district pii permission cannot access another district
  	Given the API is set up
	And a cookie is set for user "fdostoyevsky"
    When the school report API is accessed with the following parameters:
      | param        | value      |
      | subject      | subject2   |
      | year         | 2015       |
      | asmtType     | DIAGNOSTIC |
      | stateCode    | RI         |
      | districtGuid | R0001      |
      | schoolGuid   | RN001      |
      | view         | overview   |
	Then the response code should be 403
    When the school report API is accessed with the following parameters:
      | param        | value      |
      | subject      | subject2   |
      | year         | 2015       |
      | asmtType     | DIAGNOSTIC |
      | stateCode    | RI         |
      | districtGuid | R0001      |
      | schoolGuid   | RN001      |
      | view         | comp       |
	Then the response code should be 403
    When the school report API is accessed with the following parameters:
      | param        | value      |
      | subject      | subject1   |
      | year         | 2015       |
      | asmtType     | DIAGNOSTIC |
      | stateCode    | RI         |
      | districtGuid | R0001      |
      | schoolGuid   | RN001      |
      | view         |grade_level |
 	Then the response code should be 403
 	When the school report API is accessed with the following parameters:
      | param        | value           |
      | subject      | subject2        |
      | year         | 2015            |
      | gradeCourse  | Grade 3         |
      | asmtType     | DIAGNOSTIC      |
      | stateCode    | RI              |
      | districtGuid | R0001           |
      | schoolGuid   | RN001           |
      | view         | decod           |
    Then the response code should be 403
 
  @RALLY_US35548
  Scenario: Verify an user with school pii permission cannot access student from another school
  	Given the API is set up
  	And a cookie is set for user "dumbridge"
  	When the individual student report API is accessed with the following parameters:
      | param        | value                                    |
      | districtGuid | R0003                                    |
      | year         | 2015                                     |
      | studentGuid  | w7TmiOs6CWJsXS2l1crivMsf42ZlQ5WAr6Xri2rd |
      | stateCode    | RI                                       |
      | asmtType     | SUMMATIVE                                |
      | gradeCourse  | Grade 3                                  |
      | schoolGuid   | RP003                                    |
      | subject      | subject2                                 |
    Then the response code should be 403

  @RALLY_US35548
  Scenario: Verify an user with school pii permission cannot access student from another school for diagnostic
  	Given the API is set up
  	And a cookie is set for user "dumbridge"
  	When the individual student report API is accessed with the following parameters:
      | param        | value                                          |
      | subject      | subject2                                       |
      | year         | 2015                                           |
      | gradeCourse  | Grade 3                                        |
      | asmtType     | DIAGNOSTIC                                     |
      | stateCode    | RI                                             |
      | districtGuid | R0003                                          |
      | schoolGuid   | RP001                                          |
      | studentGuid  | hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ       |
    Then the response code should be 403

   @RALLY_US35548
   Scenario: Verify user with school pii permission cannot access student roster summ of another school
   	Given the API is set up
  	And a cookie is set for user "dumbridge"
   	When the student roster report API is accessed with the following parameters:
      | param        | value     |
      | subject      | subject2  |
      | year         | 2015      |
      | gradeCourse  | Grade 9   |
      | asmtType     | SUMMATIVE |
      | stateCode    | RI        |
      | districtGuid | R0003     |
      | schoolGuid   | RP001     |
      | view         | scores    |
    Then the response code should be 403
    When the student roster report API is accessed with the following parameters:
      | param        | value       |
      | subject      | subject2    |
      | year         | 2015        |
      | gradeCourse  | Grade 9     |
      | asmtType     | SUMMATIVE   |
      | stateCode    | RI          |
      | districtGuid | R0003       |
      | schoolGuid   | RP001       |
      | view         | itemAnalysis|
    Then the response code should be 403

   @RALLY_US35548
   Scenario: Verify user with district pii permission cannot access student roster summ of another school
   	Given the API is set up
  	And a cookie is set for user "fdostoyevsky"
   	When the student roster report API is accessed with the following parameters:
      | param        | value     |
      | subject      | subject2  |
      | year         | 2015      |
      | gradeCourse  | Grade 9   |
      | asmtType     | SUMMATIVE |
      | stateCode    | RI        |
      | districtGuid | R0001     |
      | schoolGuid   | RN001     |
      | view         | scores    |
    Then the response code should be 403
    When the student roster report API is accessed with the following parameters:
      | param        | value       |
      | subject      | subject2    |
      | year         | 2015        |
      | gradeCourse  | Grade 9     |
      | asmtType     | SUMMATIVE   |
      | stateCode    | RI          |
      | districtGuid | R0001       |
      | schoolGuid   | RN001       |
      | view         | itemAnalysis|
    Then the response code should be 403

   @RALLY_US35548
   Scenario: Verify user with state pii permission cannot access another state in cpop level
   	Given the API is set up
  	And a cookie is set for user "lwoman"
  	When the comparing populations report API is accessed with the following parameters:
      | param        | value     |
      | year         | 2015      |
      | gradeCourse  | Grade 11  |
      | asmtType     | SUMMATIVE |
      | stateCode    | RI        |
      | subject      | subject2  |
      | view      	 | performance|
    Then the response code should be 403

   @RALLY_US35548
   Scenario: Verify user with state pii permission can access roster, isr
   	Given the API is set up
  	And a cookie is set for user "esnyder"
  	When the school report API is accessed with the following parameters:
      | param        | value      |
      | subject      | subject2   |
      | year         | 2015       |
      | asmtType     | DIAGNOSTIC |
      | stateCode    | RI         |
      | districtGuid | R0003      |
      | schoolGuid   | RP001      |
      | view         | vocab      |
	Then the response code should be 200
	When the student roster report API is accessed with the following parameters:
      | param        | value       |
      | subject      | subject2    |
      | year         | 2015        |
      | gradeCourse  | Grade 9     |
      | asmtType     | SUMMATIVE   |
      | stateCode    | RI          |
      | districtGuid | R0001       |
      | schoolGuid   | RN001       |
      | view         | itemAnalysis|
    Then the response code should be 200
    When the individual student report API is accessed with the following parameters:
      | param        | value                                          |
      | subject      | subject2                                       |
      | year         | 2015                                           |
      | gradeCourse  | Grade 3                                        |
      | asmtType     | DIAGNOSTIC                                     |
      | stateCode    | RI                                             |
      | districtGuid | R0003                                          |
      | schoolGuid   | RP001                                          |
      | studentGuid  | hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ       |
    Then the response code should be 200

  @RALLY_US35601
  Scenario: Verify an user with state sf_extract permission cannot extract another state's data
  	Given the API is set up
  	And a cookie is set for user "lwoman"
  	When a bulk extract triggered with the following parameters
      | param        | value                                          |
      | subject      | subject2                                       |
      | year         | 2015                                           |
      | gradeCourse  | Grade 3                                        |
      | extractType  | summ_asmt_result                               |
      | stateCode    | RI                                             |
    Then the response code should be 403
  
  @RALLY_US35601
  Scenario: Verify an user with district sf_extract permission cannot extract another state's data
  	Given the API is set up
  	And a cookie is set for user "fdostoyevsky"
  	When a bulk extract triggered with the following parameters
      | param        | value                                          |
      | subject      | subject2                                       |
      | year         | 2015                                           |
      | gradeCourse  | Grade 3                                        |
      | extractType  | summ_asmt_result                               |
      | stateCode    | NY                                             |
    Then the response code should be 403

  @RALLY_US35601
  Scenario: Verify an user with state sf_extract permission can extract his own state's data
  	Given the API is set up
  	And a cookie is set for user "esnyder"
  	When a bulk extract triggered with the following parameters
      | param        | value                                          |
      | subject      | subject2                                       |
      | year         | 2015                                           |
      | gradeCourse  | Grade 3                                        |
      | extractType  | summ_asmt_result                               |
      | stateCode    | RI                                             |
    Then the response code should be 200

  @RALLY_US35601
  Scenario: Verify an user with district sf_extract permission can extract his own state's data
  	Given the API is set up
  	And a cookie is set for user "fdostoyevsky"
  	When a bulk extract triggered with the following parameters
      | param        | value                                          |
      | subject      | subject2                                       |
      | year         | 2015                                           |
      | gradeCourse  | Grade 3                                        |
      | extractType  | summ_asmt_result                               |
      | stateCode    | RI                                             |
    Then the response code should be 200

  @RALLY_US35601
  Scenario: Verify an user with state rf_extract permission can extract his own state's data
  	Given the API is set up
  	And a cookie is set for user "esnyder"
  	When a bulk extract triggered with the following parameters
      | param        | value                                          |
      | subject      | subject2                                       |
      | year         | 2015                                           |
      | gradeCourse  | Grade 3                                        |
      | extractType  | item_student_score                             |
      | stateCode    | RI                                             |
    Then the response code should be 200

  @RALLY_US35601
  Scenario: Verify an user with no rf_extract permission cannot extract data
  	Given the API is set up
  	And a cookie is set for user "rhood"
  	When a bulk extract triggered with the following parameters
      | param        | value                                          |
      | subject      | subject2                                       |
      | year         | 2015                                           |
      | gradeCourse  | Grade 3                                        |
      | extractType  | item_student_score                             |
      | stateCode    | RI                                             |
    Then the response code should be 403

  @RALLY_US35601
  Scenario: Verify an user with no pf_extract permission cannot extract data
  	Given the API is set up
  	And a cookie is set for user "esnyder"
  	When a bulk extract triggered with the following parameters
      | param        | value                                          |
      | year         | 2015                                           |
      | extractType  | p_data                                         |
      | stateCode    | RI                                             |
    Then the response code should be 403

  @RALLY_US35601
  Scenario: Verify an user with no pf_extract permission cannot extract data from another state
  	Given the API is set up
  	And a cookie is set for user "rhood"
  	When a bulk extract triggered with the following parameters
      | param        | value                                          |
      | year         | 2015                                           |
      | extractType  | p_data                                         |
      | stateCode    | RI                                             |
    Then the response code should be 403

  @RALLY_US35601
  Scenario: Verify an user with no pf_extract permission can extract data from his own state
  	Given the API is set up
  	And a cookie is set for user "gman"
  	When a bulk extract triggered with the following parameters
      | param        | value                                          |
      | year         | 2015                                           |
      | extractType  | p_data                                         |
      | stateCode    | RI                                             |
    Then the response code should be 200

  @RALLY_US36141
  Scenario: Verify any user with permissions for one state can access a state which has made its aggregate data accessible
  	Given the API is set up
	And a cookie is set for user "msmith"
	When the school report API is accessed with the following parameters:
      | param        | value      |
      | subject      | subject2   |
      | year         | 2015       |
      | asmtType     | SUMMATIVE  |
      | stateCode    | NY         |
      | districtGuid | R0003      |
      | schoolGuid   | RP001      |
	Then the response code should be 200
	And the parcc level summary data is suppressed for subject2

  @RALLY_US36141
  Scenario: Verify any user with permissions for one state can not access pii for a state which has made its aggregate data accessible
  	Given the API is set up
	And a cookie is set for user "msmith"
	When the student roster report API is accessed with the following parameters:
      | param        | value     |
      | subject      | subject2  |
      | year         | 2015      |
      | gradeCourse  | Grade 9   |
      | asmtType     | SUMMATIVE |
      | stateCode    | NY        |
      | districtGuid | R0003     |
      | schoolGuid   | RP001     |
      | view         | scores    |
	Then the response code should be 403
