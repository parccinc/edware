Feature: Verify the Comparing Populations API for Reporting

  Background:
    Given the API is set up
    And a cookie is set for user "gman"

  @RALLY_US31818
  Scenario: Verify API for school Report
    When the school report API is accessed with the following parameters:
      | param        | value     |
      | year         | 2015      |
      | asmtType     | SUMMATIVE |
      | stateCode    | RI        |
      | districtGuid | R0002     |
      | schoolGuid   | RE001     |
      | subject      | subject2  |
    Then the response code should be 200
    And I expect "4" entities in "subject2"

  @RALLY_US31818
  Scenario: Verify API for school Report with filters
    When the school report API is accessed with the following parameters:
      | param        | value     |
      | year         | 2015      |
      | asmtType     | SUMMATIVE |
      | stateCode    | RI        |
      | districtGuid | R0002     |
      | schoolGuid   | RE001     |
      | subject      | subject2  |
      | sex          | M         |
      | disabilities | Y         |
    Then the response code should be 200
    And I expect "2" entities in "subject2" 

  @RALLY_US31818
  Scenario: Verify API for school Report with filters
    When the school report API is accessed with the following parameters:
      | param        | value     |
      | year         | 2015      |
      | asmtType     | SUMMATIVE |
      | stateCode    | RI        |
      | districtGuid | R0002     |
      | schoolGuid   | RE001     |
      | subject      | subject2  |
      | sex          | M         |
      | disabilities | Y         |
      | englishLearner|ELL       |
    Then the response code should be 200
    And I expect "1" entities in "subject2"


  @RALLY_US36075
  Scenario: Verify API for school Report
    When the school report API is accessed with the following parameters:
      | param        | value     |
      | year         | 2014      |
      | asmtType     | SUMMATIVE |
      | stateCode    | RI        |
      | districtGuid | R0002     |
      | schoolGuid   | RE001     |
      | subject      | subject2  |
    Then the response code should be 200
    And I expect "0" entities in "subject2"
