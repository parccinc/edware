Feature: Verify API for ISR PDF

  Background:
    Given the API is set up
    And a cookie is set for user "gman"

  @RALLY_US33797
  Scenario: Verify API request for valid student
    When the individual student report PDF API is accessed with the following parameters:
      | param        | value                                    |
      | districtGuid | R0001                                    |
      | schoolGuid   | RN001                                    |
      | stateCode    | RI                                       |
      | year     	 | 2015                                     |
      | studentGuid  | EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q |
      | pdf          | gray                                     |
      | asmtType     | SUMMATIVE                                |
      | gradeCourse  | Grade 9                                       |
      | subject      | subject2                                 |
    Then the response code should be 200
    Then the response should have a content type of "application/pdf"
    Then the response content length should be larger than "20000"

  @RALLY_US33797
  Scenario: Verify API request for color pdf
    When the individual student report PDF API is accessed with the following parameters:
      | param        | value                                    |
      | districtGuid | R0001                                    |
      | schoolGuid   | RN001                                    |
      | stateCode    | RI                                       |
      | year	     | 2015                                     |
      | studentGuid  | EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q |
      | pdf          | color                                    |
      | asmtType     | SUMMATIVE                                |
      | gradeCourse  | Grade 9                                       |
      | subject      | subject2                                 |
    Then the response code should be 200
    Then the response should have a content type of "application/pdf"
    Then the response content length should be larger than "20000"

  @RALLY_US33797
  Scenario: Verify API request for invalid params (ISR)
    When the individual student report PDF API is accessed with the following parameters:
      | param        | value                                    |
      | districtGuid | R0001                                    |
      | schoolGuid   | RN001                                    |
      | stateCode    | RI                                       |
      | year	     | 2015                                     |
      | studentGuid  | EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q |
      | pdf          | gray                                     |
      | asmtType     | SUMMATIVE                                |
      | gradeCourse  | Grade 9                                       |
      | subject      | mathem                                   |
    Then the response code should be 200
    Then the response should have a content type of "text/html"

