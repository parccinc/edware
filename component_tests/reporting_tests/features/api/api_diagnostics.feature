Feature: Verify the API for Reporting for Diagnostic Assessments

  Background:
    Given the API is set up
    And a cookie is set for user "gman"

  @RALLY_US35458
  Scenario: Verify API for Diagnostic - Vocabulary Assessment
    When the school report API is accessed with the following parameters:
      | param        | value      |
      | subject      | subject2   |
      | year         | 2015       |
      | asmtType     | DIAGNOSTIC |
      | stateCode    | RI         |
      | districtGuid | R0003      |
      | schoolGuid   | RP001      |
      | view         | vocab      |
    Then the response code should be 200
    And the response body has the json fields "user_info,assessments,subjects,columns,context"
    And the response body has the expected number of fields for a diagnostic report
    And the response body has 4 students for a ela diagnostic report
    And the response body has the following data for a ela diagnostic report
      | asmt_grade  | score | student_display_name | staff_id                               | asmt_date  | student_guid                             |
      | 04          | 509   | Forst, Jacquline Z.  | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 03/08/2015 | hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ |
      | 06          | 454   | Gatti, Shane K.      | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 11/18/2014 | 6xduCZW2oSUGSLzpiFpJQQK8YSAzmBrMjAh4WEkn |
      | 06          | 319   | Mccreery, Sandy D.   | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 11/18/2014 | wUfuK1C1vpevOq8cV6oqQxNNwJV1TbzeraL8fC4v |
      | 03          | 443   | Mccreery, Zofia K.   | 5f3bcce2cac34dd38578b9d423801dc3b5caf6 | 04/01/2015 | zxITiM66QUVOcTyhRaooB0UIOWemGFP8eQE7xPSx |

  @RALLY_US35458
  Scenario: Verify API for Diagnostic - Reading Fluency Assessment
    When the school report API is accessed with the following parameters:
      | param        | value      |
      | subject      | subject2   |
      | year         | 2015       |
      | asmtType     | DIAGNOSTIC |
      | stateCode    | RI         |
      | districtGuid | R0003      |
      | schoolGuid   | RP001      |
      | view         | read_flu   |
    Then the response code should be 200
    And the response body has the json fields "user_info,assessments,subjects,columns,context"
    And the response body has the expected number of fields for a diagnostic report
    And the response body has 4 students for a ela diagnostic report
    And the response body has the following data for a ela diagnostic report
      |asmt_grade    | wpm   | wcpm|accuracy|express|passage_info                                                     |student_display_name    | staff_id                               | asmt_date  | student_guid                             |
      |04            | 37    | 21  |  57    |  1    |[{'passage_type': 'Literary'}]                                   |Forst, Jacquline Z.     | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 03/08/2015 | hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ |
      |06            | 124   | 1   |  1     |  3    |[{'passage_type': 'Informational'}, {'passage_type': 'Literary'}]|Gatti, Shane K.         | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 11/18/2014 | 6xduCZW2oSUGSLzpiFpJQQK8YSAzmBrMjAh4WEkn |
      |06            | 121   | 71  |  59    |  3    |[{'passage_type': 'Literary'}, {'passage_type': 'Literary'}]     |Mccreery, Sandy D.      | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 11/18/2014 | wUfuK1C1vpevOq8cV6oqQxNNwJV1TbzeraL8fC4v |
      |03            | 40    | 10  |  25    |  0    |[{'passage_type': 'Literary'}]                                   |Mccreery, Zofia K.      | 5f3bcce2cac34dd38578b9d423801dc3b5caf6 | 04/01/2015 | zxITiM66QUVOcTyhRaooB0UIOWemGFP8eQE7xPSx |

  @RALLY_US34001
  Scenario: Verify API for Diagnostic - ELA Overview
    When the school report API is accessed with the following parameters:
      | param        | value      |
      | subject      | subject2   |
      | year         | 2015       |
      | asmtType     | DIAGNOSTIC |
      | stateCode    | RI         |
      | districtGuid | R0003      |
      | schoolGuid   | RP001      |
      | view         | overview   |
    Then the response code should be 200
    And the response body has the json fields "user_info,assessments,subjects,columns,context"
    And the response body has the expected number of fields for a diagnostic report
    And the response body has the following data for a ela diagnostic report
      | asmt_grade | student_display_name  | staff_id                               | asmt_date  | student_guid                             | view                  |
      | 04          | Forst, Jacquline Z.  | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 03/08/2015 | hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ | Decoding              |
      | 04          | Forst, Jacquline Z.  | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 03/08/2015 | hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ | Reading Comprehension |
      | 04          | Forst, Jacquline Z.  | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 03/08/2015 | hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ | Reading Fluency       |
      | 04          | Forst, Jacquline Z.  | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 03/08/2015 | hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ | Vocabulary            |
      | 04          | Forst, Jacquline Z.  | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 03/08/2015 | hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ | Writing               |
      | 06          | Gatti, Shane K.      | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 11/18/2014 | 6xduCZW2oSUGSLzpiFpJQQK8YSAzmBrMjAh4WEkn | Decoding              |
      | 06          | Gatti, Shane K.      | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 11/18/2014 | 6xduCZW2oSUGSLzpiFpJQQK8YSAzmBrMjAh4WEkn | Reading Comprehension |
      | 06          | Gatti, Shane K.      | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 11/18/2014 | 6xduCZW2oSUGSLzpiFpJQQK8YSAzmBrMjAh4WEkn | Reading Fluency       |
      | 06          | Gatti, Shane K.      | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 11/18/2014 | 6xduCZW2oSUGSLzpiFpJQQK8YSAzmBrMjAh4WEkn | Vocabulary            |
      | 06          | Gatti, Shane K.      | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 11/18/2014 | 6xduCZW2oSUGSLzpiFpJQQK8YSAzmBrMjAh4WEkn | Writing               |
      | 06          | Mccreery, Sandy D.   | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 11/18/2014 | wUfuK1C1vpevOq8cV6oqQxNNwJV1TbzeraL8fC4v | Decoding              |
      | 06          | Mccreery, Sandy D.   | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 11/18/2014 | wUfuK1C1vpevOq8cV6oqQxNNwJV1TbzeraL8fC4v | Reading Comprehension |
      | 06          | Mccreery, Sandy D.   | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 11/18/2014 | wUfuK1C1vpevOq8cV6oqQxNNwJV1TbzeraL8fC4v | Reading Fluency       |
      | 06          | Mccreery, Sandy D.   | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 11/18/2014 | wUfuK1C1vpevOq8cV6oqQxNNwJV1TbzeraL8fC4v | Vocabulary            |
      | 06          | Mccreery, Sandy D.   | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 11/18/2014 | wUfuK1C1vpevOq8cV6oqQxNNwJV1TbzeraL8fC4v | Writing               |
      | 03          | Mccreery, Zofia K.   | 5f3bcce2cac34dd38578b9d423801dc3b5caf6 | 04/01/2015 | zxITiM66QUVOcTyhRaooB0UIOWemGFP8eQE7xPSx | Decoding              |
      | 03          | Mccreery, Zofia K.   | 5f3bcce2cac34dd38578b9d423801dc3b5caf6 | 04/01/2015 | zxITiM66QUVOcTyhRaooB0UIOWemGFP8eQE7xPSx | Reading Comprehension |
      | 03          | Mccreery, Zofia K.   | 5f3bcce2cac34dd38578b9d423801dc3b5caf6 | 04/01/2015 | zxITiM66QUVOcTyhRaooB0UIOWemGFP8eQE7xPSx | Reading Fluency       |
      | 03          | Mccreery, Zofia K.   | 5f3bcce2cac34dd38578b9d423801dc3b5caf6 | 04/01/2015 | zxITiM66QUVOcTyhRaooB0UIOWemGFP8eQE7xPSx | Vocabulary            |
      | 03          | Mccreery, Zofia K.   | 5f3bcce2cac34dd38578b9d423801dc3b5caf6 | 04/01/2015 | zxITiM66QUVOcTyhRaooB0UIOWemGFP8eQE7xPSx | Writing               |

  @RALLY_US35458
  Scenario: Verify API for Diagnostic - Reading Comprehension Assessment
    When the school report API is accessed with the following parameters:
      | param        | value      |
      | subject      | subject2   |
      | year         | 2015       |
      | asmtType     | DIAGNOSTIC |
      | stateCode    | RI         |
      | districtGuid | R0003      |
      | schoolGuid   | RP001      |
      | view         | comp       |
    Then the response code should be 200
    And the response body has the json fields "user_info,assessments,subjects,columns,context"
    And the response body has the expected number of fields for a ela diagnostic report
    And the response body has 4 students for a ela diagnostic report
    And row 1 has the following reading comprehension info
      |asmt_grade    | score | student_display_name    | staff_id                               | asmt_date  |irl|passage_type    |passage_rmm|
      |04            | 509   | Forst, Jacquline Z.     | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 03/08/2015 |2.6|Literary        |8.0        |
    And row 2 has the following reading comprehension info
      |asmt_grade    | score | student_display_name    | staff_id                               | asmt_date  |irl|passage_type    |passage_rmm|passage_type    |passage_rmm|
      |06            | 454   | Gatti, Shane K.         | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 11/18/2014 |2.8|Informational   |3.9        |Literary        |3.5        |
    And row 3 has the following reading comprehension info
      |asmt_grade    | score | student_display_name    | staff_id                               | asmt_date  |irl|passage_type    |passage_rmm|passage_type    |passage_rmm|
      |06            | 319   | Mccreery, Sandy D.      | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 11/18/2014 |9.3|Literary        |1.5        |Literary        |2.5        |
    And row 4 has the following reading comprehension info
      |asmt_grade    | score | student_display_name    | staff_id                               | asmt_date  |irl|passage_type    |passage_rmm|
      |03            | 443   | Mccreery, Zofia K.      | 5f3bcce2cac34dd38578b9d423801dc3b5caf6 | 04/01/2015 |7.4|Literary        |3.8        |

  @RALLY_US35458
  Scenario: Verify API for Diagnostic - Math Grade Level Assessment
    When the school report API is accessed with the following parameters:
      | param        | value      |
      | subject      | subject1   |
      | year         | 2015       |
      | asmtType     | DIAGNOSTIC |
      | stateCode    | RI         |
      | districtGuid | R0001      |
      | schoolGuid   | RN001      |
      | view         | grade_level|
    Then the response code should be 200
    And the response body has the json fields "user_info,assessments,subjects,columns,context"
    And the response body has the expected number of fields for a math diagnostic report
    And the response body has 3 students for a math diagnostic report
    And the response body has the following data for a math diagnostic report
      | student_display_name   | asmt_grade  | asmt_date  | cluster_grade_level | staff_id                               | clusters |student_guid                            |
      | Brule, Steve J.        | 05          | 04/01/2015 | 5                   | dcdaa08b9b9046beb3e77419481c860bc6598e |          |5336b7258eb545b3ae7178aeb0cfd72428a36fdb|
      | Mccreery, Jacquline B. | 08          | 03/08/2015 | 7                   | 5f3bcce2cac34dd38578b9d423801dc3b5caf6 |          |SBFMQrkvsLNLQWnEXVVeN6ZMQXqemPhTY44hgO2M|
      | Perla, Barbar O.       | 05          | 04/01/2015 | 5                   | dcdaa08b9b9046beb3e77419481c860bc6598e |          |OIeWicvTvk5kcbEc0qj7vPo2nh3Vr8XS9e2Vz57b|
    And the response body has the following ordered cluster data:
      | student_number | code_cluster | prob_cluster |
      | 0              | 5.NBT.A      | 0.27         |
      | 0              | 5.NF.B       | 0.0          |
      | 0              | 5.NF.C       | 0.74         |
      | 0              | 5.OA.D       | 0.05         |
      | 0              | 5.OA.A       | 0.67         |
      | 1              | 7.OA.A       | 0.53         |
      | 1              | 7.OA.B       | 0.33         |
      | 1              | 7.OA.C       | 0.89         |
      | 1              | 7.OA.D       | 0.71         |
      | 1              | 7.NF.E       | 0.6          |
      | 1              | 7.NF.F       | 0.33         |
      | 2              | 5.NBT.A      | 0.27         |
      | 2              | 5.NF.B       | 0.0          |
      | 2              | 5.NF.C       | 0.74         |
      | 2              | 5.OA.D       | 0.05         |
      | 2              | 5.OA.A       | 0.67         |

  @RALLY_US35461
  Scenario: Verify API for Diagnostic - Decoding Assessment
    When the school report API is accessed with the following parameters:
      | param        | value           |
      | subject      | subject2        |
      | year         | 2015            |
      | gradeCourse  | 03              |
      | asmtType     | DIAGNOSTIC      |
      | stateCode    | RI              |
      | districtGuid | R0003           |
      | schoolGuid   | RP001           |
      | view         | decod           |
    Then the response code should be 200
    And the response body has the json fields "user_info,assessments,subjects,columns,context"
    And the response body has the expected number of fields for a diagnostic report
    And the response body has 4 students for a ela diagnostic report
    And the response body has the following data for a ela diagnostic report
      |student_display_name |student_guid                             |asmt_grade   |staff_id                               | asmt_date  |p1_cvc|p2_blend|p3_cvow|p4_ccons|p5_msyl1|p6_msyl2|
      |Forst, Jacquline Z.  |hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ | 04          |3c6b9327b0484f5f84741b204a6c9f02208bc1 | 03/08/2015 | 0.87 | 0.45   | 0.22  | 0.05   | 0.54   | 0.82   |
      |Gatti, Shane K.      |6xduCZW2oSUGSLzpiFpJQQK8YSAzmBrMjAh4WEkn | 06          |3c6b9327b0484f5f84741b204a6c9f02208bc1 | 11/18/2014 | 0.51 | 0.92   | 0.77  | 0.46   | 0.82   | 0.63   |
      |Mccreery, Sandy D.   |wUfuK1C1vpevOq8cV6oqQxNNwJV1TbzeraL8fC4v | 06          |3c6b9327b0484f5f84741b204a6c9f02208bc1 | 11/18/2014 | 0.27 | 0.52   | 0.92  | 0.81   | 0.65   | 0.49   |
      |Mccreery, Zofia K.   |zxITiM66QUVOcTyhRaooB0UIOWemGFP8eQE7xPSx | 03          |5f3bcce2cac34dd38578b9d423801dc3b5caf6 | 04/01/2015 | 0.48 | 0.63   | 0.98  | 0.76   | 0.25   | 0.04   |


  @RALLY_US35550
  Scenario: Verify API for Diagnostic - ELA ISR
    When the individual student report API is accessed with the following parameters:
      | param        | value                                          |
      | subject      | subject2                                       |
      | year         | 2015                                           |
      | gradeCourse  | 03                                             |
      | asmtType     | DIAGNOSTIC                                     |
      | stateCode    | RI                                             |
      | districtGuid | R0003                                          |
      | schoolGuid   | RP001                                          |
      | studentGuid  | hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ       |
    Then the response code should be 200
    And the response body has the json fields "user_info,assessments,subjects,columns,context"
    And the response body has the expected number of fields for a ela diagnostic report
      And the ela student report has the following comp report info
      |asmt_grade    | score | student_display_name    | staff_id                               | asmt_date  |irl|passage_type    |passage_rmm|
      |04            | 509   | Forst, Jacquline Z.     | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 03/08/2015 |2.6|Literary        |8.0        |
      And the ela student report has the following decod report info
      |student_display_name |student_guid                             |asmt_grade   |staff_id                               | asmt_date  |p1_cvc|p2_blend|p3_cvow|p4_ccons|p5_msyl1|p6_msyl2|
      |Forst, Jacquline Z.  |hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ | 04          |3c6b9327b0484f5f84741b204a6c9f02208bc1 | 03/08/2015 | 0.87 | 0.45   | 0.22  | 0.05   | 0.54   | 0.82   |
      And the ela student report has the following read_flu report info
      |asmt_grade    | wpm   | wcpm|accuracy|express|passage_info                                                     |student_display_name    | staff_id                               | asmt_date  |
      |04            | 37    | 21  |  57    |  1    |[{'passage_type': 'Literary'}]                                   |Forst, Jacquline Z.     | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 03/08/2015 |
      And the ela student report has the following vocab report info
      | asmt_grade  | score | student_display_name | staff_id                               | asmt_date  | student_guid                         |
      | 04          | 509   | Forst, Jacquline Z.  | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 03/08/2015 | hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ |
      And the ela student report has the following writing report info
      | asmt_grade  | score_lit |score_nar|score_res| student_display_name | staff_id                               | asmt_date  | student_guid                         |
      | 04          | 3         |    2    |    2    | Forst, Jacquline Z.  | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 03/08/2015 | hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ |
  
  @RALLY_US35258
  Scenario: Verify API for Diagnostic - Locator Assessment
    When the school report API is accessed with the following parameters:
      | param        | value      |
      | subject      | subject1   |
      | year         | 2015       |
      | asmtType     | DIAGNOSTIC |
      | stateCode    | RI         |
      | districtGuid | R0001      |
      | schoolGuid   | RN001      |
      | view         | locator    |
    Then the response code should be 200
    And the response body has the json fields "user_info,assessments,subjects,columns,context"
    And the response body has the expected number of fields for a diagnostic report
    And the response body has 3 students for a math diagnostic report
    And the response body has the following data for a math diagnostic report
      |asmt_grade    | score | student_display_name    | staff_id                               | asmt_date  | asmt_date_ts    | student_guid                         	  | suggested_grade_level |
      |05            | 184   | Brule, Steve J.         | dcdaa08b9b9046beb3e77419481c860bc6598e | 04/01/2015 | 1427846400.0    | 5336b7258eb545b3ae7178aeb0cfd72428a36fdb | 4                     |
      |08            | 198   | Mccreery, Jacquline B.  | 5f3bcce2cac34dd38578b9d423801dc3b5caf6 | 03/08/2015 | 1425772800.0    | SBFMQrkvsLNLQWnEXVVeN6ZMQXqemPhTY44hgO2M | 7                     |
      |05            | 184   | Perla, Barbar O.        | dcdaa08b9b9046beb3e77419481c860bc6598e | 04/01/2015 | 1427846400.0    | OIeWicvTvk5kcbEc0qj7vPo2nh3Vr8XS9e2Vz57b | 4                     |

  @RALLY_US35468
  Scenario: Verify API for Diagnostic - Math Progression Assessment
    When the school report API is accessed with the following parameters:
      | param        | value       |
      | subject      | subject1    |
      | year         | 2015        |
      | asmtType     | DIAGNOSTIC  |
      | stateCode    | RI          |
      | districtGuid | R0001       |
      | schoolGuid   | RN001       |
      | view         | progress    |
    Then the response code should be 200
    And the response body has the json fields "user_info,assessments,subjects,columns,context"
    And the response body has the expected number of fields for a math diagnostic report
    And the response body has 3 students for a math diagnostic report
    And the response body has the following data for a math diagnostic report
      | student_display_name   | asmt_grade  | asmt_date  | staff_id                               | progression | clusters | student_guid							   |
      | Brule, Steve J.        | 05          | 04/01/2015 | dcdaa08b9b9046beb3e77419481c860bc6598e | NF          |          | 5336b7258eb545b3ae7178aeb0cfd72428a36fdb |
      | Mccreery, Jacquline B. | 08          | 03/08/2015 | 5f3bcce2cac34dd38578b9d423801dc3b5caf6 | NBT         |          | SBFMQrkvsLNLQWnEXVVeN6ZMQXqemPhTY44hgO2M |
      | Perla, Barbar O.       | 05          | 04/01/2015 | dcdaa08b9b9046beb3e77419481c860bc6598e | NF          |          | OIeWicvTvk5kcbEc0qj7vPo2nh3Vr8XS9e2Vz57b |
    And the response body has the following ordered cluster data:
      | student_number | code_cluster | prob_cluster |
      | 0              | 5.NBT.A      | 0.27         |
      | 0              | 5.NF.B       | 0.0          |
      | 0              | 5.NF.C       | 0.74         |
      | 0              | 5.OA.D       | 0.05         |
      | 0              | 5.OA.A       | 0.67         |
      | 1              | 7.OA.A       | 0.53         |
      | 1              | 7.OA.B       | 0.33         |
      | 1              | 7.OA.C       | 0.89         |
      | 1              | 7.OA.D       | 0.71         |
      | 1              | 7.NF.E       | 0.6          |
      | 1              | 7.NF.F       | 0.33         |
      | 2              | 5.NBT.A      | 0.27         |
      | 2              | 5.NF.B       | 0.0          |
      | 2              | 5.NF.C       | 0.74         |
      | 2              | 5.OA.D       | 0.05         |
      | 2              | 5.OA.A       | 0.67         |

  @RALLY_US35468
  Scenario: Verify API for Diagnostic - Math Cluster Assessment
    When the school report API is accessed with the following parameters:
      | param        | value       |
      | subject      | subject1    |
      | year         | 2015        |
      | asmtType     | DIAGNOSTIC  |
      | stateCode    | RI          |
      | districtGuid | R0001       |
      | schoolGuid   | RN001       |
      | view         | cluster     |
    Then the response code should be 200
    And the response body has the json fields "user_info,assessments,subjects,columns,context"
    And the response body has the expected number of fields for a math diagnostic report
    And the response body has 3 students for a math diagnostic report
    And the response body has the following data for a math diagnostic report
      | student_display_name   | asmt_grade  | asmt_date  | staff_id                               | clusters | student_guid                             |
      | Brule, Steve J.        | 05          | 04/01/2015 | dcdaa08b9b9046beb3e77419481c860bc6598e |          | 5336b7258eb545b3ae7178aeb0cfd72428a36fdb |
      | Mccreery, Jacquline B. | 08          | 03/08/2015 | 5f3bcce2cac34dd38578b9d423801dc3b5caf6 |          | SBFMQrkvsLNLQWnEXVVeN6ZMQXqemPhTY44hgO2M |
      | Perla, Barbar O.       | 05          | 04/01/2015 | dcdaa08b9b9046beb3e77419481c860bc6598e |          | OIeWicvTvk5kcbEc0qj7vPo2nh3Vr8XS9e2Vz57b |
    And the response body has the following ordered cluster data:
      | student_number | code_cluster | prob_cluster |
      | 0              | 5.NBT.A      | 0.27         |
      | 1              | 7.OA.A       | 0.53         |
      | 2              | 5.NBT.A      | 0.27         |


  @RALLY_US35462
  Scenario: Verify API for Diagnostic - ELA Reader Motivation Survey Assessment
    When the school report API is accessed with the following parameters:
      | param        | value      |
      | subject      | subject2   |
      | year         | 2015       |
      | asmtType     | DIAGNOSTIC |
      | stateCode    | RI         |
      | districtGuid | R0003      |
      | schoolGuid   | RP001      |
      | view         |reader_motiv |
    Then the response code should be 200
    And the response body has the json fields "user_info,assessments,subjects,columns,context"
    And the response body has the expected number of fields for a diagnostic report
    And the response body has 4 students for a ela diagnostic report
    And the response body has the following data for a ela diagnostic report
      | asmt_grade  | student_display_name | staff_id                               | asmt_date  | student_guid                             | responses                                                                                                                                                                             |
      | 04          | Forst, Jacquline Z.  | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 03/08/2015 | hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ |[['C'], ['D'], ['D'], ['B'], ['C'], ['C'], ['B', 'C', 'D'], ['A', 'C'], ['B'], ['C'], ['C'], ['D'], ['A'], ['A', 'B'], ['A'], ['A'], ['C'], ['C'], ['A']]                              |
      | 06          | Gatti, Shane K.      | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 11/18/2014 | 6xduCZW2oSUGSLzpiFpJQQK8YSAzmBrMjAh4WEkn |[['D'], ['A'], ['B'], ['D'], ['A'], ['D'], ['C'], ['C'], ['A'], ['D'], ['D'], ['C'], ['A'], ['D'], ['C'], ['C'], ['A'], ['B'], ['C', 'D']]                                             |
      | 06          | Mccreery, Sandy D.   | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 11/18/2014 | wUfuK1C1vpevOq8cV6oqQxNNwJV1TbzeraL8fC4v |[['B'], ['B'], ['C'], ['D'], ['A'], ['A'], ['A'], ['B', 'D'], ['B'], ['D'], ['D'], ['A'], ['B'], ['A'], ['A'], ['C'], ['D'], ['D'], ['C']]                                             |
      | 03          | Mccreery, Zofia K.   | 5f3bcce2cac34dd38578b9d423801dc3b5caf6 | 04/01/2015 | zxITiM66QUVOcTyhRaooB0UIOWemGFP8eQE7xPSx |[['A'], ['C'], ['D'], ['D'], ['A'], ['C'], ['B'], ['C'], ['D'], ['D'], ['A'], ['C'], ['D'], ['C'], ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'], ['B'], ['D'], ['B'], ['A', 'D']]|

    @RALLY_US35470
  Scenario: Verify API for Diagnostic - Fluency Assessment
    When the school report API is accessed with the following parameters:
      | param        | value      |
      | subject      | subject1   |
      | year         | 2015       |
      | asmtType     | DIAGNOSTIC |
      | stateCode    | RI         |
      | districtGuid | R0001      |
      | schoolGuid   | RN001      |
      | view         | flu        |
    Then the response code should be 200
    And the response body has the json fields "user_info,assessments,subjects,columns,context"
    And the response body has the expected number of fields for a diagnostic report
    And the response body has 5 students for a math diagnostic report
    And the response body has the following data for a math diagnostic report
      |asmt_grade    | student_display_name    | staff_id                               | asmt_date  | skill                                                 | items_correct  | total_time_per_skill     | percent_correct   | student_guid                             | total_num_skill_items |
      |05            | Brule, Steve J.         | dcdaa08b9b9046beb3e77419481c860bc6598e | 04/01/2015 | Add & Subtract within 1,000 (Gr. 3)                   | 3              | 00:03:46                 | 100.0             | 5336b7258eb545b3ae7178aeb0cfd72428a36fdb | 3                     |
      |05            | Brule, Steve J.         | dcdaa08b9b9046beb3e77419481c860bc6598e | 04/01/2015 | Multiply with Multi-digit Decimal Numbers (Gr. 6)     | 1              | 00:02:51                 | 33.33             | 5336b7258eb545b3ae7178aeb0cfd72428a36fdb | 3                     |
      |08            | Mccreery, Jacquline B.  | 5f3bcce2cac34dd38578b9d423801dc3b5caf6 | 03/08/2015 | Add/Subtract with Multi-digit Decimal Numbers (Gr. 6) | 4              | 00:11:00                 | 100.0             | SBFMQrkvsLNLQWnEXVVeN6ZMQXqemPhTY44hgO2M | 4                     |
      |05            | Perla, Barbar O.        | dcdaa08b9b9046beb3e77419481c860bc6598e | 04/01/2015 | Add & Subtract within 1,000 (Gr. 3)                   | 3              | 00:03:46                 | 100.0             | OIeWicvTvk5kcbEc0qj7vPo2nh3Vr8XS9e2Vz57b | 3                     |
      |05            | Perla, Barbar O.        | dcdaa08b9b9046beb3e77419481c860bc6598e | 04/01/2015 | Multiply with Multi-digit Decimal Numbers (Gr. 6)     | 1              | 00:02:51                 | 33.33             | OIeWicvTvk5kcbEc0qj7vPo2nh3Vr8XS9e2Vz57b | 3                     |

  @RALLY_US35257
  Scenario: Verify API for Diagnostic - Math ISR
    When the individual student report API is accessed with the following parameters:
      | param        | value                                          |
      | subject      | subject1                                       |
      | year         | 2015                                           |
      | asmtType     | DIAGNOSTIC                                     |
      | stateCode    | RI                                             |
      | districtGuid | R0001                                          |
      | schoolGuid   | RN001                                          |
      | studentGuid  | 5336b7258eb545b3ae7178aeb0cfd72428a36fdb       |
      | gradeCourse  | 05                                             |
    Then the response code should be 200
    And the response body has the json fields "user_info,assessments,subjects,columns,context"
    And the response body has the expected number of fields for a diagnostic report
    And the math student report has the following locator report info
      |asmt_grade    | score | student_display_name    | staff_id                               | asmt_date  |        student_guid                    	  | suggested_grade_level |
      |05            | 184   | Brule, Steve J.         | dcdaa08b9b9046beb3e77419481c860bc6598e | 04/01/2015 |   5336b7258eb545b3ae7178aeb0cfd72428a36fdb | 4                     |
    And the math student report has the following progress report info
      | student_display_name   | asmt_grade  | asmt_date  | staff_id                               | progression | student_guid							    | code_cluster|prob_cluster| code_cluster|prob_cluster| code_cluster|prob_cluster| code_cluster|prob_cluster| code_cluster|prob_cluster|
      | Brule, Steve J.        | 05          | 04/01/2015 | dcdaa08b9b9046beb3e77419481c860bc6598e | NF          | 5336b7258eb545b3ae7178aeb0cfd72428a36fdb |5.NBT.A      | 0.27       | 5.NF.B      |    0.0     | 5.NF.C      | 0.74       |  5.OA.D     | 0.05       |   5.OA.A    | 0.67       |
    And the math student report has the following cluster report info
      | student_display_name   | asmt_grade  | asmt_date  | staff_id                               | student_guid                             |code_cluster|prob_cluster|
      | Brule, Steve J.        | 05          | 04/01/2015 | dcdaa08b9b9046beb3e77419481c860bc6598e | 5336b7258eb545b3ae7178aeb0cfd72428a36fdb |5.NBT.A     |0.27        |
    And the math student report has the following flu report info
      |asmt_grade    | student_display_name    | staff_id                               | asmt_date  | skill                                                 | items_correct  | total_time_per_skill     | percent_correct   | student_guid                             | total_num_skill_items |
      |05            | Brule, Steve J.         | dcdaa08b9b9046beb3e77419481c860bc6598e | 04/01/2015 | Add & Subtract within 1,000 (Gr. 3)                   | 3              | 00:03:46                 | 100.0             | 5336b7258eb545b3ae7178aeb0cfd72428a36fdb | 3                     |
      |05            | Brule, Steve J.         | dcdaa08b9b9046beb3e77419481c860bc6598e | 04/01/2015 | Multiply with Multi-digit Decimal Numbers (Gr. 6)     | 1              | 00:02:51                 | 33.33             | 5336b7258eb545b3ae7178aeb0cfd72428a36fdb | 3                     |

  @RALLY_US36075
  Scenario: Verify API for Diagnostic - ELA Overview for no results
    When the school report API is accessed with the following parameters:
      | param        | value      |
      | subject      | subject2   |
      | year         | 2014       |
      | asmtType     | DIAGNOSTIC |
      | stateCode    | RI         |
      | districtGuid | R0003      |
      | schoolGuid   | RP001      |
      | view         | overview   |
    Then the response code should be 200
    And the response body has 0 students for a ela diagnostic report
