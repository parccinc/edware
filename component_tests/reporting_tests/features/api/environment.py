from unittest import TestCase
from components_tests.frontend.common import read_config
from components_tests.flaky_tests_handler.flaky_tests_handler import handle_failed_feature


def before_all(context):
    read_config(context)
    context.tc = TestCase()


def after_feature(context, feature):
    if feature.status == 'failed':
        rerun_mode = context.config.userdata.getbool('rerun_mode')
        handle_failed_feature(feature, rerun_mode)
