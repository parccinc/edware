Feature: Verify the API for Reporting

  Background:
    Given the API is set up
    And a cookie is set for user "gman"

  @RALLY_US31194
  Scenario: Verify API for Student Roster Report
    When the student roster report API is accessed with the following parameters:
      | param        | value     |
      | subject      | subject2  |
      | year         | 2015      |
      | gradeCourse  | Grade 9   |
      | asmtType     | SUMMATIVE |
      | stateCode    | RI        |
      | districtGuid | R0003     |
      | schoolGuid   | RP001     |
      | view         | scores    |
    Then the response code should be 200
    And the response body has the json fields "assessments,subjects,context,columns"
    And the response body has the expected number of fields for a student roster report

  @RALLY_US32561
  Scenario: Verify API for Breadcrumbs on Student Roster Report
    When the student roster report API is accessed with the following parameters:
      | param        | value     |
      | subject      | subject2  |
      | year         | 2015      |
      | gradeCourse  | Grade 3   |
      | asmtType     | SUMMATIVE |
      | stateCode    | RI        |
      | districtGuid | R0003     |
      | schoolGuid   | RP003     |
      | view         | scores    |
    Then the response code should be 200
    And the response body has the json fields "assessments,subjects,context,columns,user_info"
    And the response body has 5 items for breadcrumbs on the student roster report
    And the first breadcrumb type is home and display name is Home
    And the second breadcrumb type is state and display name is Rhode Island
    And the third breadcrumb type is district and display name is Providence
    And the fourth breadcrumb type is school and display name is Hope High School
    And the fifth breadcrumb type is grade and display name is Grade 3

    @RALLY_US34307
    Scenario: Verify API for Breadcrumbs on Student Roster Report
      When the student roster report API is accessed with the following parameters:
        | param        | value     |
        | subject      | subject1  |
        | year         | 2015      |
        | gradeCourse  | Geometry  |
        | asmtType     | SUMMATIVE |
        | stateCode    | RI        |
        | districtGuid | R0001     |
        | schoolGuid   | RN001     |
        | view         | scores    |
      Then the response code should be 200
      And the response body has the json fields "assessments,subjects,context,columns,user_info"
      And the response body has 5 items for breadcrumbs on the student roster report
      And the first breadcrumb type is home and display name is Home
      And the second breadcrumb type is state and display name is Rhode Island
      And the third breadcrumb type is district and display name is Newport
      And the fourth breadcrumb type is school and display name is Rogers High School
      And the fifth breadcrumb type is grade and display name is Geometry

    @RALLY_DE1057
    Scenario: Verify API for Correct number of responses per item
      When the student roster report API is accessed with the following parameters:
        | param        | value        |
        | subject      | subject2     |
        | year         | 2015         |
        | gradeCourse  | Grade 11     |
        | asmtType     | SUMMATIVE    |
        | stateCode    | RI           |
        | districtGuid | R0003        |
        | schoolGuid   | RP001        |
        | view         | itemAnalysis |
      Then the response code should be 200
      And Item 14 should have 6 responses
      And Item 11 should have 6 responses


  @RALLY_US31839
  Scenario: Verify API request for missing params (student roster report)
    When the student roster report API is accessed with the following parameters:
      | param        | value    |
      | subject      | subject2 |
      | year         | 2015     |
      | gradeCourse  | Grade 10 |
      | stateCode    | RI       |
      | districtGuid | R0003    |
      | schoolGuid   | RP001    |
    Then the response code should be 412

  @RALLY_US34137
  Scenario: Verify API for Student Roster Report with Filters
    When the student roster report API is accessed with the following parameters:
      | param        | value        |
      | subject      | subject1     |
      | year         | 2015         |
      | gradeCourse  | Algebra I    |
      | asmtType     | SUMMATIVE    |
      | stateCode    | RI           |
      | districtGuid | R0001        |
      | schoolGuid   | RN001        |
      | view         | itemAnalysis |
      | sex          | F            |
    Then the response code should be 200
    Then I expect "4" entities in "subject1"

  @RALLY_US34137
  Scenario: Verify API for Student Roster Report with Filters multi values
    When the student roster report API is accessed with the following parameters:
      | param          | value        |
      | subject        | subject2     |
      | year           | 2015         |
      | gradeCourse    | Grade 9      |
      | asmtType       | SUMMATIVE    |
      | stateCode      | RI           |
      | districtGuid   | R0003        |
      | schoolGuid     | RP001        |
      | view           | itemAnalysis |
      | sex            | M            |
      | englishLearner | ELL          |
      | englishLearner | LEP          |
    Then the response code should be 200
    Then I expect "2" entities in "subject2"

  @RALLY_US34137
  Scenario: Verify API for Student Roster Report with Filters multi values
    When the student roster report API is accessed with the following parameters:
      | param          | value        |
      | subject        | subject2     |
      | year           | 2015         |
      | gradeCourse    | Grade 9      |
      | asmtType       | SUMMATIVE    |
      | stateCode      | RI           |
      | districtGuid   | R0003        |
      | schoolGuid     | RP001        |
      | view           | itemAnalysis |
      | sex            | M            |
      | englishLearner | ELL          |
      | englishLearner | LEP          |
      | disabilities   | N            |
    Then the response code should be 200
    Then I expect "1" entities in "subject2"

  @RALLY_US34137
  Scenario: Verify API for Student Roster Report with Filters with no results
    When the student roster report API is accessed with the following parameters:
      | param        | value        |
      | subject      | subject2     |
      | year         | 2015         |
      | gradeCourse  | Grade 9      |
      | asmtType     | SUMMATIVE    |
      | stateCode    | RI           |
      | districtGuid | R0003        |
      | schoolGuid   | RP001        |
      | view         | itemAnalysis |
      | ethnicity    | 01           |
    Then the response code should be 200
    And the response body has no "assessments" field

  @RALLY_US34137
  @RALLY_US34307
  Scenario: Verify API for Student Roster Report with Filters
    When the student roster report API is accessed with the following parameters:
      | param        | value     |
      | subject      | subject1  |
      | year         | 2015      |
      | gradeCourse  | Algebra I |
      | asmtType     | SUMMATIVE |
      | stateCode    | RI        |
      | districtGuid | R0001     |
      | schoolGuid   | RN001     |
      | view         | scores    |
      | sex          | M         |
    Then the response code should be 200
    Then I expect "8" entities in "subject1"

  @RALLY_US34137
  Scenario: Verify API for Student Roster Report with Filters
    When the student roster report API is accessed with the following parameters:
      | param        | value     |
      | subject      | subject2  |
      | year         | 2015      |
      | gradeCourse  | Grade 9   |
      | asmtType     | SUMMATIVE |
      | stateCode    | RI        |
      | districtGuid | R0003     |
      | schoolGuid   | RP003     |
      | view         | scores    |
    Then the response code should be 200
    Then I expect "3" entities in "subject2"
    Then in summary row, I expect "19" sum_read_scale_score for entity "RP003" in subject "subject2"
    Then in summary row, I expect "4" entities in "subject2"
    Then in summary row, I expect "0" entities in "subject1"

    @RALLY_US34307
    Scenario: Verify API for Student Roster Report with Courses
      When the student roster report API is accessed with the following parameters:
        | param        | value     |
        | subject      | subject1  |
        | year         | 2015      |
        | gradeCourse  | Algebra I |
        | asmtType     | SUMMATIVE |
        | stateCode    | RI        |
        | districtGuid | R0001     |
        | schoolGuid   | RN001     |
        | view         | scores    |
      Then the response code should be 200
      And the response body has the json fields "assessments,subjects,context,columns"
      And the response body has the expected number of fields for a student roster report
      And the fifth breadcrumb type is grade and display name is Algebra I
      And the grade breadcrumb isCourse field has value true

  @RALLY_US36075
  Scenario: Verify API for Student Roster Report Item analysis view with no results
    When the student roster report API is accessed with the following parameters:
      | param        | value        |
      | subject      | subject2     |
      | year         | 2015         |
      | gradeCourse  | Grade 8      |
      | asmtType     | SUMMATIVE    |
      | stateCode    | RI           |
      | districtGuid | R0003        |
      | schoolGuid   | RP001        |
      | view         | itemAnalysis |
    Then the response code should be 200
    And the response body has no "assessments" field

  @RALLY_US36075
  Scenario: Verify API for Student Roster Report scores view with no results
    When the student roster report API is accessed with the following parameters:
      | param        | value        |
      | subject      | subject2     |
      | year         | 2014         |
      | gradeCourse  | Grade 10     |
      | asmtType     | SUMMATIVE    |
      | stateCode    | RI           |
      | districtGuid | R0003        |
      | schoolGuid   | RP001        |
      | view         | scores       |
    Then the response code should be 200
    And the response body has no "assessments" field
