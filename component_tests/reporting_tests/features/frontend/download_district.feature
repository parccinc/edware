@browser
Feature: District comparing populations report

  Background: Open a firefox browser and open the webpage
    # Given an open browser
    When I open the cpop report with the following parameters
      | param        | value       |
      | stateCode    | RI          |
      | districtGuid | R0003       |
      | year         | 2015        |
      | gradeCourse  | Grade 11    |
      | subject      | subject2    |
      | asmtType     | SUMMATIVE   |
      | view         | performance |
    Then I am redirected to the login page
    When I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 11th Grade   |
        | subject      | ELA/L        |
    When I am redirected to the cpop district report

    Scenario: Verify CSV download for Comparing Populations District Report - ELA
      When I click on the download link
      And I click on the download csv link
      Then the filename matches the format for the "cpop district performance" report
      And I see the following CSV header information:
        | key            | value                            |
        | Report Name    | Providence District              |
        | Date/Time      | %m-%d-%Y %H:%M:%S                |
        | Institution    | Home > Rhode Island > Providence |
        | Academic Year  | 2014 - 2015                      |
        | Subject        | ELA/L                            |
        | Grade/Course   | Grade 11                         |
        | Result         | Overall Performance              |
        | View           | Performance                      |
        | Filter Options |                                  |
        | Total Count    | 3                                |
        | Sort By        | SCHOOL                           |
      And I see the following CSV main information:
        | SCHOOL                     | STUDENTS | PERFORMANCE DISTRIBUTION                                                 | >= LEVEL 4 | AVG OVERALL | READING/WRITING              |
        | DISTRICT AVG - Providence  | 11       | Level 1 - 18%, Level 2 - 9%, Level 3 - 18%, Level 4 - 27%, Level 5 - 28% | 55%        | 278         | Reading 34/50, Writing 33/60 |
        | Alvarez High School        | 4        | Level 1 - 0%, Level 2 - 0%, Level 3 - 25%, Level 4 - 25%, Level 5 - 50%  | 75%        | 362         | Reading 44/50, Writing 43/60 |
        | Hope High School           | 3        | Level 1 - 0%, Level 2 - 0%, Level 3 - 33%, Level 4 - 33%, Level 5 - 34%  | 67%        | 346         | Reading 43/50, Writing 42/60 |
        | Mount Pleasant High School | 4        | Level 1 - 50%, Level 2 - 25%, Level 3 - 0%, Level 4 - 25%, Level 5 - 0%  | 25%        | 143         | Reading 17/50, Writing 18/60 |

    Scenario: Verify CSV download for Comparing Populations District Report - MATH Course
      Then I verify that I'm on the performance page
      When I open the cpop report with the following parameters:
        | param        | value       |
        | stateCode    | RI          |
        | districtGuid | R0001       |
        | year         | 2015        |
        | gradeCourse  | Algebra+I   |
        | asmtType     | SUMMATIVE   |
        | subject      | subject1    |
        | view         | performance |
      Then I wait for the grid to reload
      When I click on the download link
      And I click on the download csv link
      Then the filename matches the format for the "cpop district performance" report
      And I see the following CSV header information:
        | key            | value                         |
        | Report Name    | Newport District              |
        | Date/Time      | %m-%d-%Y %H:%M:%S             |
        | Institution    | Home > Rhode Island > Newport |
        | Academic Year  | 2014 - 2015                   |
        | Subject        | Mathematics                   |
        | Grade/Course   | Algebra I                     |
        | Result         | Overall Performance           |
        | View           | Performance                   |
        | Filter Options |                               |
        | Total Count    | 1                             |
        | Sort By        | SCHOOL                        |
      And I see the following CSV main information:
        | SCHOOL                 | STUDENTS | PERFORMANCE DISTRIBUTION                                                 | >= LEVEL 4 | AVG SCORE |
        | DISTRICT AVG - Newport | 7        | Level 1 - 29%, Level 2 - 14%, Level 3 - 14%, Level 4 - 43%, Level 5 - 0% | 43%        | 232       |
        | Rogers High School     | 7        | Level 1 - 29%, Level 2 - 14%, Level 3 - 14%, Level 4 - 43%, Level 5 - 0% | 43%        | 232       |

    Scenario: Verify CSV download for Comparing Populations District Report - ELA - sorting
      When I click on header grid column number 2
      And I click on the download link
      And I click on the download csv link
      Then the filename matches the format for the "cpop district performance" report
      And I see the following CSV header information:
        | key            | value                            |
        | Report Name    | Providence District              |
        | Date/Time      | %m-%d-%Y %H:%M:%S                |
        | Institution    | Home > Rhode Island > Providence |
        | Academic Year  | 2014 - 2015                      |
        | Subject        | ELA/L                            |
        | Grade/Course   | Grade 11                         |
        | Result         | Overall Performance              |
        | View           | Performance                      |
        | Filter Options |                                  |
        | Total Count    | 3                                |
        | Sort By        | STUDENTS                         |
      And I see the following CSV main information:
        | SCHOOL                     | STUDENTS | PERFORMANCE DISTRIBUTION                                                 | >= LEVEL 4 | AVG OVERALL | READING/WRITING              |
        | DISTRICT AVG - Providence  | 11       | Level 1 - 18%, Level 2 - 9%, Level 3 - 18%, Level 4 - 27%, Level 5 - 28% | 55%        | 278         | Reading 34/50, Writing 33/60 |
        | Hope High School           | 3        | Level 1 - 0%, Level 2 - 0%, Level 3 - 33%, Level 4 - 33%, Level 5 - 34%  | 67%        | 346         | Reading 43/50, Writing 42/60 |
        | Alvarez High School        | 4        | Level 1 - 0%, Level 2 - 0%, Level 3 - 25%, Level 4 - 25%, Level 5 - 50%  | 75%        | 362         | Reading 44/50, Writing 43/60 |
        | Mount Pleasant High School | 4        | Level 1 - 50%, Level 2 - 25%, Level 3 - 0%, Level 4 - 25%, Level 5 - 0%  | 25%        | 143         | Reading 17/50, Writing 18/60 |

    Scenario: Verify CSV download for Comparing Populations District Report - Filter removes student counts
      Then I verify that I'm on the performance page
      When I open the cpop report with the following parameters:
        | param        | value       |
        | stateCode    | RI          |
        | districtGuid | R0001       |
        | year         | 2015        |
        | gradeCourse  | Algebra+I   |
        | asmtType     | SUMMATIVE   |
        | subject      | subject1    |
        | view         | performance |
        | sex          | M,F         |
      Then I wait for the grid to reload
      When I click on the download link
      And I click on the download csv link
      Then the filename matches the format for the "cpop district performance" report
      And I see the following CSV header information:
        | key            | value                                   |
        | Report Name    | Newport District                        |
        | Date/Time      | %m-%d-%Y %H:%M:%S                       |
        | Institution    | Home > Rhode Island > Newport           |
        | Academic Year  | 2014 - 2015                             |
        | Subject        | Mathematics                             |
        | Grade/Course   | Algebra I                               |
        | Result         | Overall Performance                     |
        | View           | Performance                             |
        | Filter Options | Gender: Male or Female                  |
        | Total Count    | 1                                       |
        | Sort By        | SCHOOL                                  |
      And I see the following CSV main information:
        | SCHOOL                 | STUDENTS | PERFORMANCE DISTRIBUTION                                                 | >= LEVEL 4 | AVG SCORE |
        | DISTRICT AVG - Newport |          | Level 1 - 29%, Level 2 - 14%, Level 3 - 14%, Level 4 - 43%, Level 5 - 0% | 43%        | 232       |
        | Rogers High School     |          | Level 1 - 29%, Level 2 - 14%, Level 3 - 14%, Level 4 - 43%, Level 5 - 0% | 43%        | 232       |

    @mincellsize
    Scenario: Verify CSV download for Comparing Populations District Report - Filter / minimum cell size
      Then I verify that I'm on the performance page
      When I open the cpop report with the following parameters:
        | param        | value       |
        | stateCode    | RI          |
        | districtGuid | R0003       |
        | year         | 2015        |
        | gradeCourse  | Geometry    |
        | asmtType     | SUMMATIVE   |
        | subject      | subject1    |
        | view         | performance |
        | sex          | F           |
      And I verify that I am on the subject and grade/course initial selection page
      When I select the following options from the initial subject and grade/course selection page:
          | param        | value        |
          | subject      | Mathematics  |
          | gradeCourse  | Geometry     |
      Then I wait for the grid to reload
      When I click on the download link
      And I click on the download csv link
      Then the filename matches the format for the "cpop district performance" report
      And I see the following CSV header information:
        | key            | value                            |
        | Report Name    | Providence District              |
        | Date/Time      | %m-%d-%Y %H:%M:%S                |
        | Institution    | Home > Rhode Island > Providence |
        | Academic Year  | 2014 - 2015                      |
        | Subject        | Mathematics                      |
        | Grade/Course   | Geometry                         |
        | Result         | Overall Performance              |
        | View           | Performance                      |
        | Filter Options | Gender: Female                   |
        | Total Count    | 1                                |
        | Sort By        | SCHOOL                           |
      And I see the following CSV main information:
        | SCHOOL                    | STUDENTS | PERFORMANCE DISTRIBUTION                   | >= LEVEL 4 | AVG SCORE |
        | DISTRICT AVG - Providence |          | DATA SUPPRESSED TO PROTECT STUDENT PRIVACY |            |           |
        | Alvarez High School       |          | DATA SUPPRESSED TO PROTECT STUDENT PRIVACY |            |           |

    Scenario: Verify CSV download for Comparing Populations District Report - MATH Course - Subscore
      Then I verify that I'm on the performance page
      When I open the cpop report with the following parameters:
        | param        | value        |
        | stateCode    | RI           |
        | districtGuid | R0001        |
        | year         | 2015         |
        | gradeCourse  | Algebra+I    |
        | asmtType     | SUMMATIVE    |
        | subject      | subject1     |
        | view         | performance  |
        | result       | majorcontent |
#      And I verify that I am on the subject and grade/course initial selection page
#      When I select the following options from the initial subject and grade/course selection page:
#          | param        | value        |
#          | subject      | Mathematics  |
#          | gradeCourse  | Algebra+I    |
      Then I wait for the grid to reload
      When I click on the download link
      And I click on the download csv link
      Then the filename matches the format for the "cpop district performance" report
      And I see the following CSV header information:
        | key            | value                         |
        | Report Name    | Newport District              |
        | Date/Time      | %m-%d-%Y %H:%M:%S             |
        | Institution    | Home > Rhode Island > Newport |
        | Academic Year  | 2014 - 2015                   |
        | Subject        | Mathematics                   |
        | Grade/Course   | Algebra I                     |
        | Result         | Major Content                 |
        | View           | Performance                   |
        | Filter Options |                               |
        | Total Count    | 1                             |
        | Sort By        | SCHOOL                        |
      And I see the following CSV main information:
        | SCHOOL                 | STUDENTS | SUBSCORE DISTRIBUTION                      |
        | DISTRICT AVG - Newport | 7        | Level 1 - 71%, Level 2 - 29%, Level 3 - 0% |
        | Rogers High School     | 7        | Level 1 - 71%, Level 2 - 29%, Level 3 - 0% |

    Scenario: Verify CSV download for Comparing Populations District Report - ELA Course - Subscore
      Then I verify that I'm on the performance page
      When I open the cpop report with the following parameters:
        | param        | value        |
        | stateCode    | RI           |
        | districtGuid | R0001        |
        | year         | 2015         |
        | gradeCourse  | Grade 11     |
        | asmtType     | SUMMATIVE    |
        | subject      | subject2     |
        | view         | performance  |
        | result       | literarytext |
      Then I wait for the grid to reload
      When I click on the download link
      And I click on the download csv link
      Then the filename matches the format for the "cpop district performance" report
      And I see the following CSV header information:
        | key            | value                         |
        | Report Name    | Newport District              |
        | Date/Time      | %m-%d-%Y %H:%M:%S             |
        | Institution    | Home > Rhode Island > Newport |
        | Academic Year  | 2014 - 2015                   |
        | Subject        | ELA/L                         |
        | Grade/Course   | Grade 11                      |
        | Result         | Literary Text                 |
        | View           | Performance                   |
        | Filter Options |                               |
        | Total Count    | 1                             |
        | Sort By        | SCHOOL                        |
      And I see the following CSV main information:
        | SCHOOL                 | STUDENTS | SUBSCORE DISTRIBUTION                       |
        | DISTRICT AVG - Newport | 4        | Level 1 - 25%, Level 2 - 25%, Level 3 - 50% |
        | Rogers High School     | 4        | Level 1 - 25%, Level 2 - 25%, Level 3 - 50% |
