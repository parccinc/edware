@browser
Feature: Student roster report

  Background: Open a firefox browser and open the webpage
    Given an open browser
    When I open the student roster report with the following parameters:
        | param        | value     |
        | stateCode    | RI        |
        | districtGuid | R0003     |
        | schoolGuid   | RP001     |
        | gradeCourse  | Grade 9   |
        | asmtType     | SUMMATIVE |
        | year         | 2015      |
        | view         | scores    |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the student roster report

  Scenario: Verify CSV download for Student Roster Report - ELA - Scores
    When I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 9th Grade   |
        | subject      | ELA/L        |
    When I click on the download link
    And I click on the download csv link
    Then the filename matches the format for the "student roster scores" report
    And I see the following CSV header information:
      | key            | value                                                             |
      | Report Name    | Grade 9 Student Roster                                            |
      | Date/Time      | %m-%d-%Y %H:%M:%S                                                 |
      | Institution    | Home > Rhode Island > Providence > Alvarez High School > Grade 9  |
      | Academic Year  | 2014 - 2015                                                       |
      | Subject        | ELA/L                                                             |
      | Grade/Course   | Grade 9                                                           |
      | Result         | Overall Performance                                               |
      | View           | Scores                                                            |
      | Filter Options |                                                                   |
      | Total Count    | 5                                                                 |
      | Sort By        | STUDENT                                                           |
    And I see the following CSV main information:
    | STUDENT                          | GRADE                                 | OVERALL | READING: SCORE | READING: LITERARY TEXT                     | READING: INFORMATIONAL TEXT                 | READING: VOCABULARY                        | WRITING: SCORE | WRITING: WRITING EXPRESSION                | WRITING: KNOWLEDGE & USE OF LANGUAGE CONVENTIONS | GROWTH VS STATE | GROWTH VS PARCC |
    | SCHOOL AVG - Alvarez High School |                                       | 376     | 41             | Level 1 - 0%, Level 2 - 33%, Level 3 - 67% | Level 1 - 33%, Level 2 - 33%, Level 3 - 34% | Level 1 - 0%, Level 2 - 33%, Level 3 - 67% | 42             | Level 1 - 0%, Level 2 - 33%, Level 3 - 67% | Level 1 - 33%, Level 2 - 0%, Level 3 - 67%       | 80 %ile         | 40 %ile         |
    | Buckwalter, Keshia D.            | 09                                    | 404     | 49             | 3                                          | 3                                           | 3                                          | 47             | 3                                          | 3                                                | 70 %ile         | 62 %ile         |
    | Cabello, Kimberely C.            | STUDENT DID NOT RECEIVE A VALID SCORE |         |                |                                            |                                             |                                            |                |                                            |                                                  |                 |                 |
    | Cabello, Roman L.                | 09                                    | 463     | 46             | 3                                          | 1                                           | 3                                          | 49             | 3                                          | 3                                                | 72 %ile         | 24 %ile         |
    | Cartlidge, Keshia V.             | 09                                    | 261     | 28             | 2                                          | 2                                           | 2                                          | 29             | 2                                          | 1                                                | 98 %ile         | 33 %ile         |
    | Jewell, Betty S.                 | STUDENT DID NOT RECEIVE A VALID SCORE |         |                |                                            |                                             |                                            |                |                                            |                                                  |                 |                 |

  Scenario: Verify CSV download for Student Roster Report - ELA - Scores - Filters
    When I open the student roster report with the following parameters:
      | param        | value    |
      | subject      | subject2 |
      | gradeCourse  | Grade 9  |
      | stateCode    | RI       |
      | year         | 2015     |
      | districtGuid | R0003    |
      | schoolGuid   | RP001    |
      | sex          | F        |
    Then I wait for the grid to reload
    When I click on the download link
    And I click on the download csv link
    Then the filename matches the format for the "student roster scores" report
    And I see the following CSV header information:
      | key            | value                                                             |
      | Report Name    | Grade 9 Student Roster                                            |
      | Date/Time      | %m-%d-%Y %H:%M:%S                                                 |
      | Institution    | Home > Rhode Island > Providence > Alvarez High School > Grade 9 |
      | Academic Year  | 2014 - 2015                                                       |
      | Subject        | ELA/L                                                             |
      | Grade/Course   | Grade 9                                                           |
      | Result         | Overall Performance                                               |
      | View           | Scores                                                            |
      | Filter Options | Gender: Female                                                    |
      | Total Count    | 1                                                                 |
      | Sort By        | STUDENT                                                           |
    And I see the following CSV main information:
      | STUDENT                          | GRADE | OVERALL | READING: SCORE | READING: LITERARY TEXT                     | READING: INFORMATIONAL TEXT                | READING: VOCABULARY                        | WRITING: SCORE | WRITING: WRITING EXPRESSION                | WRITING: KNOWLEDGE & USE OF LANGUAGE CONVENTIONS | GROWTH VS STATE | GROWTH VS PARCC |
      | SCHOOL AVG - Alvarez High School |       | 261     | 28             | Level 1 - 0%, Level 2 - 100%, Level 3 - 0% | Level 1 - 0%, Level 2 - 100%, Level 3 - 0% | Level 1 - 0%, Level 2 - 100%, Level 3 - 0% | 29             | Level 1 - 0%, Level 2 - 100%, Level 3 - 0% | Level 1 - 100%, Level 2 - 0%, Level 3 - 0%       | 98 %ile         | 33 %ile         |
      | Cartlidge, Keshia V.             |09     | 261     | 28             | 2                                          | 2                                          | 2                                          | 29             | 2                                          | 1                                                | 98 %ile         | 33 %ile         |

    Scenario: Verify CSV download for Student Roster Report - ELA - Scores - Sort By
      When I click on header grid column number 3
      And I click on the download link
      And I click on the download csv link
      Then the filename matches the format for the "student roster scores" report
      And I see the following CSV header information:
        | key            | value                                                             |
        | Report Name    | Grade 9 Student Roster                                            |
        | Date/Time      | %m-%d-%Y %H:%M:%S                                                 |
        | Institution    | Home > Rhode Island > Providence > Alvarez High School > Grade 9 |
        | Academic Year  | 2014 - 2015                                                       |
        | Subject        | ELA/L                                                             |
        | Grade/Course   | Grade 9                                                           |
        | Result         | Overall Performance                                               |
        | View           | Scores                                                            |
        | Filter Options |                                                                   |
        | Total Count    | 5                                                                 |
        | Sort By        | OVERALL                                                           |
      And I see the following CSV main information:
      | STUDENT                          | GRADE                                 | OVERALL | READING: SCORE | READING: LITERARY TEXT                     | READING: INFORMATIONAL TEXT                 | READING: VOCABULARY                        | WRITING: SCORE | WRITING: WRITING EXPRESSION                | WRITING: KNOWLEDGE & USE OF LANGUAGE CONVENTIONS | GROWTH VS STATE | GROWTH VS PARCC |
      | SCHOOL AVG - Alvarez High School |                                       | 376     | 41             | Level 1 - 0%, Level 2 - 33%, Level 3 - 67% | Level 1 - 33%, Level 2 - 33%, Level 3 - 34% | Level 1 - 0%, Level 2 - 33%, Level 3 - 67% | 42             | Level 1 - 0%, Level 2 - 33%, Level 3 - 67% | Level 1 - 33%, Level 2 - 0%, Level 3 - 67%       | 80 %ile         | 40 %ile         |
      | Cartlidge, Keshia V.             | 09                                    | 261     | 28             | 2                                          | 2                                           | 2                                          | 29             | 2                                          | 1                                                | 98 %ile         | 33 %ile         |
      | Buckwalter, Keshia D.            | 09                                    | 404     | 49             | 3                                          | 3                                           | 3                                          | 47             | 3                                          | 3                                                | 70 %ile         | 62 %ile         |
      | Cabello, Roman L.                | 09                                    | 463     | 46             | 3                                          | 1                                           | 3                                          | 49             | 3                                          | 3                                                | 72 %ile         | 24 %ile         |
      | Cabello, Kimberely C.            | STUDENT DID NOT RECEIVE A VALID SCORE |         |                |                                            |                                             |                                            |                |                                            |                                                  |                 |                 |
      | Jewell, Betty S.                 | STUDENT DID NOT RECEIVE A VALID SCORE |         |                |                                            |                                             |                                            |                |                                            |                                                  |                 |                 |

    Scenario: Verify CSV download for Student Roster Report - ELA - Item Analysis
      When I click on the "Item Analysis" view
      Then I wait for the grid to reload
      When I click on the download link
      And I click on the download csv link
      Then the filename matches the format for the "student roster item analysis" report
      Then I see the following CSV header information:
        | key            | value                                                             |
        | Report Name    | Grade 9 Student Roster                                            |
        | Date/Time      | %m-%d-%Y %H:%M:%S                                                 |
        | Institution    | Home > Rhode Island > Providence > Alvarez High School > Grade 9  |
        | Academic Year  | 2014 - 2015                                                       |
        | Subject        | ELA/L                                                             |
        | Grade/Course   | Grade 9                                                           |
        | Result         | Overall Performance                                               |
        | View           | Item Analysis                                                     |
        | Filter Options |                                                                   |
        | Total Count    | 5                                                                 |
        | Sort By        | STUDENT                                                           |
      And I see the following CSV main information:
        | STUDENT                          | GRADE                                 | OVERALL | ITEM 1 - 10 PTS | ITEM 2 - 5 PTS | ITEM 3 - 4 PTS | ITEM 4 - 6 PTS | ITEM 5 - 7 PTS | ITEM 6 - 3 PTS | ITEM 7 - 5 PTS | ITEM 8 - 10 PTS | ITEM 9 - 3 PTS | ITEM 10 - 2 PTS | ITEM 11 - 8 PTS | ITEM 12 - 5 PTS | ITEM 13 - 10 PTS | ITEM 14 - 2 PTS | ITEM 15 - 10 PTS |
        | SCHOOL AVG - Alvarez High School |                                       | 376     | 6               | 5              | 2              | 2              | 4              | 1              | 2              | 7               | 2              | 0               | 5               | 2               | 6                | 0               | 6                |
        | Buckwalter, Keshia D.            | 09                                    | 404     | 1               | 5              | 2              | 4              | 4              | 0              | 5              | 10              | 2              | 0               | 3               | 1               | 6                | 1               | 10               |
        | Cabello, Kimberely C.            | STUDENT DID NOT RECEIVE A VALID SCORE |         |                 |                |                |                |                |                |                |                 |                |                 |                 |                 |                  |                 |                  |
        | Cabello, Roman L.                | 09                                    | 463     | 9               | 5              | 0              | 1              | 1              | 2              | 0              | 10              | 3              | 1               | 7               | 2               | 8                | 0               | 1                |
        | Cartlidge, Keshia V.             | 09                                    | 261     | 8               | 4              | 4              | 1              | 7              | 1              | 1              | 1               | 0              | 0               | 6               | 3               | 3                | 0               | 7                |
        | Jewell, Betty S.                 | STUDENT DID NOT RECEIVE A VALID SCORE |         |                 |                |                |                |                |                |                |                 |                |                 |                 |                 |                  |                 |                  |


  Scenario: Verify CSV download for Student Roster Report - ELA - Reading Comprehension
    When I open the school report with the following parameters:
        | param        | value      |
        | stateCode    | RI         |
        | districtGuid | R0003      |
        | schoolGuid   | RP001      |
        | subject      | subject2   |
        | asmtType     | DIAGNOSTIC |
        | year         | 2015       |
        | view         | vocab      |
    And I click on "Reading Comprehension" web button
    Then I wait for the grid to reload
    When I click on date range selector link
    And I enter "11/19/2014" date into "FROM CALENDAR" edit field
    And I click on apply button
    When I click on the download link
    And I click on the download csv link
    Then the filename matches the format for the "roster diagnostic ELA" report
    And I see the following CSV header information:
      | key            | value                                                  |
      | Report Name    | Alvarez High School School Report                      |
      | Date/Time      | %m-%d-%Y %H:%M:%S                                      |
      | Institution    | Home > Rhode Island > Providence > Alvarez High School |
      | Academic Year  | 2014 - 2015                                            |
      | Subject        | ELA/L                                                  |
      | Grade/Course   |                                                        |
      | Result         | Diagnostic Assessments                                 |
      | View           | Reading Comprehension                                  |
      | Filter Options | Date Range: 11/19/2014 - 04/01/2015                    |
      | Total Count    | 2                                                      |
      | Sort By        | STUDENT                                                |
    And I see the following CSV main information:
      | STUDENT             | GRADE | TEACHER                                | DATE       | SCORE | IRL | PASSAGES TYPE (RMM) |
      | Forst, Jacquline Z. | 04    | 3c6b9327b0484f5f84741b204a6c9f02208bc1 | 03/08/2015 | 509   | 2.6 | Lit (8)             |
      | Mccreery, Zofia K.  | 03    | 5f3bcce2cac34dd38578b9d423801dc3b5caf6 | 04/01/2015 | 443   | 7.4 | Lit (3.8)           |


  Scenario: Verify CSV download for Student Roster Report - Math - Fluency
    When I open the school report with the following parameters:
        | param        | value      |
        | stateCode    | RI         |
        | districtGuid | R0003      |
        | schoolGuid   | RP003      |
        | subject      | subject1   |
        | asmtType     | DIAGNOSTIC |
        | year         | 2015       |
        | view         | locator    |
    And I click on "Fluency" web button
    Then I wait for the grid to reload
    When I click on date range selector link
    And I enter "04/01/2015" date into "FROM CALENDAR" edit field
    And I enter "04/01/2015" date into "TO CALENDAR" edit field
    And I click on apply button
    When I click on the download link
    And I click on the download csv link
    Then the filename matches the format for the "roster diagnostic math" report
    And I see the following CSV header information:
      | key            | value                                               |
      | Report Name    | Hope High School School Report                      |
      | Date/Time      | %m-%d-%Y %H:%M:%S                                   |
      | Institution    | Home > Rhode Island > Providence > Hope High School |
      | Academic Year  | 2014 - 2015                                         |
      | Subject        | Mathematics                                         |
      | Grade/Course   |                                                     |
      | Result         | Diagnostic Assessments                              |
      | View           | Fluency                                             |
      | Filter Options | Date Range: 04/01/2015 - 04/01/2015                 |
      | Total Count    | 7                                                   |
      | Sort By        | STUDENT                                             |
    And I see the following CSV main information:
      | STUDENT           | GRADE | TEACHER                                | DATE       | SKILL                                             | ITEMS CORRECT | TOTAL TIME PER SKILL |
      | Cabello, Darci M. | 05    | 343fe3a084b2422796dbd7533a68de13c3cf37 | 04/01/2015 | Divide with Multi-digit Decimal Numbers (Gr. 6)   | 2/3 (66.7%)   | 00:09:23             |
      | Cabello, Darci M. | 05    | 343fe3a084b2422796dbd7533a68de13c3cf37 | 04/01/2015 | Add & Subtract within 1,000 (Gr. 3)               | 0/3 (0%)      | 00:05:12             |
      | Cabello, Darci M. | 05    | 343fe3a084b2422796dbd7533a68de13c3cf37 | 04/01/2015 | Add & Subtract within 1,000,000 (Gr. 4)           | 1/3 (33.3%)   | 00:02:34             |
      | Cabello, Darci M. | 05    | 343fe3a084b2422796dbd7533a68de13c3cf37 | 04/01/2015 | Multiply & Divide within 100 (Gr. 3)              | 4/4 (100%)    | 00:02:38             |
      | Cabello, Darci M. | 05    | 343fe3a084b2422796dbd7533a68de13c3cf37 | 04/01/2015 | Add/Subtract within 20 (Gr. 2)                    | 0/2 (0%)      | 00:06:35             |
      | Pando, Joella M.  | 05    | dcdaa08b9b9046beb3e77419481c860bc6598e | 04/01/2015 | Add & Subtract within 1,000 (Gr. 3)               | 3/3 (100%)    | 00:03:46             |
      | Pando, Joella M.  | 05    | dcdaa08b9b9046beb3e77419481c860bc6598e | 04/01/2015 | Multiply with Multi-digit Decimal Numbers (Gr. 6) | 1/3 (33.3%)   | 00:02:51             |
