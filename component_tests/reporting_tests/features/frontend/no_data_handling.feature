Feature: No data handling
  Background: Open a firefox browser and open the webpage
    Given an open browser

  @RALLY_US36075
  Scenario: Verify no data error handling on PARCC Report for ELA/L
    When I open the cpop report with the following parameters
      | param       | value     |
      | gradeCourse | Grade 4   |
      | asmtType    | SUMMATIVE |
      | subject     | subject2  |
      | year        | 2015      |
    Then I am redirected to the login page
    When I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 4th Grade    |
        | subject      | ELA/L        |
    Then I wait for the grid to reload
    And I should see the report title "All PARCC States"
    And I wait for the grid to reload
    And I should see the subtitle "0 states"
    And I should see no data message
    When I select "Literary Text" from the results dropdown
    Then I wait for the grid to reload
    And I should see no data message
    When I select "Informational Text" from the results dropdown
    Then I wait for the grid to reload
    And I should see no data message
    When I select "Vocabulary" from the results dropdown
    Then I wait for the grid to reload
    And I should see no data message
    When I select "Writing Expression" from the results dropdown
    Then I wait for the grid to reload
    And I should see no data message
    When I select "Knowledge & Use of Language Conventions" from the results dropdown
    Then I wait for the grid to reload
    And I should see no data message

  @RALLY_US36075
  Scenario: Verify no data error handling on PARCC Report for Math
    When I open the cpop report with the following parameters
      | param       | value     |
      | gradeCourse | Grade 4   |
      | asmtType    | SUMMATIVE |
      | year        | 2015      |
      | subject     | subject1  |
    Then I am redirected to the login page
    When I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 4th Grade    |
        | subject      | Mathematics        |
    Then I wait for the grid to reload
    And I should see the report title "All PARCC States"
    And I wait for the grid to reload
    And I should see the subtitle "0 states"
    And I should see no data message
    When I select "Major Content" from the results dropdown
    Then I wait for the grid to reload
    And I should see no data message
    When I select "Additional & Supporting Content" from the results dropdown
    Then I wait for the grid to reload
    And I should see no data message
    When I select "Modeling and Application" from the results dropdown
    Then I wait for the grid to reload
    And I should see no data message
    When I select "Expressing Mathematical Reasoning" from the results dropdown
    Then I wait for the grid to reload
    And I should see no data message

  @RALLY_US36075
  Scenario: Verify no data error handling on State Report for ELA/L
    When I open the cpop report with the following parameters
      | param       | value     |
      | gradeCourse | Grade 3   |
      | asmtType    | SUMMATIVE |
      | year        | 2015      |
      | stateCode   | RI        |
      | subject     | subject2  |
    And I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | ELA/L        |
    Then I wait for the grid to reload
    When I am redirected to the cpop state report
    When I click on GRADE/COURSE dropdown
    And I select "4th Grade" from GRADE/COURSE dropdown
    Then I wait for the grid to reload
    And I should see the subtitle "0 districts"
    And I should see no data message
    When I click on the "Growth" view on the cpop state report
    Then I wait for the grid to reload
    And I should see the subtitle "0 districts"
    And I should see no data message

  @RALLY_US36075
  Scenario: Verify no data error handling on State Report for Math
    When I open the cpop report with the following parameters
      | param       | value     |
      | gradeCourse | Grade 3   |
      | asmtType    | SUMMATIVE |
      | year        | 2015      |
      | stateCode   | RI        |
      | subject     | subject1  |
    And I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | Mathematics  |
    Then I wait for the grid to reload
    When I am redirected to the cpop state report
    Then I wait for the grid to reload
    And I should see the subtitle "1 district"
    When I click on the Filters button to "open" the filter menu
    And I select "Not Specified" options from the "Gender" filter
    And I select "Two or more races" options from the "Race / Ethnicity" filter
    And I select "Both" options from the "ELL / LEP" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    And I should see the subtitle "0 districts"
    And I should see no data message
    When I click on the "Growth" view on the cpop state report
    Then I wait for the grid to reload
    And I should see the subtitle "0 districts"
    And I should see no data message

  @RALLY_US36075
  Scenario: Verify no data error handling on District Report for ELA/L
    When I open the cpop report with the following parameters
      | param        | value     |
      | stateCode    | RI        |
      | districtGuid | R0003     |
      | gradeCourse  | Grade 9   |
      | asmtType     | SUMMATIVE |
      | year         | 2015      |
      | subject      | subject2  |
    And I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 9th Grade    |
        | subject      | ELA/L        |
    Then I wait for the grid to reload
    When I am redirected to the cpop district report
    And I click on GRADE/COURSE dropdown
    And I select "4th Grade" from GRADE/COURSE dropdown
    Then I wait for the grid to reload
    And I should see the subtitle "0 schools"
    And I should see no data message
    When I click on the "Growth" view on the cpop district report
    Then I wait for the grid to reload
    And I should see the subtitle "0 schools"
    And I should see no data message

  @RALLY_US36075
  Scenario: Verify no data error handling on District Report for Math
    When I open the cpop report with the following parameters
      | param        | value     |
      | stateCode    | RI        |
      | districtGuid | R0003     |
      | gradeCourse  | Grade 3   |
      | asmtType     | SUMMATIVE |
      | year         | 2015      |
      | subject      | subject1  |
    And I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | Mathematics  |
    And I am redirected to the cpop district report
    Then I wait for the grid to reload
    And I should see the subtitle "1 school"
    When I click on the Filters button to "open" the filter menu
    And I select "Not Specified" options from the "Gender" filter
    And I select "Two or more races" options from the "Race / Ethnicity" filter
    And I select "Both" options from the "ELL / LEP" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    And I should see the subtitle "0 schools"
    And I should see no data message
    When I click on the "Growth" view on the cpop district report
    Then I wait for the grid to reload
    And I should see the subtitle "0 schools"
    And I should see no data message

  @RALLY_US36075
  Scenario: Verify no data error handling on School Report for ELA/L
    When I open the school report with the following parameters
      | param        | value    |
      | subject      | subject2 |
      | stateCode    | RI       |
      | year         | 2015     |
      | districtGuid | R0003    |
      | schoolGuid   | RP003    |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the cpop school report
    Then I should see the subtitle "4 grades"
    When I click on the Filters button to "open" the filter menu
    And I select "Not Specified" options from the "Gender" filter
    And I select "Two or more races" options from the "Race / Ethnicity" filter
    And I select "Both" options from the "ELL / LEP" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    And I should see the subtitle "0 grades"
    And I should see no data message

  @RALLY_US36075
  Scenario: Verify no data error handling on School Report for Math
    When I open the school report with the following parameters
      | param        | value    |
      | subject      | subject1 |
      | stateCode    | RI       |
      | year         | 2015     |
      | districtGuid | R0003    |
      | schoolGuid   | RP003    |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the cpop school report
    Then I should see the subtitle "4 grades/courses"
    When I click on the Filters button to "open" the filter menu
    And I select "Not Specified" options from the "Gender" filter
    And I select "Two or more races" options from the "Race / Ethnicity" filter
    And I select "Both" options from the "ELL / LEP" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    And I should see the subtitle "0 grades/courses"
    And I should see no data message

  @RALLY_US36075
  Scenario: Verify no data error handling on Diagnostic Assessments for ELA/L
    When I open the school report with the following parameters
      | param        | value      |
      | stateCode    | RI         |
      | districtGuid | R0003      |
      | schoolGuid   | RP003      |
      | subject      | subject2   |
      | asmtType     | DIAGNOSTIC |
      | year         | 2015       |
      | view         | overview   |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the cpop school report
    Then I wait for the grid to reload
    Then I should see the subtitle "25 assessments"
    When I click on date range selector link
    And I enter "11/20/2014" date into "FROM CALENDAR" edit field
    And I enter "11/25/2014" date into "TO CALENDAR" edit field
    And I click on apply button
    Then I wait for the grid to reload
    And I should see the subtitle "0 assessments"
    And I should see no data message

  @RALLY_US36075
  Scenario: Verify no data error handling on Diagnostic Assessments for Math
    When I open the school report with the following parameters
      | param        | value      |
      | stateCode    | RI         |
      | districtGuid | R0003      |
      | schoolGuid   | RP003      |
      | subject      | subject1   |
      | asmtType     | DIAGNOSTIC |
      | year         | 2015       |
      | view         | locator    |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the cpop school report
    Then I wait for the grid to reload
    Then I should see the subtitle "10 assessments"
    When I click on the Filters button to "open" the filter menu
    And I select "Not Specified" options from the "Gender" filter
    And I select "Two or more races" options from the "Race / Ethnicity" filter
    And I select "Both" options from the "ELL / LEP" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    And I should see the subtitle "0 assessments"
    And I should see no data message

  @RALLY_US36075
  Scenario: Verify no data error handling on Student Roster report for ELA/L
    When I open the student roster report with the following parameters:
      | param        | value     |
      | stateCode    | RI        |
      | districtGuid | R0003     |
      | schoolGuid   | RP001     |
      | gradeCourse  | Grade 11  |
      | asmtType     | SUMMATIVE |
      | year         | 2015      |
      | subject      | subject2  |
      | view         | scores    |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the student roster report
    Then I should see the subtitle "6 students"
    When I click on the Filters button to "open" the filter menu
    And I select "Not Specified" options from the "Gender" filter
    And I select "Two or more races" options from the "Race / Ethnicity" filter
    And I select "Both" options from the "ELL / LEP" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    And I should see the subtitle "0 students"
    And I should see no data message
    When I click on the "Item Analysis" view
    Then I wait for the grid to reload
    And I should see the subtitle "0 students"
    And I should see no data message

  @RALLY_US36075
  Scenario: Verify no data error handling on Student Roster report for Math
    When I open the student roster report with the following parameters:
      | param        | value         |
      | stateCode    | RI            |
      | districtGuid | R0003         |
      | schoolGuid   | RP001         |
      | gradeCourse  | Integrated+Mathematics+I |
      | asmtType     | SUMMATIVE     |
      | year         | 2015          |
      | view         | itemAnalysis  |
      | subject      | subject1      |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the student roster report
    Then I should see the subtitle "5 students"
    When I click on the Filters button to "open" the filter menu
    And I select "Not Specified" options from the "Gender" filter
    And I select "Two or more races" options from the "Race / Ethnicity" filter
    And I select "Both" options from the "ELL / LEP" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    And I should see the subtitle "0 students"
    And I should see no data message
    When I click on the "Scores" view
    Then I wait for the grid to reload
    And I should see the subtitle "0 students"
    And I should see no data message

  @RALLY_US36075
  Scenario: Verify no data error handling on ISR
    When I open the individual student report with the following parameters:
        | param        | value                                    |
        | subject      | subject1                                 |
        | gradeCourse  | Grade 5                                  |
        | stateCode    | RI                                       |
        | studentGuid  | FkgQ5OyZAMUDgnC8f6cJHnpLkzOlGV6xnArV3Bhf |
        | year         | 2015                                     |
        | districtGuid | R0003                                    |
        | schoolGuid   | RP003                                    |
        | asmtType     | DIAGNOSTIC                               |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the individual student report
    Then I wait for the grid to reload
    When I select "ELA/L" from the subject dropdown
    And I am redirected to the individual student report
    Then I wait for the grid to reload
    Then I should see no data message
    When I select "Overall Performance" from the results dropdown
    And I am redirected to the individual student report
    Then I wait for the grid to reload
    Then I should see no data message
    When I select "Mathematics" from the subject dropdown
    And I am redirected to the individual student report
    Then I wait for the grid to reload
    Then I should see no data message
    When I select "2013 - 2014" from dropdown
    And I am redirected to the individual student report
    Then I wait for the grid to reload
    When I select "Diagnostic Assessments" from the results dropdown
    And I am redirected to the individual student report
    Then I wait for the grid to reload
    Then I should see no data message
    When I select "ELA/L" from the subject dropdown
    And I am redirected to the individual student report
    Then I wait for the grid to reload
    Then I should see no data message
    When I select "Overall Performance" from the results dropdown
    And I am redirected to the individual student report
    Then I wait for the grid to reload
    Then I should see no data message
    When I select "Mathematics" from the subject dropdown
    And I am redirected to the individual student report
    Then I wait for the grid to reload
    Then I should see no data message
