Feature: Individual Student report

  Background: Open a firefox browser and open the webpage
    Given an open browser

  @RALLY_US31841
  @RALLY_US31840
  @RALLY_US31842
  @RALLY_US31846
  @RALLY_US33243
  @RALLY_US31907
  Scenario: Validate the individual student report for ELA
  	When I open the individual student report with the following parameters:
        | param        | value                                    |
        | subject      | subject2                                 |
        | gradeCourse  | Grade 9                                  |
        | stateCode    | RI                                       |
        | studentGuid  | EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q |
        | year         | 2015                                     |
        | districtGuid | R0001                                    |
        | schoolGuid   | RN001                                    |
        | subject      | subject2                                 |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the individual student report
    Then I should see the ISR Header with PARCC logo
    And the ISR header should display the students name "Darci Forst" in the header title
    And the ISR header should display the following static breadcrumbs:
        | type     | value                                         |
        | grade    | Grade 9                                       |
        | school   | Rogers High School                            |
        | district | Newport                                       |
        | state    | RI                                            |
    Then I verify that the subject selected is "ELA/L"
    Then the overall performance section is displayed on ISR
    And the overall performance level is displayed as "Level 3"
    And the overall description displayed contains score "254", performance level "Level 3"
    #and level description "adequate understanding"
    And the performance bar is displayed with the student score marked as "254" for "ELA"
    And the performance bar legend contains the following:
        | name              | value |
        | * PROBABLE RANGE  | ± 5   |
        | SCHOOL AVG        | 233   |
        | DISTRICT AVG      | 233   |
        | STATE AVG         | 258   |
        | PARCC STATES AVG  | 258   |
    And I see the legend with three performance level description
    And the claim performance bar legend for "READING" contains the following:
      | name                       | value |
      | SUBSCORE                   |    21 |
      | AVG OF STUDENTS AT LEVEL 4 |    26 |
      | PARCC AVG                  |    28 |
      | STATE AVG                  |    28 |
      | DISTRICT AVG               |    22 |
      | SCHOOL AVG                 |    22 |
    And the claim performance bar legend for "WRITING" contains the following:
      | name                       | value |
      | SUBSCORE                   |    24 |
      | PARCC AVG                  |    29 |
      | AVG OF STUDENTS AT LEVEL 4 |    29 |
      | STATE AVG                  |    29 |
      | DISTRICT AVG               |    26 |
      | SCHOOL AVG                 |    26 |

  @RALLY_US31841
  @RALLY_US31840
  @RALLY_US31842
  @RALLY_US31907
  Scenario: Validate the individual student report for Math
  	When I open the individual student report with the following parameters:
        | param        | value                                    |
        | subject      | subject1                                 |
        | gradeCourse  | Algebra+I                                |
        | stateCode    | RI                                       |
        | studentGuid  | EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q |
        | year         | 2015                                     |
        | districtGuid | R0001                                    |
        | schoolGuid   | RN001                                    |
        | subject      | subject1                                 |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the individual student report
    Then I should see the ISR Header with PARCC logo
    Then I verify that the subject selected is "Mathematics"
    Then the overall performance section is displayed on ISR
    And the overall performance level is displayed as "Level 4"
    And the overall description displayed contains score "335", performance level "Level 4"
    #and level description "strong understanding"
    And the performance bar is displayed with the student score marked as "335" for "Mathematics"
    And the performance bar legend contains the following:
        | name              | value |
        | * PROBABLE RANGE  | ± 6   |
        | SCHOOL AVG        | 232   |
        | DISTRICT AVG      | 232   |
        | STATE AVG         | 253   |
        | PARCC STATES AVG  | 265   |
    Then I see the legend with three performance level description

  Scenario: Validate that individual student report only displays required subjects in the subject dropdown
    When I open the individual student report with the following parameters:
        | param        | value                                    |
        | subject      | subject2                                 |
        | gradeCourse  | Grade 9                                  |
        | stateCode    | RI                                       |
        | studentGuid  | Z94Y0ynyVyq3VCwLTjIRZHTZSYkFHT0lBN8meOgb |
        | year         | 2015                                     |
        | districtGuid | R0003                                    |
        | schoolGuid   | RP001                                    |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the individual student report
    Then I verify that the subject selected is "ELA/L"

  @RALLY_US34665
  Scenario: Validate the PDF view has the proper headers and text for various subject/grade combinations
    When I open the individual student report with the following parameters:
      | param        | value                                    |
      | subject      | subject2                                 |
      | gradeCourse  | Grade 9                                  |
      | stateCode    | RI                                       |
      | studentGuid  | Avrxze2AmndYm8dxmLJxDxhLsKpRgxaISvx3KS6F |
      | year         | 2015                                     |
      | districtGuid | R0003                                    |
      | schoolGuid   | RP001                                    |
      | pdf          | color                                    |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the individual student report
    Then the PDF report title area should display the following:
      | label      | value                            |
      | aboveTitle | ENGLISH LANGUAGE ARTS / LITERACY   |
      | title      | Grade 9 Assessment Report, 2014 - 2015  |
    When I open the individual student report with the following parameters:
      | param        | value                                    |
      | subject      | subject1                                 |
      | gradeCourse  | Integrated+Mathematics+I                            |
      | stateCode    | RI                                       |
      | studentGuid  | Avrxze2AmndYm8dxmLJxDxhLsKpRgxaISvx3KS6F |
      | year         | 2015                                     |
      | districtGuid | R0003                                    |
      | schoolGuid   | RP001                                    |
      | pdf          | color                                    |
    Then the PDF report title area should display the following:
      | label      | value                                 |
      | aboveTitle | MATHEMATICS                           |
      | title      | Integrated Mathematics I Assessment Report, 2014 - 2015 |
    When I open the individual student report with the following parameters:
      | param        | value                                    |
      | subject      | subject1                                 |
      | gradeCourse  | Grade 5                                  |
      | stateCode    | RI                                       |
      | studentGuid  | 5336b7258eb545b3ae7178aeb0cfd72428a36fdb |
      | year         | 2015                                     |
      | districtGuid | R0001                                    |
      | schoolGuid   | RN001                                    |
      | pdf          | color                                    |
    Then the PDF report title area should display the following:
      | label      | value                            |
      | aboveTitle | MATHEMATICS                      |
      | title      | Grade 5 Assessment Report, 2014 - 2015  |

  @RALLY_US34450
  @RALLY_US36746
  Scenario: Verify year drop-down on report
    When I open the individual student report with the following parameters:
        | param        | value                                    |
        | subject      | subject2                                 |
        | gradeCourse  | Grade 11                                 |
        | stateCode    | RI                                       |
        | studentGuid  | nQXqvAHQ0W1FOGH2IFhG68Aa590JN9bpNQtSXdiw |
        | year         | 2015                                     |
        | districtGuid | R0003                                    |
        | schoolGuid   | RP003                                    |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the individual student report
    Then I should see "2014 - 2015" on Academic Year dropdown
    Then I should see "Summative: Overall Performance " on the results dropdown
    And student score should be "483*"
    And I verify ccr styling "is" present in the perf section
    When I click on "Academic Year" drop-down
    Then I should see following values in the year dropdown
     | value       |
     | 2014 - 2015 |
     | 2013 - 2014 |
    When I click on "Academic Year" drop-down
    And I select "2013 - 2014" from dropdown
    Then I wait for the grid to reload
    And I should see "2013 - 2014" on Academic Year dropdown
    And student score should be "490*"

  @RALLY_DE1049
  Scenario: Validate Subject dropdown
    When I open the individual student report with the following parameters:
        | param        | value                                    |
        | subject      | subject1                                 |
        | gradeCourse  | Algebra+I                                |
        | stateCode    | RI                                       |
        | studentGuid  | EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q |
        | year         | 2015                                     |
        | districtGuid | R0001                                    |
        | schoolGuid   | RN001                                    |
        | subject      | subject1                                 |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the individual student report
    Then I verify that the subject dropdown has two options

  @RALLY_US36863
  @RALLY_US36746
  Scenario: Verify result selector text present - ISR
    When I open the individual student report with the following parameters:
        | param        | value                                    |
        | subject      | subject1                                 |
        | gradeCourse  | Algebra+I                                |
        | stateCode    | RI                                       |
        | studentGuid  | EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q |
        | year         | 2015                                     |
        | districtGuid | R0001                                    |
        | schoolGuid   | RN001                                    |
        | subject      | subject1                                 |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the individual student report
    When I click on the results dropdown
    Then I should not see "- Available at school level" in the results list
    Then I verify ccr styling "is not" present in the perf section


  @RALLY_US36746
  Scenario: Verify ccr styling in math III and algebra II
    When I open the individual student report with the following parameters:
        | param        | value                                    |
        | subject      | subject1                                 |
        | gradeCourse  | Integrated+Mathematics+III                          |
        | stateCode    | RI                                       |
        | studentGuid  | 4mzU3y7OgIchC8Rz9tfOk7KI8AcXlVtFPVOO1XFH |
        | year         | 2015                                     |
        | districtGuid | R0003                                    |
        | schoolGuid   | RP002                                    |
        | subject      | subject1                                 |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the individual student report
    Then I verify ccr styling "is" present in the perf section
    When I open the individual student report with the following parameters:
        | param        | value                                    |
        | subject      | subject1                                 |
        | gradeCourse  | Algebra+II                               |
        | stateCode    | RI                                       |
        | studentGuid  | p69xZhxSMQuWmke62h3j1K7izaj1evXeklkHFuMg |
        | year         | 2015                                     |
        | districtGuid | R0002                                    |
        | schoolGuid   | RE001                                    |
        | subject      | subject1                                 |
    And I am redirected to the individual student report
    Then I verify ccr styling "is" present in the perf section
