import os
from unittest import TestCase
from selenium.common.exceptions import NoAlertPresentException, UnexpectedAlertPresentException
from components_tests.frontend.support import get_url
from components_tests.frontend.common import make_download_directory, remove_download_directory
from components_tests.frontend.common import read_config, setup_browser
from time import sleep
from components_tests.flaky_tests_handler.flaky_tests_handler import handle_failed_feature


def before_feature(context, feature):
    read_config(context)
    if 'browser' in feature.tags:
        context.browser = 'firefox'

    context.tc = TestCase()
    setup_browser(context)


def close_alert_popup(origin_func):
    def catch_alerts(context, *args, **kwargs):
        try:
            return origin_func(context, *args, **kwargs)
        except UnexpectedAlertPresentException:
            close_alert_if_exist(context)
        return origin_func(context, *args, **kwargs)
    return catch_alerts


def close_alert_if_exist(context):
    try:
        alert = context.driver.switch_to.alert
        sleep(1)
        alert.accept()
    except NoAlertPresentException:
        pass


@close_alert_popup
def already_logged_in(context):
    return context.driver.get_cookie('edware')


def after_feature(context, feature):
    # Tear down web driver
    context.driver.quit()
    if feature.status == 'failed':
        rerun_mode = context.config.userdata.getbool('rerun_mode')
        handle_failed_feature(feature, rerun_mode)


def before_scenario(context, scenario):
    make_download_directory()


@close_alert_popup
def after_scenario(context, scenario):
    # delete download dir
    remove_download_directory()
    context.driver.get(get_url(context) + '/logout')
    context.driver.delete_all_cookies()


def after_step(context, step):
    if step.status == "failed":
        if hasattr(context, 'driver'):
            if hasattr(context, 'take_screenshot_on_failure') and context.take_screenshot_on_failure == 'yes':
                if not os.path.exists('screenshots'):
                    os.makedirs('screenshots')
                failures()
                context.driver.save_screenshot('screenshots/failure_{0}.png'.format(failures.count))


def failures():
    failures.count += 1
failures.count = 0
