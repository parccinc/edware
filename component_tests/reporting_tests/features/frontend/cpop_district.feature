Feature: District comparing populations report

  Background: Open a firefox browser and open the webpage
    # Given an open browser
    When I open the cpop report with the following parameters
      | param        | value       |
      | stateCode    | RI          |
      | districtGuid | R0003       |
      | year         | 2015        |
      | gradeCourse  | Grade 11    |
      | subject      | subject2    |
      | asmtType     | SUMMATIVE   |
      | view         | performance |
    Then I am redirected to the login page
    When I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 11th Grade   |
        | subject      | ELA/L        |
    When I am redirected to the cpop district report

  @RALLY_US32650
  @RALLY_US31868
  Scenario: Validate the grid, report titles, subtitle (number of students) on the cpop district report
    Then I verify that I'm on the performance page
    And I should see the breadcrumbs "Home,Rhode Island,Providence"
    And I should see the report title "Providence"
    And I should see the subtitle "3 schools"
    And I should see "SCHOOL,STUDENTS,PERFORMANCE DISTRIBUTION,≥ LVL 4,OVERALL,READING WRITING" columns for ela overall
    And I verify that the subject selected is "ELA/L"
    When I select "Literary Text" from the results dropdown
    Then I should see the breadcrumbs "Home,Rhode Island,Providence"
    And I should see the report title "Providence"
    And I should see the subtitle "3 schools"
    And I should see "SCHOOL,STUDENTS,SUBSCORE DISTRIBUTION" columns for literary text

  @RALLY_US32650
  Scenario: Validate that the subscore avg is only for ELA - Overall
    Then I verify that I'm on the performance page
    And I verify that the subject selected is "ELA/L"
    And I should see a subscore avg column "is present"
    When I select "Mathematics" from the subject dropdown
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | subject      | Mathematics  |
        | gradeCourse  | 3rd Grade    |
    Then I wait for the grid to reload
    Then I should see a subscore avg column "is not present"

  @RALLY_US32650
  @RALLY_US33946
  Scenario: Validate grid sorting
    Then I verify that I'm on the performance page
    And I should see the value "Alvarez High School" in the first "SCHOOL" cell
    When I click on SCHOOL column for sorting
    Then I should see the value "Mount Pleasant High School" in the first "SCHOOL" cell
    When I click on STUDENTS column for sorting
    Then I should see the value "3" in the first "STUDENTS" cell
    When I click on STUDENTS column for sorting
    Then I should see the value "4" in the first "STUDENTS" cell
    When I click on PERFORMANCE DISTRIBUTION column for sorting
    Then I should see the value "143" in the first "OVERALL" cell
    When I click on PERFORMANCE DISTRIBUTION column for sorting
    Then I should see the value "4" in the first "STUDENTS" cell
    When I select "Literary Text" from the results dropdown
    Then I should see the value "4" in the first "STUDENTS" cell
    When I click on SUBSCORE DISTRIBUTION column for sorting
    Then I should see the value "Mount Pleasant High School" in the first "SCHOOL" cell
    When I click on SUBSCORE DISTRIBUTION column for sorting
    Then I should see the value "Alvarez High School" in the first "SCHOOL" cell
    # Then I should see the value "363" in the first "OVERALL" cell

  @RALLY_US32650
  Scenario: Validate the level4 percentage, avg and subscore avg for ELA
    Then I verify that I'm on the performance page
    Then I verify lvl4 percentage for first school
    And I verify average score for first school is 362
    And I verify the "reading" avg subscore for first school is "44 / 50"
    And I verify the "writing" avg subscore for first school is "43 / 60"

  @RALLY_US32650
  Scenario: Validate grid has correct data for Math
    Then I verify that I'm on the performance page
    When I select "Mathematics" from the subject dropdown
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | subject      | Mathematics  |
        | gradeCourse  | 3rd Grade    |
    Then I wait for the grid to reload
    Then I verify average score for first school is 146

  @RALLY_US31867
  @RALLY_US31869
  Scenario: Validate legend bar is present and toggleable for overall and subscore
    Then I verify that I'm on the performance page
    When I select "Mathematics" from the subject dropdown
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | subject      | Mathematics  |
        | gradeCourse  | 3rd Grade    |
    Then I wait for the grid to reload
    Then I verify the overall ccr legend bar has the correct number of items
    And I verify ccr styling is not present on the legend bar
    When I click on GRADE/COURSE dropdown
    And I select "Algebra II" from GRADE/COURSE dropdown
    Then I verify ccr styling is present on the legend bar
    When I click on GRADE/COURSE dropdown
    And I select "Int. Math III" from GRADE/COURSE dropdown
    Then I verify ccr styling is present on the legend bar
    When I select "ELA/L" from the subject dropdown
    And I click on GRADE/COURSE dropdown
    And I select "11th Grade" from GRADE/COURSE dropdown
    Then I verify ccr styling is present on the legend bar
    When I select "Literary Text" from the results dropdown
    Then I verify the subscore legend bar has the correct number of items
    And I verify ccr styling is not present on the legend bar

  @RALLY_US31868
  Scenario: Validate the number of schools matches the number rows in the grid
    Then I verify that I'm on the performance page
    And I verify that the subject selected is "ELA/L"
    When I select "Literary Text" from the results dropdown
    Then I should see the subtitle "3 schools"
    And I verify the number of schools matches the number of rows in the grid

  @RALLY_US31868
  Scenario: Validate the subscore distribution adds up to 100%
    Then I verify that I'm on the performance page
    And I verify that the subject selected is "ELA/L"
    When I select "Literary Text" from the results dropdown
    Then I should see "SCHOOL,STUDENTS,SUBSCORE DISTRIBUTION" columns for literary text
    And I verify subscore distribution column adds up to 100% for the first row

  @RALLY_US31871
  @RALLY_US31873
  @RALLY_US31875
  @RALLY_US33939
  Scenario: Validate cpop(district) growth view for ELA
  When I click on the "Growth" view on the cpop district report
  And I wait for the "Growth" view to be loaded
  Then I verify that the subject selected is "ELA/L"
  And I should see the breadcrumbs "Home,Rhode Island,Providence"
  And I should see the report title "Providence"
  And I should see the subtitle "3 schools"
  Then the school list grid has "SHOW/HIDE ALL checkbox,SCHOOL,STUDENTS" columns
  And I see "3" rows in the school list grid
  And I should see the value "Alvarez High School" in the first "SCHOOL" row in student list grid
  When I click on SCHOOL column for sorting
  Then I should see the value "Mount Pleasant High School" in the first "SCHOOL" row in student list grid
  When I click on STUDENTS column for sorting
  Then I should see the value "3" in the first "STUDENTS" row in student list grid
  And the growth chart title is "Providence"
  And the overall summary bar above growth chart contains the following values:
    | name         | value              |
    | STUDENTS     | 11                 |
    | AVG SCORE    | 278                |
    | DISTRIBUTION | 18%,9%,18%,27%,28% |
  And the growth chart "X" axis label is "GROWTH(Median Student Growth Percentile)"
  And the growth chart "X" axis scale cutpoints are "0,10,20,30,40,50,60,70,80,90,100"
  And the growth chart "Y" axis label is "PERFORMANCE(Avg Overall Scale Score)"
  And the growth chart "Y" axis scale cutpoints are "1,101,201,301,401,500"
  And a static text is displayed below the growth chart
  When I select the growth chart comparison option as "PARCC"
  Then I see "2" circles in the bubble chart
  And a circle is positioned at "203.5"
  When I select the growth chart comparison option as "STATE"
  Then I see "2" circles in the bubble chart
  And a circle is positioned at "253"
  And a circle is positioned at "429"
  And I see the legend with description for the five ALD levels on the performance bar

  @RALLY_US31872
  Scenario: Validate hide/unhide school interaction between student list and bubble chart in cpop(district) growth view
    When I click on the "Growth" view on the cpop district report
    And I wait for the "Growth" view to be loaded
    Then I verify that the subject selected is "ELA/L"
    Then the school list grid has "SHOW/HIDE ALL checkbox,SCHOOL,STUDENTS" columns
    And I see "3" rows in the school list grid
    When I "uncheck" the "Hope High School" school from the school grid
    And I "uncheck" the "Alvarez High School" school from the school grid
    When I select the growth chart comparison option as "PARCC"
    Then I see "1" circles in the bubble chart
    And a circle is positioned at "374"
    When I select the growth chart comparison option as "STATE"
    Then I see "1" circles in the bubble chart
    And a circle is positioned at "429"
    Then I see the legend with description for the five ALD levels on the performance bar

  @RALLY_US31874
  @RALLY_US33939
  Scenario: Validate mouseover on the bubble opens the popover in cpop(district) growth view
    When I click on the "Growth" view on the cpop district report
    And I wait for the "Growth" view to be loaded
    Then I verify that the subject selected is "ELA/L"
    When I select the growth chart comparison option as "PARCC"
    Then I see "2" circles in the bubble chart
    When I mouseover on the bubble positioned at "374" "393"
    Then the active bubble is in front with school name "Mount Pleasant High School"
    Then I wait for the popover to appear
    And the popover is displayed with the following information:
      | name                  | value                            |
      | School                | Mount Pleasant High School       |
      | STUDENTS              | 4                                |
      | AVG SCORE             | 143                              |
      | Subtitle              | Median Student Growth Percentile |
      | COMPARED TO PARCC     | 68 %tile                         |
      | COMPARED TO STATE     | 78 %tile                         |
    When I select the growth chart comparison option as "STATE"
    Then I see "2" circles in the bubble chart
    When I mouseover on the bubble positioned at "429" "393"
    Then the active bubble is in front with school name "Mount Pleasant High School"

  @RALLY_US32685
  Scenario: Validate the average and median lines on the growth cpop growth chart
    When I click on the "Growth" view on the cpop district report
    And I wait for the "Growth" view to be loaded
    Then I verify that there are radio buttons for toggle between parcc and state
    When I select the growth chart comparison option as "PARCC"
    Then I verify that there is a parcc avg line
    And I verify that there is a parcc median line
    When I select the growth chart comparison option as "STATE"
    Then I verify that there is a state avg line
    And I verify that there is a state median line

  @RALLY_US33183
  Scenario: Validate mouseover on the grid opens the popover in cpop(district) growth view
    When I click on the "Growth" view on the cpop district report
    And I wait for the "Growth" view to be loaded
    Then I verify that the subject selected is "ELA/L"
    When I select the growth chart comparison option as "PARCC"
    Then I see "2" circles in the bubble chart
    When I mouseover the grid at row number "1"
    Then the active bubble is in front with school name "Alvarez High School"
    And the popover is displayed with the following information:
      | name                  | value                            |
      | School                | Alvarez High School              |
      | STUDENTS              | 4                                |
      | AVG SCORE             | 362                              |
      | Subtitle              | Median Student Growth Percentile |
      | COMPARED TO PARCC     | 37 %tile                         |
      | COMPARED TO STATE     | 46 %tile                         |
    When I select the growth chart comparison option as "STATE"
    Then I see "2" circles in the bubble chart
    When I mouseover the grid at row number "3"
    Then the active bubble is in front with school name "Mount Pleasant High School"
    And the popover is displayed with the following information:
      | name                  | value                            |
      | School                | Mount Pleasant High School       |
      | STUDENTS              | 4                                |
      | AVG SCORE             | 143                              |
      | Subtitle              | Median Student Growth Percentile |
      | COMPARED TO PARCC     | 68 %tile                         |
      | COMPARED TO STATE     | 78 %tile                         |

  @RALLY_US31877
  Scenario: Verify that based on the checkbox selection, related sticky row should appear
    Then I should see the summary row for "DISTRICT"
    Then I should see the value "DISTRICT AVG" for "Providence" on summary row
    And I should see value "11" on summary row of "STUDENT" column
    When I select "Mathematics" from the subject dropdown
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | subject      | Mathematics  |
        | gradeCourse  | 3rd Grade    |
    Then I wait for the grid to reload
    Then I should see the value "DISTRICT AVG" for "Providence" on summary row
    And I should see value "3" on summary row of "STUDENT" column
    When I un check on DISTRICT checkbox
    When I check on DISTRICT, STATE checkbox
    Then I should see the summary row for "DISTRICT"
    Then I should see the summary row for "STATE"
    Then Verify "Rhode Island" should be the "first" summary row
    Then Verify "Providence" should be the "second" summary row

  @RALLY_US31877
  Scenario: Verify subscore distribution based on selecting different results from drop down
    When I select "Literary Text" from the results dropdown
    Then I should see "SCHOOL,STUDENTS,SUBSCORE DISTRIBUTION" columns for literary text
    And I should see the value "DISTRICT AVG" for "Providence" on summary row
    And I should see SUBSCORE DISTRIBUTION bar contains following data on first row
     | name | value |
     | 1    | 55%   |
     | 2    | 27%   |
     | 3    | 18%   |
    When I check on STATE checkbox
    Then I should see the value "STATE AVG" for "Rhode Island" on summary row
    And I should see SUBSCORE DISTRIBUTION bar contains following data on first row
     | name | value |
     | 1    | 50%   |
     | 2    | 20%   |
     | 3    | 30%   |
    # Enable when Parcc is ready for aggregates
    When I check on PARCC checkbox
    Then I should see the value "PARCC AVG" for "2 States" on summary row
    And I should see SUBSCORE DISTRIBUTION bar contains following data on first row
    |name |value|
    |1    |51%  |
    |2    |18%  |
    |3    |31%  |
    When I select "Mathematics" from the subject dropdown
    And I select "Major Content" from the results dropdown
    Then Verify "Providence" should be the "first" summary row

  @RALLY_US31881
  Scenario: Verify Results dropdowns
    Then I verify that I'm on the performance page
    When I click on the results dropdown
    Then I should see the "Summative,Non-Summative" level one headers in the RESULT dropdown
    Then I should see the "READING,WRITING" subheaders in the RESULT dropdown
    And I should see the "Overall Performance,Literary Text,Informational Text,Vocabulary,Writing Expression,Knowledge & Use of Language Conventions,Diagnostic Assessments" selectable elements in the RESULT dropdown
    When I select "Mathematics" from the subject dropdown
    And I click on the results dropdown
    Then I should see the "Summative,Non-Summative" level one headers in the RESULT dropdown
    And I should see the "SUB-SCORES" subheaders in the RESULT dropdown
    And I should see the "Overall Performance,Major Content,Additional & Supporting Content,Modeling and Application,Expressing Mathematical Reasoning,Diagnostic Assessments" selectable elements in the RESULT dropdown

@RALLY_US33166
  Scenario: Verify subscore distrubution for ELA
    When I click on GRADE/COURSE dropdown
    And I select "9th Grade" from GRADE/COURSE dropdown
    And I select "Literary Text" from the results dropdown
    Then I should see the value "Alvarez High School" in the first "SCHOOL" cell
    And I should see column "2" contains "33%67%" value at line "2"
    When I click on SCHOOL column for sorting
    Then I should see the value "Mount Pleasant High School" in the first "SCHOOL" cell
    And I should see column "2" contains "DATA SUPPRESSED TO PROTECT STUDENT PRIVACY" value at line "2"

@RALLY_US33166
  Scenario: Verify subscore distrubution for Mathematics
    When I select "Mathematics" from the subject dropdown
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | subject      | Mathematics  |
        | gradeCourse  | 3rd Grade    |
    Then I wait for the grid to reload
    When I click on GRADE/COURSE dropdown
    And I select "Int. Math III" from GRADE/COURSE dropdown
    And I select "Major Content" from the results dropdown
    Then I should see the value "Hope High School" in the first "SCHOOL" cell
    And I should see column "2" contains "46%39%15%" value at line "2"
    When I click on SCHOOL column for sorting
    Then I should see the value "Mount Pleasant High School" in the first "SCHOOL" cell
    And I should see column "2" contains "57%14%29%" value at line "2"

@RALLY_US34450
  Scenario: Verify year drop-down on report
    Then I should see "Academic Year: 2014 - 2015" on header next to PARCC
    When I click on "Academic Year" drop-down
    Then I should see following values in the year dropdown
     | value       |
     | 2014 - 2015 |
     | 2013 - 2014 |
    When I select "Mathematics" from the subject dropdown
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | subject      | Mathematics  |
        | gradeCourse  | 3rd Grade    |
    Then I wait for the grid to reload
    When I click on GRADE/COURSE dropdown
    And I select "Int. Math III" from GRADE/COURSE dropdown
    Then I should see "Academic Year: 2014 - 2015" on header next to PARCC
    And I should see the breadcrumbs "Home,Rhode Island,Providence"
    And I should see the subtitle "2 schools"
    And I should see the value "Hope High School" in the first "SCHOOL" cell
    And I should see the value "13" in the first "STUDENTS" cell
    When I select "2013 - 2014" from dropdown
    Then I should see "Academic Year: 2013 - 2014" on header next to PARCC
    And I should see the subtitle "1 school"
    And I should see the value "Hope High School" in the first "SCHOOL" cell
    And I should see the value "3" in the first "STUDENTS" cell

  @RALLY_US34456
  Scenario: Verify data suppression based on minimum cell size
  Then I verify that I'm on the performance page
  When I click on GRADE/COURSE dropdown
    And I select "9th Grade" from GRADE/COURSE dropdown
    Then I should see "SCHOOL,STUDENTS,PERFORMANCE DISTRIBUTION,≥ LVL 4,OVERALL,READING WRITING" columns for ela overall
    And I should see the value "3" in the first "STUDENTS" cell
    And I should see "Alvarez High School" value at field "1" on line "1"
    When I click on STUDENTS column for sorting
    Then I should see the value "2" in the first "STUDENTS" cell
    And I should see "DATA SUPPRESSED TO PROTECT STUDENT PRIVACY" value at field "3" on line "1"
    And I should see "Mount Pleasant High School" value at field "1" on line "1"
    When I select "Literary Text" from the results dropdown
    Then I should see "SCHOOL,STUDENTS,SUBSCORE DISTRIBUTION" columns for literary text
    And I should see the value "3" in the first "STUDENTS" cell
    And I should see "Alvarez High School" value at field "1" on line "1"
    When I click on STUDENTS column for sorting
    Then I should see the value "2" in the first "STUDENTS" cell
    And I should see "DATA SUPPRESSED TO PROTECT STUDENT PRIVACY" value at field "3" on line "1"
    And I should see "Mount Pleasant High School" value at field "1" on line "1"

  @RALLY_US34456
  Scenario: Verify data tooltip based on minimum cell size
  Then I verify that I'm on the performance page
  When I click on GRADE/COURSE dropdown
    When I select "9th Grade" from GRADE/COURSE dropdown
    And I hover on alert icon
    Then I should see the value "3" in the first "STUDENTS" cell
    And I should see "Alvarez High School" value at field "1" on line "1"
    And I should see message "This group has fewer than 5 students" on tooltip
    When I select "Literary Text" from the results dropdown
    And I click on SCHOOL column for sorting
    And I hover on alert icon
    Then I should see "Mount Pleasant High School" value at field "1" on line "1"
    Then I should not see alert icon on line "1"
    And I should see the value "2" in the first "STUDENTS" cell
    And I should see "DATA SUPPRESSED TO PROTECT STUDENT PRIVACY" value at field "3" on line "1"

  @RALLY_US34453
  Scenario: Verify student count is suppressed when filters are selected
   When I click on the Filters button to "open" the filter menu
   And I select "Female" options from the "Gender" filter
   And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
   Then I verify that the STUDENT column should be empty
   When I click the "Clear All" button
   Then I verify that the STUDENT column should not be empty

  @RALLY_US33939
  Scenario: Verify filter functionality in growth view
    When I click on the "Growth" view on the cpop district report
    And I wait for the "Growth" view to be loaded
    When I click on the Filters button to "open" the filter menu
    And I select "Female" options from the "Gender" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then the overall summary bar above growth chart contains the following values:
      | name         | value       |
      | DISTRIBUTION | 17%,33%,50% |

  @RALLY_US33939
  Scenario: Validate cpop(district) min cell size gray bar, bubble hiding, and alert icon/popup
    When I select "Mathematics" from the subject dropdown
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | subject      | Mathematics  |
        | gradeCourse  | 3rd Grade    |
    Then I wait for the grid to reload
    When I click on the "Growth" view on the cpop district report
    Then I wait for the grid to reload
    Then I verify that the subject selected is "Mathematics"
    And I should see the breadcrumbs "Home,Rhode Island,Providence"
    And I should see the report title "Providence"
    And I should see the subtitle "1 school"
    Then the school list grid has "SHOW/HIDE ALL checkbox,SCHOOL,STUDENTS" columns
    And I see "1" rows in the school list grid
    And the overall summary bar above growth chart contains gray bar which hides the avg and distribution
    And I see "0" circles in the bubble chart
    And I should see "Hope High School" value at field "1" on line "1"
    When I hover on alert icon
    Then I should see message "Growth data suppressed to protect student privacy" on alert popover

  @RALLY_US35029
  Scenario: Validate download button and popover
    When I click on the download link
    Then I verify that the download popup appears
    And I mouseover the download popup
    Then I verify that the download popup appears
    When I mouseout of the download popup
    Then I verify that the download popup does not appear

  Scenario: Validate result selector options between views
    When I click on the "Performance" view on the cpop district report
    And I wait for the "Performance" view to be loaded
    And I click on the results dropdown
    Then I should see "ELA" subscores in the results list
    When I click on the "Growth" view on the cpop district report
    And I wait for the "Growth" view to be loaded
    And I click on the results dropdown
    Then I should not see "ELA" subscores in the results list
    When I select "Mathematics" from the subject dropdown
    And I click on the results dropdown
    Then I should not see "MATH" subscores in the results list
    When I click on the "Performance" view on the cpop district report
    And I wait for the "Performance" view to be loaded
    And I click on the results dropdown
    Then I should see "MATH" subscores in the results list

  Scenario: Validate grid view is on both report views
    When I click on the "Performance" view on the cpop district report
    And I wait for the "Performance" view to be loaded
    Then I see the grade/course dropdown
    When I click on the "Growth" view on the cpop district report
    And I wait for the "Growth" view to be loaded
    Then I see the grade/course dropdown

  @RALLY_US34099
  @wip
  Scenario: Verify Comparing Populations District Report doesn't display suppressed reports
    Then I verify that I'm on the performance page
    When I open the cpop report with the following parameters:
      | param        | value        |
      | stateCode    | RI           |
      | districtGuid | R0001        |
      | year         | 2015         |
      | gradeCourse  | Grade 3      |
      | asmtType     | SUMMATIVE    |
      | subject      | subject2     |
      | view         | performance  |
    Then I should see the subtitle "0 schools"

   @RALLY_US34099
   Scenario: Verify District report does not display aggregate calculations suppressed at district level in Performance view
    When I open the cpop report with the following parameters:
      | param        | value     |
      | stateCode    | RI        |
      | districtGuid | R0003     |
      | gradeCourse  | Grade 9   |
      | asmtType     | SUMMATIVE |
      | subject      | subject2  |
      | year         | 2015      |
    And I check on STATE checkbox
    Then I should see following values in the summary row 1
      | value        |
      | STATE AVG    |
      | Rhode Island |
      | 12           |
      | 17%          |
      | 17%          |
      | 33%          |
      | 8%           |
      | 25%          |
      | 33%          |
      | 258          |
      | 28 / 50      |
      | 29 / 60      |
   Then I should see following values in the summary row 2
      | value        |
      | DISTRICT AVG |
      | Providence   |
      | 8            |
      | 25%          |
      | 25%          |
      | 25%          |
      | 25%          |
      | 25%          |
      | 231          |
      | 24 / 50      |
      | 26 / 60      |

  @RALLY_US34099
  Scenario: Verify District report does not display aggregate calculations suppressed at district level in Growth view
    When I open the cpop report with the following parameters:
      | param        | value     |
      | stateCode    | RI        |
      | subject      | subject2  |
      | districtGuid | R0003     |
      | gradeCourse  | Grade 9   |
      | asmtType     | SUMMATIVE |
      | year         | 2015      |
    And I click on the "Growth" view on the cpop district report
    Then the growth chart title is "Providence"
    And the overall summary bar above growth chart contains the following values:
      | name         | value           |
      | STUDENTS     | 8               |
      | AVG SCORE    | 231             |
      | DISTRIBUTION | 25%,25%,25%,25% |

  @RALLY_US31907
  Scenario: Verify District report displays PARCC aggregate in Performance View for ELA
    When I open the cpop report with the following parameters:
      | param        | value     |
      | stateCode    | RI        |
      | districtGuid | R0003     |
      | gradeCourse  | Grade 9   |
      | subject      | subject2  |
      | asmtType     | SUMMATIVE |
      | year         | 2015      |
    And I check on PARCC checkbox
    Then I should see following values in the summary row 1
      | value     |
      | PARCC AVG |
      | 2 States  |
      | 24        |
      | 17%       |
      | 17%       |
      | 33%       |
      | 8%        |
      | 25%       |
      | 33%       |
      | 258       |
      | 28 / 50   |
      | 29 / 60   |

  @RALLY_US31907
  Scenario: Verify District report displays PARCC aggregate in Performance View for Math
    When I open the cpop report with the following parameters:
      | param        | value     |
      | stateCode    | RI        |
      | districtGuid | R0003     |
      | asmtType     | SUMMATIVE |
      | year         | 2015      |
      | gradeCourse  | Geometry  |
      | subject      | subject1  |
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | Geometry     |
        | subject      | Mathematics  |
    And I check on PARCC checkbox
    Then I should see following values in the summary row 1
      | value     |
      | PARCC AVG |
      | 2 States  |
      | 14        |
      | 14%       |
      | 57%       |
      | 29%       |
      | 29%       |
      | 243       |

  @RALLY_DE1029
  Scenario: Verify that Growth View changes Result selector back to "Overall" from Subscore View
    Then I verify that I'm on the performance page
    When I select "Literary Text" from the results dropdown
    And I click on the "Growth" view on the cpop district report
    And I wait for the "Growth" view to be loaded
    Then the selected result in the results dropdown should read "Summative: Overall Performance "

  @RALLY_US36863
  Scenario: Verify result selector text present - District report
    When I click on the results dropdown
    Then I should see "- Available at school level" in the results list
