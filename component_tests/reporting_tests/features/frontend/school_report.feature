Feature: School report

  Background: Open a firefox browser and open the webpage
    Given an open browser
    When I open the school report with the following parameters
    | param        | value    |
    | subject      | subject2 |
    | stateCode    | RI       |
    | year         | 2015     |
    | districtGuid | R0001    |
    | schoolGuid   | RN001    |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the cpop school report

  @RALLY_US33714
  Scenario: Validate the grid, report titles, subtitle (number of grades/courses) on the school report
    When I select "ELA" from the subject dropdown
    Then I verify that the subject selected is "ELA/L"
    Then I wait for the grid to reload
    Then I should see the breadcrumbs "Home,Rhode Island,Newport,Rogers High School"
    And I should see the report title "Rogers High School"
    And I should see the subtitle "3 grades"
    And I should see "GRADE,PERFORMANCE DISTRIBUTION,STU- DENTS,≥ LVL 4,OVERALL,READING WRITING, VS STATE, VS PARCC" columns for ela overall
    And I verify that the first row is a grouped row
    When I select "Mathematics" from the subject dropdown
    Then I wait for the grid to reload
    Then I should see "ASSESSED GRADE/COURSE,PERFORMANCE DISTRIBUTION,STUDENTS,≥ LVL 4,AVG SCORE,VS STATE,VS PARCC" columns for math overall
    And I should see the subtitle "4 grades/courses"
    When I select "Major Content" from the results dropdown
    Then I wait for the grid to reload
    Then I should see "ASSESSED GRADE/COURSE,SUBSCORE DISTRIBUTION,STUDENTS," columns for major content

  @RALLY_US35465
  Scenario: Date Range Selector on School Report
    Then I should see the breadcrumbs "Home,Rhode Island,Newport,Rogers High School"
    When I select "Diagnostic Assessments" from the results dropdown
    Then I wait for the grid to reload
    When I click on DATE column for sorting
    Then I wait for the grid to reload
    Then I should see the report title "Rogers High School"
    And I verified all the records on report should be within the date range
    When I click on date range selector link
    And I enter "11/18/2014" date into "TO CALENDAR" edit field
    And I click on apply button
    Then I verified all the records on report should be within the date range
    When I click on reset date links
    And I click on DATE column for sorting
    Then I wait for the grid to reload
    Then I verified all the records on report should be within the date range
    When I click on date range selector link
    And I select a day 19 on "second" calendar
    And I click on apply button
    Then I validate total number of assessments should be 35
    Then I validate 35 students records in table
    When I click on reset date links
    Then I validate total number of assessments should be 40
    Then I validate 40 students records in table
    When I click on date range selector link
    And I click "next" arrow on "second" calendar
    And I select a day 19 on "second" calendar
    And I click on apply button
    Then I validate total number of assessments should be 35
    When I click on the Filters button to "open" the filter menu
    And I select "Female" options from the "Gender" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    Then I validate total number of assessments should be 30

  @RALLY_US33714
  Scenario: Validate the subscore distribution adds up to 100%
    When I select "ELA" from the subject dropdown
    Then I verify that the subject selected is "ELA/L"
    Then I wait for the grid to reload
    When I select "Literary Text" from the results dropdown
    Then I wait for the grid to reload
    Then I should see "GRADE,STUDENTS,SUBSCORE DISTRIBUTION," columns for literary text
    Then I verify subscore distribution column adds up to 100%

  @RALLY_US33714 @RALLY_US31818
  Scenario: Validate the number of grades matches the number rows in the grid with sorting
    When I select "ELA/L" from the subject dropdown
    Then I verify that the subject selected is "ELA/L"
    Then I wait for the grid to reload
    When I click on column "OVERALL"
    Then I verify that the grades are Grade 9, Grade 11, Grade 10
    When I select "Literary Text" from the results dropdown
    Then I wait for the grid to reload
    Then I should see the subtitle "3 grades"
    Then I verify the number of grades matches the number of grouped rows in the grid
    Then I verify that the grades are Grade 9, Grade 10, Grade 11
    When I click on column "GRADE"
    Then I verify that the grades are Grade 11, Grade 10, Grade 9
    When I select "Vocabulary" from the results dropdown
    Then I wait for the grid to reload
    When I click on column "SUBSCORE DISTRIBUTION"
    Then I verify that the grades are Grade 9, Grade 10, Grade 11
    When I select "Mathematics" from the subject dropdown
    Then I wait for the grid to reload
    Then I verify that the grades are Grade 5, Grade 8, Algebra I, Geometry
    When I click on column "ASSESSED GRADE/COURSE"
    Then I verify that the grades are Geometry, Algebra I, Grade 8, Grade 5
    When I check on DISTRICT checkbox
    Then I see 4 summary rows
    When I check on STATE checkbox
    Then I see 8 summary rows
    When I check on DISTRICT, STATE checkbox
    Then I see 0 summary rows

  @RALLY_US31820
  Scenario: Validate that compare checkboxes are functional
    Then I verify that the district, state checkboxes exist
    Then I verify that the compare rows are added

  @RALLY_US31830
  Scenario: Check that you can show and hide the legend
    Then I verify that the legend is shown by default
    Then I verify that the overall legend is shown
    Then I verify that the legend can be hidden and reshown
    When I select "Literary Text" from the results dropdown
    Then I verify that the subscore legend is shown

  @RALLY_US31823
  Scenario: Validate that the filters work on school report
    When I click on the Filters button to "open" the filter menu
    Then I verify that the filters menu is "open"
    When I verify the filtering options available on the filter menu
    And I select "Male" options from the "Gender" filter
    And I click on the "Cancel" button to "cancel" any changes and close the filter menu
    When I click on the Filters button to "open" the filter menu
    And I select "Male" options from the "Gender" filter
    And I select "Asian" options from the "Race / Ethnicity" filter
    And I select "Yes" options from the "Economic Disadvantages" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    When I click the "Edit" button
    Then I verify that the filters menu is "open"
    When I remove "Male" from the filter bar
    And I click the "Clear All" button
    Then I verify that no filters are shown
    When I click on the Filters button to "close" the filter menu
    Then I verify that the filters menu is "closed"
#
  @RALLY_US34450
  Scenario: Verify year drop-down on report
   Then I should see "Academic Year: 2014 - 2015" on header next to PARCC
   When I click on "Academic Year" drop-down
   Then I should see following values in the year dropdown
     | value       |
     | 2014 - 2015 |
     | 2013 - 2014 |
   When I click on "Academic Year" drop-down
   And I select "2013 - 2014" from dropdown
   Then I wait for the grid to reload
   And I should see "Academic Year: 2013 - 2014" on header next to PARCC

  @RALLY_US34718
   Scenario: Verify data suppression based on minimum cell size for Math
    And I should see "DATA SUPPRESSED TO PROTECT STUDENT PRIVACY" message for "Grade 9" on "SCHOOL" row
    When I select "Mathematics" from the subject dropdown
    Then I verify that the subject selected is "Mathematics"
    And I wait for the grid to reload
    And I should see "DATA SUPPRESSED TO PROTECT STUDENT PRIVACY" message for "Grade 5" on "SCHOOL" row
    When I click on GRADES toggle for "ALGEBRA I"
    Then I wait for the grid to reload
    Then I should see following values at row 7
     | value               |
     | 9TH GRADE STUDENTS |
     | DATA SUPPRESSED TO PROTECT STUDENT PRIVACY |
     | 2                                          |
     | DATA SUPPRESSED TO PROTECT STUDENT PRIVACY |
    #When I hover on alert icon
    #Then I should see message "This group has fewer than 5 students" on tooltip at row 11
    When I click on GRADES toggle for "ALGEBRA I"
    And I click on GRADES toggle for "GEOMETRY"
    Then I wait for the grid to reload
    Then I should see following values at row 1
     | value    |
     | Grade 5  |
    Then I should see following values at row 2
     | value                                      |
     | SCHOOL                                     |
     | DATA SUPPRESSED TO PROTECT STUDENT PRIVACY |
     | 1                                          |
     | DATA SUPPRESSED TO PROTECT STUDENT PRIVACY |
    Then I should see following values at row 9
     | value               |
     | 11TH GRADE STUDENTS |
     | 33%                 |
     | 67%                 |
     | 3                   |
     | 67%                 |
     | 289                 |
     | 42 %ile             |
     | 47 %ile             |
    When I scroll down the page
    When I hover on alert icon
    Then I should see message "This group has fewer than 5 students" on tooltip at row 8

  @RALLY_US34718
   Scenario: Verify student count is suppressed when filters are selected Math
    When I select "Mathematics" from the subject dropdown
    Then I verify that the subject selected is "Mathematics"
    And I wait for the grid to reload
    When I click on the Filters button to "open" the filter menu
    And I select "Female" options from the "Gender" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    Then I should see following values at row 2
     | value  |
     | SCHOOL |
     | 33%    |
     | 33%    |
     | 34%    |
     | 34%    |
     | 241    |
     | 49 %ile|
     | 49 %ile|
    When I hover on alert icon
    Then I should see message "This group has fewer than 5 students" on tooltip at row 2
    When I click the "Clear All" button
    Then I wait for the grid to reload
    Then I should see following values at row 2
     | value                                      |
     | SCHOOL                                     |
     | DATA SUPPRESSED TO PROTECT STUDENT PRIVACY |
     | 1                                          |
     | DATA SUPPRESSED TO PROTECT STUDENT PRIVACY |

  @RALLY_US34718
   Scenario: Verify data suppression based on minimum cell size for ELA
    And I verify that the subject selected is "ELA/L"
    And I wait for the grid to reload
    And I should see following values at row 3
     | value    |
     | Grade 10 |
    Then I should see following values at row 4
     | value    |
     | SCHOOL   |
     | 33%      |
     | 33%      |
     | 34%      |
     | 3        |
     | 67%      |
     | 334      |
     | 38 / 50  |
     | 37 / 60  |
     | 54 %ile  |
     | 51 %ile  |
    When I hover on alert icon
    Then I should see message "This group has fewer than 5 students" on tooltip at row 4

  @RALLY_US34453
  @RALLY_US34718
   Scenario: Verify student count is suppressed when filters are selected for ELA
    When I click on the Filters button to "open" the filter menu
    And I select "Female" options from the "Gender" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    And I should see following values at row 1
     | value    |
     | Grade 10 |
    Then I should see following values at row 2
     | value                                      |
     | SCHOOL                                     |
     | DATA SUPPRESSED TO PROTECT STUDENT PRIVACY |
     | DATA SUPPRESSED TO PROTECT STUDENT PRIVACY |

  @RALLY_US34718
   Scenario: Verify student count suppressed for subscore distribution for ELA
    When I select "Literary Text" from the results dropdown
    Then I wait for the grid to reload
    And I should see following values at row 3
     | value    |
     | Grade 10 |
    And I should see following values at row 4
     | value  |
     | SCHOOL |
     | 33%    |
     | 33%    |
     | 34%    |
     | 3      |
    When I hover on alert icon
    Then I should see message "This group has fewer than 5 students" on tooltip at row 4
    And I should see following values at row 2
     | value                                      |
     | SCHOOL                                     |
     | DATA SUPPRESSED TO PROTECT STUDENT PRIVACY |
     | 2                                          |

  @RALLY_US34718
   Scenario: Verify student count when filters are selected for subscore distribution for ELA
    When I select "Literary Text" from the results dropdown
    Then I wait for the grid to reload
    When I click on the Filters button to "open" the filter menu
    And I select "Female" options from the "Gender" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    And I should see following values at row 1
     | value    |
     | Grade 10 |
    Then I should see following values at row 2
     | value                                      |
     | SCHOOL                                     |
     | DATA SUPPRESSED TO PROTECT STUDENT PRIVACY |
    When I check on STATE checkbox
    Then I should see following values at row 3
     | value |
     | STATE |
     | 25%   |
     | 50%   |
     | 25%   |

  @RALLY_US34718
   Scenario: Verify alert when state and district checked for ELA
   When I check on DISTRICT, STATE checkbox
   And I hover on alert icon
   Then I should see message "This group has fewer than 5 students" on tooltip at row 6

  @RALLY_US34489
  Scenario: Verify growth percentile column
    When I hover over the info icon in the "vs state" column
    Then I should see message "Median student growth percentile compared to the state" on alert popover
    When I hover over the header
    And I hover over the info icon in the "vs parcc" column
    Then I should see message "Median student growth percentile compared to PARCC" on alert popover

    @RALLY_US34099
    Scenario: Verify aggregate calculations account for suppressed reports
      When I open the school report with the following parameters
        | param        | value    |
        | subject      | subject2 |
        | stateCode    | RI       |
        | year         | 2015     |
        | districtGuid | R0003    |
        | schoolGuid   | RP001    |
      And I am redirected to the cpop school report
      When I check on DISTRICT, STATE checkbox
      Then I should see following values at row 6
        | value  |
        | SCHOOL |
        | 33%    |
        | 67%    |
        | 3      |
        | 67%    |
        | 376    |
        |41 / 50 |
        |42 / 60 |
        |80 %ile |
        |40 %ile |
      Then I should see following values at row 7
        | value  |
        |DISTRICT|
        | 25%    |
        | 25%    |
        | 25%    |
        | 25%    |
        | 8      |
        | 25%    |
        | 231    |
        |24 / 50 |
        |26 / 60 |
        |57 %ile |
        |53 %ile |
      Then I should see following values at row 8
        | value  |
        | STATE  |
        | 17%    |
        | 17%    |
        | 33%    |
        | 8%     |
        | 25%    |
        | 12     |
        | 33%    |
        | 258    |
        |28 / 50 |
        |29 / 60 |
        |52 %ile |

    @RALLY_US31907
    Scenario: Verify PARCC aggregates are displayed for ELA
      When I open the school report with the following parameters
        | param        | value    |
        | subject      | subject2 |
        | stateCode    | RI       |
        | year         | 2015     |
        | districtGuid | R0003    |
        | schoolGuid   | RP001    |
      And I am redirected to the cpop school report
      When I check on DISTRICT, PARCC checkbox
      Then I should see following values at row 4
        | value  |
        | PARCC  |
        |  14%   |
        |  19%   |
        |  29%   |
        |  29%   |
        |  9%    |
        | 21     |
        | 38%    |
        | 250    |
        |31 / 50 |
        |29 / 60 |
      Then I should see following values at row 8
        | value  |
        | PARCC  |
        |  17%   |
        |  17%   |
        |  33%   |
        |  8%    |
        | 25%    |
        | 24     |
        | 33%    |
        | 258    |
        |28 / 50 |
        |29 / 60 |
      Then I should see following values at row 12
        | value  |
        | PARCC  |
        |  10%   |
        |  18%   |
        |  36%   |
        |  21%   |
        | 15%    |
        | 39     |
        | 36%    |
        | 265    |
        |30 / 50 |
        |30 / 60 |

    @RALLY_US31907
    Scenario: Verify PARCC aggregates are displayed for Math
      When I open the school report with the following parameters
        | param        | value    |
        | subject      | subject1 |
        | stateCode    | RI       |
        | year         | 2015     |
        | districtGuid | R0001    |
        | schoolGuid   | RN001    |
        | asmtType     | SUMMATIVE|
        | gradeCourse  | Algebra+I|
      And I am redirected to the cpop school report
      When I check on DISTRICT,PARCC checkbox
      Then I should see following values at row 4
        | value  |
        | PARCC  |
        | 50%    |
        | 50%    |
        | 4      |
        | 50%    |
        | 222    |
      Then I should see following values at row 8
        | value  |
        | PARCC  |
        |  80%   |
        |  20%   |
        |  10    |
        | 0%     |
        | 189    |
      Then I should see following values at row 12
        | value  |
        | PARCC  |
        |  20%   |
        |  13%   |
        |  13%   |
        |  54%   |
        | 15     |
        | 54%    |
        | 265    |

    @RALLY_US36863
    Scenario: Verify result selector text present - School report
      When I click on the results dropdown
      Then I should not see "- Available at school level" in the results list
