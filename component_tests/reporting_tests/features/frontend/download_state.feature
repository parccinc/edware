@browser
Feature: State comparing populations report

  Background: Open a firefox browser and open the webpage
    Given an open browser
    When I open the cpop report with the following parameters
      | param       | value     |
      | stateCode   | RI        |
      | year        | 2015      |
      | gradeCourse | Grade 11  |
      | subject     | subject2  |
      | asmtType    | SUMMATIVE |
    Then I am redirected to the login page
    When I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 11th Grade   |
        | subject      | ELA/L        |
    When I am redirected to the cpop state report

  Scenario: Verify CSV download for Comparing Populations State Report - ELA
    When I click on the download link
    And I click on the download csv link
    Then the filename matches the format for the "cpop state performance" report
    And I see the following CSV header information:
      | key            | value               |
      | Report Name    | Rhode Island State  |
      | Date/Time      | %m-%d-%Y %H:%M:%S   |
      | Institution    | Home > Rhode Island |
      | Academic Year  | 2014 - 2015         |
      | Subject        | ELA/L               |
      | Grade/Course   | Grade 11            |
      | Result         | Overall Performance |
      | View           | Performance         |
      | Filter Options |                     |
      | Total Count    | 3                   |
      | Sort By        | DISTRICT            |
    And I see the following CSV main information:
      | DISTRICT                 | STUDENTS | PERFORMANCE DISTRIBUTION                                                  | >= LEVEL 4 | AVG OVERALL | READING/WRITING              |
      | STATE AVG - Rhode Island | 20       | Level 1 - 10%, Level 2 - 20%, Level 3 - 35%, Level 4 - 20%, Level 5 - 15% | 35%         | 263         | Reading 30/50, Writing 29/60 |
      | East Greenwich           | 5        | Level 1 - 0%, Level 2 - 60%, Level 3 - 20%, Level 4 - 20%, Level 5 - 0%   | 20%         | 237         | Reading 23/50, Writing 23/60 |
      | Newport                  | 4        | Level 1 - 0%, Level 2 - 0%, Level 3 - 100%, Level 4 - 0%, Level 5 - 0%    | 0%          | 252         | Reading 27/50, Writing 26/60 |
      | Providence               | 11       | Level 1 - 18%, Level 2 - 9%, Level 3 - 18%, Level 4 - 27%, Level 5 - 28%  | 55%         | 278         | Reading 34/50, Writing 33/60 |
