@browser
Feature: School report

  Background: Open a firefox browser and open the webpage
    Given an open browser
    When I open the school report with the following parameters
    | param        | value    |
    | subject      | subject2 |
    | stateCode    | RI       |
    | year         | 2015     |
    | districtGuid | R0001    |
    | schoolGuid   | RN001    |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the cpop school report

  Scenario: Verify CSV download for School Report - ELA
    When I check on DISTRICT checkbox
    When I click on the download link
    And I click on the download csv link
    Then the filename matches the format for the "school" report
    And I see the following CSV header information:
      | key            | value                                              |
      | Report Name    | Rogers High School School Report                   |
      | Date/Time      | %m-%d-%Y %H:%M:%S                                  |
      | Institution    | Home > Rhode Island > Newport > Rogers High School |
      | Academic Year  | 2014 - 2015                                        |
      | Subject        | ELA/L                                              |
      | Grade/Course   |                                                    |
      | Result         | Overall Performance                                |
      | View           |                                                    |
      | Filter Options |                                                    |
      | Total Count    | 3                                                  |
      | Sort By        | GRADE                                              |
    And I see the following CSV main information:
      | GRADE    | PERFORMANCE DISTRIBUTION                                                | STUDENTS | >= LEVEL 4                                 | AVG OVERALL | READING/WRITING          | GROWTH VS STATE | GROWTH VS PARCC |
      | Grade 9  |                                                                         |          |                                            |         |                              |                 |                 |
      | school   | DATA SUPPRESSED TO PROTECT STUDENT PRIVACY                              | 2        | DATA SUPPRESSED TO PROTECT STUDENT PRIVACY |         |                              |                 |                 |
      | district | DATA SUPPRESSED TO PROTECT STUDENT PRIVACY                              | 2        | DATA SUPPRESSED TO PROTECT STUDENT PRIVACY |         |                              |                 |                 |
      | Grade 10 |                                                                         |          |                                            |         |                              |                 |                 |
      | school   | Level 1 - 0%, Level 2 - 0%, Level 3 - 33%, Level 4 - 33%, Level 5 - 34% | 3        | 67%                                        | 334     | Reading 38/50, Writing 37/60 | 54 %ile         | 51 %ile         |
      | district | Level 1 - 0%, Level 2 - 0%, Level 3 - 33%, Level 4 - 33%, Level 5 - 34% | 3        | 67%                                        | 334     | Reading 38/50, Writing 37/60 | 54 %ile         | 51 %ile         |
      | Grade 11 |                                                                         |          |                                            |         |                              |                 |                 |
      | school   | Level 1 - 0%, Level 2 - 0%, Level 3 - 100%, Level 4 - 0%, Level 5 - 0%  | 4        | 0%                                         | 252     | Reading 27/50, Writing 26/60 | 77 %ile         | 48 %ile         |
      | district | Level 1 - 0%, Level 2 - 0%, Level 3 - 100%, Level 4 - 0%, Level 5 - 0%  | 4        | 0%                                         | 252     | Reading 27/50, Writing 26/60 | 77 %ile         | 48 %ile         |
