@browser
Feature: State Level Data Export

  Background: Open a firefox browser and open the webpage
    Given an open browser
    When I open the cpop report with the following parameters
      | param        | value       |
      | stateCode    | RI          |
      | districtGuid | R0003       |
      | year         | 2015        |
      | gradeCourse  | Grade 3     |
      | asmtType     | SUMMATIVE   |
      | view         | performance |
      | subject      | subject2    |
    Then I am redirected to the login page
    When I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | ELA/L        |
    When I am redirected to the cpop district report

  @RALLY_US32587
    Scenario: Verify Help link on main header
    Then I should see "Help" link on header
    When I click on "Help" link on header
    Then I verified 4 links on "faq" tab

  @RALLY_US34868
  @RALLY_US31989
  @RALLY_US35248
    Scenario: Verify default setting for Export components
    Then I should see "Export Data" link on header
    When I click on "Export Data" link on header
    Then I should see modal title "Data Export"
    And I should see a message below headline
    And I should see "2015 - 2016" on Academic Year dropdown on modal
    When I click on Academic Year drop down
    Then I should see following items
     |value       |
     |2015 - 2016 |
     |2014 - 2015 |
     |2013 - 2014 |
    When I select "2014 - 2015" from Academic Year drop down
    When I select "Summative Psychometric Item File" from "EXPORT TYPE" drop down
    Then I should see "Export Data" button is enabled
    When I click on "Export Data" button
    Then I should see "Copy and paste the link below to access your download:" messages
    Then I should see a link for download

  @RALLY_US31989
  Scenario: Verify close button functionality without performing any export
    When I click on "Export Data" link on header
    When I click "Close" button
    Then I should see "Export Data" link on header

  @RALLY_US34868
  @RALLY_US31989
  @RALLY_US35248
    Scenario: Verify Export functionality for Summative Assessment Results for ELA
     When I click on "Export Data" link on header
     Then I should see modal title "Data Export"
     When I click on Academic Year drop down
     When I select "2014 - 2015" from Academic Year drop down
     And I select "Summative Assessment Results" from "EXPORT TYPE" drop down
     And I select "ELA/L" from "SUBJECT" drop down
    Then I should see "Export Data" button is disabled
     When I select "4th Grade,11th Grade" from "GRADE/COURSE" drop down
    Then I should see "Export Data" button is enabled
     When I click on "Export Data" button
     Then I should see "Copy and paste the link below to access your download:" messages
     Then I should see a link for download
    When I click "Close" button
    Then I should see "Export Data" link on header
    When I go to download url
    Then I wait until I see the download file
    And the name of the file begins with "AssmtResults_Summative_ELA_multigrade_2014-2015"
    Then I convert the file to compressed tar format
    When I untar the bulk extract file
    Then the untarred file has only 1 file which is a CSV

  @RALLY_US34868
  @RALLY_US31989
  @RALLY_US35248
    Scenario: Verify Export functionality for Summative Released Item File for Math
     When I click on "Export Data" link on header
     Then I should see modal title "Data Export"
     When I click on Academic Year drop down
     When I select "2014 - 2015" from Academic Year drop down
     And I select "Summative Released Item File" from "EXPORT TYPE" drop down
     And I select "Mathematics" from "SUBJECT" drop down
     And I select "5th Grade,Algebra I,Geometry" from "GRADE/COURSE" drop down
     And I click on "Export Data" button
     Then I should see "Copy and paste the link below to access your download:" messages
     Then I should see a link for download
    When I go to download url
    Then I wait until I see the download file
    And the name of the file begins with "ReleasedItemFile_Summative_Math_multigrade_2014-2015"
    Then I convert the file to compressed tar format
    When I untar the bulk extract file
    Then the untarred file has only 1 file which is a CSV

  @RALLY_US34868
  @RALLY_US31989
  @RALLY_US35248
    Scenario: Verify Export Button functionality for Export Type: Summative Psychometric Item File
     When I click on "Export Data" link on header
     Then I should see modal title "Data Export"
     When I click on Academic Year drop down
     When I select "2014 - 2015" from Academic Year drop down
     And I select "Summative Psychometric Item File" from "EXPORT TYPE" drop down
     And I click on "Export Data" button
     Then I should see "Copy and paste the link below to access your download:" messages
     Then I should see a link for download
    When I go to download url
    Then I wait until I see the download file
    And the name of the file begins with "PsychometricItemFile_2015"
    Then I convert the file to compressed tar format
    When I untar the bulk extract file
    Then the untarred file contains a CSV and a JSON

  @RALLY_US35020
    Scenario: Verify Export Button functionality for Export Type: CDS Extract
      When I click on "Export Data" link on header
      Then I should see modal title "Data Export"
      When I click on Academic Year drop down
      When I select "2014 - 2015" from Academic Year drop down
      And I select "State Data for PARCC-level Results" from "EXPORT TYPE" drop down
      And I click on "Export Data" button
      Then I should see "Copy and paste the link below to access your download:" messages
      Then I should see a link for download
      When I go to download url
      Then I wait until I see the download file
      And the name of the file begins with "CDSFile_2014-2015"
      Then I convert the file to compressed tar format
      When I untar the bulk extract file
      Then the untarred file has only 4 file which is a CSV

  @RALLY_US34868
  @RALLY_US31989
  @RALLY_US35248
    Scenario: Verify the perform another export without going to PARCC home page
     When I click on "Export Data" link on header
     Then I should see modal title "Data Export"
     When I click on Academic Year drop down
     When I select "2014 - 2015" from Academic Year drop down
     And I select "Summative Assessment Results" from "EXPORT TYPE" drop down
     And I select "Mathematics" from "SUBJECT" drop down
     And I select "Geometry" from "GRADE/COURSE" drop down
     And I click on "Export Data" button
     Then I should see "Copy and paste the link below to access your download:" messages
     Then I should see a link for download
    When I click on "click here" link
    Then I should see modal title "Data Export"
    When I go to download url
    Then I wait until I see the download file
    And the name of the file begins with "AssmtResults_Summative_Math_Geometry_2014-2015"
    Then I convert the file to compressed tar format
    When I untar the bulk extract file
    Then the untarred file contains a CSV and a JSON

  @RALLY_US35583
    Scenario: Verify Analytics link on main header
    Then I should see "Analytics" link on header

  @RALLY_DE1027
  @RALLY_DE1040
  Scenario: Verify Select All option in Grade/course and Export Data button visibility
    When I click on "Export Data" link on header
    Then I should see modal title "Data Export"
    When I click on Academic Year drop down
    When I select "2014 - 2015" from Academic Year drop down
    And I select "Summative Assessment Results" from "EXPORT TYPE" drop down
    And I select "ELA/L" from "SUBJECT" drop down
    When I select "Select All" from "GRADE/COURSE" drop down
    When I click on "Export Data" button
    Then I should see "Copy and paste the link below to access your download:" messages
    Then I should not see Export Data button
    When I click on "click here" link
    Then I wait for the grid to reload
    When I select "Summative Released Item File" from "EXPORT TYPE" drop down
    And I select "Mathematics" from "SUBJECT" drop down
    When I select "Select All" from "GRADE/COURSE" drop down
    When I click on "Export Data" button
    Then I should see "Copy and paste the link below to access your download:" messages
    Then I should not see Export Data button
    When I click "Close" button
    Then I should see "Export Data" link on header

  @RALLY_US37739
    Scenario: Verify item level and summative files in the Bulk Extract for CDS Review
      When I click on "Export Data" link on header
      Then I should see modal title "Data Export"
      When I click on Academic Year drop down
      When I select "2014 - 2015" from Academic Year drop down
      And I select "State Data for PARCC-level Results" from "EXPORT TYPE" drop down
      And I click on "Export Data" button
      Then I should see "Copy and paste the link below to access your download:" messages
      Then I should see a link for download
      When I go to download url
      Then I wait until I see the download file
      And the name of the file begins with "CDSFile_2014-2015"
      Then I convert the file to compressed tar format
      When I untar the bulk extract file
      Then the untarred file has only 4 file which is a CSV
      Then the CDS extract contains a file that begins with "CDSFile_ITEM_ELA_2014-2015"
      Then the CDS extract contains a file that begins with "CDSFile_ITEM_Math_2014-2015"
      Then the CDS extract contains a file that begins with "CDSFile_SUMMATIVE_ELA_2014-2015"
      Then the CDS extract contains a file that begins with "CDSFile_SUMMATIVE_Math_2014-2015"