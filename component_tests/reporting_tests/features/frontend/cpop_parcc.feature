Feature: PARCC comparing populations report

  Background: Open a firefox browser and open the webpage
    Given an open browser
    When I open the cpop report with the following parameters: 
      | param        | value     | 
      | gradeCourse  | Grade 3   | 
      | asmtType     | SUMMATIVE | 
      | subject      | subject2  |
      | year         | 2015      |
    Then I am redirected to the login page

  @RALLY_US31903
  Scenario: Verify view button display functionality on CPOP PARCC report
    When I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | ELA/L        |
    Then I verify that I'm on the performance page
    Then I wait for the grid to reload
    And I validate "All PARCC States" as the report title
    And I should see the value "New York" in the first "STATE" cell
    Then I wait for the grid to reload
    And I should see column "2" contains "10%20%30%30%10%" value at line "2"
    When I click on "first" link in the first grid column
    Then I wait for the grid to reload
    Then I should see column "2" contains "100%" value at line "2"
    Then I should see "view_button" on page is present
    When I select "Mathematics" from the subject dropdown
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | subject      | Mathematics  |
        | gradeCourse  | 3rd Grade    |
    Then I wait for the grid to reload
    Then I should see "view_button" on page is present
    When I click on "Home" link on breadcrumb
    Then I wait for the grid to reload
    Then I validate "All PARCC States" as the report title
    Then I wait for the grid to reload
    And I should see the value "New York" in the first "STATE" cell
    And I should see column "2" contains "100%" value at line "2"
    When I click on "first" link in the first grid column
    Then I wait for the grid to reload
    Then I should see column "2" contains "100%" value at line "2"
    Then I should see "view_button" on page is present
    When I click on "Home" link on breadcrumb
    Then I wait for the grid to reload
    Then I validate "All PARCC States" as the report title
    When I select "ELA" from the subject dropdown
    And I click on GRADE/COURSE dropdown
    And I select "11th Grade" from GRADE/COURSE dropdown
    Then I wait for the grid to reload
    And I validate "All PARCC States" as the report title
    And I should see the value "New York" in the first "STATE" cell
    And I should see column "2" contains "10%16%37%21%16%" value at line "2"

  @RALLY_US31903
  Scenario: view button should not display on CPOP PARCC report for PERFORMANCE DISTRIBUTION and Minimum cell size
    When I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | ELA/L        |
    Then I verify that I'm on the performance page
    When I select "Mathematics" from the subject dropdown
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | subject      | Mathematics  |
        | gradeCourse  | 3rd Grade    |
    Then I wait for the grid to reload
    And I validate "All PARCC States" as the report title
    And I should see "PARCC AVG" on first row
    When I hover on alert icon
    Then I should see message "This group has fewer than 5 students" on tooltip
    When I check on parcc checkbox
    Then I should see "New York" on first row
    When I check on parcc checkbox
    Then I should see "PARCC AVG" on first row

  @RALLY_US33951
  Scenario: Verify user can compare performance across states and can drill down a state that the user belongs to
    When I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | ELA/L        |
    Then I wait for the grid to reload
    Then I should see the report title "All PARCC States"
    And I should be able to drill down "3" states
    When I click on "Rhode Island"
    Then I should see the breadcrumbs "Home,Rhode Island"
    Then I should see the subtitle "3 districts"
    When I click on PARCC logo
    Then I should see the report title "All PARCC States"

  @not_gman
  @RALLY_US33951
  Scenario: Verify user can compare performance across states and cannot drill down a state that the user does not belong to
    When I login as msmith
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | ELA/L        |
    Then I wait for the grid to reload
    Then I should see the report title "All PARCC States"
    And I should not be able to drill down "1" states

  @not_gman
  @RALLY_US33951
  Scenario: Verify user can compare performance across states and cannot drill down a state that the user does not belong to and can drill down a state that the user belong to
    When I login as lwoman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | ELA/L        |
    Then I wait for the grid to reload
    Then I should see the report title "All PARCC States"
    And I should be able to drill down "1" states
    And I should not be able to drill down "2" states

  @RALLY_US36863
  Scenario: Verify result selector text present - PARCC report
    When I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | ELA/L        |
    Then I wait for the grid to reload
    When I click on the results dropdown
    Then I should see "- Available at school level" in the results list

  @RALLY_US37301
  Scenario: Verify cds opt out state shows NA in data columns - PARCC report
    When I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | ELA/L        |
    Then I wait for the grid to reload
    And I should see column "1" contains "N/A" value at line "4"
