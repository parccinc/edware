Feature: Student roster report

  Background: Open a firefox browser and open the webpage
    Given an open browser
    When I open the student roster report with the following parameters:
        | param        | value     |
        | stateCode    | RI        |
        | districtGuid | R0003     |
        | schoolGuid   | RP001     |
        | gradeCourse  | Grade 9   |
        | asmtType     | SUMMATIVE |
        | year         | 2015      |
        | subject      | subject2  |
        | view         | scores    |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the student roster report

  @RALLY_US33193
  @RALLY_US31210
  Scenario: Validate the Scores view on the student roster report
  When I click on the "Scores" view
  Then I should see the following breadcrumbs:
         | param    | value               |
         | home     | Home                |
         | state    | Rhode Island        |
         | district | Providence          |
         | school   | Alvarez High School |
         | grade    | Grade 9            |
  Then I should see the report title "Grade 9"
  Then I should see the subtitle "5 students"
  Then I see the legend with three performance level description

  @RALLY_US32665
  Scenario: Validate the Item analysis view on the student roster report
  When I click on the "Item Analysis" view
  Then I wait for the grid to reload
  Then I should see the following breadcrumbs:
         | param    | value               |
         | home     | Home                |
         | state    | Rhode Island        |
         | district | Providence          |
         | school   | Alvarez High School |
         | grade    | Grade 9            |
  Then I should see the report title "Grade 9"
  Then I should see the subtitle "5 students"

  @RALLY_DE1019
  Scenario: Validate compare bar is hidden on the Item analysis view when there is no data
  When I open the student roster report with the following parameters:
     | param        | value     |
     | stateCode    | RI        |
     | districtGuid | R0003     |
     | schoolGuid   | RP002     |
     | gradeCourse  | Grade 3   |
     | asmtType     | SUMMATIVE |
     | year         | 2015      |
     | view         | scores    |
  Then I wait for the grid to reload
  Then I should see the compare bar
  When I click on the "Item Analysis" view
  Then I wait for the grid to reload
  Then I should not see the compare bar

  @RALLY_US31199
  @RALLY_US33747
  Scenario: Validate the filters on the student roster report
    When I click on the "Item Analysis" view on the student roster report
    Then I wait for the grid to reload
    When I click on the Filters button to "open" the filter menu
    And I verify the filtering options available on the filter menu
    And I click on the "Cancel" button to "reject" any changes and close the filter menu
    When I click on the Filters button to "open" the filter menu
    And I select "Male,Not Specified" options from the "Gender" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    Then I should see the subtitle "4 students"
    When I click on the Filters button to "open" the filter menu
    And I select "Yes" options from the "Disabilities" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    Then I should see the subtitle "2 students"
    When I remove "Disabilities" from the filter bar
    Then I wait for the grid to reload
    Then I should see the subtitle "4 students"

  @RALLY_US34450
  Scenario: Verify year drop-down on report
    When I open the student roster report with the following parameters:
       | param        | value     |
       | stateCode    | RI        |
       | districtGuid | R0003     |
       | schoolGuid   | RP003     |
       | gradeCourse  | Grade 11  |
       | asmtType     | SUMMATIVE |
       | year         | 2015      |
       | view         | scores    |
    Then I wait for the grid to reload
    Then I should see "Academic Year: 2014 - 2015" on header next to PARCC
    And I should see the subtitle "5 students"
    And I should see the value "Augustus, Orval M." in the first "STUDENT" cell
    When I click on "Academic Year" drop-down
    Then I should see following values in the year dropdown
     | value       |
     | 2014 - 2015 |
     | 2013 - 2014 |
    When I click on "Academic Year" drop-down
    And I select "2013 - 2014" from dropdown
    Then I wait for the grid to reload
    And I should see "Academic Year: 2013 - 2014" on header next to PARCC
    And I should see the subtitle "3 students"
    And I should see the value "Augustus, Orval M." in the first "STUDENT" cell

  @RALLY_US31847
  Scenario: Verify results dropdown on report
    When I open the student roster report with the following parameters:
      | param        | value    |
      | subject      | subject2 |
      | gradeCourse  | Grade 11 |
      | stateCode    | RI       |
      | year         | 2015     |
      | districtGuid | R0003    |
      | schoolGuid   | RP003    |
    Then I wait for the grid to reload
    Then I should see "Summative: Overall Performance " on the results dropdown

  @RALLY_US34307
  Scenario: Verify breadcrumbs and report title for math courses
    Then I should see the following breadcrumbs:
      | param    | value               |
      | home     | Home                |
      | state    | Rhode Island        |
      | district | Providence          |
      | school   | Alvarez High School |
      | grade    | Grade 9             |
    When I open the student roster report with the following parameters:
      | param        | value     |
      | subject      | subject1  |
      | gradeCourse  | Algebra+I |
      | stateCode    | RI        |
      | year         | 2015      |
      | districtGuid | R0003     |
      | schoolGuid   | RP003     |
    Then I wait for the grid to reload
    Then I should see the following breadcrumbs:
      | param    | value            |
      | home     | Home             |
      | state    | Rhode Island     |
      | district | Providence       |
      | school   | Hope High School |
      | grade    | Algebra I        |
    And I should see Algebra I as the report title

  @RALLY_US34489
  Scenario: Verify growth percentile column
    When I click on the "Scores" view
    And I hover over the info icon in the "vs state" column
    Then I should see message "Median student growth percentile compared to the state" on alert popover
    When I hover over the header
    And I hover over the info icon in the "vs parcc" column
    Then I should see message "Median student growth percentile compared to PARCC" on alert popover

    @RALLY_US34099
    Scenario: Verify Student Roster Report doesn't display reports suppressed for the scores view in scores view but does show them in item analysis
    Then I should see the subtitle "5 students"
    When I click on the "Item Analysis" view
    Then I wait for the grid to reload
    Then I should see the subtitle "5 students"

    @RALLY_US34099
    Scenario: Verify aggregate calculations account for suppressed reports
      When I open the student roster report with the following parameters:
        | param        | value     |
        | stateCode    | RI        |
        | year         | 2015      |
        | districtGuid | R0003     |
        | schoolGuid   | RP001     |
        | gradeCourse  | Grade 9   |
        | asmtType     | SUMMATIVE |
        | view         | scores    |
      Then I wait for the grid to reload
      When I check on STATE checkbox
      When I check on DISTRICT checkbox
      Then I should see following values in the summary row 1
        | value        |
        | STATE AVG    |
        | Rhode Island |
        | 258          |
        | 28           |
        | 29           |
        | 52 %ile      |

  @RALLY_US35496
  Scenario: Validate pagination functionality
    When I click on the "Item Analysis" view
    Then I should verify 7 cols in "NEXT" pagination link
    When I click on "NEXT" pagination
    Then I should verify 3 cols in "NEXT" pagination link
    And I should see 8 columns in the table
    And I should verify 7 cols in "PREVIOUS" pagination link
    When I click on "PREVIOUS" pagination
    Then I should verify 7 cols in "NEXT" pagination link
    When I click on "NEXT" pagination
    And I click on "NEXT" pagination
    Then I should see 4 columns in the table

  @RALLY_US34099
    Scenario: Verify aggregate calculations account for suppressed reports
      When I check on STATE checkbox
      When I check on DISTRICT checkbox
      Then I should see following values in the summary row 1
        | value        |
        | STATE AVG    |
        | Rhode Island |
        | 258          |
        | 28           |
        | 29           |
        | 52 %ile      |

  @RALLY_US31907
    Scenario: Verify PARCC aggregate calculations are displayed in Scores view for ELA
      When I check on PARCC checkbox
      Then I should see following values in the summary row 1
        | value     |
        | PARCC AVG |
        | 2 States  |
        | 258       |
        | 28        |
        | 29        |

  @RALLY_US31907
    Scenario: Verify PARCC aggregate calculations are displayed in Item Analysis view for ELA
      When I open the student roster report with the following parameters:
        | param        | value        |
        | stateCode    | RI           |
        | year         | 2015         |
        | districtGuid | R0003        |
        | schoolGuid   | RP001        |
        | gradeCourse  | Grade 9      |
        | asmtType     | SUMMATIVE    |
        | view         | itemAnalysis |
      Then I wait for the grid to reload
      When I check on PARCC checkbox
      Then I should see following values in the summary row 1
        | value     |
        | PARCC AVG |
        | 2 States  |
        | 376       |
        | 6         |
        | 5         |
        | 2         |
        | 2         |
        | 4         |
      When I click on "NEXT" pagination
      Then I should see following values in the summary row 1
        | value     |
        | PARCC AVG |
        | 2 States  |
        | 1         |
        | 2         |
        | 7         |
        | 2         |
        | 0         |
        | 5         |
        | 2         |
      When I click on "NEXT" pagination
      Then I should see following values in the summary row 1
        | value     |
        | PARCC AVG |
        | 2 States  |
        | 6         |
        | 0         |
        | 6         |

  @RALLY_US31907
    Scenario: Verify PARCC aggregate calculations are displayed in Scores view for Math
      When I open the student roster report with the following parameters:
        | param        | value     |
        | stateCode    | RI        |
        | year         | 2015      |
        | districtGuid | R0001     |
        | schoolGuid   | RN001     |
        | asmtType     | SUMMATIVE |
        | view         | scores    |
        | gradeCourse  | Algebra+I |
        | subject      | subject1  |
      Then I wait for the grid to reload
      When I check on PARCC checkbox
      Then I should see following values in the summary row 1
        | value     |
        | PARCC AVG |
        | 2 States  |
        | 265       |

    @RALLY_US31907
    Scenario: Verify PARCC aggregate calculations are displayed in Item Analysis view for Math
      When I open the student roster report with the following parameters:
        | param        | value        |
        | stateCode    | RI           |
        | year         | 2015         |
        | districtGuid | R0001        |
        | schoolGuid   | RN001        |
        | asmtType     | SUMMATIVE    |
        | view         | itemAnalysis |
        | gradeCourse  | Algebra+I    |
        | subject      | subject1     |
      Then I wait for the grid to reload
      When I check on PARCC checkbox
      Then I should see following values in the summary row 1
        | value     |
        | PARCC AVG |
        | 2 States  |
        | 272       |
        | 5         |
        | 5         |
        | 5         |
        | 6         |
        | 6         |

    @RALLY_US36337
    Scenario: Verify Student Roster Report displays student grade and not assessment grade
      When I open the student roster report with the following parameters:
        | param        | value     |
        | stateCode    | RI        |
        | year         | 2015      |
        | districtGuid | R0001     |
        | schoolGuid   | RN001     |
        | asmtType     | SUMMATIVE |
        | view         | scores    |
        | gradeCourse  | Grade 8   |
        | subject      | subject1  |
      Then I wait for the grid to reload
      Then I should see following values at row 1
        | value                  |
        | Mccreery, Jacquline B. |
        | 10                     |
        | 270                    |
        | 98 %ile                |
        | 62 %ile                |

    Scenario: Verify breadcrumb - school link
    Then I should see the breadcrumbs "Home,Rhode Island,Providence,Alvarez High School,Grade 9"
    When I click on "Alvarez High School" link on breadcrumb
    Then I wait for the grid to reload
    And I should see the subtitle "3 grades"

    Scenario: Verify breadcrumb - district link
    Then I should see the breadcrumbs "Home,Rhode Island,Providence,Alvarez High School,Grade 9"
    When I click on "Providence" link on breadcrumb
    Then I wait for the grid to reload
    And I should see the subtitle "3 schools"

  Scenario: Verify breadcrumb - state link
    Then I should see the breadcrumbs "Home,Rhode Island,Providence,Alvarez High School,Grade 9"
    When I click on "Rhode Island" link on breadcrumb
    Then I wait for the grid to reload
    And I should see the subtitle "3 districts"

  Scenario: Verify breadcrumb - Home link
    Then I should see the breadcrumbs "Home,Rhode Island,Providence,Alvarez High School,Grade 9"
    When I click on "Home" link on breadcrumb
    Then I wait for the grid to reload
    And I should see the subtitle "3 states"

  @RALLY_US36863
  Scenario: Verify result selector text present - PARCC report
    When I click on the results dropdown
    Then I should see "- Available at school level" in the results list

  @RALLY_US36539
 Scenario:Validate comparison row performance bar hover - ELA
    When I open the student roster report with the following parameters:
          | param        | value     |
          | stateCode    | RI        |
          | districtGuid | R0003     |
          | schoolGuid   | RP003     |
          | gradeCourse  | Grade 9   |
          | asmtType     | SUMMATIVE |
          | year         | 2015      |
          | view         | scores    |
    Then I wait for the grid to reload
    When I hover on "Literary Text"
    Then I should see performance distribution "33%,67%"
    When I hover on "Informational Text"
    When I wait for earlier popover to disappear
    Then I should see performance distribution "67%,33%"
    When I hover on "Vocabulary"
    When I wait for earlier popover to disappear
    Then I should see performance distribution "33%,67%"
    When I hover on "Writing Expression"
    When I wait for earlier popover to disappear
    Then I wait for the grid to reload
    Then I should see performance distribution "33%,67%"
    When I hover on "Knowledge and Convention"
    When I wait for earlier popover to disappear
    Then I should see performance distribution "33%,67%"
    When I check on PARCC checkbox
    Then I wait for the grid to reload
    When I hover on "Literary Text"
    When I wait for earlier popover to disappear
    Then I should see performance distribution "25%,33%,42%"
    When I hover on "Informational Text"
    When I wait for earlier popover to disappear
    Then I should see performance distribution "25%,42%,33%"
    When I hover on "Vocabulary"
    When I wait for earlier popover to disappear
    Then I should see performance distribution "17%,41%,42%"
    When I hover on "Writing Expression"
    When I wait for earlier popover to disappear
    Then I wait for the grid to reload
    Then I should see performance distribution "17%,41%,42%"
    When I hover on "Knowledge and Convention"
    When I wait for earlier popover to disappear
    Then I should see performance distribution "33%,25%,42%"

  @RALLY_US36539
 Scenario:Validate comparison row performance bar hover - Math
     When I open the student roster report with the following parameters:
        | param        | value          |
        | stateCode    | RI             |
        | year         | 2015           |
        | districtGuid | R0003          |
        | schoolGuid   | RP002          |
        | asmtType     | SUMMATIVE      |
        | view         | scores         |
        | gradeCourse  | Integrated+Mathematics+III|
        | subject      | subject1       |
    Then I wait for the grid to reload
    When I hover on "Major Content"
    Then I should see performance distribution "57%,14%,29%"
    When I hover on "Additional & Supporting"
    When I wait for earlier popover to disappear
    Then I should see performance distribution "57%,43%"
    When I hover on "Expressing Reasoning"
    When I wait for earlier popover to disappear
    Then I should see performance distribution "29%,14%,57%"
    When I hover on "Modeling & Application"
    When I wait for earlier popover to disappear
    Then I should see performance distribution "29%,57%,14%"
    When I check on PARCC checkbox
    Then I wait for the grid to reload
    When I hover on "Major Content"
    Then I should see performance distribution "50%,32%,18%"
    When I hover on "Additional & Supporting"
    When I wait for earlier popover to disappear
    Then I should see performance distribution "26%,26%,48%"
    When I hover on "Expressing Reasoning"
    When I wait for earlier popover to disappear
    Then I should see performance distribution "31%,24%,45%"
    When I hover on "Modeling & Application"
    When I wait for earlier popover to disappear
    Then I wait for the grid to reload
    Then I should see performance distribution "26%,50%,24%"

@RALLY_US33215
 Scenario:Verify students without scores are displayed on Student Roster report - ELA
    When I open the student roster report with the following parameters:
          | param        | value     |
          | stateCode    | RI        |
          | districtGuid | R0003     |
          | schoolGuid   | RP001     |
          | gradeCourse  | Grade 11  |
          | asmtType     | SUMMATIVE |
          | year         | 2015      |
          | view         | scores    |
    Then I wait for the grid to reload
    And I should see "Gatti, Shane S." is not a link
    And I should see following values at row 3
          | value                                 |
          | Gatti, Shane S.                       |
          | STUDENT DID NOT RECEIVE A VALID SCORE |
    And I should see "Troy, Verena W." is not a link
    And I should see following values at row 6
          | value                                 |
          | Troy, Verena W.                       |
          | STUDENT DID NOT RECEIVE A VALID SCORE |

  @RALLY_US33215
 Scenario:Verify students without scores are displayed on Student Roster report - Math
    When I open the student roster report with the following parameters:
          | param        | value     |
          | stateCode    | RI        |
          | districtGuid | R0003     |
          | schoolGuid   | RP001     |
          | gradeCourse  | Geometry  |
          | asmtType     | SUMMATIVE |
          | year         | 2015      |
          | view         | scores    |
          | subject      | subject1  |
    Then I wait for the grid to reload
    And I should see "Cabello, Jacquline O." is not a link
    And I should see following values at row 1
          | value                                 |
          | Cabello, Jacquline O.                 |
          | STUDENT DID NOT RECEIVE A VALID SCORE |
    And I should see "Jewell, Betty S." is not a link
    And I should see following values at row 5
          | value                                 |
          | Jewell, Betty S.                      |
          | STUDENT DID NOT RECEIVE A VALID SCORE |
