Feature: State comparing populations report

  Background: Open a firefox browser and open the webpage
    Given an open browser
    When I open the cpop report with the following parameters
      | param       | value     |
      | stateCode   | RI        |
      | year        | 2015      |
      | gradeCourse | Grade 11  |
      | subject     | subject2  |
      | asmtType    | SUMMATIVE |
    Then I am redirected to the login page
    When I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 11th Grade   |
        | subject      | ELA/L        |
    When I am redirected to the cpop state report

  @RALLY_US31889
  Scenario: Validate the grid, report titles, subtitle (number of students) on the cpop state report
    Then I verify that I'm on the performance page
    Then I should see the breadcrumbs "Home,Rhode Island"
    Then I should see the report title "Rhode Island"
    Then I should see the subtitle "3 districts"
    Then I should see "DISTRICT,STUDENTS,PERFORMANCE DISTRIBUTION,≥ LVL 4,OVERALL,READING WRITING" columns for ela overall

  @RALLY_US31889
  Scenario: Validate that the subscore avg is only for ELA
    Then I verify that I'm on the performance page
    Then I verify that the subject selected is "ELA/L"
    Then I should see a subscore avg column "is present"
    When I select "Mathematics" from the subject dropdown
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade   |
        | subject      | Mathematics  |
    Then I wait for the grid to reload
    Then I should see a subscore avg column "is not present"

  @RALLY_US31889
  @RALLY_US33946
  Scenario: Validate grid sorting
    Then I verify that I'm on the performance page
    And I should see the value "East Greenwich" in the first "DISTRICT" cell
    When I click on DISTRICT column for sorting
    Then I should see the value "Providence" in the first "DISTRICT" cell
    When I click on STUDENTS column for sorting
    Then I should see the value "4" in the first "STUDENTS" cell
    When I click on STUDENTS column for sorting
    Then I should see the value "11" in the first "STUDENTS" cell
    When I click on PERFORMANCE DISTRIBUTION column for sorting
    Then I should see the value "Newport" in the first "DISTRICT" cell
    When I click on PERFORMANCE DISTRIBUTION column for sorting
    Then I should see the value "Providence" in the first "DISTRICT" cell
    When I select "Literary Text" from the results dropdown
    Then I wait for the grid to reload
    And I should see the value "5" in the first "STUDENTS" cell
    When I click on SUBSCORE DISTRIBUTION column for sorting
    Then I should see the value "Providence" in the first "DISTRICT" cell
    When I click on SUBSCORE DISTRIBUTION column for sorting
    Then I should see the value "Newport" in the first "DISTRICT" cell

  @RALLY_US31889
  Scenario: Validate the level4 percentage, avg and subscore avg for ELA
    Then I verify that I'm on the performance page
    And I verify lvl4 percentage for first district
    And I verify average score for first district is 237
    And I verify the "reading" avg subscore for first district is "23 / 50"
    And I verify the "writing" avg subscore for first district is "23 / 60"

  @RALLY_US31889
  Scenario: Validate grid has correct data for Math
    Then I verify that I'm on the performance page
    When I select "Mathematics" from the subject dropdown
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | subject      | Mathematics  |
        | gradeCourse  | 3rd Grade    |
    Then I wait for the grid to reload
    When I click on DISTRICT column for sorting
    Then I wait for the grid to reload
    Then I verify average score for first district is 146

  @RALLY_US33940
  Scenario: Validate the legend bar has different values for different grades
    Then I verify that I'm on the performance page
    And I should see the subtitle "3 districts"
    And I see the legend with description for the five ALD levels on the performance bar
    When I click on GRADE/COURSE dropdown
    And I select "9th Grade" from GRADE/COURSE dropdown
    Then I wait for the grid to reload
    Then I should see the subtitle "3 districts"
    And I see the legend with description for the five ALD levels on the performance bar

  @RALLY_US31889
  Scenario: Validate the number of district matches the number rows in the grid
    Then I verify that I'm on the performance page
    And I verify that the subject selected is "ELA/L"
    And I should see the subtitle "3 districts"
    And I verify the number of districts matches the number of rows in the grid

  @RALLY_US34137
  Scenario: With filters
  Then I verify that I'm on the performance page
  When I click on the Filters button to "open" the filter menu
  And I select "Female" options from the "Gender" filter
  And I select "Yes" options from the "Disabilities" filter
  And I select "ELL" options from the "ELL / LEP" filter
  And I select "Yes" options from the "Economic Disadvantages" filter
  And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
  Then I wait for the grid to reload
  And I should see the subtitle "2 districts"

  @RALLY_US31877
  Scenario: Verify that based on the checkbox selection, related sticky row should appear
    Then I should see the summary row for "STATE"
    Then I should see the value "STATE AVG" for "Rhode Island" on summary row
    And I should see value "20" on summary row of "STUDENT" column
    When I select "Mathematics" from the subject dropdown
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | subject      | Mathematics  |
        | gradeCourse  | 3rd Grade    |
    Then I wait for the grid to reload
    Then I should see the value "STATE AVG" for "Rhode Island" on summary row
    And I should see value "3" on summary row of "STUDENT" column
    When I un check on STATE checkbox
    # Enable when Parcc aggregates are done
    #When I check on PARCC checkbox
    #Then I should see the summary row for "PARCC"
    #Then I should see the value "PARCC AVG" for "2 States" on summary row
    #And I should see value "1200" on summary row of "STUDENT" column
    #When I select "ELA" from the subject dropdown
    #Then I wait for the grid to reload
    #Then I should see the value "PARCC AVG" for "2 States" on summary row
    #And I should see value "1200" on summary row of "STUDENT" column
    #When I un check on PARCC checkbox
    #Then I should see the summary row for "NONE"
    #When I check on STATE, PARCC checkbox
    #Then I should see the summary row for "STATE"
    #Then I should see the summary row for "PARCC"
    #Then Verify "2 States" should be the "first" summary row
    #Then Verify "Rhode Island" should be the "second" summary row
    When I select "ELA" from the subject dropdown
    Then I wait for the grid to reload
    When I click on GRADE/COURSE dropdown
    And I select "11th Grade" from GRADE/COURSE dropdown
    Then I wait for the grid to reload
    And I should see "11th Grade " on GRADE/COURSE dropdown
    And I should see the value "East Greenwich" in the first "DISTRICT" cell
    When I select "Mathematics" from the subject dropdown
    Then I wait for the grid to reload
    Then I should see "3rd Grade " on GRADE/COURSE dropdown
    Then Verify "Rhode Island" should be the "first" summary row


  @RALLY_US31877
  Scenario: Verify subscore distribution based on selecting different results from drop down
    When I select "Literary Text" from the results dropdown
    Then I wait for the grid to reload
    And I should see "DISTRICT,STUDENTS,SUBSCORE DISTRIBUTION" columns for literary text
    And I should see the value "STATE AVG" for "Rhode Island" on summary row
    And I should see SUBSCORE DISTRIBUTION bar contains following data on first row
     | name | value |
     | 1    | 50%   |
     | 2    | 20%   |
     | 3    | 30%   |
    Then I should see the value "East Greenwich" in the first "DISTRICT" cell
    #When I check on PARCC checkbox
    #Then I should see the value "PARCC AVG" for "2 States" on summary row
    #Then Verify "Rhode Island" should be the "second" summary row
    #And I should see SUBSCORE DISTRIBUTION bar contains following data on first row
    # |name |value|
    # |1    |15%  |
    # |2    |35%  |
    # |3    |50%  |
   Then I should see the value "East Greenwich" in the first "DISTRICT" cell

  @RALLY_US33166
   Scenario: Verify subscore distrubution for ELA
    When I click on GRADE/COURSE dropdown
    And I select "9th Grade" from GRADE/COURSE dropdown
    Then I wait for the grid to reload
    When I select "Literary Text" from the results dropdown
    Then I wait for the grid to reload
    And I should see the value "East Greenwich" in the first "DISTRICT" cell
    And I should see column "2" contains "DATA SUPPRESSED TO PROTECT STUDENT PRIVACY" value at line "2"
    When I click on DISTRICT column for sorting
    Then I should see the value "Providence" in the first "DISTRICT" cell
    And I should see column "2" contains "12%50%38%" value at line "2"

  @RALLY_US33166
   Scenario: Verify subscore distrubution for Mathematics
    When I select "Mathematics" from the subject dropdown
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | Mathematics  |
    Then I wait for the grid to reload
    When I click on GRADE/COURSE dropdown
    And I select "Geometry" from GRADE/COURSE dropdown
    Then I wait for the grid to reload
    When I select "Major Content" from the results dropdown
    Then I wait for the grid to reload
    And I should see the value "East Greenwich" in the first "DISTRICT" cell
    And I should see column "2" contains "DATA SUPPRESSED TO PROTECT STUDENT PRIVACY" value at line "2"
    When I click on DISTRICT column for sorting
    Then I should see the value "Providence" in the first "DISTRICT" cell
    And I should see column "2" contains "33%33%34%" value at line "2"

 @RALLY_US34450
  Scenario: Verify year drop-down on report
    And I should see "Academic Year: 2014 - 2015" on header next to PARCC
    And I should see the subtitle "3 districts"
    And I should see the value "East Greenwich" in the first "DISTRICT" cell
    And I should see the value "5" in the first "STUDENTS" cell
    When I click on "Academic Year" drop-down
    Then I should see following values in the year dropdown
     | value       |
     | 2014 - 2015 |
     | 2013 - 2014 |
    When I click on "Academic Year" drop-down
    And I select "2013 - 2014" from dropdown
    Then I wait for the grid to reload
    And I should see "Academic Year: 2013 - 2014" on header next to PARCC
    And I should see the subtitle "1 district"
    And I should see the value "Providence" in the first "DISTRICT" cell
    And I should see the value "3" in the first "STUDENTS" cell

  @RALLY_US34456
  Scenario: Verify data suppression based on minimum cell size
  Then I verify that I'm on the performance page
  When I click on GRADE/COURSE dropdown
    And I select "9th Grade" from GRADE/COURSE dropdown
    Then I wait for the grid to reload
    And I should see the value "2" in the first "STUDENTS" cell
    And I should see "East Greenwich" value at field "1" on line "1"
    And I should see "DATA SUPPRESSED TO PROTECT STUDENT PRIVACY" value at field "3" on line "1"
    When I click on DISTRICT column for sorting
    Then I should see the value "8" in the first "STUDENTS" cell
    And I should see "Providence" value at field "1" on line "1"
    When I select "Literary Text" from the results dropdown
    Then I wait for the grid to reload
    And I should see the value "2" in the first "STUDENTS" cell
    And I should see "DATA SUPPRESSED TO PROTECT STUDENT PRIVACY" value at field "3" on line "1"
    And I should see "East Greenwich" value at field "1" on line "1"
    When I click on DISTRICT column for sorting
    Then I should see the value "8" in the first "STUDENTS" cell
    And I should see "Providence" value at field "1" on line "1"

  @RALLY_US34456
  Scenario: Verify data tooltip based on minimum cell size
  Then I verify that I'm on the performance page
  When I click on GRADE/COURSE dropdown
    And I select "11th Grade" from GRADE/COURSE dropdown
    Then I should not see alert icon on line "1"
    When I click on PERFORMANCE DISTRIBUTION column for sorting
    And I hover on alert icon
    Then I wait for the grid to reload
    And I should see the value "4" in the first "STUDENTS" cell
    And I should see "Newport" value at field "1" on line "1"
    And I should see message "This group has fewer than 5 students" on tooltip
    When I select "Literary Text" from the results dropdown
    Then I wait for the grid to reload
    When I click on STUDENTS column for sorting
    And I hover on alert icon
    Then I should see "Newport" value at field "1" on line "1"
    And I should see the value "4" in the first "STUDENTS" cell
    And I should see message "This group has fewer than 5 students" on tooltip

  @RALLY_US34099
  Scenario: Verify Comparing Populations State Report doesn't display suppressed reports
  Then I verify that I'm on the performance page
  When I select "ELA" from the subject dropdown
    And I click on GRADE/COURSE dropdown
    And I select "3rd Grade" from GRADE/COURSE dropdown
    Then I should see the subtitle "3 districts"

  @RALLY_US34453
  Scenario: Verify student count is suppressed when filters are selected
   When I click on the Filters button to "open" the filter menu
   And I select "Female" options from the "Gender" filter
   And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
   Then I wait for the grid to reload
   And I verify that the STUDENT column should be empty
   When I click the "Clear All" button
   Then I wait for the grid to reload
   And I verify that the STUDENT column should not be empty

  @RALLY_US33939
  Scenario: Verify filter functionality in growth view
    When I click on the "Growth" view on the cpop district report
    And I wait for the "Growth" view to be loaded
    When I click on the Filters button to "open" the filter menu
    And I select "Female" options from the "Gender" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    And the overall summary bar above growth chart contains the following values:
      | name         | value           |
      | DISTRIBUTION | 18%,27%,27%,28% |

  @RALLY_US34099
  Scenario: Verify State report does not display aggregate calculations suppressed at state level in Performance view
    When I open the cpop report with the following parameters:
      | param       | value     |
      | stateCode   | RI        |
      | gradeCourse | Grade 9   |
      | asmtType    | SUMMATIVE |
      | year        | 2015      |
    Then I should see following values in the summary row 1
      | value        |
      | STATE AVG    |
      | Rhode Island |
      | 12           |
      | 17%          |
      | 17%          |
      | 33%          |
      | 8%           |
      | 25%          |
      | 33%          |
      | 258          |
      | 28 / 50      |
      | 29 / 60      |

  @RALLY_US34099
  Scenario: Verify Sate report does not display aggregate calculations suppressed at State level in Growth view
    When I open the cpop report with the following parameters:
      | param       | value     |
      | stateCode   | RI        |
      | gradeCourse | Grade 9   |
      | asmtType    | SUMMATIVE |
      | year        | 2015      |
    When I click on the "Growth" view on the cpop state report
    Then the growth chart title is "Rhode Island"
    And the overall summary bar above growth chart contains the following values:
      | name         | value              |
      | STUDENTS     | 12                 |
      | AVG SCORE    | 258                |
      | DISTRIBUTION | 17%,17%,33%,8%,25% |

  @RALLY_US31907
  Scenario: Verify State report displays PARCC aggregate in Performance View for ELA
    When I open the cpop report with the following parameters:
      | param       | value     |
      | stateCode   | RI        |
      | gradeCourse | Grade 9   |
      | asmtType    | SUMMATIVE |
      | year        | 2015      |
    When I check on PARCC checkbox
    Then I should see following values in the summary row 1
      | value     |
      | PARCC AVG |
      | 2 States  |
      | 24        |
      | 17%       |
      | 17%       |
      | 33%       |
      | 8%        |
      | 25%       |
      | 33%       |
      | 258       |
      | 28 / 50   |
      | 29 / 60   |

  @RALLY_US31907
  Scenario: Verify state report displays PARCC aggregate in Performance View for Math
    When I open the cpop report with the following parameters:
      | param       | value     |
      | stateCode   | RI        |
      | asmtType    | SUMMATIVE |
      | year        | 2015      |
      | gradeCourse | Algebra+I |
      | subject     | subject1  |
    When I check on PARCC checkbox
    Then I should see following values in the summary row 1
      | value     |
      | PARCC AVG |
      | 2 States  |
      | 15        |
      | 20%       |
      | 13%       |
      | 13%       |
      | 54%       |
      | 54%       |
      | 265       |

  @RALLY_US36863
  Scenario: Verify result selector text present - State report
    When I click on the results dropdown
    Then I should see "- Available at school level" in the results list
