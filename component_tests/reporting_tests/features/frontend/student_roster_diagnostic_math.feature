Feature: Diagnostic Student Roster math

  Background: Open a firefox browser and open the webpage
    Given an open browser
    When I open the school report with the following parameters:
        | param        | value         |
        | stateCode    | RI            |
        | districtGuid | R0003         |
        | schoolGuid   | RP003         |
        | subject      | subject1      |
        | asmtType     | DIAGNOSTIC    |
        | year         | 2015          |
        | view         | locator       |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the cpop school report
    Then I wait for the grid to reload


  @RALLY_US35468, @RALLY_US35463
    Scenario: Validate student data in progression and validate sorting, filtering functionality, and legend
    Then I validate "Hope High School" as the report title
    When I select "Mathematics" from the subject dropdown
    When I hover over the "Progression" view selector
    Then I should see message “This assessment provides information about students' strengths and weaknesses in a learning progression that spans across grades.” on alert popover
    When I click on "Progression" web button
    Then I wait for the grid to reload
    And I should see the Probability of Mastery legend bar with cutpoints
    Then I validate 10 students records in table
    When I click on PROG column for sorting
    Then I validate the top record as follows:
     | value                                                                    |
     | Cabello, Darci M.                                                        |
     | 05                                                                       |
     | 343fe3a084b2422796dbd7533a68de13c3cf37                                   |
     | 04/01/2015                                                               |
     | NBT                                                                      |
     | 4.NF.A 0.83 4.NF.B 0.67 4.NBT.C 0.76 4.NBT.D 0.84 4.OA.E 0.47 4.OA.F 0.85|
    When I click on DATE column for sorting
    Then I validate the top record as follows:
     | value                                                        |
     | Augustus, Orval M.                                           |
     | 05                                                           |
     | 0a4061fcdd3f4e6ea8db682199fee2757e8275                       |
     | 11/18/2014                                                   |
     | NF                                                           |
     | 5.NBT.A 0.64 5.NF.B 0.37 5.NF.C 0.61 5.NF.D 0.92 5.NF.E 0.39 |
    When I click on TEACHER column for sorting
    Then I validate the top record as follows:
     | value                                                       |
     | Augustus, Orval M.                                          |
     | 05                                                          |
     | 0a4061fcdd3f4e6ea8db682199fee2757e8275                      |
     | 11/18/2014                                                  |
     | NF                                                          |
     | 5.NBT.A 0.64 5.NF.B 0.37 5.NF.C 0.61 5.NF.D 0.92 5.NF.E 0.39|
    When I click on GRADE column for sorting
    And I click on GRADE column for sorting
    Then I validate the top record as follows:
     | value                                                                  |
     | Pando, Joella M.                                                       |
     | 08                                                                     |
     | 5f3bcce2cac34dd38578b9d423801dc3b5caf6                                 |
     | 03/08/2015                                                             |
     | NBT                                                                    |
     | 7.OA.A 0.53 7.OA.B 0.33 7.OA.C 0.89 7.OA.D 0.71 7.NF.E 0.6 7.NF.F 0.33 |
    When I click on STUDENT column for sorting
    Then I validate the top record as follows:
     | value                                                        |
     | Augustus, Orval M.                                           |
     | 05                                                           |
     | 0a4061fcdd3f4e6ea8db682199fee2757e8275                       |
     | 11/18/2014                                                   |
     | NF                                                           |
     | 5.NBT.A 0.64 5.NF.B 0.37 5.NF.C 0.61 5.NF.D 0.92 5.NF.E 0.39 |
    When I click on the Filters button to "open" the filter menu
    And I select "Male" options from the "Gender" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    And I validate 0 students records in table
    When I click the "Clear All" button
    Then I wait for the grid to reload
    And I validate 10 students records in table

  @RALLY_US35467, @RALLY_US35463
    Scenario: Validate student data in grade level and validate sorting, filtering functionality, legend
    Then I validate "Hope High School" as the report title
    When I select "Mathematics" from the subject dropdown
    When I hover over the "Grade Level" view selector
    Then I should see message “This assessment provides information about students' strengths and weaknesses in the major content of the identied grade.” on alert popover
    When I click on "Grade Level" web button
    Then I wait for the grid to reload
    And I should see the Probability of Mastery legend bar with cutpoints
    Then I validate 10 students records in table
    When I click on GRADE LEVEL column for sorting
    Then I validate the top record as follows:
     | value                                                                    |
     | Pedone, Shane M.                                                         |
     | 04                                                                       |
     | dcdaa08b9b9046beb3e77419481c860bc6598e                                   |
     | 06/15/2015                                                               |
     | 3                                                                        |
     | 3.OA.A 0.41 3.NF.C 0.34 3.NF.C 0.66 3.NBT.D 0.33 3.NF.D 0.02 3.NF.D 0.06 |
    When I click on DATE column for sorting
    Then I validate the top record as follows:
     | value                                  |
     | Augustus, Orval M.                     |
     | 05                                     |
     | 0a4061fcdd3f4e6ea8db682199fee2757e8275 |
     | 11/18/2014                             |
     | 5                                      |
     | 5.NBT.A 0.64 5.NF.B 0.37 5.NF.C 0.61 5.NF.D 0.92 5.NF.E 0.39 |
    When I click on TEACHER column for sorting
    Then I validate the top record as follows:
     | value                                                        |
     | Augustus, Orval M.                                           |
     | 05                                                           |
     | 0a4061fcdd3f4e6ea8db682199fee2757e8275                       |
     | 11/18/2014                                                   |
     | 5                                                            |
     | 5.NBT.A 0.64 5.NF.B 0.37 5.NF.C 0.61 5.NF.D 0.92 5.NF.E 0.39 |
    When I click on GRADE column for sorting
     | value                                  |
     | Mccreery, Sandy M.                     |
     | 08                                     |
     | 0a4061fcdd3f4e6ea8db682199fee2757e8275 |
     | 06/15/2015                             |
     | 8                                      |
     | 8.NF.A 0.69 8.OA.B 0.43 8.OA.C 0.53    |
    When I click on STUDENT column for sorting
    And I click on STUDENT column for sorting
    Then I validate the top record as follows:
     | value                                                        |
     | Perla, Orval M.                                              |
     | 05                                                           |
     | 0a4061fcdd3f4e6ea8db682199fee2757e8275                       |
     | 11/18/2014                                                   |
     | 5                                                            |
     | 5.NBT.A 0.64 5.NF.B 0.37 5.NF.C 0.61 5.NF.D 0.92 5.NF.E 0.39 |
    When I click on the Filters button to "open" the filter menu
    And I select "Male" options from the "Gender" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    And I validate 0 students records in table
    When I click the "Clear All" button
    Then I wait for the grid to reload
    And I validate 10 students records in table

  @RALLY_US35258, @RALLY_US35463
    Scenario: Validate student data in Locator and validate sorting, filtering functionality, legend
    Then I validate "Hope High School" as the report title
    When I select "Mathematics" from the subject dropdown
    When I hover over the "Locator" view selector
    Then I should see message “This assessment can be used to suggest the most appropriate grade level subtest that the student should take next to provide more detailedinformation about the students' strengths and weaknesses with respect to this content.” on alert popover
    When I click on "Locator" web button
    Then I wait for the grid to reload
    And I should not see the Probability of Mastery legend bar with cutpoints
    And I validate column label should be following:
     | col_label                     |
     | STUDENT                       |
     | GRADE                         |
     | TEACHER                       |
     | DATE                          |
     | SCORE                         |
     | SUGGESTED GRADE LEVEL SUBTEST |
    Then I validate 10 students records in table
    When I click on GRADE column for sorting
    And I click on GRADE column for sorting
    Then I validate the top record as follows:
     | value                                  |
     | Pando, Joella M.                       |
     | 08                                     |
     | 5f3bcce2cac34dd38578b9d423801dc3b5caf6 |
     | 03/08/2015                             |
     | 198                                    |
     | 7                                      |
    When I click on STUDENT column for sorting
    And I click on STUDENT column for sorting
    Then I validate the top record as follows:
     | value                                  |
     | Perla, Orval M.                        |
     | 05                                     |
     | 0a4061fcdd3f4e6ea8db682199fee2757e8275 |
     | 11/18/2014                             |
     | 162                                    |
     | 6                                      |
    When I click on DATE column for sorting
    And I click on DATE column for sorting
    Then I validate the top record as follows:
     | value                                  |
     | Pedone, Shane M.                       |
     | 04                                     |
     | dcdaa08b9b9046beb3e77419481c860bc6598e |
     | 06/15/2015                             |
     | 191                                    |
     | 3                                      |
    When I click on TEACHER column for sorting
    And I click on TEACHER column for sorting
    Then I validate the top record as follows:
     | value                                  |
     | Pedone, Shane M.                       |
     | 04                                     |
     | dcdaa08b9b9046beb3e77419481c860bc6598e |
     | 06/15/2015                             |
     | 191                                    |
     | 3                                      |
    When I hover on the info icon in the "SCORE" column
    Then I should see message “The scale score is used to suggest the most appropriate grade level subtest that the student should take next to provide more detailed information about the student'/s strengths and weaknesses with respect to this content.” on alert popover
    When I click on SCORE column for sorting
    Then I validate the top record as follows:
     | value                                  |
     | Cabello, Darci M.                      |
     | 05                                     |
     | 343fe3a084b2422796dbd7533a68de13c3cf37 |
     | 04/01/2015                             |
     | 102                                    |
     | 4                                      |
#    When I hover on the info icon in the "SUGGESTED GRADE LEVEL SUBTEST" column
#    Then I should see message “Based on the scale score, this is the most appropriate grade level subtest that the student should take next to provide more detailed information about the student's strengths and weaknesses with respect to this content." on alert popover
    When I click on SUGGESTED GRADE LEVEL SUBTEST column for sorting
    And I click on SUGGESTED GRADE LEVEL SUBTEST column for sorting
     | value                                  |
     | Mccreery, Sandy M.                     |
     | 08                                     |
     | 0a4061fcdd3f4e6ea8db682199fee2757e8275 |
     | 06/15/2015                             |
     | 144                                    |
     | 8                                      |
    When I click on the Filters button to "open" the filter menu
    And I select "Male" options from the "Gender" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    And I validate 0 students records in table
    When I click the "Clear All" button
    Then I wait for the grid to reload
    And I validate 10 students records in table

  @RALLY_US35469, @RALLY_US35463
    Scenario: Validate student data in cluster view and validate data, sorting, filtering functionality, and legend
    Then I validate "Hope High School" as the report title
    When I select "Mathematics" from the subject dropdown
    When I hover over the "Cluster" view selector
    Then I should see message “This assessment provides information about students' strengths and weaknesses at the cluster level.” on alert popover
    When I click on "Cluster" web button
    Then I wait for the grid to reload
    And I should see the Probability of Mastery legend bar with cutpoints
    Then I validate 10 students records in table
    When I click on GRADE column for sorting
    And I click on GRADE column for sorting
    Then I validate the top record as follows:
     | value                                  |
     | Pando, Joella M.                       |
     | 08                                     |
     | 5f3bcce2cac34dd38578b9d423801dc3b5caf6 |
     | 03/08/2015                             |
     | 7.OA.A 0.53                            |
    When I click on STUDENT column for sorting
    And I click on STUDENT column for sorting
    Then I validate the top record as follows:
     | value                                                        |
     | Perla, Orval M.                                              |
     | 05                                                           |
     | 0a4061fcdd3f4e6ea8db682199fee2757e8275                       |
     | 11/18/2014                                                   |
     | 5.NBT.A 0.64                                                 |
    When I click on DATE column for sorting
    And I click on DATE column for sorting
    Then I validate the top record as follows:
     | value                                  |
     | Pedone, Shane M.                       |
     | 04                                     |
     | dcdaa08b9b9046beb3e77419481c860bc6598e |
     | 06/15/2015                             |
     | 3.OA.A 0.41                            |
    When I click on TEACHER column for sorting
    And I click on TEACHER column for sorting
    Then I validate the top record as follows:
     | value                                                                    |
     | Pedone, Shane M.                                                         |
     | 04                                                                       |
     | dcdaa08b9b9046beb3e77419481c860bc6598e                                   |
     | 06/15/2015                                                               |
     | 3.OA.A 0.41                                                              |
    When I click on the Filters button to "open" the filter menu
    And I select "Male" options from the "Gender" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    And I validate 0 students records in table
    When I click the "Clear All" button
    Then I wait for the grid to reload
    And I validate 10 students records in table

  @RALLY_US35470, @RALLY_US35463
    Scenario: Validate student data in Fluency view and validate data, sorting and filtering functionality
    Then I validate "Hope High School" as the report title
    When I select "Mathematics" from the subject dropdown
    When I hover over the "Fluency" view selector
    Then I should see message “This assessment provides information about students' strengths and weaknesses with regard to uency standards, where both speed and accuracy are provided.” on alert popover
    When I click on "Fluency" web button
    Then I wait for the grid to reload
    Then I should see the subtitle "34 assessments"
    When I click on ITEMS CORRECT column for sorting
    And I click on ITEMS CORRECT column for sorting
    Then I validate the top record as follows:
     | value                                           |
     | Pedone, Shane M.                                |
     | 04                                              |
     | 0a4d255f40644147aaffbcb10fef0d908e8bd9          |
     | 02/02/2015                                      |
     | Divide with Multi-digit Decimal Numbers (Gr. 6) |
     | 3/3 (100%)                                      |
     | 08:04                                           |
    When I click on TOTAL TIME PER SKILL column for sorting
    And I click on TOTAL TIME PER SKILL column for sorting
    Then I validate the top record as follows:
     | value                                         |
     | Pedone, Shane M.                              |
     | 04                                            |
     | 0a4d255f40644147aaffbcb10fef0d908e8bd9        |
     | 02/02/2015                                    |
     | Division of Multi-digit Whole Numbers (Gr. 6) |
     | 2/4 (50%)                                     |
     | 11:57                                         |
    When I click on the Filters button to "open" the filter menu
    And I select "Male" options from the "Gender" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    And I validate 0 students records in table
    When I click the "Clear All" button
    Then I wait for the grid to reload
    And I validate 34 students records in table

  @RALLY_US35257
  Scenario: Verify diagnostic Math results for individual students
    Then I validate that I am on "Hope High School" school page
    When I click on "Mccreery, Sandy M."
    And I navigate to the new opened window
    And I am redirected to the individual student report
    Then I verified student name "Sandy Mccreery" on page header
    And I should see the Probability of Mastery legend bar with cutpoints
    And I verified the following section
      | section     |
      | LOCATOR     |
      | GRADE LEVEL |
      | PROGRESSION |
      | CLUSTER     |
      | FLUENCY     |
    And I should see "2014 - 2015" on Academic Year dropdown
    And I verified "Mathematics" value in "SUBJECT" selected
    And I verified "Diagnostic Assessments " value in "RESULTS" selected
    And I verified "LOCATOR" section contains following columns
      | column_name    |
      | TEACHER        |
      | DATE           |
      | SCORE          |
      | SUGGESTED GRADE LEVEL SUBTEST |
    And I verified "GRADE LEVEL " section contains following columns
      | column_name       |
      | TEACHER           |
      | DATE              |
      | GRADE LEVEL       |
      | CLUSTERS          |
    And in LOCATOR section, I should see the following values in the top row:
      | value                                  |
      | 0a4061fcdd3f4e6ea8db682199fee2757e8275 |
      | 06/15/2015                             |
      | 144                                    |
      | 8                                      |
    And in GRADE LEVEL section, I should see the following values in the top row:
      | value                                  |
      | 0a4061fcdd3f4e6ea8db682199fee2757e8275 |
      | 06/15/2015                             |
      | 8                                      |
      | 8.NF.A                                 |
      | 0.69                                   |
      | 8.OA.B                                 |
      | 0.43                                   |
      | 8.OA.C                                 |
      | 0.53                                   |
    And in PROGRESSION section, I should see the following values in the top row:
      | value                                  |
      | 0a4061fcdd3f4e6ea8db682199fee2757e8275 |
      | 06/15/2015                             |
      | NBT                                    |
      | 8.NF.A                                 |
      | 0.69                                   |
      | 8.OA.B                                 |
      | 0.43                                   |
      | 8.OA.C                                 |
      | 0.53                                   |
    And in CLUSTER section, I should see the following values in the top row:
      | value                                  |
      | 0a4061fcdd3f4e6ea8db682199fee2757e8275 |
      | 06/15/2015                             |
      | 8.NF.A                                 |
      | 0.69                                   |
    And in FLUENCY section, I should see the following values in the top row:
      | value                                  |
      | 0a4061fcdd3f4e6ea8db682199fee2757e8275 |
      | 06/15/2015                             |
      | Multiply & Divide within 100 (Gr. 3)   |
      | 0/4 (0%)                               |
      | 03:08                                  |
