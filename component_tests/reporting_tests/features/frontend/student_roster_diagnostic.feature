Feature: Diagnostic Student Roster

  Background: Open a firefox browser and open the webpage
    Given an open browser
    When I open the school report with the following parameters:
        | param        | value         |
        | stateCode    | RI            |
        | districtGuid | R0003         |
        | schoolGuid   | RP001         |
        | subject      | subject2      |
        | asmtType     | DIAGNOSTIC    |
        | year         | 2015          |
        | view         | vocab         |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the cpop school report
    Then I wait for the grid to reload

  Scenario: Verify breadcrumb - district link
    Then I should see the breadcrumbs "Home,Rhode Island,Providence,Alvarez High School"
    When I click on "Providence" link on breadcrumb
    And I verify that I am on the subject and grade/course initial selection page
    And I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 11th Grade   |
        | subject      | ELA/L        |
    Then I wait for the grid to reload
    And I should see the subtitle "3 schools"

  Scenario: Verify breadcrumb - state link
    Then I should see the breadcrumbs "Home,Rhode Island,Providence,Alvarez High School"
    When I click on "Rhode Island" link on breadcrumb
    And I verify that I am on the subject and grade/course initial selection page
    And I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 11th Grade   |
        | subject      | ELA/L        |
    Then I wait for the grid to reload
    And I should see the subtitle "3 districts"

  Scenario: Verify breadcrumb - Home link
    Then I should see the breadcrumbs "Home,Rhode Island,Providence,Alvarez High School"
    When I click on "Home" link on breadcrumb
    And I verify that I am on the subject and grade/course initial selection page
    And I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 11th Grade   |
        | subject      | ELA/L        |
    Then I wait for the grid to reload
    And I should see the subtitle "3 states"

  @RALLY_US34002
  Scenario: Verify diagnostic ELA results for individual students
    When I open the school report with the following parameters:
        | param        | value         |
        | stateCode    | RI            |
        | districtGuid | R0001         |
        | schoolGuid   | RN001         |
        | subject      | subject2      |
        | asmtType     | DIAGNOSTIC    |
        | year         | 2015          |
        | view         | vocab         |
    Then I wait for the grid to reload
    Then I validate that I am on "Rogers High School" school page
    When I click on "Cartlidge, Shane B."
    And I navigate to the new opened window
    And I am redirected to the individual student report
    Then I verified student name "Shane Cartlidge" on page header
    And I verified the following section
     | section               |
     | READING COMPREHENSION |
     | VOCABULARY            |
     | READING FLUENCY       |
     | WRITING               |
     | DECODING              |
    And I verified "2014 - 2015" value in "ACADEMIC YEAR" selected
    And I verified "Vocabulary" section contains following columns
     | column_name    |
     | TEACHER        |
     | DATE           |
     | SCORE          |
    And I verified "Writing" section contains following columns
     | column_name       |
     | TEACHER           |
     | DATE              |
     | RESEARCH          |
     | LITERARY ANALYSIS |
     | NARRATIVE WRITING |
    When I click on SCORE column in VOCABULARY for sorting
    Then in VOCABULARY section, I should see the following values in the top row:
    | value                                  |
    | dcdaa08b9b9046beb3e77419481c860bc6598e |
    | 06/15/2015                             |
    | 337                                    |
    When I click on TEACHER column in WRITING for sorting
    Then in WRITING section, I should see the following values in the top row:
    |                                  value |
    | 0a4d255f40644147aaffbcb10fef0d908e8bd9 |
    |                             02/02/2015 |
    |                                      2 |
    |                                      3 |
    |                                      5 |
    Then I close the new opened window

  @RALLY_US34001
  Scenario: Validate student data in Overview and validate sorting, filtering functionality
    Then I validate "Alvarez High School" as the report title
    When I click on "Overview" web button
    Then I wait for the grid to reload
    Then I validate 20 students records in table
    When I click on GRADE column for sorting
    Then I validate the top record as follows:
     | value                                   |
     | Mccreery, Zofia K.                      |
     | 03                                      |
     | 5f3bcce2cac34dd38578b9d423801dc3b5caf6  |
     | 04/01/2015                              |
     | Decoding                                |
    When I click on STUDENT column for sorting
    Then I validate the top record as follows:
     | value                                   |
     | Forst, Jacquline Z.                     |
     | 04                                      |
     | 3c6b9327b0484f5f84741b204a6c9f02208bc1  |
     | 03/08/2015                              |
     | Decoding                                |
    When I click on DATE column for sorting
    Then I validate the top record as follows:
     | value                                   |
     | Gatti, Shane K.                         |
     | 06                                      |
     | 3c6b9327b0484f5f84741b204a6c9f02208bc1  |
     | 11/18/2014                              |
     | Decoding                                |
    When I click on GRADE column for sorting
    And I click on TEACHER column for sorting
    Then I validate the top record as follows:
     | value                                   |
     | Forst, Jacquline Z.                     |
     | 04                                      |
     | 3c6b9327b0484f5f84741b204a6c9f02208bc1  |
     | 03/08/2015                              |
     | Decoding                                |
    When I click on the Filters button to "open" the filter menu
    And I select "Female" options from the "Gender" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    And I validate 10 students records in table
    When I click the "Clear All" button
    Then I wait for the grid to reload
    And I validate 20 students records in table

  @RALLY_US35458
  Scenario: Validate the default view
    Then I validate "Alvarez High School" as the report title
    Then I validate total number of assessments should be 4
    And I validate total number of buttons should be 7
    And I should see 5 columns in the table
    And I validate column label should be following:
     | col_label |
     | STUDENT   |
     | GRADE     |
     | TEACHER   |
     | DATE      |
     | SCORE     |
    And I validate label for each button should be following
     | btn_label                   |
     | Overview                    |
     | Reading Comprehension       |
     | Vocabulary                  |
     | Reading Fluency             |
     | Writing                     |
     | Decoding                    |
     | Reader Motivation Survey    |

  @RALLY_US35458
  Scenario: Validate student data for vocabulary, sorting, filtering functionality
    Then I validate "Alvarez High School" as the report title
    When I click on "Vocabulary" web button
    Then I wait for the grid to reload
    Then I validate 4 students records in table
    When I click on SCORE column for sorting
    Then I validate the top record as follows:
     | value                                   |
     | Mccreery, Sandy D.                      |
     | 06                                      |
     | 3c6b9327b0484f5f84741b204a6c9f02208bc1  |
     | 11/18/2014                              |
     | 319                                     |
    When I click on SCORE column for sorting
    Then I validate the top record as follows:
     | value                                   |
     | Forst, Jacquline Z.                     |
     | 04                                      |
     | 3c6b9327b0484f5f84741b204a6c9f02208bc1  |
     | 03/08/2015                              |
     | 509                                     |
    When I click on STUDENT column for sorting
    Then I validate the top record as follows:
     | value                                   |
     | Forst, Jacquline Z.                     |
     | 04                                      |
     | 3c6b9327b0484f5f84741b204a6c9f02208bc1  |
     | 03/08/2015                              |
     | 509                                     |
    When I click on STUDENT column for sorting
    Then I validate the top record as follows:
     | value                                   |
     | Mccreery, Zofia K.                      |
     | 03                                      |
     | 5f3bcce2cac34dd38578b9d423801dc3b5caf6  |
     | 04/01/2015                              |
     | 443                                     |
    When I click on the Filters button to "open" the filter menu
    And I select "Female" options from the "Gender" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    And I validate 2 students records in table
    And I validate the top record as follows:
     | value                                   |
     | Forst, Jacquline Z.                     |
     | 04                                      |
     | 3c6b9327b0484f5f84741b204a6c9f02208bc1  |
     | 03/08/2015                              |
     | 509                                     |
    When I click on STUDENT column for sorting
    Then I validate the top record as follows:
     | value                                   |
     | Mccreery, Zofia K.                      |
     | 03                                      |
     | 5f3bcce2cac34dd38578b9d423801dc3b5caf6  |
     | 04/01/2015                              |
     | 443                                     |
    When I click the "Clear All" button
    Then I wait for the grid to reload
    And I validate 4 students records in table

  @RALLY_US35458, @RALLY_US35463
  Scenario: Validate student data for writing, sorting, filtering functionality
    Then I validate "Alvarez High School" as the report title
    When I click on "Writing" web button
    Then I wait for the grid to reload
    Then I validate 4 students records in table
    And I validate column label should be following:
     | col_label         |
     | STUDENT           |
     | GRADE             |
     | TEACHER           |
     | DATE              |
     | RESEARCH          |
     | LITERARY ANALYSIS |
     | NARRATIVE WRITING |
    When I hover on the info icon in the writing "RESEARCH" column
    Then I should see message "Students write analyzing informational text sources" on alert popover
    When I hover on the info icon in the writing "LITERARY ANALYSIS" column
    Then I should see message "Students write analyzing literary text sources" on alert popover
    When I hover on the info icon in the writing "NARRATIVE WRITING" column
    Then I should see message "Students write using a text as a stimulus to produce a narrative" on alert popover
    When I click on STUDENT column for sorting
    Then I validate the top record as follows:
     | value                                   |
     | Mccreery, Zofia K.                      |
     | 03                                      |
     | 5f3bcce2cac34dd38578b9d423801dc3b5caf6  |
     | 04/01/2015                              |
     | 3                                       |
     | 3                                       |
     | 1                                       |
    When I click on RESEARCH column for sorting
    Then I validate the top record as follows:
     | value                                   |
     | Mccreery, Sandy D.                      |
     | 06                                      |
     | 3c6b9327b0484f5f84741b204a6c9f02208bc1  |
     | 11/18/2014                              |
     | 1                                       |
     | 1                                       |
     | 3                                       |
    When I click on RESEARCH column for sorting
    Then I validate the top record as follows:
     | value                                   |
     | Gatti, Shane K.                         |
     | 06                                      |
     | 3c6b9327b0484f5f84741b204a6c9f02208bc1  |
     | 11/18/2014                              |
     | 4                                       |
     | 4                                       |
     | 5                                       |
    When I click on NARRATIVE WRITING column for sorting
    Then I validate the top record as follows:
     | value                                   |
     | Mccreery, Zofia K.                      |
     | 03                                      |
     | 5f3bcce2cac34dd38578b9d423801dc3b5caf6  |
     | 04/01/2015                              |
     | 3                                       |
     | 3                                       |
     | 1                                       |

  @RALLY_US35459, @RALLY_US35463
  Scenario: Validate student data for Reading Fluency, Sorting and Filters
    When I click on "Reading Fluency" web button
    Then I should see the subtitle "4 assessments"
    And I wait for the grid to reload
    And I should see "STUDENT,GRADE,TEACHER,DATE,WPM,WCPM,ACCURACY,EXPRESSIVENESS,PASSAGE(S)" columns for Reading Fluency
    When I hover on the info icon in the "WPM" column
    Then I should see message "Words per minute" on alert popover
    When I hover on the info icon in the "WCPM" column
    Then I should see message "Words correct per minute" on alert popover
    When I hover on the info icon in the "ACCURACY" column
    Then I should see message "Words correct per minute divided by words per minute x 100 is the formula used to compute the accuracy rate" on alert popover
    And I validate the top record as follows:
      |value                                  |
      |Forst, Jacquline Z.                    |
      |04                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |03/08/2015                             |
      |37                                     |
      |21                                     |
      |57%                                    |
      |1                                      |
      |Lit                                    |
    When I click on TEACHER column for sorting
    When I click on TEACHER column for sorting
    Then I validate the top record as follows:
      |value                                  |
      |Mccreery, Zofia K.                     |
      |03                                     |
      |5f3bcce2cac34dd38578b9d423801dc3b5caf6 |
      |04/01/2015                             |
      |40                                     |
      |10                                     |
      |25%                                    |
      |0                                      |
      |Lit                                    |
    When I click on DATE column for sorting
    Then I validate the top record as follows:
      |value                                  |
      |Gatti, Shane K.                        |
      |06                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |11/18/2014                             |
      |124                                    |
      |1                                      |
      |1%                                     |
      |3                                      |
      |Info • Lit                             |
    When I click on WPM column for sorting
    Then I validate the top record as follows:
      |value                                  |
      |Forst, Jacquline Z.                    |
      |04                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |03/08/2015                             |
      |37                                     |
      |21                                     |
      |57%                                    |
      |1                                      |
      |Lit                                    |
    When I click on WCPM column for sorting
    Then I validate the top record as follows:
      |value                                  |
      |Gatti, Shane K.                        |
      |06                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |11/18/2014                             |
      |124                                    |
      |1                                      |
      |1%                                     |
      |3                                      |
      |Info • Lit                             |
    When I click on ACCURACY column for sorting
    And I click on ACCURACY column for sorting
    Then I validate the top record as follows:
      |value                                  |
      |Mccreery, Sandy D.                     |
      |06                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |11/18/2014                             |
      |121                                    |
      |71                                     |
      |59%                                    |
      |3                                      |
      |Lit • Lit                              |
    When I click on EXPRESSIVENESS column for sorting
    Then I validate the top record as follows:
      |value                                  |
      |Mccreery, Zofia K.                     |
      |03                                     |
      |5f3bcce2cac34dd38578b9d423801dc3b5caf6 |
      |04/01/2015                             |
      |40                                     |
      |10                                     |
      |25%                                    |
      |0                                      |
      |Lit                                    |
    When I click on the Filters button to "open" the filter menu
    And I select "Male" options from the "Gender" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    Then I should see the subtitle "2 assessments"
    Then I validate the top record as follows:
      |value                                  |
      |Gatti, Shane K.                        |
      |06                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |11/18/2014                             |
      |124                                    |
      |1                                      |
      |1%                                     |
      |3                                      |
      |Info • Lit                             |
    When I click on the Filters button to "open" the filter menu
    And I select "White" options from the "Race / Ethnicity" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    Then I should see the subtitle "2 assessments"
    Then I validate the top record as follows:
      |value                                  |
      |Gatti, Shane K.                        |
      |06                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |11/18/2014                             |
      |124                                    |
      |1                                      |
      |1%                                     |
      |3                                      |
      |Info • Lit                             |

  @RALLY_US35457
  Scenario: Validate student data for Reading Comprehension, Sorting and Filters
    When I click on "Reading Comprehension" web button
    Then I should see the subtitle "4 assessments"
    And I wait for the grid to reload
    And I should see "STUDENT,GRADE,TEACHER,DATE,SCORE,IRL,PASSAGES TYPE (RMM)" columns for Reading Comprehension
    And I validate the top record as follows:
      |value                                  |
      |Forst, Jacquline Z.                    |
      |04                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |03/08/2015                             |
      |509                                    |
      |2.6                                    |
      |Lit (8)                                |
    When I click on TEACHER column for sorting
    When I click on TEACHER column for sorting
    Then I validate the top record as follows:
      |value                                  |
      |Mccreery, Zofia K.                     |
      |03                                     |
      |5f3bcce2cac34dd38578b9d423801dc3b5caf6 |
      |04/01/2015                             |
      |443                                    |
      |7.4                                    |
      |Lit (3.8)                              |
    When I click on DATE column for sorting
    Then I validate the top record as follows:
      |value                                  |
      |Gatti, Shane K.                        |
      |06                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |11/18/2014                             |
      |454                                    |
      |2.8                                    |
      |Info (3.9) • Lit (3.5)                 |
    When I click on SCORE column for sorting
    Then I validate the top record as follows:
      |value                                  |
      |Mccreery, Sandy D.                     |
      |06                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |11/18/2014                             |
      |319                                    |
      |9.3                                    |
      |Lit (1.5) • Lit (2.5)                  |
    When I click on IRL column for sorting
    Then I validate the top record as follows:
      |value                                  |
      |Forst, Jacquline Z.                    |
      |04                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |03/08/2015                             |
      |509                                    |
      |2.6                                    |
      |Lit (8)                                |
    When I click on the Filters button to "open" the filter menu
    And I select "Male" options from the "Gender" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    Then I should see the subtitle "2 assessments"
    Then I validate the top record as follows:
      |value                                  |
      |Gatti, Shane K.                        |
      |06                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |11/18/2014                             |
      |454                                    |
      |2.8                                    |
      |Info (3.9) • Lit (3.5)                 |
    When I click on the Filters button to "open" the filter menu
    And I select "White" options from the "Race / Ethnicity" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    Then I should see the subtitle "2 assessments"
    Then I validate the top record as follows:
      |value                                  |
      |Gatti, Shane K.                        |
      |06                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |11/18/2014                             |
      |454                                    |
      |2.8                                    |
      |Info (3.9) • Lit (3.5)                 |

  @RALLY_US35461
  Scenario: Validate student data for Decoding, Sorting and Filters
    When I click on "Decoding" web button
    Then I should see the subtitle "4 assessments"
    And I wait for the grid to reload
    And I validate column label should be following:
     | col_label          |
     | STUDENT            |
     | GRADE              |
     | TEACHER            |
     | DATE               |
     | CVC                |
     | BLENDS & DIAGRAPHS |
     | COMPLEX VOWELS     |
     | COMPLEX CONSONANTS |
     | MULTI-SYLLABIC 1   |
     | MULTI-SYLLABIC 2   |
    And I validate the top record as follows:
      |value                                  |
      |Forst, Jacquline Z.                    |
      |04                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |03/08/2015                             |
      |0.87                                   |
      |0.45                                   |
      |0.22                                   |
      |0.05                                   |
      |0.54                                   |
      |0.82                                   |
    When I click on GRADE column for sorting
      |value                                  |
      |Mccreery, Zofia K.                     |
      |03                                     |
      |5f3bcce2cac34dd38578b9d423801dc3b5caf6 |
      |04/01/2015                             |
      |0.48                                   |
      |0.63                                   |
      |0.98                                   |
      |0.76                                   |
      |0.25                                   |
      |0.04                                   |
    When I click on TEACHER column for sorting
    Then I validate the top record as follows:
      |value                                  |
      |Forst, Jacquline Z.                    |
      |04                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |03/08/2015                             |
      |0.87                                   |
      |0.45                                   |
      |0.22                                   |
      |0.05                                   |
      |0.54                                   |
      |0.82                                   |
    When I click on DATE column for sorting
    Then I validate the top record as follows:
      |value                                  |
      |Gatti, Shane K.                        |
      |06                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |11/18/2014                             |
      |0.51                                   |
      |0.92                                   |
      |0.77                                   |
      |0.46                                   |
      |0.82                                   |
      |0.63                                   |
    When I click on CVC column for sorting
    Then I validate the top record as follows:
      |value                                  |
      |Mccreery, Sandy D.                     |
      |06                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |11/18/2014                             |
      |0.27                                   |
      |0.52                                   |
      |0.92                                   |
      |0.81                                   |
      |0.65                                   |
      |0.49                                   |
    When I click on BLENDS & DIAGRAPHS column for sorting
    Then I validate the top record as follows:
      |value                                  |
      |Forst, Jacquline Z.                    |
      |04                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |03/08/2015                             |
      |0.87                                   |
      |0.45                                   |
      |0.22                                   |
      |0.05                                   |
      |0.54                                   |
      |0.82                                   |
    When I click on COMPLEX VOWELS column for sorting
    When I click on COMPLEX VOWELS column for sorting
    Then I validate the top record as follows:
      |value                                  |
      |Mccreery, Zofia K.                     |
      |03                                     |
      |5f3bcce2cac34dd38578b9d423801dc3b5caf6 |
      |04/01/2015                             |
      |0.48                                   |
      |0.63                                   |
      |0.98                                   |
      |0.76                                   |
      |0.25                                   |
      |0.04                                   |
    When I click on COMPLEX CONSONANTS column for sorting
    Then I validate the top record as follows:
      |value                                  |
      |Forst, Jacquline Z.                    |
      |04                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |03/08/2015                             |
      |0.87                                   |
      |0.45                                   |
      |0.22                                   |
      |0.05                                   |
      |0.54                                   |
      |0.82                                   |
    When I click on MULTI-SYLLABIC 1 column for sorting
    Then I validate the top record as follows:
      |value                                  |
      |Mccreery, Zofia K.                     |
      |03                                     |
      |5f3bcce2cac34dd38578b9d423801dc3b5caf6 |
      |04/01/2015                             |
      |0.48                                   |
      |0.63                                   |
      |0.98                                   |
      |0.76                                   |
      |0.25                                   |
      |0.04                                   |
    When I click on MULTI-SYLLABIC 2 column for sorting
    When I click on MULTI-SYLLABIC 2 column for sorting
    Then I validate the top record as follows:
      |value                                  |
      |Forst, Jacquline Z.                    |
      |04                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |03/08/2015                             |
      |0.87                                   |
      |0.45                                   |
      |0.22                                   |
      |0.05                                   |
      |0.54                                   |
      |0.82                                   |
    When I click on the Filters button to "open" the filter menu
    And I select "Male" options from the "Gender" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    Then I should see the subtitle "2 assessments"
    Then I validate the top record as follows:
      |value                                  |
      |Gatti, Shane K.                        |
      |06                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |11/18/2014                             |
      |0.51                                   |
      |0.92                                   |
      |0.77                                   |
      |0.46                                   |
      |0.82                                   |
      |0.63                                   |
    When I click on the Filters button to "open" the filter menu
    And I select "White" options from the "Race / Ethnicity" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    Then I should see the subtitle "2 assessments"
    Then I validate the top record as follows:
      |value                                  |
      |Gatti, Shane K.                        |
      |06                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |11/18/2014                             |
      |0.51                                   |
      |0.92                                   |
      |0.77                                   |
      |0.46                                   |
      |0.82                                   |
      |0.63                                   |

  @RALLY_US35461
  Scenario: Validate the legend bar has Probability of Mastery levels in Decoding view
    When I click on "Decoding" web button
    Then I wait for the grid to reload
    And I should see the Probability of Mastery legend bar with cutpoints

  @RALLY_US37933
  Scenario: Validate the legend bar has both shorter and longer version of Expressiveness rubric in Reading Fluency view
    When I click on "Reading Fluency" web button
    Then I should see the subtitle "4 assessments"
    And I wait for the grid to reload
    Then I should see the expressiveness rubric legend bar shorter version

    When I click on details in the expressiveness legend to expand the legend
    Then I should see the expressiveness rubric legend bar longer version
    When I click on details in the expressiveness legend to collapse the legend
    Then I should see the expressiveness rubric legend bar shorter version

  @RALLY_US35462
  Scenario: Validate student data for Reader Motivation Survey, Sorting and Filters
    When I click on "Reader Motivation Survey" web button
    Then I wait for the grid to reload
    Then I should see the subtitle "4 assessments"
    And I should see "STUDENT,GRADE,TEACHER,DATE,1,2,3,4,5" columns for Reading Fluency
    When I click on STUDENT column for sorting
    When I click on STUDENT column for sorting
    Then I validate the top record as follows:
      |value                                  |
      |Forst, Jacquline Z.                    |
      |04                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |03/08/2015                             |
      |C                                      |
      |D                                      |
      |D                                      |
      |B                                      |
      |C                                      |
    When I click on "NEXT" pagination
    Then I validate the top record as follows:
      | value               |
      | Forst, Jacquline Z. |
      | C                   |
      | B • C • D           |
      | A • C               |
      | B                   |
      | C                   |
      | C                   |
      | D                   |
      | A                   |
    When I click on "NEXT" pagination
    Then I validate the top record as follows:
      | value               |
      | Forst, Jacquline Z. |
      | A • B               |
      | A                   |
      | A                   |
      | C                   |
      | C                   |
      | A                   |
    When I click on the Filters button to "open" the filter menu
    And I select "White" options from the "Race / Ethnicity" filter
    And I click on the "Apply Filters" button to "apply" any changes and close the filter menu
    Then I wait for the grid to reload
    Then I should see the subtitle "2 assessments"
    Then I validate the top record as follows:
      |value                                  |
      |Gatti, Shane K.                        |
      |06                                     |
      |3c6b9327b0484f5f84741b204a6c9f02208bc1 |
      |11/18/2014                             |
      |D                                      |
      |A                                      |
      |B                                      |
      |D                                      |
      |A                                      |

