Feature: Context security and permissions
  Background: Open a firefox browser and open the webpage
    Given an open browser

  @RALLY_US31992
  Scenario: Verify user can see an export link in the header and has export options that he/she has permission to execute in State Report
    When I open the cpop report with the following parameters: 
      | param        | value     |
      | gradeCourse  | Grade 3   |
      | asmtType     | SUMMATIVE |
      | year         | 2015      |
      | stateCode    | NY        |
      | subject      | subject2  |
    Then I am redirected to the login page
    When I login as lwoman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | ELA/L        |
    And I am redirected to the cpop state report
    Then I should see "Export Data" link on header
    When I click on "Export Data" link on header
    Then I should see modal title "Data Export"
    When I click on Export Type drop down
    Then I should see following items
     |  value                           |
     | Summative Assessment Results     |
     | Summative Released Item File     |
     | Summative Psychometric Item File |
     |State Data for PARCC-level Results|

  @RALLY_US31992
  Scenario: Verify user can see an export link in the header and has export options that he/she has permission to execute in District Report
    When I open the cpop report with the following parameters: 
      | param        | value     |
      | gradeCourse  | Grade 3   |
      | asmtType     | SUMMATIVE |
      | year         | 2015      |
      | stateCode    | NY        |
      | districtGuid | R0003     |
      | subject      | subject2  |
    Then I am redirected to the login page
    When I login as lwoman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | ELA/L        |
    And I am redirected to the cpop state report
    Then I should see "Export Data" link on header
    When I click on "Export Data" link on header
    Then I should see modal title "Data Export"
    When I click on Export Type drop down
    Then I should see following items
     |  value                           |
     | Summative Assessment Results     |
     | Summative Released Item File     |
     | Summative Psychometric Item File |
     |State Data for PARCC-level Results|

  @RALLY_US31992
  Scenario: Verify user can see an export link in the header and has export options that he/she has permission to execute in School Report
    When I open the school report with the following parameters: 
      | param        | value    |
      | subject      | subject2 |
      | stateCode    | NY       |
      | year         | 2015     |
      | districtGuid | R0003    |
      | schoolGuid   | RP001    |
    Then I am redirected to the login page
    When I login as lwoman
    And I am redirected to the cpop school report
    Then I should see "Export Data" link on header
    When I click on "Export Data" link on header
    Then I should see modal title "Data Export"
    When I click on Export Type drop down
    Then I should see following items
     |  value                           |
     | Summative Assessment Results     |
     | Summative Released Item File     |
     | Summative Psychometric Item File |
     |State Data for PARCC-level Results|

  @RALLY_US31992
  Scenario: Verify user can see an export link in the header and does not have export options that he/she has no permission to execute in State Report
    When I open the cpop report with the following parameters: 
      | param        | value     |
      | gradeCourse  | Grade 3   |
      | asmtType     | SUMMATIVE |
      | year         | 2015      |
      | stateCode    | NY        |
      | subject      | subject1  |
    Then I am redirected to the login page
    When I login as rhood
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | Mathematics  |
    And I am redirected to the cpop state report
    Then I should see "Export Data" link on header
    When I click on "Export Data" link on header
    Then I should see modal title "Data Export"
    When I click on Export Type drop down
    Then I should see following items
     |  value                           |
     | Summative Psychometric Item File |
    Then I should not see following items
     |  value                           |
     | Summative Assessment Results     |
     | Summative Released Item File     |

  @RALLY_US31992
  Scenario: Verify user can see an export link in the header and does not have export options that he/she has no permission to execute in District Report
    When I open the cpop report with the following parameters: 
      | param        | value     |
      | gradeCourse  | Grade 3   |
      | asmtType     | SUMMATIVE |
      | year         | 2015      |
      | stateCode    | NY        |
      | districtGuid | R0003     |
      | subject      | subject1  |
    Then I am redirected to the login page
    When I login as rhood
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | Mathematics  |
    And I am redirected to the cpop district report
    Then I should see "Export Data" link on header
    When I click on "Export Data" link on header
    Then I should see modal title "Data Export"
    When I click on Export Type drop down
    Then I should see following items
     |  value                           |
     | Summative Psychometric Item File |
    And I should not see following items
     |  value                           |
     | Summative Assessment Results     |
     | Summative Released Item File     |

  @RALLY_US31992
  Scenario: Verify user can see an export link in the header and does not have export options that he/she has no permission to execute in in School Report
    When I open the school report with the following parameters: 
      | param        | value    |
      | subject      | subject1 |
      | stateCode    | NY       |
      | year         | 2015     |
      | districtGuid | R0003    |
      | schoolGuid   | RP001    |
    Then I am redirected to the login page
    When I login as rhood
    And I am redirected to the cpop school report
    Then I should see "Export Data" link on header
    When I click on "Export Data" link on header
    Then I should see modal title "Data Export"
    When I click on Export Type drop down
    Then I should see following items
     |  value                           |
     | Summative Psychometric Item File |
    And I should not see following items
     |  value                           |
     | Summative Assessment Results     |
     | Summative Released Item File     |

  @RALLY_US35548
  Scenario: Verify user is able to access PII data that he/she allowed to see
    When I open the student roster report with the following parameters:
        | param        | value     |
        | stateCode    | NY        |
        | districtGuid | R0003     |
        | schoolGuid   | RP003     |
        | gradeCourse  | Grade 3   |
        | asmtType     | SUMMATIVE |
        | year         | 2015      |
    Then I am redirected to the login page
    When I login as lwoman
    And I am redirected to the student roster report
    Then I wait for the grid to reload
    Then I should see the report title "Grade 3"
    And I should see the subtitle "1 student"

  @RALLY_US35548
  Scenario: Verify user is not able to access PII data that he/she not allowed to see
    When I open the student roster report with the following parameters:
        | param        | value     |
        | stateCode    | RI        |
        | districtGuid | R0003     |
        | schoolGuid   | RP001     |
        | gradeCourse  | Grade 11  |
        | asmtType     | SUMMATIVE |
        | year         | 2015      |
    Then I am redirected to the login page
    When I login as dumbridge
    Then I am redirected to the error page

  @RALLY_US35549
    Scenario: Verify user can drill down to ELA Student Roster Report if he/she has PII permission
    When I open the school report with the following parameters
        | param        | value    |
        | subject      | subject2 |
        | stateCode    | NY       |
        | year         | 2015     |
        | districtGuid | R0003    |
        | schoolGuid   | RP003    |
        | asmtType     | SUMMATIVE|
    Then I am redirected to the login page
    When I login as lwoman
    And I am redirected to the cpop school report
    Then I should see the breadcrumbs "Home,New York,Providence,Hope High School"
    And I should see the report title "Hope High School"
    And I should see the subtitle "4 grades"
    When I click on "Grade 3"
    When I am redirected to the student roster report
    Then I wait for the grid to reload
    Then I should see the report title "Grade 3"
    Then I should see the subtitle "1 student"
    When I click on "Hope High School" link on breadcrumb
    And I am redirected to the cpop school report
    Then I wait for the grid to reload
    Then I should see the report title "Hope High School"
    When I click on "Grade 9"
    And I am redirected to the student roster report
    Then I wait for the grid to reload
    Then I should see the report title "Grade 9"
    Then I should see the subtitle "3 students"

  @RALLY_US35549
    Scenario: Verify user can drill down to Math Student Roster Report if he/she has PII permission
      When I open the school report with the following parameters
        | param        | value    |
        | subject      | subject1 |
        | stateCode    | NY       |
        | year         | 2015     |
        | districtGuid | R0003    |
        | schoolGuid   | RP003    |
        | asmtType     | SUMMATIVE|
    Then I am redirected to the login page
    When I login as lwoman
    And I am redirected to the cpop state report
    Then I should see the breadcrumbs "Home,New York,Providence,Hope High School"
    And I should see the report title "Hope High School"
    And I should see the subtitle "4 grades/courses"
    When I click on "Grade 8"
    Then I wait for the grid to reload
    Then I should see the report title "Grade 8"
    Then I should see the subtitle "1 student"
    When I click on "Hope High School" link on breadcrumb
    Then I wait for the grid to reload
    Then I should see the report title "Hope High School"
    When I click on "Grade 3"
    Then I wait for the grid to reload
    Then I should see the report title "Grade 3"
    Then I should see the subtitle "1 student"

  @RALLY_US35549
    Scenario: Verify user can't drill down to ELA Student Roster Report if he/she does not have PII permission
      When I open the school report with the following parameters
        | param        | value    |
        | subject      | subject2 |
        | stateCode    | RI       |
        | year         | 2015     |
        | districtGuid | R0003    |
        | schoolGuid   | RP003    |
        | asmtType     | SUMMATIVE|
    Then I am redirected to the login page
    When I login as dumbridge
    And I am redirected to the cpop state report
    Then I should see the breadcrumbs "Home,Rhode Island,Providence,Hope High School"
    And I should see the report title "Hope High School"
    And I should see the subtitle "4 grades"
    And I should see "Grade 3" is not link
    And I should see "Grade 9" is not link
    And I should see "Grade 10" is not link
    And I should see "Grade 11" is not link

  @RALLY_US35549
    Scenario: Verify user can't drill down to Math Student Roster Report if he/she does not have PII permission
      When I open the school report with the following parameters
        | param        | value    |
        | subject      | subject1 |
        | stateCode    | RI       |
        | year         | 2015     |
        | districtGuid | R0003    |
        | schoolGuid   | RP003    |
        | asmtType     | SUMMATIVE|
    Then I am redirected to the login page
    When I login as dumbridge
    And I am redirected to the cpop state report
    Then I should see the breadcrumbs "Home,Rhode Island,Providence,Hope High School"
    And I should see the report title "Hope High School"
    And I should see the subtitle "4 grades/courses"
    And I should see "Grade 3" is not link
    And I should see "Grade 5" is not link
    And I should see "Grade 8" is not link
    And I should see "Integrated Mathematics III" is not link

  @RALLY_US35321
    Scenario: Verify user from an opt-out state cannot see ELA/L results for consortium average
  	  When I open the individual student report with the following parameters:
        | param        | value                                    |
        | subject      | subject2                                 |
        | gradeCourse  | Grade 3                                  |
        | stateCode    | VT                                       |
        | studentGuid  | w7TmiOs6CWJsXS2l1crivMsf42ZlQ5WAr6Xri2rd |
        | year         | 2015                                     |
        | districtGuid | R0003                                    |
        | schoolGuid   | RP003                                    |
        | subject      | subject2                                 |
    Then I am redirected to the login page
    When I login as msmith
    And I am redirected to the individual student report
    Then I verify that the subject selected is "ELA/L"
    Then the overall performance section is displayed on ISR
    And the performance bar legend contains the following:
        | name              | value |
        | * PROBABLE RANGE  | ± 4   |
        | SCHOOL AVG        | 256   |
        | DISTRICT AVG      | 212   |
        | STATE AVG         | 240   |
        | PARCC STATES AVG  | N/A   |
    And the claim performance bar legend for "READING" contains the following:
      | name                       | value  |
      | SUBSCORE                   |    14  |
      | AVG OF STUDENTS AT LEVEL 4 |    N/A |
      | PARCC AVG                  |    N/A |
    And the claim performance bar legend for "WRITING" contains the following:
      | name                       | value  |
      | SUBSCORE                   |    11  |
      | PARCC AVG                  |    N/A |
      | AVG OF STUDENTS AT LEVEL 4 |    N/A |

  @RALLY_US35321
    Scenario: Verify user from an opt-out state cannot see results for consortium average
  	  When I open the individual student report with the following parameters:
        | param        | value                                    |
        | subject      | subject2                                 |
        | gradeCourse  | Grade 3                                  |
        | stateCode    | VT                                       |
        | studentGuid  | w7TmiOs6CWJsXS2l1crivMsf42ZlQ5WAr6Xri2rd |
        | year         | 2015                                     |
        | districtGuid | R0003                                    |
        | schoolGuid   | RP003                                    |
        | subject      | subject2                                 |
    Then I am redirected to the login page
    When I login as msmith
    And I am redirected to the individual student report
    Then I verify that the subject selected is "ELA/L"
    Then the overall performance section is displayed on ISR
    And the performance bar legend contains the following:
        | name              | value |
        | * PROBABLE RANGE  | ± 4   |
        | SCHOOL AVG        | 256   |
        | DISTRICT AVG      | 212   |
        | STATE AVG         | 240   |
        | PARCC STATES AVG  | N/A   |

  @RALLY_US36141
    Scenario: Verify user from VT can view state's aggregate reports for NY that has allowed other states to view its aggregate reports
      When I open the cpop report with the following parameters: 
        | param        | value     | 
        | gradeCourse  | Grade 3   | 
        | asmtType     | SUMMATIVE | 
        | year         | 2015      |
      When I login as msmith
      And I verify that I am on the subject and grade/course initial selection page
      When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | ELA/L        |
      Then I verify that I'm on the performance page
      Then I wait for the grid to reload
      And I validate "All PARCC States" as the report title
      And I should be able to drill down "2" states
      And I should not be able to drill down "1" states
      When I click on "New York"
      Then I should see the breadcrumbs "Home,New York"
      Then I should see the subtitle "3 districts"
      When I click on "Providence"
      Then I should see the breadcrumbs "Home,New York,Providence"
      Then I should see the subtitle "3 schools"
      When I click on "Hope High School"
      Then I should see the breadcrumbs "Home,New York,Providence,Hope High School"
      Then I should see the subtitle "4 grades"
      And I should not be able to drill down "4" grades

  @RALLY_US37978
    Scenario: Verify drill-down is disabled to rosters from school report for users where they have no students
      When I open the school report with the following parameters
        | param        | value    |
        | subject      | subject2 |
        | stateCode    | RI       |
        | year         | 2015     |
        | districtGuid | R0003    |
        | schoolGuid   | RP003    |
        | asmtType     | SUMMATIVE|
    Then I am redirected to the login page
    When I login as imontoya
    And I am redirected to the cpop state report
    Then I should see the breadcrumbs "Home,Rhode Island,Providence,Hope High School"
    And I should see the report title "Hope High School"
    And I should see the subtitle "4 grades"
    And I should see "Grade 3" is not link
    And I should see "Grade 9" is not link
    And I should see "Grade 10" is not link
    And I should see "Grade 11" is not link

  @RALLY_US36146
    Scenario: Verify on student roster, user only sees students whose records match their staff id
    When I open the student roster report with the following parameters:
        | param        | value     |
        | stateCode    | NY        |
        | districtGuid | R0003     |
        | schoolGuid   | RP003     |
        | gradeCourse  | Grade 9   |
        | asmtType     | SUMMATIVE |
        | year         | 2015      |
    Then I am redirected to the login page
    When I login as lwoman
    And I am redirected to the student roster report
    Then I wait for the grid to reload
    Then I should see the report title "Grade 9"
    And I should see the subtitle "3 students"
    When I click on the "Scores" view on the student roster report
    And I click on "Gatti, Lois C."
    When I navigate to the new opened window
    And I am redirected to the individual student report
    Then I should see the ISR Header with PARCC logo
    And the ISR header should display the students name "Lois Gatti" in the header title
