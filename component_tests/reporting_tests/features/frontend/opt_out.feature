Feature: Context security and permissions
  Background: Open a firefox browser and open the webpage
    Given an open browser

  @not_gman
  @RALLY_US37300
  Scenario: Verify checkbox to compare PARCC averages is disabled for opt out states
    When I open the cpop report with the following parameters: 
      | param        | value     | 
      | gradeCourse  | Grade 3   | 
      | asmtType     | SUMMATIVE | 
      | year         | 2015      |
      | subject      | subject2  |
    Then I am redirected to the login page
    When I login as msmith
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | ELA/L        |
    Then I should see the report title "All PARCC States"
    Then I should see the compare bar
    Then I verify that the checkbox to compare PARCC averages is disabled
    And I see an alert popover when I hover over the compare PARCC checkbox
    Then I should see opt out message "Your state has opted to not participate in PARCC consortium level results." on alert popover
    When I click on "Vermont"
    Then I should see the subtitle "3 districts"
    Then I verify that the checkbox to compare PARCC averages is disabled
    And I see an alert popover when I hover over the compare PARCC checkbox
    Then I should see opt out message "Your state has opted to not participate in PARCC consortium level results." on alert popover
    When I click on "Providence"
    Then I should see the subtitle "3 schools"
    Then I verify that the checkbox to compare PARCC averages is disabled
    And I see an alert popover when I hover over the compare PARCC checkbox
    Then I should see opt out message "Your state has opted to not participate in PARCC consortium level results." on alert popover
    When I click on "Hope High School"
    Then I should see the subtitle "4 grades"
    Then I verify that the checkbox to compare PARCC averages is disabled
    And I see an alert popover when I hover over the compare PARCC checkbox
    Then I should see opt out message "Your state has opted to not participate in PARCC consortium level results." on alert popover
    When I click on "Grade 3"
    When I check on SCHOOL checkbox
    Then I wait for the grid to reload
    Then I should see the subtitle "1 student"
    Then I verify that the checkbox to compare PARCC averages is disabled
    And I see an alert popover when I hover over the compare PARCC checkbox
    Then I should see opt out message "Your state has opted to not participate in PARCC consortium level results." on alert popover

  @RALLY_US37300
  Scenario: Verify info icon on PARCC summary row triggers hover with list of opted-in states
    When I open the cpop report with the following parameters: 
      | param        | value     | 
      | gradeCourse  | Grade 3   | 
      | asmtType     | SUMMATIVE | 
      | year         | 2015      |
      | subject      | subject2  |
    Then I am redirected to the login page
    When I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | ELA/L        |
    Then I verify that I'm on the performance page
    Then I wait for the grid to reload
    And I validate "All PARCC States" as the report title
    And I should see "PARCC AVG" on first row
    Then I should see the value "PARCC AVG" for "2 States" on summary row
    When I hover over the info icon on PARCC summary row
    Then a list of opted-in states "NY, RI" is displayed on the info popover
    When I click on "Vermont"
    Then I should see the subtitle "3 districts"
    When I check on PARCC checkbox
    Then I should see the summary row for "PARCC"
    Then I should see the value "PARCC AVG" for "2 States" on summary row
    When I hover over the info icon on PARCC summary row
    Then a list of opted-in states "NY, RI" is displayed on the info popover
    When I click on "Providence"
    Then I should see the subtitle "3 schools"
    When I check on PARCC checkbox
    Then I should see the summary row for "PARCC"
    Then I should see the value "PARCC AVG" for "2 States" on summary row
    When I hover over the info icon on PARCC summary row
    Then a list of opted-in states "NY, RI" is displayed on the info popover
    When I click on "Hope High School"
    Then I should see the subtitle "4 grades"
    When I click on "Grade 3"
    Then I should see the subtitle "5 students"
    When I check on PARCC checkbox
    Then I should see the summary row for "PARCC"
    Then I should see the value "PARCC AVG" for "2 States" on summary row
    When I hover over the info icon on PARCC summary row
    Then a list of opted-in states "NY, RI" is displayed on the info popover
