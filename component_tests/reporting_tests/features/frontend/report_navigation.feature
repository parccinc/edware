Feature: Report navigation

  Background: Open a firefox browser and open the webpage
    Given an open browser

  Scenario: Navigate to a student from student roster to isr report
    When I open the student roster report with the following parameters:
        | param        | value     |
        | stateCode    | RI        |
        | districtGuid | R0003     |
        | schoolGuid   | RP001     |
        | gradeCourse  | Grade 9   |
        | asmtType     | SUMMATIVE |
        | year         | 2015      |
        | subject      | subject2  |
        | view         | scores    |
    Then I am redirected to the login page
    When I login as gman
#    And I verify that I am on the subject and grade/course initial selection page
#    When I select the following options from the initial subject and grade/course selection page:
#        | param        | value        |
#        | gradeCourse  | 9th Grade   |
#        | subject      | ELA/L        |
    And I am redirected to the student roster report
    When I click on the "Scores" view on the student roster report
    And I click on "Cabello, Roman L."
    When I navigate to the new opened window
    And I am redirected to the individual student report
    Then I should see the ISR Header with PARCC logo
    And the ISR header should display the students name "Roman Cabello" in the header title
    Then I verify that the subject selected is "ELA/L"
    Then the overall performance section is displayed on ISR
    And the overall performance level is displayed as "Level 5"
    And the overall description displayed contains score "463", performance level "Level 5"
    And the performance bar is displayed with the student score marked as "463" for "ELA"
    And I click on the print button and choose "Color" pdf
    When I navigate to the new opened window
    Then the content body is loaded without error

  Scenario: Navigate from student roster to isr report - Math course
    When I open the student roster report with the following parameters:
        | param        | value     |
        | stateCode    | RI        |
        | districtGuid | R0001     |
        | schoolGuid   | RN001     |
        | gradeCourse  | Algebra+I |
        | subject      | subject1  |
        | asmtType     | SUMMATIVE |
        | year         | 2015      |
        | view         | scores    |
    Then I am redirected to the login page
    When I login as gman
    And I am redirected to the student roster report
    And I click on "Augustus, Verena L."
    When I navigate to the new opened window
    And I am redirected to the individual student report
    Then I should see the ISR Header with PARCC logo
    And the ISR header should display the students name "Verena Augustus" in the header title
    Then I verify that the subject selected is "Mathematics"

  @RALLY_US34554
  Scenario: Verify grade or course selection is saved correctly for one user
    When I open the cpop report with the following parameters: 
      | param        | value     | 
      | asmtType     | SUMMATIVE | 
      | year         | 2015      |
      | subject      | subject2  |
    Then I am redirected to the login page
    When I login as gman
    Then I wait for the grid to reload
    When I click on GRADE/COURSE dropdown
    And I select "11th Grade" from GRADE/COURSE dropdown
    Then I wait for the grid to reload
    Then I log out
    When I open the cpop report with the following parameters: 
      | param        | value     | 
      | asmtType     | SUMMATIVE | 
      | year         | 2015      |
      | subject      | subject2 |
    Then I am redirected to the login page
    When I login as gman
    Then I wait for the grid to reload
    Then I verify that the grade or course selected is "11th Grade"

  @RALLY_US34554
  Scenario: Verify grade or course selection is saved correctly when switching subjects
    When I open the cpop report with the following parameters: 
      | param        | value     | 
      | asmtType     | SUMMATIVE | 
      | year         | 2015      |
      | subject      | subject1  |
    Then I am redirected to the login page
    When I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | Mathematics  |
    Then I wait for the grid to reload
    When I click on GRADE/COURSE dropdown
    And I select "4th Grade" from GRADE/COURSE dropdown
    Then I wait for the grid to reload
    When I select "ELA/L" from the subject dropdown
    And I verify that I am on the subject and grade/course initial selection page
    And I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 4th Grade    |
        | subject      | ELA/L        |
    Then I wait for the grid to reload
    When I click on GRADE/COURSE dropdown
    And I select "3rd Grade" from GRADE/COURSE dropdown
    Then I wait for the grid to reload
    When I select "Mathematics" from the subject dropdown
    Then I wait for the grid to reload
    Then I verify that the grade or course selected is "4th Grade"

  @RALLY_US34554
  Scenario: Verify grade or course selection is saved correctly when switching users
    When I open the cpop report with the following parameters: 
      | param        | value     | 
      | asmtType     | SUMMATIVE | 
      | year         | 2015      |
      | subject      | subject1  |
    Then I am redirected to the login page
    When I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | Mathematics  |
    Then I wait for the grid to reload
    When I click on GRADE/COURSE dropdown
    And I select "4th Grade" from GRADE/COURSE dropdown
    Then I wait for the grid to reload
    Then I log out
    When I open the cpop report with the following parameters: 
      | param        | value     | 
      | asmtType     | SUMMATIVE | 
      | year         | 2015      |
      | subject      | subject1  |
    Then I am redirected to the login page
    When I login as msmith
    And I verify that I am on the subject and grade/course initial selection page
    And I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 5th Grade    |
        | subject      | Mathematics  |
    Then I wait for the grid to reload
    When I click on GRADE/COURSE dropdown
    And I select "5th Grade" from GRADE/COURSE dropdown
    Then I wait for the grid to reload
    Then I log out
    When I open the cpop report with the following parameters: 
      | param        | value     | 
      | asmtType     | SUMMATIVE | 
      | year         | 2015      |
      | subject      | subject1  |
    Then I am redirected to the login page
    When I login as gman
    Then I wait for the grid to reload
    Then I verify that the grade or course selected is "4th Grade"

  @RALLY_US37932
  Scenario: Verify subject and grade course selection initial page is displayed initially and then the cpop page is available from second time onwards
    When I open the cpop report with the following parameters: 
      | param        | value     | 
      | asmtType     | SUMMATIVE | 
      | year         | 2015      |
      | subject      | subject2  |
    Then I am redirected to the login page
    When I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select "ELA/L" from the subject dropdown
    Then I should see GRADE/COURSE button is enabled
    When I click on GRADE/COURSE dropdown
    When I select "3rd Grade" from GRADE/COURSE dropdown
    Then I wait for the grid to reload
    Then I log out
    When I open the cpop report with the following parameters: 
      | param        | value     | 
      | asmtType     | SUMMATIVE | 
      | year         | 2015      |
    Then I am redirected to the login page
    When I login as gman
    Then I wait for the grid to reload
    Then I verify that the grade or course selected is "3rd Grade"

  @RALLY_US37932
  Scenario: Selecting new subject from cpop redirects user to initial subject/grade selection page
    When I open the cpop report with the following parameters: 
      | param        | value     | 
      | asmtType     | SUMMATIVE | 
      | year         | 2015      |
    Then I am redirected to the login page
    When I login as gman
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value        |
        | gradeCourse  | 3rd Grade    |
        | subject      | ELA/L        |
    Then I wait for the grid to reload
    Then I verify that the subject selected is "ELA/L"
    When I select "Mathematics" from the subject dropdown
    And I verify that I am on the subject and grade/course initial selection page
    When I select the following options from the initial subject and grade/course selection page:
        | param        | value              |
        | gradeCourse  | 3rd Grade          |
        | subject      | Mathematics        |
    Then I wait for the grid to reload
    Then I verify that the subject selected is "Mathematics"
    When I select "ELA/L" from the subject dropdown
    Then I wait for the grid to reload