from behave import when, then
from components_tests.frontend.widgets.grid import Grid
from components_tests.frontend.widgets.year_selector import YearSelector


@then('I should see "{academic_year}" on Academic Year dropdown')
def check_selected_year(context, academic_year):
    actual_value = context.driver.find_element_by_css_selector("#yearSelector .selected-label").text
    assert academic_year == actual_value, "value should match\nExpected: {0}\nActual: {1}".format(academic_year, actual_value)


@when('I click on "Academic Year" drop-down')
def step_impl(context):
    context.driver.find_element_by_css_selector("#yearSelector .caret").click()


@when('I select "{year}" from dropdown')
def select_academic_year(context, year):
    yearSelector = YearSelector(context)
    yearSelector.click_academic_year()
    yearSelector.select_academic_year(year)
    Grid(context).wait_while_reloading()


@then('I should see following values in the year dropdown')
def step_impl(context):
    yr_list = context.driver.find_elements_by_css_selector(
        "#yearSelector .dropdown-menu li")
    values = []
    for row in context.table:
        values.append(row['value'])
    for year in yr_list:
        year_text = year.text
        if year_text in values:
            values.remove(year_text)
    assert values == []


@then('I verified "{exp_data}" value in "{selector_type}" selected')
def step_impl(context, exp_data, selector_type):
    if selector_type == 'ACADEMIC YEAR':
        assert selector_type == context.driver.find_element_by_css_selector('#yearSelector .year-label').text
        assert exp_data == context.driver.find_element_by_css_selector('#yearSelector .btn-group .selected-label').text
    elif selector_type == 'SUBJECT':
        assert selector_type == context.driver.find_element_by_css_selector(
            '#subjectSelector .subject-label').text
        assert exp_data == context.driver.find_element_by_css_selector(
            '#subjectSelector .btn-group .selected-label').text
    elif selector_type == 'RESULTS':
        assert selector_type == context.driver.find_element_by_css_selector(
            '#resultSelector .result-label').text
        assert exp_data == context.driver.find_element_by_css_selector(
            '#resultSelector .btn-group .selected-label.label-wrap').text
    else:
        raise AssertionError('Error: selector element not found.')
