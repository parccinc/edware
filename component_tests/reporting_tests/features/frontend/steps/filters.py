from behave import when, then
from components_tests.frontend.widgets.filters import Filter
from components_tests.frontend.widgets.grid import Grid


def get_filter_bar(context):
    return context.driver.find_element_by_id("infoBar").find_element_by_class_name("filter-editor").find_element_by_class_name("animating-spacer")


def get_all_filters(context):
    filter_bar = get_filter_bar(context)
    return filter_bar.find_element_by_class_name("selectors").find_elements_by_class_name("btn-group")


def select_filter(context, name):
    all_filters = get_all_filters(context)
    filters = ['Gender', 'Race / Ethnicity', 'Disabilities',
               'ELL / LEP', 'Economic Disadvantages']
    return all_filters[filters.index(name)]


def click_filter_selection(context, name, selection):
    filter = select_filter(context, name)
    filter.find_element_by_tag_name("button").click()
    all_options = filter.find_elements_by_tag_name("li")
    for opt in all_options:
        if opt.text in selection:
            opt.find_element_by_tag_name("label").click()


def remove_filter_from_crumbbar(context, name):
    buttons = context.driver.find_element_by_class_name(
        "crumb-bar").find_elements_by_class_name("filter-crumb")
    for b in buttons:
        print(b.text)
        if name in b.text:
            b.click()
            break


def verify_filter(filter, name, exp_options):
    assert name == str(filter.find_element_by_tag_name(
        "button").text), "{0} filter not found".format(name)
    filter.find_element_by_tag_name("button").click()
    all_options = filter.find_elements_by_tag_name("li")
    actual_options = []
    for each in all_options:
        actual_options.append(each.text)
    assert exp_options == actual_options, "Invalid filter options displayed as: {0}".format(
        actual_options)

# Behave Steps


@when('I click on the Filters button to "{action}" the filter menu')
def open_filter(context, action):
    # TODO: I don't understand the use of `action` here
    assert action in ['open', 'close']
    filters = Filter(context)
    filters.test_filter_label()
    filters.click_filter_link()


@when('I verify the filtering options available on the filter menu')
def step_impl(context):
    filter_bar = get_filter_bar(context)
    assert "STUDENT FILTERS" == str(
        filter_bar.find_element_by_class_name("filters-header").text)
    all_filters = get_all_filters(context)
    assert 5 == len(
        all_filters), "5 filtering options not found in the filter menu"

    verify_filter(
        all_filters[0], "Gender", ['Male', 'Female'])
    verify_filter(all_filters[1], "Race / Ethnicity", ['American Indian or Alaska Native', 'Asian', 'Black or African American',
                                                       'Hispanic or Latino', 'White', 'Native Hawaiian or other Pacific Islander', 'Two or more races'])
    verify_filter(all_filters[2], "Disabilities", ['Yes', 'No'])
    verify_filter(all_filters[3], "ELL / LEP", ['ELL', 'LEP', 'Both'])
    verify_filter(all_filters[4], "Economic Disadvantages", ['Yes', 'No'])


@when('I click on the "{button_name}" button to "{action}" any changes and close the filter menu')
def step_impl(context, button_name, action):
    filter_bar = context.driver.find_element_by_id("infoBar").find_element_by_class_name(
        "filter-editor").find_element_by_class_name("animating-spacer").find_element_by_class_name("text-center")
    all_buttons = filter_bar.find_elements_by_tag_name("button")
    actual_button_names = []
    for each in all_buttons:
        actual_button_names.append(each.text)
    assert ['Cancel', 'Apply Filters'] == actual_button_names
    if button_name == "Cancel":
        to_click = all_buttons[0]
    elif button_name == "Apply Filters":
        to_click = all_buttons[1]
    else:
        raise AssertionError(
            'Invalid button name passed in the step. Valid values are: Cancel, Apply Filters, and Clear All')
    to_click.click()
    Grid(context).wait_while_reloading()


@when('I select "{selection}" options from the "{filter_name}" filter')
def step_impl(context, selection, filter_name):
    click_filter_selection(context, filter_name, selection.split(','))


@when('I remove "{name}" from the filter bar')
def step_impl(context, name):
    remove_filter_from_crumbbar(context, name)


@when('I click the "{button_name}" button')
def step_impl(context, button_name):
    if button_name == "Clear All":
        to_click = context.driver.find_element_by_class_name(
            "clear-all-filters")
    elif button_name == "Edit":
        to_click = context.driver.find_element_by_class_name("edit-filters")
    else:
        raise AssertionError(
            'Invalid button name. Valid values are Clear All, and Edit')
    to_click.click()
    Grid(context).wait_while_reloading()


@then('I verify that the filters menu is "{close_option}"')
def step_impl(context, close_option):
    try:
        open_filter = context.driver.find_element_by_css_selector(".filter-bar.expanded")
    except:
        open_filter = None
    if close_option == "closed":
        assert not open_filter
    elif close_option == "open":
        assert open_filter
    else:
        raise AssertionError(
            'Invalid option. Valid options are closed and open.')


@then('I verify that no filters are shown')
def step_impl(context):
    filters = context.driver.find_elements_by_css_selector(".filter-crumb")
    assert not filters
