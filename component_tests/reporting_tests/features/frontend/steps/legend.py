from behave import then
from hamcrest import assert_that, equal_to, has_length, not_none, is_not, contains_string, has_item
from components_tests.frontend.widgets.legend import Legend
from selenium.common.exceptions import TimeoutException


def test_label(legend, exp_label):
    legend_label = legend.get_legend_label()
    # assert_that(legend_label, equal_to('Legend:'), "Legend: label not found on the legend bar.")
    assert_that(legend_label, equal_to(exp_label), "Legend label incorrectly displayed on the legend bar.")


def test_subclaim_levels(legend, expected_perf_levels):
    perf_levels = legend.get_legend_levels()
    assert_that(perf_levels, has_length(len(expected_perf_levels)))
    for element, expected in zip(perf_levels, expected_perf_levels):
        perf_img = element.find_element_by_class_name(expected['class_name'])
        assert_that(perf_img, not_none(), 'Performance level image not found.')
        desc = element.find_elements_by_tag_name("p")
        assert_that(desc[0].text, expected['text'])
        assert_that(desc[1].text, expected['desc'])


def test_toggle_button(legend):
    legend.click_hide_button()
    try:
        legend.wait_for_class('collapsed')
    except TimeoutException:
        assert False, "Legend bar is not collapsed."


def test_expressiveness_levels(legend, expected_perf_levels):
    act_perf_levels = legend.get_legend_levels()
    assert_that(act_perf_levels, has_length(len(expected_perf_levels)))

    for element, expected in zip(act_perf_levels, expected_perf_levels):
        perf_img = element.find_element_by_class_name("level-colored-value")
        assert_that(perf_img, not_none(), 'Performance level image not found.')
        assert_that(perf_img.text, equal_to(expected["level_name"]), "Performance Label image text incorrectly displayed on the legend bar.")
        desc = element.find_element_by_tag_name("p")
        assert_that(desc.text, equal_to(expected["desc"]), "Description incorrectly displayed on the legend bar.")


@then('I should see the expressiveness rubric legend bar shorter version')
def expressiveness_legend(context):
    legend = Legend(context)
    test_label(legend, 'Expressiveness:')
    legend_perf_level = [
        {
            'level_name': '0',
            'desc': 'Silence or irrelevant',
        },
        {
            'level_name': '1',
            'desc': 'Word-by-word',
        },
        {
            'level_name': '2',
            'desc': 'Two-word phrases',
        },
        {
            'level_name': '3',
            'desc': 'Three- or four-word phrases',
        },
        {
            'level_name': '4',
            'desc': 'Larger, meaningful phrases'
        }
    ]
    test_expressiveness_levels(legend, legend_perf_level)


@then('I should see the expressiveness rubric legend bar longer version')
def expressiveness_legend(context):
    legend = Legend(context)
    test_label(legend, 'Expressiveness:')
    legend_perf_level = [
        {
            'level_name': '0',
            'desc': 'Silence or irrelevant or completely unintelligible material.',
        },
        {
            'level_name': '1',
            'desc': 'Reads primarily word-by-word. Occasional two-word or three-word phrases may occur - but these are infrequent and/or they do not preserve meaningful syntax.',
        },
        {
            'level_name': '2',
            'desc': 'Reads primarily in two-word phrases with some three-or-four-word groupings. Some word-by-word reading may be present. Word groupings may seem awkward and unrelated to larger context of sentence of passage.',
        },
        {
            'level_name': '3',
            'desc': 'Reads primarily in three- or four-word phrase groups. Some smaller groupings may be present. However, the majority of phrasing seems appropriate and preserves the syntax of the author. Little or no expressive interpretation is present.'
        },
        {
            'level_name': '4',
            'desc': "Reads primarily in larger, meaningful phrase groups. Although some regressions, repetitions, and deviations from text may be present, these do not appear to detract from the overall structure of the story. Preservation of the author's syntax is consistent. Some or most of the story is read with expressive interpretation.",
        }
    ]
    test_expressiveness_levels(legend, legend_perf_level)


@when('I click on details in the expressiveness legend to {action} the legend')
def expressiveness_legend_action(context, action):
    legend = Legend(context)
    context.driver.find_element_by_css_selector('.legend').find_element_by_tag_name("button").click()
    if action == "expand":
        assert_that("Hide details", str(context.driver.find_element_by_css_selector('.legend').find_element_by_tag_name("button").text))
        try:
            legend.wait_for_class('hide-details-btn')
        except TimeoutException:
            assert False, "Expressiveness Legend bar is not expanded."
    elif action == "collapse":
        assert_that("Click for details...", str(context.driver.find_element_by_css_selector('.legend').find_element_by_tag_name("button").text))
        try:
            legend.wait_for_class('show-details-btn')
        except TimeoutException:
            assert False, "Expressiveness Legend bar is not collapsed."


@then('I see the legend with three performance level description')
def test_isr_legend(context):
    legend = Legend(context)
    test_label(legend, 'Legend:')
    legend_perf_level = [
        {
            'class_name': 'perf_level_1',
            'text': 'BELOW',
            'desc': 'STUDENTS PERFORMING AT LEVEL 4',
        },
        {
            'class_name': 'perf_level_2',
            'text': 'NEAR',
            'desc': 'STUDENTS PERFORMING AT LEVEL 4',
        },
        {
            'class_name': 'perf_level_3',
            'text': 'AT OR ABOVE',
            'desc': 'STUDENTS PERFORMING AT LEVEL 4',
        },
    ]
    test_subclaim_levels(legend, legend_perf_level)
    test_toggle_button(legend)


@then('I see the student roster score view legend')
def test_student_roster_legend(context):
    legend = Legend(context)
    test_label(legend, 'Legend:')
    legend_perf_level = [
        {
            'class_name': 'perf_level_1',
            'text': 'BELOW',
            'desc': 'LVL 4 AVERAGE',
        },
        {
            'class_name': 'perf_level_2',
            'text': 'NEAR',
            'desc': 'LVL 4 AVERAGE',
        },
        {
            'class_name': 'perf_level_3',
            'text': 'AT OR ABOVE',
            'desc': 'LVL 4 AVERAGE',
        },
    ]
    test_subclaim_levels(legend, legend_perf_level)
    test_toggle_button(legend)


@then('I verify that the legend is shown by default')
def step_impl(context):
    legend_shown = False
    below_show = context.driver.find_element_by_css_selector('.below.show')
    if below_show.find_element_by_css_selector('.legend'):
        legend_shown = True
    assert legend_shown


@then('I verify that the legend can be hidden and reshown')
def step_impl(context):
    legend_hidden = False
    legend_reshown = False
    legend = context.driver.find_element_by_css_selector('.legend')
    hide = legend.find_element_by_css_selector('.hide')
    hide.click()
    below_collapsed = context.driver.find_element_by_css_selector(
        '.below.collapsed')
    if below_collapsed.find_element_by_css_selector('.legend'):
        legend_hidden = True
    assert legend_hidden
    unhide = below_collapsed.find_element_by_css_selector('.unhide')
    print(unhide)
    unhide.click()
    below_show = context.driver.find_element_by_css_selector('.below.show')
    if below_show.find_element_by_css_selector('.legend'):
        legend_reshown = True
    assert legend_reshown


@then('I verify that the overall legend is shown')
def test_overall_legend(context):
    levels = Legend(context).get_legend_levels()
    assert_that(levels, has_length(5))


@then('I verify that the subscore legend is shown')
def test_subscore_legend(context):
    levels = Legend(context).get_legend_levels()
    assert_that(levels, has_length(3))


@then('I should see the Probability of Mastery legend bar with cutpoints')
def test_diagnostic_legend_presence(context):
    legend = Legend(context)
    test_label(legend, 'Legend:')
    legend_perf_level = [
        {
            'class_name': 'x',
            'text': 'PROBABLY NOT',
            'desc': 'MASTERED (0 - 0.39)',
        },
        {
            'class_name': 'o',
            'text': 'POSSIBLY',
            'desc': 'MASTERED (0.4 - 0.59)',
        },
        {
            'class_name': 'check',
            'text': 'PROBABLY',
            'desc': 'MASTERED (0.6 - 1)',
        },
    ]
    test_subclaim_levels(legend, legend_perf_level)


@then('I should not see the Probability of Mastery legend bar with cutpoints')
def test_diagnostic_legend_absence(context):
    legend_container = Legend(context).get_legend_container()
    assert_that(legend_container, is_not(contains_string('Legend')))


@then('I verify ccr styling is present on the legend bar')
def test_ccr_styling_presence(context):
    classes = Legend(context).get_legend_classes()
    assert_that(classes, has_item('ccr'))


@then('I verify ccr styling is not present on the legend bar')
def test_ccr_styling_absence(context):
    classes = Legend(context).get_legend_classes()
    assert_that(classes, is_not(has_item('ccr')))
