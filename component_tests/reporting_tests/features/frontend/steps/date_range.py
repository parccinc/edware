from behave import when, then
from selenium.webdriver.common.keys import Keys


@then('I verified all the records on report should be within the date range')
def step_impl(context):
    date_range_elements_value = context.driver.find_element_by_css_selector('.date-range-picker').text
    date_range_from = date_range_elements_value.split('-')[0][6:]
    date_range_to = date_range_elements_value.split('-')[1]
    date_elements = context.driver.find_elements_by_css_selector('#gridTable .grid-canvas .slick-cell.l3.r3')
    date_in_student_rec = []
    for each in date_elements:
        date_in_student_rec.append(each.text)
    assert date_range_from.strip() in date_in_student_rec, "value should match\nexpected: {0}\nactual: {1}".format(date_range_from, from_date_table[3])
    assert date_range_to.strip() in date_in_student_rec, "\nexpected: {0}\nactual: {1}".format(date_range_to, from_date_table[3])


@when('I enter "{input_date_value}" date into "{cal_section}" edit field')
def step_impl(context, input_date_value, cal_section):
    to_calendar_edit_box = '.daterangepicker_end_input .input-mini'
    from_calendar_edit_box = '.daterangepicker_start_input .input-mini'
    if cal_section == "TO CALENDAR":
        context.driver.find_element_by_css_selector(to_calendar_edit_box).clear()
        context.driver.find_element_by_css_selector(to_calendar_edit_box).send_keys(input_date_value)
        context.driver.find_element_by_css_selector(to_calendar_edit_box).send_keys(Keys.RETURN)
    elif cal_section == "FROM CALENDAR":
        context.driver.find_element_by_css_selector(from_calendar_edit_box).clear()
        context.driver.find_element_by_css_selector(from_calendar_edit_box).send_keys(input_date_value)
        context.driver.find_element_by_css_selector(from_calendar_edit_box).send_keys(Keys.RETURN)
    else:
        raise "Calendar elements not found."


@when('I click "{direction}" arrow on "{cal_section}" calendar')
def step_impl(context, direction, cal_section):
    context.driver.find_element_by_css_selector('.daterangepicker .calendar.{0} .{1}'.format(cal_section, direction)).click()


@when('I select a day {day_on_cal} on "{cal_section}" calendar')
def step_impl(context, cal_section, day_on_cal):
    list_of_days = context.driver.find_elements_by_css_selector('.calendar.{0} .calendar-date .table-condensed>tbody>tr .available.in-range'.format(cal_section))
    for each in list_of_days:
        if each.text == str(day_on_cal):
            each.click()
            break


@when('I click on apply button')
def step_impl(context):
    context.driver.find_element_by_css_selector('.daterangepicker .applyBtn').click()


@when('I click on reset date links')
def step_impl(context):
    context.driver.find_element_by_css_selector('.date-selector .btn').click()


@when('I click on date range selector link')
def step_impl(context):
    context.driver.find_element_by_css_selector('.date-range-picker').click()
