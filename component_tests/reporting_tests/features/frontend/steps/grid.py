import re
from behave import when, then
from hamcrest import assert_that, is_, equal_to, has_length, has_item, not_none, greater_than_or_equal_to
from selenium.webdriver.common.action_chains import ActionChains
from components_tests.frontend.widgets.grid import Grid
from components_tests.frontend.widgets.info_bar import InfoBar


@when('I click on "{name}"')
def click_on_link(context, name):
    Grid(context).click_link(name)


@when('I click on column "{column}"')
def step_context(context, column):
    Grid(context).click_column(column)


@when('I click on {column} column for sorting')
def sort_column(context, column):
    Grid(context).click_column(column)


@then('I wait for the grid to reload')
def wait_for_grid(context):
    Grid(context).wait_while_reloading()


@then('I should see the value "{value}" in the first "{col_name}" cell')
def test_first_column_header(context, value, col_name):
    actual = Grid(context).get_first_row_of_column_by_name(col_name)
    assert_that(actual, equal_to(value))


@when('I hover over the info icon in the "vs parcc" column')
def hover_parcc_info_icon(context):
    Grid(context).hover_parcc_icon()


@when('I hover over the info icon in the "vs state" column')
def hover_state_info_icon(context):
    Grid(context).hover_state_icon()


@when('I hover on the info icon in the "{column_name}" column')
def step_impl(context, column_name):
    Grid(context).hover_column_info_icon(column_name)


@when('I hover on the info icon in the writing "{column_name}" column')
def step_impl(context, column_name):
    selector = Grid(context).get_writing_selector_name(column_name)
    Grid(context).hover_column_info_icon(selector)


@then('I should see message "{message}" on alert popover')
def verify_alert_popover_message(context, message):
    actual_message = Grid(context).get_alert_popover_message(message)
    assert_that(actual_message, equal_to(message))


@when('I hover over the header')
def hover_over_header(context):
    Grid(context).hover_header()


@then('I should see message “{tooltip}” on alert popover')
def step_impl(context, tooltip):
    pass


@then('I should see {num_of_cols} columns in the table')
def test_number_of_columns(context, num_of_cols):
    actual_num_cols = Grid(context).get_number_of_columns()
    assert_that(actual_num_cols, equal_to(int(num_of_cols)))


@then('I validate {num_of_rec} students records in table')
def test_number_of_rows(context, num_of_rec):
    actual = Grid(context).get_number_of_rows()
    assert_that(actual, equal_to(int(num_of_rec)))


@when('I click on "{direction}" pagination')
def click_pagination(context, direction):
    if direction == "NEXT":
        Grid(context).click_next()
    elif direction == "PREVIOUS":
        Grid(context).click_previous()


@then('I should verify {num_of_col} cols in "{direction}" pagination link')
def test_pagination_link(context, num_of_col, direction):
    assert direction in ('NEXT', 'PREVIOUS')
    link = Grid(context).get_pagination_link(num_of_col, direction).upper()
    expected = "%s (%s COLS)" % (direction, num_of_col)
    assert_that(link, equal_to(expected))


@then('I see {count} summary rows')
def test_number_of_summary_rows(context, count):
    summary_rows = Grid(context).get_grouped_rows()
    assert_that(summary_rows, has_length(int(count)))


@then('I should see following values in the summary row {row_pos}')
def step_impl(context, row_pos):
    element_row = context.driver.find_element_by_css_selector(
        '#gridHeader .grid-canvas .slick-row:nth-child({0})'.format(row_pos))
    text_values = element_row.text.splitlines()
    expected_values = [row['value'] for row in context.table]
    assert expected_values == text_values, "value should match\nExpected: {0}\nActual: {1}".format(
        expected_values, text_values)


@then('I should see following values at row {row_pos}')
def test_row_in_grid(context, row_pos):
    text_values = Grid(context).get_values_at_row(row_pos)
    expected_values = [row['value'] for row in context.table]
    assert_that(text_values, equal_to(expected_values))


@when('I click on {col_name} column in {section_name} for sorting')
def sort_column_in_table(context, col_name, section_name):
    Grid(context).click_column_in_table(col_name, section_name)


@then('in {section_name} section, I should see the following values in the top row')
def step_impl(context, section_name):
    g = Grid(context)
    grid = g.get_grid_by_section_name(section_name)
    actual_values = grid.find_element_by_css_selector('.slick-row').text.splitlines()
    expected_values = [row['value'] for row in context.table]
    assert_that(actual_values, is_(equal_to(expected_values)))


@then('I validate the top record as follows')
def step_impl(context):
    data_array = []
    for row in context.table:
        data_array.append(row['value'])
    list_of_field_elms = context.driver.find_elements_by_css_selector(
        '#gridTable .slick-viewport .slick-row:first-child .slick-cell')
    row_values = []
    for element in list_of_field_elms:
        element_text = element.text.replace('\n', ' ')
        row_values.append(element_text)
    for each in data_array:
        assert each in row_values, "value should match\nExpected: {0}\nActual: {1}".format(data_array, row_values)


@then('I validate column label should be following')
def test_grid_headers(context):
    expected = [row['col_label'] for row in context.table]
    actual = Grid(context).get_headers()
    assert_that(actual, equal_to(expected))


@then('I verify that the first row is a grouped row')
def test_first_group_row(context):
    elements = Grid(context).get_first_grouped_row()
    assert_that(elements, not_none())


@then('I verify the number of {subdivision_type} matches the number of grouped rows in the grid')
def test_number_of_rows_in_each_group(context, subdivision_type):
    claimed_length = InfoBar(context).get_claimed_length_from_subtitle()
    row_elements = Grid(context).get_group_headers()
    assert_that(row_elements, has_length(claimed_length))


@then('I verify that the compare rows are added')
def test_compare_rows(context):
    checkboxes = InfoBar(context).check_all_compare_checkboxes()
    group_rows = Grid(context).get_grouped_rows()
    for group in group_rows:
        assert_that(checkboxes, has_item(group))


@then('I verify that the grades are {grades}')
def test_grades(context, grades):
    grades = grades.split(',')
    headers = Grid(context).get_group_headers()
    assert_that(headers, has_length(len(grades)))
    for expected in grades:
        assert_that(headers, has_item(expected.strip()))


@when('I click on GRADES toggle for "{subject}"')
def click_subject_link(context, subject):
    Grid(context).click_group_toggle(subject)


@then('I should see message "{message}" on tooltip at row {row_pos}')
def test_tooltip_at_row(context, message, row_pos):
    actual_message = context.driver.find_element_by_css_selector(
        "#gridTable .grid-canvas .slick-row:nth-child({0}) .l0 .icon".format(row_pos)).get_attribute("data-content")
    assert message == actual_message, "value should match\nExpected: {0}\nActual: {1}".format(
        message, actual_message)


@then('I should see a subscore avg column "{presence}"')
def step_impl(context, presence):
    elements = context.driver.find_elements_by_css_selector(
        "div#gridTable span.slick-column-name")
    avg_col_found = False
    for element in elements:
        if "READING" in element.text and "WRITING" in element.text:
            avg_col_found = True
    if presence == "is present":
        assert len(elements) == 6
        assert avg_col_found
    elif presence == "is not present":
        assert len(elements) == 5, "expected {1}, got {0}".format(
            len(elements), 5)
        assert not avg_col_found
    else:
        raise AssertionError("Invalid option passed to the step")


@then('I verify lvl4 percentage for first {subdivision_type}')
def step_impl(context, subdivision_type):
    def get_lvl_percentage(level_num):
        try:
            level = context.driver.find_element_by_css_selector(
                "div.slick-row:first-child .level-{0}".format(level_num))
            return int(level.text.replace("%", ""))
        except:
            # No level
            return 0

    if subdivision_type in ["school", "district"]:
        level3_perc = get_lvl_percentage(3)
        level4_perc = get_lvl_percentage(4)
        level5_perc = get_lvl_percentage(5)
        total_percentage = level4_perc + level5_perc
        avg_score_element = context.driver.find_element_by_css_selector(
            "div.slick-row:first-child div.slick-cell.l3")
        avg_score = int(avg_score_element.text.replace("%", ""))
        assert total_percentage == avg_score, "Lvl4 percentage doesn't match.\n{0} in performance bar.\n{1} in ≥ LVL 4 column".format(
            total_percentage, avg_score)
    else:
        raise AssertionError('Invalid subdivision type')


@then('I verify average score for first school is {value}')
def step_impl(context, value):
    avg_score_element = context.driver.find_element_by_css_selector(
        "div#gridTable div.ui-widget-content.slick-row.even div.slick-cell.l4")
    assert int(value) == int(
        avg_score_element.text), "SCORE: " + avg_score_element.text


@then('I verify average score for first {subdivision_type} is {value}')
def step_impl(context, value, subdivision_type):
    if subdivision_type in ["school", "district"]:
        avg_score_element = context.driver.find_element_by_css_selector(
            "div#gridTable div.ui-widget-content.slick-row.even div.slick-cell.l4")
        assert int(value) == int(
            avg_score_element.text), "Unexpected average score value.\nExpected: {0}\nActual: {1}".format(
            value, avg_score_element.text)
    else:
        raise AssertionError('Invalid subdivision type')


@then('I verify the "{claim}" avg subscore for first {subdivision_type} is "{value}"')
def step_impl(context, claim, value, subdivision_type):
    if subdivision_type in ["school", "district"]:
        selector = "div#gridTable  div.ui-widget-content.slick-row.even div.slick-cell.l5 div.{0} span.avgRightBar".format(
            claim)
        subscore = context.driver.find_element_by_css_selector(selector)
        assert value == subscore.text
    else:
        raise AssertionError('Invalid subdivision type')


@then('I verify the number of {subdivision_type} matches the number of rows in the grid')
def step_impl(context, subdivision_type):
    subtitle_number = Grid(context).get_subtitle_number(subdivision_type)
    num_of_rows = Grid(context).get_number_of_rows()
    assert_that(subtitle_number, equal_to(num_of_rows))


@then('I should see "{grade}" is not link')
def check_if_group_header_is_link(context, grade):
    header_rows = Grid(context).get_group_header_rows()
    for element in header_rows:
        if grade in element.text:
            assert not element.tag_name == 'a', "{0} is a link".format(grade)
            break
    else:
        raise AssertionError("Grade/Course : {0} not found in grid".format(grade))


@when('I scroll down the page')
def hover_alert_icon(context):
    Grid(context).scroll_down()


@when('I hover on alert icon')
def hover_alert_icon(context):
    Grid(context).hover_alert_icon()


@when('I hover over the info icon on PARCC summary row')
def step_impl(context):
    Grid(context).hover_parcc_summary_info_icon()


@then('a list of opted-in states "{message}" is displayed on the info popover')
def step_impl(context, message):
    actual_message = Grid(context).get_tooltip_message()
    assert_that(actual_message, equal_to(message))


@when('I click on "{field_pos}" link in the first grid column')
def click_first_row_link(context, field_pos):
    Grid(context).click_first_row_link(field_pos)


@then('I should see "{field_value}" on first row')
def check_comparison_row_name(context, field_value):
    Grid(context).check_comparison_row_name(field_value)


@then('I verify that the STUDENT column should {status} empty')
def step_impl(context, status):
    student_elements = Grid(context).get_student_elements()
    for elem in student_elements:
        # THAT IS THE QUESTION!!!
        if status == 'be':
            assert_that(elem.text, equal_to(""))
            assert elem.find_element_by_class_name('invisible-cell')
        elif status == 'not be':
            assert_that(len(elem.text), greater_than_or_equal_to(0))


@then('I should be able to drill down "{number_of_states}" states')
def check_clickable_states(context, number_of_states):
    num_clickable = Grid(context).get_num_drillable()
    assert_that(num_clickable, equal_to(int(number_of_states)))


@then('I should not be able to drill down "{number_of_states}" states')
def check_non_clickable_states(context, number_of_states):
    num_non_clickable = Grid(context).get_num_drillable(False)
    assert_that(num_non_clickable, equal_to(int(number_of_states)))


@then('I should not be able to drill down "{number_of_grades}" grades')
def check_non_clickable_grades(context, number_of_grades):
    num_non_clickable = Grid(context).get_num_drillable(False)
    assert_that(num_non_clickable, equal_to(int(number_of_grades)))


@then('I should see the summary row for "{rpt_type}"')
def check_summary_row(context, rpt_type):
    summaries = Grid(context).get_summary_row_names()
    assert any(rpt_type in summary for summary in summaries), "{0} not found in {1}".format(rpt_type, summaries)


@then('Verify "{exp_value}" should be the "{pos}" summary row')
def verify_value_in_summary_row(context, exp_value, pos):
    found = False
    rec = context.driver.find_elements_by_css_selector(
        ".ui-widget-content.slick-row")

    if pos == "first":
        rec = rec[0].text
    if pos == "second":
        rec = rec[1].text
    if pos == "third":
        rec = rec[2].text

    rec = rec.split('\n')
    for each in rec:
        if each == exp_value:
            found = True
            break
    assert found


@then('I should see the value "{title_data}" for "{dist_name}" on summary row')
def check_first_summary_row_name(context, title_data, dist_name):
    summaries = Grid(context).get_summary_row_names()
    summary = summaries[0].split('\n')
    first_line = summary[0]
    second_line = summary[1]
    assert_that(first_line, equal_to(title_data))
    assert_that(second_line, equal_to(dist_name))


@then('I should see value "{num_of_student}" on summary row of "STUDENT" column')
def verify_num_students(context, num_of_student):
    empty_value = False
    fields_value = Grid(context).get_first_row_of_col_num(1)[0]
    assert_that(fields_value, equal_to(num_of_student))


@then('I should see message "{message}" on tooltip')
def verify_tooltip_message(context, message):
    actual_message = Grid(context).get_tooltip_message()
    assert_that(actual_message, equal_to(message))


@then('I should not see alert icon on line "{line_num}"')
def no_alert_icon_in_row(context, line_num):
    alert_icons = Grid(context).num_alert_icons_in_row(line_num)
    assert_that(alert_icons, is_(None))


@then('I should see SUBSCORE DISTRIBUTION bar contains following data on first row')
def verify_subscore_dist_first_row(context):
    actual_val = Grid(context).get_first_row_of_col_num(2)
    for row in context.table:
        idx = int(row['name']) - 1
        assert_that(actual_val[idx], equal_to(row['value']))


@then('I should see column "{col_name}" contains "{exp_value}" value at line "{row_num}"')
def verify_value_at_row_column(context, exp_value, row_num, col_name):
    cell = Grid(context).get_value_at_row_column(row_num, col_name)
    assert_that(cell, equal_to(exp_value))


@when('I hover on "{subclaim}"')
def hover_on_subclaim(context, subclaim):
    Grid(context).hover_on_subclaim(subclaim)


@then('I should see performance distribution "{exp_value}"')
def test_performance_distribution(context, exp_value):
    expected = exp_value.split(',')
    actual_value = Grid(context).get_performance_popover(expected)
    assert_that(actual_value, equal_to(expected))


@when('I wait for earlier popover to disappear')
def step_impl(context):
    while len(context.driver.find_elements_by_css_selector('.popover-content')) == 2:
        pass


@then('I should see "{student_name}" is not a link')
def step_impl(context, student_name):
    element = Grid(context).get_student_with_text(student_name)
    assert_that(element.text, equal_to(student_name))
    assert_that(element.tag_name, not equal_to('a'))
