from behave import then, when
from hamcrest import assert_that, equal_to, contains_string
from components_tests.frontend.widgets.header import Header
from components_tests.frontend.widgets.year_selector import YearSelector


@then('I should see "{academic_year}" on header next to PARCC')
def step_impl(context, academic_year):
    actual = YearSelector(context).get_academic_year()
    assert_that(actual, equal_to(academic_year))


@then('I verified student name "{student_name}" on page header')
def test_student_name(context, student_name):
    actual_res = Header(context).get_student_name()
    assert_that(actual_res, contains_string(student_name))


@when('I click on PARCC logo')
def click_parcc(context):
    Header(context).click_parcc()


@when('I click on the "{link_name}" link in the reporting header')
def link_click(context, link_name):
    Header(context).click_link(link_name)


@then('I verified that user name "{user_name}" is on page header')
def verify_user_name(context, user_name):
    actual_result = Header(context).get_user_name()
    assert_that(actual_result, contains_string(user_name))
