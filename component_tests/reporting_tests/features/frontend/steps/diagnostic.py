from behave import when, then


@then('I verified the following section')
def test_diagnostic_views(context):
    list_of_section_elms = context.driver.find_elements_by_css_selector('.grid-wrapper')

    data_array = []
    for row in context.table:
        data_array.append(row['section'])

    for i in range(len(data_array)):
        assert data_array[i] == list_of_section_elms[i].find_element_by_tag_name(
            'h2').text, "value should match\nExpected: {0}\nActual: {1}".format(data_array[i], list_of_section_elms[i].find_element_by_tag_name('h2').text)


@then('I verified "{section}" section contains following columns')
def step_impl(context, section):
    list_of_view_elms = context.driver.find_elements_by_css_selector('[data-component-grid-container]>h2')
    column_list = context.driver.find_elements_by_css_selector('.slick-header-columns .slick-column-name')
    column_list_array = []
    for each in column_list:
        column_list_array.append(each.text)
    for i in range(len(list_of_view_elms)):
        if section == list_of_view_elms[i].text:
            for row in context.table:
                assert row['column_name'] in column_list_array
            break
