from behave import when, then
from hamcrest import assert_that, equal_to, has_length, has_item, is_not
import csv
import re
import os
import glob
from datetime import datetime
from components_tests.frontend.widgets.breadcrumb import Breadcrumb
from components_tests.frontend.widgets.info_bar import InfoBar, GradeCourse, ResultSelector, SubjectSelector
from components_tests.frontend.common import DOWNLOAD_DIR
from components_tests.frontend.support import wait_for_condition
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from components_tests.frontend.widgets.grid import Grid


@when('I click on the "{view}" view')
def select_a_view(context, view):
    InfoBar(context).click_view(view)


@when('I click on the "{view_selection}" view on the cpop {report_type} report')
def step_impl(context, view_selection, report_type):
    if report_type in ['district', 'state']:
        if view_selection in ["Performance", "Growth"]:
            button_class_name = view_selection.lower()
        else:
            raise AssertionError(
                'Invalid view selection name passed in the step.')
        context.driver.find_element_by_class_name(
            "view-selector").find_element_by_class_name(button_class_name).click()
    else:
        raise AssertionError('Unsupported comparing populations report type')


@when('I click on the "{view_selection}" view on the student roster report')
def step_impl(context, view_selection):
    if view_selection == "Scores":
        button_class_name = "scores"
    elif view_selection == "Item Analysis":
        button_class_name = "items"
    else:
        raise AssertionError('Invalid view selection name passed in the step.')
    context.driver.find_element_by_class_name(
        "view-selector").find_element_by_class_name(button_class_name).click()


@then("I verify that I'm on the performance page")
def step_impl(context):
    headers = context.driver.find_elements_by_class_name("slick-column-name")
    found = False
    for header in headers:
        if "PERFORMANCE DISTRIBUTION" in header.text:
            found = True
    assert found, "Performance Distribution column not found"


@when('I wait for the "{view_selection}" view to be loaded')
def step_impl(context, view_selection):
    if view_selection == 'Growth':
        element_id = "growthChart"
    else:
        element_id = "gridTable"
    try:
        WebDriverWait(context.driver, 10).until(
            expected_conditions.visibility_of_element_located((By.ID, element_id)))
    except:
        raise AssertionError('Error in loading the growth view.')


@then('I should see the breadcrumbs "{breadcrumbs_list}"')
def test_breadcrumbs(context, breadcrumbs_list):
    expected_breadcrumbs = breadcrumbs_list.split(",")
    actual_breadcrumbs = []
    breadcrumbs_links = context.driver.find_elements_by_css_selector(
        "#breadcrumb a")
    for each in breadcrumbs_links:
        actual_breadcrumbs.append(str(each.text))
    assert expected_breadcrumbs == actual_breadcrumbs, "Breadcrumbs: Expected: {0}; Actual {1}".format(
        expected_breadcrumbs, actual_breadcrumbs)


@then('I should see the following breadcrumbs')
def test_breadcrumbs(context):
    Breadcrumb(context).test_breadcrumbs(context.table.rows)


# TODO: should use the same step to check report title
@then('I should see the report title "{title}"')
def test_report_title(context, title):
    InfoBar(context).test_title(title)


@then('I should see {gradeCourse} as the report title')
def test_report_title(context, gradeCourse):
    InfoBar(context).test_title(gradeCourse)


@then('I should see the subtitle "{subtitle}"')
def test_report_subtitle(context, subtitle):
    InfoBar(context).test_subtitle(subtitle)


@then('I should see the compare bar')
def test_compare_bar_presence(context):
    compare_bar = InfoBar(context).get_compare_bar()
    assert compare_bar.is_displayed(), 'compare bar should display'


@then('I should not see the compare bar')
def test_compare_bar_absence(context):
    compare_bar = InfoBar(context).get_compare_bar()
    assert not compare_bar.is_displayed(), 'compare bar should be hidden'


@when('I click on the download link')
def click_download_link(context):
    InfoBar(context).click_download()


@when('I click on the download csv link')
def click_download_csv_link(context):
    InfoBar(context).click_download_csv()


@then('The filename matches the format for the "{report_type}" report')
def step_impl(context, report_type):
    timestamp_fmt = "\d{1,2}-\d{1,2}-\d{4}_\d{1,2}-\d{1,2}-\d{1,2}"
    report_types = {
        'school': 'school_report_{0}\.csv',
        'cpop parcc performance': 'parcc_performance_report_{0}\.csv',
        'cpop state performance': 'state_performance_report_{0}\.csv',
        'cpop district performance': 'district_performance_report_{0}\.csv',
        'student roster scores': 'student_roster_scores_report_{0}\.csv',
        'student roster item analysis': 'student_roster_item_analysis_report_{0}\.csv',
        'roster diagnostic ELA': 'diagnostic_ELA_{0}\.csv',
        'roster diagnostic math': 'diagnostic_Math_{0}\.csv',
    }
    report_types.update((k, v.format(timestamp_fmt)) for k, v in report_types.items())
    if report_type not in report_types:
        assert False, "invalid report type specified: {0}".format(report_type)
    newest_file = get_newest_file("csv")
    dir, name = os.path.split(newest_file)
    pattern = report_types[report_type]
    if re.match(pattern, name):
        assert True
    else:
        assert False, "CSV filename does not match report format: {0}".format(newest_file)


@then('I see the following CSV header information')
def step_impl(context):
    header_row_names = [r['key'] for r in context.table]
    table = {r['key']: r['value'] for r in context.table}
    fname = get_newest_file("csv")
    with open(fname, 'r') as f:
        r = csv.reader(f, quotechar='"')
        for idx, line in enumerate(r):
            if idx == len(table):
                break
            # check that all the headers match
            if idx == 1:
                # check date is in the proper format
                try:
                    dt = datetime.strptime(line[1], table[header_row_names[idx]])
                except ValueError:
                    assert False, "date {0} does not match format '{1}'".format(line[1], table[header_row_names[idx]])
                continue
            assert header_row_names[idx] == line[
                0], "expected {0} got {1}".format(header_row_names[idx], line[0])
            # check that the values for each header match
            assert table[header_row_names[idx]] == line[
                1], "expected {0} got {1}".format(table[header_row_names[idx]], line[1])


@then('I see the following CSV main information')
def step_impl(context):
    demo_state_message = "This is a Demo state. All data is mocked and doesn't reflect progress of any real student."
    num_empty_lines = 1  # spaces in between header and body
    num_header_lines = 11  # header rows
    main_row_names = context.table.headings
    table_rows = [[row[col] for col in main_row_names]
                  for row in context.table]
    fname = get_newest_file("csv")
    main_rows_start = num_header_lines + num_empty_lines
    with open(fname, 'r') as f:
        r = csv.reader(f, quotechar='"')
        for idx, line in enumerate(r):
            if idx < main_rows_start:
                # skip header rows and blank rows
                continue
            if idx == main_rows_start:
                # check header row
                assert_that(main_row_names, equal_to(line), "Header doesn't match")
                continue
            if idx > main_rows_start + len(table_rows):
                # check the csv footer
                if line == [" "]:
                    # skip empty line after the table
                    continue
                # check demo state message
                assert_that([demo_state_message], equal_to(line), 'Text of demo state message is not valid')
                break
            assert_that(table_rows[idx - (main_rows_start + 1)], equal_to(line), "Line data doesn't match")


def get_newest_file(ext):
    def is_file_generating():
        files = glob.glob('*.{0}'.format(ext))
        return False if files else True

    current = os.getcwd()
    os.chdir(DOWNLOAD_DIR)
    wait_for_condition(is_file_generating)
    newest = max(glob.iglob('*.{0}'.format(ext)), key=os.path.getctime)
    os.chdir(current)
    return os.path.join(DOWNLOAD_DIR, newest)


@when("I check on DISTRICT checkbox")
def step_impl(context):
    context.driver.find_element_by_css_selector(
        'input[name="compare_district"]').click()


@when('I un check on {name} checkbox')
def step_impl(context, name):
    InfoBar(context).check_compare_checkboxes(name)


@when('I check on {name} checkbox')
def step_impl(context, name):
    InfoBar(context).check_compare_checkboxes(name)


@when('I click on "{link_value}" link on breadcrumb')
def step_impl(context, link_value):
    bread_crumb_links = context.driver.find_elements_by_css_selector('#breadcrumb .breadcrumb>li>a')
    for each in bread_crumb_links:
        if each.text == link_value:
            each.click()
            break


@then('I validate total number of assessments should be {num_of_asmnt}')
def test_number_of_assessments(context, num_of_asmnt):
    expected_subtitle = "%s assessments" % num_of_asmnt
    InfoBar(context).test_subtitle(expected_subtitle)


@then('I validate total number of buttons should be {num_of_btn}')
def test_number_of_diagnostic_views(context, num_of_btn):
    views = InfoBar(context).get_diagnostic_views()
    assert_that(views, has_length(int(num_of_btn)))


@then('I validate label for each button should be following')
def test_diagnostic_views_labels(context):
    expected = [row['btn_label'] for row in context.table]
    views = InfoBar(context).get_diagnostic_views()
    assert_that(views, equal_to(expected))


@when('I click on "{view}" web button')
def click_a_diagnostic_view(context, view):
    InfoBar(context).click_diagnostic_view(view)


@when('I hover over the "{view_name}" view selector')
def step_impl(context, view_name):
    InfoBar(context).hover_diagnostic_view(view_name)


@then('I verify that the district, state checkboxes exist')
def test_compare_checkbox(context):
    labels = InfoBar(context).get_compare_checkboxes()
    assert_that(labels, has_item('DISTRICT'))
    assert_that(labels, has_item('STATE'))


@then('I verify that district is checked')
def test_compare_checkbox_selected(context):
    labels = InfoBar(context).get_checked_compare_checkboxes()
    assert_that(labels, has_item('DISTRICT'))
    assert_that(labels, is_not(has_item('STATE')))


@then('I verify that the checkbox to compare PARCC averages is disabled')
def test_disabled_compare_checkbox(context):
    labels = InfoBar(context).get_disabled_compare_checkboxes()
    assert_that(labels, has_item('PARCC'))


@then('I see an alert popover when I hover over the compare PARCC checkbox')
def test_hover_disabled_compare_checkbox(context):
    InfoBar(context).hover_disabled_parcc_icon()


@then('I should see opt out message "{message}" on alert popover')
def verify_optout_alert_popover_message(context, message):
    actual_message = InfoBar(context).get_alert_popover_message(message)
    assert_that(actual_message, equal_to(message))


@then('I validate "{message}" as the report title')
def test_report_title(context, message):
    InfoBar(context).test_title(message)


@then('I should see "view_button" on page is present')
def step_impl(context):
    list_of_elem = context.driver.find_elements_by_css_selector('.selectors .view-selector .btn-group')
    assert len(list_of_elem) == 1, "\nExpected Result: {0}\nActual Result: {1}".format(1, len(list_of_elem))


@when('I select "{grade_level}" from GRADE/COURSE dropdown')
def click_option_in_grade_course_dropdown(context, grade_level):
    GradeCourse(context).select_dropdown_option(grade_level)
    Grid(context).wait_while_reloading()


@then('I should see "{grade_level}" on GRADE/COURSE dropdown')
def step_impl(context, grade_level):
    expected_result = grade_level
    actual_result = context.driver.find_element_by_css_selector(
        "div#gradeCourseSelector .btn .selected-label").text
    assert expected_result == actual_result, "value should match\nExpected: {0}\nActual: {1}".format(
        expected_result, actual_result)


@when('I click on GRADE/COURSE dropdown')
def click_grade_course_dropdown(context):
    GradeCourse(context).click_dropdown()


@then('I should see the "{expected_headers}" {selector_type} in the RESULT dropdown')
def step_impl(context, expected_headers, selector_type):
    if selector_type == 'level one headers':
        selector_class = '.h1'
    elif selector_type == 'subheaders':
        selector_class = '.h2'
    elif selector_type == 'selectable elements':
        selector_class = ':not(.h1):not(.h2)'
    else:
        raise AssertionError("Invalid selector_type")

    header_list = context.driver.find_elements_by_css_selector(
        "#resultSelector li{selector_class}".format(selector_class=selector_class))
    header_list = [header.text for header in header_list]
    expected_hlist = expected_headers.split(',')
    for header in expected_hlist:
        assert header in header_list, "{header} not found".format(
            header=header)


@then('I click on the print button and choose "{mode}" pdf')
def check_print_button(context, mode):
    InfoBar(context).click_print_button(mode)


@then('I should see list of grade under GRADE/COURSE dropdown')
def test_grade_course_dropdown(context):
    actual = GradeCourse(context).get_dropdown_items()
    expected = [row['value'] for row in context.table]
    assert_that(actual, equal_to(expected))


@then('I should see "{result_type}" on the results dropdown')
def step_impl(context, result_type):
    actual_value = context.driver.find_element_by_css_selector("#resultSelector .selected-label").text
    assert result_type == actual_value, "value should match\nExpected: {0}\nActual: {1}".format(result_type, actual_value)


@when('I click on the results dropdown')
def click_results_dropdown(context):
    ResultSelector(context).click_dropdown()


@when('I select "{result}" from the results dropdown')
def click_option_in_results_selector(context, result):
    ResultSelector(context).click_dropdown()
    ResultSelector(context).select_dropdown_option(result)
    Grid(context).wait_while_reloading()


@when('I select the following options from the initial subject and grade/course selection page')
def step_impl(context):
    for r in context.table:
        if r['param'] == "gradeCourse":
            grade = r['value']
        elif r['param'] == 'subject':
            subject = r['value']
    print(grade, subject)
    SubjectSelector(context).click_dropdown()
    SubjectSelector(context).click_subject(subject)
    GradeCourse(context).click_dropdown()
    GradeCourse(context).select_dropdown_option(grade)
    Grid(context).wait_while_reloading()
