from behave import when, then
from components_tests.frontend.widgets.grid import Grid
from components_tests.frontend.widgets.growth_chart import GrowthChart
from hamcrest import assert_that, equal_to, greater_than, has_key, contains, is_, not_none, contains_inanyorder, has_items
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC


@then('I see "{exp_num_rows}" rows in the school list grid')
def step_impl(context, exp_num_rows):
    actual_num_rows = Grid(context).get_number_of_rows()
    assert_that(actual_num_rows, equal_to(int(exp_num_rows)))


@then('the growth chart title is "{expected_title}"')
def step_impl(context, expected_title):
    actual_title = GrowthChart(context).get_title()
    assert_that(actual_title, equal_to(expected_title))


@then('the overall summary bar above growth chart contains the following values')
def step_impl(context):
    summary_bar_info = GrowthChart(context).get_summary_bar_info()
    for row in context.table:
        name = row['name']
        value = row['value']
        assert_that(summary_bar_info, has_key(name))
        assert_that(value, equal_to(summary_bar_info[name]))


@then('the overall summary bar above growth chart contains gray bar which hides the avg and distribution')
def step_impl(context):
    gray_bar = GrowthChart(context).get_gray_bar()
    assert_that(gray_bar, is_(not_none()))


@then('the growth chart "{axis}" axis label is "{exp_label}"')
def step_impl(context, axis, exp_label):
    actual_label = GrowthChart(context).get_axis_label(axis)
    assert_that(actual_label, equal_to(exp_label))


@then('the growth chart "{axis}" axis scale cutpoints are "{cutpoints}"')
def step_impl(context, axis, cutpoints):
    actual = GrowthChart(context).get_axis_cutpoints(axis)
    expected = cutpoints.split(',')
    assert_that(sorted(actual), equal_to(sorted(expected)))


@then('a static text is displayed below the growth chart')
def step_impl(context):
    actual_text = GrowthChart(context).get_text_below_chart()
    expected_text = "Circles represents schools. Circle size represents the number of students tested."
    assert_that(actual_text, equal_to(expected_text))


@when('I select the growth chart comparison option as "{selection}"')
def step_impl(context, selection):
    GrowthChart(context).click_radio_button(selection)


@then('I see "{exp_num_circles}" circles in the bubble chart')
def step_impl(context, exp_num_circles):
    act_num_circles = GrowthChart(context).get_number_of_circles()
    assert_that(act_num_circles, equal_to(int(exp_num_circles)))


@then('a circle is positioned at "{x_position}"')
def step_impl(context, x_position):
    found = GrowthChart(context).is_circle_at_x_position(x_position)
    assert found, "Circle with expected position cx = {0} not found".format(position)


@when('I mouseover on the bubble positioned at "{x}" "{y}"')
def step_impl(context, x, y):
    GrowthChart(context).mouseover_bubble_at_position(x, y)


@when('I mouseover the grid at row number "{position}"')
def step_impl(context, position):
    GrowthChart(context).mouseover_grid_at_position(position)


@then('the popover is displayed with the following information')
def step_impl(context):
    expected = {row['name']: row['value'] for row in context.table}
    grid = GrowthChart(context)
    title = grid.get_popover_title()
    assert_that(title, equal_to(expected['School']))
    subtitle = grid.get_popover_subtitle()
    assert_that(subtitle, equal_to(expected['Subtitle']))
    percentiles = grid.get_popover_content()
    print(percentiles)
    for name, value in percentiles.items():
        assert_that(value, equal_to(expected[name]))


@then('the active bubble is in front with school name "{school_name}"')
def test_active_bubble(context, school_name):
    # in front => (is the LAST element in the parentNode)
    chart = GrowthChart(context)
    idx, num_bubbs = chart.get_active_bubble_idx(school_name)
    assert_that(idx, is_(num_bubbs - 1))
    actual = chart.get_popover_title(school_name)
    assert_that(actual, equal_to(school_name))


@then('the school list grid has "{col_headers}" columns')
def step_impl(context, col_headers):
    exp_col_header_list = col_headers.split(',')
    actual_header_columns = GrowthChart(context).get_header_columns()
    actual_column_names = [col.text for col in actual_header_columns]

    assert_that(len(actual_header_columns), is_(len(exp_col_header_list)))
    for col in exp_col_header_list:
        if "checkbox" in col:
            chk = actual_header_columns[0].find_element_by_class_name("headerCheckbox")
            assert_that(chk, is_(not_none()))
        elif col not in actual_column_names:
            raise AssertionError('{0} column header not found').format(col)


@when('I "{action}" the "{sch_name}" school from the school grid')
def step_impl(context, action, sch_name):
    GrowthChart(context).click_checkbox_by_school_name(sch_name)


@when('there are "{num_checked}" rows checked on the school grid')
def step_impl(context, num_checked):
    num_checked_rows = GrowthChart(context).get_number_of_checked_rows()
    assert_that(num_checked_rows, is_(equal_to(int(num_checked))))


@then('I should see "{message}" value at field "{field_x}" on line "{field_y}"')
def step_impl(context, message, field_x, field_y):
    actual = Grid(context).get_value_at_x_y(field_x, field_y)
    assert_that(actual, is_(equal_to(message)))


@then('I should see the value "{value}" in the first "{col_name}" row in student list grid')
def step_impl(context, value, col_name):
    actual_value = GrowthChart(context).get_first_row_col_value(col_name)
    assert_that(actual_value, is_(equal_to(value)))


@then("I verify that there are radio buttons for toggle between parcc and state")
def step_impl(context):
    expected = {"parcc", "state"}
    actual_radios = GrowthChart(context).get_comparison_sources()
    assert_that(actual_radios, is_(equal_to(expected)))


@then("I verify that there is a {line_name} line")
def step_impl(context, line_name):
    line_presence = GrowthChart(context).is_line_present(line_name)
    assert_that(line_presence, is_(not_none()))


@then("I wait for the popover to appear")
def wait_for_bubbs(context, timeout=30):
    WebDriverWait(context.driver, timeout).until(
        EC.visibility_of_element_located((By.CLASS_NAME, 'growth-percentile')))
