from behave import given, when, then
from hamcrest import assert_that, equal_to, is_not, contains_string, is_
from selenium.webdriver.common.action_chains import ActionChains
from components_tests.frontend.common import WindowManager
from components_tests.frontend.support import WebDriverHelper, get_url
from components_tests.frontend.support import open_report
from components_tests.frontend.constants import Reports
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from reporting_tests.features.frontend.environment import close_alert_if_exist, already_logged_in
import re
import os
import time

__author__ = 'nparoha'


@given('an open browser')
def open_browser(context):
    pass


@when('I open the {report_name} report with the following parameters')
def open_a_report(context, report_name):
    if report_name == 'individual student':
        open_report(context, Reports.ISR)
    elif report_name == 'student roster':
        open_report(context, Reports.STUDENT_ROSTER)
    elif report_name == 'school':
        open_report(context, Reports.SCHOOL_REPORT)
    elif report_name == 'cpop':
        open_report(context, Reports.CPOP)


@when("I open the cpop report with the {tenant} tenant's state code and the following additional parameters")
def step_impl(context, tenant):
    tenant_to_state_code = {
        'first': context.first_tenant_state_code,
        'second': context.second_tenant_state_code
    }
    state_code = tenant_to_state_code[tenant]
    steps = ('When I open the cpop report with the following parameters' + os.linesep +
             '  | param     | value |' + os.linesep +
             '  | stateCode | {0}   |'.format(state_code))
    for row in context.table:
        steps += os.linesep + '  | {0} | {1} |'.format(row['param'], row['value'])
    context.execute_steps(steps)


@then('I am redirected to the login page')
def redirect_to_login_page(context):
    if already_logged_in(context):
        return
    try:
        driver = WebDriverHelper(context)
        close_alert_if_exist(context)
        driver.wait_for_id("IDToken1")
    except:
        raise AssertionError('Error in redirecting back to the login page.')


@when('I am redirected to the {report_name} report')
def wait_for_redirecting(context, report_name):
    WebDriverHelper(context).wait_for_css('#subjectSelector')
    assert_that(context.driver.current_url, is_not(contains_string("error")))


@then('I validate that I am on "{school_name}" school page')
def step_impl(context, school_name):
    assert school_name == context.driver.find_element_by_css_selector('#infoBar .reportTitle').text


@when('I open {webpage} webpage')
def go_to_url(context, webpage):
    context.driver.get(webpage)


@then('I should see no data message')
def no_data_message(context):
    container = WebDriverHelper(context).by_id('infoBar')
    assert_that(container.text, contains_string('No report data available for current selections.'))


@when('the assessment tab is displayed')
def step_impl(context):
    context.driver.find_element_by_id("assessment")


@when('I wait for {time_wait} seconds')
def wait_for(context, time_wait):
    time.sleep(int(time_wait))


@when('I login as "{username}" with password "{password}"')
def setup_impl(context, username, password):
    if already_logged_in(context):
        return
    driver = WebDriverHelper(context)
    close_alert_if_exist(context)

    driver.by_id("IDToken1").send_keys(username)
    driver.by_id("IDToken2").send_keys(password)
    driver.by_name("Login.Submit").click()


@when('I login as {username}')
def login_as(context, username):
    password = username + '1234'
    context.execute_steps('When I login as "{0}" with password "{1}"'.format(username, password))


@when('I click on header grid column number {number}')
def step_impl(context, number):
    n = int(number) - 1
    cols_selector = '.slick-header-column'
    context.driver.find_elements_by_css_selector(cols_selector)[n].click()


@then('I verify subscore distribution column adds up to 100%')
def step_impl(context):
    sum_of_bar_vals = 0
    first_row = context.driver.find_element_by_id("gridTable").find_element_by_class_name(
        "grid-canvas").find_element_by_class_name("slick-row")
    for i in range(1, 4):
        distribution_bar_element_var = ".progress .level-{0} .percentageOnBar".format(
            i)
        try:
            curr_level_divdata = context.driver.find_element_by_css_selector(
                distribution_bar_element_var)
        except:
            # No level for "i"
            curr_level_divdata = None
            pass

        if curr_level_divdata and len(curr_level_divdata.text) > 0:
            curr_level_val = int(curr_level_divdata.text.replace("%", ""))
        else:
            curr_level_val = 0

        sum_of_bar_vals += curr_level_val

    assert sum_of_bar_vals == 100, 'Total Sum for Distribution Bar does not equal 100 percent.\nActual: {0}'.format(
        sum_of_bar_vals)


# TODO: All this does is count the number of headers.... doesn't actually check for the specific text you write...
@then('I should see "{col_headers}" columns {grid_type}')
def step_impl(context, col_headers, grid_type):
    # Could be expanded to map if we have more formatted headers
    num_formatted_headers = 0

    # elements =
    # context.driver.find_element_by_css_selector("div#gridTable").find_elements_by_class_name("slick-column-name")
    elements = context.driver.find_elements_by_css_selector(
        "div#gridHeader span.slick-column-name")

    col_header_list = col_headers.split(',')
    # accounting for avg subscore column in performance view
    # assert len(col_header_list) + num_formatted_headers == len(elements), "Invalid number of columns found in the grid"
    assert len(col_header_list) + num_formatted_headers == len(
        elements), "Invalid number of columns available in the School List grid" + str(elements)
    for element in elements:
        if element.text not in col_header_list and ("READING" not in element.text or 'ela overall' not in grid_type):
            raise AssertionError('Grid column headers did not match')


@then('the selected result in the results dropdown should read "{selected}"')
def selected_result(context, selected):
    elem = context.driver.find_element_by_css_selector(
        "#resultSelector span.selected-label")
    assert elem.text == selected, "expected `{0}`, got `{1}`".format(
        selected, elem.text)


@then('I see the grade/course dropdown')
def step_impl(context):
    dropdown = context.driver.find_elements_by_css_selector(
        "div#gradeCourseSelector button.btn.dropdown-toggle")
    assert len(dropdown) == 1, "grade/course dropdown not found"


@when('I select "{grade}" from the grade/course dropdown')
def step_impl(context, grade):
    context.driver.find_element_by_css_selector(
        "div#gradeCourseSelector button.btn.dropdown-toggle").click()

    grade_elements = context.driver.find_elements_by_css_selector(
        "#gradeCourseSelector  li a")
    found = False

    for index, value in enumerate(grade_elements):
        if value.text == grade:
            grade_elements[index].click()
            found = True
            break

    assert found, "Element not found"


@then('I verify that the grade or course selected is "{gradeCourse}"')
def check_selected_grade_course(context, gradeCourse):
    grade_course_selector = WebDriverHelper(context).by_css("div#gradeCourseSelector span.selected-label")
    selected = grade_course_selector.text.strip()
    assert_that(selected, equal_to(gradeCourse))


@then('I {should_or_not} see "{subject}" subscores in the results list')
def step_impl(context, should_or_not, subject):
    # Check for valid input
    if subject not in ["ELA", "MATH"]:
        assert False, "wrong subject '{0}' passed".format(subject)

    # Check for valid input
    if should_or_not not in ["should", "should not"]:
        assert False, "wrong value should_or_not '{0}'".format(should_or_not)

    # Which subject to look for in results list
    if subject == "ELA":
        count = 2  # ELA has two subheadings: reading and writing
    else:
        count = 1  # Math has one subheading: sub-scores

    if should_or_not == "should not":
        count = 0  # No subheadings

    result_selector_inner_html = context.driver.find_element_by_id('resultSelector').get_attribute('innerHTML')
    records = re.findall(r'class=\"(.+?sub-header.+?)\"', result_selector_inner_html)
    # There's a message under 'Diagnostic Assessments' that has the sub-header class, but it's not a subheader
    # in the same sense. So the following line filters out the class 'diagnostic'
    records = list(filter(lambda r: 'diagnostic-result-info' not in r, records))
    assert count == len(records), "value should match\nExpected: {0}\nActual: {1}.".format(count, len(records))


@then('I {should_or_not} see "{message}" in the results list')
def diagnostic_message_result_selector(context, should_or_not, message):
    if should_or_not == 'should':
        count = 1
    elif should_or_not == 'should not':
        count = 0
    else:
        raise AssertionError("improper value passed for should_or_not")
    wdh = WebDriverHelper(context)
    result_selector = wdh.by_id('resultSelector')
    result_selector_inner_html = result_selector.get_attribute('innerHTML')
    records = re.findall(r'class=\"(.+?diagnostic-result-info.+?)\"', result_selector_inner_html)
    assert_that(len(records), is_(equal_to(count)))
    if count == 1:
        assert_that(result_selector_inner_html, contains_string(message))


@then('I verify the {legend_bar_type} legend bar has the correct number of items')
def step_impl(context, legend_bar_type):
    if legend_bar_type == 'subscore':
        num_elements = 3
        class_name = 'subclaimLevels'
    else:
        num_elements = 5
        class_name = 'performanceLevels'

    first_list_element = context.driver.find_element_by_css_selector(
        "#footer .legend .container li:first-child")
    assert first_list_element.text == 'Legend:', "Legend bar not found. Looking for first elemnt of bar, found: {first}".format(
        first_list_element)

    legend_list = context.driver.find_elements_by_css_selector(
        "#footer .legend .container ul.{class_name} li.level".format(class_name=class_name))
    assert len(
        legend_list) == num_elements, "Legend had {number} elements for specified type {type}. Expected {expected}".format(
        type=legend_bar_type, number=len(legend_list), expected=num_elements)


@then('I see the legend with description for the five ALD levels on the performance bar')
def step_impl(context):
    assert context.driver.find_element_by_id(
        "footer").find_element_by_class_name("show"), "Legend bar is hidden."
    legend_bar = context.driver.find_element_by_class_name("legend")

    all_li = legend_bar.find_elements_by_tag_name("li")
    assert "Legend:" == str(all_li[0].text), "Legend: label not found on the legend bar."

    all_perf_levels = legend_bar.find_elements_by_class_name("level")
    assert 5 == len(all_perf_levels), "5 ALD performance levels not found on the legend."

    perf_level1_text = str(all_perf_levels[0].text).split('\n')
    assert "1" == perf_level1_text[0], "ALD level 1 image not found on the legend bar."
    assert "Did not yet meet" == str(all_perf_levels[0].find_element_by_class_name("legend-title-1").text)
    assert "expectations" == str(all_perf_levels[0].find_element_by_class_name("legend-title-2").text)

    perf_level2_text = str(all_perf_levels[1].text).split('\n')
    assert "2" == perf_level2_text[0], "ALD level 2 image not found on the legend bar."
    assert "Partially met" == perf_level2_text[1]
    assert "expectations" == perf_level2_text[2]

    perf_level3_text = str(all_perf_levels[2].text).split('\n')
    assert "3" == perf_level3_text[0], "ALD level 3 image not found on the legend bar."
    assert "Approached" == perf_level3_text[1]
    assert "expectations" == perf_level3_text[2]

    perf_level4_text = str(all_perf_levels[3].text).split('\n')
    assert "4" == perf_level4_text[0], "ALD level 4 image not found on the legend bar."
    assert "Met" == perf_level4_text[1]
    assert "expectations" == perf_level4_text[2]

    perf_level5_text = str(all_perf_levels[4].text).split('\n')
    assert "5" == perf_level5_text[0], "ALD level 5 image not found on the legend bar."
    assert "Exceeded" == str(all_perf_levels[4].find_element_by_class_name("legend-title-1").text)
    assert "expectations" == str(all_perf_levels[0].find_element_by_class_name("legend-title-2").text)

    # TODO: DISABLING THESE STEPS SINCE THEY ARE CONSISTENTLY FAILING ON JENKINS ###
'''
   assert legend_bar.find_element_by_class_name("hide"), "Hide button not found on the legend bar"
   legend_bar.find_element_by_class_name("hide").click()
   assert context.driver.find_element_by_id("footer").find_element_by_class_name("collapsed"), "Legend bar is not collapsed."
   context.driver.find_element_by_id("footer").find_element_by_class_name("show").find_element_by_class_name("unhide").click()
'''


@then('I verify subscore distribution column adds up to 100% for the first row')
def step_impl(context):
    sum_of_bar_vals = 0
    first_row = context.driver.find_element_by_id("gridTable").find_element_by_class_name(
        "grid-canvas").find_element_by_class_name("slick-row")
    prog_bar_divs = first_row.find_element_by_class_name(
        "progress").find_elements_by_class_name("claims")
    for each in prog_bar_divs:
        curr_level_val = int(
            each.find_element_by_class_name("percentageOnBar").text.replace("%", ""))
        sum_of_bar_vals += curr_level_val
    assert sum_of_bar_vals == 100, 'Total Sum for Distribution Bar does not equal 100 percent.\nActual: {0}'.format(
        sum_of_bar_vals)


@when('I navigate to the new opened window')
def go_to_new_window(context):
    WindowManager(context).go_to_new_window()


@then("I close the new opened window")
def close_new_window(context):
    WindowManager(context).close_new_window()


@then('I should see the overall performance legend bar with the following cutpoints')
def step_impl(context):
    expected_cutpoints = [entry['cutpoint'] for entry in context.table]
    assert len(expected_cutpoints) == 5, 'Expected 5 cutpoints'

    legend_bar = context.driver.find_element_by_class_name('legend')

    all_li = legend_bar.find_elements_by_tag_name('li')
    assert 'Legend:' == str(
        all_li[0].text), 'Legend: label not found on the legend bar.'

    all_perf_levels = legend_bar.find_elements_by_class_name(
        'level')
    assert 5 == len(
        all_perf_levels), '5 performance levels not found on the legend.'

    perf_level1_text = str(all_perf_levels[0].text).splitlines()
    assert '1' == perf_level1_text[
        0], 'Level 1 image not found on the legend bar.'
    assert 'MINIMAL' == perf_level1_text[
        1], 'Expected "MINIMAL", Actual "{0}"'.format(perf_level1_text[1])
    expected_string = 'UNDERSTANDING ({0})'.format(expected_cutpoints[0])
    assert expected_string == perf_level1_text[
        2], 'Expected "{0}", Actual "{1}"'.format(expected_string, perf_level1_text[2])

    perf_level2_text = str(all_perf_levels[1].text).splitlines()
    assert '2' == perf_level2_text[
        0], 'Level 2 image not found on the legend bar.'
    assert 'PARTIAL' == perf_level2_text[
        1], 'Expected "PARTIAL", Actual "{0}"'.format(perf_level2_text[1])
    expected_string = 'UNDERSTANDING ({0})'.format(expected_cutpoints[1])
    assert expected_string == perf_level2_text[
        2], 'Expected "{0}", Actual "{1}"'.format(expected_string, perf_level2_text[2])

    perf_level3_text = str(all_perf_levels[2].text).splitlines()
    assert '3' == perf_level3_text[
        0], 'Level 3 image not found on the legend bar.'
    assert 'ADEQUATE' == perf_level3_text[
        1], 'Expected "ADEQUATE", Actual "{0}"'.format(perf_level3_text[1])
    expected_string = 'UNDERSTANDING ({0})'.format(expected_cutpoints[2])
    assert expected_string == perf_level3_text[
        2], 'Expected "{0}", Actual "{1}"'.format(expected_string, perf_level3_text[2])

    perf_level4_text = str(all_perf_levels[3].text).splitlines()
    assert '4' == perf_level4_text[
        0], 'Level 4 image not found on the legend bar.'
    assert 'STRONG' == perf_level4_text[
        1], 'Expected "STRONG", Actual "{0}"'.format(perf_level4_text[1])
    expected_string = 'UNDERSTANDING ({0})'.format(expected_cutpoints[3])
    assert expected_string == perf_level4_text[
        2], 'Expected "{0}", Actual "{1}"'.format(expected_string, perf_level4_text[2])

    perf_level5_text = str(all_perf_levels[4].text).splitlines()
    assert '5' == perf_level5_text[
        0], 'Level 5 image not found on the legend bar.'
    assert 'DISTINGUISHED' == perf_level5_text[
        1], 'Expected "DISTINGUISHED", Actual "{0}"'.format(perf_level5_text[1])
    expected_string = 'UNDERSTANDING ({0})'.format(expected_cutpoints[4])
    assert expected_string == perf_level5_text[
        2], 'Expected "{0}", Actual "{1}"'.format(expected_string, perf_level5_text[2])


@then('I should see "{message}" message for "{grade}" on "{exp_value}" row')
def step_impl(context, message, grade, exp_value):
    actual_rows_from_app = context.driver.find_elements_by_css_selector(
        "#gridTable .grid-canvas .slick-cell")
    for index in range(len(actual_rows_from_app)):
        if grade == actual_rows_from_app[index].text:
            assert exp_value == actual_rows_from_app[
                index + 1].text, "{0} not found. \nExpected: {0}\nActual: {1}".format(exp_value, actual_rows_from_app[index + 1].text)
            assert message == actual_rows_from_app[
                index + 2].text, "{0} not found. \nExpected: {0}\nActual: {1}".format(message, actual_rows_from_app[index + 2].text)
            return
    raise AssertionError("Grade {grade} not found".format(grade=grade))


@then('I verify that the download popup appears')
def step_impl(context):
    popover_text = context.driver.find_element_by_css_selector(
        '.download-text').text
    assert popover_text == "Download the current view as a spreadsheet:"


@then('I mouseover the download popup')
def step_impl(context):
    popover = context.driver.find_element_by_css_selector('.download-text')
    ActionChains(context.driver).move_to_element(popover).perform()


@when('I mouseout of the download popup')
def step_impl(context):
    header = context.driver.find_element_by_id('header')
    ActionChains(context.driver).move_to_element(header).perform()


@then('I verify that the download popup does not appear')
def step_impl(context):
    try:
        WebDriverWait(context.driver, 20).until(
            expected_conditions.invisibility_of_element_located((By.CSS_SELECTOR, ".download-text")))
    except:
        raise AssertionError('Error in popup disappearing.')


@then('I log out')
def step_impl(context):
    context.driver.get(get_url(context) + '/logout')


@when('I verify that I am on the subject and grade/course initial selection page')
def step_impl(context):
    driver = WebDriverHelper(context)
    driver.wait_for_id("subjectSelector")


@then('I should see GRADE/COURSE button is enabled')
def step_impl(context):
    btn_disable = context.driver.find_element_by_css_selector('.btn-group .dropdown-toggle:not([disabled])')
    if len(btn_disable.text) > 0:
        assert True
    else:
        raise AssertionError('button is disabled')
