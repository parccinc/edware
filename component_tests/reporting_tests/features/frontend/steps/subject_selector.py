from behave import when, then
from hamcrest import assert_that, equal_to, has_length
from components_tests.frontend.support import WebDriverHelper
from components_tests.frontend.widgets.info_bar import InfoBar
from components_tests.frontend.widgets.grid import Grid
from components_tests.frontend.widgets.info_bar import SubjectSelector


SUBJECT_SELECTOR = {
    'ELA': '#subjectSelector li:nth-child(1) > a',
    'ELA/L': '#subjectSelector li:nth-child(1) > a',
    'Mathematics': '#subjectSelector li:nth-child(2) > a',
}


@then('I verify that the subject selected is "{subject}"')
def check_selected_subject(context, subject):
    subject_selector = WebDriverHelper(context).by_css("div#subjectSelector span.selected-label")
    selected = subject_selector.text
    assert_that(subject, equal_to(selected))


@then('I verify that the subject dropdown has two options')
def check_dropdown_options(context):
    browser = WebDriverHelper(context)
    selector = browser.by_css('#subjectSelector .dropdown-toggle')
    # expand dropdown
    selector.click()
    options = browser.by_id("subjectSelector").find_elements_by_tag_name('li')
    assert_that(options, has_length(2))

    expected_subjects = ['ELA/L', 'Mathematics']
    for option, expected in zip(options, expected_subjects):
        assert_that(option.text, equal_to(expected))
    # collapse dropdown, important, or succeeding steps will be affected
    selector.click()


@when('I select "{subject}" from the subject dropdown')
def select_subject(context, subject):
    SubjectSelector(context).click_dropdown()
    SubjectSelector(context).click_subject(subject)
    # need to wait until page is reloaded because switching subject loads data
    # from backend
    Grid(context).wait_while_reloading()
