from behave import when, then
from hamcrest import assert_that, equal_to, is_not, contains_string, has_item
from components_tests.frontend.support import WebDriverHelper, wait_for_condition
import re

SUBJECT_TO_PERF_LEVEL = {
    'ELA': [
        ("1", "75", "LEVEL 1", "Minimal Understanding"),
        ("76", "150", "LEVEL 2", "Partial Understanding"),
        ("151", "275", "LEVEL 3", "Adequate Understanding"),
        ("276", "350", "LEVEL 4", "Strong Understanding"),
        ("351", "500", "LEVEL 5", "Distinguished Understanding"),
    ],
    'Mathematics': [
        ("1", "100", "LEVEL 1", "Minimal Understanding"),
        ("101", "200", "LEVEL 2", "Partial Understanding"),
        ("201", "300", "LEVEL 3", "Adequate Understanding"),
        ("301", "400", "LEVEL 4", "Strong Understanding"),
        ("401", "500", "LEVEL 5", "Distinguished Understanding"),
    ]
}


@then('I should see the ISR Header with PARCC logo')
def check_header(context):
    header = WebDriverHelper(context).by_id("header")
    assert header.find_element_by_tag_name("img"), "PARCC logo not found on the header"


@then('the ISR header should display the students name "{name}" in the header title')
def check_student_name(context, name):
    actual = WebDriverHelper(context).by_css("div.headerStudentName span").text
    assert name == str(actual), "{0} not displayed on the header".format(name)


@then('the ISR header should display the following static breadcrumbs')
def check_breadcrumb(context):
    breadcrumbs = WebDriverHelper(context).by_class('staticBreadcrumbs')
    levels = breadcrumbs.find_elements_by_tag_name("span")
    for row, level in zip(context.table, levels):
        actual = level.text
        expect = row['value']
        assert expect == actual, "{0} not displayed on the static breadcrumbs".format(expect)


@then('the overall performance section is displayed on ISR')
def check_overall_performance(context):
    browser = WebDriverHelper(context)
    overall_perf_text = browser.by_class("report-header").text
    assert_that(overall_perf_text, contains_string("PERFORMANCE"))
    context.overallPerfBar = browser.by_class("perf-section")


@then('the overall performance level is displayed as "{exp_level}"')
def check_overall_perf_level(context, exp_level):
    perf_level = WebDriverHelper(context).by_css('span.overall-level-indicator').text
    assert exp_level == perf_level, "overall performance level %s not displayed" % exp_level


@then('the overall description displayed contains score "{exp_score}", performance level "{exp_level}"')
def check_overall_perf_description(context, exp_score, exp_level):
    overall_perf_desc = str(WebDriverHelper(context).by_class("overall-performance-box").find_element_by_tag_name("p").text)
    desc_ln1 = "Your student performed at {exp_level} and earned a score of {exp_score} *.".format(
        exp_level=exp_level,
        exp_score=exp_score,
    )
    assert_that(overall_perf_desc, equal_to(desc_ln1))


@then('the performance bar is displayed with the student score marked as "{exp_score}" for "{subject}"')
def check_overall_perf_bar(context, exp_score, subject):
    browser = WebDriverHelper(context)
    indicator_text = browser.by_class('indicator-descriptor').text
    expected = "Student's Score: %s*" % exp_score
    assert_that(indicator_text, equal_to(expected))
    perf_indicator = browser.by_class('perf-indicator')
    assert perf_indicator, "Student score indicator not found on the performance bar."
    bar_sections = browser.by_class("perf-detail-section").find_elements_by_tag_name('div')
    assert len(bar_sections) == 5, "5 ALD sections not found on the performance bar."

    perf_levels = SUBJECT_TO_PERF_LEVEL[subject]
    for bar_section, perf_level in zip(bar_sections, perf_levels):
        check_each_perf_bar_section(bar_section, *perf_level)


def check_each_perf_bar_section(section, low_cutpt, high_cutpt, level_num, level_name):
    if level_num == "LEVEL 1":
        assert section.find_element_by_class_name("start").text == low_cutpt, "Lower cutpoint incorrectly displayed on perf bar"
    end = section.find_element_by_class_name("end").text
    assert_that(end, equal_to(high_cutpt), "Upper cutpoint incorrectly displayed on perf bar")
    actual_level_num = section.find_element_by_class_name("level-number").text
    assert_that(actual_level_num, equal_to(level_num), "Level number incorrectly displayed on perf bar")


@then('the performance bar legend contains the following')
def check_perf_bar_legend(context):
    legend_bar = WebDriverHelper(context).by_css('.bar-legand')
    perf_legends = legend_bar.find_elements_by_tag_name('li')
    for perf_legend, row in zip(perf_legends, context.table):
        exp_name, exp_value = row['name'], row['value']
        actual_name = perf_legend.find_element_by_class_name("name").text
        actual_value = perf_legend.find_element_by_class_name("value").text
        assert_that(exp_name, equal_to(actual_name))
        assert_that(exp_value, equal_to(actual_value))


@then('the claim performance bar legend for "{claim_type}" contains the following')
def check_claim(context, claim_type):

    def get_claim():
        if claim_type == 'READING':
            selector = 'div.subject2:nth-child(1)'
        elif claim_type == 'WRITING':
            selector = 'div.subject2:nth-child(2)'
        else:
            raise ValueError("Invalid claim_type")
        return WebDriverHelper(context).by_css(selector)

    def test_claim_title(claim_element):
        claim_title = claim_element.find_element_by_tag_name('h4').text
        assert_that(claim_title, equal_to(claim_type))

    def test_subclaim(claim_element):
        subscores = claim_element.find_element_by_class_name('subscore').text
        for row in context.table:
            assert_that(subscores, contains_string('score'))
            assert_that(subscores, contains_string(row['value']))

    claim_element = get_claim()
    test_claim_title(claim_element)
    test_subclaim(claim_element)


@then('the PDF report title area should display the following')
def check_pdf_version(context):
    info_bar = WebDriverHelper(context).by_id('infoBar')
    titles = info_bar.find_elements_by_tag_name('h1')
    for title, expected in zip(titles, context.table):
        assert_that(title.text, equal_to(expected['value']))


@then('student score should be "{score}"')
def check_student_score(context, score):
    actual_value = WebDriverHelper(context).by_class("score").text
    assert_that(actual_value, equal_to(score))


@then('I verify ccr styling "{is_present_or_not}" present in the perf section')
def test_isr_ccr_styling_presence(context, is_present_or_not):
    perf_section_inner_html = WebDriverHelper(context).by_css('.perf-section').get_attribute('innerHTML')
    ccr_elements = re.findall(r'isr-ccr-indicator', perf_section_inner_html)
    found = True if ccr_elements else False
    if is_present_or_not == "is":
        assert found, ccr_elements
    elif is_present_or_not == "is not":
        assert not found


@then('the content body is loaded without error')
def step_impl(context):
    '''
    Want to make sure the new window for PDF has some content that isnt' error page
    '''
    def is_page_loading():
        return 'html' not in context.driver.page_source
    # Any way to test for PDF content?
    wait_for_condition(is_page_loading, attempts=8)
    assert "error" not in context.driver.current_url
