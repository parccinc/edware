from behave import given, when, then
from components_tests.frontend.common import go_to_url, DOWNLOAD_DIR
import shutil
import csv
import tarfile
import zipfile
from time import sleep
import sys
import os


def unzip(zip_file, output_path):
    fh = open(zip_file, 'rb')
    z = zipfile.ZipFile(fh)
    for name in z.namelist():
        z.extract(name, output_path)
    fh.close()


def add_to_archive(dir_name, fileobj):
    previous_directory = os.getcwd()
    os.chdir(dir_name)
    for filename in os.listdir(dir_name):
        if os.path.isfile(filename):
            if not os.path.basename(filename).startswith("."):
                fileobj.add(filename)
    os.chdir(previous_directory)


def untar_file(file):
    with tarfile.open(file) as tar:
        untar_directory = os.path.join(os.path.dirname(file), 'untar_directory')
        if os.path.exists(untar_directory):
            shutil.rmtree(untar_directory)
        os.makedirs(untar_directory)
        tar.extractall(untar_directory)
    return untar_directory


@when('I untar the bulk extract file')
def step_impl(context):
    context.untar_directory = untar_file(context.local_extract_file)


@then('I convert the file to compressed tar format')
def step_impl(context):
    extract_file = os.path.join(DOWNLOAD_DIR, context.extract_files[0])
    extract_name_no_extension = os.path.splitext(os.path.basename(extract_file))[0]
    unzip(extract_file, DOWNLOAD_DIR)
    os.remove(extract_file)
    # Temporary path so that the tar.gz won't contain itself
    tmp_tar_dir = os.path.join(DOWNLOAD_DIR, 'tempTarDir')
    if not os.path.exists(tmp_tar_dir):
        os.makedirs(tmp_tar_dir)
    tar_gz_path = os.path.join(tmp_tar_dir, extract_name_no_extension) + '.tar.gz'
    with tarfile.open(name=tar_gz_path, mode='w:gz') as f:
        add_to_archive(DOWNLOAD_DIR, f)
    # Remove unzipped files from the download directory
    for file_name in os.listdir(DOWNLOAD_DIR):
        if os.path.isfile(os.path.join(DOWNLOAD_DIR, file_name)):
            os.remove(os.path.join(DOWNLOAD_DIR, file_name))
    shutil.move(tar_gz_path, DOWNLOAD_DIR)
    shutil.rmtree(tmp_tar_dir)
    export_path = os.path.join(DOWNLOAD_DIR, extract_name_no_extension) + '.tar.gz'
    context.local_extract_file = export_path


@then('the untarred file has only {num} file which is a CSV')
def step_impl(context, num):
    file_list = os.listdir(context.untar_directory)
    csv_files = [f for f in file_list if f.endswith(".csv")]
    expected = len(csv_files)
    assert expected == int(num), 'expected: {0}, actual: {1}'.format(int(num), expected)
    assert csv_files[0].endswith(".csv")


@then('the name of the file begins with "{file_prefix}"')
def step_impl(context, file_prefix):
    extract = context.extract_files[0]
    assert os.path.basename(extract).startswith(file_prefix), 'found {0}. Download dir: {1}'.format(extract, str(os.listdir(DOWNLOAD_DIR)))


@when('I go to download url')
def go_to_download_page(context):
    go_to_url(context, context.download_url)


@then('the CDS extract contains a file that begins with "{prefix}"')
def step_impl(context, prefix):
    file_list = os.listdir(context.untar_directory)
    context.filename = [f for f in file_list if f.startswith(prefix)]
    assert context.filename
