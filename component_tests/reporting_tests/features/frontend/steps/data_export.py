import os
import re
import time
import datetime
from behave import then
from behave import when
from components_tests.frontend.common import DOWNLOAD_DIR
from reporting_tests.features.frontend.steps.common import go_to_url
from components_tests.frontend.support import wait_for_condition, WebDriverHelper
from components_tests.frontend.widgets.header import Header, DataExportModal, HelpModal
from hamcrest import assert_that, equal_to, has_length, not_none, contains_string, has_item


def get_hpz_server_url(context):
    return "{0}://{1}".format(context.hpz_protocol, context.hpz_app_server)


def grade_selector_func(context, item):
    dropdown_list = context.driver.find_elements_by_css_selector('.modal-body .dropdown-toggle:first-child')
    dropdown_list[3].click()
    grd_list = context.driver.find_elements_by_css_selector('#DataExportModal .checkbox span')
    if item[0] == 'all':
        for grd_elem in grd_list:
            grd_elem.click()
    else:
        i = 0
        for grd_elem in grd_list:
            if i < len(item):
                if item[i] == grd_elem.text:
                    grd_elem.click()
                    i += 1
            else:
                break


@then('I should see "{exp_res}" link on header')
def test_link_on_header(context, exp_res):
    links = Header(context).get_links()
    assert_that(links, has_item(exp_res))


@when('I click on "{exp_res}" link on header')
def click_link_on_header(context, exp_res):
    Header(context).click_link(exp_res)


@then('I should see modal title "{expected_title}"')
def test_modal_title(context, expected_title):
    title = Header(context).get_modal_title()
    assert_that(title, equal_to(expected_title))


@when('I click on "{tab_name}" tab')
def click_model_tab(context, tab_name):
    HelpModal(context).click_tab(tab_name)


@then('I verified {links_count} links on "{tab_name}" tab')
def test_number_of_links(context, links_count, tab_name):
    actual = HelpModal(context).get_links_count_on_tab(tab_name)
    assert_that(actual, has_length(int(links_count)))


@then('I verified that "{section_name}" section on "FAQ" tab')
def test_general_section(context, section_name):
    actual = HelpModal(context).get_faq_section('question_section_header')
    assert_that(actual, equal_to(section_name))


@when('I scroll down at the end of the page')
def scroll_down_on_faq(context):
    HelpModal(context).scroll_down()


@then('I should be able to click on the last "GO BACK TO TOP" link')
def click_go_to_top(context):
    HelpModal(context).click_go_to_top()


@then('I should see a message below headline')
def test_data_export_message(context):
    expected = "Your export will include all students for whom you have PII permission. Please select export parameters below."
    actual = DataExportModal(context).get_data_export_message()
    assert_that(actual, equal_to(expected))


@then('I should see "{exp_res}" on Academic Year dropdown on modal')
def test_academic_year_selection(context, exp_res):
    actual = DataExportModal(context).get_academic_year()
    assert_that(actual, equal_to(exp_res))


@when('I click on Academic Year drop down')
def click_academic_year(context):
    DataExportModal(context).click_academic_year()


@then('I should see following items')
def test_dropdown_menu(context):
    items = Header(context).get_dropdown_menu()
    expected = [row['value'] for row in context.table]
    assert_that(items, equal_to(expected))


@when('I select "{year}" from Academic Year drop down')
def step_impl(context, year):
    WebDriverHelper(context).wait_for_css('.modal-body .btn-group .dropdown-menu li > a')
    jquery_selector = ".modal-body .btn-group .dropdown-menu li > a:contains('%s')" % year
    year_element = context.driver.execute_script("return $(\"{0}\")[0]".format(jquery_selector))
    year_element.click()


@then('I should see "Export Data" button is disabled')
def step_impl(context):
    btn_disable = context.driver.find_element_by_css_selector('.modal-footer .btn-success[disabled]')
    if len(btn_disable.text) > 0:
        assert True
    else:
        raise AssertionError('button is enabled')


@then('I should see "Export Data" button is enabled')
def step_impl(context):
    btn_disable = context.driver.find_element_by_css_selector('.modal-footer .btn-success:not([disabled])')
    if len(btn_disable.text) > 0:
        assert True
    else:
        raise AssertionError('button is disabled')


@when('I click on "Export Data" button')
def step_impl(context):
    context.driver.find_element_by_css_selector('.modal-footer .btn-success').click()


@then('I should see "{exp_res}" messages')
def step_impl(context, exp_res):

    def is_message_shown():
        return exp_res != context.driver.find_element_by_css_selector('.modal-body > p').text

    wait_for_condition(is_message_shown, msg="Waited too long. Message not displayed")


@then('I should see a link for download')
def step_impl(context):
    export_body = context.driver.find_element_by_css_selector('.modal-body')
    export_link = export_body.find_element_by_css_selector('a')
    hpz_server_url = get_hpz_server_url(context)
    assert hpz_server_url in export_link.text, 'expected: {0} in {1}'.format(hpz_server_url, export_link.text)
    context.download_url = export_link.text


@when('I click "Close" button')
def step_impl(context):
    Header(context).close_modal()


@when('I select "{item}" from "{drop_down_name}" drop down')
def step_impl(context, item, drop_down_name):
    if drop_down_name == context.driver.find_element_by_css_selector('#DataExportModal .export-type-label').text:
        export_drop_down_pos = 1
    elif drop_down_name == context.driver.find_element_by_css_selector('#DataExportModal .subject-label').text:
        subject_drop_down_pos = 2

    export_type_array = ['Summative Assessment Results',
                         'Summative Released Item File']
    subject_array = ['ELA/L', 'Mathematics']
    jquery_selector = ".modal-body .btn-group .dropdown-menu li > a:contains('%s')" % item
    javascript = "return $(\"{0}\")[0]".format(jquery_selector)
    #Psychometric file handle separately
    if item in('Summative Psychometric Item File', 'State Data for PARCC-level Results'):
        dropdown_list = context.driver.find_elements_by_css_selector('.modal-body .dropdown-toggle:first-child')
        dropdown_list[export_drop_down_pos].click()
        context.driver.execute_script(javascript).click()
    elif item in export_type_array:
        export_list_pos = export_type_array.index(item) + 1
        dropdown_list = context.driver.find_elements_by_css_selector('.modal-body .dropdown-toggle:first-child')
        dropdown_list[export_drop_down_pos].click()
        context.driver.execute_script(javascript).click()
    elif item in subject_array:
        subject_list_pos = subject_array.index(item) + 1
        dropdown_list = context.driver.find_elements_by_css_selector('.modal-body .dropdown-toggle:first-child')
        dropdown_list[subject_drop_down_pos].click()

        menu_list = context.driver.find_elements_by_css_selector('.modal-body ul.dropdown-menu > li:nth-child({0})'.format(subject_list_pos))
        menu_list[subject_drop_down_pos].find_element_by_tag_name("a").click()
    else:
        item_list = item.split(',')
        grade_selector_func(context, item_list)


@when('I click on "click here" link')
def step_impl(context):
    context.driver.find_element_by_css_selector('#start-over-export').click()


@when('I click on Export Type drop down')
def click_export_type(context):
    modal = DataExportModal(context)
    modal.verify_close_button_clickable()
    modal.click_export_type()


@then('I am redirected to the error page')
def step_impl(context):
    def is_error_page_loading():
        return 'error' not in context.driver.page_source
    wait_for_condition(is_error_page_loading, attempts=8)
    assert context.driver.title == "Error", "User is able to access PII data that he/she not allowed to see"


@then('I should not see following items')
def step_impl(context):
    list_elements = context.driver.find_elements_by_css_selector('.modal-body .btn-group .dropdown-menu li')
    list_values = []
    for element in list_elements:
        list_values.append(element.text)
    for row in context.table:
        assert not row['value'] in list_values, "{0} found in list values".format(row['value'])


# Wait for download file, and also set context.local_extract_file
@then(u'I wait until I see the download file')
def wait_for_file(context):

    # return False if there are file(s) in the download directory
    def is_file_generating():
        time.sleep(1)
        files = os.listdir(DOWNLOAD_DIR)
        is_empty = len(files) == 0
        if is_empty:
            # Trigger reload
            go_to_url(context, context.download_url)
        return is_empty

    # return False if there are no .part files in the download directory
    # (Which means the file is finished downloading)
    def is_finished_downloading():
        begin_time = datetime.datetime.now()
        while True:
            now = datetime.datetime.now()
            diff = now - begin_time
            diff = diff.seconds
            if diff >= 20:
                # BREAKDOWN
                raise AssertionError('Took more than 20 seconds to download')

            files = os.listdir(DOWNLOAD_DIR)
            if not any('.part' in file_name for file_name in files):
                # File is done downloading; return
                return False

    wait_for_condition(is_file_generating)
    wait_for_condition(is_finished_downloading)
    context.extract_files = os.listdir(DOWNLOAD_DIR)
    context.local_extract_file = os.path.join(DOWNLOAD_DIR, context.extract_files[0])


@then('the untarred file contains a CSV and a JSON')
def step_impl(context):
    file_list = os.listdir(context.untar_directory)
    assert len(file_list) == 2
    file_endings = [os.path.splitext(file_name)[1].lower() for file_name in file_list]
    assert '.csv' in file_endings and '.json' in file_endings


@then('I should not see Export Data button')
def step_impl(context):
    modal_inner_html = context.driver.find_element_by_class_name('modal-dialog').get_attribute('innerHTML')
    button = re.findall(r'class=\"(.+?btn-success.+?)\"', modal_inner_html)
    assert len(button) == 0, "Export Data button is displayed"
