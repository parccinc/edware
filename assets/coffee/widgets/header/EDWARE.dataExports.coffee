define [
  "jquery"
  "react"
  "edwareConstants"
  "EdwareApplication",
  "edwareDataProxy"
  "edwareYearSelector"
  "edwareExportTypeSelector"
  "edwareMultiGradeCourseSelector"
  "edwareSubjectSelector"
  "edwareUtil"
  "edwareModal"
  "edwareContextSecurity"
], ($
  React
  Constants
  edwareApplication
  edwareDataProxy
  edwareYearSelector
  edwareExportTypeSelector
  edwareMultiGradeCourseSelector
  edwareSubjectSelector
  edwareUtil
  edwareModal
  contextSecurity
) ->
  App = edwareApplication.createApp()
  YearsDropdown = edwareYearSelector.YearsDropdown
  ExportTypeDropdown = edwareExportTypeSelector.ExportTypeDropdown
  SubjectsDropdown = edwareSubjectSelector.SubjectsDropdown
  MultiGradesCoursesDropdown = edwareMultiGradeCourseSelector.MultiGradesCoursesDropdown
  EVENTS = Constants.EVENTS

  DataExportHeader = React.createClass
    render: () ->
      <div className="modal-header">
        <button data-tab-end type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 className="modal-title">Data Export</h4>
      </div>

  DataExportFooter = React.createClass
    render: () ->
      if @props.readyForExport
        exportButton = <button onClick={@props.onExport} type="button" className="btn btn-success">Export Data</button>
      else if not @props.exported
        exportButton = <button type="button" disabled className="btn btn-success">Export Data</button>
      <div className="modal-footer">
        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
        {exportButton}
      </div>

  DataExportContent = React.createClass

    renderSubjectSelector: () ->
      exportType = App.getState().exportType
      if exportType in [
        Constants.EXTRACT_SELECTORS.SUMMATIVE_ASMT_RESULT,
        Constants.EXTRACT_SELECTORS.SUMMATIVE_RIF,
      ]
        <SubjectsDropdown app={App} data={@props.config.subjectsList}  />
      else
        false


    render: () ->
      exportTypesList = contextSecurity.get().exportableTypesList(@props.config.exportTypesList)
      if @props.exported
        <div className="modal-body">
          <p id="exportedLinkLabel" aria-hidden="true">Copy and paste the link below to access your download:</p>
          <p><a aria-labelledby="exportedLinkLabel" className="exported-link" href={@props.downloadUrl} target="_blank">{@props.downloadUrl}</a></p>

          <p><a aria-label="Perform another export" role="presentation" id="start-over-export" href="#" onClick={@props.startOver} >Click here</a> to perform another export.</p>
        </div>
      else
        <div className="modal-body">
          <p data-tab-start tabIndex="0">Your export will include all students for whom you have PII permission. Please select export parameters below.</p>
          <div className="selectors">
            <YearsDropdown labelInside={false} app={App} />
            <ExportTypeDropdown app={App} data={exportTypesList} />
            {@renderSubjectSelector()}
            <MultiGradesCoursesDropdown app={App} data={@props.config.gradesCoursesList}  />
          </div>
        </div>

  DataExportModal = React.createClass
    componentDidMount: () ->
      App.subscribe(EVENTS.EXPORT_STATUS_CHANGE, @onChange)

    componentWillUnmount: () ->
      App.unsubscribe(EVENTS.EXPORT_STATUS_CHANGE, @onChange)

    onChange: () ->
      @setProps({
        readyForExport : App.getState().readyForExport
        exported : App.getState().exported
      })

    getInitialState: () ->
      { readyForExport : App.getState().readyForExport  }

    render: () ->
      <div id="DataExportModal" className="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true" >
        <div className="modal-dialog">
          <div className="modal-content">
            <DataExportHeader />
            <DataExportContent downloadUrl={@props.downloadUrl} startOver={@props.startOver} exported={@props.exported} config={@props.config} />
            <DataExportFooter onExport={@props.onExport} readyForExport={@props.readyForExport} exported={@props.exported} />
          </div>
        </div>
      </div>


  class EdwareDataExports

    constructor: (@container, @config) ->
      @localApp = App

    render: () ->
      self = this

      @component = React.render(
        <DataExportModal
          startOver={self.startOver}
          exported={App.getState().exported}
          onExport={self.onExport}
          readyForExport={App.getState().readyForExport}
          config={self.config}/>,
        $(self.container).get(0)
      )

    show: () ->
      @startOver()
      App.subscribe(EVENTS.SUBJECT_CHANGE, @onSubjectSelect)
      App.subscribe(EVENTS.EXPORT_TYPE_CHANGE, @onExportTypeSelect)
      App.subscribe(EVENTS.GRADE_COURSE_CHANGE, @onGradeCourseSelect)
      App.subscribe(EVENTS.YEAR_CHANGE, @onYearSelect)
      $('#DataExportModal').edwareModal()

    onSubjectSelect: () =>
      @clearSelectedGrades()
      @updateExportStatus()

    onExportTypeSelect: () =>
      @updateExportStatus()

    onGradeCourseSelect: () =>
      @updateExportStatus()

    onYearSelect: () =>
      @updateExportStatus()

    onExport: () =>
      exportParams = App.getState()
      if not exportParams.gradeCourse.length
        delete exportParams.gradeCourse
      exportParams.extractType = exportParams.exportType
      self = this
      loadingData = edwareDataProxy.fetchData "/services/extract", {
        method: "POST"
        params: exportParams
      }
      loadingData.done (data) ->
        self.component.setProps({downloadUrl : data[Constants.EXTRACT_JSON_RESPONSE.FILES][0][Constants.EXTRACT_JSON_RESPONSE.DOWNLOAD_URL]})
        App.setState({
          exported : true
          readyForExport: false
        })
        $('.exported-link').trigger('focus')

    startOver: () =>
      @getCleanSlate()
      @render()
      # breaking front-end unit tests. (we need a better way to do this)
      # setTimeout (() => $('[data-tab-start]', $(@component.getDOMNode())).trigger('focus')),0
    getCleanSlate: () ->
      App.setState({
        year : edwareUtil.getCurrentAcademicYear()
        subject : null
        gradeCourse : []
        exportType : null
        readyForExport : false
        exported : false
        stateCode: edwareApplication.getState().stateCode
      })
      App.forgetState()

    clearSelectedGrades: () ->
      App.setState({
        gradeCourse : []
      }, true)

    updateExportStatus: () ->
      if App.getState().exportType in [
        Constants.EXTRACT_SELECTORS.PSYCHOMETRIC,
        Constants.EXTRACT_SELECTORS.CDS_DATA,
      ]
        App.setValue({
          subject : null
          gradeCourse : []
        }, true)

        App.setState({
          readyForExport : true
        })
      else if App.getState().subject and App.getState().gradeCourse.length
        App.setState({
          readyForExport : true
        })
      else
        App.setState({
          readyForExport : false
        })


  create = (container, config) ->
    new EdwareDataExports(container, config)

  EdwareDataExports: EdwareDataExports
  DataExportHeader: DataExportHeader
  DataExportFooter: DataExportFooter
  DataExportContent: DataExportContent
  DataExportModal: DataExportModal
  create: create
