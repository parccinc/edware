define [
  "jquery"
  "react"
  "reactbootstrap"
  "edwareHeader"
  "edwareBreadcrumbs"
  "edwareSubjectSelector"
  "edwareViewSelector"
  "edwareResultSelector"
  "edwareYearSelector"
  "edwareGradeCourseSelector"
  "compareControls"
  "edwareFilterBar"
  "csvDownload"
  "edwareUtil"
  "edwareConstants"
  "EdwareApplication"
  "edwareDateRangePicker"
], ($, React, RB, edwareHeader, edwareBreadcrumbs, edwareSubjectSelector, edwareViewSelector, edwareResultSelector, edwareYearSelector, edwareGradeCourseSelector, compareControls, filterBar, CSVDownload, Utils, Constants, App, edwareDateRangePicker) ->

  Header = edwareHeader.Header
  EdwareBreadcrumbList = edwareBreadcrumbs.EdwareBreadcrumbList
  SubjectsDropdown = edwareSubjectSelector.SubjectsDropdown
  ViewSelector = edwareViewSelector.ViewSelector
  ResultsDropdown = edwareResultSelector.ResultsDropdown
  YearsDropdown = edwareYearSelector.YearsDropdown
  GradesCoursesDropdown = edwareGradeCourseSelector.GradesCoursesDropdown
  CompareOptions = compareControls.CompareBar
  filters = filterBar.FilterBar
  EdwareDateRangePicker = edwareDateRangePicker.EdwareDateRangePicker

  ReportTitle = React.createClass

    render: () ->
      <div>
        <h1 className="aboveReportTitle">
          {@props.aboveTitle}
        </h1>
        <h1 className="reportTitle">
          {@props.title}
        </h1>
      </div>


  ReportSubtitle = React.createClass

    render: () ->
      subtitle = "#{@props.length} #{@props.subtitle}".toLowerCase()
      <div className="reportSubtitle pull-left">{subtitle}</div>


  ReportInfoBar = React.createClass

    # needed in cpop where we recreate the bar on view change
    componentWillReceiveProps: (nextProps) ->
      if nextProps.hasCompareBarDisplayed isnt null
        @setState {
          hasCompareBarDisplayed: nextProps.hasCompareBarDisplayed
        }

    getInitialState: () ->
      {
        subtitle: @props.subtitle
        hasCompareBarDisplayed: @props.hasCompareBarDisplayed
        count: if @props.count? then @props.count else ''
        isFiltersOpen: false
      }

    onResultSelect: (result) ->
      @props.onResultSelect result

    createViewSelector: () ->
      if @props.hasViewSelector
        <div className="view-selector pull-right">
          <div>
          <ViewSelector data={@props.config.viewsList} />
          </div>
        </div>

    createSubtitle: () ->
      if not @props.subtitle
        return
      <ReportSubtitle length={@props.count} subtitle={@props.subtitle}/>

    createComponentAboveGrid: () ->
      state = App.getState()
      if @state.hasCompareBarDisplayed
        <CompareOptions activeToken={@props.compare} data={@props.data['summary']} assessmentCount={@props.count} labels={@props.config.labels}/>
      else if state.asmtType is 'DIAGNOSTIC' and Utils.getReportType(state) is Constants.REPORT_LEVELS.SCHOOL
        <EdwareDateRangePicker data={@props.data} />

    createBreadCrumbs: () ->
      <div id="breadcrumb">
            <EdwareBreadcrumbList contextData={@props.data.context} configs={@props.config.breadcrumb} displayHome=true labels={@props.config.labels}/>
      </div>

    createGradeCourseSelector: () ->
      if @props.hasGradeCourseSelector
        <div id="gradeCourseSelector" className="pull-left infobar-selector">
          <GradesCoursesDropdown data={@props.config.gradesCoursesList} />
        </div>

    createResultSelector: () ->
      if @props.hasResultSelector
        <div id="resultSelector" className="pull-left infobar-selector">
          <ResultsDropdown
             data={@props.config.resultsLists}
             handleResultSelection={@onResultSelect} />
        </div>

    createYearSelector: () ->
      if @props.hasYearSelector
          <div id="yearSelector" className="pull-left infobar-selector">
            <YearsDropdown labelInside={false} />
          </div>

    createSubjectSelector: () ->
      if @props.hasSubjectSelector
          <div id="subjectSelector" className="pull-left infobar-selector">
            <SubjectsDropdown data={@props.config.subjectsList} />
          </div>

    toggleFilters: (expanded=false) ->
      @setState({isFiltersOpen: expanded})

    handleFilterToggle: () ->
      @toggleFilters(!@state.isFiltersOpen)

    createFilterButton: () ->
      if not @props.hasFilters
        return
      @filters = (<RB.Button bsStyle="link" className="filters-link" onClick={@handleFilterToggle}><span>Filters</span></RB.Button>)

    createFilterBar: () ->
      if @props.hasFilters
        <filters data={@props.config['filters']} expanded={@state.isFiltersOpen} expandCollapse={@toggleFilters}></filters>

    createDownloadButton: () ->
      if not @props.hasDownloadButton
        return
      <CSVDownload></CSVDownload>

    noDataMessage: () ->
      if @props.count is 0
        <div className='no-results-msg'>
          <span className='no-data-icon font-icon'></span>
          <span>No report data available for current selections.</span>
        </div>

    render:() ->
      # Creates header, breadcrumb, report title, and selector dropdowns
      <div>
        <div id="header">
          <Header data={@props.data} hasYearSelector={!@props.hasYearSelector} config={@props.config} reverseBreadcrumb={@props.reverseBreadcrumb}/>
        </div>
        <div className="container">
          {@createBreadCrumbs()}
          <ReportTitle title={@props.title} aboveTitle={@props.aboveTitle}/>
          <div className="selectors">
              {@createYearSelector()}
              {@createSubjectSelector()}
              {@createGradeCourseSelector()}
              {@createResultSelector()}
              {@createViewSelector()}
          </div>
          {React.Children.map(@props.children, (child) -> child)}
          <div className="context-bar row">
            <div className="span4">{@createSubtitle()}</div>
            <div className="span5">{@createComponentAboveGrid()}</div>
            <div className="span3 pull-right text-right">
              {@createFilterButton()}
              {@createDownloadButton()}
            </div>
          </div>
          {@createFilterBar()}
          {@noDataMessage()}
        </div>
      </div>

  create = (options) ->
    defaultConfig =
      data: {}
      config: {}
      title: ''
      subtitle: null
      additionalChildren: '',
      headerBreadcrumb: false
      filters: false
      gradeCourseSelector: false
      subjectSelector: true
      cssSelector: '#infoBar'
      download: true
      onResultSelect: () ->
    conf = $.extend true, {}, defaultConfig, options
    conf.hasCompareBarDisplayed ?= conf.compare
    React.render(
      <ReportInfoBar
        data={conf.data}
        config={conf.config}
        hasFilters={conf.filters}
        hasGradeCourseSelector={conf.gradeCourseSelector}
        hasResultSelector={conf.resultSelector}
        hasSubjectSelector={conf.subjectSelector}
        hasViewSelector={conf.viewSelector}
        filterResultsList={conf.filterResultsList}
        hasYearSelector={conf.yearSelector}
        hasCompareBarDisplayed={conf.hasCompareBarDisplayed}
        hasDownloadButton={conf.download}
        reverseBreadcrumb={conf.reverseBreadcrumb}
        title={conf.title}
        subtitle={conf.subtitle}
        count={conf.count}
        aboveTitle={conf.aboveTitle}
        compare={conf.compare}
        onResultSelect={conf.onResultSelect}
        onViewSelect={conf.onViewSelect} >
        {options.additionalChildren}
        </ReportInfoBar>,
      $(conf.cssSelector)[0]
    )

  create:create
  ReportInfoBar:ReportInfoBar
