define [
  "jquery"
  "bootstrap"
  "react"
  "edwareUtil"
  "edwareConstants"
  "edwareHelpMenu"
  "edwareEvents"
  "edwareModal"
  "edwareYearSelector"
  "EdwareApplication"
  "edwareDataExports"
  "edwareDataProxy"
  "edwareContextSecurity"
],
($,
 Bootstrap,
 React,
 edwareUtil,
 Constants,
 edwareHelpMenu,
 edwareEvents,
 edwareModal,
 edwareYearSelector,
 App,
 edwareDataExports,
 edwareDataProxy
 contextSecurity) ->

  EdwareHelpMenu = edwareHelpMenu.EdwareHelpMenu
  YearsDropdown = edwareYearSelector.YearsDropdown
  HeaderLogoElement = React.createClass
    render: () ->
      <a aria-label="Home" href="../html/index.html" id='logo' title='Home'>
        <img aria-hidden="true" className="logo" src={@props.logoSrcPath}/>
      </a>

  HeaderTenantLabelElement = React.createClass
    render: () ->
      <li className='pull-left'>{@props.tenantLabel}</li>

  HeaderUserSettingElement = React.createClass
    handleLogOut: () ->
      window.open '/logout', 'iframe_logout'

    handleOnMouseLeave: () ->
      $(@getDOMNode()).removeClass 'open'

    render: () ->
      <li className='dropdown' >
        <a id='user-settings' className='headerLink' data-toggle='dropdown' aria-label='Settings' href='#'>
            <span id='username' className='glyphicon username menu'>{@props.userName}</span>
        </a>
        <ul id='languageSelector' onMouseLeave={@handleOnMouseLeave} className="dropdown-menu" role="radiogroup">
            <div id="log_out">
                <li className='user-settings-in-menu'>
                  <a className='headerLink user-settings' data-toggle='dropdown' aria-label='Settings' href>
                    <span className='glyphicon username menu'>{@props.userName}</span>
                  </a>
                </li>
                <li className="divider"></li>
                <li>
                    <button id="log_out_button" onBlur={@handleOnMouseLeave} onClick={@handleLogOut} type="button" className="btn btn-primary">
                      <span>{@props.logoutLabel}</span>
                    </button>
                </li>
            </div>
        </ul>
      </li>

  HeaderHelpElement = React.createClass
    componentDidMount: () ->

      config = {
        labels: @props.labels
      }
      self = this
      edwareDataProxy.getDataForHelpContent().done (data) ->
        config.data = data
        # Creates Help Menu PopUp
        self.helpMenu = edwareHelpMenu.create '.HelpMenuContainer', config

    handleOnClick: () ->
      @helpMenu.show()

    render: () ->
      <li className=''>
        <a id='help' onClick={@handleOnClick} href='#' className='headerLink'>
          <span className="text_help glyphicon help">{@props.help_text}</span>
        </a>
      </li>

  HeaderIframeElement = React.createClass
    #<!--This iframe is used for logout redirect.  Do not remove it.-->
    render: () ->
      <iframe frameBorder='0' height='0px' width='0px' name='iframe_logout'></iframe>

  HeaderHelpMenuContainer = React.createClass
    # Container for Help Menu Pop up
    render: () ->
      <div className="HelpMenuContainer"></div>

  HeaderDataExportsModalContainer = React.createClass
  # //Container for Help Menu Pop up
    render: () ->
      <div className="DataExportContainer"></div>

  HeaderReverseBreadcrumb = React.createClass
    #// This is a reverse breadcrumb only used in ISR
    createCrumbFromContext: (context, key) ->
      <span className={"#{context.type} breadcrumb-item"} title={context[key]}>{context[key]}</span>

    isPdf: () ->
      !!App.getState().pdf

    render: () ->
      studentName = @props.context[5].name
      studentDOB = @props.context[5].dob
      gradeCourse = @props.context[4].name
      elementNodes = @props.context.slice(1, 4).map((item, index) =>
        key = if index is 0 and !@isPdf() then 'id' else 'name'
        @createCrumbFromContext(item, key)
      )
      if not @isPdf()
        elementNodes.push (<span className={"#{@props.context[4].type} breadcrumb-item"} title= {gradeCourse}>{gradeCourse}</span>)
      else
        gradeCourse = gradeCourse.replace('Grade', '')
        elementNodes.push (<span className={"#{@props.context[5].type} breadcrumb-item"}>{"Date of Birth: #{studentDOB}, SID: #{@props.context[5].id}, Grade: #{gradeCourse}"}</span>)
        elementNodes.splice(0,2)
        elementNodes.unshift (<span className={"#{@props.context[1].type} #{@props.context[2].type} breadcrumb-item"}>{"#{@props.context[2]['name']}, #{@props.context[1]['name']}"}</span>)

      <div className="headerStudentName">
        {"#{studentName}"}
        <div className="staticBreadcrumbs">
          {elementNodes.reverse()}
        </div>
      </div>

  HeaderExportLink = React.createClass
    componentDidMount: () ->
      # Creates Help Menu PopUp
      @exportMenu = edwareDataExports.create '.DataExportContainer', @props.config

    handleOnClick: () ->
      @exportMenu.show()

    render: () ->
      <li className=''>
        <a onClick={@handleOnClick} href='#' className='headerLink export-link'>
          <span className="glyphicon export-data">{@props.exportLabel}</span>
        </a>
      </li>




  Header = React.createClass
    isPrint: () ->
      App.getState().pdf?

    createReverseBreadcrumb: () ->
      if not @props.reverseBreadcrumb or @props.data.context.items.length < 5
        return
      context = @props.data.context.items
      <HeaderReverseBreadcrumb context={context} />

    createExportsLink: (label, config) ->
      security = contextSecurity.get()
      if edwareUtil.getReportType(App.getState()) isnt Constants.REPORT_TYPE_INT.CONSORTIUM and (security and security.canExportAny())
        <HeaderExportLink canDisplay={false} exportLabel={label} config={config} />

    createAnalyticsLink: (label, props) ->
      security = contextSecurity.get()
      if security and security.canAccessAnalytics()
        <li className="">
          <a href={@props.data.analytics.url} target="_blank" className="headerLink analytics-link" >
            <span className="glyphicon analytics">{label}</span>
          </a>
        </li>

    render: () ->
      labels = @props.config.labels
      userInfo = @props.data.user_info
      userName = edwareUtil.getUserName userInfo
      #// Get tenant level branding information
      templateData = edwareUtil.getTenantBrandingData @props.data.metadata, @isPrint()
      tenantLabel = ''
      yearDropDown = ''
      if @props.hasYearSelector and not @isPrint()
        yearDropDown =
          <div className="selectors">
            <div id="yearSelector" className="infobar-selector">
              <YearsDropdown labelInside={true} />
            </div>
          </div>
      <div role="presentation" className='container'>
        <HeaderLogoElement logoSrcPath={templateData.tenantLogo} />
        {yearDropDown}
        <ul className='nav navbar-nav pull-right' role="navigation">
          <HeaderHelpElement labels={labels} help_text={labels.help} labels={labels}/>
          {@createExportsLink(labels.export_data, @props.config)}
          {@createAnalyticsLink(labels.analytics, @props)}
          <HeaderUserSettingElement userName={userName} logoutLabel={labels.logout} />
          <HeaderTenantLabelElement tenantLabel={tenantLabel} />




        </ul>
        <HeaderIframeElement />
        <HeaderHelpMenuContainer />
        <HeaderDataExportsModalContainer />
        {@createReverseBreadcrumb()}
      </div>

  create = (options) ->
    options.reverseBreadcrumb ?= false
    React.render(
      <Header data={options.data} config={options.config} reverseBreadcrumb={options.reverseBreadcrumb}/>,
      $('#header')[0]
    )

  create: create
  Header: Header
  HeaderHelpElement: HeaderHelpElement
  HeaderUserSettingElement: HeaderUserSettingElement
  HeaderHelpMenuContainer: HeaderHelpMenuContainer
