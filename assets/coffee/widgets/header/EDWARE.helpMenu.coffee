define [
  "jquery"
  "react"
  "edwareDataProxy"
  "edwareModal"
], ($,
    React,
    edwareDataProxy,
    edwareModal) ->


  HelpMenuHeader = React.createClass
    render: () ->
      <div className="modal-header help-menu-header">
        <button data-tab-end type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <ul className="nav nav-tabs">
          <li className="disabled"><a className="modal-title">{@props.labels.help}</a></li>
          <li className="active"><a className="help_anchor" href="#help_faq" data-toggle="tab">User Guide</a></li>
          <li className=""><a className="help_anchor" href="#video_faq" data-toggle="tab">Video</a></li>
        </ul>
      </div>

  HelpMenuFooter = React.createClass
    render: () ->
      <div className="modal-footer">
        <div dangerouslySetInnerHTML={{__html: @props.data}}></div>
      </div>

  HelpMenuContent = React.createClass
    render: () ->

      r_links = @props.data.resources_content.items.map((item, index, list) -> <li dangerouslySetInnerHTML={{__html: item}}></li>)
      v_links = @props.data.video_content.items.map((item, index, list) -> <li dangerouslySetInnerHTML={{__html: item}}></li>)

      <div className="modal-body">
        <div className="tab-content">
          <div className="tab-pane active" id="help_faq">
            <ul>
            {r_links}
            </ul>
          </div>
          <div className="tab-pane" id="video_faq">
            <ul>
            {v_links}
            </ul>
          </div>
        </div>
      </div>

  HelpMenuModal = React.createClass
    render: () ->
      <div id="HelpMenuModal" className="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div className="modal-dialog modal-lg">
          <div className="modal-content">
            <HelpMenuHeader labels={@props.labels} />
            <HelpMenuContent data={@props.data} />
            <HelpMenuFooter data={@props.data.faq_footer} />
          </div>
        </div>
      </div>


  class EdwareHelpMenu

    constructor: (@container, @config) ->

    render: () ->
      self = this
      @component = React.render(
        <HelpMenuModal labels={self.config.labels} data={self.config.data} />,
        $(self.container).get(0)
      )

    show: () ->
      @render()
      $('#HelpMenuModal').edwareModal()

  create = (container, config) ->
    new EdwareHelpMenu(container, config)

  EdwareHelpMenu: EdwareHelpMenu
  HelpMenuHeader: HelpMenuHeader
  HelpMenuFooter: HelpMenuFooter
  HelpMenuContent: HelpMenuContent
  HelpMenuModal: HelpMenuModal
  create: create
