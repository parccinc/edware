define [
  "jquery"
  "edwareConstants"
], ($, Constants) ->

  ### Edware client storage. ###
  class EdwareClientStorage

    constructor: (@key, @isLongTerm = true) ->

      # By default, we use sessionStorage unless isLongTerm is true
      @isLongTerm = if typeof @isLongTerm isnt "undefined" then @isLongTerm else false
      # Test if localStorage is supported, if not, make it sessionStorage
      if @isLongTerm and not window.localStorage
        @isLongTerm = false
      @storage = if @isLongTerm then localStorage else sessionStorage

    getKey: () ->
      @key

    setKey: (key) ->
      @key = key

    load: () ->
      ### Loads data from session storage###
      JSON.parse @storage.getItem(@key)

    save: (data) ->
      ###
      Saves data into session storage.
      Data must be able to convert to a JSON string in order to be put into session storage.
      ###
      @storage.setItem(@key, JSON.stringify(data))

    update: (data) ->
      ###
      Merge data into existing storage
      ###
      merged = $.extend (@load() or {}), data
      @storage.setItem(@key, JSON.stringify(merged))

    clear: () ->
      ### Clear data ###
      @storage.removeItem(@key)


    clearAll: () ->
      ### Clears all client storage. ###
      sessionStorage.clear()
      localStorage.clear()
