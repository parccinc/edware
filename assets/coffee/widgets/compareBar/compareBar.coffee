define ['jquery', 'react', 'EdwareApplication'], ($, React, App) ->

  getState = (data, token) ->
    activeSummaries: App.get(token)
    activeViews: (data?['SUMMATIVE'] or {})[App.get('subject')]

  CompareControl = React.createClass
    getInitialState: () ->
      {checked: @props.active}

    handleChange: (e) ->
      active = !@props.active
      @props.handleChange active, @props.data

    showOptOutPopover: (event) ->
      opener = @getDOMNode(@refs.optOutMsgTrigger)
      if opener
        $opener = $(opener)
        if not $opener.data('popover')
          $opener.popover({
            content: @props.labels.cds_opt_out_message
            animation: true
            html: false
            placement: 'top'
            container: 'body'
            delay: {show: 100, hide: 300}
          })
        $opener.popover('show')

    hideOptOutPopover: (event) ->
      opener = @getDOMNode(@refs.optOutMsgTrigger)
      $(opener).popover('hide') if opener

    renderOptOutMessage: () ->
      if @props.isDisabled
        <span className="font-icon info compare-opt-out-message"
          ref="optOutMsgTrigger"
          onMouseEnter={@showOptOutPopover}
          onMouseLeave={@hideOptOutPopover}
        ></span>

    render: () ->
      classList = ['checkbox', 'inline', 'summaryLabel']
      if @props.active
        classList.push 'active'
      disabled = false
      if @props.isDisabled
        disabled = 'disabled'
        classList.push disabled
      <label className={classList.join ' '}>
          <input aria-label={"Compare with #{@props.name} average"} className='compare-toggle' type="checkbox" name={"compare_#{@props.name}"} checked={@props.active} onChange={@handleChange} disabled={disabled}/>
          <span aria-hidden="true" >{@props.name}</span>
          {@renderOptOutMessage()}
      </label>

  CompareBar = React.createClass

    getInitialState: () ->
      # setup defaults if they don't exist yet.
      state = getState(@props.data, @props.activeToken)
      if !state.activeSummaries and state.activeViews
        state.activeSummaries = {}
        App.set(@props.activeToken, state.activeSummaries)
      # return initial state data
      @getViewItems()

    getViewItems: () ->
      data = getState(@props.data, @props.activeToken)
      data.activeSummaries = data.activeSummaries or {}
      data.activeViews = data.activeViews or []
      data

    componentDidMount: () ->
      App.subscribe(@props.activeToken, @onChange)
      App.subscribe('subject', @onChange)

    componentWillUnmount: () ->
      App.unsubscribe(@props.activeToken, @onChange)
      App.unsubscribe('subject', @onChange)

    handleChange: (isActive, item) ->
      activeSummaries = App.get(@props.activeToken) or {}
      activeSummaries[item.type] = isActive
      App.setValue(@props.activeToken, activeSummaries)

    onChange: () ->
      @setState(@getViewItems())

    render: () ->
      handleChange = @handleChange
      hiddenClass = 'hidden' if not @state.activeViews or !@state.activeViews.length or !@props.assessmentCount
      <div aria-label="Compare options" className="compare-controls text-center #{hiddenClass or ''}">
        <span aria-hidden="true" className="copmare-label inline">Compare:</span>
        {@state.activeViews.map((summary, index) =>
          <CompareControl data={summary} index={index} name={summary.type} active={@state.activeSummaries[summary.type]} handleChange={handleChange} key={summary.name} isDisabled={summary.disabled} labels={@props.labels}/>
        ).reverse()}
      </div>

    create = (data, activeToken, container='#compare-controls') ->
      React.render(
        <CompareBar activeToken={activeToken} data={data} />,
        $(container).get(0)
    )
  {
    create: create
    CompareBar: CompareBar
  }
