define [
  'jquery'
  'edwareBreadcrumbs'
  'edwareConstants'
  'EdwareApplication'
  'edwareUtil'
  'edwareFilterBar'
  'edwareResultSelector'
  'edwareViewSelector'
  'edwareYearSelector'
  'selectorUtil'
], ($, Breadcrumbs, Constants, App, Utils, Filters, ResultSelector, ViewSelector, YearSelector, selectorUtil) ->

  EVENTS = Constants.EVENTS

  class ListReport

    constructor: () ->

    initialize: () ->

    setState: (state) ->
      @state = $.extend true, {}, state

    getState: () ->
      @state

    setSubject: (@subject) ->

    getSubject: () ->
      @subject

    setResult: (@result) ->

    setDateRange: (@dateRange) ->

    getDateRange: () ->
      @dateRange

    getResult: () ->
      @result

    setYear: (@year) ->
    getYear: () -> @year

    setView: (@view) ->

    getView: () ->
      @view

    setSortColumn: (@sortColumn, @sortAsc) ->

    getSortColumn: () ->
      @sortColumn

    getSortAsc: () ->
      @sortAsc

    setReportType: (@reportType) ->

    getReportType: () ->
      @reportType

    setTitle: (@title) ->

    getTitle: () ->
      @title

    setSubtitle: (@subtitle) ->

    getSubtitle: () ->
      @subtitle

    setColumns: (@columnProvider) ->

    getColumns: () ->
      @columnProvider

    setDataProvider: (@dataProvider) ->

    getDataProvider: () ->
      @dataProvider

    setSummaryProvider: (@summaryProvider) ->

    getSummaryProvider: () ->
      @summaryProvider

    setReportCount: (@reportCount) ->
    getReportCount: () ->
      @reportCount or @dataProvider.getLength()

    setBreadCrumbs: (@breadCrumbs) ->

    getBreadCrumbs: () ->
      @breadCrumbs

    setFilterData: (@filterData) ->
    getFilterOptions: () ->
      Filters.FilterUtil.getActiveFilterCrumbs(@filterData)


  class ReportBuilder

    constructor: () ->
      @report = new ListReport()

    titles: (module) ->
      [title, subtitle] = module.getReportTitle()
      @report.setTitle title
      @report.setSubtitle subtitle
      this

    dataProvider: (module) ->
      @report.setDataProvider module.gridObj.getDataSource()
      this

    summaryProvider: (module) ->
      @report.setSummaryProvider module.gridObj.headerDataSource
      this

    columns: (module, state) ->
      @report.setColumns module.gridObj.getColumnProvider().getColumns state
      this

    breadcrumbs: (module) ->
      configs = module.config.breadcrumb['items']
      contextData = module.data.context['items']
      labels = module.config.labels
      @report.setBreadCrumbs Breadcrumbs.Utils.getCurrentPath contextData, configs, labels
      this

    filters: (module, state) ->
      @report.setFilterData module.config['filters']
      if state.asmt_date
        @report.setDateRange(state.asmt_date)

      this

    reportType: (module) ->
      @report.setReportType module.getReportType()
      this

    state: (state) ->
      @report.setState state
      this

    result: (module, state) ->
      reportState = selectorUtil.currentReportState state
      resultsList = module.config.resultsLists
      options = ResultSelector.Util.buildResultsForReport(resultsList, reportState)
      @report.setResult ResultSelector.Util.getForCurrentState(reportState, options)
      this

    subject: (module, state) ->
      # get subject object
      subject = module.data.subjects.filter (subject) ->
        subject.value is state.subject
      if subject.length
        @report.setSubject subject[0]
      this

    year: (state) ->
      @report.setYear YearSelector.Utils.getYearObject state.year
      this

    view: (module, state) ->
      reportState = selectorUtil.currentReportState state
      viewsList = module.config.viewsList
      if state.view and viewsList
        options = ViewSelector.Utils.buildViewsForReport(viewsList, state)
        @report.setView ViewSelector.Utils.getForCurrentState(state, options)
      this

    sortColumn: (module) ->
      getColumnForId = (columns, id) ->
        for column in columns
          if column.id is id
            return column

      sortInfo = module.gridObj.getSortColumn()
      # we can take a shortcut because
      # we currently dont support multi-column sort
      if sortInfo and sortInfo.length
        columns = @report.getColumns()
        column = getColumnForId(columns, sortInfo[0].columnId)
        if column
          @report.setSortColumn(column, sortInfo[0])
      this

    getReport: ->
      @report


  buildReport = (module, state) ->
    (new ReportBuilder())
      .titles(module)
      .dataProvider(module)
      .summaryProvider(module)
      .columns(module, state)
      .breadcrumbs(module)
      .filters(module, state)
      .reportType(module)
      .state(state)
      .result(module, state)
      .subject(module, state)
      .view(module, state)
      .year(state)
      .sortColumn(module)
      .getReport()


  create = () ->

  {
    create: create
    buildForReport: buildReport
    Report: ListReport
    Builder: ReportBuilder
  }
