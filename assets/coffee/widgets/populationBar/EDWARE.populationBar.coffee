define [
  'jquery'
  'mustache'
  'text!populationBarTemplate'
], ($, Mustache, populationBarTemplate) ->

  renderPopulationBar = (items) ->
    for performanceLevel, idx in items.performanceLevels
      if performanceLevel.percentage < 5
        # so it doesn’t show percentage text when there is no room
        items.performanceLevels[idx].hidePercentage = 'hidePercentage'
    items.textVersion = textVersion(items)
    output = Mustache.to_html populationBarTemplate, items
    output

  create = (data) ->
    renderPopulationBar data

  createPopover = (elm, data, conf) ->
    $(elm).popover($.extend({
      trigger: 'hover focus',
      html: true
      content: create(data)
      animation: true
      container: 'body'
    }, conf))

  levelText = (level) ->
    "Level #{level.level} - #{level.percentage}%"

  textVersion = (data) ->
    levels = []
    levels.push levelText(level) for level in data.performanceLevels
    levels.join(', ')


  create: create
  createPopover: createPopover
  createText: textVersion
