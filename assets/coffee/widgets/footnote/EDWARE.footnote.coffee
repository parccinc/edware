define [
  'jquery'
  'react'
  'edwareConstants'
  'EdwareApplication'
], ($, React, Constants, App) ->

  Footnote = React.createClass

    render: () ->
      if @props.labels.footnotes[@props.state.stateCode] and @props.show
        <div className="footnote_text">{@props.labels.footnotes[@props.state.stateCode]}</div>
      else
        <div className="footnote_text"></div>


  create = (options) ->
    container = options['container'] || '#footnote'
    React.render(
      <Footnote
        labels={options.labels}
        state={options.state}
        show={options.show}/>,
      $(container)[0]
    )

  {
    create: create,
  }
