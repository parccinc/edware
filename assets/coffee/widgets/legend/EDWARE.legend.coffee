define [
  'jquery'
  'mustache'
  'react'
  'edwareConstants'
  'EdwareApplication'
  'selectorUtil'
], ($, Mustache, React, Constants, App, SelectorUtil) ->

  ExpressivenessLevel = React.createClass
    render: () ->
      item = @props.item
      iconClasses = ['legend-item-icon', 'level-colored-value', "level-#{@props.level}"]
      <li className="level expressiveness">
        <span className="screenreader">level {@props.level}</span>
        <span className={iconClasses.join(' ')}>{@props.level}</span>
        <p className="legend-title legend-title-1">{@props.item.title1}</p>
      </li>


  MasteryLevel = React.createClass
    render: () ->
      item = @props.item
      iconClasses = ['legend-item-icon', 'glyphicon', 'checks-and-x', @props.cutpoint.type]
      <li className="level" aria-label="#{item.title1} #{item.title2.replace('-', ' to ')}">
        <span className="screenreader">level {@props.level}</span>
        <span className={iconClasses.join(' ')}></span>
        <p className="legend-title legend-title-1">{item.title1}</p>
        <p className="legend-title legend-title-2">{item.title2}</p>
      </li>

  PerformanceLevel = React.createClass
    render: () ->
      item = @props.item
      iconClasses = ["legend-item-icon", "legend_bar_square", "numeric_perf_level_#{item.level}"]
      <li className="level" aria-label="Level #{item.level}, #{item.title1} #{item.title2.replace('-', ' to ')}">
        <span className="screenreader">Level</span>
        <span className={iconClasses.join(' ')}>{item.level}</span>
        <p className="legend-title legend-title-1">{item.title1}</p>
        <p className="legend-title legend-title-2">{item.title2}</p>
      </li>

  SubClaimLevel = React.createClass
    render: () ->
      item = @props.item
      iconClassess = ["legend-item-icon", "glyphicon", "perf_level_#{item.level}"]
      <li className="level" aria-label="Subclaim Level #{@props.level}, #{item.title1}, #{item.title2.replace('-', ' to ')}">
        <span className="screenreader">Subclaim level {@props.level},</span>
        <span className={iconClassess.join(' ')}></span>
        <p className="legend-title legend-title-1">{item.title1}</p>
        <p className="legend-title legend-title-2">{item.title2}</p>
      </li>


  Text = React.createClass
    render: () ->
      <li className="star">{@props.desc}</li>

  Content = React.createClass

    renderText: (text) ->
      <Text desc={text} />

    renderSubclaim: (item, index) ->
      <SubClaimLevel item={item} level={index + 1} />

    renderPerformance: (item) ->
      <PerformanceLevel item={item} />

    renderMastery: (item, index) ->
      <MasteryLevel item={item} cutpoint={@props.cutpoints[index]} level={index + 1} />

    renderExpressiveness: (item, index) ->
      <ExpressivenessLevel item={item} level={index} />

    render: () ->
      itemTypes = {
        "performanceLevels": @renderPerformance
        "subclaimLevels": @renderSubclaim
        "text": @renderText
        "masteryLevels": @renderMastery
        "expressivenessLevels": @renderExpressiveness
      }
      renderer = itemTypes[@props.type]
      data = @props.data or [@props.desc]
      options = data.map (item, index) ->
        renderer(item, index)

      <ul className="legend-items-list float #{@props.type}" aria-label="levels">
        <li role="presentation" aria-hidden="true" className="legend-label">{"#{@props.legendLabel}:"}</li>
        {options}
      </ul>

  Legend = React.createClass

    componentDidMount: () ->
      if @getLegendData()
        @setAccessibilityProps(@state)

    toggleLegend: (show, event) ->
      newState = {show: show}
      event.preventDefault()
      @setState(newState)
      @setAccessibilityProps(newState)

    setAccessibilityProps: (state) ->
      buttons = @getButtonMap(state)
      buttons.active.attr({href: '','tab-index': 0})
      if buttons.inactive.is(document.activeElement)
        buttons.active.focus()
      buttons.inactive.removeAttr('href').attr({'tab-index': -1})

    getButtonMap: (state) ->
      if state.show
        {
          active: $(@refs.showBtn.getDOMNode())
          inactive: $(@refs.hideBtn.getDOMNode())
        }
      else
        {
          active: $(@refs.hideBtn.getDOMNode())
          inactive: $(@refs.showBtn.getDOMNode())
        }

    getInitialState: () ->
      {show: true}

    getLegendData: () ->
      legendData = SelectorUtil.buildOptionsForReport(@props.data, @props.report)
      legendData and legendData[0]

    showLegendDetails: () ->
      @setState({showDetails: true})

    hideLegendDetails: () ->
      @setState({showDetails: false})

    createLegendContent: (legendData) ->
      levels = legendData.levels
      if legendData.details
        if @state.showDetails
          hideDetailLink = <button
            onClick={@hideLegendDetails}
            className="hide-details-btn btn-link"
            >Hide details</button>
          levels = legendData.details.levels
        else
          detailLink = <button
            onClick={@showLegendDetails}
            className="show-details-btn btn-link"
          >Click for details...</button>
      if @props.cutpoints and @props.cutpoints.length
        cutPoints = SelectorUtil.buildOptionsForReport(@props.cutpoints, @props.report)
      <div>
        {hideDetailLink}
        <Content
          data={levels}
          legendLabel={legendData.legendLabel or 'Legend'}
          type={legendData.type or 'text'}
          desc={legendData.content}
          cutpoints={cutPoints}
        />
        {detailLink}
      </div>

    createCollegeCareerIndicator: () ->
      if @props.ccReady
        <span className="ccr-indicator legend-ccr-indicator">
          {@props.labels.collegeCareerReady}
        </span>

    renderLegend: (legendData) ->
      onToggleLegend = @toggleLegend
      <div className="below #{if @state.show then 'show' else 'collapsed'} #{if @state.showDetails then 'legend-details' else ''}" aria-label="performance legend" aria-live="polite">
          <div className="show container" role="presentation">
            <a
              href
              ref="hideBtn"
              type="button"
              aria-label="click here to open the Legend"
              className="unhide legend-toggle toggle-link"
              aria-hidden={@state.show}
              onClick={onToggleLegend.bind(this, true)}>Legend</a>
          </div>
          <div className="legend #{if @props.ccReady then 'ccr'}">
            <div className='container'>
              <div className="centered" aria-hidden={not @state.show} id='legend-description'>
                {@createLegendContent(legendData)}
                {@createCollegeCareerIndicator()}
              </div>
              <a
                href
                ref="showBtn"
                aria-label="click here to close the legend."
                aria-hidden={not @state.show}
                className="hide legend-toggle toggle-link"
                onClick={onToggleLegend.bind(this, false)}
                type='button'></a>
            </div>
          </div>
        </div>

    render: () ->
      legendData = @getLegendData()
      if legendData
       <div className="legend-wrapper">
        {@renderLegend(legendData)}
        </div>
      else
        false


  create = (options) ->
    container = options['container'] || '#footer'
    React.render(
      <Legend
        labels={options.labels}
        report={options.reportState}
        data={options.data}
        ccReady={options.ccReady or false}
        cutpoints={options.cutpoints} />,
      $(container)[0]
    )

  {
    create: create,
  }
