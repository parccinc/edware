define [
  'jquery'
  'react'
  'reactbootstrap'
  'edwareConstants'
  'EdwareApplication'
], ($, React, RB, Constants, App) ->

  PLACEHOLDER_EVENT = Constants.EVENTS.DOWNLOAD_CSV

  CSVDownloader = React.createClass

    handleDownload: () ->
      App.setValue(PLACEHOLDER_EVENT, null)
      @handleBlur()

    # This could look a lot better if we didn't need the arrow offset, and box off to bottom left
    openPopover: (e) ->
      e.preventDefault()
      if !$(".download-text").length
        $('.download-link').popover 'show'
        @handleMouseout()
        windowWidth = $( window ).width()
        elementWidth = $('.download-link').offset().left + $('download-link').width()
        diff = windowWidth - elementWidth
        if diff > 50
          $('.popover').css({'visibility':'visible','margin-left':'-120px','max-width':'100%'})
          $('.popover .arrow').css('left', '85%')
        $('.popover').css({'visibility':'visible'})

        $('.popover').on 'mouseleave', () =>
          @handleBlur()
        $('.csv-link').on 'click', @handleDownload
        $('.csv-link').trigger('focus')
        $('.go-back-from-popover').on 'focus', () =>
          $('.download-link').trigger('focus')
          @handleBlur()
        $('.go-forward-from-popover').on 'focus', () =>
          nextTabbableElement = $('#footer .collapsed .unhide').get(0) or
            $('#footer .show .hide').get(0) or
            $('.prev-arrow').get(0) or
            $('.next-arrow').get(0) or
            $('.slick-header-column span').get(0)

          $(nextTabbableElement).trigger('focus')
          @handleBlur()

    handleMouseout: () ->
      setTimeout (() =>
        if not $(".popover:hover").length and not $('.download-link:hover').length and not $('.csv-link:focus').length
            $('.popover').css('visibility':'hidden')
            $('.popover').css('margin-left', 'auto')
            $('.csv-link').off 'click'
            $('.popover').off 'mouseleave'
            $('.go-back-from-popover').off 'focus'
            $('.go-forward-from-popover').off 'focus'
            $('.download-link').popover 'hide'
        else
            @handleMouseout()
      ), 300

    handleBlur: () ->
      $('.popover').css('visibility':'hidden')
      $('.popover').css('margin-left', 'auto')
      $('.csv-link').off 'click'
      $('.popover').off 'mouseleave'
      $('.go-back-from-popover').off 'focus'
      $('.go-forward-from-popover').off 'focus'
      $('.download-link').popover 'hide'

    render: () ->
      content = '<div class="download-text">Download the current view as a spreadsheet:</div><div class="download-pic-and-link"><a href="#" class="go-back-from-popover"></a><a href="#" class="csv-link">DOWNLOAD SPREADSHEET (CSV)</a><a href="#" class="go-forward-from-popover"></a></div>'
      <RB.Button
        bsStyle="link"
        className="download-link"
        data-animation='true'
        data-html='true'
        data-placement='bottom'
        data-trigger='manual'
        data-content={content}
        data-container='body'
        ref='popover'
        onClick={@openPopover}><span>Download</span></RB.Button>
