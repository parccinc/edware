define [
  'jquery'
  'mustache'
  'edwareConstants'
  'edwareUtil'
  'EdwareApplication'
  'moment'
], ($, Mustache, Constants, utils, App, moment) ->

  class CSVBuilder
    constructor: (@report, @labels) ->

    buildTimestamp: () ->
      current = new Date()
      @timestamp = "#{current.getMonth() + 1}-#{current.getDate()}-#{current.getFullYear()}
        #{current.getHours()}:#{current.getMinutes()}:#{current.getSeconds()}"
      @timestamp

    buildFileNameTimestamp: () ->
      @buildTimestamp().replace(' ', '_').replace(/\:/g, '-')

    buildFileName: () ->
      timestamp = @buildFileNameTimestamp()
      utils.escapeFilename "#{@report.getReportType()}_#{@buildView()}_report_#{timestamp}.csv"

    buildFootnotes: () ->
      records = []
      if @hasFootnote()
        records.push ' '
        records.push @escapeCSV @buildFootnote()
      records

    build: () ->
      records = [] # fixed to 9 rows
      records = records.concat @buildHeader()
      records = records.concat @buildContent()
      records = records.concat @buildFootnotes()
      records.join Constants.DELIMITOR.NEWLINE

    buildColumnRow: () ->
      columns = @report.getColumns()
      @escapeCSV((column.textName || column.name).toUpperCase()) for column in columns when @isExportable(column)

    buildContent: () ->
      rows = []
      summaryProvider = @report.getSummaryProvider()
      dataProvider = @report.getDataProvider()
      columns = @report.getColumns()

      rows.push @buildColumnRow()

      if summaryProvider.length or summaryProvider.getLength
        # get summary specific column settings
        summaryColumns = columns.slice() #copy
        for columnDef, idx in summaryColumns
          summaryColumns[idx] = $.extend {}, columnDef, (columnDef.summary || {})
        for idx in [0...summaryProvider.getLength()]
          summaryRowColumns = @createColumnsForRow(summaryProvider, idx, summaryColumns)
          rows.push @extractContent summaryProvider.getItem(idx), summaryRowColumns

      for idx in [0...dataProvider.getLength()]
        rowColumns = @createColumnsForRow(dataProvider, idx, columns)
        rows.push @extractContent dataProvider.getItem(idx), rowColumns
      rows

    createColumnsForRow: (dataProvider, idx, baseColumns) ->
      columns = []
      if dataProvider.getItemMetadata
        rowMeta = dataProvider.getItemMetadata(idx)
        if not rowMeta or not rowMeta.columns
          return baseColumns

        skipColumns = 0
        for column, colIndex in baseColumns
          if skipColumns > 0
            skipColumns--
            columns.push {skip: true}
            continue

          colspan = 1
          if rowMeta and rowMeta.columns
            columnData = rowMeta.columns[colIndex]
            colspan = (columnData && columnData.colspan) or 1
            if colspan is "*"
                colspan = baseColumns.length - colIndex

          if colspan > 1
            skipColumns = (colspan - 1)
          columns.push $.extend true, {}, (rowMeta.columns[colIndex] or {}), column
        return columns
      else
        return baseColumns

    getStateCode: () ->
      return App.getState().stateCode

    hasFootnote: () ->
      stateCode = @getStateCode()
      return @labels.footnotes[stateCode]?

    buildFootnote: () ->
      stateCode = @getStateCode()
      return @labels.footnotes[stateCode]

    buildHeader: () ->
      records = []
      # build title
      records.push @escapeCSV [@labels.report_name, @buildTitle()]
      records.push @escapeCSV [@labels.date, @buildTimestamp()]
      records.push @escapeCSV [@labels.institution, @buildBreadCrumbs()]
      records.push @escapeCSV [@labels.academic_year, @report.getYear().label]
      records.push @escapeCSV [@labels.subject, @buildSubject()]
      records.push @escapeCSV [@labels.grade_course, @buildGradeCourse()]
      records.push @escapeCSV [@labels.result, @buildResult()]
      records.push @escapeCSV [@labels.view, @buildView()]
      records.push @escapeCSV [@labels.filter_options, @buildFilterOptions()]
      records.push @escapeCSV [@labels.total_count, @report.getReportCount()]
      records.push @escapeCSV [@labels.sort_by, @buildSortBy()]
      for i in [records.length..11] # fix first 16 rows as headers
        records.push ''
      records

    buildSortBy: () ->
      column = @report.getSortColumn()
      if column
        (column.textName || column.name).toUpperCase()
      else
        ''
    buildSubject: () ->
      @report.getSubject().label

    buildGradeCourse: () ->
      @report.getState().gradeCourse

    buildResult: () ->
      @report.getResult()?.label

    buildView: () ->
      view = @report.getView()
      view and view.label

    buildTitle: () ->
      "#{@report.getTitle()} #{utils.capFirst(@report.getReportType())}"

    buildBreadCrumbs: () ->
      (crumb.displayName for crumb in @report.getBreadCrumbs()).join(' > ')

    buildFilterOptions: () ->
      filters = []
      for filterOption in @report.getFilterOptions()
        options = []
        for option in filterOption.options
          options.push option.label
        filters.push "#{filterOption.display}: #{options.join(' or ')}"
      dateRange = @report.getDateRange()
      if dateRange
        filters.push "Date Range: #{moment.utc(dateRange.min, 'X').format('MM/DD/YYYY')} - #{moment.utc(dateRange.max, 'X').format('MM/DD/YYYY')}"

      if filters.length
        filters.join(', ')
      else
        ''

    extractContent: (row, columns) ->

      getColumnValue = (column) ->
        if column.skip
          return ""
        getter = column.rawValue || (column.formatter && (column.formatter.text || column.formatter.render || column.formatter))
        formatterValue = getter and getter(0, 0, row[column.field], column, row)
        if formatterValue? then formatterValue else row[column.field]
      rowValues = (@escapeCSV getColumnValue column for column in columns when @isExportable(column))
      rowValues.join Constants.DELIMITOR.COMMA

    isExportable: (column) ->
        # defaults to true
        not column.hasOwnProperty('exportable') or column.exportable

    # shortcut
    escapeCSV: utils.escapeCSV




  class EdwareDownload

    constructor: () ->
      @blobBuilder = window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder || window.MSBlobBuilder
      @URL = window.URL || window.webkitURL || window.mozURL || window.msURL
      # IE 10 has a handy navigator.msSaveBlob method.
      navigator.saveBlob = navigator.saveBlob || navigator.msSaveBlob || navigator.mozSaveBlob || navigator.webkitSaveBlob
      window.saveAs = window.saveAs || window.webkitSaveAs || window.mozSaveAs || window.msSaveAs

      # Use any available BlobBuilder/URL implementation
      @support = {
        Blob: !!window.Blob
        BlobBuilder: !!@blobBuilder
        saveAs: !!window.saveAs
        saveBlob: !!navigator.saveBlob
        downloadLink: 'download' of document.createElement 'a'
      }

    saveAsURI: (data, name, mimeType) ->
      # Support Chrome, FF, Safari
      url = "data:text/csv;charset=UTF-8,#{encodeURIComponent(data)}"
      if @support.downloadLink
        event = @createMouseEvent()
        link = @createDownloadLink(url, name)
        link.dispatchEvent(event)
      else
        window.open(url, '__parent', '')

    saveAsBlob: (data, name, mimeType) ->
      # Support IE10+
      blob = this.createBlob(data, mimeType)
      if window.saveAs
        window.saveAs blob, name
      else
        navigator.saveBlob blob, name

    saveAsWindow: (data, name, mimeType) ->
      # Support < IE10
      x = window.open()
      x.document.open(mimeType, 'replace')
      x.document.write(data)
      x.document.close()

    create: () ->
      # Blobs and saveAs (or saveBlob)
      if (@support.Blob or @support.BlobBuilder) and
      (@support.saveAs or @support.saveBlob)
        # Support IE 10+
        @saveAsBlob.bind(@)
      else if !/\bMSIE\b/.test(navigator.userAgent)
        # Blobs and object URLs, support webkit and gecko
        # IE does not support URLs longer than 2048 characters (actually bytes), so it is useless for data:-URLs.
        @saveAsURI.bind(@)
      else
        @saveAsWindow.bind(@)

    createBlob: (data, mime) ->
      mimeType = mime or 'application/octet-stream'
      if @support.Blob
        new Blob([data], mimeType)
      else if @support.BlobBuilder
        builder = new @blobBuilder()
        builder.append(data)
        builder.getBlob(mimeType)
      else
        msg = 'your browser doesnt support Blobs'
        alert(msg)

    createDownloadLink: (url, filename) ->
      $('<a>',{
        href: url,
        download: filename
      }).get(0)

    createMouseEvent: () ->
      # dont need to support IE here because IE doesnt support long
      # data uri's
      event = document.createEvent('MouseEvent')
      event.initMouseEvent 'click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null
      event


  builder: CSVBuilder
  downloader: EdwareDownload
