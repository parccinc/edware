define [
  "jquery"
  "bootstrap"
  "react"
  "edwareConstants"
  "EdwareApplication"
], ($, bootstrap,React, Constants, App) ->

  ComparisonOption = React.createClass

    onChange: () ->
      @props.onChange @props.option.value

    render: () ->
      disabled = false
      if @props.option.value is 'parcc' and @props.disable_parcc
        disabled="disabled"
      checked = @props.status is @props.option.value
      <Input type="radio" name="ComparisonOptions" aria-label={@props.option.label} checked={checked} onChange={@onChange} disabled={disabled} ><span aria-hidden="true">{@props.option.label}</span></Input>


  ComparisonGroup = React.createClass

    toggleComparison: (source) ->
      @setState source: source
      App.setState(Constants.EVENTS.COMPARE_SOURCE_CHANGE, source)

    getInitialState: () ->
      if @props.disable_parcc
        default_source = 'state'
      else
        default_source = 'parcc'
      source: App.getState()[Constants.EVENTS.COMPARE_SOURCE_CHANGE] || default_source

    getDefaultProps: () ->
      options: [
        {label: 'PARCC', value: 'parcc'},
        {label: 'STATE', value: 'state'},
      ]

    render: () ->
      options = for option in @props.options
        <ComparisonOption status={@state.source} option={option} onChange={@toggleComparison} disable_parcc={@props.disable_parcc}/>
      <div className="comparisonSource">
        <span>Growth compared to: </span>
        {options}
      </div>


  ComparisonGroup: ComparisonGroup
