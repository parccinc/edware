define [
  'jquery'
  'd3'
  "react"
  'reactbootstrap'
  'edwareConstants'
  'edwareComparisonSource'
  'edwareGrowthPopover'
  'stickies'
], ($, d3, React, RB, Constants, edwareComparisonSource, growthPopover, sticky) ->

  ComparisonGroup = edwareComparisonSource.ComparisonGroup

  CHART_CONFIG =
    STROKE_COLOR: 'rgb(182, 177, 172)'
    HEIGHT: 550
    WIDTH: 550
    BUBBLE_SIZE: [10, 80]

  POPOVER_CONFIG =
    animation: true
    html: true
    trigger: 'hover'
    container: 'body'
    placement: 'top'

  class GrowthChart

    constructor: (@cutPoints, @labels, @summary) ->
      @initialize()
      # draw background
      @drawBackground()
      # draw axis
      @drawAxis()
      # draw labels
      @drawLabels()
      @hiddenBubbles = []

      # draw origin
      @positionOrigin(@summary)
      $(@chart.node().parentNode).sticky('invalidate')

    initialize: () ->
      @chart = d3.select('#bubbleChart')
      @minPoint = d3.min @cutPoints, (d) -> d.start
      @maxPoint = d3.max @cutPoints, (d) -> d.end
      # scaling function for projecting overall scores
      @yScale = d3.scale.linear().domain([@minPoint, @maxPoint]).range([CHART_CONFIG.HEIGHT, 0])
      @bubble = d3.select('#bubbles')
          .attr('width', CHART_CONFIG.WIDTH)
          .attr('height', CHART_CONFIG.HEIGHT)
      @origin = d3.select('#origin-lines')
        .attr('width', CHART_CONFIG.WIDTH)
        .attr('height', CHART_CONFIG.HEIGHT)
      #'.sticky-container'
      $(@chart.node().parentNode).sticky({topMargin: 0})

    drawBackground: () ->
      barOffset = 0
      range = @maxPoint - @minPoint
      backgroundScale = d3.scale.linear().domain([0, range]).range([0, CHART_CONFIG.HEIGHT])
      scaleHeight = (d, i) ->
        backgroundScale(d.end - d.start) + 1

      # Create each section with a fill color, and move it to the correct position
      # Sets the widht and height of each background rectangles and draw a dash line in between them
      g = @chart.selectAll('g')
        .data(@cutPoints)
        .enter()
        .append('g')
        .attr 'fill', (d, i) ->
          d.color
        .attr 'transform', (d, i) ->
          temp = "translate(0," + barOffset + ")"
          barOffset += scaleHeight(d, i)
          temp

      @chart.selectAll('g')
        .append('rect')
        .attr('width', CHART_CONFIG.WIDTH)
        .attr('height', scaleHeight)

      @chart.selectAll('g')
        .append('line')
        .attr('x1', 1)
        .attr('x2', CHART_CONFIG.WIDTH)
        .attr('y1', scaleHeight)
        .attr('y2', scaleHeight)
        .attr('stroke-dasharray', 4)
        .attr('stroke-width', '1')
        .attr('stroke', CHART_CONFIG.STROKE_COLOR)

      # draw top and right boundaries
      boundaries = [
        {
          x1: 0
          y1: 0
          x2: CHART_CONFIG.WIDTH
          y2: 0
        },
        {
          x1: CHART_CONFIG.WIDTH
          y1: 0
          x2: CHART_CONFIG.WIDTH
          y2: CHART_CONFIG.HEIGHT
        }]
      @chart.selectAll('.boundary')
        .data(boundaries)
        .enter()
        .append('line')
        .attr('class', 'boundary')
        .attr 'x1', (d) -> d.x1
        .attr 'y1', (d) -> d.y1
        .attr 'x2', (d) -> d.x2
        .attr 'y2', (d) -> d.y2

    drawAxis: () ->
      ticks = for point in @cutPoints
        point.start
      ticks.reverse()
      ticks.push(@maxPoint)

      # Sets x- and y-axis ranges, scale, and position
      yAxis = d3.svg.axis()
        .orient('left')
        .scale(@yScale)
        .tickValues(ticks)

      @chart.append('g')
        .attr('class', 'y axis')
        .call(yAxis)

      x = d3.scale.linear()
        .domain([0, 100])
        .range([0, CHART_CONFIG.WIDTH])

      xAxis = d3.svg.axis()
        .orient('bottom')
        .scale(x)

      @chart.append('g')
        .attr('class', 'x axis')
        .attr('transform', "translate(0, #{CHART_CONFIG.HEIGHT})")
        .call(xAxis)

    drawLabels: () ->
      # Defines and positions x and y axis labels
      xLabels = d3.select('#xLabels').attr('transform', 'translate(20, 20)')
      xLabels.append('text')
        .attr('x', 180)
        .attr('y', CHART_CONFIG.HEIGHT + 50)
        .text('GROWTH')
        .style('font-weight', 'bold')
      xLabels.append('text')
        .attr('x', 250)
        .attr('y', CHART_CONFIG.HEIGHT + 50)
        .text('(Median Student Growth Percentile)')

      yLabels = d3.select('#yLabels').attr('transform', 'translate(20, 20)')
      yLabels.append('text')
        .attr('transform', 'rotate(-90)')
        .attr('x', -446)
        .text('PERFORMANCE')
        .style('font-weight', 'bold')
      yLabels.append('text')
        .attr('transform', 'rotate(-90)')
        .attr('x', -330)
        .text('(Avg Overall Scale Score)')

    drawIndicators: (bubble, data) ->
      translateX = d3.select(bubble).attr('cx')
      translateY = @yScale(data.overall) #d3.select(bubble).attr('cy')
      d3.select('#xIndicator')
        .attr('transform', "translate(#{(70 + parseInt(translateX) - 15)}, 580)")
        .attr('visibility', 'visible')
      d3.select('#xIndicatorText')
        .text(data.growthCompared)
      d3.select('#yIndicator')
        .attr('transform', "translate(30, #{parseInt(translateY)})")
        .attr('visibility', 'visible')
      d3.select('#yIndicatorText')
        .text(data.overall)

    removeIndicators: () ->
      d3.select('#xIndicator')
        .attr('visibility', 'hidden')
      d3.select('#yIndicator')
        .attr('visibility', 'hidden')

    positionOrigin: (summary) ->
      # draw origin
      self = @
      widthMidpoint = Math.floor(CHART_CONFIG.WIDTH / 2)
      boundaries = [
        {
          x1: 0
          y1: self.yScale(parseInt(summary.score))
          x2: CHART_CONFIG.WIDTH
          y2: self.yScale(parseInt(summary.score))
          rotate: "0"
          textY: self.yScale(parseInt(summary.score)) - 2
          textX: 12
          text: summary.type + ' AVG'
        },
        {
          y1: 0,
          y2: CHART_CONFIG.HEIGHT
          x1: widthMidpoint,
          x2: widthMidpoint,
          rotate: "-90"
          textX: - CHART_CONFIG.HEIGHT + 12
          textY: widthMidpoint - 2
          text: summary.type + ' MEDIAN'

        }
      ]
      @origin.selectAll('.origin')
        .remove()

      axis = @origin.selectAll('.origin')
        .data(boundaries)
      axis
        .enter()
        .append('g')
        .attr('class', 'origin')

      axis.each (d, i) ->
        g = d3.select(this)
        g.append('line')
        .attr('x1', (d) ->
          d.x1
        )
        .attr('x2', (d)->
          d.x2
        )
        .attr('y1', (d) ->
          d.y1
        )
        .attr('y2', (d) ->
          d.y2
        )
        g.append('text')
        .attr('class', 'origin-text')
        .attr('x', (d) ->
          d.textX
        )
        .attr('fill', 'black')
        .attr('y', (d) ->
          d.textY
        )
        .attr('transform', (d) ->
          "rotate(#{d.rotate})"
        )
        .text (d) ->
          d.text

    setBubbleDataSource: (@data, @summary) ->
      # callback for updating the data source - removes old bubbles and then draws new ones
      minSize = d3.min @data, (d) ->
        d.students
      maxSize = d3.max @data, (d) ->
        d.students
      rScale = d3.scale.linear().domain([minSize, maxSize]).range(CHART_CONFIG.BUBBLE_SIZE)
      @scaleRadius = (d, i) =>
        rScale(d.students) || d3.min CHART_CONFIG.BUBBLE_SIZE
      # Clear out all known bubbles from canvas
      @drawBubbles []
      @positionOrigin(@summary)
      # Redraw them based on any in hidden list
      @updateViewableBubbles()

    updateOneBubble: (guid, display) ->
      # callback for updating one bubble
      if not display
        @hiddenBubbles.push guid
      else
        index = @hiddenBubbles.indexOf(guid)
        @hiddenBubbles.splice(index, 1) if index > -1
      @updateViewableBubbles()

    updateAllBubbles: (display) ->
      # callback used to show or hide all bubbles
      @clearHiddenBubbles()
      display?= true
      data = @data
      if not display
        data = []
        # add all bubbles to hidden list
        @hiddenBubbles = @data.map (element) ->
          element.guid
      @updateViewableBubbles data

    clearHiddenBubbles: () ->
      @hiddenBubbles = []

    filterSuppressed: (data) ->
      data.filter (d) ->
        not d.less_than_min_cell_size

    drawBubbles: (data) ->
      bubbles = @bubble.selectAll('.bubble')
      self = @
      update = bubbles.data @filterSuppressed(data), (d) ->
        d.guid
      update
        .exit()
        .each (d, i) ->
          self.removePopover(@, d)
        .remove()
      update
        .enter().append('circle')
        .attr('class', 'bubble')
        .attr 'cx', (d, i) ->
          d.growthCompared / 100 * CHART_CONFIG.WIDTH
        .attr 'cy', (d, i) =>
          @yScale(d.overall)
        .attr 'r', (d, i) =>
          radius = @scaleRadius(d, i)
          radius
        .each (d, i) ->
          d.domPos = i
          self.initializePopover this, d
        .on 'mouseover', (d, i) ->
          # TODO: handle clicks?
          d3.select(this).classed('activeBubble', true)
          $(this).popover('show')
          self.drawIndicators(@, d)
        .on 'mouseout', (d, i) ->
          d3.select(this).classed('activeBubble', false)
          $(this).popover('hide')
          self.removeIndicators()

    removePopover: (element, data) ->
      $(element).popover 'hide'
      @removeIndicators()

    initializePopover: (element, data) ->
      $(element).popover $.extend {}, POPOVER_CONFIG, content: React.renderToStaticMarkup growthPopover.createContent data, @labels

    updateViewableBubbles: (data) ->
      if not data
        data = @data.filter (element) =>
          # Filter all bubbles that are not hidden
          @hiddenBubbles.indexOf(element.guid) is -1
      @drawBubbles data

    activateBubble: (guid) ->
      self = @
      @bubble.selectAll('.bubble').filter (d, i) ->
        d.guid is guid
      .each (d, i) ->
        $this = $(this)
        $this.popover('show')
        # move to front
        $this.parent().append(this)
        # draw indicator
        self.drawIndicators(@, d)
      .classed('activeBubble', true)

    deactivateBubble: (guid) ->
      self = @
      @bubble.selectAll('.bubble').filter (d, i) ->
        d.guid is guid
      .each (d, i) ->
        $this = $(this)
        $this.popover('hide')
        # move to back
        $this.parent().prepend(this)
        self.removeIndicators()
      .classed('activeBubble', false)

  create = (config) ->
    cutPoints = config.cutPoints
    labels = config.labels || {}
    summary = config.summary || {}
    container = config.container || '#comparisonSource'
    onSourceSelect = config.onSourceSelect || () ->
    React.render(
      <ComparisonGroup disable_parcc={config.disable_parcc} callback={onSourceSelect} />,
      $(container)[0]
    )
    new GrowthChart(cutPoints, labels, summary)


  create: create
