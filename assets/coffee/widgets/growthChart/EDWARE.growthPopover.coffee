define [
  "jquery"
  "react"
], ($, React, edwarePopulationBar) ->

  GrowthPopover = React.createClass
    greyOut: () ->
      if @props.data.has_filters
        <div className="grayOut"></div>

    render: () ->
      <div>
        <span className="name-value-pairs-title">{@props.data.name}</span>
        <ul className="name-value-pairs bold-values">
          <li className="name-value-pair">
            <div style={position:'relative'}>
              <span className="name">{@props.labels.students}</span>
              <span className="value">{@props.data.students || '-'}</span>
              {@greyOut()}
            </div>
          </li>
          <li className="name-value-pair">
            <span className="name">{@props.labels.avg_score}</span>
            <span className="value">{@props.data.overall}</span>
          </li>
        </ul>
        <span className="name-value-pairs-title sub-title growth-percentile">{@props.labels.student_growth_percentile}</span>
        <ul className="name-value-pairs bold-values">
          <li className="name-value-pair">
            <span className="name">{@props.labels.compared_to_parcc}</span>
            <span className="value">{@props.data.growth.parcc} %tile</span>
          </li>
          <li className="name-value-pair">
            <span className="name">{@props.labels.compared_to_state}</span>
            <span className="value">{@props.data.growth.state} %tile</span>
          </li>
        </ul>
      </div>

  create = (data, labels={}) ->
      <GrowthPopover data={data} labels={labels} />

  createContent:create
