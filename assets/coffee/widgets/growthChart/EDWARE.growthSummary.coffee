define [
  "jquery"
  "react"
  "edwarePopulationBar"
], ($, React, edwarePopulationBar) ->

  GrowthSummaryTitleElement = React.createClass
    render: () ->
      <span className='name-value-pairs-title'>{@props.entityName}</span>

  GrowthSummaryDataElement = React.createClass
    grayOut: () ->
      <div className="grayOut"></div> if @props.grayout
    render: () ->
      <li className='growthSummaryItem name-value-pair' style={position: 'relative'}>
        <span className='name'>{@props.name}</span>
        <span className='value'>{@props.value}</span>
        {@grayOut()}
      </li>

  GrowthSummaryPopulationBarElement = React.createClass
    createPopulationBar: () ->
      edwarePopulationBar.create @props.value

    render: () ->
      populationBarHtml = edwarePopulationBar.create @props.value
      <li className='growthSummaryItem name-value-pair'>
        <span className='name'>{@props.name}</span>
        <span className='value growthSummaryPopBar' dangerouslySetInnerHTML={{__html: populationBarHtml}}/>
      </li>

  GrowthSummarySection = React.createClass
    createSummaryElements: () ->
      elements = [<GrowthSummaryDataElement name={@props.labels.students} value={@props.data.students || '-'} grayout={@props.data.has_filters} />]
      if @props.data.is_less_than_min_cell_size
        elements.push(
          <li className="name-value-pair">
            <div className='grayBar'></div>
          </li>
        )
      else
        elements.push <GrowthSummaryDataElement name={@props.labels.avg_score} value={@props.data.score}/>,
          <GrowthSummaryPopulationBarElement name={@props.labels.distribution} value={'performanceLevels': @props.data.performanceLevels}/>
      elements

    render: () ->
      <div>
        <GrowthSummaryTitleElement entityName={@props.data.name}/>
        <ul className='name-value-pairs bold-values'>
          {@createSummaryElements()}
        </ul>
      </div>

  create = (data, labels) ->
    React.render(
      <GrowthSummarySection data={data} labels={labels} />,
      $('#growthSummary')[0]
    )

  create:create
