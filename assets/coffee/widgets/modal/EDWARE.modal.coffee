define ["jquery", "bootstrap"], ($, bootstrap) ->

  lastFocus = undefined

  $.fn.edwareModal = (options) ->
    # flag to indicate whether current modal resets last focused
    # element in the page
    resetLastFocus = true unless options?.keepLastFocus
    # bind events when initialize
    if not options or typeof(options) is 'object'
      # remember last focus
      this.one 'show', ->
        lastFocus = document.activeElement if resetLastFocus

      this.one 'shown', ->
        tabStart = $('[data-tab-start]', $(this))
        tabEnd = $('[data-tab-end]', $(this))
        tabStart.trigger('focus')
        self = this
        $('a, button', $(this)).on 'blur.modalKeyboardNav', () ->
          if $(this).closest('.btn-group').andSelf().length
            setTimeout (() ->
              tabEnd.trigger('focus') if not $.contains self, document.activeElement
            ), 0


      # restore last focus
      this.on 'hidden', ->
        lastFocus.focus() if lastFocus
        $('a, button', $(this)).off 'blur.modalKeyboardNav'
    this.modal options
