define [
  'jquery'
  'react'
  'edwareClientStorage'
  'edwareConstants',
  'EdwareApplication'
  'selectorUtil'
], ($, React, state, Constants, App, selectorUtil) ->

  class ViewSelectorUtils

    @availableForReport: (item, report) ->
      selectorUtil.availableForReport(item, report)

    @buildViewsForReport: (viewsList, report, options = {}) ->
      selectorUtil.buildOptionsForReport(viewsList, report, options)

    @flattenViews: (viewsList) ->
      selectorUtil.flattenOptions(viewsList)

    @getForCurrentState: (reportState, views) ->
      selectorUtil.getForCurrentState(reportState, views)

    @setViewState: (view) ->
      # set view on body element
      $('body').attr('data-view', view)


    @isCurrentState: (reportState, view) ->
      selectorUtil.isCurrentState reportState, view


  ViewElement = React.createClass

    handleClick: (e) ->
      if not @props.selected
        @props.onViewSelect(@props.view)

    componentDidMount: () ->
      if @props.hoverInfo
        $(@getDOMNode()).find('.info').popover()

    componentWillUnmount: () ->
      if @props.hoverInfo
        $(@getDOMNode()).find('.info').popover('destroy')

    showPopover: () ->
      if @props.hoverInfo and not @popoverVisible
        $(@getDOMNode()).find('.info').popover('show')
        @popoverVisible = true

        hoverCheck = () =>
          setTimeout((() =>
            if not $(".popover:hover").length and not $(@getDOMNode()).is(":hover")
              $(@getDOMNode()).find('.info').popover('hide')
              @popoverVisible = false
            else
              hoverCheck()
          ), 15)
        hoverCheck()

    render: () ->
      classes = [@props.view.cssclass or '']
      classes = classes.concat(['btn', 'btn-default'])
      label = "#{@props.view.label} view"
      if @props.selected
        classes.push 'selected'
        label = "#{label}, selected"
      popover = null
      popoverAttr = {}
      if @props.hoverInfo
        popover = <span className="info"
          data-animation={true}
          data-html={true}
          data-placement='top'
          data-content={@props.hoverInfo}
          data-container='body'
          data-original-title=''
          title=''></span>
        popoverAttr = {
          onMouseEnter: @showPopover
        }
      <button
        aria-label={label}
        onClick={@handleClick}
        className={classes.join(' ')}
        {...popoverAttr}>
        {@props.view.label}
        {popover}
      </button>


  ViewSelector = React.createClass

    componentDidMount: () ->
      App.subscribe(@onChange)
      ViewSelectorUtils.setViewState(App.getState().view)

    componentWillUnmount: () ->
      App.unsubscribe(@onChange)

    getInitialState: () ->
      reportState = selectorUtil.currentReportState(App.getState())
      {
        report: reportState.report
        options: @getOptions(reportState)
      }

    onChange: (newVal, oldVal, changes, evntObj) ->
      if evntObj and evntObj.after
        reportState = @getReportState()
        @setState({options: @getOptions(reportState)})
        ViewSelectorUtils.setViewState(reportState.view)

    onViewSelect: (view) ->
      App.setState(view.states or {})

    getReportState: () ->
      selectorUtil.currentReportState(App.getState())

    getOptions: (forReportState) ->
      ViewSelectorUtils.buildViewsForReport(@props.data, forReportState)

    createButtonGroup: (group) ->
        <ButtonGroup>{group}</ButtonGroup>

    createButton: (view, index, reportState) ->
      <ViewElement
        name={view.label}
        view={view}
        onViewSelect={@onViewSelect}
        key={view.label}
        hoverInfo={view.hoverInfo}
        selected={ViewSelectorUtils.isCurrentState(reportState, view.states)} />

    render: () ->
      onViewSelect = @onViewSelect
      options = ViewSelectorUtils.flattenViews(@state.options)
      reportState = @getReportState()
      ###
      # 4 + 1 Diagnostics/Math we need grouped buttons
      ###
      buttonsForGroups = {}
      for option, index in options
        group = option.group or 0
        buttonsForGroups[group] ?= []
        buttonsForGroups[group].push @createButton(option, index, reportState)

      if options and options.length
        groups = (@createButtonGroup(group) for groupName, group of buttonsForGroups)
        <div>
            <label aria-hidden="true" id="viewSelectorLabel" className="category view-bar-label">View</label>
            {groups}
        </div>
      else
       false

  create = (views, onSelect) ->
    onSelect = onSelect || () ->
    React.render(
      <ViewSelector data={views}/>,
      $('#viewSelector')[0]
    )

  create:create
  ViewSelector:ViewSelector
  Utils: ViewSelectorUtils
