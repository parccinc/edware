define [
  'jquery'
], ($) ->
   
  #
  #    * Loading mask widget
  #    
  $.fn.loader = (message) ->
    message = message || "Loading..."
    this.addClass("loader").html("<div class='message'>" + message + "</div>").appendTo "body"

  create = (opts) ->
    context = opts.context or "<div></div>"
    $(context).loader opts.message

    # Add the class to the body in order to improve the performance of Selenium tests which wait for the loading mask to
    # disappear.
    $('body').addClass('body-loader')

  destroy = ->
    $('body').removeClass('body-loader')
    $('.loader').remove()
        
  create: create
  destroy: destroy
