define [
  'jquery'
  'react'
  'reactbootstrap'
  'edwareConstants'
  'EdwareApplication'
  'edwareUtil'
], ($, React, RB, Constants, App, Utils) ->

  ###
  # synthetic event used to singal filter changes
  ###
  FILTER_EVENT_PLACEHOLDER = App.Constants.EVENTS.FILTER_CHANGE

  ###
  # Static utils used for filters
  ###
  class FilterUtil
    @triggerFilterChange: () ->
      App.setValue(FILTER_EVENT_PLACEHOLDER, null)

    @filterKeys: Object.keys(App.Constants.FILTER_TYPES).map (filterType) ->
      App.Constants.FILTER_TYPES[filterType]

    @serialize: (filters) ->
      serialized = {}
      for key, filterGroup of filters
        groupActiveFilters = Object.keys(filterGroup).filter (key) ->
          filterGroup[key] == true
        serialized[key] = if groupActiveFilters.length then groupActiveFilters else null
      serialized

    @deserialize: (filters) ->
      deserialized = {}
      values = @ensureArray(filters)
      for value in values
        deserialized[value] = true
      deserialized

    @getActiveFilterCrumbs: (data) ->
      crumbs = []
      activeFilters = @getActiveFilters()
      for filter in data
        groupName = filter.name
        if activeFilters[groupName]
          filterGroup = activeFilters[groupName]
          activeOnly = filter.options.filter (option) ->
            filterGroup[option.value]
          if activeOnly and activeOnly.length
            crumbs.push {
              display: filter.display
              group: groupName
              options: activeOnly
            }
      crumbs
    @hasActiveFilters: () ->
      for filter of @getActiveFilters()
        for option of filter
          # we can break out on the first
          # because this is deserialized and only has true values...
          return true
      return false

    @clearState: () ->
      stateMask = {}
      activeFilterGroups = @getActiveFilters()
      $.each(@getFilterKeys(), (index, group) ->
        stateMask[group] = null
      )
      App.setState(stateMask)
      @triggerFilterChange()

    @clearFilter: (groupName) ->
      App.setState(groupName, null)
      @triggerFilterChange()

    @setFilters: (activeMap) ->
      App.setState(@serialize(activeMap))
      @triggerFilterChange()

    @getActiveFilters: () ->
      state = App.getState()
      activeFilters = {}
      for filterType in @getFilterKeys()
        if state[filterType]
          activeFilters[filterType] = @deserialize(state[filterType])
      activeFilters

    @getFilterKeys: () ->
      @filterKeys

    @ensureArray: (args) ->
      Utils.ensureArray(args)

  ###
  # Dropdown with multi-selections
  ###
  FilterMultiSelect = React.createClass

    componentDidMount: () ->
      domNode = @getDOMNode()
      $('input[type="checkbox"]:last, button', $(domNode)).on 'blur.filterMultiSelect', () ->
        setTimeout (() -> $(domNode).find('.btn-group').andSelf().removeClass('open') if not $.contains domNode, document.activeElement), 0

    componentWillUnmount: () ->
      domNode = @getDOMNode()
      $('input[type="checkbox"]:last, button', $(domNode)).off 'blur.filterMultiSelect'

    handleClick: (group, item, shouldSet) ->
      # workaround to keep dropdown open
      $(@refs.container.getDOMNode()).addClass('open')
      @props.onChange  group, item, shouldSet

    render: () ->
      onSelect = @handleClick
      state = @props.optionState
      tabIndex = if @props.isOpen then 0 else -1
      options = @props.options.map (option, index) =>
        <MultiSelectOption
          isChecked={state[option.value]}
          eventKey={index}
          onSelect={onSelect}
          name={option.label}
          value={option.value}
          tabIndex={tabIndex}
          group={@props.group}></MultiSelectOption>

      <div className="btn-group" ref="container">
        <Button className="dropdown-toggle btn btn-default" type="button" id="dropdown_#{@props.group}" data-toggle="dropdown" tabIndex={tabIndex}>
          <span className="caret"></span>
          <span>{@props.description}</span>
        </Button>
        <ul className="dropdown-menu" role="menu" aria-labeldby="dropwdown_#{@props.group}">
          {options}
        </ul>
      </div>

  ###
  # dropdown-item part of a multi-select
  ###
  MultiSelectOption = React.createClass
    onChange: (event) ->
      @props.onSelect @props.group, @props.value, !@props.isChecked
      event.stopPropagation()
    render: () ->
      id = @props.id
      label = @props.name
      voId = @props.name.replace(/\s+/g, '')
      <li role="presentation" eventKey={@props.eventKey}>
        <a role="presentation" tabIndex="-1">
          <label className="checkbox">
            <input aria-labelledby={"#{voId}FilterLabel#{@props.eventKey}"} role="menuitemcheckbox" type="checkbox" onChange={@onChange} checked={@props.isChecked} />
            <span aria-hidden="true" id={"#{voId}FilterLabel#{@props.eventKey}"}>{label}</span>
          </label>
        </a>
      </li>

  ###
  # bar with currently active filters
  ###
  FilterCrumb = React.createClass
    handleClick: () ->
      FilterUtil.clearFilter(@props.groupname)

    render: () ->
      clickHandler = @handleClick
      tag = @props.display

      itemsString = @props.appliedFilters.map (item, index) ->
        ret = []
        if index > 0
          ret.push ' or '
        ret.push <span className='filter'>{item.label}</span>
        ret

      <RB.Button className="filter-crumb" onClick={clickHandler}>
        {tag}: {itemsString}
      </RB.Button>

  ###
  # Filter Widget
  ###
  FilterBar = React.createClass

    getActiveFilters: () ->
      FilterUtil.getActiveFilters()

    componentDidMount: () ->
      App.subscribe(FILTER_EVENT_PLACEHOLDER, @onChange)

    componentWillUnmount: () ->
      App.unsubscribe(FILTER_EVENT_PLACEHOLDER, @onChange)

    getCleanState: () ->
      activeFilters: @getActiveFilters()
      hasChanges: false

    resetSelections: () ->
      @setState(@getCleanState())

    getInitialState: () ->
      @getCleanState()

    onChange: () ->
      @setState(@getCleanState())

    hasChanges: () ->
      @state.hasChanges

    requestClose: () ->
      @resetSelections()
      @props.expandCollapse(false)

    requestOpen: () ->
      @props.expandCollapse(true)

    addFilter: (group, item, shouldSet) ->
      filters = @state.activeFilters
      newState = {}
      filters[group] ?= {}
      filters[group][item] = shouldSet
      newState.activeFilters = filters
      newState.hasChanges = true
      @setState(newState)

    handleCancelPending: () ->
      @requestClose()

    handleApplyPending: () ->
      if !@hasChanges()
        return
      FilterUtil.setFilters(@state.activeFilters)
      @requestClose()
    handleClearAll: () ->
      FilterUtil.clearState()

    createFilterCrumbs: () ->
      FilterUtil.getActiveFilterCrumbs(@props.data).map (crumb) ->
        <FilterCrumb key={crumb.group} groupname={crumb.group} appliedFilters={crumb.options} display={crumb.display}></FilterCrumb>

    createDropdowns: (isOpen) ->
      @props.data.map (filter) =>
        <FilterMultiSelect
          options={filter.options}
          optionState={@state.activeFilters[filter.name] || {}}
          description={filter.display}
          isOpen={isOpen}
          group={filter.name}
          onChange={@addFilter}></FilterMultiSelect>

    createEditor: (isOpen) ->
      tabIndex = if isOpen then 0 else -1
      applyBtnTabIndex = if @hasChanges() then tabIndex else -1
      <div className="filter-editor" aria-hidden={not isOpen}>
        <div className="animating-spacer">
          <div className="filters-header">Student Filters</div>
          <div className="selectors">
            {@createDropdowns(isOpen)}
          </div>
          <div className="text-center">
            <RB.Button
              bsStyle='link'
              onClick={@handleCancelPending}
              className="cancel-link"
              tabIndex={tabIndex}>Cancel</RB.Button>
            <RB.Button
              bsStyle='default'
              type="button"
              onClick={@handleApplyPending}
              tabIndex={applyBtnTabIndex}
              className="btn apply-button">Apply Filters</RB.Button>
          </div>
        </div>
      </div>

    createCrumbBar: () ->
      if !FilterUtil.hasActiveFilters()
        return
      <div className="crumb-bar">
        {@createFilterCrumbs()}
        <RB.Button bsStyle="link" className="clear-all-filters" onClick={@handleClearAll}>clear all</RB.Button>
        <RB.Button bsStyle="link" className="edit-filters" onClick={@requestOpen}>edit</RB.Button>
      </div>

    render: () ->
      barClasses = ['filter-bar']
      barClasses.push 'expanded' if @props.expanded
      barClasses.push 'pending-apply' if @hasChanges()

      <div className={barClasses.join(' ')}>
        {@createEditor(@props.expanded)}
        {@createCrumbBar()}
      </div>

  create = (data, defaultExpanded, expandCollapse) ->
    React.render(
      <FilterBar data={data} expanded={defaultExpanded} expandCollapse={expandCollepse}/>,
      $('#subjectSelector')[0]
    )

  create:create
  FilterBar:FilterBar
  FilterUtil: FilterUtil
