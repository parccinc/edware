define [
  'jquery'
  'react'
  'edwareUtil'
  'edwareConstants'
  'EdwareApplication'
] , ($, React, edwareUtil, Constants, App) ->

  TYPES = Constants.BREADCRUMB_TYPES

  class BreadCrumbUtil

    @getCurrentPath: (contextData, configs, labels) ->
      elements = []
      for element, i in contextData
        staticElement = configs[i]
        # sets the url link and returns the current query parameters
        currentParams = @setUrlLink currentParams, element, staticElement
        elements.push @formatName element, labels
      if elements.length > 0
        elements[elements.length - 1].link = '#'
      elements

    @formatName: (element, labels) ->
      type = element.type
      name = element.name
      displayName = name
      if type is TYPES.HOME
        displayName = labels.breadcrumb_home
      element.displayName = displayName
      element

    @setUrlLink: (currentParams, element, staticElement) ->
      # Appends the current set of query parameters to build breadcrumb link
      # Sets element.link used for individual breadcrumb links
      # currentParams keeps track of existing query parameters for the rest of the breadcrumb trail
      currentParams ?= ''
      element.ignoreParams = staticElement.ignoreParams
      if element.id
        params = staticElement.queryParam + "=" + element.id
        if currentParams.length is 0
          currentParams = params
        else
          currentParams = currentParams + "&" + params
        element.link = staticElement.link + "?" + currentParams
      else if staticElement.link
        element.link = staticElement.link
      currentParams

  # Individual Element that makes up a series of breadcrumbs
  EdwareBreadcrumbElement = React.createClass {

    updateLink: () ->
      if not @props.isLastElement
        currentUrl = $(@refs.anchor.getDOMNode()).attr('href')
        newUrl = edwareUtil.generateUrl currentUrl, {}, App.getState(), [], @props.ignoreParams
        $(@refs.anchor.getDOMNode()).attr('href', newUrl)

    createCrumbContent: () ->
      if @props.isLastElement
        <a>{@props.displayName}</a>
      else [
        <a
          href={@props.link}
          aria-label={"breadcrumb #{@props.displayName}"}
          ref="anchor"
          onClick={@updateLink}
          onContextMenu={@updateLink}
        >{@props.displayName}</a>,
        <span aria-hidden="true" role="presentation" className="divider" ref="divider">></span>
      ]

    render: () ->
      className = if not @props.isLastElement then 'link' else ''
      <li className={className}>
        {@createCrumbContent()}
      </li>
  }

  # A series of breadcrumbs
  EdwareBreadcrumbList = React.createClass {
    getCurrentPath: ->
      BreadCrumbUtil.getCurrentPath @props.contextData['items'], @props.configs['items'], @props.labels

    createBreadCrumbElement: (nodeInfo, key, isLast) ->
      <EdwareBreadcrumbElement
        displayName={nodeInfo.displayName}
        link={nodeInfo.link}
        key={key}
        isLastElement={isLast}
        ignoreParams={nodeInfo.ignoreParams}>
      </EdwareBreadcrumbElement>

    render: () ->
      nodes = @getCurrentPath()
      lastElement = nodes.pop()
      elementNodes = nodes.map (nodeInfo, index) =>
        @createBreadCrumbElement(nodeInfo, index, false)
      elementNodes.push @createBreadCrumbElement lastElement, 'last', true

      <ul className="breadcrumb">
        {elementNodes}
      </ul>
  }

  create = (containerId, contextData, configs, displayHome, labels) ->
    React.render(
      <EdwareBreadcrumbList
        contextData={contextData}
        configs={configs}
        displayHome={displayHome}
        labels={labels}/>,
      $(containerId)[0]
    )
  {
    create: create
    EdwareBreadcrumbList: EdwareBreadcrumbList
    Utils: BreadCrumbUtil
  }
