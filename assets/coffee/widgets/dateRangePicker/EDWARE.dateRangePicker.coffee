define [
  'jquery'
  'react'
  'reactbootstrap'
  'edwareConstants'
  'EdwareApplication'
  'moment'
  'moment-range'
  'daterangepicker'
], ($, React, RB, Constants, App, moment, DateRange, daterangepicker) ->

  EVENTS = Constants.EVENTS

  EdwareDateRangePicker = React.createClass

    componentDidMount: () ->
      rangeAndSelection = @getRangeAndSelection()
      $('.date-range-picker').daterangepicker {
        opens: 'center',
        timeZone: 0,
        startDate: rangeAndSelection.startDate,
        endDate: rangeAndSelection.endDate,
        minDate: rangeAndSelection.minDate,
        maxDate: rangeAndSelection.maxDate,
        locale: {
          fromLabel: 'From:',
          toLabel: 'To: '
        }
      }, @onChange
      @plugin = $('.date-range-picker').data('daterangepicker')
      @bindKeyboardNavigationEvents()


    componentWillUnmount: () ->
      App.setValue({asmt_date: null})
      @unbindKeyboardNavigationEvents()

    bindKeyboardNavigationEvents: () ->
      self = @
      $('.daterangepicker label').attr('aria-hidden', true)
      $('.daterangepicker input[name="daterangepicker_start"]').attr('aria-label', 'From')
      $('.daterangepicker input[name="daterangepicker_end"]').attr('aria-label', 'To')
      $('.date-range-picker').on 'show.daterangepicker', (ev, picker) ->
        $('.daterangepicker_start_input input').trigger('focus')
        $('.daterangepicker').prepend('<a href="#" class="go-back-from-popover"></a>')
        $('.daterangepicker').append('<a href="#" class="go-forward-from-popover"></a>')

        $('.daterangepicker .go-back-from-popover').on 'focus', () ->
          $('.date-range-picker').data().daterangepicker.hide()
          $('.rangebox').trigger('focus')

        $('.daterangepicker .go-forward-from-popover').on 'focus', () ->
          $('.date-range-picker').data().daterangepicker.hide()
          $('.filters-link').trigger('focus')

      $('.date-range-picker').on 'hide.daterangepicker', (ev, picker) ->
        $(@).find('button').trigger('focus')
        $('.daterangepicker .go-back-from-popover').off 'focus'
        $('.daterangepicker .go-forward-from-popover').off 'focus'
        $('.daterangepicker .go-back-from-popover').remove()
        $('.daterangepicker .go-forward-from-popover').remove()

    unbindKeyboardNavigationEvents: () ->
      $('.date-range-picker').off 'show.daterangepicker'
      $('.date-range-picker').off 'hide.daterangepicker'

    onChange: (start, end, label) ->
      App.setValue({
        asmt_date: {
          min: start.utc().unix()
          max: end.utc().unix()
        }
      })

    isDefaultRange: (range) ->
      defaultRange = @getDefaultRange()
      range.startDate is defaultRange.startDate and range.endDate is defaultRange.endDate

    setRangeToDefault: () ->
      defaultState = App.getState().availableDateRange
      @plugin.setStartDate(defaultState.min)
      @plugin.setEndDate(defaultState.max)
      @plugin.setOptions({
        minDate: defaultState.min,
        maxDate: defaultState.max
      })
      App.setValue({asmt_date: null})

    getRangeAndSelection: () ->
      state = App.getState()
      maxAvailableRange = state.availableDateRange

      if not maxAvailableRange.min
        maxAvailableRange.min = moment.utc().unix()

      if not maxAvailableRange.max
        maxAvailableRange.min = moment.utc().unix()

      selectedRange = $.extend true, {}, state.asmt_date

      if not selectedRange.min or selectedRange.min < maxAvailableRange.min
        selectedRange.min = maxAvailableRange.min

      if not selectedRange.max or selectedRange.max > maxAvailableRange
        selectedRange.max = maxAvailableRange.max


      startDate = moment.utc(selectedRange.min, 'X').format('MM/DD/YYYY')
      endDate = moment.utc(selectedRange.max, 'X').format('MM/DD/YYYY')
      minDate = moment.utc(maxAvailableRange.min, 'X').format('MM/DD/YYYY')
      maxDate = moment.utc(maxAvailableRange.max, 'X').format('MM/DD/YYYY')

      {
        startDate: startDate,
        endDate: endDate,
        minDate: minDate,
        maxDate: maxDate
      }



    getRange: () ->
      state = @getRangeAndSelection()
      @plugin?.setOptions {
        opens: 'center',
        timeZone: 0,
        startDate: state.startDate,
        endDate: state.endDate,
        minDate: state.minDate,
        maxDate: state.maxDate,
        locale: {
          fromLabel: 'From:',
          toLabel: 'To: '
        }
      },
      @onChange
      state

    getDefaultRange: () ->
      defualtRangeAndSelection = @getRangeAndSelection()
      defualtRangeAndSelection.startDate = defualtRangeAndSelection.minDate
      defualtRangeAndSelection.endDate = defualtRangeAndSelection.maxDate
      defualtRangeAndSelection

    renderResetButton: () ->
      if not @isDefaultRange(@getRangeAndSelection())
        <button onClick={@setRangeToDefault} className="btn btn-link reset-range" type="button">RESET DATES</button>
      else
        null

    render: () ->
      range = @getRange()
      <div className="date-selector">
        <div className="date-range-picker">
          <span aria-hidden="true">Dates:&nbsp;</span><button aria-label={"Currently selected date range is #{range.startDate} - #{range.endDate}"} className="rangebox">{"#{range.startDate} - #{range.endDate}"}</button>
        </div>
        {@renderResetButton()}
      </div>

  create = (data) ->
    React.render(
      <EdwareDateRangePicker data={data}/>,
      $('#dateRangePicker')[0]
    )

  create: create
  EdwareDateRangePicker: EdwareDateRangePicker
