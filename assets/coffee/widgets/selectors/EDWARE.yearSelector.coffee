define [
  'jquery'
  'react'
  'dropdownMixin'
  'edwareConstants'
  'EdwareApplication'
], ($, React, dropdownMixin, Constants, App) ->


  class YearSelectorUtils
    @getAcademicYears: =>
      today = new Date
      currentYear = today.getFullYear()
      start = Math.max(2014, currentYear - 7)
      cutoff = new Date today.getFullYear(), 7, 1, 0, 0, 0
      years = []
      if today >= cutoff
        currentYear++
      [start..currentYear].map((year) -> { label : "#{year - 1} - #{year}", value : year}).reverse()

    @getYearObject: (forYear) =>
      forYearValue = parseInt(forYear)
      academicYears = @getAcademicYears()
      for year in academicYears
        if(year.value is forYearValue)
          return year

    @getDefaultSelectedYear: () =>
      years = @getAcademicYears()
      years[0]

  YearsDropdown = React.createClass

    mixins: [dropdownMixin]

    componentDidMount: () ->
      @getApp().subscribe(Constants.EVENTS.YEAR_CHANGE, @onChange)

    componentWillUnmount: () ->
      @getApp().unsubscribe(Constants.EVENTS.YEAR_CHANGE, @onChange)

    getAcademicYears: () ->
      YearSelectorUtils.getAcademicYears()

    getYear: (value)->
      YearSelectorUtils.getYearObject value

    onChange: () ->
      year = @getApp().getState().year
      @setState({year: @getYear(year)})
      $(@getDOMNode()).find('button').trigger('focus')

    updateYearState: (value) ->
      year = @getAcademicYears()[value]
      @getApp().setState('year', year.value)

    onYearSelect: (value, e) ->
      e.preventDefault()
      @updateYearState(value)

    getInitialState: () ->
      year = @getApp().getState().year
      if year?
        {year: @getYear(year)}
      else
        year = YearSelectorUtils.getDefaultSelectedYear()
        @getApp().setValue({ year : year.value })
        { year : year }

    getApp: () ->
      @props.app or App

    getTitle: () ->
      if @props.labelInside
        <span className="selected-label">Academic Year:&nbsp;{@state.year.label}</span>
      else
        <span className="selected-label">{@state.year.label}</span>

    renderMenuItem: () ->
      @getAcademicYears().map((year, index) =>
        <li role="presentation">
          <a href="#" onClick={@onYearSelect.bind(this, index)} role="menuitem">{year.label}</a>
        </li>
      )

    renderBtnGroup: () ->
      <div className="btn-group">
         <button aria-labelledby="academicYearLabel" name="academicYearMenu" data-toggle="dropdown" className="dropdown-toggle btn btn-default">
          {@getTitle()}
          <span className="caret"></span>
        </button>
        <ul className="dropdown-menu" role="menu">
          {@renderMenuItem()}
        </ul>
      </div>

    render: () ->
      if @props.labelInside
        @renderBtnGroup()
      else
        <div>
          <label aria-hidden="true" id="academicYearLabel" for="academicYearMenu" className="year-label">Academic Year</label>
          {@renderBtnGroup()}
        </div>

  create = () ->
    React.render(
      <YearsDropdown  />,
      $('#yearSelector')[0]
  )

  create:create
  YearsDropdown:YearsDropdown
  Utils: YearSelectorUtils
