define [
  'jquery'
  'react'
  'reactbootstrap'
  'edwareConstants'
  'EdwareApplication'
  'edwareUtil'
  'selectorUtil'
  'edwareContextSecurity'
  'dropdownMixin'
], ($, React, RB, Constants, App, util, selectorUtil, ContextSecurity, dropdownMixin) ->

  capFirst = util.capFirst
  EVENTS = Constants.EVENTS

  class ResultSelectorUtil
    @availableForReport: (item, report) ->
      selectorUtil.availableForReport(item, report)

    @buildResultsForReport: (resultsList, report, options = {}) ->
      selectorUtil.buildOptionsForReport(resultsList, report, options)

    @flattenResults: (resultsList) ->
      selectorUtil.flattenOptions(resultsList)

    @getForCurrentState: (reportState, options) ->
      selectorUtil.getForCurrentState(reportState, options)

    @setViewState: (asmtType) ->
      # set view on body element
      $('body').attr('data-asmtType', asmtType)

  ResultsDropdown = React.createClass

    mixins: [dropdownMixin]

    componentDidMount: () ->
      App.subscribe(@onChange)
      App.subscribe(EVENTS.VIEW_CHANGE, @onViewChange)
      ResultSelectorUtil.setViewState(App.getState().asmtType)

    componentWillUnmount: () ->
      App.unsubscribe(@onChange)

    getInitialState: () ->
      reportState = selectorUtil.currentReportState(App.getState())
      contextSecurity = ContextSecurity.get()
      reportState.piiAll = contextSecurity and contextSecurity.canAccessPII()
      {
        # TODO: report should be passed in directly from the report module
        report: reportState.report
        options: @getOptions(reportState)
      }

    getResult: (value) ->
      state  = App.getState()
      state.report = @props.data.report
      ResultSelectorUtil.getForCurrentState(state, @props.data)

    getOptions: (forReportState) ->
      options = ResultSelectorUtil.buildResultsForReport(@props.data, forReportState)

    onChange: (newVal, oldVal, changes, evntObj) ->
      if evntObj and evntObj.after
        reportState = @getReportState()
        @setState({options: @getOptions(@getReportState())})
        ResultSelectorUtil.setViewState(reportState.asmtType)

    onViewChange: () ->
      state = App.getState()
      if state.view is 'growth' and state.result isnt 'overall'
        App.set 'result', 'overall', false, true

    onResultSelect: (result, e) ->
      # if it doesn't have an action or is disabled
      if result.header or result.subheader or result.disabled
        return false
      App.setState(result.states);

    getReportState: () ->
      contextSecurity = ContextSecurity.get()
      reportState = App.getState()
      reportState.report = @state.report
      reportState.piiAll = contextSecurity and contextSecurity.canAccessPII()
      reportState

    getTitle: () ->
      titleElements = []
      options = @state.options
      selectedOption = ResultSelectorUtil.getForCurrentState @getReportState(), options
      if selectedOption
        if selectedOption.states.asmtType is 'SUMMATIVE'
          titleElements.push capFirst selectedOption.states.asmtType.toLowerCase()
          titleElements.push <span>:&nbsp;</span>
        titleElements.push <span className="selected-label">{selectedOption.label}</span>
      titleElements

    renderItem: (item, sub) ->
      classes = []
      ariaHidden = false
      if item.header
        classes.push 'h1 header'
        ariaHidden = true
      else if item.subheader
        classes.push 'h2 sub-header'
        ariaHidden = true

      if item.disabled
        classes.push 'disabled'

      if item.classes
        classes.push item.classes
      # enabled={not item.header and not item.subheader and not item.disabled}
      if item.header or item.subheader
        <RB.MenuItem
        aria-hidden={ariaHidden}
        className={classes.join(' ')}
        disabled={item.disabled}
        header={item.header or item.subheader}
        title={item.tooltipText or ''}>{item.label}</RB.MenuItem>
      else
        <li role="presentation" disabled={item.disabled} className={classes.join(' ')} >
          <a role="button"
            href="#"
            aria-hidden={ariaHidden}
            onClick={@onResultSelect.bind(this, item)}
            title={item.tooltipText or ''}>{item.label}</a>
        </li>

    render: () ->
      options = ResultSelectorUtil.flattenResults(@state.options)
      <div>
        <label aria-hidden="true" id="resultSelectorLabel" className="result-label">Results</label>
        <div className="btn-group">
          <button aria-labelledby="resultSelectorLabel" data-toggle="dropdown" className="dropdown-toggle btn btn-default">
            <span className="selected-label label-wrap">{@getTitle()}&nbsp;</span>
            <span className="caret"></span>
          </button>
          <ul className="dropdown-menu" role="menu">
            {(@renderItem(option) for option in options)}
          </ul>
        </div>
      </div>

  create = (results) ->
    React.render(
      <ResultsDropdown data={results}/>,
      $('#resultSelector')[0]
    )

  create:create
  ResultsDropdown:ResultsDropdown
  Util: ResultSelectorUtil
