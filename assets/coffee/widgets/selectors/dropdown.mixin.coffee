define ['jquery', 'edwareEvents'], ($, edwareEvents) ->
# we make sure that edwareEvents is loaded even tho its global

  {
    focusLost: () ->
      wrapper = $(@getDOMNode()).children().andSelf().filter('.btn-group')
      if wrapper.hasClass('open')
        $('.dropdown-backdrop').remove()
        wrapper.removeClass('open')

    componentDidMount: () ->
      $(@getDOMNode()).focuslost(@focusLost)

    componentWillUnmount: () ->
      $(@getDOMNode()).unbindFocuslost(@focusLost)
  }
