define [
  'jquery'
  'react'
  'reactbootstrap'
  'dropdownMixin'
  'edwareConstants'
  'EdwareApplication'
], ($, React, RB, dropdownMixin, Constants, App) ->

  GradesCoursesDropdown = React.createClass

    mixins: [dropdownMixin]

    componentDidMount: () ->
      @getApp().subscribe Constants.EVENTS.GRADE_COURSE_CHANGE, @onChange
      if @props.app
        @getApp().subscribe Constants.EVENTS.SUBJECT_CHANGE, @onChange

    componentWillUnmount: () ->
      @getApp().unsubscribe Constants.EVENTS.GRADE_COURSE_CHANGE, @onChange
      if @props.app
        @getApp().unsubscribe Constants.EVENTS.SUBJECT_CHANGE, @onChange

    getGradesAndCourses: (value) ->
      subject = @getApp().getState().subject
      grades = @props.data[subject]

      if $.isArray grades
        gradeCourse =  grades.filter (s) -> s.value is value
        gradeCourse[0] if gradeCourse.length
      else
        for label, gradeGroup of grades
          gradeCourse = gradeGroup.filter (s) -> s.value is value
          return gradeCourse[0] if gradeCourse.length


    onChange: () ->
      gradeCourse = @getApp().getState().gradeCourse
      @setState({gradeCourse: @getGradesAndCourses(gradeCourse)})

    onGradesCoursesSelect: (value, e) ->
      index = value
      subject = @getApp().getState().subject
      grades = @props.data[subject]
      gradeCourse
      if $.isArray grades
        gradeCourse = grades[index]
      else
        allOptions = []
        for label, group of grades
          allOptions.push group...
        gradeCourse = allOptions[index]
      @getApp().setState('gradeCourse', gradeCourse.value)

    getInitialState: () ->
      gradeCourse = @getApp().getState().gradeCourse
      {gradeCourse: @getGradesAndCourses(gradeCourse)}

    getApp: () ->
      @props.app or App

    getTitle: () ->
      <span className="selected-label">{@getGradesAndCourses(@getApp().getState().gradeCourse)?.label || 'Select...'}</span>

    render: () ->
      if not @props.app or (@props.app and @props.app.getState().subject)
        onSelect = @onGradesCoursesSelect
        subject = @getApp().getState().subject
        grades = @props.data[subject]
        disabled = @props.disabled
        self = @
        options = []
        if $.isArray grades
          options = grades.map((gradeCourse, index) ->
            <li role="presentation">
              <a href="#" onClick={onSelect.bind(self, index)} role="button">{gradeCourse.label}</a>
            </li>
            )
        else
          i = -1
          for label, gradeGroup of grades
            if options.length
              options.push(<RB.MenuItem divider></RB.MenuItem>)
            group = gradeGroup.map((gradeCourse, index) ->
              i++
              <li role="presentation">
                <a href="#" data-index={i} onClick={onSelect.bind(self, i)} role="button" tabindex="-1" >{gradeCourse.label}</a>
              </li>)
            options.push group...
        <div>
          <label aria-hidden="true" id="gradeCourseSelectorLabel" className="grade-course-label">Grade/Course</label>
          <div className="btn-group">
            <button disabled={disabled} aria-labelledby="gradeCourseSelectorLabel" data-toggle="dropdown" className="dropdown-toggle btn btn-default">
              <span className="selected-label">{@getTitle()}&nbsp;</span>
              <span className="caret"></span>
            </button>
            <ul className="dropdown-menu" role="menu">
              {options}
            </ul>
          </div>
        </div>
      else
        <br/>

  create = (data) ->
    React.render(
      <GradesCoursesDropdown data={data}/>,
      $('#gradeCourseSelector')[0]
    )

  create:create
  GradesCoursesDropdown:GradesCoursesDropdown
