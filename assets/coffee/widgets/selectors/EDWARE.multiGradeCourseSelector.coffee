define [
  'jquery'
  'react'
  'reactbootstrap'
  'edwareConstants'
  'EdwareApplication'
], ($, React, RB, Constants, App) ->

  MultiSelectOption = React.createClass
    onChange: (event) ->
      @props.onSelect @props.value, !@props.isChecked
      event.stopPropagation()

    render: () ->
      label = @props.name
      <li role="presentation" eventKey={@props.eventKey}>
      <a role="presentation" tabIndex="-1">
      <label className="checkbox">
        <input aria-labelledby={"gradeCourseLabel#{@props.eventKey}"} role="menuitemcheckbox" type="checkbox" onChange={@onChange} checked={@props.isChecked} />
        <span aria-hidden="true" id={"gradeCourseLabel#{@props.eventKey}"}>{label}</span>
      </label>
      </a>
      </li>

  MultiGradesCoursesDropdown = React.createClass
    componentDidMount: () ->
      @getApp().subscribe Constants.EVENTS.GRADE_COURSE_CHANGE, @onChange
      if @props.app
        @getApp().subscribe Constants.EVENTS.SUBJECT_CHANGE, @onChange

      domNode = @getDOMNode()
      $('a:last, button', $(domNode)).on 'blur.multiGradesCoursesSelector', () ->
        setTimeout (() -> $(domNode).find('.btn-group').andSelf().removeClass('open') if not $.contains domNode, document.activeElement), 0

    componentWillUnmount: () ->
      @getApp().unsubscribe Constants.EVENTS.GRADE_COURSE_CHANGE, @onChange
      if @props.app
        @getApp().unsubscribe Constants.EVENTS.SUBJECT_CHANGE, @onChange

      domNode = @getDOMNode()
      $('a:last, button', $(domNode)).off 'blur.multiGradesCoursesSelector'

    getGradesAndCourses: (value) ->
      subject = @getApp().getState().subject
      grades = @props.data[subject]

      if value isnt 'all'
        if $.isArray grades
          gradeCourse =  grades.filter (s) -> s.value in value
          return gradeCourse if gradeCourse.length
        else
          combinedGradeCourse = []
          for label, gradeGroup of grades
            gradeCourse = gradeGroup.filter (s) -> s.value in value
            combinedGradeCourse = combinedGradeCourse.concat(gradeCourse)
          return combinedGradeCourse
      else
        @getAllGradesCourses()

    getAllGradesCourses: () ->
      subject = @getApp().getState().subject
      grades = @props.data[subject]
      if $.isArray grades
        grades.map (item) -> item.value
      else
        gradesArray = []
        for label, gradeGroup of grades
          gradesArray = gradesArray.concat(gradeGroup)
        gradesArray.map (item) -> item.value
    onChange: () ->
      appState = @getApp().getState()
      gradeCourse = appState.gradeCourse
      @setState({gradeCourse: @getGradesAndCourses(gradeCourse)})

      if gradeCourse.length is @getAllGradesCourses().length
        @setState({allSelected: true})
      else
        @setState({allSelected: false})
      $(@refs.dropdown?.getDOMNode()).trigger('focus')

    onGradesCoursesSelect: (value, shouldSet) ->
      gradeCourse = value
      if gradeCourse isnt 'all'
        current = @getApp().getState().gradeCourse
        if shouldSet
          current.push gradeCourse
        else
          current = @removeGradeCourse(gradeCourse)
        @getApp().setState('gradeCourse', current)
      else
        if shouldSet
          @addAllCourses()
        else
          @removeAllCourses()

    removeGradeCourse: (value) ->
      remove = (index, array) ->
        array.splice index, 1
      current = @getApp().getState().gradeCourse
      remove i, current for item, i in current when item is value
      current

    removeAllCourses: () ->
      @getApp().setState({
        gradeCourse: []
      })

    addAllCourses: () ->
      @getApp().setState({
        gradeCourse: @getAllGradesCourses()
      })

    isOptionSelected: (value) ->
      current = @getApp().getState().gradeCourse
      for item in current
        return true if item is value
      false

    getInitialState: () ->
      gradeCourse = @getApp().getState().gradeCourse
      {
        gradeCourse: gradeCourse,
        allSelected: false
      }

    getApp: () ->
      @props.app or App

    handleClick: (item, shouldSet) ->
      @onGradesCoursesSelect item, shouldSet
      $(@refs.container.getDOMNode()).addClass('open')

    getTitle: () ->
      gradesCourses = @getGradesAndCourses(@getApp().getState().gradeCourse)
      if gradesCourses
        labels = (gc.label for gc in gradesCourses)
        labels = labels.join(', ')
      <span className="selected-label">{labels || 'Select...'}</span>

    render: () ->
      style = {display: 'none'}
      if not @props.app or (@props.app and @props.app.getState().subject)
        style = {display: 'block'}
      self = this
      onSelect = @handleClick
      subject = @getApp().getState().subject
      grades = @props.data[subject]
      options = []
      if $.isArray grades
        options = grades.map((gradeCourse, index) ->
          selected = self.isOptionSelected(gradeCourse.value)
          <MultiSelectOption isChecked={selected} eventKey={index} onSelect={onSelect} name={gradeCourse.label} value={gradeCourse.value}></MultiSelectOption>)
      else
        for label, gradeGroup of grades
          if options.length
            options.push(<RB.MenuItem divider></RB.MenuItem>)
          group = gradeGroup.map((gradeCourse, index) ->
            selected = self.isOptionSelected(gradeCourse.value)
            <MultiSelectOption isChecked={selected} eventKey={index} onSelect={onSelect} name={gradeCourse.label} value={gradeCourse.value}></MultiSelectOption>)
          options.push group...
      <div style={style}>
        <label aria-hidden="true" id="multiGradeCourseSelectorLabel" className="grade-course-label">Grade/Course</label>
        <div className="btn-group" ref="container">
          <RB.Button aria-labelledby="multiGradeCourseSelectorLabel" ref="dropdown" className="dropdown-toggle btn btn-default" type="button" data-toggle="dropdown">
            <span>{@getTitle()}</span>
            <span className="caret"></span>
          </RB.Button>
          <ul className="dropdown-menu" role="menu">
            <MultiSelectOption isChecked={@state.allSelected} eventKey={'all'} onSelect={onSelect} name={'Select All'} value={'all'}></MultiSelectOption>
            {options}
          </ul>
        </div>
      </div>
      # else
      #   <br/>

    create = (gradeCourse) ->
      React.render(
        <GradesCoursesDropdown data={data}/>,
        $('#gradeCourseSelector')[0]
      )

  create:create
  MultiGradesCoursesDropdown: MultiGradesCoursesDropdown
