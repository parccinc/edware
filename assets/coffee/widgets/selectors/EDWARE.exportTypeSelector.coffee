define [
  'jquery'
  'react'
  'dropdownMixin'
  'edwareConstants'
  'EdwareApplication'
], ($, React, dropdownMixin, Constants, App) ->

  ExportTypeDropdown = React.createClass

    mixins: [dropdownMixin]

    componentDidMount: () ->
      @getApp().subscribe(Constants.EVENTS.EXPORT_TYPE_CHANGE, @onChange)

    componentWillUnmount: () ->
      @getApp().unsubscribe(Constants.EVENTS.EXPORT_TYPE_CHANGE, @onChange)

    getExportType: (value) ->
      exportType = @props.data.filter (s) -> s.value is value
      exportType[0] if exportType.length

    onChange: () ->
      exportType = @getApp().getState().exportType
      @setState({exportType: @getExportType(exportType)})
      $(@getDOMNode()).find('button').andSelf().trigger('focus')

    onExportTypeSelect: (value, e) ->
      index = value
      exportType = @props.data[index]
      @getApp().setState('exportType', exportType.value)

    getInitialState: () ->
      exportType = @getApp().getState().exportType
      {exportType: @getExportType(exportType)}

    getApp: () ->
      # can use a private copy of App if passed in as a prop
      @props.app or App

    getTitle: () ->
      <span className="selected-label">{@state.exportType?.label || 'Select...'}</span>

    render: () ->
      options = @props.data.map((exportType, index) =>
        <li role="presentation">
          <a href="#" onClick={@onExportTypeSelect.bind(this, index)} role="button">{exportType.label}</a>
        </li>
      )
      <div>
        <label aria-hidden="true" id="exportTypeLabel" className="export-type-label">Export Type</label>
        <div className="btn-group exportType">
          <button aria-labelledby="exportTypeLabel" data-toggle="dropdown" className="dropdown-toggle btn btn-default">
            {@getTitle()}
            <span className="caret"></span>
          </button>
          <ul className="dropdown-menu" role="menu">
            {options}
          </ul>
        </div>
      </div>

  create = (data) ->
    React.render(
      <ExportTypeDropdown data={data}/>,
      $('#exportTypeSelector')[0]
  )

  create: create
  ExportTypeDropdown: ExportTypeDropdown
