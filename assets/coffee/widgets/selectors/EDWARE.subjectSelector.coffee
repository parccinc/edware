define [
  'jquery'
  'react'
  'dropdownMixin'
  'edwareConstants'
  'EdwareApplication'
], ($, React, dropdownMixin, Constants, App) ->

  SubjectsDropdown = React.createClass

    mixins: [dropdownMixin]

    componentDidMount: () ->
      @getApp().subscribe(Constants.EVENTS.SUBJECT_CHANGE, @onChange)
      if @props.app
        @getApp().subscribe(Constants.EVENTS.EXPORT_TYPE_CHANGE, @onChange)

    componentWillUnmount: () ->
      @getApp().unsubscribe(Constants.EVENTS.SUBJECT_CHANGE, @onChange)
      if @props.app
        @getApp().unsubscribe(Constants.EVENTS.EXPORT_TYPE_CHANGE, @onChange)

    getSubject: (value) ->
      subject =  @props.data.filter (s) ->
        if s.value is value
          return s
      subject[0] if subject.length

    onChange: () ->
      subject = @getApp().getState().subject
      @setState({subject: @getSubject(subject)})
      $(@getDOMNode()).find('button').trigger('focus')

    onSubjectSelect: (value, e) ->
      e.preventDefault()
      subject = @props.data[value]
      @getApp().setState('subject', subject.value)

    getInitialState: () ->
      subject = @getApp().getState().subject
      {subject: @getSubject(subject)}

    getApp: () ->
      @props.app or App

    getTitle: () ->
      <span className="selected-label">{@state.subject?.label || 'Select...'}</span>

    renderMenuItems: () ->
      @props.data.map((subject, index) =>
        <li role="presentation">
          <a href="#" onClick={@onSubjectSelect.bind(this, index)} role="button">{subject.label}</a>
        </li>
      )

    render: () ->
      <div ref="dropdown">
        <label aria-hidden="true" id="subjectSelectorLabel" className="subject-label">Subject</label>
        <div className="btn-group">
          <button aria-labelledby="subjectSelectorLabel" data-toggle="dropdown" className="dropdown-toggle btn btn-default">
            {@getTitle()}
            <span className="caret"></span>
          </button>
          <ul className="dropdown-menu" role="menu">
            {@renderMenuItems()}
          </ul>
        </div>
      </div>

  create = (subjects) ->
    React.render(
      <SubjectsDropdown data={data}/>,
      $('#subjectSelector')[0]
    )

  create:create
  SubjectsDropdown:SubjectsDropdown
