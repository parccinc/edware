define ['mustache'], (Mustache) ->

  class ClusterToolTipProvider

    constructor: (clusters) ->
      @clusters = clusters

    cellNeedsPostRender: (row, column) ->
      column.name in [ "CLUSTER", "CLUSTERS"]

    doCellPostRender: (row, column, node, popoverProvider) ->
      clusterItems = $('.cluster-item', $(node))
      self = this
      clusterItems.each ()->
        i = $(this).index()
        if self.clusters[row.clusters[i].code_cluster]
          btn = $('<span />', {
            'class': "icon info-icon #{column.field}-#{i}"
            'tabindex': '0'
          })

          $(this).find('.cluster-code-info').append(btn)
          popoverProvider.initializePopover btn, self.getPopoverMarkup(self.clusters[row.clusters[i].code_cluster])

    getPopoverMarkup: (clusterInfo) ->
      Mustache.render('
        <div class="cluster-popover-wrapper">
          <div class="clearfix">
            <div class="cluster-grade-level">GRADE LEVEL<br/>{{grade}}</div>
            <div class="cluster-progression">PROGRESSION<br/>{{data.progression_description}}</div>
          </div>
          <div class="cluster-description">CLUSTER<br/><b>{{data.cluster}}.</b>&nbsp;{{data.cluster_description}}</div>
        </div>
      ', { grade: clusterInfo.grade, data: clusterInfo})


  ClusterToolTipProvider: ClusterToolTipProvider
