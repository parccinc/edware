define [
  "jquery"
  "react"
  "edwareUtil"
  'EdwareApplication'
], ($, React, edwareUtil, App) ->

  PrintMenu = React.createClass
    componentDidMount: () ->
      $('.print-button').popover()

    printUrl: (type) ->
      state = App.getState()
      url = edwareUtil.getBaseURL() + "/services/pdf/studentReport.html?"
      params =
        stateCode: state.stateCode
        districtGuid: state.districtGuid
        schoolGuid: state.schoolGuid
        gradeCourse: state.gradeCourse
        year: state.year
        studentGuid: state.studentGuid
        asmtType: state.asmtType
        subject: state.subject
        pdf: type
      params = Object.keys(params).map (key, index) ->
        key+'='+params[key]
      params = params.join('&')
      url+=params
      url

    handleClick: (e) ->
      if !$('.print-option').length
        $('.print-button').popover 'show'
      eventTarget = $(e.target)
      optionTarget = eventTarget.add(eventTarget.parents('.print-option')).filter('.print-option')
      if optionTarget.length
        @print(optionTarget.attr('data-print-type'))

      $('.popover').on 'mouseleave', () =>
        @handleBlur()

      $('.print-option.grayScale').trigger('focus')
      @handleMouseOut()
      $('.go-back-from-popover').on 'focus', () =>
        $('.print-button').trigger('focus')
        @handleBlur()
      $('.go-forward-from-popover').on 'focus', () =>
        nextTabbableElement = $('.prev-arrow').get(0) or $('.next-arrow').get(0) or $('.slick-header-column span').get(0)
        $(nextTabbableElement).trigger('focus')
        @handleBlur()

    handleBlur: () ->
      $('.popover').off 'mouseleave'
      $('.go-back-from-popover').off 'focus'
      $('.go-forward-from-popover').off 'focus'
      $('.print-button').popover 'hide'

    handleMouseOut: () ->
      setTimeout (() =>
        if not $(".popover:hover").length and not $('.print-option.grayScale:focus').length and not $('.print-option.color:focus').length
            $('.go-back-from-popover').off 'focus'
            $('.go-forward-from-popover').off 'focus'
            $('.popover').off 'mouseleave'
            $('.print-button').popover 'hide'

        else
            @handleMouseOut()
      ), 300

    print: (type) ->
        window.open(@printUrl(type), "_blank",'toolbar=0,location=0,menubar=0,status=0,resizable=yes')

    createOptions: () ->
      React.renderToStaticMarkup(
        <div>
        <a className="go-back-from-popover" href="#"></a>
          <ul className="nav nav-list" role="menu" style={whiteSpace:'nowrap'}>
            <li><a className="print-option grayScale" href="#" role="menuitem" aria-label="Print black & white" data-print-type="gray" onClick={@printGrayScale}>GrayScale&nbsp;<span className="muted">(for black & white printing)</span></a></li>
            <li className="divider"></li>
            <li><a className="print-option color" href="#" role="menuitem" aria-label="Print in color" data-print-type="color" onClick={@printColor}>Color</a></li>
          </ul>
          <a className="go-forward-from-popover" href="#"></a>
        </div>
      )

    render: () ->
      <div className="print-widget" onClick={@handleClick}>
        <button
          className="btn-link pull-right print-button"
          data-animation="true"
          data-html="true"
          data-placement="bottom"
          data-trigger="click"
          data-title=""
          data-content={@createOptions()}
          onMouseOut={@handleMouseOut}
          ref="popover"
          container="body">PRINT</button>
      </div>
  create = (config, labels) ->
    <PrintMenu></PrintMenu>
  create: create
  PrintMenu: PrintMenu
