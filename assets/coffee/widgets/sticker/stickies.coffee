define ['jquery'], ($) ->
  defaults =
    className: 'is-fixed'
    bottomMargin: 0
    topMargin: 0
    wrapperClassName: 'fixed-wrapper'
  $window = $(window)
  $document = $(document)
  windowHeight = $window.height()
  stickies = []

  stickElement = (s) ->
    s.element.css
      width: s.element.width()
      position: 'fixed'
      top: s.currentTop
    s.wrapper.addClass s.className
    s.element.trigger 'sticking'
    return

  unstickElement = (s) ->
    s.element.css
      width: ''
      position: ''
      top: ''
    s.element.trigger 'unsticking', [ s ]
    s.wrapper.removeClass s.className
    return

  windowResize = ->
    windowHeight = $window.height()
    return

  getStickyIndexForElement = (element) ->
    idx = -1
    i = 0
    len = stickies.length
    while i < len
      if stickies[i].element.get(0) == element.get(0)
        idx = i
        break
      i++
    idx

  recalculateHeight = (sticky) ->
    sticky.wrapper.css 'height', sticky.element.outerHeight()
    return

  digest = ->
    scrollTop = $window.scrollTop()
    documentHeight = $document.height()
    dwh = documentHeight - windowHeight
    overBounce = if scrollTop > dwh then dwh - scrollTop else 0
    i = 0
    len = stickies.length
    while i < len
      s = stickies[i]
      elementTop = s.wrapper.offset().top
      elementAnchorPoint = elementTop - s.topMargin - overBounce
      if scrollTop <= elementAnchorPoint
        if s.isSitcking or s.currentTop != null

          ###remove fixed styles.
          # NOTE: should probably save old styles to restore.
          ###

          s.currentTop = null
          unstickElement s
      else
        anchorPos = documentHeight - s.element.outerHeight() - s.topMargin - s.bottomMargin - scrollTop - overBounce
        if anchorPos < 0
          anchorPos = anchorPos + s.topMargin
        else
          anchorPos = s.topMargin
        if s.currentTop != anchorPos
          s.currentTop = anchorPos
          stickElement s
      i++
    return

  methods =
    init: (_options) ->
      options = $.extend({}, defaults, _options)
      @each ->
        element = $(this)
        # the jquery way: skip if it already has an instance
        if getStickyIndexForElement(element) != -1
          return

        wrapper = element.wrapAll('<div />').parent().addClass(options.wrapperClassName)

        ###make sure we reference the valid dom node?
        * is this still needed?
        ###

        sticky =
          bottomMargin: options.bottomMargin
          topMargin: options.topMargin
          currentTop: null
          className: options.className
          element: element
          wrapper: wrapper
        recalculateHeight sticky
        stickies.push sticky
        return
    invalidate: (options) ->
      ret = @each(->
        element = $(this)
        index = getStickyIndexForElement(element)
        if index != -1
          recalculateHeight stickies[index]
        return
      )
      digest()
      ret
    unstick: (options) ->
      @each ->
        element = $(this)
        idx = getStickyIndexForElement(element)
        if idx != -1
          unstickElement stickies[idx]
          stickies.splice idx, 1
          element.unwrap()
        return

  $.fn.sticky = (method) ->
    if methods[method]
      return methods[method].apply(this, Array::slice.call(arguments, 1))
    else if typeof method == 'object' or !method
      return methods.init.apply(this, arguments)
    else
      $.error 'invalid Action ' + method + ' in jquery.sticky'
    return

  $.fn.unstick = (method) ->
    if methods[method]
      return methods[method].apply(this, Array::slice.call(arguments, 1))
    else
      return methods.unstick.apply(this, arguments)
      # $.error 'invalid action ' + method + ' in jquery.unstrick'
    return

  $ ->
    # let the event-queue free up first
    setTimeout (->
      #initialize here - to prevent height bugs in some browsers
      $(stickies).each (index, sticky) ->
        recalculateHeight sticky
        return
      # once all heights are set. lets recalulate stickies;
      digest()
      return
    ), 0
    return
  if window.addEventListener
    window.addEventListener 'scroll', digest, false
    window.addEventListener 'resize', windowResize, false
  else if window.attachEvent
    window.attachEvent 'onscroll', digest
    window.attachEvent 'onresize', windowResize

  $.fn.sticky
