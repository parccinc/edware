define ['mustache'], (Mustache) ->

  class ExpressivenessTooltipProvider

    @headerNeedsPostRender: (column) ->
      column.toolTipData?

    @doHeaderPostRender: (node, column, popoverProvider) ->
      btn = popoverProvider.createInfoIcon(column.field)
      popoverProvider.appendToHeader(btn, node)
      scores = ''
      for scoreString, index in column.toolTipData
        scores = "#{scores}#{@renderScoreTemplate(index, scoreString)}"

      popoverProvider.initializePopover btn, Mustache.render("
      <div class='expressiveness-container'>
        <h3>expressiveness rubric</h3>
        <ul class='clearfix'>
        #{scores}
        </ul>
      </div>
      ", column)

    @renderScoreTemplate: (level, scoreString) ->
      Mustache.render('
      <li style="max-width:20%;">
        <span class="overall level-{{level}}">{{level}}</span>
        <div style="height:150px;overflow:hidden;">{{scoreString}}</div>
      </li>
      ', {level: level, scoreString: scoreString})
