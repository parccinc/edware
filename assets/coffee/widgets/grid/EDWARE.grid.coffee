define [
  'edwareGridAdapter',
  'SlickDataSource',
  'SlickSummaryDataSource',
  'SlickGroupDataSource',
  'SlickColumnModel'
], (
  edwareGridAdapter,
  SlickDataSource,
  SlickSummaryDataSource,
  SlickDataGroupSource,
  SlickColumnModel
) ->

  # Module to composites the GridApi

  edwareGridAdapter: edwareGridAdapter
  SlickDataSource: SlickDataSource
  SlickDataGroupSource: SlickDataGroupSource
  SlickColumnModel: SlickColumnModel
  SlickSummaryDataSource: SlickSummaryDataSource
  create: (config) ->
    options = config.options
    columnModel = new config.columnModel options['columns'], options['reportConfig']
    gridSource = new config.dataSource options['assessments']
    if config.headerDataSource
      headerSource = new config.headerDataSource(options['summary'] || {})
    else
      headerSource = []
    new edwareGridAdapter gridSource, columnModel, options, headerSource
