define [
  'jquery',
  'SlickDataSource',
  'edwareUtil'
], ($, SlickDataSource, edwareUtil) ->

  GROUP_DEFAULT_CONFIG =
    groupIsHeader: true # - is the the group object a row in iteself ?
    childrenKey: 'children' # the key collapsed chidlren
    startCollapsed: true
    toggleButton: '.group-toggle'
    groupHeaderMetaProvider: () ->  # - special formatting for header rows (group Ojbect)
        $.error "GroupDataSourcre requires a groupHeaderMetaProvider"
    groupItemMetaProvider: () ->
      $.error "GroupDataSourcre requires a groupItemMetaProvider"

  class SlickDataGroupSource extends SlickDataSource

    constructor: (config, groups) ->
      super groups
      @config = $.extend true, {}, GROUP_DEFAULT_CONFIG, config
      @childrenField = config.childrenKey
      #GroupDataSource properties
      @isNewView = true # flag if we need to recreate groups
      @rows = [] # computed rows as the grid sees it
      @dataPoints = []

    ###
    # Grouping logic
    ###
    setView: (view) ->
      # will cuase it to recalculate the rows on next call to getView
      @isNewView = true
      super

    getView: () ->
      @groups = super
      # do we need to do some book keeping ?
      if @isNewView
        @isNewView = false
        @initializeGroups @groups #groups
        @calculateVisibleRows @groups
      @rows

    getGroups: () ->
      @groups

    initializeGroups: (groups) ->
      for group, index in groups
        children = group[@childrenField]
        group.groupInfos =
          isHeader: true
          groupId: index # unique id for each group
          hasCollapsibleChildren: children && children.length > 0
          isCollapsed: @config.startCollapsed
        # may need this for styling purposes
        if children and children.length > 0
          for child in children
            # temp flattened array
            for row in @extractDataPoints(child, @dataPoints).push child
              row.isInCollapsible = true

        groups

    update: () ->
      @calculateVisibleRows(@groups)

    calculateVisibleRows: (groups) ->
      isDataPointCollection = @dataPoints.length > 0
      # uses local var for performance
      rows = []
      for group in groups
        groupConfig = group.groupInfos
        # should we render the group row or is it just a container
        if @config.groupIsHeader
          rows.push group
        if isDataPointCollection
          rows.push.apply rows, @extractDataPoints(group, @dataPoints)
        if groupConfig.hasCollapsibleChildren and !groupConfig.isCollapsed
          for child in  group[@childrenField]
            if isDataPointCollection
              rows.push.apply rows, @extractDataPoints(child, @dataPoints)
            else
              rows.push child
      @rows = rows

    getGroupById: (id) ->
      @groups[id]

    expand: (index) ->
      @groups[index].groupInfos.isCollapsed = false

    collapse: (index) ->
      @groups[index].groupInfos.isCollapsed = true

    isExpanded: (index) ->
      not @groups[index].groupInfos.isCollapsed

    toggle: (group) ->
      group.groupInfos.isCollapsed = not group.groupInfos.isCollapsed

    isGroupHeader: (index) -> # this is row index not groupIndex
      @rows[index].groupInfos?

    ###
    # Good startingPoint for compare features
    ###
    getDataPoints: () ->
      @dataPoints

    # set an array of rows to show
    # can be in or out of the collapsible region
    setDataPoints: (@dataPoints) ->

    # Given an object retuns an array of all visible dataPoints
    extractDataPoints: (root, dataPoints) ->
      data = []
      for dataPoint in dataPoints
        if root[dataPoint]
          root[dataPoint]._dataPoint = dataPoint
          data.push root[dataPoint]
      data

    ###
    # DataAdapter interface expected by slickgrid
    # Overrides default implementation in SlickDataSource
    ###
    getItemMetadata: (index) ->
      row = @getView()[index]
      if row.groupInfos
        return @config.groupHeaderMetaProvider row, index

      @config.groupItemMetaProvider row, index

    clickHandler: (event, args, grid) ->
      row = args.row
      if @isGroupHeader row
        group = @getItem row
        if $(event.target).is @config.toggleButton
          groupId = group.groupInfos.groupId
          @toggle group
          @calculateVisibleRows(@groups)
          grid.notifyUpdated()
          event.stopImmediatePropagation()
          event.preventDefault()
          edwareUtil.initPopovers()
          # accessibility (keep focus)
          $(grid.gridObj.getCellNode(args.row, args.cell)).find(@config.toggleButton).focus()


    sort: (sortCol, isAsc) =>
      if not sortCol
        @update()
        return
      # name is sorted by header row
      # other columns are sorted by main dataPoint
      if sortCol.sortByData
        getter = sortCol.valueGetter || edwareUtil.getDeepValue
        valueGetter = (row, lookup, sortCol) ->
          getter(row[sortCol.sortByData], lookup, sortCol)
      else
        valueGetter = (a) ->
          a.sortOrder
      SlickDataSource::_sort @getGroups(), sortCol, isAsc, valueGetter
      @update()

    ###
    #  internal interface to bind to grid events
    #  similar to SlickSelectionModel interface
    ###
    init: (grid, _slickgrid) ->
      if @config.toggleButton
        self = @
        _slickgrid.onClick.subscribe (event, args) =>
          @clickHandler event, args, grid

    destroy: (grid) -> # cleanup / unsubscribe
