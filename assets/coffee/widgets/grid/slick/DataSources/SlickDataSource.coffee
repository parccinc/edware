define ['edwareUtil'], (edwareUtil) ->

  class SlickDataSource

    constructor: (@viewRows) ->
      # preserve initial order for sorting stability
      for _, bySubject of @viewRows
        for _, assessments of bySubject
          for i, asmt of assessments
            asmt.sortOrder = parseInt(i)

    setView: (view) ->
      @asmtType = view.asmtType
      @subject = view.subject

    getView: () ->
      asmtData = @viewRows[@asmtType] or {}
      asmtData[@subject] or []

    getData: () ->
      @viewRows

    setData: (rows) ->
      @viewRows = rows

    ### Expected interface for slickGrid data Source ###
    getItemMetadata: (row) ->
      item = @getItem(row)
      item.canDrillDown = true
      if item?.is_less_than_min_cell_size
        return {
          columns: {
            2: {colspan: "4"},
            3: {colspan: "0"},
            4: {colspan: "0"},
            5: {colspan: "0"}
          }
        }

    getLength: () ->
      @getView().length

    getItem: (index) ->
      @getView()[index]

    sort: (sortCol, isAsc) =>
      data = @getView()
      valueGetter = sortCol.valueGetter || edwareUtil.getDeepValue
      @_sort(data, sortCol, isAsc, valueGetter)

    _sort: (data, sortCol, isAsc, valueGetter) ->

      relativeOrder = (a, b) ->
        if a == b
          return 0
        else if a > b
          return if isAsc then 1 else -1
        else
          return if isAsc then -1 else 1

      toValue = (valA) ->
        if $.isNumeric(valA)
          return parseFloat(valA)
        else if edwareUtil.isDate(valA)
          return new Date(valA).valueOf()
        else if not valA
          return Number.NEGATIVE_INFINITY
        return valA

      comparer = (valA, valB) ->
        valueA = toValue(valA)
        valueB = toValue(valB)
        return relativeOrder(valueA, valueB)

      lookup = sortCol.sortValue || sortCol.field
      data.sort (a, b) ->
        valueA = valueGetter(a, lookup, sortCol, isAsc)
        valueB = valueGetter(b, lookup, sortCol, isAsc)
        res = comparer valueA, valueB
        if res == 0
          return relativeOrder(a.sortOrder, b.sortOrder)
        else
          return res
