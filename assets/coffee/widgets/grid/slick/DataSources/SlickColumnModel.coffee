define ['edwareGridFormatters', 'selectorUtil'], (edwareGridFormatters, SelectorUtil) ->

  DEFAULT_COLUMN =
    sortable: true
    resizable: false

  # dataProvider for header grid
  class SlickColumnModel

    constructor: (@config, @reportConfig) ->
      @columnsMapping = @createColumns()

    createColumns: () ->
      columnsMapping = {}
      for asmtType, subjectMapping of @config
        columnsMapping[asmtType] = {} if not columnsMapping[asmtType]
        for subject, columns of subjectMapping
          view = {
            asmtType: asmtType
            subject: subject
          }
          columnsForView = []
          baseColumnsMap = @baseColumnMapForView(view)

          # create static columns
          for column in @createStaticColumns(view)
            column = $.extend true, {}, DEFAULT_COLUMN, (baseColumnsMap[column.baseId or column.id] or {}), column
            columnsForView.push column
          # create dynamic columns
          for idx, column of columns
            column = $.extend true, {}, DEFAULT_COLUMN, (baseColumnsMap[column.baseId or column.id] or {}), column
            column = @createColumn parseInt(idx), column, view
            columnsForView.push column

          columnsMapping[asmtType][subject] = columnsForView
          columnsMapping[asmtType][subject].colGroups = @createColGroups columnsForView, view
      columnsMapping

    createStaticColumns: (view) ->
      []

    createColumn: (idx, column) ->
      alert "implement me!"

    baseColumnMapForView: (view) ->
      baseColumns = @reportConfig?.baseColumns or []
      filteredBaseColumns = SelectorUtil.buildOptionsForReport(baseColumns, view)
      baseMap = {}
      for column in filteredBaseColumns
        baseMap[column.id] = column
      baseMap

    getColumns: (view) ->

      asmtData = @columnsMapping[view.asmtType] or {}
      columns = asmtData[view.subject] or []

      SelectorUtil.buildOptionsForReport(columns, view)

    createColGroups: (columns, view) ->
      []

    getFormatter: (formatterName) ->
      edwareGridFormatters.createFormatter formatterName
