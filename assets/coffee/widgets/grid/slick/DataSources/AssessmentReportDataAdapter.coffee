define [
  'jquery'
  'edwareConstants'
  'SlickDataSource'
], ($,
    Constants,
    SlickDataSource
) ->

  class AssessmentReportDataAdapter extends SlickDataSource

    constructor: (data) ->
      viewRows = {}
      for asmtType, assessments of data
        viewRows[asmtType] = {} if not viewRows[asmtType]
        for subjectName, subject of Constants.SUBJECTS
          viewRows[asmtType][subject] = assessments[subject]
      super viewRows

    getItemMetadata: (row) ->
      item = @getItem(row)
      item.canDrillDown = not item?.INVALID_SCORE
      if not item.canDrillDown
        return {
          columns:
            1: {colspan: "11", display_suppression_msg: true},
        }
  {
    AssessmentReportDataAdapter: AssessmentReportDataAdapter
  }
