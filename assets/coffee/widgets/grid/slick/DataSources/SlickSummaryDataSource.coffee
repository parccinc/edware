define [
  'SlickDataSource',
  'EdwareApplication'
], (SlickDataSource, App) ->

  EVENTS = App.Constants.EVENTS

  class SlickSummaryDataSource extends SlickDataSource

    constructor: (@data) ->
      @update()

    update: () ->
      @filteredView = []
      summaries = @data?['SUMMATIVE']?[App.getState().subject]
      activeSummaries = App.get(EVENTS.SUMMARY_CHANGE)
      if activeSummaries && summaries
        for summary in summaries
          if activeSummaries[summary.type]
            @filteredView.push summary
      @filteredView

    setView: (view) ->
      @subject = view.subject
      @update()

    getView: () ->
      @filteredView
