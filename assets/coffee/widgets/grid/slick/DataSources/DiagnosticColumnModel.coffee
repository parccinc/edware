define [
  'jquery'
  'EdwareApplication'
  'edwareGrid'
  'selectorUtil'
], ($, App, edwareGrid, SelectorUtil) ->

  BaseColumnModel = edwareGrid.SlickColumnModel

  class DiagnosticColumnModel extends BaseColumnModel

    constructor: (@config, @reportConfig) ->
      super @config, @reportConfig

    createColumn: (idx, column, view) ->
      columnDef = $.extend(true, {}, column)
      # if its set in the config ignore the default classes
      columnDef.headerCssClass ?= 'wrap center'
      if column.formatter
        if column.formatter is 'checksAndX' or column.formatter is 'showClusters'
          columnDef.formatter = @getCutpointFormatter(column.formatter)
        else
          columnDef.formatter = @getFormatter column.formatter
      if column.formatter is 'showSurveyResponses'
        columnDef.overflowToolTip = true
      if columnDef.id is 'grade'
        columnDef.cssClass = 'right'
      else
        columnDef.sortByData = 'school'
      columnDef

    getCutpointFormatter: (formatterName) ->
      formatterFactories = {
        'checksAndX': 'createCheckAndXFormatter',
        'showClusters': 'createClusterFormatter'
      }
      formatterFactory = @getFormatter(formatterFactories[formatterName])
      cutpoints = SelectorUtil.buildOptionsForReport(@reportConfig.cutpoints, App.getState())
      formatterFactory(cutpoints)
