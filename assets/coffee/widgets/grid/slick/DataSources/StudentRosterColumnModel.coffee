define [
  'jquery'
  'edwareConstants'
  'edwareGrid'
  'EdwareApplication'
  'edwareGridFormatters'
  'edwareUtil'
], ($,
    Constants,
    edwareGrid
    App
    edwareGridFormatters
    edwareUtil
) ->

  SlickColumnModel = edwareGrid.SlickColumnModel


  class StudentRosterRowWrapper extends edwareGridFormatters.HiddenRowDataWrapper
    shouldHide: (rowObject) ->
      return 'INVALID_SCORE' of rowObject and rowObject['INVALID_SCORE']
    getHiddenMessage: ->
      Constants.HIDDEN_ROW_MSG.INVALID_SCORE


  class StudentRosterColumnModel extends SlickColumnModel
    # Mapping of formatters to a summary or non summary row
    formatterMapping = {
      nonsummary: {
        perf: 'showPerformanceLevel'
        percentile: 'showPercentile'
        default: 'showDeepValue'
      }
      summary: {
        perf: 'showPopulationBar'
        percentile: 'showPercentile'
        default: 'showDeepValue'
      }
    }

    constructor: () ->
      super

    createColumn: (idx, column, view) ->
      defaultView = App.getState().view
      if defaultView is Constants.VIEWS.ITEM_ANALYSIS
        @createColumnForItemAnalysis(idx, column, view)
      else
        @createColumnForScoreView(idx, column, view)

    getKey: (idx, view) ->
      key = 'default'
      if view.subject is 'subject1'
        key = if idx <= 3 then 'perf' else 'percentile'
      else
        if not (idx is 0 or idx is 4)
          key = if idx <= 6 then 'perf' else 'percentile'
      key

    createColumnForScoreView: (idx, column, view) ->
      headerClasses = ["wrap", "center"]
      key = @getKey(idx, view)
      columnDef = $.extend true, {}, column, {
        id: idx
        name: column.name
        field: [column.field]
        group: column.group
        formatter:@getFormatter(formatterMapping['nonsummary'][key])
        valueGetter: edwareUtil.getHiddenRowSortingValue
        headerCssClass: headerClasses.join(' ')
        summary: {
          rawValue: false
          formatter: @getFormatter formatterMapping['summary'][key]
        # Field values are different for different formatters
          field: column.field
          additionalClasses: 'claims summary'
        }
      }
      columnDef.rawValue = @getFormatter columnDef.rawValue if columnDef.rawValue
      if key is 'perf'
        columnDef.summary.asyncPostRender = @getFormatter 'populationPopover'
      columnDef

    columnHasInfo: (column) ->
      !!column.item_max_points or
      !!column.item_num_responses or
      !!column.item_type

    createColumnForItemAnalysis: (idx, column, view) ->
      columnDef = $.extend true, {}, column, {
        id: idx
        hasInfo: @columnHasInfo(column)
        name: "#{column.name}
          <span class='sub'>#{column.item_max_points} PTS</span>"
        toolTip: "#{column.name} (#{column.item_max_points} PTS)"
        textName: "#{column.name} - #{column.item_max_points} PTS"
        field: [column.name, 'score']
        width: 100
        formatter: new StudentRosterRowWrapper(@getFormatter('showApplicableDeepValue'))
        valueGetter: edwareUtil.getHiddenRowSortingValue
        headerCssClass: "right wrap info"
      }

    createStaticColumns: (view) ->
      [
        {
          id: "student_name"
          name: "STUDENT"
          field: "student_display_name"
          width: if view.subject is 'subject2' then 192 else 240
          link: '/assets/html/studentReport.html'
          target: '_blank'
          formatter: @getFormatter('showLink')
          param: {'studentGuid': 'student_parcc_id'}
          queryParamKeys: ['stateCode', 'districtGuid', 'schoolGuid']
          summary: {
            formatter: @getFormatter 'showSummary'
            field: 'name'
          },
          overflowToolTip: true
        },
        {
          id: "student_grade"
          name: "GRADE"
          field: "student_grade"
          width: 65
          formatter: new StudentRosterRowWrapper(@getFormatter('showDeepValue'))
          valueGetter: edwareUtil.getHiddenRowSortingValue
        },
        {
          id: "overall"
          name: "OVERALL"
          field: ['Overall', 'score']
          level_field: ['Overall' , 'level']
          width: 75
          headerCssClass: "right"
          formatter:@getFormatter('showLevelColoredValue')
          rawValue: @getFormatter('showDeepValue')
          cssClass: "overall"
          valueGetter: edwareUtil.getHiddenRowSortingValue
          summary: {
            formatter: @getFormatter 'showDeepValue'
            field: ['Overall', 'score']
            level_field: ['Overall' , 'level']
          }
        }
      ]

  StudentRosterColumnModel : StudentRosterColumnModel
