define [
  'jquery'
  'react'
  'slickgrid'
  'stickies'
  'edwareUtil'
  'SlickGridAccessibility'
 'edwareConstants'
], ($, React, SlickGrid, sticky, edwareUtil, AccessibilityPlugin, Constants) ->

  DEFAULT_CONFIG =
    gridElement: '[data-component-grid]'
    headerElement: '[data-component-header-grid]'
    gridOptions:
      autoHeight: true
      fullWidthRows: true
      forceFitColumns: true
      rowHeight: 50
      enableAddRow: false
      resizable: false
      enableCellNavigation: false
      enableColumnReorder: false
      multiColumnSort: false
      enableAsyncPostRender: true
      enablePagination: false

  class EdwareGridAdapter
    registerPlugin: (plugin) ->
      pluginObj = {
        plugin: plugin
        contexts: {}
      }
      @plugins.unshift pluginObj
      @initializePlugin pluginObj

    unregisterPlugin: (plugin) ->
      for registeredPlugin, index in @plugins
        if registeredPlugin.plugin is plugin
          @destroyPlugin(registeredPlugin)
          @plugins.slice index, 1
          break

    destroyPlugin: (plugin) ->
      if plugin.plugin.destroy
        for context, contextVal of plugin.contexts
          plugin.plugin.destroy(contextVal)
          delete plugin.contexts[context]

    destroyPlugins: (forContext) ->
      for registeredPlugin, index in @plugins
        if registeredPlugin.contexts[forContext] and registeredPlugin.plugin.destroy
          registeredPlugin.plugin.destroy registeredPlugin.contexts[forContext]
          delete registeredPlugin.contexts[forContext]

    initializePlugins: (forContext) ->
      for plugin in @plugins
        if not plugin.contexts[forContext]
          @initializePlugin plugin, forContext

    initializePlugin: (plugin, forContext) ->
      # for each grid
      if @gridObj and forContext is 'gridContext'
        gridContext = {
          type: 'grid'
          grid : @gridObj
          columnProvider: @getColumnProvider()
          columns : @columns,
          wrapper: this
        }
        plugin.plugin.init gridContext
        plugin.contexts.gridContext = gridContext

      if @headerGrid and forContext is 'headerContext'
        headerContext = {
          type: 'header'
          grid: @headerGrid
          columnProvider: @getColumnProvider()
          columns : @columns,
          wrapper: this
        }
        plugin.plugin.init headerContext
        plugin.contexts.headerContext = headerContext

    constructor: (@dataSource, @columnModel, config, @headerDataSource=[]) ->
      # merge customized option with default one
      @plugins = []
      @asyncPostRenderListeners = []
      mergedConfig = $.extend true, {}, DEFAULT_CONFIG, config
      # groupedHeaderPlugin needs access to config
      @mergedConfig = mergedConfig
      @gridOptions = mergedConfig.gridOptions
      @gridElm = $(mergedConfig.gridElement)
      @headerElement = mergedConfig.headerElement
      @stickyElement = $("#sticky-header").sticky({topMargin: 0})
      @accessibilityPlugin = new AccessibilityPlugin()
      @registerPlugin(@accessibilityPlugin)

    registerOnAsyncPostRender: (callback) ->
      @asyncPostRenderListeners.push callback

    unregisterOnAsyncPostRender: (callback) ->
      index = @asyncPostRenderListeners.indexOf callback
      if index isnt -1
        @asyncPostRenderListeners = @asyncPostRenderListeners.splice index, 1

    onAsyncPostCellRender: (cellNode, row, rowObject, columnDef) ->
      if columnDef._asyncPostRender
        columnDef._asyncPostRender(cellNode, row, rowObject, columnDef)
      # todo: this is not a gridAdapter concern
      if columnDef.overflowToolTip
        @overflowToolTip(rowObject, columnDef, cellNode)
      for listener in @asyncPostRenderListeners
        listener(cellNode, row, rowObject, columnDef)

    isSummaryRow: (row) ->
      row.type in ['parcc', 'state', 'district', 'school']

    # todo: this doesnt belong on gridAdapter
    overflowToolTip: (rowObject, columnDef, cellNode) ->
      # For summary rows, only display state/district/school name, not
      # "state avgRhode Island", etc.
      if @isSummaryRow(rowObject)
        $(cellNode).attr('title', $(cellNode).find('span').text())
      else
        $(cellNode).attr('title', $(cellNode).text())

    onCellRender: (row, cell, value, columnDef, rowObject) ->
      formatter = columnDef._formatter && columnDef._formatter.render || columnDef._formatter
      if formatter then formatter(row, cell, value, columnDef, rowObject) else value

    loadView: (view, @defaultSortColumn) ->
      @switchView view
      @updateBtnsAndCounts()
      # binding events
      @bindEvents()
      @gridObj.onSort.subscribe @onSort

    bindEvents: () ->
      @dataSource.init?(@, @gridObj, @headerGrid)

    createHeader: () ->
      if not @headerElement
        return
      # data is empty. should be a sepperate instance of dataSource for stickyRows
      @destroyPlugins 'headerContext' if @headerGrid
      @headerGrid = new Slick.Grid $(@headerElement), @headerDataSource, @getHeaderColumns(@columns), @gridOptions
      @headerGrid.onKeyDown.subscribe @linkHandler
      @initializePlugins 'headerContext', @headerGrid
      @headerGrid.onSort.subscribe @onSort

    updateHeader: () ->
      if not @headerGrid
        return
      if @headerDataSource && $.type(@headerDataSource) != 'array' && @headerDataSource.update
        @headerDataSource.update()
      @headerGrid.invalidate()
      @headerGrid.render()
      @headerGrid.resizeCanvas()
      @updateStickyHeader()
      @accessibilityPlugin.initializeGrid(@headerGrid)

    updateStickyHeader: () ->
      # temp: we dont need to expose this
      # destroy method should handle it
      @stickyElement.sticky('invalidate')

    onFilter: (data, view) ->
      @dataSource = data
      @gridObj.setData(@dataSource, true)
      @switchView(view)


    getSortColumn: () ->
       grid = @headerGrid or @gridObj
       grid.getSortColumns()

    onSort: (e, args) =>
      # Get the columnDefinition from the gridObj
      columnId = args.sortCol.id
      columnIndex = @gridObj.getColumnIndex columnId
      columnDef = @gridObj.getColumns()[columnIndex]
      @sort columnDef, args.sortAsc

    sort: (index, asc) ->
      @dataSource.sort index, asc
      @updateBody()
      edwareUtil.initPopovers()

    setSortColumn: (index, asc) ->
      # index is an int
      sortColumn = @columns[index]
      @sort sortColumn, asc
      # Call slick grid for setting sorted column (adds css to it)
      # We really just need to apply to headerGrid, but this is to keep the 2 grid objects consistent
      @headerGrid.setSortColumn(sortColumn['id'], asc) if @headerGrid
      @gridObj.setSortColumn sortColumn['id'], asc

    setColumns: (columns) ->
      @gridObj.setColumns columns
      @accessibilityPlugin.initializeGrid(@gridObj)
      @setHeaderColumns columns
      @updateBtnsAndCounts()

    getHeaderColumns: (columns) ->
      columns = columns.slice()
      for columnDef, idx in columns
        # make sure we still handle formatters with onCellRender
        summaryData = $.extend true, {}, columnDef.summary
        if summaryData.formatter
          summaryData._formatter = summaryData.formatter
          delete summaryData.formatter
        if summaryData.asyncPostRender
          summaryData._asyncPostRender = summaryData.asyncPostRender
          delete summaryData.asyncPostRender

        columns[idx] = $.extend {}, columnDef, summaryData
      columns

    setHeaderColumns: (columns) ->
      if @headerGrid
        @headerGrid.setColumns @getHeaderColumns columns
        @accessibilityPlugin.initializeGrid(@headerGrid)

    initializeView: (view) ->
      @columns = @columnModel.getColumns(view).map (column, ind) ->
        $.extend true, {}, column

      for column in @columns
        column._formatter = column.formatter
        column.formatter = @onCellRender.bind(this)
        column._asyncPostRender = column.asyncPostRender
        column.asyncPostRender = @onAsyncPostCellRender.bind(this)

      @totalColumns = @columns.length
      @dataSource.setView view
      if @headerDataSource && $.type(@headerDataSource) != "array"
        @headerDataSource.setView view

      if @gridObj
        @destroyPlugins 'gridContext'
      @gridObj = new Slick.Grid @gridElm, @dataSource, @getVisibleColumns(), @gridOptions
      @gridObj.onKeyDown.subscribe @linkHandler
      @initializePlugins 'gridContext'
      # Sets default sort column of first column
      @defaultSortColumn = 0 if not @defaultSortColumn
      @createHeader()

    switchView: (view) ->
      @initializeView view
      columns = @getVisibleColumns()
      @setColumns @getVisibleColumns()
      if columns and columns.length
        @setSortColumn(@defaultSortColumn, true)
      plugin.plugin.onViewSwitched?(view) for plugin in @plugins

    updateFilters: (newFilter) ->
      plugin.plugin.onFilterChange?(view) for plugin in @plugins

    getLength: () ->
      # return size of dataset of current view
      @dataSource.getLength()

    getDataSource: () ->
      @dataSource

    getColumnProvider: () ->
      @columnModel

    getSlickGrid: () ->
      @gridObj

    getSlickHeaderGrid: () ->
      @headerGrid

    updateBtnsAndCounts: () ->
      if @stickyElement
        @stickyElement.sticky 'invalidate'

    linkHandler: (e) =>
      element = $(e.target)
      if e.which is Constants.KEYS.ENTER and element.is 'a[href]'
        href = element.attr 'href'
        target = element.attr 'target'
        if href
          window.open(href, if target then target else '_self')

    getVisibleColumns: () ->
      @columns

    getConfig: () ->
      # exposed so plugins can access it
      @mergedConfig

    updateBody: () ->
      if @gridObj
        @gridObj.invalidate()
        @gridObj.render()
        @accessibilityPlugin.initializeGrid(@gridObj)

    notifyUpdated: () ->
      @updateBody()
      @updateHeader()

    destroy: () ->
      if @gridObj
        @gridObj.onKeyDown.unsubscribe @linkHandler
        @destroyPlugins 'gridContext'
        @gridObj.destroy()
      if @headerGrid
        @gridObj.onKeyDown.unsubscribe @linkHandler
        @destroyPlugins 'headerContext'
        @headerGrid.destroy()
        @stickyElement.sticky 'invalidate'
      @dataSource = @columnModel = @headerDataSource = null
      @mergedConfig = null
      @gridOptions = null
      @gridElm = null
      @headerElement = null
      @stickyElement = null
