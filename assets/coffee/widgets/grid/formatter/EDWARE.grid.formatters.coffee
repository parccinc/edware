define [
  'jquery'
  'mustache'
  'edwareConstants'
  'edwareUtil'
  'edwarePopulationBar'
  'EdwareApplication'
   'text!claimAvgTemplate'
], ($, Mustache, Constants, edwareUtil, edwarePopulationBar, App, claimAvgTemplate) ->

  ordinal = (value) ->
    i = parseInt value
    if i is 1
      "#{i}st"
    else if i is 2
      "#{i}nd"
    else if i is 3
      "#{i}rd"
    else if isNaN(i)
      "#{value}"
    else
      "#{i}th"

  buildUrl = (columnDef, rowObject) ->
    params = {}
    for k, v of columnDef.param
      val = edwareUtil.getDeepValue rowObject, v
      params[k] = val if val
    edwareUtil.generateUrl columnDef.link, params, App.getState(), columnDef.queryParamKeys, columnDef.ignoreParams

  showDeepValue = (row, cell, value, columnDef, rowObject) ->
    deepVal = edwareUtil.getDeepValue(rowObject, columnDef.field)
    if deepVal? then deepVal else columnDef.defaultValue || ''

  class showApplicableDeepValue

    # This method is used to decide whether to display a score or NA*
    # for an item, based on whether or not a student attempted the item
    @valueOrNotApplicableMessage: (value, id, notApplicableMessage) ->
      valIsEmpty = not value? or value is ''
      if valIsEmpty and  id isnt 'student_grade' then notApplicableMessage else value

    @render: (row, cell, value, columnDef, rowObject) =>
      deepVal = showDeepValue(row, cell, value, columnDef, rowObject)
      notApplicableMessage = '<span class="not-applicable">NA</span>*'
      @valueOrNotApplicableMessage(deepVal, columnDef.id, notApplicableMessage)

    @text: (row, cell, value, columnDef, rowObject) =>
      deepVal = showDeepValue(row, cell, value, columnDef, rowObject)
      @valueOrNotApplicableMessage(deepVal, columnDef.id, 'NA*')

  class showSurveyResponses

    @render: (row, cell, value, columnDef, rowObject) ->
      responses = rowObject.responses[parseInt(columnDef.name) - 1]
      pageResponses = responses.join('&nbsp;&bull;&nbsp;')
      ariaLabel = responses.join(', ')
      "<span aria-label='#{ariaLabel}'>#{pageResponses}</span>"

    @text: (row, cell, value, columnDef, rowObject) ->
      rowObject.responses[parseInt(columnDef.name) - 1].join(' , ')

  showCheckbox = (row, cell, value, columnDef, rowObject) ->
    checked = if rowObject.isHidden then "" else "checked=checked"
    "<input class='gridCheckbox' type='checkbox' #{checked}>"

  # suppression message for min cell size and filter threshold
  getSuppressionInfoMsg = (rowObject) ->
    if rowObject.is_less_than_threshold
      "This group has fewer than #{rowObject.alert_threshold} students"
    else if rowObject.is_less_than_min_cell_size
      rowObject.min_cell_size_msg
    else
      ''

  getExtraInfoToolTip = (rowObject) ->
    extraInfoMsg = getSuppressionInfoMsg(rowObject)
    if extraInfoMsg isnt ''
      "<span class='icon alert-icon-suppression'
        tabindex='0'
        data-animation='true'
        data-html='true'
        data-placement='top'
        data-trigger='hover focus'
        data-content='#{extraInfoMsg}'
        data-container='body'></span>"
    else
      ''

  class showLink
    @render: (row, cell, value, columnDef, rowObject) ->
      if rowObject.canDrillDown
        url = buildUrl columnDef, rowObject
        target = if columnDef.target then "target=#{columnDef.target}" else ''
        ret = "<a class='columnLink apply-ellipsis' href='#{url}' #{target} aria-label='#{rowObject[columnDef.field]}. #{getSuppressionInfoMsg(rowObject)}'>#{rowObject[columnDef.field]}</a>"
      else
        ret = "<div class='apply-ellipsis'>#{rowObject[columnDef.field]}</div>"
      # dont show min-cell alert on performance view
      if !columnDef.hideSuppressionAlert or !rowObject.is_less_than_min_cell_size
        ret = getExtraInfoToolTip(rowObject) + ret
      ret
    @text: (row, cell, value, columnDef, rowObject) ->
      rowObject[columnDef.field]


  class showSummary
    @render: (row, cell, value, columnDef, rowObject) ->
      if rowObject.type == 'parcc'
        display_value = value.split(',').length + ' States'
      else
        display_value = value
      ret = "<div class='summaryType'>#{rowObject.type} avg</div><span class='apply-ellipsis'>#{display_value}</span>"
      # dont show min-cell alert on performance view
      if !columnDef.hideSuppressionAlert or !rowObject.is_less_than_min_cell_size
        ret = getExtraInfoToolTip(rowObject) + ret
      ret

    @text: (row, cell, value, columnDef, rowObject) ->
      "#{rowObject.type} AVG - ".toUpperCase() + value

  class showLevelColoredValue

    @render: (row, cell, value, columnDef, rowObject) =>
      score = edwareUtil.getDeepValue rowObject, columnDef.field
      level = edwareUtil.getDeepValue rowObject, columnDef.level_field
      "<span class='level-colored-value level-#{level}'>#{score}</span>"

    @text: (row, cell, value, columnDef, rowObject) =>
      edwareUtil.getDeepValue rowObject, columnDef.field

  class showPassages
    @getShortNames: (passages) ->
      shortNames = {
        "Literary": "Lit"
        "Informational": "Info"
      }
      passageText = []
      for passage, i in passages
        text = shortNames[passage.passage_type]
        if passage.passage_rmm
          text = "#{text} (#{passage.passage_rmm})"
        passageText.push text
      passageText

    @render: (row, cell, value, columnDef, rowObject) =>
      passageTexts = @getShortNames(rowObject[columnDef.field])
      ariaLabel = "#{passageTexts.join(', ')}"
      "<div class='passages-cell' aria-label='#{ariaLabel}'>#{passageTexts.join('&nbsp;&bull;&nbsp;')}</div>"

    @text: (row, cell, value, columnDef, rowObject) =>
      @getShortNames(rowObject[columnDef.field]).join(' , ')


  showPerformanceLevel = (row, cell, value, columnDef, rowObject) ->
    level = edwareUtil.getDeepValue rowObject, columnDef.field
    ariaLabel = "Level #{level}"
    if level then "<span class='glyphicon perf_level_#{level}' aria-label='#{ariaLabel}'></span>" else ''

  class showPercentage
    @getValue: (row, cell, value, columnDef, rowObject) ->
      "#{edwareUtil.getDeepValue rowObject, columnDef.field}%"

    @render: (row, cell, value, columnDef, rowObject) =>
      if rowObject.is_less_than_min_cell_size
        rowObject.level = -1
        rowObject.score = -1
        "<div class='suppressed-row' >#{rowObject.min_cell_size_msg}</div>"
      else
        @getValue(row, cell, value, columnDef, rowObject)

    @text: (row, cell, value, columnDef, rowObject) =>
      if rowObject.is_less_than_min_cell_size
        rowObject.min_cell_size_msg
      else
        @getValue(row, cell, value, columnDef, rowObject)

  showPopulationBar = {

    render: (row, cell, value, columnDef, rowObject) ->
      # summary rows need a column def field
      data = if columnDef.field then rowObject[columnDef.field] else rowObject
      if not rowObject.is_less_than_min_cell_size
        # In column definition, set additionalClasses to 'claims' for claims perf level colors, 'summary' for summary row
        row = $.extend {}, data, {additionalClasses: columnDef.additionalClasses}
        edwarePopulationBar.create row
      else
        "<div class='suppressed-row' >#{rowObject.min_cell_size_msg}</div>"

    text: (row, cell, value, columnDef, rowObject) ->
       # summary rows need a column def field
      data = if columnDef.field then rowObject[columnDef.field] else rowObject
      if not rowObject.is_less_than_min_cell_size
        # In column definition, set additionalClasses to 'claims' for claims perf level colors, 'summary' for summary row
        row = $.extend {}, data, {additionalClasses: columnDef.additionalClasses}
        edwarePopulationBar.createText row
      else
        rowObject.min_cell_size_msg
  }

  # populationPopover is an AsyncPostRender func, not a formatter
  populationPopover = (cellNode, row, rowObject, columnDef) ->
    data = $.extend true, {}, rowObject[columnDef.field], {additionalClasses: 'claims'}
    edwarePopulationBar.createPopover($(cellNode).find('.progress'), data, {
      placement: () ->
        edwareUtil.popupPlacement cellNode, 370, 32
    })

  class showClaimAvg

    @calculateValues: (row, cell, value, columnDef, rowObject) ->
      contents = rowObject['avg']
      readingDecimal = contents['reading'] / Constants.TOTAL_POINTS_POSSIBLE['reading']
      contents['readingPercentage'] = (readingDecimal * 100).toFixed(0)
      contents['readingOffset'] = (1 - readingDecimal) * Constants.CLAIM_BAR_SIZE
      contents['readingTotal'] = Constants.TOTAL_POINTS_POSSIBLE['reading']
      writingDecimal = contents['writing'] / Constants.TOTAL_POINTS_POSSIBLE['writing']
      contents['writingPercentage'] = (writingDecimal * 100).toFixed(0)
      contents['writingOffset'] = (1 - writingDecimal) * Constants.CLAIM_BAR_SIZE
      contents['writingTotal'] = Constants.TOTAL_POINTS_POSSIBLE['writing']
      contents

    @render: (row, cell, value, columnDef, rowObject) =>
      if rowObject.is_less_than_min_cell_size
        return ''
      Mustache.to_html claimAvgTemplate, @calculateValues(row, cell, value, columnDef, rowObject)

    @text: (row, cell, value, columnDef, rowObject) =>
      if rowObject.is_less_than_min_cell_size
        return ''
      contents = @calculateValues row, cell, value, columnDef, rowObject
      "Reading #{contents.reading}/#{contents.readingTotal}, Writing #{contents.writing}/#{contents.writingTotal}"

  class showPercentile

    @getValue: (rowObject, columnDef) ->
      edwareUtil.getDeepValue rowObject, columnDef.field

    @getTextValue: (value) ->
      if value? and value isnt '' then "#{value} %ile" else ''

    @render: (row, cell, value, columnDef, rowObject) =>
      value = @getValue(rowObject, columnDef)
      textValue = @getTextValue(value)
      ariaLabel = "#{ordinal(value)} percentile"
      "<span aria-label='#{ariaLabel}'>#{textValue}</span>"

    @text: (row, cell, value, columnDef, rowObject) =>
      value = @getValue(rowObject, columnDef)
      textValue = @getTextValue(value)

  class showNothing
    @render: (row, cell, value, columnDef, rowObject) ->
      '<div class="invisible-cell"></div><span></span>'

    @text: (row, cell, value, columnDef, rowObject) ->
      ''

  showGroupHeaderRow = (row, cell, value, columnDef, rowObject) =>

      headerDiv = "<div class='group-header-row'>"
      url = buildUrl columnDef, rowObject
      if not rowObject.canDrillDown? or rowObject.canDrillDown
        headerText = "<a href='#{url}' class='group-header-title'>#{rowObject.name}</a>"
      else
        headerText = "<div class='group-header-title'>#{rowObject.name}</div>"
      if rowObject.groupInfos.hasCollapsibleChildren
        #Should come from a label instead
        actionString = if rowObject.groupInfos.isCollapsed then 'Expand' else 'Hide'
        stateString = if rowObject.groupInfos.isCollapsed then 'hidden' else 'expanded'
        cls = rowObject.name.replace(' ', '_')
        headerDiv = "<div class='group-header-row with-collapsible-children'>"
        headerText += "<button class='btn-link expand group-toggle #{cls}' data-group='#{columnDef.groupId}' state='#{stateString}'>#{actionString} Grades</button>"
      "#{headerDiv}#{headerText}</div>"

  class showGradeName
    @getText: (row, cell, report_level, columnDef, rowObject) =>
      grade = rowObject['grade']
      if not rowObject.is_nested
        report_level
      else if report_level is 'school'
        "#{ordinal(grade)} grade students"
      else
        "#{report_level} #{ordinal(grade)} graders"

    @render: (row, cell, report_level, columnDef, rowObject) =>
      ret = @getText(row, cell, report_level, columnDef, rowObject)

      if not columnDef.hideSuppressionAlert and not rowObject.is_less_than_min_cell_size
        ret = "<span class='grade-name apply-ellipsis'>#{ret}</span>"
        ret = getExtraInfoToolTip(rowObject) + ret
      ret
    @text: (row, cell, report_level, columnDef, rowObject) =>
      @getText(row, cell, report_level, columnDef, rowObject)

  getClassForCutpoint = (cutpoints, value) ->
    floatVal = parseFloat(value)
    if floatVal < cutpoints[0].end
      'x'
    else if floatVal < cutpoints[1].end
      'o'
    else
      'check'

  getLabelForCutpoint = (cutpoints, value) ->
    floatVal = parseFloat(value)
    if floatVal < cutpoints[0].end
      cutpoints[0].label
    else if floatVal < cutpoints[1].end
      cutpoints[1].label
    else
      cutpoints[2].label

  createClusterFormatter = (cutpoints) ->
    {
      render: (row, cell, value, columnDef, rowObject) ->
        cluster = []
        for item, i in value
          itemClass = getClassForCutpoint(cutpoints, item.prob_cluster)
          itemLabel = getLabelForCutpoint(cutpoints, item.prob_cluster)
          ariaLabel = "#{item.code_cluster}: #{item.prob_cluster} (#{itemLabel})"
          cluster.push("<li class='cluster-item name-value-pair' aria-label='#{ariaLabel}'>
            <span class='cluster-code-info name'>
              <span class='cluster-code'>#{item.code_cluster}</span>
            </span>
            <span class='checks-and-x #{itemClass} value'>#{item.prob_cluster}</span>
          </li>")

        "<ul class='clusters name-value-pairs'>#{cluster.join('')}</ul>"

      text: (row, cell, value, columnDef, rowObject) ->
        clusters = []
        for item, i in value
          itemLabel = getLabelForCutpoint(cutpoints, item.prob_cluster)
          clusters.push "#{item.code_cluster} - #{item.prob_cluster} (#{itemLabel})"
        clusters.join(' , ')
    }

  createCheckAndXFormatter = (cutpoints) ->
    {
      render: (row, cell, value, columnDef, rowObject) ->
        cutpointClass = getClassForCutpoint(cutpoints, value)
        cutpointLabel = getLabelForCutpoint(cutpoints, value)
        ariaLabel = "#{value} (#{cutpointLabel})"
        "<span class='checks-and-x #{cutpointClass}' aria-label='#{ariaLabel}'>#{value}</span>"
      text: (row, cell, value, columnDef, rowObject) ->
        cutpointLabel = getLabelForCutpoint(cutpoints, value)
        "#{value} (#{cutpointLabel})"
    }


  class showItemsCorrect
    @getText: (row, cell, report_level, columnDef, rowObject) =>
      fraction = "#{rowObject['items_correct']}/#{rowObject['total_num_skill_items']}"
      # Show one or zero decimal places as needed
      percentage = parseFloat(rowObject['percent_correct'])
      percentage = +percentage.toFixed(1)
      [fraction, percentage]

    @render: (row, cell, report_level, columnDef, rowObject) =>
      [fraction, percentage] = @getText(row, cell, report_level, columnDef, rowObject)
      ariaLabel = "#{fraction.split('/').join(' out of ')}, (#{percentage}%)"
      "<span aria-label='#{ariaLabel}'><span>#{fraction} </span><span class='grayText'>(#{percentage}%)</span></span>"

    @text: (row, cell, report_level, columnDef, rowObject) =>
      [fraction, percentage] = @getText(row, cell, report_level, columnDef, rowObject)
      "#{fraction} (#{percentage}%)"

  class showTotalTimePerSkill
    @getText: (row, cell, report_level, columnDef, rowObject) =>
      rowObject['total_time_per_skill']

    @render: (row, cell, report_level, columnDef, rowObject) =>
      # Expect input like 00:12:34, remove the leading '00' hours if necessary
      # aria-label like "12 minutes, 34 seconds"
      labels = ['hours', 'minutes', 'seconds']
      hoursMinutesSeconds = @getText(row, cell, report_level, columnDef, rowObject)
      hasZeroHours = hoursMinutesSeconds.slice(0,2) == "00"
      value = if hasZeroHours then hoursMinutesSeconds.slice(3) else hoursMinutesSeconds
      ariaLabel = for idx, t of hoursMinutesSeconds.split(':')
        if t isnt '00'
          "#{parseInt(t)} #{labels[idx]}"
        else
          ""
      ariaLabel = ariaLabel.filter (i) -> i isnt ""
      "<span aria-label='#{ariaLabel}'>#{value}</span>"

    @text: (row, cell, report_level, columnDef, rowObject) =>
      @getText(row, cell, report_level, columnDef, rowObject)

  class showSkill
    @getText: (row, cell, report_level, columnDef, rowObject) =>
      rowObject['skill']

    @render: (row, cell, report_level, columnDef, rowObject) =>
      "<div class='cell-skill-holder'>#{rowObject['skill']}</div>"

    @text: (row, cell, report_level, columnDef, rowObject) =>
      @getText(row, cell, report_level, columnDef, rowObject)


  class HiddenRowDataWrapper
    # This wrapper checks if the cell should be greyed out with a message. Otherwise, normal rendering takes place.
    # You need to extend it with a shouldHide and a getHiddenMessage method
    constructor: (@formatter) ->

    render: (row, cell, report_level, columnDef, rowObject) =>
      if @shouldHide(rowObject)
        msg = @getHiddenMessage()
        "<div class='suppressed-row' title='#{msg}'>#{msg}</div>"
      else
        formatter_method = (@formatter.render || @formatter)
        formatter_method(row, cell, report_level, columnDef, rowObject)

    text: (row, cell, report_level, columnDef, rowObject) =>
      if @shouldHide(rowObject)
        if columnDef.display_suppression_msg
          #for grids with pagination, we only want this message for the first column, otherwise it's repeated in 508 exports
          @getHiddenMessage()
        else
          ''
      else
        formatter_method = (@formatter.text || @formatter)
        formatter_method(row, cell, report_level, columnDef, rowObject)


  mapping =
      showDeepValue: showDeepValue
      showApplicableDeepValue: showApplicableDeepValue
      showLink: showLink
      showLevelColoredValue: showLevelColoredValue
      showPassages: showPassages
      showPerformanceLevel: showPerformanceLevel
      showPopulationBar: showPopulationBar
      populationPopover: populationPopover
      showPercentile: showPercentile
      showSummary: showSummary
      showClaimAvg: showClaimAvg
      showCheckbox: showCheckbox
      showPercentage: showPercentage
      showGroupHeaderRow: showGroupHeaderRow
      showGradeName: showGradeName
      showNothing: showNothing
      showSurveyResponses: showSurveyResponses
      createCheckAndXFormatter:createCheckAndXFormatter
      createClusterFormatter: createClusterFormatter
      showItemsCorrect: showItemsCorrect
      showTotalTimePerSkill: showTotalTimePerSkill
      showSkill: showSkill

  createFormatter = (formatterName)->
    mapping[formatterName]

  createFormatter: createFormatter
  HiddenRowDataWrapper: HiddenRowDataWrapper
