define [
  'jquery',
  'react',
  'edwareUtil',
  'AssessmentReportDataAdapter'
  'moment'
], ($,
    React,
    util,
    AssessmentReportDataAdapter,
    moment) ->

  AssessmentReportDataAdapter = AssessmentReportDataAdapter.AssessmentReportDataAdapter

  class GridFilter

    constructor: () ->
      @contexts = []

    init: (context) ->
      @contexts = @contexts.filter (c) -> c.type isnt context.type
      @contexts.push context

    getContexts: () ->
      @contexts

    prepareData: (data, state) ->
      rows = data[state.asmtType][state.subject]
      for row in rows
        [month, day, year] = row.asmt_date.split('/')
        row.filterDate = moment.utc("#{year}-#{month}-#{day}").unix()
      @getRange(rows)

    getRange: (data) ->
      if not data or data.length is 0
        return {
          min: moment.utc().unix()
          max: moment.utc().unix()
        }
      max = data.reduce (item1, item2) ->
        date1 = item1.filterDate.valueOf()
        date2 = item2.filterDate.valueOf()
        return if date1 > date2 then item1 else item2
      min = data.reduce (item1, item2) ->
        date1 = item1.filterDate.valueOf()
        date2 = item2.filterDate.valueOf()
        return if date1 < date2 then item1 else item2
      return {
        min: min.filterDate
        max: max.filterDate
      }

    filterRows: (newFilter, data, view) ->
      asmtType = view.asmtType
      subject = view.subject
      rows = data[asmtType][subject]
      maxDate = newFilter['max']
      minDate = newFilter['min']
      rangeFilter = (rows, filter) ->
        temp = []
        for item in rows
          itemDate = item[filter['field']]
          temp.push(item) if itemDate >= minDate and itemDate <= maxDate
        temp
      newRows = rangeFilter(rows, newFilter)
      data[asmtType][subject] = newRows
      newRows

    filter: (data, view) ->
      dataSource = new AssessmentReportDataAdapter(data)
      @contexts[0].wrapper.onFilter(dataSource, view)
      dataSource

    unfilter: (data, view) ->
      dataSource = new AssessmentReportDataAdapter(data)
      @contexts[0].wrapper.onFilter(dataSource, view)
      asmtType = view.asmtType
      subject = view.subject
      rows = data[asmtType][subject]
      rows

      #TO DO, implement other filtering types. This is only Min-Max.

    destroy: (context) ->
      @contexts = @contexts.filter (c) -> c.type isnt context.type

  GridFilter: GridFilter
