define [
  'jquery',
  'evidenceStatementProvider',
  'expressivenessTooltipProvider',
  'edwareUtil'
], ($,
    EvidenceStatementProvider,
    ExpressivenessTooltipProvider
    util
) ->

  DEFAULT_CONFIG = {
    decorateColumns: false
    decorateSummaryCells: false
    decorateDataCells: false
    multiPopoverCells: false
  }


  class PopoverHelper

    createInfoIcon: (id) ->
      $('<div />', {
        'class': "font-icon info #{id}"
        'tabindex': '0'
      })

    initializePopover: (btn, content) ->
      hoverActive = false
      btn.popover {
        animation: true
        html: true
        placement: @getPopupPlacement
        trigger: 'focus' #hover is done manualy
        delay: { show: 100, hide: 300 }
        content: content
        container: 'body'
      }
      hoverCheck = () ->
        setTimeout (() ->
          if not $(".popover:hover").length and not btn.is(":hover")
            btn.popover 'hide'
            hoverActive = false
          else
            hoverCheck()
        ), 50
      btn.on 'mouseenter', () ->
        if not hoverActive
          btn.popover 'show'
          hoverActive = true
          hoverCheck()

    getPopupPlacement: (popoverElement, btn) ->
      popover = $ popoverElement
      # Needed for browser to calculate width of popover
      popWidth = popover.appendTo("body").width()
      popHeight = popover.height()
      popover.remove()
      util.popupPlacement btn, popWidth, popHeight + 10

    appendToHeader: (btn, node) ->
      colName = $(node).find('.slick-column-name')
      if colName.length
        btn.insertAfter(colName)
      else
        btn.appendTo(node)


  class ExtraInfoPopover

    constructor: (@provider, config) ->
      @options = $.extend {}, DEFAULT_CONFIG, config
      @hasHeaderGrid = false
      @popoverHelper = new PopoverHelper()

    init: (context) ->
      # we only want to do column post rendering on the visible column headers
      # if we have a header grid we are not going to do post rendering on column headers for body grid
      @hasHeaderGrid = @hasHeaderGrid or context.type is 'header'
      if @options.decorateColumns
        context.headerCellEventHandler = @createHeaderCellRenderedHandler context
        context.grid.onHeaderCellRendered.subscribe context.headerCellEventHandler
      if @options.decorateSummaryCells or @options.decorateDataCells
        context.asyncPostRenderHandler = @createDataCellDecorator context
        context.wrapper.registerOnAsyncPostRender context.asyncPostRenderHandler

    destroy: (context) ->
      context.grid.onHeaderCellRendered.unsubscribe context.headerCellEventHandler

    createHeaderCellRenderedHandler: (context) ->
        (e, args) =>
          if not @hasHeaderGrid or context.type is 'header'
            @decorateHeaderCell args.column, args.node, context,

    isSummaryRow: (row, context) ->
      context.wrapper.isSummaryRow(row)

    shouldTriggerDecoratorOnCell: (row, columnDef, context) ->
      (context.type is 'header' and @options.decorateSummaryCells and @isSummaryRow(row, context)) or
      (context.type is 'grid' and @options.decorateDataCells and not @isSummaryRow(row, context))

    createDataCellDecorator: (context) ->
      (cellNode, row, rowObject, columnDef) =>
        if @shouldTriggerDecoratorOnCell(rowObject, columnDef, context)
          @decorateDataCell rowObject, columnDef, cellNode, context

    decorateHeaderCell: (column, node, context) ->
      if @provider.headerNeedsPostRender?(column)
        @provider.doHeaderPostRender(node, column, @popoverHelper)

    decorateDataCell: (row, column, node, context) ->
      if @provider.cellNeedsPostRender?(row, column)
        @provider.doCellPostRender(row, column, node, @popoverHelper)


  class ParccStatesToolTipProvider

    @cellNeedsPostRender: (row, column) ->
      row.type is 'parcc' and column.field is 'name'

    @doCellPostRender: (row, column, node, popoverProvider) ->
      btn = popoverProvider.createInfoIcon column.field
      $(node).append btn
      popoverProvider.initializePopover btn, row.name

  class SkillToolTipProvider

    @cellNeedsPostRender: (row, column) ->
      column.id is 'skill'

    @doCellPostRender: (row, column, node, popoverProvider) ->
      btn = popoverProvider.createInfoIcon column.id
      $(node).append btn
      popoverProvider.initializePopover btn, row.skill


  class ColumnToolTipProvider

    @headerNeedsPostRender: (column) ->
      column?.toolTipPopover?

    @doHeaderPostRender: (node, column, popoverProvider) ->
      btn = popoverProvider.createInfoIcon(column.field)
      popoverProvider.appendToHeader btn, node
      popoverProvider.initializePopover btn, column.toolTipPopover


  class MinCellToolTipProvider

    @cellHasExtra: (row, column) ->
     (column.id is 'student_name' or column.id is 'name') and
     (not column.hideSuppression or not row.is_less_then_min_cell_size)


  ExtraInfoPopover: ExtraInfoPopover
  EvidenceStatementProvider: EvidenceStatementProvider
  ExpressivenessTooltipProvider: ExpressivenessTooltipProvider
  ColumnToolTipProvider: ColumnToolTipProvider
  SkillToolTipProvider: SkillToolTipProvider
  ParccStatesToolTipProvider: ParccStatesToolTipProvider
