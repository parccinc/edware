define ['jquery', 'mustache', 'edwareUtil'], ($, Mustache, util) ->

    class ColumnGroupHeaders

        constructor: (@options = {}, provider) ->

        init: (context) ->
            gridPluginConfig = context.wrapper.getConfig().ColumnGroupHeaders or {}
            context.options = $.extend {}, @options, gridPluginConfig
            context.headerCellEventHandler = @createHeaderCellRenderedHandler context
            context.grid.onHeaderCellRendered.subscribe context.headerCellEventHandler
            # set default wrapper
            context.groupHeader = context.options.container or '.slick-group-header'
            context.$groupHeaderContainer = $ context.groupHeader
            context.groups = {}


        destroy: (context) ->
            context.grid.onHeaderCellRendered.unsubscribe context.headerCellEventHandler
            $(context.groupHeader).empty()

        createHeaderCellRenderedHandler: (context) ->
            (e, args) =>
                @handleHeaderCellRendered e, args, context

        onViewSwitched: (state) ->
          #TO DO

        handleHeaderCellRendered: (e, args, context) ->
            column = args.column
            width = column.width
            group = column.group
            node = args.node
            if group
                if context.groups[group]
                    # already encountered this group heading
                    @updateExistingGroupHeader(column, context, node)
                else
                    # first time seeing group heading
                    @createNewGroupHeader(column, context, node)
                $(args.node).addClass 'sub-header'

        createNewGroupHeader: (column, context, node) ->
            group = column.group
            context.groups[group] = {}
            offset = context.$groupHeaderContainer.offset().left
            groupHeader = $("<span />", {
                "class": "slick-col-group-header #{group}"
                "text": group
                "role": "presentation"
                "aria-hidden": true
            }).css({
                "width": "#{column.width - 1}px",
                "position": "absolute",
                "left": $(node).offset().left - offset,
                "top": "3px"
            })
            context.$groupHeaderContainer.append groupHeader
            context.groups[group].node = groupHeader

        updateExistingGroupHeader: (column, context, node) ->
            group = column.group
            groupHeader = context.groups[group].node
            currentWidth = $(groupHeader).width()
            newWidth = currentWidth + column.width
            groupHeader.css({
                "width": "#{newWidth}px"
            })
