define ['jquery'], ($) ->

  KEYCODES = {
    enter: 13
    space: 32
  }
  ###
  # you need a separate instance for each edwareGridAdapter.
  ###
  class GridAccessibility

    constructor: (@provider, @options) ->
        @hasHeaderGrid = false

    getColumnIdForCell: (columns, cellNode) ->
      $(columns.children().get(cellNode.index())).attr('id')

    init: (context) ->
      if context.type is 'header'
        @hasHeaderGrid = true
      gridLabelMap = {
        header: 'Comparison'
        grid: 'Results'
      }
      # cache DOM elements
      context.$gridNode = $(context.grid.getCanvasNode()).parents('.report-grid')
      context.$headerRow = context.$gridNode.find('.slick-header-columns')

      # remove slickgrids difault tabbing behavior
      context.$gridNode.children(':first-child, :last-child').attr('tabindex', -1)

      context.$gridNode.attr('aria-label', gridLabelMap[context.type])

      context.onHeaderCellRenderedHandler = @createHeaderCellDecorator(context)
      context.grid.onHeaderCellRendered.subscribe context.onHeaderCellRenderedHandler
      context.onAsyncPostRenderHandler = @createDataCellDecorator(context)
      context.wrapper.registerOnAsyncPostRender context.onAsyncPostRenderHandler

    createHeaderCellDecorator: (context) ->
      (e, args) =>
        # can't be cached because we need to wait and see if there is a header grid
        # so tabbing through grids behaves as expected
        isSortableGrid = not @hasHeaderGrid or context.type is 'header'

        $node = $(args.node)
        context.$headerRow.attr({'role': 'row'})
        $node.attr({'role': 'columnHeader'})

        @decorateHeaderCell($node, args.column, isSortableGrid, context)

    decorateHeaderCell: ($cellNode, columnDef, isSortableGrid, context) ->
      # add accessible column name for AT
      accessibleHeaderText = @getAccessibileColumnName(columnDef)
      if accessibleHeaderText
        $cellNode.find('.slick-column-name')
        .attr({'aria-label': accessibleHeaderText})

      # enable sorting on header grid when we have one.
      # enable sorting on body grid otherwise
      if isSortableGrid and columnDef.sortable
        $cellNode.find('.slick-column-name').attr({'tabindex': '0'})
        $cellNode.find('.slick-column-name').attr({'role': 'presentation'})
        @bindSortEvent($cellNode, columnDef, context)

    bindSortEvent: ($cellNode, columnDef, context) ->
      $cellNode.on 'keydown', (keyEvent) =>
        if keyEvent.which is KEYCODES.space or keyEvent.which is KEYCODES.enter
          sortAsc = true
          currentSortCol = context.grid.getSortColumns()[0]
          if currentSortCol.columnId is columnDef.id
            sortAsc = not currentSortCol.sortAsc
          index = context.grid.getColumnIndex(columnDef.id)
          context.wrapper.setSortColumn(index, sortAsc)

    getAccessibileColumnName: (columnDef) ->
      # show text friendly name if we have one
      if columnDef.textName
        columnDef.textName
      else if columnDef.group
        "#{columnDef.group}"
      else
        ""

    createDataCellDecorator: (context) ->
      (cellNode, row, rowObject, columnDef) =>
        $cellNode = $(cellNode)
        # when the column has special AT text we need to add it the cell direcly
        # when the column doesn't have special AT text we can label it by the columnheader cell
        accessibleHeaderText = @getAccessibileColumnName(columnDef)
        if accessibleHeaderText
          $cellNode.attr('aria-label', accessibleHeaderText)
        else
          $cellNode.attr('aria-labelledby', @getColumnIdForCell(context.$headerRow, $cellNode))

    # we call initializeGrid directly from edwareGridAdapter because
    # it needs to be triggered in the same tick of the slickGrid rendering to dom
    # or the accessibility tree will not be updated
    initializeGrid: (grid) ->
      gridNode = $(grid.getCanvasNode()).parents('.report-grid')
      gridNode.attr({'role': 'grid', 'aria-readonly': true, 'aria-describedby': 'legend-description'})
      gridNode.find('.slick-row').attr('role', 'row')
      gridNode.find('.slick-cell').attr('role', 'gridcell')


    destroy: (context) ->
      context.grid.onHeaderCellRendered.unsubscribe context.onHeaderCellRenderedHandler
      context.wrapper.unregisterOnAsyncPostRender context.onAsyncPostRenderHandler
      # do we also need to unbind the keydown events?
