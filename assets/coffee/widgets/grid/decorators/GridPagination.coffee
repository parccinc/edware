define ['jquery', 'react', 'edwareUtil'], ($, React, util) ->

  Pagination = React.createClass

    getInitialState: () ->
      return { start : 1 }

    render: () ->
      if @props.data.canPageLeft
        leftButton = <button aria-label="Previous Page (#{@props.data.leftRemaining} columns)"  onClick={@props.data.onPageLeft} className="pull-left prev-arrow btn-link">Previous (<span>{@props.data.leftRemaining}</span> cols)</button>

      if @props.data.canPageRight
        rightButton = <button aria-label="Next Page (#{@props.data.rightRemaining} columns)" onClick={@props.data.onPageRight} className="pull-right next-arrow btn-link">Next (<span>{@props.data.rightRemaining}</span> cols)</button>

      <div aria-live="polite" aria-atomic="true">
        <div aria-label="Grid Column Pagination (showing #{@props.data.visibleColumns} of #{@props.data.totalColumns} columns)">
          {leftButton}
          {rightButton}
        </div>
      </div>

  class GridPagination

    constructor: (@options = {}) ->
      @showColumns = @options.showColumns
      @start = 1
      @contexts = []

    init: (context) ->
      gridPluginConfig = context.wrapper.getConfig().pagination or {}
      context.options = $.extend {}, @options, gridPluginConfig

      @contexts.push context
      if $('.paginate-bar').length < 1 then $('[data-component-header-bar]').before('<div class="paginate-bar"></div>')
      @component = React.render(<Pagination data={@getPaginationProps()} />, $('.paginate-bar').get(0))

    getContexts: () ->
      @contexts

    getComponent: () ->
      @component

    getTotalNumberOfColumns: () ->
      @contexts[0].columns.length

    canPageLeft: () =>
      @start > 1

    pageLeft: () =>
      @start = @start - (@showColumns - 1)
      @start = 1 if @start < 1
      @setColumns @getVisibleColumns()

    pageRight: () =>
      @start = @start + (@showColumns - 1)
      @setColumns @getVisibleColumns()

    canPageRight: () =>
      @start + @showColumns <= @getTotalNumberOfColumns()

    getRemainingOnLeft: () ->
      Math.min(@showColumns - 1, @start)

    getRemainingOnRight: () ->
      Math.min(@showColumns - 1, @getTotalNumberOfColumns() - (@start + @showColumns - 1))

    prevPage: (e) =>
      e.preventDefault()
      @pageLeft() if @canPageLeft()
      @updateComponent()

    nextPage: (e) =>
      e.preventDefault()
      @pageRight() if @canPageRight()
      @updateComponent()

    onViewSwitched: (state) ->
      @start = 1
      @setColumns @getVisibleColumns()
      @updateComponent()
    getPaginationProps: () ->
      {
        data : {
          onPageLeft: @prevPage,
          onPageRight: @nextPage,
          canPageLeft: @canPageLeft(),
          canPageRight: @canPageRight(),
          leftRemaining: @getRemainingOnLeft(),
          rightRemaining: @getRemainingOnRight(),
          visibleColumns: @getVisibleColumns().length,
          totalColumns: @getTotalNumberOfColumns()
        }
      }
    updateComponent: () ->
      @component.setProps(@getPaginationProps())

    getVisibleColumns: () ->
      totalColumns = @getTotalNumberOfColumns()
      start = @start
      end = Math.min totalColumns, start + (@showColumns - 1)
      @contexts[0].columns.slice(0, 1).concat(@contexts[0].columns.slice(start, end))

    setColumns: (columns) ->
      @contexts[0].wrapper.setColumns(columns)

    destroy: (context) ->
      @contexts = @contexts.filter (c) -> c.type isnt context.type
      React.unmountComponentAtNode($('.paginate-bar').get(0))

  GridPagination: GridPagination
  Pagination: Pagination
