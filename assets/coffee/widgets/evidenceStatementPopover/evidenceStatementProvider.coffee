define ['mustache'], (Mustache) ->

  class EvidenceStatementProvider

    @headerNeedsPostRender: (column) ->
      column.hasInfo

    @doHeaderPostRender: (node, column, popoverProvider) ->
      btn = popoverProvider.createInfoIcon(column.field)
      popoverProvider.appendToHeader(btn, node)

      popoverProvider.initializePopover btn, Mustache.render('
        <ul class="name-value-pairs">
          <li class="name-value-pair list-item">
            <span class="name">Item type</name>
            <span class="value">{{item_type}}</span>
          </li>
          <li class="name-value-pair list-item">
            <span class="name">Responses</span>
            <span class="value">{{item_num_responses}}</span>
          </li>
        </ul>
        <ul class="name-value-pairs">
          <li class="name-value-pair list-item">
            <span class="name">standards</span>
            <span class="value">{{#standards}}{{.}}<span class="values-delimiter"></span>{{/standards}}</span>
          </li>
        </ul>', column)
