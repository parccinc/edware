define ['edwareConstants', 'edwareUtil'], (Constants, util) ->

  class SelectorUtil

    @availableForReport: (option, report, allowDefault = true) ->
      states = option['for'] or {}
      retDefault = true
      for conditionKey, conditionValues of states
        retDefault = false
        isNegated = conditionKey[0] is '!'
        if isNegated
          conditionKey = conditionKey.slice(1)

        matches = report[conditionKey] in conditionValues

        ###
        # short-circuit if: it matched the state but it was negative key (i.e "!view") \
        #  or if it didnt match when it should've
        # `if (matches and isNegated) or (not matches and not isNegated)`
        ###
        if matches is isNegated
          return false

      ###
      # if its not default and it got here it passed so we return TRUE
      # its not default when (options['for'] existed and had atleast one key/val pair)
      ###
      not retDefault or allowDefault

    @buildOptionsForReport: (optionsList, report, settings = {}) ->
      results = []
      for result in optionsList
        settingsForChildren = $.extend true, {}, settings
        if not @availableForReport(result, report)
          continue
        isGroupWrapper = not result.label and result.options and result.options.length # is not a visual node
        if result.states
          # children inherit states
          # child can override parent states
          settingsForChildren = $.extend true, {}, {states: result.states}, settingsForChildren
        if result.options and result.options.length
          children = @buildOptionsForReport(result.options, report, settingsForChildren)
        if not isGroupWrapper
          # copy item and add proccessed children and inherit states
          item = $.extend true, {}, {options: null, states: settingsForChildren.states}, result
          item.options = children or []
          results.push item
        else
          results = results.concat(children or [])
      results

    @flattenOptions: (optionsList = []) ->
      items = []
      for result in optionsList
        items.push result
        if result.options and result.options.length
          items = items.concat @flattenOptions(result.options)
      items

    @getForCurrentState: (reportState, options) ->
      options = @flattenOptions options
      for item in options
        if item.header or item.subheader
          continue
        if @isCurrentState(reportState, item.states)
          return item

    @isCurrentState: (reportState, item) ->
      for stateName, stateValue of item
        if reportState[stateName] isnt stateValue
          return false
      return true

    # TODO should be moved out into ListReport
    @currentReport: (appState) ->
      Constants.REPORT_LEVEL_NAME[util.getReportLevel(appState)].toUpperCase()

    @currentReportState: (appState) ->
      reportState = $.extend true, {}, appState
      reportState.report = @currentReport(reportState)
      reportState
