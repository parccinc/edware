define [
  'edwareConstants'
  'edwareUtil'
], (Constants, edwareUtil) ->

  class ContextSecurity

    constructor: (user, @context) ->
      @user = user._User__info

    canDrillDown: (rowObject) ->
      acceptedValues = @context.permissions.pii.guid
      rowValue = rowObject.guid
      if not acceptedValues
        canDrillDown = true
      else
        canDrillDown = acceptedValues.indexOf(rowValue) > -1
      canDrillDown

    canExportAny: () ->
      @context.permissions.display_extract

    canExport: (key) ->
      @context.permissions[key]

    exportableTypesList: (allExportTypes) ->
      (expType for expType in allExportTypes when @canExport(expType.permission))

    canAccessAnalytics: () ->
      "PII_ANALYTICS" in @user.roles

    canAccessPII: (rowObject) ->
      @context.permissions.pii.all

    hasStaffPermission: (rowObject) ->
      rowObject.student_count > 0

  instance = (user, context) ->
    @contextSecurity = new ContextSecurity user, context


  get = () ->
    @contextSecurity

  {
    instance: instance
    get: get
  }
