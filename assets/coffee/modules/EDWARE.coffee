define ["jquery", 'reactbootstrap'], ($, ReactBootstrap) ->

  # define namespace
  EDWARE = EDWARE or {}

  EDWARE

  # bug from wkhtmltopdf causes issue when we use bind https://github.com/masayuki0812/c3/issues/552
  # Function::bind = Function::bind or (thisp) ->
  #   fn = this
  #   ->
  #     fn.apply thisp, arguments

  # React bootstrap objects
  for name, widget of ReactBootstrap
    window[name] = widget
