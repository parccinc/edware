define [
  'jquery'
  'edwareConstants'
  'edwareDataProxy'
  'edwareUtil'
  'edwareReportInfoBar'
  'edwareGrid'
  'EdwareApplication'
  'edwareLegend'
  'edwareFootnote'
  'SlickColumnGroupHeaders'
  'selectorUtil'
  'DiagnosticColumnModel'
  'SlickExtraInfoPopover'
  'clusterTooltipProvider'
], ($,
    Constants,
    edwareDataProxy,
    utils,
    reportInfoBar,
    edwareGrid,
    App,
    legend,
    footnote,
    SlickColumnGroupHeaders,
    SelectorUtil,
    DiagnosticColumnModel,
    GridDecoratorPlugin,
    clusterTooltipProvider
  ) ->

  reportName = Constants.REPORT_NAME.DIAGNOSTIC_ISR

  EVENTS = Constants.EVENTS

  SlickViewColumnModel = DiagnosticColumnModel
  DataSource = edwareGrid.SlickDataSource
  Grid = edwareGrid.edwareGridAdapter
  ClusterToolTipProvider = clusterTooltipProvider.ClusterToolTipProvider

  App.initialize({
    asmtType: Constants.ASMT_TYPE.DIAGNOSTIC
    result: Constants.RESULT_TYPES.OVERALL
  })


  class StudentDiagnosticReport

    constructor: (@config) ->
      if App.getState().asmtType is Constants.ASMT_TYPE.SUMMATIVE
        @redirectSummative()
        return
      @initialize()
      @grids = []

    initialize: () ->
      @config.viewsList = []
      App.subscribe(EVENTS.ASSESSMENT_TYPE_CHANGE, @asmtTypeChange)
      App.subscribe(EVENTS.SUBJECT_CHANGE, @onSubjectChange)
      App.subscribe(Constants.EVENTS.YEAR_CHANGE, @onYearChange)
      @groupedHeaders = new SlickColumnGroupHeaders()
      @tooltipPopover = new GridDecoratorPlugin.ExtraInfoPopover(GridDecoratorPlugin.ColumnToolTipProvider, {
        decorateColumns: true
      })

      @clusterTooltipPopover = new GridDecoratorPlugin.ExtraInfoPopover(new ClusterToolTipProvider(@config.clusters), {
        decorateDataCells: true
      })

      @skillToolTipProvider = new GridDecoratorPlugin.ExtraInfoPopover(GridDecoratorPlugin.SkillToolTipProvider, {
        decorateDataCells: true
      })

      @fetchData()

    render: () ->
      state= App.getState()
      @config.subjectsList = @data.subjects
      numberOfGrids = @createGrids()
      infoBarOptions = {
        data: @data
        config: @config
        reverseBreadcrumb: true
        isPdf: false
        yearSelector: true
        resultSelector: true
        filterResultsList: true
        count: numberOfGrids
      }
      @reportBar = reportInfoBar.create infoBarOptions
      @footnote = @createFootnote(state)
      @legend = legend.create @getLegendConfig(state)

    getLegendData: () ->
      legends = []
      # combine legend and diagnostic legends config. the legend widget filters out the ones we dont need
      # we should be able to get rid of this when summative and diagnostic module_config are separated
      (legends = legends.concat @config[legendConf] for legendConf in @config.legends)
      legends

    getLegendConfig: (state) ->
      {
        labels: @config.labels
        data: @getLegendData()
        cutpoints: @config.cutpoints
        reportState: state
      }

    getFootnoteConfig: (state) ->
      {
        labels: @config.labels
        state: state
        show: true
      }

    createFootnote: (state) ->
      $('#main').append $("<div id='footnote'></div>")
      footnote.create @getFootnoteConfig(state)

    createGrids: () ->
      state = App.getState()
      gridData = @getGridData @data, state
      # for gridName, grid of gridData
      columnModel = new SlickViewColumnModel(@config.grid, {
        cutpoints: @config.cutpoints,
        baseColumns: @config.diagnosticBaseColumns
      })
      viewTemplate = $("#view-grid-template").text()
      $('#main').empty()
      reportsCount = 0
      for gridConfig in @config.grids
        if not gridData[gridConfig.view] or gridData[gridConfig.view].length is 0
          continue
        else
          reportsCount += 1
        container = $(viewTemplate)
          .attr('data-component-grid-container', gridConfig.view)
        id = gridConfig.name.replace(/\s+/g, '').toLowerCase()
        id = "#{id}_gridWrapper"
        container.find('[data-template-grid-header]').attr('id', id).html(gridConfig.name)
        container.find('[data-component-grid]').attr('aria-labelledby', id)
        $('#main').append container
        gridConfig = $.extend gridConfig, @createGrid(gridData[gridConfig.view], gridConfig.view, columnModel, container)
        gridConfig.gridObj.registerPlugin @groupedHeaders
        gridConfig.gridObj.registerPlugin @tooltipPopover
        gridConfig.gridObj.registerPlugin @clusterTooltipPopover
        gridConfig.gridObj.registerPlugin @skillToolTipProvider
        gridConfig.gridObj.loadView($.extend(true, {}, state, {view: gridConfig.view}))
        gridConfig.gridObj.setSortColumn(1, false)
      reportsCount


    createGrid: (data, view, columnModel, container) ->
      dataSource = new SlickViewDataSource(data, view)
      gridObj = new Grid(dataSource, columnModel, {
        gridElement: container.find('[data-component-grid]'),
        headerElement: false,
        ColumnGroupHeaders: {
          container: container.find('.slick-group-header')
        }
      })
      {
        dataSource: dataSource,
        gridObj: gridObj
      }

    getGridData: (data, state) ->
      diagnosticData = data.assessments[Constants.ASMT_TYPE.DIAGNOSTIC]
      subjectViews = diagnosticData[state.subject]
      subjectViews

    asmtTypeChange: (newValue, oldValue, key, evnt) =>
      if newValue is Constants.ASMT_TYPE.SUMMATIVE
        @redirectSummative()

    onSubjectChange: (newValue, oldValue, key, evnt) =>
      @fetchData()

    onYearChange: () =>
      @fetchData()

    redirectSummative: () ->
      document.location.href = utils.generateUrl('studentSummativeReport.html', App.getState())

    getReportTitle: () ->
      items = @data.context.items
      [..., finalBreadcrumbEntry] = items
      subtitle = " #{@config.labels.students}"
      if finalBreadcrumbEntry.isCourse
        title = finalBreadcrumbEntry.name
      else
        title = "#{@config.labels.grade} #{finalBreadcrumbEntry.name}"
      [title, subtitle]

    fetchData: () ->
      params = App.getState()
      params['asmtType'] = Constants.ASMT_TYPE.DIAGNOSTIC
      # TODO: better way to exclude result in request
      delete params['result']
      loadingData = edwareDataProxy.getDatafromSource "/data/diagnostic_student_report", {
        method: "POST"
        params: params
      }
      loadingData.done (data) =>
        @data = data
        @render()

  class SlickViewDataSource extends DataSource

    constructor: (@viewRows, @name) ->
      super @viewRows

    getView: () ->
      @viewRows

    getViewType: () ->
      @name


  edwareDataProxy.getDataForReport(Constants.REPORT_JSON_NAME.DIAGNOSTIC_ISR).done (config) ->
    new StudentDiagnosticReport(config)
