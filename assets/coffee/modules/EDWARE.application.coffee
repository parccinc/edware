define [
  'jquery',
  'edwareConstants',
  'edwareUtil',
  'history'
], ($, Constants, edwareUtil) ->

  DEFAULT_SETTING = {
    asmtType: Constants.ASMT_TYPE.SUMMATIVE
    result: Constants.RESULT_TYPES.OVERALL
  }

  class Application
    #@Constants: Constants

    @EventBusDefaults: {
      'unique': true,
      'memory': false,
      'stopOnFalse': false
    }

    constructor: () ->
      # COMPOSITE: Application Constants
      @Constants = Constants
      # key val config, state and others...
      @settings = {}

      # Object to trigger and listen for events on
      @callbacks = {}
      @globalCallbacks = $.Callbacks()
      @globallySilent = true

    initialize: (@persistedDefaults, defaults) ->
      @globallySilent = false
      state = @getInitialState()
      # set defaults
      if state
        @set state, true, true


    setState: (key, value, silent = false) ->
      if $.type(key) is "object"
        if value is null
          value = silent
        return @set key, value, true
      @set key, value, silent, true

    getState: () ->
      $.extend {}, @settings

    getInitialState: () ->
      params = edwareUtil.getUrlParams()
      $.extend {}, DEFAULT_SETTING, @persistedDefaults, params

    persistState: (key, value) ->
      params = edwareUtil.getUrlParams()
      params[key] = value
      params = @removeEmpty(params)
      History.replaceState({key: value}, '', "?#{edwareUtil.setUrlParams(params)}")

    removeEmpty: (params) ->
      for key, value of params
        if typeof value is 'undefined' or value is null
          delete params[key]
      params

    setValue: (key, value, silent = false) ->
      if $.type(key) is "object"
        if value is null
          value = silent
        return @set key, value, false
      @set key, value, silent, false

    set: (key, value, silent = false, remember = false) ->
      if $.type(key) is "string" and typeof value isnt 'undefined'
        settings = {}
        settings[key] = value
      else if $.type(key) is "object"
        settings = key
        remember = silent
        silent = value

      if @globallySilent
        remember = false

      changes = []
      oldVal = {}
      newVal = {}
      for key, value of settings
        oldVal[key] = @settings[key]
        newVal[key] = value
        changes.push key
      evntObj = {before: true, after: false}
      if not silent and changes.length
        @globalCallbacks.fireWith evntObj, [newVal, oldVal, changes, evntObj]
      for key, newValue of settings
        @settings[key] = newValue
        if remember
          @persistState key, newValue
        if @callbacks[key] and not silent
          @callbacks[key].fireWith {before: false, after: true}, [newValue, oldVal[key], key, {before: false, after: true}]
      if not silent and changes.length
        evntObj = {before: false, after: true}
        @globalCallbacks.fireWith {before: false, after: true}, [newVal, oldVal, changes, evntObj]
      @settings = @removeEmpty(@settings)
      newVal

    get: (key) ->
      @settings[key]

    keepState: () ->
      @globallySilent = false

    forgetState: () ->
      @globallySilent = true

    events: () ->
      Object.keys @settings
    # application state observers

    ###
    # helper method, checks if a value actually changed
    ###
    hasChanged: (key, newValues, oldValues) ->
      newValues[key] and newValues[key] isnt oldValues[key]

    subscribe: (key, action) ->
      if action
        if not @callbacks[key]
          flags = (flag for flag, bool of Application.EventBusDefaults when bool)
          @callbacks[key] = $.Callbacks(flags.join(" "))
        @callbacks[key].add action
      else if $.type(key) is "function"
        @globalCallbacks.add key

    unsubscribe: (key, action) ->
      if $.type(key) is "function"
        return @globalCallbacks.remove(key)
      @callbacks[key].remove action

    # used for testing
    createApp: () ->
      new Application()

  ###
  # Return Singleton Instance
  ###
  new Application()
