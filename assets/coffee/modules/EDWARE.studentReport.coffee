define [
  'edwareConstants'
  'EdwareApplication'
  'edwareUtil'
], (Constants,
    App,
    Utils
  ) ->

  reportName = Constants.REPORT_NAME.ISR

  App.initialize({
    asmtType: Constants.ASMT_TYPE.SUMMATIVE
    result: Constants.RESULT_TYPES.OVERALL
  })

  urls = {
    SUMMATIVE: 'studentSummativeReport.html'
    DIAGNOSTIC: 'studentDiagnosticReport.html'
  }
  state = App.getState()
  reportType = state.asmtType
  url = urls[reportType]
  document.location.href = Utils.generateUrl(url, state)
