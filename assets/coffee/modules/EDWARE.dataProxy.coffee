define [
  'jquery'
  'mustache'
  'edwareLoadingMask'
  'edwareConstants'
  'edwareClientStorage'
  'EdwareApplication'
  'edwareUtil'
], ($, Mustache, edwareLoadingMask, Constants, EdwareClientStorage, App, util) ->

  gradeCourseStorage = new EdwareClientStorage(null, true)
  userInfo = null
  language = 'en'
  lastFocus = null
  URLs = {
    labels: "../data/#{language}/common/labels.json"
    filters: "../data/#{language}/filter/filter.json"
    common: "../data/#{language}/common/common.json"
    diagnostics: "../data/#{language}/common/diagnostic.json"
    helpContent: "../data/#{language}/content/helpContent.json"
    clusters: "../data/#{language}/common/clusters.json"
  }

  # Additional data files used by specific reports
  REPORT_DATA = {
    LOS: ["../data/en/common/studentRosterReport.json", URLs.filters]
    CPOP: ["../data/en/common/comparingPopulationsReport.json", URLs.filters]
    SUMMATIVE_ISR: ["../data/en/common/isr/summativeReport.json"]
    DIAGNOSTIC_ISR: ["../data/en/common/isr/diagnosticReport.json", URLs.diagnostics, URLs.clusters]
    SCHRPT: ["../data/en/common/schoolReport.json", URLs.filters, URLs.diagnostics, URLs.clusters]
  }

  # setup URLs for report's specific JSON
  for reportName, fileName of Constants.REPORT_JSON_NAME
    URLs[fileName] = REPORT_DATA[reportName]

  DEFAULT_SETTING = {
    type: 'GET'
    data: ''
    async: true
    dataType: 'json'
    contentType: 'application/json'
  }

  getErrorPage = ->
    "/assets/public/error.html"

  onError = (xhr, ajaxOptions, thrownError, redirectOnError) ->
    # Read the redirect url on 401 Unauthorized Error
    responseHeader = xhr.getResponseHeader('Content-Type')
    # redirect to login page
    if xhr.status is 401 and /application\/json/.test(responseHeader)
      return location.href = JSON.parse(xhr.responseText).redirect
    location.href = getErrorPage() if redirectOnError

  getDatafromSource = (sourceURL, options) ->
    if not sourceURL or not $.type(sourceURL) in ['string', 'array']
      return new TypeError('sourceURL invalid')

    options = options or {}
    deferred = $.Deferred()
    dataLoader = edwareLoadingMask.create({context: '<div></div>'})
    dataLoader.show()
    getUserInfo().then (data) ->
      gradeCourseStorage.setKey(data.user_info._User__info.guid)
      user = data.user_info
      if checkAndSetDefaults(options)
        fetchData(sourceURL, options).done (data) ->
          data = $.extend {}, data, {user_info: user}
          deferred.resolve data
    deferred.promise()

  checkAndSetDefaults = (options) ->
    if not options.params
      return true
    cached = gradeCourseStorage.load()
    if not options?.params?.subject
      if not cached?.subject
        edwareLoadingMask.destroy()
        navigateToDefaultsPage(options)
        return false
      else
        options.params.subject = cached.subject
    toCache = {}
    toCache.subject = options.params.subject
    gradeCourseStorage.update(toCache)
    App.setState 'subject', options.params.subject, true

    if not options?.params?.gradeCourse and util.getReportLevel(options.params) < Constants.REPORT_LEVELS.SCHOOL
      if not cached?[options?.params?.subject]
        edwareLoadingMask.destroy()
        navigateToDefaultsPage(options)
        return false
      else
        options.params.gradeCourse = cached[options.params.subject]

    toCache = {}
    toCache[options.params.subject] = options.params.gradeCourse
    gradeCourseStorage.update(toCache)
    App.setState 'gradeCourse', options.params.gradeCourse, true
    true

  navigateToDefaultsPage = (options) ->
    if window.location.pathname isnt '/assets/html/initialSetup.html'
      gradeCourseStorage.update({previousUrl: window.location.pathname, previousState: App.getState()})
      location = "/assets/html/initialSetup.html"
      if options?.params?.subject
        location = "#{location}?subject=#{options.params.subject}"
      window.location.href = location

  fetchData = (sourceURL, options) ->
    # define if redirect to error page in case error occurs on server side
    if options.hasOwnProperty('redirectOnError')
      redirectOnError = options.redirectOnError
    else
      redirectOnError = true

    config = $.extend {}, DEFAULT_SETTING
    # setup settings
    config.data = JSON.stringify options.params if options.params
    config.type = options.method if options.method

    dataLoader = edwareLoadingMask.create({context: '<div></div>'})
    dataLoader.show()

    sourceURL = [ sourceURL ] if $.type(sourceURL) is 'string'
    deferreds = for url in sourceURL
      $.ajax url, config

    defer = $.Deferred()
    btnGroup = $(document.activeElement).parents('.btn-group')

    if $(document.activeElement).parents('.dropdown-menu').length > 0
      lastFocus = btnGroup.find('button').andSelf()
    else
      lastFocus = document.activeElement
    $.when.apply($, deferreds)
      .always ->
        edwareLoadingMask.destroy()
      .done ->
        if $.type(arguments[1]) is 'string'
          # single ajax call response
          data = arguments[0]
        else
          data = {} # multiple ajax calls' response
          for args in arguments
            $.extend true, data, args[0]
        if lastFocus and $(lastFocus).parent().length > 0
          $(lastFocus).focus()
        defer.resolve data
      .fail (xhr, ajaxOptions, thrownError) ->
        defer.reject arguments
        onError xhr, ajaxOptions, thrownError, redirectOnError
    defer.promise()


  getDataForReport = (reportName) ->
    reportUrl = URLs[reportName]
    reportUrl ?= []
    resources = [URLs.common, URLs.labels].concat(reportUrl)
    defer = $.Deferred()
    getDatafromSource(resources).done (data) ->
      defer.resolve data
    defer.promise()

  getDataForHelpContent = ->
    getDatafromSource [URLs.helpContent]

  getUserInfo = (extraParams) ->
    deferred = $.Deferred()
    if userInfo
      deferred.resolve(userInfo)
    else
      $.ajax
        url: '/services/userinfo'
        type: 'POST'
        dataType: 'json'
        async: true
        success: (data) =>
          data = $.extend {}, data, extraParams
          userInfo = data
          deferred.resolve(data)
        error: (xhr, textStatus, error) ->
          deferred.reject()
          responseHeader = xhr.getResponseHeader('Content-Type')
          if xhr.status is 401 and /application\/json/.test(responseHeader)
            return location.href = JSON.parse(xhr.responseText).redirect

    deferred.promise()

  {
    fetchData: fetchData
    getDatafromSource: getDatafromSource
    getDataForReport: getDataForReport
    getDataForHelpContent: getDataForHelpContent
  }
