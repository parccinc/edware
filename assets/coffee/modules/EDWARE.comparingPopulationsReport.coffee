define [
  'jquery'
  'edwareConstants'
  'edwareDataProxy'
  'edwareReportInfoBar'
  'edwareGrid'
  'edwareGrowthChart'
  'edwareLegend'
  'edwareFootnote'
  'EdwareApplication'
  'edwareGrowthSummary'
  'edwareFilterBar'
  'edwareUtil'
  'edwareContextSecurity'
  'SlickColumnGroupHeaders'
  'listReport'
  'csvExport'
  'edwareYearSelector'
  'edwareGridFormatters'
  'SlickExtraInfoPopover'
  'edwareClientStorage'
], ($,
  Constants,
  edwareDataProxy,
  reportInfoBar,
  edwareGrid,
  growthChart,
  legend,
  footnote,
  App,
  edwareGrowthSummary,
  edwareFilterBar,
  edwareUtil,
  ContextSecurity
  SlickColumnGroupHeaders,
  ListReport,
  exporter
  YearSelector
  edwareGridFormatters
  GridDecoratorPlugin,
  EdwareClientStorage
) ->

  SlickDataSource = edwareGrid.SlickDataSource
  SlickColumnModel = edwareGrid.SlickColumnModel
  SlickSummaryDataSource = edwareGrid.SlickSummaryDataSource
  FilterUtil = edwareFilterBar.FilterUtil
  reportName = Constants.REPORT_JSON_NAME.CPOP
  EVENTS = Constants.EVENTS
  ParrcStatesProvider = GridDecoratorPlugin.ParccStatesToolTipProvider
  gradeCourseStorage = new EdwareClientStorage()

  App.initialize {
    view: Constants.VIEWS.PERFORMANCE
    asmtType: Constants.ASMT_TYPE.SUMMATIVE
    result: Constants.RESULT_TYPES.OVERALL
    year: YearSelector.Utils.getDefaultSelectedYear().value
  }

  App.setState('asmtType', 'SUMMATIVE')

  ReportTypeToName = ['consortium', 'state', 'district']

  getReportLevel = () ->
    edwareUtil.getReportLevel(App.getState())

  ensureCompareSource = (source, isParccSummaryAvailable) ->
    source or (if isParccSummaryAvailable then 'parcc' else 'state')

  class ComparingPopulationsConfigBuilder

    constructor: (@config) ->
      # We persist view and result params
      # It's possible that we don't want this for all levels
      @requiredParams = ['view', 'result']
      @nextParam = {}
      @nextIgnoreParams = []
      @loading = false
      @initialize()

    initialize: () ->
      @setReportType()
      @setRequiredQueryParams()
      @setConfigForNextHierarchy()

    setReportType: () ->
      @reportType = edwareUtil.getReportType(App.getState())
      @reportLevel = getReportLevel()

    setRequiredQueryParams: () ->
      for entity in @config.slice(0, @reportLevel + 1)
        @requiredParams.push entity.queryParam if entity.queryParam

    setConfigForNextHierarchy: () ->
      nextEntity = @config[@reportLevel + 1]
      paramName = nextEntity.queryParam
      @nextLink = nextEntity.link
      @nextParam[paramName] = 'guid'
      @nextEntityType = nextEntity.type
      @nextIgnoreParams = nextEntity.ignoreParams if nextEntity.ignoreParams

    getNextHierarchyInfo: () ->
      # Data used to construct next hierarchy
      {
        name: @nextEntityType
        link: @nextLink
        param: @nextParam
        queryParamKeys: @requiredParams
        ignoreParams: @nextIgnoreParams
      }


  class ComparingPopulationsReport

    constructor: (@config) ->
      @initialize()

    initialize: () ->
      # bind events
      App.subscribe(EVENTS.SUBJECT_CHANGE, @onSubjectChange)
      App.subscribe(EVENTS.VIEW_CHANGE, @onViewChange)
      App.subscribe(EVENTS.RESULT_CHANGE, @onResultChange)
      App.subscribe(EVENTS.SUMMARY_CHANGE, @onSummaryChange)
      App.subscribe(EVENTS.GRADE_COURSE_CHANGE, @onGradeCourseChange)
      App.subscribe(EVENTS.YEAR_CHANGE, @onYearChange)
      App.subscribe(EVENTS.FILTER_CHANGE, @onFilterChange)
      App.subscribe(EVENTS.DOWNLOAD_CSV, @onDownloadCSV)

      @groupedHeaders = new SlickColumnGroupHeaders()
      @parccStatesToolTip = new GridDecoratorPlugin.ExtraInfoPopover ParrcStatesProvider, {
        decorateSummaryCells: true
      }
      @fetchData()

    onDownloadCSV: () =>
      report = ListReport.buildForReport this, App.getState()
      csvExporter = new exporter.builder report, @config.labels
      downloader = (new exporter.downloader()).create()
      downloader(csvExporter.build(), csvExporter.buildFileName(), 'application/octet-stream')

    getReportType: () ->
      ReportTypeToName[edwareUtil.getReportType(App.getState())]
      # ReportLevelToName[getReportLevel()]

    loadPage: () ->
      @destroyGrid()
      @destroyGrowthChart()
      state = App.getState()
      isPerformanceView = state.view is Constants.VIEWS.PERFORMANCE
      isGrowthView = state.view is Constants.VIEWS.GROWTH

      [title, subtitle] = @getReportTitle()
      assessmentCount = edwareUtil.getAssessmentCount(@data.assessments, state)
      if assessmentCount > 0
        @createGrid()
      subtitle = edwareUtil.pluralize(subtitle, assessmentCount)
      reportBarOptions = {
        data: @data
        config: @config
        title: title
        filters: true
        gradeCourseSelector: true
        resultSelector: true
        viewSelector: !!edwareUtil.getReportType(App.getState())
        filterResultsList: isGrowthView
        yearSelector: false
        subtitle: " #{subtitle}"
        hasCompareBarDisplayed: isPerformanceView
        count: assessmentCount
      }

      reportBarOptions.compare = EVENTS.SUMMARY_CHANGE if isPerformanceView
      @reportBar = reportInfoBar.create reportBarOptions
      App.setValue(EVENTS.SUMMARY_CHANGE, @getDefaultSelectedForReportType()) # scope of report should be enabled by default
      @footnote = footnote.create @getFootnoteConfig(state, assessmentCount)
      @legend = legend.create @getLegendConfig(state)
      if assessmentCount > 0
        @updateSubtitle()
        if App.getState().view is Constants.VIEWS.GROWTH
          @createGrowthChart(state.compare_source)

    getLegendData: () ->
      if @data.legend
        @data.legend.type = 'performanceLevels'
        [@data.legend]
      else
        @config.legend

    getLegendConfig: (state) ->
      {
        labels: @config.labels
        data: @getLegendData()
        reportState: state
        ccReady: state.result is Constants.RESULT_TYPES.OVERALL and state.gradeCourse in ['Grade 11'].concat(Constants.CCR_COURSES)
      }

    getFootnoteConfig: (state, assessmentCount) ->
      {
        labels: @config.labels
        state: state,
        show: assessmentCount > 0
      }

    getReportTitle: () ->
      items = @data.context.items
      [..., finalBreadcrumbEntry] = items
      subtitle = @config.breadcrumb.items[items.length].type
      if not edwareUtil.getReportType(App.getState()) then title = 'All PARCC States' else title = finalBreadcrumbEntry.name
      [title, subtitle]

    getDefaultSelectedForReportType: () ->
      ret = {}
      summaries = @data['summary']?.SUMMATIVE[App.getState().subject]
      if summaries and summaries.length and not summaries[summaries.length - 1].disabled
        ret[summaries[summaries.length - 1].type] = true
      return ret

    createGrid: () ->
      currentView = App.getState().view
      currentResultType = App.getState().result
      # Deep copy of the data, since we're injected isHidden flag into it for growth view
      data = $.extend true, {}, @data

      if currentView is Constants.VIEWS.PERFORMANCE
        performanceType = Constants.PERFORMANCE_RESULT_COLUMN_MAP[currentResultType]
        data.columns = @config.grid[currentView][performanceType]
        dataSourceObj = ComparingPopulationsDataSource
      else
        data.columns = @config.grid[currentView]
        dataSourceObj = ComparingPopulationsGrowthDataSource

      @gridObj = edwareGrid.create {
        options: {
          assessments: data['assessments']
          columns: data['columns']
          summary: data['summary']
          reportConfig: @config
        }
        dataSource: dataSourceObj
        headerDataSource: if currentView is Constants.VIEWS.GROWTH then SummaryGrowthDataSource else SlickSummaryDataSource
        columnModel: ComparingPopulationsColumnModel
      }
      sortColumn = if currentView is Constants.VIEWS.GROWTH then 1 else 0
      @gridObj.registerPlugin @groupedHeaders
      @gridObj.registerPlugin @parccStatesToolTip
      @gridObj.loadView App.getState(), sortColumn

    destroyGrid: () ->
      if @gridObj
        @gridObj.destroy()
        @gridObj = null

    onSummaryChange: () =>
      @gridObj?.updateHeader()

    onGradeCourseChange: () =>
      if @loading
        return
      @gridObj?.updateHeader()
      @legend.setState {
        resultType: App.getState().result
        gradeCourse: App.getState().gradeCourse
      }
      @fetchData()

    onSubjectChange: () =>
      @loading = true
      state = App.getState()
      App.setState {
        'gradeCourse': gradeCourseStorage.load()?[state.subject] or null
        'result': 'overall'
      }
      @legend.setState {
        resultType: App.getState().result
        gradeCourse: App.getState().gradeCourse
      }
      @loading = false
      @fetchData()

    onResultChange: () =>
      if @loading
        return
      currentResultType = App.getState().result
      @legend.setState {resultType: currentResultType}
      @fetchData()

    onYearChange: () =>
      @fetchData()

    onViewChange: () =>
      App.setState {result: 'overall'}, true
      @fetchData()

    onFilterChange: () =>
      @fetchData()

    updateSubtitle: () ->
      length = @gridObj.getLength()
      @reportBar.setState {count: length}

    isParccSummaryAvailable: () ->
      @gridObj.headerDataSource.getComparisonData('parcc')?

    createGrowthChart: (compareSource) ->
      template = $('#growth-chart-template').text()
      $('[data-growth-wrapper]').html(template)
      @createBubbleChart(compareSource)
      edwareUtil.initPopovers()

    destroyGrowthChart: () ->
      # TODO: cleanup growthChart add destroy method
      $('[data-growth-wrapper]').empty()
      @bubbleChart = null

    createBubbleChart: (source) ->
      isParccSummaryAvailable = @isParccSummaryAvailable()
      compareSource = ensureCompareSource(source, isParccSummaryAvailable)
      @bubbleChart ?= growthChart.create {
        cutPoints: @data.legend.cut_points
        summary: @gridObj.headerDataSource.getComparisonData(compareSource)
        labels: @config.labels
        disable_parcc: not isParccSummaryAvailable
      }
      # Right now, we recreate the summary section
      @growthSummary = edwareGrowthSummary.create @gridObj.headerDataSource.getGrowthData(), @config.labels
      # reset only when view changed - right now that's subject toggles
      @bubbleChart.clearHiddenBubbles()
      # draw bubbles in default view
      @onSourceChange App.getState()[EVENTS.COMPARE_SOURCE_CHANGE]
      App.subscribe(EVENTS.COMPARE_SOURCE_CHANGE, @onSourceChange)
      App.subscribe(EVENTS.GRID_CHECKBOX_CHANGE, @updateBubble)
      App.subscribe(EVENTS.HEADER_CHECKBOX_CHANGE, @showOrHideBubbles)
      App.subscribe(EVENTS.GRIDROW_CHANGE, @onActivateRow)

    onSourceChange: (source) =>
      compareSource = ensureCompareSource(source, @isParccSummaryAvailable())
      summary = @gridObj.headerDataSource.getComparisonData(compareSource)
      if summary
        @bubbleChart.setBubbleDataSource @gridObj.dataSource.getGrowthData(compareSource), summary

    onActivateRow: (obj) =>
      if obj.display
        @bubbleChart.activateBubble obj.guid
      else
        @bubbleChart.deactivateBubble obj.guid

    updateBubble: (obj) =>
      @bubbleChart.updateOneBubble obj.guid, obj.display

    showOrHideBubbles: (mode) =>
      @bubbleChart.updateAllBubbles mode

    fetchData: () =>
      currentView = App.getState().view
      currentResultType = App.getState().result
      params = App.getState()
      loadingData = edwareDataProxy.getDatafromSource "/data/comparing_populations", {
        method: "POST"
        params: params
      }
      loadingData.done (data) =>
        @data = data
        ContextSecurity.instance(@data.user_info, @data.context)
        gradeCourseStorage = new EdwareClientStorage(data.user_info._User__info.guid)
        @loadPage()

  class ComparingPopulationsDataSource extends SlickDataSource

    constructor: (data) ->
      viewRows = {}
      for asmtType, assessments of data
        viewRows[asmtType] = {} if not viewRows[asmtType]
        for subjectName, subject of Constants.SUBJECTS
          viewRows[asmtType][subject] = assessments[subject]
      super viewRows

    getItemMetadata: (row) ->
      metadata = {}
      item = @getItem(row)
      item.canDrillDown = true
      if item?.CDS_OPT_OUT
        metadata = {
          columns: {
            1: {colspan: '5', display_suppression_msg: true}
            2: {colspan: '0'}
            3: {colspan: '0'}
            4: {colspan: '0'}
            5: {colspan: '0'}
          }
        }
      else if item?.is_less_than_min_cell_size
        metadata = {
          columns: {
            2: {colspan: '4'}
            3: {colspan: '0'}
            4: {colspan: '0'}
            5: {colspan: '0'}
          }
        }
      if not ContextSecurity.get().canDrillDown(item)
        item.canDrillDown = false
        metadata.cssClasses = 'noDrillDown'
      metadata

  class ComparingPopulationsGrowthDataSource extends ComparingPopulationsDataSource

    constructor: (data) ->
      super data

    getGrowthData: (source) ->
      dataSource = source
      data = []
      for school in @getView()
        if not school.performanceLevels
          continue

        data.push {
          has_filters: school['has_filters']
          students: school['students']
          guid: school['guid']
          name: school['name']
          overall: school['score']
          growthCompared: school['growth'][dataSource]
          growth: {
            parcc: school['growth']['parcc']
            state: school['growth']['state']
          }
        }
      # sort by number of students in descending order s.t. smaller
      # bubbles are on top of bigger ones
      data.sort (a, b) ->
        b.students - a.students
      data

    init: (grid, slickgrid, headergrid) ->
      onMouseEvent = (event, display) ->
        cell = slickgrid.getCellFromEvent(event)
        row = slickgrid.getDataItem cell.row
        App.setValue(EVENTS.GRIDROW_CHANGE, {'guid': row.guid, 'display': display})

      slickgrid.onClick.subscribe (event, args) =>
        return if event.target.type isnt 'checkbox'
        $(event.target).attr('checked', event.target.checked)
        row = @getItem args.row
        row.isHidden = not event.target.checked
        # Check if we need to update the header show/hide
        updateHeader = $(".gridCheckbox:not(:checked)").length is 0 or $(".gridCheckbox:checked").length is 0
        $('.headerCheckbox').attr('checked', event.target.checked) if updateHeader
        App.setValue(EVENTS.GRID_CHECKBOX_CHANGE, {'guid': row.guid, 'display': event.target.checked})

      slickgrid.onMouseEnter.subscribe (event) ->
        onMouseEvent(event, true)

      slickgrid.onMouseLeave.subscribe (event) ->
        onMouseEvent(event, false)

      headergrid.onHeaderClick.subscribe (event, args) =>
        if args.column.headerCheckbox
          checked = event.target.checked
          $('.gridCheckbox').attr('checked', checked)
          for d in this.getView()
            d.isHidden = not checked
          App.setValue(EVENTS.HEADER_CHECKBOX_CHANGE, checked)

  class SummaryGrowthDataSource extends SlickSummaryDataSource
    constructor: (data) ->
      super data

    update: () -> # we do not have compare rows in GrowthView
      @filteredView = []

    getGrowthData: (source) ->
      state = App.getState()
      currentSubject = state.subject
      dataForSubject = @data[state.asmtType]
      last = dataForSubject[currentSubject].length - 1
      dataForSubject[currentSubject][last]

    getComparisonData: (source) ->
      currentSubject = App.getState().subject
      asmtType = App.getState().asmtType
      dataForCurrentSubject = @data[asmtType][currentSubject]
      for dataRow in dataForCurrentSubject
        if dataRow.type is source
          return dataRow

  class CpopConsortiumRowWrapper extends edwareGridFormatters.HiddenRowDataWrapper
    shouldHide: (rowObject) ->
      return 'CDS_OPT_OUT' of rowObject and rowObject['CDS_OPT_OUT']
    getHiddenMessage: ->
      Constants.HIDDEN_ROW_MSG.CDS_OPT_OUT

  class ComparingPopulationsColumnModel extends SlickColumnModel

    constructor: (config, reportConfig) ->
      @configBuilder = new ComparingPopulationsConfigBuilder(reportConfig.breadcrumb.items)
      @hideSuppressionAlert = App.getState().view is Constants.VIEWS.PERFORMANCE
      super config, reportConfig

    createStaticColumns: (view) ->
      []

    createColumn: (idx, column, view) ->
      columnDef = $.extend({}, column)
      columnDef.rawValue = @getFormatter(columnDef.rawValue) if columnDef.rawValue
      columnDef.formatter = @getFormatter(columnDef.formatter) if columnDef.formatter
      if columnDef.field is 'name'
        # Dynamically inject configuration for name column for building links
        if column.formatter is 'showLink'
          data = @configBuilder.getNextHierarchyInfo()
        columnDef = $.extend(columnDef, data or {})
        columnDef.hideSuppressionAlert = @hideSuppressionAlert
        columnDef.summary = {
          formatter: @getFormatter 'showSummary'
          field: 'name'
        }
      else
        if columnDef.id is 'students'
          columnDef.formatter = @getFormatter('showNothing') if not $.isEmptyObject(FilterUtil.getActiveFilters())
          columnDef.formatter = new CpopConsortiumRowWrapper(columnDef.formatter) if columnDef.formatter
        columnDef.valueGetter = edwareUtil.getHiddenRowSortingValue
      columnDef

  edwareDataProxy.getDataForReport(reportName).done (config) ->
    new ComparingPopulationsReport(config)
