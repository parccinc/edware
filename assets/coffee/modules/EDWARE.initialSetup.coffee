define [
  'jquery'
  'react'
  'edwareConstants'
  'EdwareApplication'
  'edwareUtil'
  'edwareClientStorage'
  'edwareDataProxy'
  'edwareHelpMenu'
  'edwareHeader'
  'edwareSubjectSelector'
  'edwareGradeCourseSelector'
], ($,
  React,
  Constants,
  App,
  edwareUtil,
  EdwareClientStorage
  edwareDataProxy
  edwareHelpMenu,
  edwareHeader,
  edwareSubjectSelector
  edwareGradeCourseSelector
) ->

  EVENTS = Constants.EVENTS
  gradeCourseStorage = new EdwareClientStorage(null, true)
  HeaderHelpElement = edwareHeader.HeaderHelpElement
  HeaderUserSettingElement = edwareHeader.HeaderUserSettingElement
  HeaderHelpMenuContainer = edwareHeader.HeaderHelpMenuContainer
  GradesCoursesDropdown = edwareGradeCourseSelector.GradesCoursesDropdown
  SubjectsDropdown = edwareSubjectSelector.SubjectsDropdown
  App.initialize edwareUtil.getUrlParams()


  class InitialSetup

    constructor: (@config) ->
      @initialize()

    initialize: () ->
      if not gradeCourseStorage.getKey()
        gradeCourseStorage.setKey @config.user_info._User__info.guid
      App.subscribe(EVENTS.SUBJECT_CHANGE, @onSubjectChange)
      App.subscribe(EVENTS.GRADE_COURSE_CHANGE, @onGradeCourseChange)
      @render()

    createGradeCourseSelector: () ->
      config = @config
      if App.getState().subject
        <div id="gradeCourseSelector" className="pull-left infobar-selector">
          <GradesCoursesDropdown data={config.gradesCoursesList} disabled="" />
        </div>
      else
        <div id="gradeCourseSelector" className="pull-left infobar-selector">
          <GradesCoursesDropdown data={config.gradesCoursesList} disabled="disabled" />
        </div>
    render: () ->
      labels = @config.labels
      config = @config
      fullName = config.user_info._User__info.name.fullName
      React.render(
        <div role="presentation" className="container">
          <a href="../html/index.html" id="logo" title="Home">
            <img className="logo" src="/assets/images/logo172x42.png" />
          </a>
          <ul className="nav navbar-nav pull-right" role="navigation" >
            <HeaderHelpElement labels={labels} help_text={labels.help} labels={labels}/>
            <HeaderUserSettingElement userName={fullName} logoutLabel={labels.logout} />
          </ul>
          <HeaderHelpMenuContainer />
        </div>,
        $('#header')[0])
      React.render(
        <div>
          <div className="selectors clearfix">
            <div id="subjectSelector" className="pull-left infobar-selector">
              <SubjectsDropdown data={config.subjectsList} />
            </div>
            {@createGradeCourseSelector()}
          </div>
          <div className="defaults-not-set-message">
            <span>Select <b>'subject'</b> and <b>'grade'</b> to view results.</span>
          </div>
        </div>,
        $('#selector-container')[0]
      )

    onGradeCourseChange: () =>
      state = App.getState()
      if state.subject and state.gradeCourse
        gradeForSubject = {}
        gradeForSubject[state.subject] = state.gradeCourse
        gradeCourseStorage.update(gradeForSubject)
        storage = gradeCourseStorage.load()
        newState = $.extend {}, storage.previousState, state
        window.location.href = edwareUtil.generateUrl(storage.previousUrl, newState)

    onSubjectChange: () =>
      state = App.getState()
      @render()
      gradeCourseStorage.update({subject: state.subject})
      if state.subject and state.gradeCourse
        storage = gradeCourseStorage.load()
        newState = $.extend {}, storage.previousState, state
        window.location.href = edwareUtil.generateUrl(storage.previousUrl, newState)


  edwareDataProxy.getDataForReport().done (config) ->
    new InitialSetup(config)
