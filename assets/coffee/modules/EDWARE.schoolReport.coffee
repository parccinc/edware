define [
  'jquery'
  'edwareConstants'
  'edwareDataProxy'
  'edwareReportInfoBar'
  'edwareGrid'
  'edwareLegend'
  'edwareFootnote'
  'EdwareApplication'
  'edwareUtil'
  'edwareGridFormatters'
  'SlickGridFilter'
  'SlickExtraInfoPopover'
  'clusterTooltipProvider'
  'edwareFilterBar'
  'SlickColumnGroupHeaders'
  'SlickGridPagination'
  'listReport'
  'csvExport'
  'edwareContextSecurity'
  'selectorUtil'
  'AssessmentReportDataAdapter'
  'DiagnosticColumnModel'
  'edwareClientStorage'
], ($,
  Constants,
  edwareDataProxy,
  reportInfoBar,
  edwareGrid,
  legend,
  footnote,
  App,
  edwareUtil,
  edwareGridFormatters,
  SlickGridFilter,
  GridDecoratorPlugin,
  clusterTooltipProvider
  edwareFilterBar,
  SlickColumnGroupHeaders,
  SlickGridPagination,
  ListReport,
  exporter,
  ContextSecurity,
  SelectorUtil
  AssessmentReportDataAdapter
  DiagnosticColumnModel
  EdwareClientStorage
) ->

  SlickDataSource = edwareGrid.SlickDataSource
  SlickGroupDataSource = edwareGrid.SlickDataGroupSource
  SlickColumnModel = edwareGrid.SlickColumnModel
  FilterUtil = edwareFilterBar.FilterUtil
  AssessmentReportDataAdapter = AssessmentReportDataAdapter.AssessmentReportDataAdapter
  ClusterToolTipProvider = clusterTooltipProvider.ClusterToolTipProvider
  reportName = Constants.REPORT_JSON_NAME.SCHRPT
  EVENTS = Constants.EVENTS
  gradeCourseStorage = new EdwareClientStorage()

  App.initialize({
    asmtType: Constants.ASMT_TYPE.SUMMATIVE
    result: Constants.RESULT_TYPES.OVERALL
  })

  class SchoolReport

    SUBTITLE = {}
    SUBTITLE[Constants.SUBJECTS.MATH] = "grade/course"
    SUBTITLE[Constants.SUBJECTS.ELA] = "grade"

    constructor: (@config) ->
      @loading = false
      @initialize()

    initialize: () ->
      App.subscribe(EVENTS.SUBJECT_CHANGE, @onSubjectSelect)
      App.subscribe(EVENTS.SUMMARY_CHANGE, @onSummaryChange)
      App.subscribe(EVENTS.YEAR_CHANGE, @onYearChange)
      App.subscribe(EVENTS.FILTER_CHANGE, @fetchData)
      App.subscribe(EVENTS.DOWNLOAD_CSV, @onDownloadCSV)
      App.subscribe(EVENTS.DATE_CHANGE, @onDateChange)
      App.subscribe(@onStateChange)
      @groupedHeaders = new SlickColumnGroupHeaders()
      @gridPagination = new SlickGridPagination.GridPagination({showColumns: 9})
      @tooltipPopover = new GridDecoratorPlugin.ExtraInfoPopover(GridDecoratorPlugin.ColumnToolTipProvider, {
        decorateColumns: true
      })

      @clusterTooltipPopover = new GridDecoratorPlugin.ExtraInfoPopover(new ClusterToolTipProvider(@config.clusters), {
        decorateDataCells: true
      })

      @skillToolTipProvider = new GridDecoratorPlugin.ExtraInfoPopover(GridDecoratorPlugin.SkillToolTipProvider, {
        decorateDataCells: true
      })

      @gridFilter = new SlickGridFilter.GridFilter()
      @fetchData()

    onDateChange: () =>
      @render()

    getFilteredData: (state) ->
      data = $.extend true, {}, @data
      if state.asmt_date
        filter = $.extend true, {field: 'filterDate'}, state.asmt_date
        @gridFilter.filterRows(filter, data.assessments, state)
      data

    onDownloadCSV: () =>
      state = App.getState()
      asmtType = state.asmtType
      subject = state.subject
      summativeReport = asmtType is Constants.ASMT_TYPE.SUMMATIVE
      diagnosticReport = asmtType is Constants.ASMT_TYPE.DIAGNOSTIC
      # we set the pii flag to get because the result type depends on it
      state.piiAll = false
      report = ListReport.buildForReport this, state
      # for summative we set count to grades/courses (groups)
      if summativeReport
        report.setReportCount @gridObj.dataSource.groups.length
      csvExporter = new exporter.builder report, @config.labels
      downloader = (new exporter.downloader()).create()
      timestamp = csvExporter.buildFileNameTimestamp()

      # Logic for setting filename is based on report type and subject
      if summativeReport
        filename = "school_report_#{timestamp}.csv"
      else if diagnosticReport and subject is Constants.SUBJECTS.ELA
        filename = "diagnostic_ELA_#{timestamp}.csv"
      else if diagnosticReport and subject is Constants.SUBJECTS.MATH
        filename = "diagnostic_Math_#{timestamp}.csv"
      downloader(csvExporter.build(), filename, 'application/octet-stream')

    getReportTitle: () ->
      items = @data.context.items
      [..., finalBreadcrumbEntry] = items
      [finalBreadcrumbEntry.name, "Loading..."]

    getReportType: () ->
      'School Report'

    render: () ->
      state = App.getState()
      @gridObj?.destroy()
      @gridObj = null
      data = @getFilteredData(state)
      assessmentCount = edwareUtil.getAssessmentCount data.assessments, state
      if assessmentCount > 0
        @createGrid(data)

      [title, subtitle] = @getReportTitle()
      options = @updateReportBarOptions({
        data: data
        config: @config
        title: title
        subtitle: subtitle
        compare: EVENTS.SUMMARY_CHANGE
        resultSelector: true
        yearSelector: false
        viewSelector: true
        filters: true
        count: assessmentCount
      }, state)

      @reportBar = reportInfoBar.create options
      @footnote = footnote.create @getFootnoteConfig(state, assessmentCount)
      if assessmentCount > 0
        App.setValue(EVENTS.SUMMARY_CHANGE, {})
        @createLegend()
        edwareUtil.initPopovers()

    createLegend: () ->
      state = App.getState()
      legends = []
      # combine legend and diagnostic legends config. the legend widget filters out the ones we dont need
      legendConfs = SelectorUtil.buildOptionsForReport(@config.legends, state)
      (legends = legends.concat(@config[legendConf.name]) for legendConf in legendConfs)
      @legend = legend.create {
        labels: @config.labels
        data: legends
        reportState: state
        cutpoints: @config.cutpoints
      }

    getFootnoteConfig: (state, assessmentCount) ->
      {
        labels: @config.labels
        state: state
        show: assessmentCount > 0
      }

    isDateRangeFilterOn: () ->
      state = App.getState()
      edwareUtil.getReportType(state) is Constants.REPORT_TYPE_INT.SCHOOL and state.asmtType is Constants.ASMT_TYPE.DIAGNOSTIC

    createGrid: (data) ->
      assessments = data.assessments
      state = App.getState()
      currentResultType = state.result

      dataSource = if state.asmtType is Constants.ASMT_TYPE.SUMMATIVE then SchoolReportDataAdapter else AssessmentReportDataAdapter

      performanceType = Constants.PERFORMANCE_RESULT_COLUMN_MAP[currentResultType]
      @gridObj = edwareGrid.create {
        options: {
          columns: @config.grid[performanceType]
          assessments: assessments
          reportConfig: {
            cutpoints: @config.cutpoints
            baseColumns: @config.diagnosticBaseColumns
          }
          gridOptions: {
            pagination: {
              enabled: state.view is Constants.VIEWS.READER_MOTIV
            }
          }
        }
        dataSource: dataSource
        columnModel: SchoolReportColumnModel
      }

      @gridObj.registerPlugin @tooltipPopover
      @gridObj.registerPlugin @groupedHeaders
      # @gridObj.registerPlugin @expressivenessTooltipPopover
      @gridObj.registerPlugin @clusterTooltipPopover
      @gridObj.registerPlugin @skillToolTipProvider

      if @isDateRangeFilterOn()
        @gridObj.registerPlugin @gridFilter
      if state.view is Constants.VIEWS.READER_MOTIV and state.asmtType is Constants.ASMT_TYPE.DIAGNOSTIC
        @gridObj.registerPlugin @gridPagination
      @gridObj.loadView App.getState()

    ###
    # we capture the result change in this global way,
    # to make sure that all state changes caused by resultSelector
    # are applied before we continue
    ###
    onStateChange: (newValues, oldValues, changes, evnt) =>
      if @loading
        return
      if evnt.after
        if (
          App.hasChanged(EVENTS.ASSESSMENT_TYPE_CHANGE, newValues, oldValues) or
          App.hasChanged(EVENTS.RESULT_CHANGE, newValues, oldValues) or
          App.hasChanged(EVENTS.SUBJECT_CHANGE, newValues, oldValues)
        )
          state = App.getState()
          defaultViews = {subject2: 'overview', subject1: 'locator'}
          if state.asmtType is Constants.ASMT_TYPE.DIAGNOSTIC and state.view isnt defaultViews[state.subject]
            App.setState({view: defaultViews[state.subject]})
          else
            @fetchData()
            @createLegend()
        else if App.hasChanged(EVENTS.VIEW_CHANGE, newValues, oldValues)
          @fetchData()
          @createLegend()

    onSubjectSelect: (subject) =>
      ###
      # laoding = true
      # tells listeners not to make changes as we
      # will already reload the whole page after everything settles
      ###
      @loading = true
      App.setState {'result': 'overall'}
      @loading = false
      # we are not calling fetch data here as its already happening in the global callback

    onYearChange: () =>
      if @loading
        return
      @fetchData()

    onSummaryChange: (newSummaries, oldVal, keys) =>
      state = App.getState()
      if state.asmtType isnt Constants.ASMT_TYPE.DIAGNOSTIC
        enabledDataPoints = ["school"] # always display School
        datapoints = ['district', 'state', 'parcc']
        enabledDataPoints.push(compareName) for compareName in datapoints when newSummaries[compareName] is true
        if @gridObj
          @gridObj.dataSource.setDataPoints(enabledDataPoints)
          @gridObj.dataSource.update()
          @gridObj.notifyUpdated()
          edwareUtil.initPopovers()

    updateReportBarOptions: (barOptions, state) ->
      barOptions.hasCompareBarDisplayed = state.asmtType isnt Constants.ASMT_TYPE.DIAGNOSTIC
      @updateSubtitle(barOptions, state)

    updateSubtitle: (barOptions, state) ->
      if state.asmtType isnt Constants.ASMT_TYPE.DIAGNOSTIC
        if @gridObj and @gridObj.dataSource.groups
          barOptions.count = @gridObj.dataSource.groups.length
        else
          barOptions.count = 0
        barOptions.subtitle = SUBTITLE[App.getState().subject]
      else
        barOptions.subtitle = 'Assessment'
      barOptions.subtitle = edwareUtil.pluralize(barOptions.subtitle, barOptions.count, '/')
      barOptions

    fetchData: () =>
      state = App.getState()
      endpoint = "/data/#{state.asmtType.toLowerCase()}_school_report"
      loadingData = edwareDataProxy.getDatafromSource endpoint, {
        method: "POST"
        params: state
      }
      loadingData.done (data) =>
        @data = data
        gradeCourseStorage = new EdwareClientStorage(data.user_info._User__info.guid)
        if state.asmtType is Constants.ASMT_TYPE.DIAGNOSTIC
          availableRange = @gridFilter.prepareData(data.assessments, state)
          App.setValue({availableDateRange: availableRange})
        ContextSecurity.instance @data.user_info, @data.context
        @render()


  class SchoolReportDataAdapter extends SlickGroupDataSource

    constructor: (groups) ->
      config = {
        groupHeaderMetaProvider: @headerMetaProvider
        groupItemMetaProvider: (row) ->
          rowMeta = {}
          if row._dataPoint isnt "school"
            compareCssClass = "summary-compare-row"
            rowMeta.cssClasses = compareCssClass # adds this class on rendering
          if row.is_less_than_min_cell_size
            rowMeta.columns = {
              3: {colspan: '5'}
              4: {colspan: '0'}
              5: {colspan: '0'}
              6: {colspan: '0'}
              7: {colspan: '0'}
            }
          rowMeta
        childrenKey: 'grades'
      }
      super  config, groups
      @setDataPoints(['school'])

    headerMetaProvider: (row, index) ->
      contextSecurity = ContextSecurity.get()
      canDrillDown = contextSecurity.canAccessPII(row) and contextSecurity.hasStaffPermission(row)
      row.canDrillDown = canDrillDown
      {
        columns: {
          0: {
            colspan: "*"
            field: "name"
            # easy way to retrieve group
            # since this is already row level metadata
            groupIndex: index
            formatter: edwareGridFormatters.createFormatter 'showGroupHeaderRow'
          }
        }
        cssClasses: if not canDrillDown then 'noDrillDown' else ''
      }

  class SchoolReportColumnModel extends DiagnosticColumnModel

    gradeValueGetter: (value, lookup, sortCol, isAsc) ->
      return if value.indexOf("Grade") > -1 then parseInt(value.replace "Grade ", 10) else value

    createStaticColumns: (view) ->
      []

    createColumn: (idx, column, view) ->
      columnDef = super(idx, column, view)
      if columnDef.id is 'students' and not $.isEmptyObject(FilterUtil.getActiveFilters())
        columnDef.formatter = @getFormatter 'showNothing'
      else if columnDef.id is 'grade'
        columnDef.sortByData = 'guid'
        columnDef.valueGetter = @gradeValueGetter
      else if columnDef.id is 'student_name'
        # this may break because we create the columns for all views on pageLoad
        # it works now because we completely recreate all grid components on each view change
        if App.getState().view in ['grade_level', 'progress', 'decod']
          columnDef.width = 174 # spec said 170 so this can be tweaked if needed
      columnDef

  edwareDataProxy.getDataForReport(reportName).done (config) ->
    new SchoolReport(config)
