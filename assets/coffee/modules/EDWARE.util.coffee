define [
  'jquery'
  'edwareConstants'
], ($, Constants) ->
  #
  # * EDWARE util
  # * Handles constants, reusable or common methods required by other EDWARE javascript files
  #

  #global  window

  # These query params are needed for every report. Use to rebuild URL for links
  PERSISTENCE_PARAMS = ['asmtType', 'gradeCourse', 'year', 'subject'].concat \
    (Constants.FILTER_TYPES[i] for i in Object.keys(Constants.FILTER_TYPES))

  getHiddenRowSortingValue = (value, lookup, sortCol, isAsc) ->
    # always show blocked entry last
    if value.CDS_OPT_OUT or value.INVALID_SCORE
      if isAsc then Number.POSITIVE_INFINITY else Number.NEGATIVE_INFINITY
    else if value[lookup[0]][lookup[1]] is ""
      if isAsc then Constants.BIG_NUMBER.POSITIVE else Constants.BIG_NUMBER.NEGATIVE
    else
      getDeepValue(value, lookup)

  getAssessmentCount = (assessments, state) ->
    count = 0
    if assessments and state.asmtType of assessments
      count = assessments[state.asmtType][state.subject].length
    return count

  getPersistedParams = (knownValues, additionalRequiredParams, ignoreParams) ->
    # knownValues is an object related to current state
    knownValues ?= {}
    params = {}
    additionalRequiredParams ?= []
    ignoreParams ?= []
    for i in PERSISTENCE_PARAMS.concat(additionalRequiredParams)
      params[i] = knownValues[i] if knownValues[i] and ignoreParams.indexOf(i) is -1
    params

  escapeFilename = (string) ->
    (string.replace /\s/g, "_").toLowerCase()

  capFirst = (string) ->
    string.charAt(0).toUpperCase() + string.slice(1)

  generateUrl = (url, params, knownValues, additionalRequiredParams, ignoreParams) ->
    persistedParams = getPersistedParams(knownValues, additionalRequiredParams, ignoreParams)
    queryString = @setUrlParams($.extend {}, persistedParams, params)
    separator = if url.indexOf('?') < 0 then "?" else "&"
    "#{url}#{separator}#{queryString}"

  getUrlParams = ->
    params = {}
    location = History.getState().url
    decoded = decodeURIComponent(location).replace(/\+/g, ' ')
    decoded.replace /[?&]+([^=&]+)=([^&]*)/g, (str, key, value) ->
      params[key] = value
    decodeParams(params)

  setUrlParams = (params) ->
    urlParams = encodeParams(params)
    $.param(urlParams)

  decodeParams = (params) ->
    for key, param of params
      if param.indexOf(',') isnt -1
        params[key] = param.split(',')
    params

  encodeParams = (params) ->
    for key, param of params
      if $.isArray(param)
        params[key] = param.join(',')
    params

  getBaseURL = ->
    window.location.protocol + "//" + window.location.host

  # Given an user_info object, return the one role of that user from the list
  getRole = (userInfo) ->
    # roles is a list, for beta, we can assume we only have 1 role
    userInfo._User__info.roles[0].toUpperCase()

  # Given an user_info object, return the first and last name of the user
  getUserName = (userInfo) ->
    userInfo._User__info.name.firstName + ' ' + userInfo._User__info.name.lastName

  # Given an user_info object, return the uid
  getUid = (userInfo) ->
    userInfo._User__info.uid

  # Given an user_info object, return the uid
  getGuid = (userInfo) ->
    userInfo._User__info.guid

  # Given an user_info object, return the state_code
  getUserStateCode = (userInfo) ->
    userInfo._User__info.stateCode

  getDisplayBreadcrumbsHome = (userInfo) ->
    userInfo._User__info.displayHome

  initPopovers = () ->
    $('.alert-icon-suppression').each () ->
      if not $(this).data().popover then $(this).popover()

  format_full_name_reverse = (first_name, middle_name, last_name) ->
    if (middle_name and middle_name.length > 0)
      middle_init = middle_name[0] + '.'
    else
      middle_init = ''
    return "#{last_name}, #{first_name} #{middle_init}".replace '  ', ' '

  # truncate the content and add ellipsis "..." if content is more than character limits
  truncateContent = (content, characterLimits) ->
    if content.length > characterLimits
      content = content.substr(0, characterLimits)

      # ignore characters after the last word
      content = content.substr(0, content.lastIndexOf(' ') + 1) + "..."

    content

  # Set the popup position to left, right, top, bottom
  popupPlacement = (element, popupWidth, popupHeight) ->
    isWithinBounds = (elementPosition) ->
      boundTop < elementPosition.top and
        boundLeft < elementPosition.left and
        boundRight > (elementPosition.left + actualWidth) and
        boundBottom > (elementPosition.top + actualHeight)

    $element = $(element)
    pos = $.extend({}, $element.offset(), {
      width: element.offsetWidth
      height: element.offsetHeight
    })
    actualWidth = popupWidth
    actualHeight = popupHeight
    boundTop = $(document).scrollTop()
    boundLeft = $(document).scrollLeft()
    boundRight = boundLeft + $(window).width()
    boundBottom = boundTop + $(window).height()
    elementAbove = {
      top: pos.top - actualHeight
      left: pos.left + pos.width / 2 - actualWidth / 2
    }

    elementBelow = {
      top: pos.top + pos.height
      left: pos.left + pos.width / 2 - actualWidth / 2
    }

    elementLeft = {
      top: pos.top + pos.height / 2 - actualHeight / 2
      left: pos.left - actualWidth
    }

    elementRight = {
      top: pos.top + pos.height / 2 - actualHeight / 2
      left: pos.left + pos.width
    }

    above = isWithinBounds(elementAbove)
    below = isWithinBounds(elementBelow)
    left = isWithinBounds(elementLeft)
    right = isWithinBounds(elementRight)
    if above
      "top"
    else
      if below
        "bottom"
      else
        if left
          "left"
        else
          if right
            "right"
          else
            "left"

  # Add comma as thousand separator to numbers
  # Return 0 if parameter is undefined
  formatNumber = (num) ->
    if num then num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") else 0

  formatDate = (date) ->
    if not date
      return ""
    return "#{date[0..3]}.#{date[4..5]}.#{date[6..]}"

  getCurrentAcademicYear = () ->
    today = new Date()
    currentYear = today.getFullYear()
    cutoff = new Date today.getFullYear(), 7, 1, 0, 0, 0
    if today >= cutoff
      currentYear++
    currentYear

  escapeCSV = (value) ->
    if typeof(value) is 'string'
      # return '"' + value + '"'
      # escape double quote
      return '"' + value.replace(/"/g, '""') + '"'
    else if typeof(value) is 'number'
      return '"' + value + '"'
    else if $.isArray(value)
      for item in value
        escapeCSV item
    else
      value

  getAbsolutePathForBrandingResource = (path) ->
    # If it starts with \, then it's some abs path
    if /^\/.+\..+/.test path
      return path
    else
      return '/assets/images/branding/' + path

  getTenantBrandingData = (metadata, isPrint = false) ->
    return getTenantBranding(metadata, isPrint, false)

  getTenantBrandingDataForPrint = (metadata, isGrayscale) ->
    return getTenantBranding(metadata, true, isGrayscale)

  getTenantBranding = (metadata, isPrint, isGrayscale) ->
    defaultLogo = if isPrint then '/assets/images/logo_black_172x42.png' else '/assets/images/logo172x42.png'
    brandingData = {'tenantLogo': defaultLogo, 'tenantLabel': '', 'higherEdLink': ''}
    if metadata and metadata.branding
      brandingData.tenantLogo = getAbsolutePathForBrandingResource metadata.branding.image if metadata.branding.image
      brandingData.tenantLabel = truncateContent metadata.branding.display, 50 if metadata.branding.display
      brandingData.higherEdLink = metadata.branding.higherEdLink
    return brandingData

  getDeepValue = (rowObject, path) ->
    return '' if not path
    path = ensureArray(path)
    value = rowObject
    $(path).each (ind, key) ->
      value = value and value[key]
    if value? then value else ''

  getReportType = (state) ->
    if 'studentGuid' of state
      Constants.REPORT_TYPE_INT.STUDENT
    else if 'schoolGuid' of state
      Constants.REPORT_TYPE_INT.SCHOOL
    else if 'districtGuid' of state
      Constants.REPORT_TYPE_INT.DISTRICT
    else if 'stateCode' of state
      Constants.REPORT_TYPE_INT.STATE
    else
      Constants.REPORT_TYPE_INT.CONSORTIUM

  getReportLevel = (state) ->
    REPORT_LEVELS = Constants.REPORT_LEVELS
    if 'studentGuid' of state
      REPORT_LEVELS.STUDENT
    else if 'schoolGuid' of state and 'gradeCourse' of state
      REPORT_LEVELS.GRADE
    else if 'schoolGuid' of state
      REPORT_LEVELS.SCHOOL
    else if 'districtGuid' of state
      REPORT_LEVELS.DISTRICT
    else if 'stateCode' of state
      REPORT_LEVELS.STATE
    else
      REPORT_LEVELS.CONSORTIUM

  isDate = (txtDate) ->
    reg = /^(0[1-9]|1[012])([\/-])(0[1-9]|[12][0-9]|3[01])\2(\d{4})$/
    reg.test(txtDate)

  ensureArray = (item) ->
    if $.isArray(item) then item else [item]
  ###
  # takes in an object an returns obj with the values matching keys
  # based on internals of react https://github.com/STRML/keyMirror
  ###
  keyMirror = (obj) ->
    ret = {}
    for key of obj
      if not obj.hasOwnProperty(key)
        continue
      ret[key] = key
    ret
  ###
  # takes a function and returns a function that can only be called once
  ###
  once = (action) ->
    _action = action # copy to local var for optimization
    slice = Array.prototype.slice
    () ->
      if _action
        func = _action
        _action = null
        return func.apply this, slice.call arguments

  pluralize = (text, count, separator = '/') ->
    if count is 1
      return text
    words = text.split separator
    words = (word + 's' for word in words)
    words.join(separator)


  {
    generateUrl: generateUrl
    capFirst: capFirst
    getUrlParams: getUrlParams
    setUrlParams: setUrlParams
    getBaseURL: getBaseURL
    getRole: getRole
    getUserName: getUserName
    getUid: getUid
    getGuid: getGuid
    getCurrentAcademicYear: getCurrentAcademicYear
    getUserStateCode: getUserStateCode
    getDisplayBreadcrumbsHome: getDisplayBreadcrumbsHome
    initPopovers: initPopovers
    truncateContent: truncateContent
    popupPlacement: popupPlacement
    format_full_name_reverse: format_full_name_reverse
    formatNumber: formatNumber
    escapeCSV: escapeCSV
    getTenantBrandingData: getTenantBrandingData
    getTenantBrandingDataForPrint: getTenantBrandingDataForPrint
    getReportType: getReportType
    formatDate: formatDate
    getDeepValue: getDeepValue
    getReportLevel: getReportLevel
    escapeFilename: escapeFilename
    once: once
    ensureArray: ensureArray
    keyMirror: keyMirror
    isDate: isDate
    getAssessmentCount: getAssessmentCount
    getHiddenRowSortingValue: getHiddenRowSortingValue
    pluralize: pluralize
  }
