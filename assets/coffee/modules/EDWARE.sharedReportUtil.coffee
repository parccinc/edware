define [
  'jquery'
  'EdwareApplication'
], ($, App) ->

  class EdwareReportDataStore
    # A simple class that has an object to keep track of data
    constructor: () ->
      @data = {}

    saveData: (value) ->
      @data[@getKey()] = value

    getKey: () ->
      App.getState().view

    hasData: () ->
      if @getKey() of @data then true else false

    getData: () ->
      @data[@getKey()]

  {reportDataStore: EdwareReportDataStore}
