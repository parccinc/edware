define [
  'jquery'
  'react'
  'mustache'
  'edwareConstants'
  'edwareDataProxy'
  'edwareReportInfoBar'
  'edwareUtil'
  'edwareClientStorage'
  'EdwareApplication'
  'edwareLegend'
  'edwareFootnote'
  'edwareSharedReportUtil'
  'edwareHeader'
  'edwarePrint'
  'edwareClientStorage'
  'text!perfSectionTemplate'
  'text!perfSectionBarTemplate'
  'text!printIsrFaqTemplate'
], ($,
    React,
    Mustache,
    Constants,
    edwareDataProxy,
    reportInfoBar,
    edwareUtil,
    state,
    App,
    legend,
    footnote,
    sharedReportUtil,
    edwareHeader,
    print,
    EdwareClientStorage
    perfSectionTemplate,
    perfBarTemplate,
    printTemplate
  ) ->
    reportName = Constants.REPORT_NAME.SUMMATIVE_ISR
    gradeCourseStorage = new EdwareClientStorage()
    App.initialize({
      asmtType: Constants.ASMT_TYPE.SUMMATIVE
    })

    class StudentSummativeReport

      constructor: (@config) ->
        if App.getState().asmtType is Constants.ASMT_TYPE.DIAGNOSTIC
          @redirectDiagnostic()
          return

        @initialize()
        # bind events
        App.subscribe(Constants.EVENTS.SUBJECT_CHANGE, @onSubjectChange)
        App.subscribe(Constants.EVENTS.YEAR_CHANGE, @onYearChange)
        App.subscribe(Constants.EVENTS.ASSESSMENT_TYPE_CHANGE, @asmtTypeChange)

      initialize: () ->
        @dataStore = new sharedReportUtil.reportDataStore()
        # no view selector on ISR
        @config.viewsList = []
        @fetchData()

      isPdf: () ->
        return !!App.getState().pdf

      render: () ->
        data = @dataStore.getData()
        state= App.getState()
        @config.subjectsList = @dataStore.getData().subjects
        assessmentCount = if data.assessments then 1 else 0
        additionalChildren = if data.assessments then (<print.PrintMenu></print.PrintMenu>) else ''
        infoBarOptions = {
          data: data
          config: @config
          reverseBreadcrumb: true
          isPdf: @isPdf()
          yearSelector: true
          resultSelector: true
          filterResultsList: true
          count: assessmentCount
          additionalChildren: additionalChildren
        }
        if not @isPdf()
          infoBarOptions = @generateReportTitleBarOptions(data, infoBarOptions)
        else
          $.extend infoBarOptions, @getTitle(data), true
        @reportBar = reportInfoBar.create infoBarOptions
        @legend = legend.create @getLegendConfig(state)
        # get student grade
        @grade = (data.context.items.filter (e) ->
          e.id if e.type is 'grade'
        )[0]
        if assessmentCount > 0
          @showPerformance()
          @footnote = footnote.create @getFootnoteConfig(state)
        else
          $('#main').empty()

      getLegendConfig: (state) ->
        {
          labels: @config.labels
          data: @config.legend
          reportState: state
        }

      getFootnoteConfig: (state) ->
        {
          labels: @config.labels
          state: state
          show: true
        }

      getTitle: (data) ->
        year = App.getState().year
        prevYear = year - 1
        subject = App.getState().subject
        grade = data.assessments[subject].grade
        titlePrefix = data.assessments[subject].titlePrefix
        options = {
          title: "#{titlePrefix} Assessment Report, #{prevYear} - #{year}",
          aboveTitle: Constants.ISR_PRINT_SUBJECTS[App.getState().subject]
        }
        options

      showPerformance: () ->
        claims_level = @config.claims_level
        claims_level_aria = @config.claims_level_aria
        assessments = @dataStore.getData().assessments
        storeData = @dataStore.getData()
        data = @constructDataForPerformanceSection(storeData)
        for subscore in data.subscores
          for claim in subscore.claims
            claim.ariaid = claim.name.replace(/\s+/g, '')
        state = App.getState()
        templateData = {
          labels: @config.labels
          data: data
          perfBar: @getPerfBar(storeData)
          show_claim_level: () ->
            # mapping claim level to description text
            (text, render) -> "<p aria-label='#{claims_level_aria[@level]}'>#{Mustache.render(claims_level[@level], data)}</p>"
          cols: 12 / data.subscores.length
          subject: state.subject
          isPdf : @isPdf(),
          aboveTitle: Constants.ISR_PRINT_SUBJECTS[state.subject]
        }

        if not @isPdf()
          templateData.data.additional_info.pop()

        $('#main').html Mustache.render perfSectionTemplate, templateData
        if @isPdf() # is print
          $('body').append Mustache.render printTemplate, @compilePrint(storeData)
          infoBarOptions = @generateReportTitleBarOptions(storeData)
          infoBarOptions.cssSelector = '.informational-paper .header-container'
          @PDFReportBar ?= reportInfoBar.create infoBarOptions

      asmtTypeChange: (newValue, oldValue, key, evnt) =>
        if newValue is Constants.ASMT_TYPE.DIAGNOSTIC
          @redirectDiagnostic()

      redirectDiagnostic: () ->
        document.location.href = edwareUtil.generateUrl('studentDiagnosticReport.html', App.getState())

      # constructs data for the performance section of ISR
      # uses a default template defined in isr/summativeReport.json to some prerendering before the data is actually passed
      # to the performance section template.
      constructDataForPerformanceSection: (reportData) ->
        assessments = reportData.assessments
        state = App.getState()
        subject = state.subject
        asmtType = state.asmtType
        person = if @isPdf() then @config.labels.child else @config.labels.student
        summary = reportData.summary[asmtType][subject]
        # For ISR, we need to reverse the order of summary data
        summary = summary.reverse()
        defaultTemplate = @config.template.default[subject]
        aOrAn = if assessments[subject].overall.name[0].toLowerCase() in ['a', 'e', 'i', 'o', 'u'] then 'an' else 'a'
        custom = @config.template[@grade.id]?[subject]
        template = $.extend(true, {}, defaultTemplate, custom)
        @config.ccReady = state.result is Constants.RESULT_TYPES.OVERALL and state.gradeCourse in ['Grade 11'].concat(Constants.CCR_COURSES)
        if state.result is Constants.RESULT_TYPES.OVERALL and state.gradeCourse in Constants.CCR_COURSES.concat(Constants.ON_TRACK_COURSES)
          readyFor = 'college and careers'
        else
          readyFor = 'the next grade level'

        data = $.extend true, {}, assessments[subject], {
          person: person,
          aOrAn: aOrAn,
          readyFor: readyFor,
          isPdf : @isPdf()
        }

        # reformat gradeCourse
        data.gradeCourse = data.textBodyGradeCourse
        compiled = JSON.parse(Mustache.render(JSON.stringify(template), data))
        assessment_data = $.extend true, {}, assessments[subject], compiled, {
          person: person
        }
        @addAggregationInfoToAssessmentData(assessment_data, summary)
        assessment_data

      # adds aggregation info to assessment data for use in rendering claims' averages
      # this is required as we can't do key look up in mustache so we need a simpler json structure
      addAggregationInfoToAssessmentData: (assessment_data, summary) ->
        for subscore in assessment_data.subscores
          subscore.averages = []
          for summary_element in summary
            if summary_element.disabled
              subscore.averages.push {
                type: summary_element.type,
                average_score: Constants.NOT_APPLICABLE_TEXT
                level_3_average: Constants.NOT_APPLICABLE_TEXT
              }
            else
              if summary_element.type is 'parcc'
                subscore.avg_meet_expectations = summary_element.level4_avg[subscore.claim_type] or '-'
                continue
              subscore.averages.push {
                type: summary_element.type,
                average_score: summary_element.average_claim_score[subscore.claim_type]
              }

      compilePrint: (storeData) ->
        data = @constructDataForPerformanceSection(storeData)
        subject = App.getState().subject
        template = $.extend(true, {}, @config.about) # use subject here
        template = $.extend template, @getTitle(storeData), true
        renderData = $.extend {}, @config.labels, true
        if subject is 'subject2'
          renderData.subject_wording = 'reading and writing'
          renderData.subject_name = 'English language arts / literacy'
        else
          renderData.subject_wording = 'mathematics'
          renderData.subject_name = 'mathematics'
        renderData.probable_range = data.overall.error
        compiled = JSON.parse(Mustache.render(JSON.stringify(template), renderData))
        compiled

      getPerfBar: (reportData) ->
        assessments = reportData.assessments
        summary = reportData.summary
        asmtType = App.getState().asmtType
        subject = App.getState().subject
        overall = assessments[subject].overall

        summary[asmtType][subject].map (item) ->
          if item.type isnt 'parcc'
            item
          else
            item.type = "#{item.type} states"

        performance = overall.performance
        totalWidth = performance[performance.length - 1].cutpoint_end - performance[0].cutpoint_start
        for level in performance
          level.width = Math.floor ((level.cutpoint_end - (level.cutpoint_start - 1)) / (totalWidth) ) * 100
        data = $.extend true, overall, {
          labels: @config.labels
          indicator: Math.floor ((overall.score - performance[0].cutpoint_start) / totalWidth) * 100
          ccReady: @config.ccReady
          startCutpoint: edwareUtil.once () ->
            (template, render) ->
              render template
          isPdf: @isPdf()
        }


        data.summary = summary[asmtType][subject]
        data.showApplicableValue = @showApplicableValue
        Mustache.render perfBarTemplate, data

      showApplicableValue: () ->
        (text, render) ->
          render(text) or Constants.NOT_APPLICABLE_TEXT

      switchView: () ->
        # Determines if we need to refetch from BE or not
        if @dataStore.hasData() then @render() else @fetchData()

      fetchData: () =>
        params = App.getState()
        loadingData = edwareDataProxy.getDatafromSource "/data/summative_student_report", {
          method: "POST"
          params: params
        }
        loadingData.done (data) =>
          gradeCourseStorage = new EdwareClientStorage(data.user_info._User__info.guid)
          @dataStore.saveData data
          @render()

      onSubjectChange: (newVal, oldVal, key) =>
        if newVal isnt oldVal
          state = App.getState()
          gradeCourse = null
          if Constants.SHARED_COURSES.indexOf(state.gradeCourse) > -1
            gradeCourse = state.gradeCourse
          App.setState {
            'gradeCourse': gradeCourseStorage.load()?[state.subject] or gradeCourse
          }
          @fetchData()

      onYearChange: () =>
        @fetchData()

      generateReportTitleBarOptions: (data, infoBarOptions) =>
        defaultOptions = {
          data: data
          config: @config
          reverseBreadcrumb: true
          isPdf: @isPdf()
          additionalChildren: (<print.PrintMenu></print.PrintMenu>)
        }
        infoBarOptions ?= defaultOptions
        # Display title along with the course name in the reportBar
        infoBarOptions

    edwareDataProxy.getDataForReport(Constants.REPORT_JSON_NAME.SUMMATIVE_ISR).done (config) ->
      new StudentSummativeReport(config)
