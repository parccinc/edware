define [
  'jquery'
  'edwareConstants'
  'edwareDataProxy'
  'edwareReportInfoBar'
  'edwareGrid'
  'StudentRosterColumnModel'
  'AssessmentReportDataAdapter'
  'EdwareApplication'
  'edwareLegend'
  'edwareFootnote'
  'edwareSharedReportUtil'
  'SlickExtraInfoPopover'
  'SlickColumnGroupHeaders'
  'SlickGridPagination'
  'listReport'
  'csvExport'
  'edwareContextSecurity'
  'edwareUtil'
  'edwareClientStorage'
], ($,
    Constants,
    edwareDataProxy,
    reportInfoBar,
    edwareGrid,
    StudentRosterColumnModel,
    AssessmentReportDataAdapter,
    App,
    legend,
    footnote,
    sharedReportUtil,
    GridDecoratorPlugin,
    SlickColumnGroupHeaders,
    SlickGridPagination,
    ListReport,
    exporter,
    ContextSecurity
    edwareUtil
    EdwareClientStorage
  ) ->

  SlickSummaryDataSource = edwareGrid.SlickSummaryDataSource
  StudentRosterColumnModel = StudentRosterColumnModel.StudentRosterColumnModel
  AssessmentReportDataAdapter = AssessmentReportDataAdapter.AssessmentReportDataAdapter
  gridPopoverPlugin = GridDecoratorPlugin.ExtraInfoPopover
  EvidenceStatementProvider = GridDecoratorPlugin.EvidenceStatementProvider
  ColumnToolTipProvider = GridDecoratorPlugin.ColumnToolTipProvider
  parccStatesToolTipProvider = GridDecoratorPlugin.ParccStatesToolTipProvider
  reportName = Constants.REPORT_JSON_NAME.LOS
  EVENTS = Constants.EVENTS
  gradeCourseStorage = new EdwareClientStorage()

  App.initialize({
    view: Constants.VIEWS.SCORES
    asmtType: Constants.ASMT_TYPE.SUMMATIVE
    result: Constants.RESULT_TYPES.OVERALL
  })


  class StudentRosterReport

    constructor: (@config) ->
      @initialize()

    initialize: () ->
      App.subscribe(EVENTS.SUBJECT_CHANGE, @onSubjectChange)
      App.subscribe(EVENTS.VIEW_CHANGE, @onViewChange)
      App.subscribe(EVENTS.SUMMARY_CHANGE, @onSummaryChange)
      App.subscribe(EVENTS.YEAR_CHANGE, @onYearChange)
      App.subscribe(EVENTS.FILTER_CHANGE, @fetchData)
      App.subscribe(EVENTS.RESULT_CHANGE, @fetchData)
      App.subscribe(EVENTS.DOWNLOAD_CSV, @onDownloadCSV)
      @groupedHeaders = new SlickColumnGroupHeaders()
      @evidenceStatementPlugin = new gridPopoverPlugin(EvidenceStatementProvider, {
        decorateColumns: true
      })
      @tooltipPopover = new gridPopoverPlugin(ColumnToolTipProvider, {
        decorateColumns: true
      })
      @parccStatesToolTip = new gridPopoverPlugin(parccStatesToolTipProvider, {
        decorateSummaryCells: true
      })
      @gridPagination = new SlickGridPagination.GridPagination({showColumns: 8})
      @fetchData()

    onDownloadCSV: () =>
      report = ListReport.buildForReport this, App.getState()
      csvExporter = new exporter.builder report, @config.labels
      downloader = (new exporter.downloader()).create()
      downloader(csvExporter.build(), csvExporter.buildFileName(), 'application/octet-stream')

    getReportType: () ->
      'Student Roster'

    getReportTitle: () ->
      items = @data.context.items
      [..., finalBreadcrumbEntry] = items
      subtitle = " #{@config.labels.student}"
      title = finalBreadcrumbEntry.name
      [title, subtitle]

    render: () ->
      @destroyGrid()
      state = App.getState()
      @config.subjectsList = @data.subjects
      [title, subtitle] = @getReportTitle()
      assessmentCount = edwareUtil.getAssessmentCount(@data.assessments, state)
      if assessmentCount > 0
        @createGrid()
      subtitle = edwareUtil.pluralize(subtitle, assessmentCount)
      options = {
        data: @data
        config: @config
        title: title
        subtitle: subtitle
        resultSelector: true
        viewSelector: true
        filterResultsList: true
        filters: true
        compare: EVENTS.SUMMARY_CHANGE
        count: assessmentCount
      }
      @reportBar = reportInfoBar.create options
      defaultSelected = {school: true}
      App.setValue(EVENTS.SUMMARY_CHANGE, defaultSelected) # school should be enabled by default
      @legend = legend.create @getLegendConfig(state)
      @footnote = footnote.create @getFootnoteConfig(state, assessmentCount)
      @updateSubtitle()

    getLegendConfig: (state) ->
      {
        labels: @config.labels
        data: @config.legend
        reportState: state
      }

    getFootnoteConfig: (state, assessmentCount) ->
      {
        labels: @config.labels
        state: state,
        show: assessmentCount > 0
      }

    createGrid: () ->
      currentView = App.getState().view
      data = @data
      @gridObj = edwareGrid.create {
        options: {
          columns: $.extend true, {}, @config.grid[currentView], data.columns
          assessments: data.assessments
          summary: data.summary
          reportConfig: {}
          gridOptions: {
            pagination: {
              enabled: currentView is Constants.VIEWS.ITEM_ANALYSIS
            }
          }
        }
        dataSource: AssessmentReportDataAdapter
        columnModel: StudentRosterColumnModel
        headerDataSource: SlickSummaryDataSource
      }
      @gridObj.registerPlugin @evidenceStatementPlugin
      @gridObj.registerPlugin @groupedHeaders
      @gridObj.registerPlugin @tooltipPopover
      @gridObj.registerPlugin @parccStatesToolTip
      if currentView is Constants.VIEWS.ITEM_ANALYSIS
        @gridObj.registerPlugin @gridPagination
      @gridObj.loadView App.getState()

    destroyGrid: () ->
      @gridObj?.destroy()
      @gridObj = null

    onSubjectChange: (newVal, oldVal, key) =>
      return if newVal is oldVal
      state = App.getState()
      App.setState {
        'gradeCourse': gradeCourseStorage.load()?[state.subject] or null
        'result': 'overall'
      }
      @updateSubtitle()
      @fetchData()

    onYearChange: () =>
      @fetchData()

    onSummaryChange: (newVal, oldVal, keys) =>
      @gridObj?.updateHeader()

    updateSubtitle: () ->
      count = if @gridObj then @gridObj.getLength() else 0
      @reportBar.setState {count: count}

    onViewChange: () =>
      @fetchData()

    fetchData: () =>
      loadingData = edwareDataProxy.getDatafromSource "/data/student_roster", {
        method: "POST"
        params: App.getState()
      }
      loadingData.done (data) =>
        @data = data
        ContextSecurity.instance(@data.user_info, @data.context)
        gradeCourseStorage = new EdwareClientStorage(data.user_info._User__info.guid)
        @render()

  edwareDataProxy.getDataForReport(reportName).done (config) ->
    new StudentRosterReport(config)
