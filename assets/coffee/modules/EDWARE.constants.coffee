define [], () ->
  {
    BIG_NUMBER: {
      POSITIVE: 9999999999
      NEGATIVE: 9999999999
    }

    REPORT_NAME: {
      CPOP: 'comparing_populations'
      LOS: 'list_of_students'
      ISR: 'individual_student_report'
      SUMMATIVE_ISR: 'individual_summative_report'
      DIAGNOSTIC_ISR: 'individual_diagnostic_report'
    }

    HIDDEN_ROW_MSG: {
      INVALID_SCORE: 'STUDENT DID NOT RECEIVE A VALID SCORE'
      CDS_OPT_OUT: 'N/A'
    }

    REPORT_TYPE_INT: {
      CONSORTIUM: 0
      STATE: 1
      DISTRICT: 2
      SCHOOL: 3
      STUDENT: 4
    }

    REPORT_LEVEL_NAME: ['consortium', 'state', 'district', 'school', 'grade', 'student']

    REPORT_LEVELS: {
      CONSORTIUM: 0,
      STATE: 1,
      DISTRICT: 2,
      SCHOOL: 3
      GRADE: 4
      STUDENT: 5
    }

    EXTRACT_SELECTORS: {
      SUMMATIVE_ASMT_RESULT: "summ_asmt_result"
      SUMMATIVE_RIF: "item_student_score"
      PSYCHOMETRIC: "p_data"
      CDS_DATA: "cds_data"
    }

    EXTRACT_JSON_RESPONSE: {
      DOWNLOAD_URL: 'download_url'
      FILES: 'files'
    }

    REPORT_TYPE: {
      STATE: 'state'
      DISTRICT: 'district'
      SCHOOL: 'school'
      GRADE: 'grade'
    }

    REPORT_JSON_NAME: {
      CPOP: 'comparingPopulations'
      LOS: 'studentList'
      ISR: 'indivStudentReport'
      DIAGNOSTIC_ISR: 'isr/diagnosticReport'
      SUMMATIVE_ISR: 'isr/summativeReport'
      SCHRPT: 'schoolReport'
    }

    DELIMITOR: {
      COMMA: ','
      NEWLINE: '\n'
    }

    ASMT_TYPE: {
      SUMMATIVE: 'SUMMATIVE',
      DIAGNOSTIC: 'DIAGNOSTIC'
    }

    EVENTS: {
      SORT_COLUMNS: 'edwareOnSortColumns'
      EXPAND_COLUMN: 'edwareOnExpandColumn'
      SUBJECT_CHANGE: 'subject'
      SUMMARY_CHANGE: 'summaries'
      RESULT_CHANGE: 'result'
      ASSESSMENT_TYPE_CHANGE: 'asmtType'
      GRADE_COURSE_CHANGE: 'gradeCourse'
      VIEW_CHANGE: 'view'
      COMPARE_SOURCE_CHANGE: 'compare_source'
      GRID_CHECKBOX_CHANGE: 'redrawBubble'
      HEADER_CHECKBOX_CHANGE: 'redrawAllBubbles'
      GRIDROW_CHANGE: 'activeBubble'
      FILTER_CHANGE: 'filter_change_trigger'
      YEAR_CHANGE: 'year'
      DATE_CHANGE: 'asmt_date'
      EXPORT_TYPE_CHANGE: 'exportType',
      EXPORT_STATUS_CHANGE: 'readyForExport'
      DOWNLOAD_CSV: 'onCSVDownload'
    }

    KEYS: {
      F: 70
      ENTER: 13
      SHIFT: 16
      SPACE: 8
      ESC: 27
      UP_ARROW: 38
      DOWN_ARROW: 40
      TAB: 9
    }

    # TODO: below constants are for parcc, need to clean up constants from SBAC above
    VIEWS: {
      SCORES: "scores",
      ITEM_ANALYSIS: "itemAnalysis",
      PERFORMANCE: "performance",
      GROWTH: "growth"
      READER_MOTIV: "reader_motiv"
    }

    RESULT_TYPES: {
      OVERALL: "overall"
      SUBSCORE: "subscore"
    }

    PERFORMANCE_RESULT_COLUMN_MAP: {
      "overall": "overall"
      "literarytext": "subscore",
      "infotext": "subscore",
      "vocab": "subscore",
      "writingexp": "subscore",
      "knwldconv": "subscore",
      "majorcontent": "subscore",
      "addcontent": "subscore",
      "mathreason": "subscore",
      "modelapp": "subscore"
    }

    SUBJECTS: {
      MATH: "subject1"
      ELA: "subject2"
    }

    ISR_PRINT_SUBJECTS: {
      subject1: "Mathematics"
      subject2: "English Language Arts / Literacy"
    }

    TOTAL_POINTS_POSSIBLE: {
      writing: 60
      reading: 50
    }

    CLAIM_BAR_SIZE: 90

    NOT_APPLICABLE_TEXT: 'N/A'

    BREADCRUMB_TYPES: {
      HOME: 'home'
      STATE: 'state'
      DISTRICT: 'district'
      SCHOOL: 'school'
      GRADECOURSE: 'grade'
      STUDENT: 'student'
    }

    QUERY_PARAM: [
      'stateCode',
      'districtGuid',
      'schoolGuid',
      'gradeCourse',
      'student'
    ]


    FILTER_TYPES: {
      SEX: 'sex',
      ETHNICITY: 'ethnicity',
      DISABILITIES: 'disabilities',
      ENGLISH_LEARNER: 'englishLearner',
      ECONOMIC_DISADVANTAGES: 'econoDisadvantage'
    }

    GRADES_COURSES: {
      SUBJECT1: {
        THIRD: 'Grade 3'
        FOURTH: 'Grade 4'
        FIFTH: 'Grade 5'
        SIXTH: 'Grade 6'
        SEVENTH: 'Grade 7'
        EIGHTH: 'Grade 8'
        ALG_1: 'Algebra I'
        ALG_2: 'Algebra II'
        GEO: 'Geometry'
        MATH_1: 'Integrated Mathematics I'
        MATH_2: 'Integrated Mathematics II'
        MATH_3: 'Integrated Mathematics III'
      }
      SUBJECT2: {
        THIRD: 'Grade 3'
        FOURTH: 'Grade 4'
        FIFTH: 'Grade 5'
        SIXTH: 'Grade 6'
        SEVENTH: 'Grade 7'
        EIGHTH: 'Grade 8'
        NINTH: 'Grade 9'
        TENTH: 'Grade 10'
        ELEVENTH: 'Grade 11'
      }
    }

    CCR_COURSES: ['Integrated Mathematics III', 'Algebra II']

    ON_TRACK_COURSES: [
      'Integrated Mathematics I',
      'Integrated Mathematics II',
      'Algebra I',
      'Geometry',
      'Grade 9',
      'Grade 10',
      'Grade 11'
    ]

    SHARED_COURSES: [
      'Grade 3',
      'Grade 4',
      'Grade 5',
      'Grade 6',
      'Grade 7',
      'Grade 8'
    ]
  }
