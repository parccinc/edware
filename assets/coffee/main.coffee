require {
# version
  urlArgs: 'v1',
  baseUrl: '../js/src/',
  paths: {
  # globals
    jquery: '../3p/jquery-1.7.2.min',
    jqueryui: '../3p/jquery-ui-1.8.16.min',
    jqueryeventdrag: '../3p/jquery.event.drag-2.2',
#    jqueryhistory: '../3p/history.adapter.jquery',
    history: '../3p/history',
    slickcore: '../3p/slick.core',
    slickgrid: '../3p/slick.grid',
    bootstrap: '../3p/bootstrap.min',
    text: '../3p/text',
    mustache: '../3p/mustache',
    usmap: '../3p/usmap/jquery.usmap',
    raphael: '../3p/usmap/raphael',
    cs: '../3p/cs',
    react: '../3p/react',
    reactbootstrap: '../3p/react-bootstrap',
    'JSXTransformer': '../3p/JSXTransformer',
    'coffee-script': '../3p/coffee-script',
    d3: '../3p/d3.v3.min',
    moment: '../3p/moment',
    'moment-range': '../3p/moment-range',
    daterangepicker: '../3p/daterangepicker'

  #Base module
    EdwareApplication: 'modules/EDWARE.application',
  # modules
    'Edware.StudentReport': 'modules/EDWARE.studentReport',
    'studentSummativeReport': 'modules/studentSummativeReport',
    'studentDiagnosticReport': 'modules/studentDiagnosticReport',
    'Edware.StudentRosterReport': 'modules/EDWARE.studentRosterReport',
    'Edware.SchoolReport': 'modules/EDWARE.schoolReport',
    'Edware.ComparingPopulationsReport': 'modules/EDWARE.comparingPopulationsReport',
    'Edware.InitialSetup': 'modules/EDWARE.initialSetup',
    'Edware.StateViewRedirect': 'modules/EDWARE.stateViewRedirect',

    edware: 'modules/EDWARE',
    edwareEvents: 'modules/EDWARE.events',
    edwareContextSecurity: 'modules/EDWARE.contextSecurity',
    edwareUtil: 'modules/EDWARE.util',
    edwareConstants: 'modules/EDWARE.constants',
    edwareDataProxy: 'modules/EDWARE.dataProxy',
    edwareSharedReportUtil: 'modules/EDWARE.sharedReportUtil',
    edwareGridFormatters: 'widgets/grid/formatter/EDWARE.grid.formatters',
    edwareGrowthChart: 'widgets/growthChart/EDWARE.growthChart',
    edwareGrowthPopover: 'widgets/growthChart/EDWARE.growthPopover',
    edwareComparisonSource: 'widgets/growthChart/EDWARE.comparisonSource',
    edwareGrowthSummary: 'widgets/growthChart/EDWARE.growthSummary',
    edwareBreadcrumbs: 'widgets/breadcrumb/EDWARE.breadcrumbs',
    edwareHeader: 'widgets/header/EDWARE.header',
    edwarePopulationBar: 'widgets/populationBar/EDWARE.populationBar',
    edwareLegend: 'widgets/legend/EDWARE.legend',
    edwareFootnote: 'widgets/footnote/EDWARE.footnote',
    edwareLoadingMask: 'widgets/loadingMask/EDWARE.loadingMask',
    edwareFilterBar: 'widgets/filter/filterBar',
    edwareClientStorage: 'widgets/clientStorage/EDWARE.clientStorage',
    edwareYearSelector: 'widgets/selectors/EDWARE.yearSelector',
    edwareReportInfoBar: 'widgets/header/EDWARE.infoBar',
    edwareHelpMenu: 'widgets/header/EDWARE.helpMenu',
    edwarePrint: 'widgets/print/EDWARE.print',
    edwarePrintURL: 'widgets/print/EDWARE.printURL',
    edwareModal: 'widgets/modal/EDWARE.modal',
    edwareSubjectSelector: 'widgets/selectors/EDWARE.subjectSelector',
    edwareGradeCourseSelector: 'widgets/selectors/EDWARE.gradeCourseSelector'
    edwareMultiGradeCourseSelector: 'widgets/selectors/EDWARE.multiGradeCourseSelector'
    selectorUtil: 'modules/selector.util',
    edwareResultSelector: 'widgets/selectors/EDWARE.resultSelector',
    edwareExportTypeSelector: 'widgets/selectors/EDWARE.exportTypeSelector',
    edwareViewSelector: 'widgets/viewSelector/EDWARE.viewSelector',
    edwareDataExports: 'widgets/header/EDWARE.dataExports',
    edwareDateRangePicker: 'widgets/dateRangePicker/EDWARE.dateRangePicker',
    stickies: 'widgets/sticker/stickies',
    dropdownMixin: 'widgets/selectors/dropdown.mixin',

    csvExport: 'widgets/exportCsv/exportCSV',
    csvDownload: 'widgets/exportCsv/downloadCSV',

  # components
    listReport: "widgets/report/ListReport",

    edwareGrid: 'widgets/grid/EDWARE.grid',
    edwareGridAdapter: 'widgets/grid/slick/EDWARE.gridAdapter',
    SlickDataSource: 'widgets/grid/slick/DataSources/SlickDataSource',
    SlickSummaryDataSource: 'widgets/grid/slick/DataSources/SlickSummaryDataSource',
    SlickGroupDataSource: 'widgets/grid/slick/DataSources/SlickGroupDataSource',
    AssessmentReportDataAdapter: 'widgets/grid/slick/DataSources/AssessmentReportDataAdapter',
    SlickColumnModel: 'widgets/grid/slick/DataSources/SlickColumnModel',
    compareControls: 'widgets/compareBar/compareBar',
    SlickExtraInfoPopover: 'widgets/grid/decorators/ExtraInfoPopover',
    SlickColumnGroupHeaders: 'widgets/grid/decorators/ColumnGroupHeaders',
    SlickGridPagination: 'widgets/grid/decorators/GridPagination',
    SlickGridFilter: 'widgets/grid/decorators/GridFilter',
    SlickGridAccessibility: 'widgets/grid/decorators/GridAccessibility',
    StudentRosterColumnModel: 'widgets/grid/slick/DataSources/StudentRosterColumnModel',
    DiagnosticColumnModel: 'widgets/grid/slick/DataSources/DiagnosticColumnModel',
    evidenceStatementProvider: 'widgets/evidenceStatementPopover/evidenceStatementProvider',
    clusterTooltipProvider: 'widgets/clusterPopover/clusterTooltipProvider',
    expressivenessTooltipProvider: 'widgets/expressivenessPopover/expressivenessTooltipProvider',


  # Templates
    populationBarTemplate: 'widgets/populationBar/populationBarTemplate.html',
    claimAvgTemplate: 'widgets/grid/formatter/templates/claimAvgTemplate.html',
    perfSectionTemplate: 'widgets/perfSection/template.html',
    perfSectionBarTemplate: 'widgets/perfSection/perfBarTemplate.html',
    printIsrFaqTemplate: 'widgets/print/EDWARE.aboutTemplate.html'
  },
  shim: {
    'slickgrid': {
      deps: ['jquery', 'slickcore', 'jqueryeventdrag', 'jqueryui'],
      exports: 'slickgrid'
    },
    'slickcore': {
      deps: ['jquery'],
      exports: 'slickcore'
    },
    'jqueryui': {
      deps: ['jquery'],
      exports: 'jqueryui'
    },
    'jqueryeventdrag': {
      deps: ['jquery'],
      exports: 'jqueryeventdrag'
    },
    'history': {
      deps: ['jquery'],
      exports: 'history'
    },
    'bootstrap': {
      deps: ['jquery'],
      exports: 'bootstrap'
    },
    'usmap': {
      deps: ['jquery', 'raphael'],
      exports: 'usmap'
    },
    'reactbootstrap': {
      deps: ['react']
    },
    'moment-range': {
      deps: ['moment']
    },
    'daterangepicker': {
      deps: ['moment', 'bootstrap'],
      exports: 'daterangepicker'
    }
  }
}
