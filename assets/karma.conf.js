

module.exports = function(config) {

	/**
	 * array of files that need to be preloaded before app is bootstraped
	 * all other files will be added after this
	*/
	var dependencies = [];

	config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine', 'requirejs'],


    // list of files / patterns to load in the browser
    files: dependencies.concat([
      'test-main.js',
      {pattern: './js/**/*.js', included: false},
      {pattern: './data/**/*.json', included: false},
      {pattern: './test/js/**/*.spec.js', included: false}
    ]),


    // list of files to exclude
    exclude: [
    	//'js/modules/*.js'
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      'js/src/**/*.js': 'coverage'
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: [
      'progress',
      'notify',
      'coverage'
    ],//progress

    // notifyReporter config (Optional)
    notifyReporter: {
      reportEachFailure: false, // Default: false, Will notify on every failed sepc
      reportSuccess: false, // Default: true, Will notify when a suite was successful
    },

    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR
    // || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_WARN,//LOG_INFO


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    /*Chrome*/
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    /*
     'karma-chrome-launcher',
     'karma-firefox-launcher',
     */
    plugins: [
      'karma-jasmine',
      'karma-requirejs',
      'karma-coverage',
      'karma-notify-reporter',
      'karma-phantomjs-launcher',
      'karma-chrome-launcher'
    ],

    coverageReporter : {
      type: 'html',
      dir: 'coverage'
    }

  });
};