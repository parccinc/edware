var gulp = require('gulp'),
    // coffee = require('gulp-coffee'),
    coffee = require('gulp-coffee-react'),
    less = require('gulp-less'),
    LessPluginAutoPrefix = require('less-plugin-autoprefix'),
    minifyCSS = require('gulp-minify-css'),
    runSequence = require('run-sequence'),
    del = require('del'),
    plumber = require('gulp-plumber'),
    requirejs = require('requirejs'),
    karma = require('karma').server,
    coffeeLint = require('gulp-coffeelint'),
    coffeeReactTransform = require('gulp-coffee-react-transform'),
    gutil = require('gulp-util'),
    changed = require('gulp-changed')/*,
    temp disable because of python version issues
    gulpif = require('gulp-if'),
    sprite = require('css-sprite').stream
    */;

var AUTOPREFIXER_BROWSERS = [
    'last 4 versions',
    'ie >= 9',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7'
];


var autoprefix= new LessPluginAutoPrefix({browsers: AUTOPREFIXER_BROWSERS});
var config = {
    optimizeModules: [
        'Edware.ComparingPopulationsReport',
        'Edware.SchoolReport',
        'Edware.StudentReport',
        'Edware.StudentRosterReport',
        'Edware.StateViewRedirect',
        'studentDiagnosticReport',
        'studentSummativeReport',
        'Edware.InitialSetup'
    ]
};
gulp.task('compile', function(cb){
    // copy over templates, required by optimizer
    gulp.src('coffee/**/*.html').pipe(gulp.dest('js/build'));
    var stream = gulp.src('coffee/**/*.coffee')
        .pipe(plumber())
        .pipe(changed('js/build', {extension: '.js'}))
        .pipe(coffee())
        .pipe(gulp.dest('js/build'));
    stream.on('end', function(){
        cb();
    });
});

gulp.task('clean-tests', function (cb) {
    del(['test/js'], function(err, deletedFiles) {
        console.log('File deleted:', deletedFiles.join(','));
        cb();
    });
});

gulp.task('compile-tests', ['clean-tests'], function (cb){
    var stream = gulp.src('test/coffee/**/*.spec.coffee')
        .pipe(plumber())
        .pipe(changed('test/js', {extension: '.js'}))
        .pipe(coffee())
        .pipe(gulp.dest('test/js'));
    stream.on('end', function(){
        cb();
    });
});

gulp.task('optimize', ['compile', 'less'], function(cb) {
    //minify css
    gulp.src('css/*.css')
        .pipe(minifyCSS())
        .pipe(changed('css'))
        .pipe(gulp.dest('css'));

    //combine and uglify js
    requirejs.optimize({
        mainConfigFile: 'js/build/main.js',
        baseUrl: 'js/build',
        dir: 'js/src',
        removeCombined: true,
        modules: config.optimizeModules.map(function(moduleName){
            return {name: moduleName};
        })
    }, function(){cb();}, function(err){cb(err);});
});

gulp.task('test', function (done) {
    karma.start({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, done);
});

gulp.task('tdd', function (done) {
  karma.start({
    configFile: __dirname + '/karma.conf.js'
  }, function(arg1, arg2){
    console.log(arg1, arg2, arguments);
    done(arg1, arg2);
  });
});

gulp.task('less', function () {
    gulp.src(['less/style.less'])
        .pipe(plumber())
        //.pipe(changed('css', {extension: '.css'}))
        .pipe(less({
            plugins: [autoprefix]
        }))
        .pipe(gulp.dest('css'));
});
/*
gulp.task('sprites', function (cb) {
    return gulp.src('sprites/**\/*.png')
        .pipe(sprite({
            name: 'sprite',
            processor: 'less',
            cssPath: '../images/',
            style: 'less/sprite.less'
        }, function(){
            console.log(arguments);
            cb();
        }))
        .pipe(gulpif('*.png', gulp.dest('images/'), gulp.dest('less/')))
});
*/

gulp.task('copy', ['compile'], function () {
    gulp.src('coffee/**/*.html')
        .pipe(gulp.dest('js/src'))
        .pipe(changed('js/src'))
        /*
        * not needed anymore because we are running tests directly on js/src
        .pipe(gulp.dest('test/js/src'))*/;
    return gulp.src('js/build/**/*.js')
        .pipe(changed('js/src'))
        .pipe(gulp.dest('js/src'))
        /*.pipe(gulp.dest('test/js/src'))*/;
});


gulp.task('clean', function(cb) {
    del(['js/build', 'js/src'], function(err, deletedFiles) {
        console.log('File deleted:', deletedFiles.join(','));
        cb();
    });
});

gulp.task('lint', function(){
    gulp.src('coffee/**/*.coffee')
        .pipe(coffeeReactTransform().on('error', gutil.log))
        .pipe(coffeeLint())
        .pipe(coffeeLint.reporter());

    gulp.src('test/coffee/**/*.coffee')
        .pipe(coffeeReactTransform().on('error', gutil.log))
        .pipe(coffeeLint())
        .pipe(coffeeLint.reporter());
});

gulp.task('watch', function() {
    gulp.watch('./coffee/**/*.coffee', ['lint','compile', 'copy']);
    gulp.watch('./coffee/**/*.html', ['copy']);
    gulp.watch('less/**/*.less', ['less']);
    gulp.watch('./test/coffee/**/*.spec.coffee', ['lint', 'compile-tests']);
});

gulp.task('dev', function(cb) {
    runSequence('clean', ['lint', 'compile', 'less', 'copy', 'watch'],'compile-tests', 'tdd', cb);
});

gulp.task('preparetests', function(cb) {
    runSequence('clean', ['compile', 'copy', 'compile-tests'], cb);
});

gulp.task('prod', function(cb) {
    runSequence('clean', ['compile', 'less', 'copy'], 'optimize', cb);
});

gulp.task('default', ['dev']);
