define [
  'edwareHelpMenu',
  'react',
  'jquery',
  'edwareDataProxy',
], (edwareHelpMenu, React, $, edwareDataProxy) ->

  describe 'HelpMenuModal', () ->
    helpModal = undefined
    container = null
    config = {
      "labels": {
        "avg": "avg",
        "avg_score": "avg score",
        "avg_students_at_level_4": "AVG OF STUDENTS AT LEVEL 4",
        "breadcrumb_home": "Home",
        "district": "District",
        "districts": "DISTRICTS",
        "distribution": "distribution",
        "grade": "Grade",
        "help": "Help",
        "export_data": "Export Data",
        "level": "Level",
        "logout": "Log Out",
        "margin_of_error": "Margin of error",
        "overall_performance": "OVERALL PERFORMANCE",
        "range": "Range",
        "schools": "SCHOOLS",
        "students": "STUDENTS",
        "student_score": "Student Score",
        "subscore": "subscore",
        "compared_to_parcc": "Compared to Parcc",
        "compared_to_state": "Compared to State",
        "student_growth_percentile": "Student Growth Percentile",
        "child": "child",
        "student": "student",
        "report_name": "Report Name",
        "date": "Date/Time",
        "institution": "Institution",
        "filter_options": "Filter Options",
        "sort_by": "Sort By",
        "asmt_type": "Assessment",
        "total_count": "Total Count",
        "academic_year": "Academic Year",
        "subject": "Subject",
        "grade_course": "Grade/Course",
        "result": "Result",
        "view": "View",
        "help_tab_faq": "FAQ",
        "help_tab_guide": "User Guide",
        "help_tab_glossary": "Glosarry",
        "help_tab_resources": "Resources"
      },
      data: {
        "resources_content": {
          "header": "Aenean imperdiet, justo id fringilla auctor, nulla quam sollicitudin metus, eu congue nisi diam ornare mauris.:",
          "items": [
            "<a class='help_anchor' href='https://google.com' target='_blank'>Lorem ipsum dolor sit amet</a>",
            "<a class='help_anchor' href='https://google.com' target='_blank'>Pellentesque facilisis</a>",
            "<a class='help_anchor' href='https://google.com' target='_blank'>Maecenas mollis, tellus</a>",
            "<a class='help_anchor' href='https://google.com' target='_blank'>Donec fermentum</a>"
          ]
        },
        "video_content": {
          "items": [
              "<a class='help_anchor' href='/assets/data/video/story.html' target='_blank'>Video Guide</a>"
            ]
        },
        "faq_content": [
          {
            "name": "General",
            "key": "general",
            "questions": [
              {
                "id": 1,
                "question": "Lorem ipsum?",
                "answer": "Aliquam erat volutpat."
              },
              {
                "id": 2,
                "question": "pellentesque turpis vel?",
                "answer": "consectetur diam"
              }
            ]
          },
          {
            "name": "For Educators",
            "key": "educator",
            "questions": [
              {
                "id": 1,
                "question": "ligula, non ?",
                "answer": "Sed lacinia pretium felis."
              },
              {
                "id": 2,
                "question": "turpis, ac accumsan tellus. ?",
                "answer": "Integer rhoncus leo aliquam sapien aliquam."
              }
            ]
          },
          {
            "name": "For Parents",
            "key": "parents",
            "questions": [
              {
                "id": 1,
                "question": "Pellentesque quis turpis urna?",
                "answer": "Aliquam cursus ultrices fringilla. Vestibulum lacinia dapibus nunc."
              },
              {
                "id": 2,
                "question": "Quisque malesuada feugiat sapien a euismod?",
                "answer": "Proin fermentum."
              },
              {
                "id": 3,
                "question": "Quisque consequat?",
                "answer": "Duis euismod nibh."
              }
            ]
          }
        ],
        "user_guide_content": [
          {
            "display_text": "Download the User Guide",
            "link": "../data/smarter_balanced_reporting_user_guide.pdf"
          }
        ],
        "glossary_content": [
          {
            "alphabet": "#",
            "has_data": true,
            "info": [
              {
                "info_term": "504 plan",
                "info_def": "The 504 Plan "
              }
            ]
          },
          {
            "alphabet": "A",
            "has_data": true,
            "info": [
              {
                "info_term": "Achievement Level Descriptors (ALDs)",
                "info_def": "Statements that "
              }
            ]
          },
          {
            "alphabet": "B",
            "has_data": true,
            "info": [
              {
                "info_term": "Breadcrumb Navigation Trail",
                "info_def": "List of report titles "
              }
            ]
          },
          {
            "alphabet": "C",
            "has_data": true,
            "info": [
              {
                "info_term": "Claim Level",
                "info_def": "Claim performance "
              }
            ]
          },
          {
            "alphabet": "D",
            "has_data": true,
            "info": [
              {
                "info_term": "Download",
                "info_def": "A navigation."
              }
            ]
          },
          {
            "alphabet": "E",
            "has_data": true,
            "info": [
              {
                "info_term": "Economic Disadvantage",
                "info_def": "A locally-defined."
              },
              {
                "info_term": "Error band",
                "info_def": "Smarter Balanced."
              }
            ]
          },
          {
            "alphabet": "F",
            "has_data": true,
            "info": [
              {
                "info_term": "Filter",
                "info_def": "A navigation."
              }
            ]
          },
          {
            "alphabet": "G",
            "has_data": false,
            "info": []
          },
          {
            "alphabet": "H",
            "has_data": true,
            "info": [
              {
                "info_term": "Help Section",
                "info_def": "Access to ."
              }
            ]
          },
          {
            "alphabet": "I",
            "has_data": true,
            "info": [
              {
                "info_term": "Interim",
                "info_def": "Interim assessments ."
              }
            ]
          },
          {
            "alphabet": "J",
            "has_data": false,
            "info": []
          },
          {
            "alphabet": "K",
            "has_data": false,
            "info": []
          },
          {
            "alphabet": "L",
            "has_data": true,
            "info": [
              {
                "info_term": "Legend",
                "info_def": "Report feature ."
              }
            ]
          },
          {
            "alphabet": "M",
            "has_data": true,
            "info": [
              {
                "info_term": "Mouse Over (Hover Over)",
                "info_def": "A navigation feature."
              }
            ]
          },
          {
            "alphabet": "N",
            "has_data": false,
            "info": []
          },
          {
            "alphabet": "O",
            "has_data": false,
            "info": []
          },
          {
            "alphabet": "P",
            "has_data": true,
            "info": [
              {
                "info_term": "Personally Identifiable Information (PII)",
                "info_def": "Personally Identifiable Information"
              },
              {
                "info_term": "Print",
                "info_def": "A navigation feature that"
              }
            ]
          },
          {
            "alphabet": "Q",
            "has_data": false,
            "info": []
          },
          {
            "alphabet": "R",
            "has_data": true,
            "info": [
              {
                "info_term": "Report Information ",
                "info_def": "An icon that appears"
              }
            ]
          },
          {
            "alphabet": "S",
            "has_data": true,
            "info": [
              {
                "info_term": "Select",
                "info_def": "A navigation feature."
              },
              {
                "info_term": "Smarter Balanced Summative Assessment",
                "info_def": "The Smarter Balanced assessment."
              },
              {
                "info_term": "Smarter Balanced Interim Assessment Block (IAB)",
                "info_def": "Interim Assessment"
              },
              {
                "info_term": "Smarter Balanced Interim Comprehensive Assessment (ICA)",
                "info_def": "Interim"
              },
              {
                "info_term": "Sort",
                "info_def": "A navigation feature that ."
              },
              {
                "info_term": "Student attributes",
                "info_def": "Student attributes include: grade level"
              },
              {
                "info_term": "Sub-group",
                "info_def": "Any group of individual"
              }
            ]
          },
          {
            "alphabet": "T",
            "has_data": false,
            "info": []
          },
          {
            "alphabet": "U",
            "has_data": false,
            "info": []
          },
          {
            "alphabet": "V",
            "has_data": false,
            "info": []
          },
          {
            "alphabet": "W",
            "has_data": false,
            "info": []
          },
          {
            "alphabet": "X",
            "has_data": false,
            "info": []
          },
          {
            "alphabet": "Y",
            "has_data": false,
            "info": []
          },
          {
            "alphabet": "Z",
            "has_data": false,
            "info": []
          }
        ],
        "faq_footer": "If you require assistance"
      }
    }

    beforeEach () ->

      container = $('<div class="HelpMenuContainer"></div>')
      $(document.body).append(container)
      helpModal = edwareHelpMenu.create '.HelpMenuContainer', config

    afterEach () ->
      container.remove()

    describe 'Instantiation ,Rendering, Tabs', () ->

      beforeEach () ->
        spyOn($.fn, 'edwareModal').and.callThrough()
        spyOn($.fn, 'tab').and.callThrough()

      it 'should instantiate', () ->
        expect(helpModal).toBeDefined()

      it 'should render', () ->
        helpModal.render()
        expect(helpModal.component).toBeDefined()
        expect(helpModal.component.isMounted()).toBe(true)

      it 'should display modal when show() is called', () ->
        helpModal.show()
        expect($.fn.edwareModal).toHaveBeenCalled()


