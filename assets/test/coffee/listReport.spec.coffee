define ['listReport'], (ListReport) ->

  describe 'ListReport Module', () ->
    report = null

    describe 'sanity check', () ->
      it 'has a buildForReport helper', () ->
        expect(ListReport.buildForReport).toBeDefined()

      it 'has a Report class', () ->
        expect(ListReport.Report).toBeDefined()

      it 'has a Builder class', () ->
        expect(ListReport.Builder).toBeDefined()

    describe 'listReport methods', () ->
      report = null

      beforeEach () ->
        report = new ListReport.Report()

      it 'doesn\'t leak state', () ->
        state = {
          nestedObject: {'key': 'value'}
        }
        report.setState state
        state.nestedObject.key = 'other value'
        expect(report.getState().nestedObject.key).toBe('value')

    describe 'ReportBuilder methods', () ->
      class DummyDataSource

      class DummySummaryProvider

      class DummyColumnProvider

        @getColumns: (state) ->
          if state.asmtType is 'SUMMATIVE'
            # these are not the real column model
            return ["Dummy Column 1", "Dummy Column 2"]

      builder = null
      module = {
        getReportTitle: () -> ["My Title", "My Subtitle"]
        getReportType: () -> "Comparing Populations"
        gridObj: {
          getDataSource: () -> DummyDataSource
          headerDataSource: DummySummaryProvider
          getColumnProvider: () -> DummyColumnProvider
        }
        data: {
          context: {
            items: [
              {"type": "home", "name": "Home"},
              {"id": "RI", "type": "state", "name": "Rhode Island"},
              {"id": "R0003", "type": "district", "name": "Providence"}
            ]
          }
          subjects: [
            {"value": "subject1", "label": "Mathematics"},
            {"value": "subject2", "label": "ELA"}
          ]
        }
        config: {
          breadcrumb: {
            items: [
              {"type": "home", "link": "/assets/html/stateMap.html"},
              {"type": "state", "queryParam": "stateCode", "link": "/assets/html/comparingPopulationsReport.html"},
              {
                "type": "district",
                "queryParam": "districtGuid",
                "link": "/assets/html/comparingPopulationsReport.html"
              },
              {
                "type": "school",
                "queryParam": "schoolGuid",
                "link": "/assets/html/schoolReport.html",
                "ignoreParams": [
                  "gradeCourse", "view"
                ]
              },
              {"type": "grade", "queryParam": "gradeCourse", "link": "/assets/html/studentRosterReport.html"},
              {"type": "student", "queryParam": "student"}
            ]
          }
          filters: [
            {
              display: "Gender",
              name: "sex",
              options: [
                {label: "Male", value: "M"},
                {label: "Female", value: "F"},
                {label: "Not Specified", value: "NS"}
              ],
              tag: "Gender"
            }
          ]
          labels: {"breadcrumb_home": "Home"}
          resultsLists: [
            {
              "states": {"asmtType": "SUMMATIVE"},
              "group": "Summative",
              "header": true,
              "label": "Summative",
              "showLabel": true,
              "options": [
                {
                  "label": "Overall Performance",
                  "states": {"result": "overall"}
                },
                {
                  "for": {"report": ["CONSORTIUM", "STATE", "DISTRICT", "SCHOOL"]},
                  "options": [
                    {
                      "for": {"subject": ["subject1"]},
                      "options": [
                        {
                          "group": "Subscores",
                          "label": "Sub-Scores",
                          "subheader": true,
                          "options": [
                            {
                              "classes": "sub",
                              "label": "Major Content",
                              "states": {"result": "majorcontent"}
                            },
                            {
                              "classes": "sub",
                              "label": "Additional & Supporting Content",
                              "states": {"result": "addcontent"}
                            },
                            {
                              "classes": "sub",
                              "states": {"result": "modelapp"},
                              "label": "Modeling and Application"
                            },
                            {
                              "classes": "sub",
                              "states": {"result": "mathreason"},
                              "label": "Expressing Mathematical Reasoning"
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "for": {"subject": ["subject2"]},
                      "options": [
                        {
                          "group": "reading",
                          "label": "Reading",
                          "subheader": true,
                          "options": [
                            {
                              "classes": "sub",
                              "states": {"result": "literarytext"},
                              "label": "Literary Text"
                            },
                            {
                              "classes": "sub",
                              "states": {"result": "infotext"},
                              "label": "Informational Text"
                            },
                            {
                              "classes": "sub",
                              "states": {"result": "vocab"},
                              "label": "Vocabulary"
                            }
                          ]
                        },
                        {
                          "group": "writing",
                          "label": "Writing",
                          "subheader": true,
                          "options": [
                            {
                              "classes": "sub",
                              "label": "Writing Expression",
                              "states": {"result": "writingexp"}
                            },
                            {
                              "classes": "sub",
                              "label": "Knowledge & Use of Language Conventions",
                              "states": {"result": "knwldconv"}
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            },
            {
              "states": {"asmtType": "DIAGNOSTICS"},
              "group": "Diagnostics",
              "header": true,
              "label": "Non-Summative",
              "showLabel": false,
              "options": [
                {
                  "for": {"report": ["SCHOOL", "STUDENT"]},
                  "label": "Diagnostic Assessments",
                  "states": {"result": "overall"}
                },
                {
                  "for": {"report": ["CONSORTIUM", "STATE", "DISTRICT", "GRADE"]},
                  "disabled": true,
                  "label": "Diagnostic Assessments",
                  "states": {"result": "overall"}
                }
              ]
            }
          ]
          viewsList: [
            {"label": "Performance", "value": "performance", "cssclass": "view-icon performance"},
            {"label": "Growth", "value": "growth", "cssclass": "view-icon growth"}
          ]
        }
      }
      state = {
        asmtType: 'SUMMATIVE'
        result: 'overall'
        subject: 'subject1'
        view: 'performance',
        schoolGuid: '124'
      }


      beforeEach () ->
        builder = new ListReport.Builder()

      it 'sets title and subtitle', () ->
        builder.titles(module)
        expect(builder.report.getTitle()).toBe('My Title')
        expect(builder.report.getSubtitle()).toBe('My Subtitle')

      it 'sets data provider', () ->
        builder.dataProvider(module)
        expect(builder.report.getDataProvider()).toBe(DummyDataSource)

      it 'sets summary provider', () ->
        builder.summaryProvider(module)
        expect(builder.report.getSummaryProvider()).toBe(DummySummaryProvider)

      it 'sets columns', () ->
        builder.columns(module, state)
        expect(builder.report.getColumns(state)).toEqual(["Dummy Column 1", "Dummy Column 2"])

      it 'sets breadcrumbs', () ->
        builder.breadcrumbs(module)
        expect(builder.report.getBreadCrumbs().length).toEqual(3)

      it 'sets filter options', () ->
        builder.filters(module, state)
        # Didn't click any options, expect number of filter options to be zero
        expect(builder.report.getFilterOptions().length).toBe(0)

      it 'sets report type', () ->
        builder.reportType(module)
        expect(builder.report.getReportType()).toBe("Comparing Populations")

      it 'sets state', () ->
        builder.state(state)
        expect(builder.report.getState()).toEqual(state)

      it 'sets result', () ->
        builder.result(module, state)
        expect(builder.report.getResult().label).toEqual('Overall Performance')

      it 'sets subject', () ->
        builder.subject(module, state)
        expect(builder.report.getSubject()).toEqual({"value": "subject1", "label": "Mathematics"})

      it 'sets view', () ->
        builder.view(module, state)
        expect(builder.report.getView())
        .toEqual(
          jasmine.objectContaining({
            "label": "Performance",
            "value": "performance",
            "cssclass": "view-icon performance"
          })
        )

      # it 'sets sort column', () ->
      #   # TODO
