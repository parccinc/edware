define [
  'SlickGridPagination'
  'react'
  'jquery'
  'edwareUtil'
], (SlickGridPagination,
    React,
    $,
    edwareUtil) ->
  describe 'GridPagination', () ->
    gridPagination = {}
    showColumns = 8
    columns = [0...10]

    class Grid
      setColumns: (columns) ->
        columns

      getConfig: () ->
        {pagination: {}}

    class MockEvent
      preventDefault: () ->
        true

    edwareGrid = new Grid()
    grid = new Grid()
    header = new Grid()

    gridContext = {
      columns: columns,
      grid: grid,
      type: 'grid',
      wrapper: edwareGrid
    }

    headerContext = {
      columns: columns,
      grid: header,
      type: 'header',
      wrapper: edwareGrid
    }
    $('body')
    .append('<div class="slick-header-bar" data-component-header-bar>')
    .append('<div id="gridHeader" data-component-header-grid></div>')
    .append('<div id="gridTable" data-component-grid></div>')

    beforeEach () ->
      gridPagination = new SlickGridPagination.GridPagination({showColumns: showColumns})

    afterEach () ->
      gridPagination.destroy(gridContext)
      gridPagination.destroy(headerContext)

    describe 'GridPagination Creation', () ->
      it 'should instantiate the plugin', () ->
        expect(gridPagination).toBeDefined()

    describe 'GridPagination Initialization', () ->

      beforeEach () ->
        gridPagination.init(gridContext)

      it 'should initialize the plugin with a given context', () ->
        expect(gridPagination.getContexts()).toContain(gridContext)

      it 'should initialize the plugin with multiple contexts', () ->
        gridPagination.init(headerContext)

        expect(gridPagination.getContexts().length).toBe(2)
        expect(gridPagination.getContexts()).toContain(headerContext)

      it 'should mount the React component', () ->
        expect(gridPagination.getComponent().isMounted()).toBe(true)

      describe 'GridPagination Functions', () ->

        beforeEach () ->
          spyOn(edwareGrid, 'setColumns').and.callThrough()
          spyOn(gridPagination, 'updateComponent').and.callThrough()
          spyOn(gridPagination, 'destroy').and.callThrough()

        it 'should return right number of columns', () ->
          expect(gridPagination.getTotalNumberOfColumns()).toBe(columns.length)

        it 'should not be able to page left initially', () ->
          expect(gridPagination.canPageLeft()).toBe(false)

        it 'should be able to page right initially', () ->
          expect(gridPagination.canPageRight()).toBe(true)

        it 'should page right', () ->
          e = new MockEvent()
          gridPagination.nextPage(e)
          remaining = Math.min(showColumns - 1, columns.length - (8 + showColumns - 1))
          expect(gridPagination.getRemainingOnRight()).toBe(remaining)
          expect(edwareGrid.setColumns).toHaveBeenCalled()

        it 'should be able to page left after paged right once', () ->
          e = new MockEvent()
          gridPagination.nextPage(e)
          expect(gridPagination.canPageLeft()).toBe(true)

        it 'should page left', () ->
          e = new MockEvent()
          gridPagination.nextPage(e)
          gridPagination.prevPage(e)
          expect(gridPagination.getRemainingOnLeft()).toBe(Math.min(showColumns - 1, 1))
          expect(edwareGrid.setColumns).toHaveBeenCalled()

        it 'should update the react component on page', () ->
          e = new MockEvent()
          gridPagination.nextPage(e)
          expect(gridPagination.updateComponent).toHaveBeenCalled()
