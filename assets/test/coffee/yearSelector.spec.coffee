define [
  'EdwareApplication',
  'edwareConstants' ,
  'edwareYearSelector',
  'react',
  'jquery'
], (App,
  Constants,
  edwareYearSelector,
  React,
  $
) ->
  describe 'Year Selector', () ->
    yearSelector = {}
    container = null
    beforeEach () ->

      EVENTS = Constants.EVENTS
      container = $('<div id="yearSelector"></div>')
      $(document.body).append(container)
      App.initialize {
        view: Constants.VIEWS.PERFORMANCE
        subject: Constants.SUBJECTS.MATH
        asmtType: Constants.ASMT_TYPE.SUMMATIVE
        result: Constants.RESULT_TYPES.OVERALL
        gradeCourse: Constants.GRADES_COURSES.SUBJECT1.THIRD
        reportName: Constants.REPORT_NAME.CPOP
        year: 2015
      }

    afterEach () ->
      container.remove()

    describe 'Year Selector Rendering', () ->

      it 'should render with label inside', () ->
        yearSelectorElement = React.createElement(edwareYearSelector.YearsDropdown, {labelInside: true})
        yearSelector = React.render(
          yearSelectorElement,
          container.get(0)
        )
        expect(yearSelector).toBeDefined()
        expect(yearSelector.isMounted()).toBe(true)
        expect($('#yearSelector label').length).toBe(0)
        expect(yearSelector.getTitle().props).toEqual({className: 'selected-label', children: ['Academic Year: ', '2014 - 2015']})

      it 'should render with label outside', () ->
        yearSelectorElement = React.createElement(edwareYearSelector.YearsDropdown, {labelInside: false})
        yearSelector = React.render(yearSelectorElement, container.get(0))

        expect(yearSelector).toBeDefined()
        expect(yearSelector.isMounted()).toBe(true)
        expect($('#yearSelector label').length).toBe(1)
        expect(yearSelector.getTitle().props).toEqual({className: 'selected-label', children: '2014 - 2015'})



    describe 'Year Selector Functions', () ->
      currentYear = null
      beforeEach () ->
        App.set('year', null, true) # clear out any states from before
        spyOn App, 'subscribe'
        yearSelectorElement = React.createElement(edwareYearSelector.YearsDropdown, {labelInside: false})
        yearSelector = React.render(yearSelectorElement, container.get(0))
        start = 2014
        today = new Date()
        currentYear = today.getFullYear()
        cutoff = new Date(today.getFullYear(), 7, 1, 0, 0, 0)
        years = []
        if today >= cutoff
          currentYear++

      it 'should subscribe to the change event on mount', () ->
        expect(App.subscribe).toHaveBeenCalled()

      it 'should have the current year as the initial state', () ->
        expect(yearSelector.state.year.value).toBe(currentYear)


      it 'should update App state on year change', () ->
        yearSelector.updateYearState(1)
        expect(App.getState().year).toBe(currentYear - 1)
        yearSelector.updateYearState(0)
        expect(App.getState().year).toBe(currentYear)

      it 'should return proper label and value for a given year when getYear called', () ->
        year = 2015
        yearObj = {
          label: '2014 - 2015',
          value: 2015
        }
        expect(yearSelector.getYear(2015)).toEqual(yearObj)


