define [
  'EdwareApplication',
  'edwareConstants' ,
  'edwareDataExports',
  'react',
  'jquery',
  'reactbootstrap',
  'edwareUtil'
  'edwareContextSecurity'
], (App, Constants, edwareDataExports, React, $, RB, edwareUtil, ContextSecurity) ->
  describe 'Data Exports Modal', () ->
    exportModal = {}
    container = null
    app = {}
    userInfo = {
      "_User__context": {
        "dog": {
          "RF_EXTRACT": {
            "resp_dist_id": [],
            "resp_school_id": [],
            "state_code": [
              "RI"
            ]
          },
          "PII": {
            "resp_dist_id": [],
            "resp_school_id": [],
            "state_code": [
              "RI"
            ]
          },
          "SF_EXTRACT": {
            "resp_dist_id": [],
            "resp_school_id": [],
            "state_code": [
              "RI"
            ]
          },
          "GENERAL": {
            "resp_dist_id": [],
            "resp_school_id": [],
            "state_code": [
              "RI"
            ]
          }
        }
      },
      "_User__info": {
        "name": {
          "lastName": "Snyder",
          "fullName": "Evan Snyder",
          "firstName": "Evan"
        },
        "roles": [
          "PII",
          "RF_EXTRACT",
          "GENERAL",
          "SF_EXTRACT"
        ],
        "stateCode": [
          "RI"
        ],
        "guid": "534ac3f1-df9d-48cd-8f25-b74f18dc1f9c",
        "tenant": [
          "dog"
        ],
        "uid": "esnyder"
      }
    }
    contextData = {
      "items": [
        {
          "type": "home",
          "name": "Home"
        }
      ],
      "permissions": {
        "pii": {
          "all": true
        },
        "sf_extract": false,
        "pf_extract": false,
        "cds_extract": false,
        "display_extract": false,
        "psrd_extract": false,
        "rf_extract": false,
        "pii_analytics": false
      }
    }
    config = {
      "subjectsList": [
        {
          "value": "subject2",
          "label": "ELA"
        },
        {
          "value": "subject1",
          "label": "Mathematics"
        }
      ],
      "exportTypesList": [
        {
          "value": Constants.EXTRACT_SELECTORS.SUMMATIVE_ASMT_RESULT,
          "label": "Summative Assessment Results"
        },
        {
          "value": "diag_ass_results",
          "label": "Diagnostic Assessment Results"
        },
        {
          "value": Constants.EXTRACT_SELECTORS.SUMMATIVE_RIF,
          "label": "Summative Released Item File"
        },
        {
          "value": Constants.EXTRACT_SELECTORS.PSYCHOMETRIC,
          "label": "Summative Psychometric Item File"
        }
      ],
      "gradesCoursesList": {
        "subject1": {
          "group1": [
            {
              "value": "03",
              "label": "3rd Grade"
            },
            {
              "value": "04",
              "label": "4th Grade"
            },
            {
              "value": "05",
              "label": "5th Grade"
            },
            {
              "value": "06",
              "label": "6th Grade"
            },
            {
              "value": "07",
              "label": "7th Grade"
            },
            {
              "value": "08",
              "label": "8th Grade"
            }
          ],
          "group3": [
            {
              "value": "Integrated Mathematics I",
              "label": "Int. Math I"
            },
            {
              "value": "Integrated Mathematics II",
              "label": "Int. Math II"
            },
            {
              "value": "Integrated Mathematics III",
              "label": "Int. Math III"
            }
          ],
          "group2": [
            {
              "value": "Algebra I",
              "label": "Algebra I"
            },
            {
              "value": "Algebra II",
              "label": "Algebra II"
            },
            {
              "value": "Geometry",
              "label": "Geometry"
            }
          ]
        },
        "subject2": [
          {
            "value": "03",
            "label": "3rd Grade"
          },
          {
            "value": "04",
            "label": "4th Grade"
          },
          {
            "value": "05",
            "label": "5th Grade"
          },
          {
            "value": "06",
            "label": "6th Grade"
          },
          {
            "value": "07",
            "label": "7th Grade"
          },
          {
            "value": "08",
            "label": "8th Grade"
          },
          {
            "value": "09",
            "label": "9th Grade"
          },
          {
            "value": "10",
            "label": "10th Grade"
          },
          {
            "value": "11",
            "label": "11th Grade"
          }
        ]
      }
    }

    init = {}
    beforeEach () ->

      container = $('<div class="DataExportContainer"></div>')
      $(document.body).append(container)
      App.initialize {
        view: Constants.VIEWS.PERFORMANCE
        subject: Constants.SUBJECTS.ELA
        asmtType: Constants.ASMT_TYPE.SUMMATIVE
        result: Constants.RESULT_TYPES.OVERALL
        gradeCourse: Constants.GRADES_COURSES.SUBJECT1.THIRD
        reportName: Constants.REPORT_NAME.CPOP
        year: 2015
        stateCode: 'RI'
      }
      ContextSecurity.instance(userInfo, contextData)

    afterEach () ->
      container.remove()

    describe 'DataExportsModal Rendering', () ->

      it 'renders using global application state', () ->
        exportModal = edwareDataExports.create '.DataExportContainer', config
        exportModal.show()
        expect(exportModal.component).toBeDefined()
        expect(exportModal.component.isMounted()).toBe(true)

      it 'has its own local state', () ->
        exportModal = edwareDataExports.create '.DataExportContainer', config

        expect(exportModal.localApp).toBeDefined()
        expect(exportModal.localApp).not.toEqual(App)


    describe 'DataExportsModal Functions', () ->

      beforeEach () ->
        exportModal = edwareDataExports.create '.DataExportContainer', config

        app = exportModal.localApp

        spyOn(app, 'subscribe').and.callThrough()
        spyOn(app, 'unsubscribe').and.callThrough()

        exportModal.show()

        init = {
          year: edwareUtil.getCurrentAcademicYear(),
          gradeCourse: [],
          readyForExport: false,
          exported: false,
          stateCode: App.getState().stateCode
        }

      it 'subscribes to the change event on mount', () ->
        expect(app.subscribe).toHaveBeenCalled()

      it 'should have initial state defined', () ->
        expect(app.getState()).toEqual(init)

      it 'is only ready for export when exportType is "summ_psy_item_file" or when both subject and grade are selected', () ->
        app.setState({year: 2015, subject: 'subject1', exportType: Constants.EXTRACT_SELECTORS.PSYCHOMETRIC})
        exportModal.updateExportStatus()
        expect(app.getState().readyForExport).toBe(true)

        app.setState({exportType: Constants.EXTRACT_SELECTORS.SUMMATIVE_ASMT_RESULT})
        exportModal.updateExportStatus()
        expect(app.getState().readyForExport).toBe(false)

        app.setState({subject: 'subject1', gradeCourse: ['03', '04']})
        exportModal.updateExportStatus()
        expect(app.getState().readyForExport).toBe(true)

      it 'is not ready to export yet when you click onExport', () ->
        exportModal.onExport()
        expect(app.getState().readyForExport).toBe(false)

      it 'has a clean state when startOver is called', () ->
        exportModal.startOver()
        expect(app.getState()).toEqual(init)

      it 'removes component safely and unsubscribe from events', () ->
        React.unmountComponentAtNode(container.get(0))
        expect(app.unsubscribe).toHaveBeenCalled()

