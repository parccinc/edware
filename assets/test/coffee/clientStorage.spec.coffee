define [
  'edwareClientStorage'
  'jquery'
  'edwareUtil'
], (EdwareClientStorage,
    $,
    util) ->

  clientStorage = undefined
  key = 'dummy'

  data =
    foo: 'bar'

  updateData =
    foo: 'baz'

  describe 'ClientStorage', () ->

    beforeEach () ->
      clientStorage = new EdwareClientStorage(key)

    afterAll () ->
      clientStorage?.clearAll()

    describe 'creation', () ->

      it 'should be successfully created', () ->
        expect(clientStorage).toBeDefined()

    describe 'functionality', () ->

      it 'should successfully save data to localStorage', () ->
        spyOn(localStorage, 'setItem').and.callThrough()
        clientStorage.save(data)
        expect(localStorage.setItem).toHaveBeenCalled()

      it 'should successfully update data on localStorage', () ->
        clientStorage.update(updateData)
        expect(JSON.parse(localStorage[key])).toEqual(updateData)

      it 'should successfully load data from localStorage', () ->
        spyOn(localStorage, 'getItem').and.callThrough()
        loadedData = clientStorage.load(key)
        expect(loadedData).toEqual(updateData)
        expect(localStorage.getItem).toHaveBeenCalled()

      it 'should successfully delete data from localStorage', () ->
        spyOn(localStorage, 'removeItem').and.callThrough()
        clientStorage.clear()
        expect(localStorage[key]).not.toBeDefined()
        expect(localStorage.removeItem).toHaveBeenCalled()

      it 'should successfully clear all storages', () ->
        spyOn(sessionStorage, 'clear').and.callThrough()
        spyOn(localStorage, 'clear').and.callThrough()
        clientStorage.clearAll()
        expect(sessionStorage[key]).not.toBeDefined()
        expect(localStorage[key]).not.toBeDefined()
        expect(sessionStorage.clear).toHaveBeenCalled()
        expect(localStorage.clear).toHaveBeenCalled()
