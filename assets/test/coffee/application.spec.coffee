define ['EdwareApplication'], (App) ->
  testerObject = () ->
    testObj = {
      testKey: 'key',
      obj: {key: 'val'},
      callback: () ->
        #linter barks on empty functions
        console.log 'callback'
    }
    spyOn testObj, 'callback'
    testObj

  describe 'EdwareApplication PubSub Module', () ->
    app = null

    beforeEach () ->
      app = App.createApp()
      app.initialize()
    describe 'setState', () ->
      it 'triggers persistState', () ->
        spyOn app, 'persistState'
        app.setState({option: 'value'})
        expect(app.persistState).toHaveBeenCalled()

    describe 'getState', () ->
      it 'contains values set', () ->
        stateObject = {
          # subject: 'subject1' #TODO: we need to override params properly from within each component
          view: 'view1'
        }
        expect(app.setState(stateObject))
          .toEqual(jasmine.objectContaining(stateObject))

    describe 'removeEmpty', () ->
      it 'deletes properties that are null or undefined', () ->
        actual = app.removeEmpty {key: null, otherKey: undefined}
        expect not actual.hasOwnProperty('key') and not actual.hasOwnProperty('otherKey')

      it 'doesn\'t delete properties with falsy values', () ->
        actual = app.removeEmpty({key: false})
        expect(actual.key).toBe false

    describe 'set', () ->
      tester = null

      beforeEach () ->
        tester = testerObject()

      it 'triggers global callbacks', () ->
        app.subscribe tester.callback
        app.set tester.obj
        expect(tester.callback).toHaveBeenCalled()

      it 'defaults to trigger callbacks', () ->
        app.subscribe tester.testKey, tester.callback
        app.set tester.obj
        expect(tester.callback).toHaveBeenCalled()

      it 'doesnt trigger global callbacks when empty object is passed in', () ->
        app.subscribe tester.callback
        app.setState({})
        expect(tester.callback).not.toHaveBeenCalled()

      it 'doesn\'t trigger global callbacks when silant is true', () ->
        app.subscribe tester.callback
        app.set tester.obj, true
        expect(tester.callback).not.toHaveBeenCalled()

      it 'doesn\'t trigger callbacks when silant is true', () ->
        app.subscribe tester.testKey, tester.callback
        app.set tester.obj, true
        expect(tester.callback).not.toHaveBeenCalled()


      it 'defaults not to persist non initialized events', () ->
        spyOn app, 'persistState'
        app.set tester.obj
        expect(app.persistState).not.toHaveBeenCalled()

      it 'persists state when remember is set to true', () ->
        spyOn app, 'persistState'
        app.set tester.obj, false, true
        expect(app.persistState).toHaveBeenCalled()

      it 'doesn\'t persist state when remember is set to false', () ->
        spyOn app, 'persistState'
        app.set tester.obj, false, false
        expect(app.persistState).not.toHaveBeenCalled()


    describe 'subscribe', () ->

      it 'triggers the callback', () ->
        tester = testerObject()
        app.subscribe tester.testKey, tester.callback
        app.setState tester.obj
        expect(tester.callback).toHaveBeenCalled()

    describe 'unsubscribe', () ->
      tester = null

      beforeEach () ->
        tester = testerObject()

      it 'doesn\'t trigger callback anymore', () ->
        app.subscribe tester.testKey, tester.callback
        app.unsubscribe tester.testKey, tester.callback
        app.setState tester.obj
        expect(tester.callback).not.toHaveBeenCalled()

      it 'doesn\'t trigger global callbacks anymore', () ->
        app.subscribe tester.callback
        app.unsubscribe tester.callback
        app.setState tester.obj
        expect(tester.callback).not.toHaveBeenCalled()




