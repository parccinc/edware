define [
  'EdwareApplication',
  'edwareConstants' ,
  'edwareDateRangePicker',
  'react',
  'jquery',
  'moment'
], (App,
    Constants,
    edwareDateRangePicker,
    React,
    $,
    moment
) ->

  container = $('<div id="dateRangePicker"></div>')
  dateRangePicker = undefined
  data = {
    "analytics": {
      "url": "#"
    },
    "columns": {},
    "user_info": {
      "_User__context": {
        "dog": {
          "PII": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          },
          "RF_EXTRACT": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          },
          "SUPER_USER": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          },
          "SF_EXTRACT": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          },
          "PII_ANALYTICS": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          },
          "GENERAL": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          },
          "PF_EXTRACT": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          }
        },
        "cat": {
          "PII": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          },
          "RF_EXTRACT": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          },
          "SUPER_USER": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          },
          "SF_EXTRACT": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          },
          "PII_ANALYTICS": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          },
          "GENERAL": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          },
          "PF_EXTRACT": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          }
        },
        "fish": {
          "PII": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          },
          "RF_EXTRACT": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          },
          "SUPER_USER": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          },
          "SF_EXTRACT": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          },
          "PII_ANALYTICS": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          },
          "GENERAL": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          },
          "PF_EXTRACT": {
            "resp_school_id": [],
            "state_code": [],
            "resp_dist_id": []
          }
        }
      },
      "_User__info": {
        "name": {
          "lastName": "Man",
          "firstName": "Guy",
          "fullName": "Guy Man"
        },
        "guid": "ed81c400-8874-4a96-b6fd-3573b3c3000a",
        "roles": [
          "RF_EXTRACT",
          "SUPER_USER",
          "GENERAL",
          "PII_ANALYTICS",
          "PII",
          "SF_EXTRACT",
          "PF_EXTRACT"
        ],
        "uid": "gman",
        "stateCode": [
          "NY",
          "RI",
          "VT"
        ],
        "tenant": [
          "dog",
          "cat",
          "fish"
        ]
      }
    },
    "assessments": {
      "DIAGNOSTIC": {
        "subject2": [
          {
            "student_guid": "w7TmiOs6CWJsXS2l1crivMsf42ZlQ5WAr6Xri2rd",
            "asmt_grade": "08",
            "student_display_name": "Cabello, Becki L.",
            "asmt_date": "03/08/2015",
            "view": "Decoding",
            "staff_id": "0a4d255f40644147aaffbcb10fef0d908e8bd9"
          },
          {
            "student_guid": "w7TmiOs6CWJsXS2l1crivMsf42ZlQ5WAr6Xri2rd",
            "asmt_grade": "08",
            "student_display_name": "Cabello, Becki L.",
            "asmt_date": "03/08/2015",
            "view": "Reading Comprehension",
            "staff_id": "0a4d255f40644147aaffbcb10fef0d908e8bd9"
          },
          {
            "student_guid": "w7TmiOs6CWJsXS2l1crivMsf42ZlQ5WAr6Xri2rd",
            "asmt_grade": "08",
            "student_display_name": "Cabello, Becki L.",
            "asmt_date": "03/08/2015",
            "view": "Reading Fluency",
            "staff_id": "0a4d255f40644147aaffbcb10fef0d908e8bd9"
          },
          {
            "student_guid": "w7TmiOs6CWJsXS2l1crivMsf42ZlQ5WAr6Xri2rd",
            "asmt_grade": "08",
            "student_display_name": "Cabello, Becki L.",
            "asmt_date": "03/08/2015",
            "view": "Vocabulary",
            "staff_id": "0a4d255f40644147aaffbcb10fef0d908e8bd9"
          },
          {
            "student_guid": "w7TmiOs6CWJsXS2l1crivMsf42ZlQ5WAr6Xri2rd",
            "asmt_grade": "08",
            "student_display_name": "Cabello, Becki L.",
            "asmt_date": "03/08/2015",
            "view": "Writing",
            "staff_id": "0a4d255f40644147aaffbcb10fef0d908e8bd9"
          },
          {
            "student_guid": "KL3R8L1EaO8MgymDcMI59fe7DnjLk0ZuDZiwO2J2",
            "asmt_grade": "04",
            "student_display_name": "Gatti, Sandy V.",
            "asmt_date": "06/15/2015",
            "view": "Decoding",
            "staff_id": "43e0d272a0c245bfb568ee9e944a12c9436f17"
          },
          {
            "student_guid": "KL3R8L1EaO8MgymDcMI59fe7DnjLk0ZuDZiwO2J2",
            "asmt_grade": "04",
            "student_display_name": "Gatti, Sandy V.",
            "asmt_date": "06/15/2015",
            "view": "Reading Comprehension",
            "staff_id": "43e0d272a0c245bfb568ee9e944a12c9436f17"
          },
          {
            "student_guid": "KL3R8L1EaO8MgymDcMI59fe7DnjLk0ZuDZiwO2J2",
            "asmt_grade": "04",
            "student_display_name": "Gatti, Sandy V.",
            "asmt_date": "06/15/2015",
            "view": "Reading Fluency",
            "staff_id": "43e0d272a0c245bfb568ee9e944a12c9436f17"
          },
          {
            "student_guid": "KL3R8L1EaO8MgymDcMI59fe7DnjLk0ZuDZiwO2J2",
            "asmt_grade": "04",
            "student_display_name": "Gatti, Sandy V.",
            "asmt_date": "06/15/2015",
            "view": "Vocabulary",
            "staff_id": "43e0d272a0c245bfb568ee9e944a12c9436f17"
          },
          {
            "student_guid": "KL3R8L1EaO8MgymDcMI59fe7DnjLk0ZuDZiwO2J2",
            "asmt_grade": "04",
            "student_display_name": "Gatti, Sandy V.",
            "asmt_date": "06/15/2015",
            "view": "Writing",
            "staff_id": "43e0d272a0c245bfb568ee9e944a12c9436f17"
          },
          {
            "student_guid": "oBPTLwB89Yetjpa88VqxOvlIENOqe4XKGKnOrJfM",
            "asmt_grade": "03",
            "student_display_name": "Matsuura, Orval B.",
            "asmt_date": "05/05/2015",
            "view": "Decoding",
            "staff_id": "0a4061fcdd3f4e6ea8db682199fee2757e8275"
          },
          {
            "student_guid": "oBPTLwB89Yetjpa88VqxOvlIENOqe4XKGKnOrJfM",
            "asmt_grade": "03",
            "student_display_name": "Matsuura, Orval B.",
            "asmt_date": "05/05/2015",
            "view": "Reading Comprehension",
            "staff_id": "0a4061fcdd3f4e6ea8db682199fee2757e8275"
          },
          {
            "student_guid": "oBPTLwB89Yetjpa88VqxOvlIENOqe4XKGKnOrJfM",
            "asmt_grade": "03",
            "student_display_name": "Matsuura, Orval B.",
            "asmt_date": "05/05/2015",
            "view": "Reading Fluency",
            "staff_id": "0a4061fcdd3f4e6ea8db682199fee2757e8275"
          },
          {
            "student_guid": "oBPTLwB89Yetjpa88VqxOvlIENOqe4XKGKnOrJfM",
            "asmt_grade": "03",
            "student_display_name": "Matsuura, Orval B.",
            "asmt_date": "05/05/2015",
            "view": "Vocabulary",
            "staff_id": "0a4061fcdd3f4e6ea8db682199fee2757e8275"
          },
          {
            "student_guid": "oBPTLwB89Yetjpa88VqxOvlIENOqe4XKGKnOrJfM",
            "asmt_grade": "03",
            "student_display_name": "Matsuura, Orval B.",
            "asmt_date": "05/05/2015",
            "view": "Writing",
            "staff_id": "0a4061fcdd3f4e6ea8db682199fee2757e8275"
          },
          {
            "student_guid": "6boPVVoO1Vuyg8VJ5AqV0Zha4rVGrZt3uPrM7xgw",
            "asmt_grade": "06",
            "student_display_name": "Parr, Betty E.",
            "asmt_date": "11/18/2014",
            "view": "Decoding",
            "staff_id": "43e0d272a0c245bfb568ee9e944a12c9436f17"
          },
          {
            "student_guid": "6boPVVoO1Vuyg8VJ5AqV0Zha4rVGrZt3uPrM7xgw",
            "asmt_grade": "06",
            "student_display_name": "Parr, Betty E.",
            "asmt_date": "11/18/2014",
            "view": "Reading Comprehension",
            "staff_id": "43e0d272a0c245bfb568ee9e944a12c9436f17"
          },
          {
            "student_guid": "6boPVVoO1Vuyg8VJ5AqV0Zha4rVGrZt3uPrM7xgw",
            "asmt_grade": "06",
            "student_display_name": "Parr, Betty E.",
            "asmt_date": "11/18/2014",
            "view": "Reading Fluency",
            "staff_id": "43e0d272a0c245bfb568ee9e944a12c9436f17"
          },
          {
            "student_guid": "6boPVVoO1Vuyg8VJ5AqV0Zha4rVGrZt3uPrM7xgw",
            "asmt_grade": "06",
            "student_display_name": "Parr, Betty E.",
            "asmt_date": "11/18/2014",
            "view": "Vocabulary",
            "staff_id": "43e0d272a0c245bfb568ee9e944a12c9436f17"
          },
          {
            "student_guid": "6boPVVoO1Vuyg8VJ5AqV0Zha4rVGrZt3uPrM7xgw",
            "asmt_grade": "06",
            "student_display_name": "Parr, Betty E.",
            "asmt_date": "11/18/2014",
            "view": "Writing",
            "staff_id": "43e0d272a0c245bfb568ee9e944a12c9436f17"
          },
          {
            "student_guid": "e3OGNkIbM4h2skSI4nPuESDwrF1EMmjIfrprGMHz",
            "asmt_grade": "04",
            "student_display_name": "Pedone, Verena S.",
            "asmt_date": "04/01/2015",
            "view": "Decoding",
            "staff_id": "ded3479fc4504921ac1cf878b8c5d015efffc1"
          },
          {
            "student_guid": "e3OGNkIbM4h2skSI4nPuESDwrF1EMmjIfrprGMHz",
            "asmt_grade": "04",
            "student_display_name": "Pedone, Verena S.",
            "asmt_date": "04/01/2015",
            "view": "Reading Comprehension",
            "staff_id": "ded3479fc4504921ac1cf878b8c5d015efffc1"
          },
          {
            "student_guid": "e3OGNkIbM4h2skSI4nPuESDwrF1EMmjIfrprGMHz",
            "asmt_grade": "04",
            "student_display_name": "Pedone, Verena S.",
            "asmt_date": "04/01/2015",
            "view": "Reading Fluency",
            "staff_id": "ded3479fc4504921ac1cf878b8c5d015efffc1"
          },
          {
            "student_guid": "e3OGNkIbM4h2skSI4nPuESDwrF1EMmjIfrprGMHz",
            "asmt_grade": "04",
            "student_display_name": "Pedone, Verena S.",
            "asmt_date": "04/01/2015",
            "view": "Vocabulary",
            "staff_id": "ded3479fc4504921ac1cf878b8c5d015efffc1"
          },
          {
            "student_guid": "e3OGNkIbM4h2skSI4nPuESDwrF1EMmjIfrprGMHz",
            "asmt_grade": "04",
            "student_display_name": "Pedone, Verena S.",
            "asmt_date": "04/01/2015",
            "view": "Writing",
            "staff_id": "ded3479fc4504921ac1cf878b8c5d015efffc1"
          }
        ]
      }
    },
    "subjects": [
      {
        "label": "ELA",
        "value": "subject2"
      },
      {
        "label": "Mathematics",
        "value": "subject1"
      }
    ],
    "context": {
      "items": [
        {
          "name": "Home",
          "type": "home"
        },
        {
          "name": "Rhode Island",
          "id": "RI",
          "type": "state"
        },
        {
          "name": "Providence",
          "id": "R0003",
          "type": "district"
        },
        {
          "name": "Hope High School",
          "id": "RP003",
          "type": "school"
        }
      ],
      "permissions": {
        "pii": {
          "guid": [],
          "all": true
        },
        "rf_extract": true,
        "display_extract": true,
        "psrd_extract": false,
        "sf_extract": true,
        "cds_extract": false,
        "pf_extract": true,
        "pii_analytics": true
      }
    },
    "summary": {
      "SUMMATIVE": {
        "subject1": [
          {
            "type": "parcc"
          },
          {
            "type": "state"
          },
          {
            "type": "district"
          }
        ],
        "subject2": [
          {
            "type": "parcc"
          },
          {
            "type": "state"
          },
          {
            "type": "district"
          }
        ]
      }
    }
  }
  dates = []
  data.assessments.DIAGNOSTIC.subject2.map (item) ->
    itemDate = (new Date(item.asmt_date)).getTime() / 1000
    dates.push itemDate
    item.filterDate = itemDate
  dates.sort()

  describe 'DateRangePicker', () ->

    beforeEach () ->

      App.initialize({
        "asmtType": "DIAGNOSTIC",
        "subject": "subject2",
        "result": "overall",
        "schoolGuid": "RP003",
        "districtGuid": "R0003",
        "year": "2015",
        "view": "overview",
        "stateCode": "RI",
        "sl": "1434142582",
        "summaries": {
          "district": true
        }
      })

      App.setState({
        "asmtType": "DIAGNOSTIC",
        "subject": "subject2",
        "result": "overall",
        "schoolGuid": "RP003",
        "districtGuid": "R0003",
        "year": "2015",
        "view": "overview",
        "stateCode": "RI",
        "sl": "1434142582",
        "summaries": {
          "district": true
        }
      })

      $(document.body).append(container)
      App.setValue({availableDateRange: {
        min: dates[0]
        max: dates[dates.length - 1]
      }})
      dateRangePickerElement = React.createElement(edwareDateRangePicker.EdwareDateRangePicker, {
        data: data
      })

      dateRangePicker = React.render(
        dateRangePickerElement,
        container.get(0)
      )

    afterEach () ->
      React.unmountComponentAtNode container.get(0)
      container.remove()

    it 'it should successfully create the DateRangePicker component', () ->
      expect(dateRangePicker).toBeDefined()

    it 'it should successfully create the DateRangePicker plugin', () ->
      expect(dateRangePicker.plugin).toBeDefined()

    it 'it should successfully calculate the start date', () ->
      range = dateRangePicker.getDefaultRange()
      expect(range.startDate).toBe('11/18/2014')

    it 'it should successfully calculate the end date', () ->
      range = dateRangePicker.getDefaultRange()
      expect(range.endDate).toBe('06/15/2015')

    it 'it should successfully update the App state on change', () ->
      dateRangePicker.onChange(moment('2015-06-10'), moment('2015-06-15'))
      expect(App.getState().asmt_date.min).toBe(moment('06/10/2015').unix())
      expect(App.getState().asmt_date.max).toBe(moment('06/15/2015').unix())





