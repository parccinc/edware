define ['selectorUtil'], (SelectorUtil) ->

  describe 'selector util', ->

    describe 'sanity check', ->

      it 'is defined', ->
        expect(SelectorUtil).toBeDefined()

      it 'has availableForReport method', () ->
        expect(SelectorUtil.availableForReport).toBeDefined()

      it 'has buildOptionsForReport method', () ->
        expect(SelectorUtil.buildOptionsForReport).toBeDefined()

      it 'has flattenOptions method', () ->
        expect(SelectorUtil.flattenOptions).toBeDefined()

      it 'has getForCurrentState method', () ->
        expect(SelectorUtil.getForCurrentState).toBeDefined()

      it 'has isCurrentState method', () ->
        expect(SelectorUtil.isCurrentState).toBeDefined()

      it 'has currentReport method', () ->
        expect(SelectorUtil.currentReport).toBeDefined()

      it 'has currentReportState method', () ->
        expect(SelectorUtil.currentReportState).toBeDefined()

    describe 'availableForReport', () ->

      it 'for block parsed as an object of arrays', () ->
        expect(SelectorUtil.availableForReport({
          'for': {'stateName': ['21']}
        }, {stateName: '21'})).toBe(true)

        expect(SelectorUtil.availableForReport({
          'for': {'state1': ['23']}
        }, {'state1': 'noneExistentValue'})).toBe(false)

      it '"or"\'s multiple items in "for" option', () ->
        expect(SelectorUtil.availableForReport({
          'for': {'state1': ['23', '24']}
        }, {'state1': '24'})).toBe(true)

        expect(SelectorUtil.availableForReport({
          'for': {'state1': ['23', '24']}
        }, {'state1': 'noneExistentValue'})).toBe(false)

      it 'and\'s multiple options in for block', () ->
        testItem = {'for': {'state1': ['opt1', 'opt2'], 'state2': ['opt3', 'opt4']}}
        expect(SelectorUtil.availableForReport(testItem, {'state2': 'opt5', 'state1': 'opt1'})).toBe(false)
        expect(SelectorUtil.availableForReport(testItem, {'state1': 'opt2', 'state2': 'opt4'})).toBe(true)

      it '"and\'s" negated options', () ->
        testItem = {'for': {'!state1': ['opt1'], 'state1': ['opt2'], 'state2': ['opt3', 'opt4']}}

        expect(SelectorUtil.availableForReport(testItem, {'state1': 'opt1', 'state2': 'opt3'})).toBe(false)
        ## redundent scenerio if the state is not opt2 then it will always be not opt1
        # expect(SelectorUtil.availableForReport(testItem, {'state1': 'opt5'})).toBe(false)
        expect(SelectorUtil.availableForReport(testItem, {'state1': 'opt2', 'state2': 'opt3'})).toBe(true)

    #todo: need to add tests for buildOptionsForReport
    # describe 'buildOptionsForReport', () ->

    describe 'flattenOptions', () ->

      it 'returns empty Array when no Options are passed in', () ->
        expect(SelectorUtil.flattenOptions()).toEqual([])

      it 'changes nothing when passed in flat array', () ->
        obj = [{"test", "b", "key2": "val2"}]
        expect(SelectorUtil.flattenOptions(obj).length).toBe(1)
        expect(SelectorUtil.flattenOptions(obj).toString()).toEqual(obj[0].toString())

      it 'flattens deeply nested arrays', () ->
        obj = [{
          "key1": "val1"
          "options": [{
            "key2": "val2",
            "options": [
              {"key2": "val3"}
            ]
          }]
        }]
        expect(SelectorUtil.flattenOptions(obj).length).toBe(3)




