define [
  'edwareConstants',
  'edwareUtil',
  'EdwareApplication',
  'edwareContextSecurity'
],
(Constants, edwareUtil, App, ContextSecurity) ->

  #ContextSecurity = edwareContextSecurity.ContextSecurity
  #contextSecurity = undefined

  vtRow = {guid: "VT"}

  njRow = {guid: "NJ"}

  riRow = {guid: "RI"}


  user = {
    "_User__info": {
      "stateCode": [
        "NY",
        "VT",
        "RI"
      ],
      "roles": [
        "PII",
        "GENERAL",
        "PF_EXTRACT"
      ],
      "name": {
        "firstName": "Robin",
        "lastName": "Hood",
        "fullName": "Robin Hood"
      },
      "uid": "rhood",
      "tenant": [
        "cat"
      ],
      "guid": "534ac3f1-df9d-48cd-8f25-b74f18dc1f9c"
    },
    "_User__context": {
      "cat": {
        "PII": {
          "resp_dist_id": [],
          "resp_school_id": [],
          "state_code": [
            "NY"
          ]
        },
        "GENERAL": {
          "resp_dist_id": [],
          "resp_school_id": [],
          "state_code": [
            "NY"
          ]
        },
        "PF_EXTRACT": {
          "resp_dist_id": [],
          "resp_school_id": [],
          "state_code": [
            "NY"
          ]
        }
      }
    }
  }

  context = {
    "permissions": {
      "pii_analytics": false,
      "sf_extract": false,
      "display_extract": true,
      "pf_extract": true,
      "cds_extract": false,
      "psrd_extract": false,
      "display_extract": true,
      "pii": {
        "all": false,
        "guid": ["NY", "VT", "RI"]
      },
      "rf_extract": false
    },
    "items": [
      {
        "type": "home",
        "name": "Home"
      },
      {
        "type": "state",
        "id": "NY",
        "name": "New York"
      }
    ]
  }

  describe 'Context Security', () ->
    beforeEach () ->

      App.initialize({
        gradeCourse: Constants.GRADES_COURSES.SUBJECT1.THIRD
        asmtType: 'SUMMATIVE'
        subject: 'subject2'
        view: 'performance'
        year: 2015
        result: 'overall'
      })
      ContextSecurity.instance(user, context)
      # contextSecurity = new ContextSecurity(user, context)

    it 'should create the contextSecurity object', () ->
      expect(ContextSecurity.get()).toBeDefined()

    it 'should correctly check if this user can do any of the extracts', () ->
      expect(ContextSecurity.get().canExportAny()).toBe(true)

    it 'should correctly check if the user can do a specific extract', () ->
      expect(ContextSecurity.get().canExport('rf_extract')).toBe(false)
      expect(ContextSecurity.get().canExport('pf_extract')).toBe(true)

    it 'should allow the user to drill down into states that the user has permission to', () ->
      expect(ContextSecurity.get().canDrillDown(vtRow)).toBe(true)
      expect(ContextSecurity.get().canDrillDown(riRow)).toBe(true)

    it 'should not allow the user to drill down into states that the user has no permission to', () ->
      expect(ContextSecurity.get().canDrillDown(njRow)).toBe(false)

    it 'should filter down ExportableTypesList to exports with permissions', () ->
      expect(ContextSecurity.get().exportableTypesList([
        {permission: "rf_extract"},
        {permission: "sf_extract"},
        {permission: "pf_extract"}
      ]).length).toBe(1)

    it 'should not allow access to PII when pii permission flag is false', () ->
      expect(ContextSecurity.get().canAccessPII()).toBe(false)

    it 'should allow access to PII when pii permission flag is true', () ->
      testContext = $.extend true, {}, context
      testContext.permissions.pii.all = true

      ContextSecurity.instance(user, testContext)
      expect(ContextSecurity.get().canAccessPII()).toBe(true)
