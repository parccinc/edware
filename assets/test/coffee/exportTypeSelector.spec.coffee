define [
  'EdwareApplication',
  'edwareConstants' ,
  'edwareExportTypeSelector',
  'react',
  'jquery',
  'reactbootstrap'
], (App,
  Constants,
  edwareExportTypeSelector,
  React,
  $,
  RB
) ->
  describe 'ExportTypeSelector', () ->
    exportTypeSelector = {}
    container = null
    app = {}

    mockEvent = {
      target: {
        data: () ->
          {
            index: 2
          }
      }
    }

    exportTypesList = [
      {
        "label": "Summative Assessment Results",
        "value": "summ_ass_results"
      },
      {
        "label": "Diagnostic Assessment Results",
        "value": "diag_ass_results"
      },
      {
        "label": "Summative Released Item File",
        "value": "summ_rel_item_file"
      },
      {
        "label": "Summative Psychometric Item File",
        "value": "summ_psy_item_file"
      }
    ]

    beforeEach () ->

      EVENTS = Constants.EVENTS
      container = $('<div id="exportTypeSelector"></div>')
      $(document.body).append(container)
      App.initialize {
        view: Constants.VIEWS.PERFORMANCE
        subject: Constants.SUBJECTS.ELA
        asmtType: Constants.ASMT_TYPE.SUMMATIVE
        result: Constants.RESULT_TYPES.OVERALL
        gradeCourse: Constants.GRADES_COURSES.SUBJECT1.THIRD
        reportName: Constants.REPORT_NAME.CPOP
        year: 2015
      }

    afterEach () ->
      container.remove()

    describe 'ExportTypeSelector Rendering', () ->

      it 'should render with global application state', () ->
        exportTypeSelectorElement = React.createElement(edwareExportTypeSelector.ExportTypeDropdown, {
          data: exportTypesList
        })
        exportTypeSelector = React.render(
          exportTypeSelectorElement,
          container.get(0)
        )

        expect(exportTypeSelector).toBeDefined()
        expect(exportTypeSelector.isMounted()).toBe(true)

      it 'should render with local application state', () ->
        app = App.createApp()
        exportTypeSelectorElement = React.createElement(edwareExportTypeSelector.ExportTypeDropdown, {
          data: exportTypesList,
          app: app
        })
        exportTypeSelector = React.render(
          exportTypeSelectorElement,
          container.get(0)
        )

        expect(exportTypeSelector).toBeDefined()
        expect(exportTypeSelector.isMounted()).toBe(true)


    describe 'ExportTypeSelector Functions', () ->

      beforeEach () ->

        app = App.createApp()


        spyOn(app, 'subscribe').and.callThrough()
        spyOn(app, 'unsubscribe').and.callThrough()

        app.setState({
          year: 2015,
          subject: null,
          gradeCourse: [],
          exportType: null
          readyForExport: false,
          exported: false,
          stateCode: App.getState().stateCode
        })

        app.forgetState()

        exportTypeSelectorElement = React.createElement(edwareExportTypeSelector.ExportTypeDropdown, {
          data: exportTypesList,
          app: app
        })
        exportTypeSelector = React.render(exportTypeSelectorElement, container.get(0))

      it 'should subscribe to the change event on mount', () ->
        expect(app.subscribe).toHaveBeenCalled()

      it 'should have no state defined', () ->
        expect(exportTypeSelector.state.exportType).not.toBeDefined()

      it 'should update app state on select', () ->
        exportTypeSelector.onExportTypeSelect(0, mockEvent)
        expect(app.getState().exportType).toBe("summ_ass_results")

      it 'should remove component safely and unsubscribe from events', () ->
        React.unmountComponentAtNode(container.get(0))
        expect(app.unsubscribe).toHaveBeenCalled()
