define [
  'EdwareApplication',
  'edwareConstants',
  'edwareGradeCourseSelector',
  'react',
  'jquery'
], (App,
  Constants,
  edwareGradeCourseSelector,
  React,
  $
) ->
  describe 'GradeCourseSelector', () ->
    gradeCourseSelector = {}
    container = null

    #Same object from common.json
    gradesCoursesList = {
      "subject1": {
        "group1": [
          {
            "label": "3rd Grade",
            "value": "Grade 3"
          },
          {
            "label": "4th Grade",
            "value": "Grade 4"
          },
          {
            "label": "5th Grade",
            "value": "Grade 5"
          },
          {
            "label": "6th Grade",
            "value": "Grade 6"
          },
          {
            "label": "7th Grade",
            "value": "Grade 7"
          },
          {
            "label": "8th Grade",
            "value": "Grade 8"
          }
        ],
        "group2": [
          {
            "label": "Algebra I",
            "value": "Algebra I"
          },
          {
            "label": "Algebra II",
            "value": "Algebra II"
          },
          {
            "label": "Geometry",
            "value": "Geometry"
          }
        ],
        "group3": [
          {
            "label": "Int. Math I",
            "value": "Integrated Mathematics I"
          },
          {
            "label": "Int. Math II",
            "value": "Integrated Mathematics II"
          },
          {
            "label": "Int. Math III",
            "value": "Integrated Mathematics III"
          }
        ]
      },
      "subject2": [
        {
          "label": "3rd Grade",
          "value": "Grade 3"
        },
        {
          "label": "4th Grade",
          "value": "Grade 4"
        },
        {
          "label": "5th Grade",
          "value": "Grade 5"
        },
        {
          "label": "6th Grade",
          "value": "Grade 6"
        },
        {
          "label": "7th Grade",
          "value": "Grade 7"
        },
        {
          "label": "8th Grade",
          "value": "Grade 8"
        },
        {
          "label": "9th Grade",
          "value": "Grade 9"
        },
        {
          "label": "10th Grade",
          "value": "Grade 10"
        },
        {
          "label": "11th Grade",
          "value": "Grade 11"
        }
      ]
    }

    beforeEach () ->

      EVENTS = Constants.EVENTS
      container = $('<div id="gradeCourseSelector"></div>')
      $(document.body).append(container)
      App.initialize {
        view: Constants.VIEWS.PERFORMANCE
        subject: Constants.SUBJECTS.ELA
        asmtType: Constants.ASMT_TYPE.SUMMATIVE
        result: Constants.RESULT_TYPES.OVERALL
        gradeCourse: Constants.GRADES_COURSES.SUBJECT1.THIRD
        reportName: Constants.REPORT_NAME.CPOP
        year: 2015
      }

    afterEach () ->
      container.remove()

    describe 'GradeCourseSelector Rendering', () ->

      it 'should render with global application state', () ->
        gradeCourseSelectorElement = React.createElement(edwareGradeCourseSelector.GradesCoursesDropdown, {
          data: gradesCoursesList
        })
        gradeCourseSelector = React.render(
          gradeCourseSelectorElement,
          container.get(0)
        )

        expect(gradeCourseSelector).toBeDefined()
        expect(gradeCourseSelector.isMounted()).toBe(true)

      it 'should render with local application state', () ->
        app = App.createApp()
        gradeCourseSelectorElement = React.createElement(edwareGradeCourseSelector.GradesCoursesDropdown, {
          data: gradesCoursesList,
          app: app
        })
        gradeCourseSelector = React.render(
          gradeCourseSelectorElement,
          container.get(0)
        )

        expect(gradeCourseSelector).toBeDefined()
        expect(gradeCourseSelector.isMounted()).toBe(true)


    describe 'GradeCourseSelector Functions', () ->

      beforeEach () ->
        spyOn App, 'subscribe'
        spyOn App, 'unsubscribe'
        App.setState({subject: Constants.SUBJECTS.ELA})
        gradeCourseSelectorElement = React.createElement(edwareGradeCourseSelector.GradesCoursesDropdown, {
          data: gradesCoursesList
        })
        gradeCourseSelector = React.render(gradeCourseSelectorElement, container.get(0))


      it 'should subscribe to the change event on mount', () ->
        expect(App.subscribe).toHaveBeenCalled()

      it 'should have the 3rd grade as the initial state', () ->
        expect(gradeCourseSelector.state.gradeCourse.value).toBe('Grade 3')

      it 'should update App state on gradeCourse change', () ->
        gradeCourseSelector.onGradesCoursesSelect(1)
        expect(App.getState().gradeCourse).toBe('Grade 4')
        App.setState({subject: Constants.SUBJECTS.MATH})
        gradeCourseSelector.onGradesCoursesSelect(6)
        expect(App.getState().gradeCourse).toBe('Algebra I')

      it 'should work for ELA grades', () ->
        App.setState({subject: Constants.SUBJECTS.ELA})
        expect(gradeCourseSelector.getGradesAndCourses('Grade 9')).toEqual({label: '9th Grade', value: 'Grade 9'})

      it 'should work for MATH grades', () ->
        App.setState({subject: Constants.SUBJECTS.MATH})
        expect(gradeCourseSelector.getGradesAndCourses('Geometry')).toEqual({label: 'Geometry', value: 'Geometry'})

      it 'should remove component safely and unsubscribe from events', () ->
        React.unmountComponentAtNode(container.get(0))
        expect(App.unsubscribe).toHaveBeenCalled()

