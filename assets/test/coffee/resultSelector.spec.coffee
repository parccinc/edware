define [
  'EdwareApplication',
  'edwareConstants' ,
  'edwareResultSelector',
  'react',
  'jquery'
], (App,
 Constants,
 edwareResultSelector,
 React,
 $
) ->
  describe 'ResultSelector', () ->

    EVENTS = Constants.EVENTS
    resultSelector = {}
    container = null
    data = null
    selector = null

    beforeAll (cb) ->
      $.ajax {
        type: "GET"
        url: "/base/data/en/common/common.json"
        dataType: 'json'
        success: (response) ->
          data = response
          cb()
      }

      App.initialize {
        view: Constants.VIEWS.PERFORMANCE
        subject: Constants.SUBJECTS.ELA
        asmtType: Constants.ASMT_TYPE.SUMMATIVE
        result: Constants.RESULT_TYPES.OVERALL
        gradeCourse: Constants.GRADES_COURSES.SUBJECT2.THIRD
        reportName: Constants.REPORT_NAME.ISR
        year: 2015
      }

    beforeEach () ->
      id = 'resultSelector'
      container = $("<div id='#{id}'></div>")
      selector = "##{id}"
      $(document.body).append(container)

    afterEach () ->
      container.remove()

    it 'exists', () ->
      expect(edwareResultSelector.ResultsDropdown).toBeDefined()

    describe 'Result Selector Rendering', () ->

      it 'renders with default resultsList data', () ->
        resultSelectorElement = React.createElement(edwareResultSelector.ResultsDropdown, {
          data: data.resultsLists
        })
        resultSelector = React.render(
          resultSelectorElement,
          container.get(0)
        )
        expect(resultSelector).toBeDefined()
        expect(resultSelector.isMounted()).toBe(true)
        expect($("#{selector} label").length).toBe(1)
        # not stable on jenkins. enable when fixed
        #expect($("#{selector} li.h1").length).toBe(2)
        #expect($("#{selector} li.h2").length).toBe(2)
        #expect($("#{selector} li.sub").length).toBe(5)

      # this feels like integration test
      it 'properly properly renders with student-level resultsList data', () ->
        App.setState({'studentGuid': '12094304'})
        resultSelectorElement = React.createElement(edwareResultSelector.ResultsDropdown, {data: data.resultsLists})
        resultSelector = React.render(
          resultSelectorElement,
          container.get(0)
        )
        # sanity
        expect(resultSelector).toBeDefined()
        expect(resultSelector.isMounted()).toBe(true)

        expect($("#{selector} label").length).toBe(1)
        # doesnt have subscores
        expect($("#{selector} .sub").length).toBe(0)
        # has overall and diagnostics
        expect($("#{selector} li:not(.h1, .h2)").length).toBe(2)

      describe 'Result Selector Functions', () ->

        beforeEach () ->
          spyOn App, 'subscribe'
          resultSelectorElement = React.createElement(edwareResultSelector.ResultsDropdown, {
            data: data.resultsLists
          })
          resultSelector = React.render(
            resultSelectorElement,
            container.get(0)
          )

        it 'subscribes to the change event on mount', () ->
          expect(App.subscribe).toHaveBeenCalled()

        it 'updates App state on result change', () ->
          resultSelector.onResultSelect({
            states: {
              'result': 'overall'
            }
          })
          expect(App.getState().result).toEqual('overall')

        it 'returns the proper result for getResult', () ->
          expect(resultSelector.getResult({
            'asmtType': 'SUMMATIVE',
            'result': 'overall'
          }).label).toEqual("Overall Performance")
        null
