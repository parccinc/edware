define ['edwareUtil'], (utils) ->

  describe 'EdwareUtils module', () ->

    it 'should be defined', () ->
      expect(utils).toBeDefined()

    describe 'getDeepValue', () ->
      rowObject = {
        aValue: 'value',
        nestedObject: {
          nestedValue: 'nestedVal'
        }
      }

      it 'accepts a string as path', () ->
        expect(utils.getDeepValue(rowObject, 'aValue'))
          .toBe rowObject.aValue

      it 'accepts an array with only one item as the path', () ->
        expect(utils.getDeepValue(rowObject, ['aValue']))
          .toBe rowObject.aValue

      it 'returns the correct nested value', () ->
        expect(utils.getDeepValue(rowObject, ['nestedObject', 'nestedValue']))
          .toBe(rowObject.nestedObject.nestedValue)

    describe 'keyMirror', () ->
      it 'is defined', () ->
        expect(utils.keyMirror).toBeDefined()

      it 'returns an object where the values are the same as teh keys', () ->
        data = {
          someKey: null,
          someOtherKey: null
        }
        expectedResult = {
          someKey: 'someKey',
          someOtherKey: 'someOtherKey'
        }
        actualResult = utils.keyMirror data
        expect(actualResult.someKey).toBe(expectedResult.someKey)
        expect(actualResult.someOtherKey).toBe(expectedResult.someOtherKey)

      it 'returns a new empty object when an empty object or null is passed in', () ->

        expect(Object.keys(utils.keyMirror()).length).toBe(0)
        passedInObject = {}
        expect(utils.keyMirror(passedInObject)).not.toBe(passedInObject)
        expect(typeof utils.keyMirror()).toBe('object')


