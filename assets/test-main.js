/**********************************
 ****** Phantom js bind fix ********
 **********************************/
!function(){var a=Array.prototype,b=a.slice,c=Function.prototype;c.bind||(c.bind=function(a){function e(){var e=c.prototype&&this instanceof c;return c.apply(!e&&a||this,d.concat(b.call(arguments)))}var c=this,d=b.call(arguments,1);return e.prototype=c.prototype,e})}();

/*
 * load up require js
 */
var allTestFiles = [];
var TEST_REGEXP = /(spec|test)\.js$/i;

var pathToModule = function(path) {
  return path.replace(/^\/base\//, '').replace(/\.js$/, '');
};

Object.keys(window.__karma__.files).forEach(function(file) {
  if (TEST_REGEXP.test(file)) {
    // Normalize paths to RequireJS module names.
    allTestFiles.push(pathToModule(file));
  }
});

require.config({
  // Karma serves files under /base, which is the basePath from your config file
  baseUrl: '/base/js/src/',

  paths: {
    jquery: '../3p/jquery-1.7.2.min',
    jqueryui: '../3p/jquery-ui-1.8.16.min',
    jquerysticky: '../3p/jquery.sticky',
    jqueryeventdrag: '../3p/jquery.event.drag-2.2',
    jqueryhistory: '../3p/history.adapter.jquery',
    history: '../3p/history',
    slickcore: '../3p/slick.core',
    slickgrid: '../3p/slick.grid',
    bootstrap: '../3p/bootstrap.min',
    text: '../3p/text',
    mustache: '../3p/mustache',
    templates: '../templates',
    usmap: '../3p/usmap/jquery.usmap',
    raphael: '../3p/usmap/raphael',
    cs: '../3p/cs',
    react: '../3p/react',
    reactbootstrap: '../3p/react-bootstrap',
    'JSXTransformer': '../3p/JSXTransformer',
    'coffee-script': '../3p/coffee-script',
    d3: '../3p/d3.v3.min',
    moment: '../3p/moment',
    'moment-range': '../3p/moment-range',
    daterangepicker: '../3p/daterangepicker',
    EdwareApplication: 'modules/EDWARE.application',
    'Edware.StudentReport': 'modules/EDWARE.studentReport',
    'Edware.StudentRosterReport': 'modules/EDWARE.studentRosterReport',
    'Edware.SchoolReport': 'modules/EDWARE.schoolReport',
    'Edware.ComparingPopulationsReport': 'modules/EDWARE.comparingPopulationsReport',
    'Edware.StateViewRedirect': 'modules/EDWARE.stateViewRedirect',
    edware: 'modules/EDWARE',
    edwareEvents: 'modules/EDWARE.events',
    edwareContextSecurity: 'modules/EDWARE.contextSecurity',
    edwareUtil: 'modules/EDWARE.util',
    edwareConstants: 'modules/EDWARE.constants',
    edwareDataProxy: 'modules/EDWARE.dataProxy',
    edwareSharedReportUtil: 'modules/EDWARE.sharedReportUtil',
    edwareGrid: 'widgets/grid/EDWARE.grid.tablegrid',
    edwareGridFormatters: 'widgets/grid/formatter/EDWARE.grid.formatters',
    SlickGridFilter: 'widgets/grid/decorators/GridFilter',
    edwareGrowthChart: 'widgets/growthChart/EDWARE.growthChart',
    edwareGrowthPopover: 'widgets/growthChart/EDWARE.growthPopover',
    edwareComparisonSource: 'widgets/growthChart/EDWARE.comparisonSource',
    edwareGrowthSummary: 'widgets/growthChart/EDWARE.growthSummary',
    edwareBreadcrumbs: 'widgets/breadcrumb/EDWARE.breadcrumbs',
    edwareHeader: 'widgets/header/EDWARE.header',
    edwarePopulationBar: 'widgets/populationBar/EDWARE.populationBar',
    edwareLegend: 'widgets/legend/EDWARE.legend',
    edwareLoadingMask: 'widgets/loadingMask/EDWARE.loadingMask',
    edwareFilterBar: 'widgets/filter/filterBar',
    edwareClientStorage: 'widgets/clientStorage/EDWARE.clientStorage',
    edwareExport: 'modules/EDWARE.export',
    edwareDataExports: 'widgets/header/EDWARE.dataExports',
    edwareDateRangePicker: 'widgets/dateRangePicker/EDWARE.dateRangePicker',
    edwareReportInfoBar: 'widgets/header/EDWARE.infoBar',
    edwareHelpMenu: 'widgets/header/EDWARE.helpMenu',
    edwarePrint: 'widgets/print/EDWARE.print',
    edwarePrintURL: 'widgets/print/EDWARE.printURL',
    edwareModal: 'widgets/modal/EDWARE.modal',
    edwareSubjectSelector: 'widgets/selectors/EDWARE.subjectSelector',
    selectorUtil: 'modules/selector.util',
    edwareResultSelector: 'widgets/selectors/EDWARE.resultSelector',
    edwareViewSelector: 'widgets/viewSelector/EDWARE.viewSelector',
    edwareYearSelector: 'widgets/selectors/EDWARE.yearSelector',
    edwareGradeCourseSelector: 'widgets/selectors/EDWARE.gradeCourseSelector',
    edwareMultiGradeCourseSelector: 'widgets/selectors/EDWARE.multiGradeCourseSelector',
    edwareExportTypeSelector: 'widgets/selectors/EDWARE.exportTypeSelector',
    stickies: 'widgets/sticker/stickies',
    dropdownMixin: 'widgets/selectors/dropdown.mixin',

    csvExport: 'widgets/exportCsv/exportCSV',
    csvDownload: 'widgets/exportCsv/downloadCSV',

    // components
    "listReport": "widgets/report/ListReport",

    edwarePagedGrid: 'widgets/grid/EDWARE.SlickPagedGrid',
    PagedGrid: 'widgets/grid/slick/PagedGrid',
    SlickDataSource: 'widgets/grid/slick/DataSources/SlickDataSource',
    SlickGroupDataSource: 'widgets/grid/slick/DataSources/SlickGroupDataSource',
    SlickSummaryDataSource: 'widgets/grid/slick/DataSources/SlickSummaryDataSource',
    SlickColumnModel: 'widgets/grid/slick/DataSources/SlickColumnModel',
    SlickGridPagination: 'widgets/grid/decorators/GridPagination',
    StudentRosterColumnModel: 'widgets/grid/slick/DataSources/StudentRosterColumnModel',
    AssessmentReportDataAdapter: 'widgets/grid/slick/DataSources/AssessmentReportDataAdapter',
    compareControls: 'widgets/compareBar/compareBar',
    'text!populationBarTemplate': 'widgets/populationBar/populationBarTemplate.html',
    'text!claimAvgTemplate': 'widgets/grid/formatter/templates/claimAvgTemplate.html',
    'text!perfSectionTemplate': 'widgets/perfSection/template.html',
    'text!perfSectionBarTemplate': 'widgets/perfSection/perfBarTemplate.html',

    /*Map test files to Karma paths (/base/test)*/
    'test/js/application.spec': '/base/test/js/application.spec',
    'test/js/utils.spec': '/base/test/js/utils.spec',
    'test/js/selectorUtil.spec': '/base/test/js/selectorUtil.spec',
    'test/js/yearSelector.spec': '/base/test/js/yearSelector.spec',
    'test/js/resultSelector.spec': '/base/test/js/resultSelector.spec',
    'test/js/exportCsv.spec': '/base/test/js/exportCsv.spec',
    'test/js/listReport.spec': '/base/test/js/listReport.spec',
    'test/js/gradeCourseSelector.spec': '/base/test/js/gradeCourseSelector.spec',
    'test/js/multiGradeCourseSelector.spec': '/base/test/js/multiGradeCourseSelector.spec',
    'test/js/exportTypeSelector.spec': '/base/test/js/exportTypeSelector.spec',
    'test/js/dataExports.spec': '/base/test/js/dataExports.spec',
    'test/js/contextSecurity.spec': '/base/test/js/contextSecurity.spec',
    'test/js/gridPagination.spec': '/base/test/js/gridPagination.spec',
    'test/js/helpModal.spec': '/base/test/js/helpModal.spec',
    'test/js/dateRangePicker.spec': '/base/test/js/dateRangePicker.spec',
    'test/js/gridFilter.spec': '/base/test/js/gridFilter.spec',
    'test/js/clientStorage.spec': '/base/test/js/clientStorage.spec'
  },
  shim: {
    'slickgrid': {
      deps: ['jquery', 'slickcore', 'jqueryeventdrag', 'jqueryui'],
      exports: 'slickgrid'
    },
    'slickcore': {
      deps: ['jquery'],
      exports: 'slickcore'
    },
    'jqueryui': {
      deps: ['jquery'],
      exports: 'jqueryui'
    },
    'jqueryeventdrag': {
      deps: ['jquery'],
      exports: 'jqueryeventdrag'
    },
    'history': {
      deps: ['jquery'],
      exports: 'history'
    },
    'bootstrap': {
      deps: ['jquery'],
      exports: 'bootstrap'
    },
    'usmap': {
      deps: ['jquery', 'raphael'],
      exports: 'usmap'
    },
    'reactbootstrap': {
      deps: ['react']
    },
    'moment-range': {
      deps: ['moment']
    },
    'daterangepicker': {
      deps: ['moment', 'bootstrap'],
      exports: 'daterangepicker'
    }
  },
  // dynamically load all test files
  deps: allTestFiles,

  // we have to kickoff jasmine, as it is asynchronous
  callback: window.__karma__.start
});
