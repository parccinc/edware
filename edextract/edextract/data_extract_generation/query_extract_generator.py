__author__ = 'tshewchuk'

"""
This module contains the logic to write to an Assessment CSV or JSON extract file.
"""

from edextract.status.constants import Constants
from edextract.tasks.constants import Constants as TaskConstants, QueryType
from edextract.status.status import ExtractStatus, insert_extract_stats
from edcore.database.edcore_connector import EdCoreDBConnection
from edcore.database.cds_connector import CdsDBConnection
from edcore.utils.csv_writer import write_csv
from itertools import chain
import logging
import json

logger = logging.getLogger(__name__)


def generate_csv(tenant, output_file, task_info, extract_args):
    """
    Write data extract to CSV file.

    @param tenant: Requestor's tenant ID
    @param output_file: File pathname of extract file
    @param task_info: Task information for recording stats
    @param extract_args: Arguments specific to generate_csv
    """

    query = extract_args[TaskConstants.TASK_QUERIES][QueryType.QUERY]
    with EdCoreDBConnection(tenant=tenant) as connection:
        results = connection.get_streaming_result(query)  # this result is a generator
        header, data = _generate_csv_data(results)
        write_csv(output_file, data, header=header)
        insert_extract_stats(task_info, {Constants.STATUS: ExtractStatus.EXTRACTED})


def generate_json(tenant, output_file, task_info, extract_args):
    """
    Write data extract to JSON file.

    @param tenant: Requestor's tenant ID
    @param output_file: File pathname of extract file
    @param task_info: Task information for recording stats
    @param extract_args: Arguments specific to generate_json
    """

    formatter = extract_args[TaskConstants.TASK_JSON_FORMATTER]

    with EdCoreDBConnection(tenant=tenant) as connection, open(output_file, 'w') as json_file:
        query_obj = extract_args.get(TaskConstants.TASK_QUERIES)
        query = query_obj[QueryType.QUERY]
        if query:
            results = connection.get_result(query)
            json_body = formatter(results, output_file)
        else:
            extract_type = extract_args[TaskConstants.EXTRACT_TYPE]
            subject = extract_args[TaskConstants.SUBJECT]
            json_body = formatter(extract_type, subject)
        json.dump(json_body, json_file)
        insert_extract_stats(task_info, {Constants.STATUS: ExtractStatus.GENERATED_JSON})


def generate_cds_csv(tenant, output_file, task_info, extract_args):
    """
    Write data extract to CSV file.

    @param tenant: Requestor's tenant ID
    @param output_file: File pathname of extract file
    @param task_info: Task information for recording stats
    @param extract_args: Arguments specific to generate_csv
    """
    query = extract_args[TaskConstants.TASK_QUERIES][QueryType.QUERY]
    with CdsDBConnection(tenant=tenant) as connection:
        results = connection.get_streaming_result(query)  # this result is a generator
        header, data = _generate_csv_data(results)
        write_csv(output_file, data, header=header)
        insert_extract_stats(task_info, {Constants.STATUS: ExtractStatus.EXTRACTED})


def _generate_csv_data(results):
    """
    Generate the CSV data for the extract.

    @param tenant: ID of tenant for which toe extract data
    @param query: DB query used to extract the data.

    @return: CSV extract header and data
    """
    header = []
    data = []

    try:
        first = next(results)
    except StopIteration as ex:
        logger.error(str(ex))
        return header, data

    header = list(first.keys())
    data = _gen_to_val_list(chain([first], results))

    return header, data


def _gen_to_val_list(dict_gen):
    """
    Convert a generator of dicts into a generator of lists of values for each dict.

    @param dict_gen: Generator of dicts

    @return: Generator of corresponding value lists
    """

    for item in dict_gen:
        yield list(item.values())
