'''
Created on Dec 9, 2013

@author: dip
'''


class ExtractionDataType():
    QUERY_CDS = 'QueryCDSExtract'
    QUERY_CSV = 'QueryCSVExtract'
    QUERY_JSON = 'QueryJSONExtract'
    GEN_JSON = 'GenerateJSON'


class Constants():
    TASK_IS_JSON_REQUEST = 'is_json_request'
    TASK_TASK_ID = 'task_id'
    TASK_JSON_FORMATTER = 'task_json_formatter'
    CELERY_TASK_ID = 'celery_task_id'
    TASK_FILE_NAME = 'file_name'
    TASK_QUERIES = 'task_queries'
    CSV_HEADERS = 'csv_headers'
    DEFAULT_QUEUE_NAME = 'extract'
    SYNC_QUEUE_NAME = 'extract_sync'
    ARCHIVE_QUEUE_NAME = 'extract_archve'
    PARALLEL_QUERY_QUEUE_NAME = 'parallel_query'
    QUERY = 'query'
    TENANT = 'tenant'
    IS_PUBLIC = 'is_public'
    STATE_CODE = 'state_code'
    ACADEMIC_YEAR = 'academicYear'
    EXTRACTION_DATA_TYPE = 'extraction_data_type'
    ROOT_DIRECTORY = 'root_directory'
    ITEM_IDS = 'item_ids'
    DIRECTORY_TO_ARCHIVE = 'directory_to_archive'
    THRESHOLD_SIZE = 'threshold_size'
    EXTRACT_TYPE = 'extract_type'
    SUBJECT = 'subject'
    REQUEST_ID = 'request_id'


class QueryType():
    QUERY = 'query'
    MATCH_ID_QUERY = 'match_id_query'
    ASMT_OUTCOME_QUERY = 'asmt_outcome_query'
