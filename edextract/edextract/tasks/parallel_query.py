import logging
from celery import group
from edextract.celery import celery
from edcore.database.routing import ReportingDbConnection
from edextract.tasks.constants import Constants

log = logging.getLogger('edextract')


@celery.task(name='tasks.parallel_query.execute_in_parallel')
def execute_in_parallel(queries, state_code, queue_name):
    jobs = [execute_query.subtask(args=[{Constants.QUERY: query, Constants.STATE_CODE: state_code}]) for query in queries]
    results = group(jobs).apply_async(queue=queue_name).get()
    return sum(results, [])


@celery.task(name='tasks.parallel_query.execute_mapped_queries_in_parallel')
def execute_mapped_queries_in_parallel(query_map, queue_name):
    '''
    Executes all queries in query_map in parallel and returns a map of query keys to the corresponding
    query results
    '''
    jobs = [execute_keyed_query.subtask(args=[query_key, query]) for query_key, query in query_map.items()]
    results = group(jobs).apply_async(queue=queue_name).get()
    results_map = {}
    for result in results:
        results_map.update(result)
    return results_map


@celery.task(name='tasks.parallel_query.execute_query')
def execute_query(query_info):
    '''
    Executes query for reporting purposes
    :params dict query_info  Contains information about a query to be executed
    ex. query_info = {'query': 'select ...', 'is_public': True, 'tenant': 'ES'}
    tenant must be provided when is_public is True
    state_code must be provided when is_public is False
    '''
    query = query_info.get(Constants.QUERY)
    is_public = query_info.get(Constants.IS_PUBLIC, False)
    tenant = query_info.get(Constants.TENANT)
    state_code = query_info.get(Constants.STATE_CODE)
    log.debug("Start execute query %s in parallel" % (query,))
    with ReportingDbConnection(tenant=tenant, state_code=state_code, is_public=is_public) as conn:
        return conn.get_result(query)


@celery.task(name='tasks.parallel_query.execute_keyed_query')
def execute_keyed_query(query_key, query):
    '''
    Executes query and returns the result mapped to the query_key
    '''
    result = {
        query_key: execute_query(query)
    }
    return result
