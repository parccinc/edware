'''
Created on Oct 31, 2013

@author: dip
'''


class Constants():
    '''
    Constants related to extracts
    '''
    TIMESTAMP = 'timestamp'
    EXTRACT_STATS = 'extract_stats'
    EXTRACT_PARAMS = 'extract_params'
    STATUS = 'status'
    TASK_ID = 'task_id'
    REQUEST_GUID = 'request_guid'
    CELERY_TASK_ID = 'celery_task_id'
    INFO = 'info'
