__author__ = 'tshewchuk'


import os
import tempfile
import shutil
import csv
import json
from edcore.tests.utils.unittest_with_stats_sqlite import Unittest_with_stats_sqlite
from edextract.tasks.constants import Constants as TaskConstants, QueryType
from unittest.mock import patch
from collections import OrderedDict
from edextract.status.constants import Constants
from edcore.tests.utils.unittest_with_edcore_sqlite import (
    Unittest_with_edcore_sqlite,
    get_unittest_tenant_name
)
from edextract.data_extract_generation.query_extract_generator import (
    generate_csv,
    generate_json,
    _generate_csv_data
)


class TestQueryExtractGenerator(Unittest_with_edcore_sqlite, Unittest_with_stats_sqlite):

    def setUp(self):
        self.__tmp_dir = tempfile.mkdtemp('file_archiver_test')
        self._tenant = get_unittest_tenant_name()

    @classmethod
    def setUpClass(cls):
        Unittest_with_edcore_sqlite.setUpClass()
        Unittest_with_stats_sqlite.setUpClass()

    def tearDown(self):
        shutil.rmtree(self.__tmp_dir)

    @patch('edcore.database.edcore_connector.EdCoreDBConnection.get_streaming_result')
    def test_generate_csv_success(self, get_streaming_result_patch):
        def generate_results():
            results = [OrderedDict([('foo', 'stuff'), ('bar', 'more stuff')]),
                       OrderedDict([('foo', 'blahs'), ('bar', 'blurgs')])]
            for i in results:
                yield i
        get_streaming_result_patch.return_value = generate_results()
        output = os.path.join(self.__tmp_dir, 'asmt.csv')
        query = 'Select Dummy_Column From Dummy_Table;'
        task_info = {Constants.TASK_ID: '01',
                     Constants.CELERY_TASK_ID: '02',
                     Constants.REQUEST_GUID: '03'}
        extract_args = {TaskConstants.TASK_QUERIES: {QueryType.QUERY: query}}
        generate_csv(self._tenant, output, task_info, extract_args)

        get_streaming_result_patch.assert_called_once_with(query)
        self.assertTrue(os.path.exists(output))
        csv_data = []
        with open(output) as out:
            data = csv.reader(out)
            for row in data:
                csv_data.append(row)
        self.assertEqual(len(csv_data), 3)
        self.assertEqual(csv_data[0], ['foo', 'bar'])
        self.assertEqual(csv_data[1], ['stuff', 'more stuff'])
        self.assertEqual(csv_data[2], ['blahs', 'blurgs'])

    @patch('edcore.database.edcore_connector.EdCoreDBConnection.get_result')
    def test_generate_json_success(self, get_result_patch):
        def format_json(results, *args):
            return {'blah': results[0]['test'],
                    'blurg': results[1]['test']}
        query_result = [{'test': 'foo'}, {'test': 'bar'}]
        get_result_patch.return_value = query_result
        query = 'Select Dummy_Column From Dummy_Table;'
        output = os.path.join(self.__tmp_dir, 'asmt.json')
        task_info = {Constants.TASK_ID: '01',
                     Constants.CELERY_TASK_ID: '02',
                     Constants.REQUEST_GUID: '03'}
        extract_args = {TaskConstants.TASK_QUERIES: {QueryType.QUERY: query},
                        TaskConstants.TASK_JSON_FORMATTER: format_json}
        generate_json(self._tenant, output, task_info, extract_args)

        get_result_patch.assert_called_once_with(query)
        self.assertTrue(os.path.exists(output))
        self.assertTrue(os.path.getsize(output) > 0)

        expected_json = {'blah': 'foo', 'blurg': 'bar'}
        with open(output) as out:
            json_data = json.load(out)
        self.assertEqual(json_data, expected_json)

    def test_generate_csv_data_no_result(self):
        # Results are empty
        results = self.dummy_empty_iterator()

        header, data = _generate_csv_data(results)
        self.assertEqual(len(header), 0)
        self.assertEqual(len(data), 0)

    def dummy_empty_iterator(self):
        for ctr in range(0, 0):
            yield ctr
