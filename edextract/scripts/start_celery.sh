#!/bin/sh

celeryctl purge

# config
if [ -z "$CELERY_PROD_CONFIG" ]; then
    current_dir=$(pwd)
    export CELERY_PROD_CONFIG="${current_dir%/*/*}/config/development.ini"
fi
celery worker --app=edextract.celery --workdir=../edextract -l debug
