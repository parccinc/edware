import argparse
from pathlib import Path
import gnupg


def parse_args(args: list=None,
               namespace: argparse.Namespace=None
               ) -> argparse.Namespace:
    """

    :param args: optional: manually specify arguments
    :param namespace: optiona: augment namespace
    :return: command line argument namespace
    """
    main_path = Path(__file__).parent
    parser = argparse.ArgumentParser(description='Tool for manually encrypting something for lz')

    parser.add_argument('file', type=Path)
    parser.add_argument('--gpg-home-dir', '-l', type=Path,
                        default=(main_path / '../component_tests/common/data/keys/dev').resolve(),
                        help='location of your gpg keys (default: %(default)s)')
    parser.add_argument('--gpg-recipient', '-r', default='sbac_data_provider@sbac.com',
                        help='recipient is who you are encrypting the file for (default: %(default)s)')
    parser.add_argument('--gpg-passphrase', '-p', default='sbac udl2',
                        help='passphrase (default: %(default)s)')
    parser.add_argument('--gpg-signature-key', '-k', default='ca_user@ca.com',
                        help='owner of your key for signing the output (default: %(default)s)')

    return parser.parse_args(args, namespace)


def main(args: argparse.Namespace):
    """
    :param args: command line argument namespace
    """
    gpg = gnupg.GPG(gnupghome=str(args.gpg_home_dir))

    with args.file.open('rb') as tar_fh:
        gpg.encrypt_file(tar_fh,
                         recipients=[args.gpg_recipient],
                         sign=args.gpg_signature_key,
                         passphrase=args.gpg_passphrase,
                         output=str(args.file) + '.gpg')


if __name__ == '__main__':
    main(parse_args())
