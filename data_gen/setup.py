try:
    import numpy
    import scipy
except ImportError:
    print("Error: must install numpy and scipy via pip because distutils/setuptools are broken.")
    exit(1)

import os
import setuptools

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.md')).read()

requires = [
    'SQLAlchemy == 0.9.9',
    'psycopg2 == 2.5.1',
    'python-gnupg',
    'jsonschema',
    'pathlib',
    'enum34',
    'backports.statistics',
    'scipy',
    'numpy',
]

tests_require = requires

setuptools.setup(name='DataGeneration-PARCC',
                 version='1.2',
                 description='Data generator for the PARCC project',
                 license='proprietary',
                 packages=setuptools.find_packages(),
                 include_package_data=True,
                 zip_safe=False,
                 test_suite='nose.collector',
                 install_requires=requires,
                 tests_require=tests_require,)
