import logging
import itertools

import data_generation.config.assessment
import data_generation.config.config
import data_generation.config.hierarchy
from data_generation.config.population import Grade
from data_generation.config.out import OutputFormat
from data_generation.model.assessment_perf_level import AssessmentPerfLevel
from data_generation.model.staff import Staff
from data_generation.util.exceptions import DataGenConfigurationError
from data_generation.config import levels as level_config
from data_generation.config.assessment import AssessmentSubject, TestPeriod
from data_generation.config.config import Subject
from data_generation.config.batch import Batch, CustomBatch
from data_generation.model.assessment import Assessment
from data_generation.model.assessment_outcome import AssessmentOutcome
from data_generation.model.district import District
from data_generation.model.school import School
from data_generation.model.state import State
from data_generation.util.assessment_stats import RandomLevelByDemographics
from data_generation.util.id_gen import IDGen
from data_generation.writers.multi_writer import MultiWriter

STATE_SUBDIR_FORMAT = "{state}-{subject}"
DISTRICT_SUBDIR_FORMAT = "{state}-{district}-{subject}"


class ParccDataGen:
    __slots__ = (
        '_out_path',
        '_id_gen',
        '_batch',
        '_record_writer',
        '_output_chunk_size',
    )

    def __init__(self, args):
        self._out_path = args.out_path

        self._id_gen = IDGen()
        if args.random_seed is not None:
            self._id_gen.seed(args.random_seed)

        if args.batch == 'CUSTOM':
            self._batch = CustomBatch(args)

        else:
            self._batch = Batch[args.batch]

        self._record_writer = self._get_record_writer(args)

        self._output_chunk_size = args.output_chunk_size

    @staticmethod
    def _get_record_writer(args):
        writer_instances = []

        for output_format_name in args.output_formats:
            output_format = OutputFormat[output_format_name]

            kwargs = dict(output_format.default_kwargs)
            kwargs.update({
                'out_path': args.out_path,
                'per_district': args.multithread or args.per_district,
                'consolidate': args.multithread and not args.per_district,
                'json_schema_path': args.json_schema,
                'gpg_home_dir': args.gpg_home_dir,
                'gpg_recipient': args.gpg_recipient,
                'gpg_signature_key': args.gpg_signature_key,
                'gpg_passphrase': args.gpg_passphrase,

                'db_connector': args.db_connector,
                'db_username': args.db_username,
                'db_password': args.db_password,
                'db_host': args.db_host,
                'db_port': args.db_port,
                'db_database': args.db_database,
                'db_schema': args.db_schema,
                'db_use_transactions': args.db_use_transactions,
            })

            writer_instance = output_format.record_writer_class(formats=output_format.formats, **kwargs)
            writer_instances.append(writer_instance)

        return MultiWriter(writer_instances)

    @property
    def _years(self):
        """
        assumed to be sorted

        :return:
        """
        return self._batch.years

    @property
    def _grades_of_concern(self):
        return self._batch.grades_of_concern

    @property
    def _state_configs(self):
        return self._batch.state_configs

    def generate(self):
        """
        generate PARCC LZ data
        """
        # Start the generation of data
        for state_cfg in self._batch.state_configs:
            logging.info('Creating State: %s' % (state_cfg.state_name.label.capitalize(),))

            # Create the state object
            state = State(state_cfg, self._id_gen)

            # Process the state
            self._generate_state_data(state)

    def _generate_state_data(self, state: State) -> dict:
        """
        Generate an entire data set for a single state.

        @param state: State to generate data for
        @return: a dict of assessments by id, needed for generating the LZ JSON file.
        """
        # Create the assessment objects
        self._record_writer.state_initialize(state)

        assessments = self._generate_state_assessments(state)  # dict of assessments by key

        logging.info("  Created %s assessments" % (len(assessments),))

        # Build the districts
        student_avg_count = 0
        student_unique_count = 0
        for district_type in state.state_type.generate_districts(self._id_gen):
            # Create the district
            district = District(district_type, state, self._id_gen)

            # Generate the district data set
            avg_year, unique, num_schools = self._generate_district_data(district, assessments)

            # Print completion of district
            student_avg_count += avg_year
            student_unique_count += unique

        self._record_writer.state_finalize(state)

        # Print completion of state
        logging.info('  Created State %s with average of %i students/year and %i total unique' %
                     (state.code, student_avg_count, student_unique_count))

    @staticmethod
    def _asmt_key(year: int, period: TestPeriod, grade: Grade, subject: AssessmentSubject):
        return "%04i-%s-%02i-%s" % (year, period.name, grade.code, subject.name)

    def _generate_state_assessments(self, state: State) -> dict:
        """
        Generate assessments for a state.

        :return: a dict of assessments by key
        """
        assessment_perf_levels = [
            AssessmentPerfLevel(assessment_subject, state, perf_level, self._id_gen)
            for assessment_subject, perf_levels in self._batch.assessment_levels.items()
            for perf_level in perf_levels[0]  # [0] is the overall levels
        ]

        self._record_writer.write_perf_levels(assessment_perf_levels)

        assessments_by_key = {
            self._asmt_key(year, period, grade, assessment_subject): Assessment(year, period, state, assessment_subject, perf_levels, self._id_gen)

            for year in self._batch.years
            for period in TestPeriod
            for grade in self._batch.grades_of_concern
            for assessment_subject, perf_levels in self._batch.assessment_levels.items()
            if grade in assessment_subject.grades
        }

        self._record_writer.write_assessments(assessments_by_key.values())

        # TODO US38872: We should add column for periods to the table rpt_item_p_data
        assessment_items = [
            assessment_item
            for assessment in assessments_by_key.values()
            for items_for_subclaim in assessment.eoy.items_by_subclaim.values()
            for assessment_item in items_for_subclaim
        ]

        self._record_writer.write_assessment_items(assessment_items)

        return assessments_by_key

    def _generate_staff(self, schools):
        all_staff_by_subject = {}
        staff_by_subject_by_grade_by_school = {}

        for school in schools:
            staff_by_subject_by_grade_in_school = staff_by_subject_by_grade_by_school.setdefault(school, {})

            for grade in school.grades:
                staff_by_subject_in_grade_in_school = staff_by_subject_by_grade_in_school.setdefault(grade, {})

                # TODO: make class size a thing (low priority)
                # grade_size = school.expected_grade_size(grade)
                num_classes = self._id_gen.choice(data_generation.config.hierarchy.CLASS_COUNT_RANGE)

                for subject in data_generation.config.config.Subject:
                    staff = [Staff(self._id_gen) for _ in range(num_classes)]

                    staff_by_subject_in_grade_in_school[subject] = staff
                    all_staff_by_subject.setdefault(subject, []).extend(staff)

        # ensure that some staff are associated w/ multiple things
        for subject in data_generation.config.config.Subject:
            all_staff_in_subject = all_staff_by_subject[subject]

            for school in schools:
                for grade in school.grades:
                    if self._id_gen.random() < data_generation.config.hierarchy.STAFF_MULTISUBJECT_RATE:
                        staff = staff_by_subject_by_grade_by_school[school][grade][subject]

                        index_to_replace = self._id_gen.choice(range(len(staff)))
                        staff_to_replace = staff[index_to_replace]

                        # try to ensure we're picking someone not already there, but don't loop forever
                        for _ in range(10):
                            if staff_to_replace not in staff:
                                break

                            staff_to_replace = self._id_gen.choice(all_staff_in_subject)

                        if staff_to_replace not in staff:
                            staff[index_to_replace] = staff_to_replace

        return staff_by_subject_by_grade_by_school

    def _generate_schools(self, district: District):
        """
        Generate schools.

        Note that we don't use the hierarchies for anything in PARCC, but the base generator expects them.

        :param district:
        :return: a tuple, the first element being a list of schools, the second being the hierarchies
        """
        # Make the schools
        schools = tuple(School(school_type, district, self._batch.grades_of_concern, self._id_gen)
                        for school_type in district.district_type.generate_schools(self._id_gen))

        return schools

    @staticmethod
    def _group_schools_by_grade(schools):
        """
        Sort a list of schools by grades available in the school.

        @param schools: Schools to sort
        @returns: Dictionary of sorted schools
        """
        schools_by_grade = {}

        for school in schools:
            for grade in school.grades:
                schools_by_grade.setdefault(grade, list()).append(school)

        return schools_by_grade

    @staticmethod
    def _set_up_students_by_grade_by_school(schools, grades_of_concern):
        """
        Build a dictionary that associates each school with the grades of concern that a given school has.

        @param schools: Schools to set up
        @param grades_of_concern: The overall set of grades that we are concerned with
        @returns: Dictionary of schools to dictionary of grades
        """
        schools_with_grades = {}

        for school in schools:
            grades_for_school = tuple(grade
                                      for grade in school.grades
                                      if grade in grades_of_concern)

            schools_with_grades[school] = {grade: [] for grade in grades_for_school}

        return schools_with_grades

    def _generate_district_data(self,
                                district: District,
                                assessments: dict) -> tuple:
        """
        Generate an entire data set for a single district.

        TODO: I'd love to refactor this method so it isn't such a beast, but there's a huge number of interrelated
              variables and no real meaningful abstractions.

        @param district: District to generate data for
        @param assessments: Dictionary of all assessment objects
        :return: Some statistics for console output
        """
        logging.info('  Populating District: %s (expected pop %s, expected # schools: %s)' % (district.name, district.district_type.mean_student_pop, district.district_type.mean_school_count))
        self._record_writer.district_initialize(district)

        schools = self._generate_schools(district)
        staff_by_subject_by_grade_by_school = self._generate_staff(schools)

        # Sort the schools
        district_schools_by_grade = self._group_schools_by_grade(schools)

        # PARCC has separate schools that host tests, this seems like as good a place as any to set it.
        # Mostly because schools are defined here, and not outside this method.
        pba_test_school_by_asmt_id = {}
        eoy_test_school_by_asmt_id = {}

        for asmt_key in assessments:
            pba_test_school_by_asmt_id[asmt_key] = self._id_gen.choice(schools)
            eoy_test_school_by_asmt_id[asmt_key] = self._id_gen.choice(schools)

        # keep track of students for re-enrollment
        all_students = {}
        student_count = 0

        assessment_outcomes = list()
        assessment_item_outcomes = list()

        # Begin processing the years for data
        for asmt_year, is_first_year in zip(self._years, itertools.chain((True,), itertools.repeat(False))):
            logging.info('    processing year %s' % (asmt_year,))
            # Prepare output file names

            # Set up a collection of students by grade by school
            # students_by_grade_by_school[school][grade] == list of students in that grade at that school
            # note that it is empty at this point; we populate it shortly
            students_by_grade_by_school = self._set_up_students_by_grade_by_school(schools, self._grades_of_concern)

            # If students already exist, advance the students forward in the grades
            # note that "students" is empty on the first iteration
            for guid, student in all_students.items():
                # Move the student forward (false from the advance method means the student disappears)
                advanced_into_grade_of_concern = student.advance_student(district_schools_by_grade, self._id_gen)

                if advanced_into_grade_of_concern:
                    # also note that they may have changed school
                    students_by_grade_by_school[student.school][student.grade].append(student)

            # With the students moved around, we will re-populate empty grades and create assessments with outcomes for
            # the students

            for school, students_by_grade in sorted(students_by_grade_by_school.items(),
                                                    key=lambda item: min(item[0].grades)):
                logging.info("      processing school %s" % (school.name,))

                staff_by_subject_by_grade_in_school = staff_by_subject_by_grade_by_school[school]

                # Process the whole school
                for grade, grade_students in sorted(students_by_grade.items()):
                    # populate or re-populate the student population
                    # note that the list "grade_students" is modified by this call
                    school.repopulate_school_grade(grade, grade_students, self._id_gen, acad_year=asmt_year)
                    student_count += len(grade_students)

                    all_students.update((student.guid, student) for student in grade_students)

                    staff_by_subject_in_grade_in_school = staff_by_subject_by_grade_in_school[grade]

                    for subject in data_generation.config.config.Subject:
                        assessment_subjects = AssessmentSubject.assessments_by_subject_grade(subject, grade)

                        if subject == data_generation.config.config.Subject.Math and grade in {Grade.G09, Grade.G10, Grade.G11, Grade.G12}:
                            # high school math, high school math ...
                            assessment_subject = school.get_hs_math_assessment_for_grade(grade)

                        elif len(assessment_subjects) == 1:
                            assessment_subject = assessment_subjects[0]

                        else:
                            raise DataGenConfigurationError("More than 1 assessment for grade/subject combo for non-high-school math? %s %s -> %s"
                                                            % (subject, grade, assessment_subjects))

                        staff_in_subject_in_grade_in_school = staff_by_subject_in_grade_in_school[subject]

                        # we could probably cache these level generators, but they're not that expensive...
                        grade_demographics = school.demographics[grade]
                        level_breakdowns = level_config.LEVELS_BY_GRADE_BY_SUBJ[subject][grade]
                        level_generator = RandomLevelByDemographics(grade_demographics, level_breakdowns)

                        for student in grade_students:
                            # choose a period for the student to take the test in
                            period = self._id_gen.weighted_choice(TestPeriod.weight_by_period())

                            asmt_key = self._asmt_key(asmt_year, period, grade, assessment_subject)
                            asmt = assessments[asmt_key]

                            if self._id_gen.random() < data_generation.config.assessment.TEST_SCHOOL_IS_RESP_SCHOOL_RATE:
                                pba_test_school = school

                            else:
                                pba_test_school = pba_test_school_by_asmt_id[asmt_key]

                            if self._id_gen.random() < data_generation.config.assessment.TEST_SCHOOL_IS_RESP_SCHOOL_RATE:
                                eoy_test_school = school

                            else:
                                eoy_test_school = eoy_test_school_by_asmt_id[asmt_key]

                            assessment_outcome = AssessmentOutcome(asmt, student, pba_test_school, eoy_test_school,
                                                                   level_generator, is_first_year,
                                                                   staff_in_subject_in_grade_in_school,
                                                                   self._id_gen)

                            assessment_outcomes.append(assessment_outcome)

                            assessment_item_outcomes.extend([
                                assessment_item_outcome
                                for item_outcomes_for_subclaim in assessment_outcome.eoy.item_outcomes_by_subclaim.values()
                                for assessment_item_outcome in item_outcomes_for_subclaim
                            ])

                        # if too many assessment outcomes in list, flush it
                        if len(assessment_outcomes) > self._output_chunk_size:
                            self._record_writer.write_assessment_outcomes(assessment_outcomes)
                            self._record_writer.write_assessment_item_outcomes(assessment_item_outcomes)
                            assessment_outcomes.clear()
                            assessment_item_outcomes.clear()

                    # ... MUST flush all AOs at this point, as afterwards the student records change (grades)
                    # TODO: refactor the models so they don't contain reference to other model objects (low priority)
                    #       this would be so we can flush them at any time w/out worrying that other object's values change
                    if assessment_outcomes:
                        self._record_writer.write_assessment_outcomes(assessment_outcomes)
                        self._record_writer.write_assessment_item_outcomes(assessment_item_outcomes)
                        assessment_outcomes.clear()
                        assessment_item_outcomes.clear()

        # Return the average student count
        avg_year, unique, num_schools = student_count // len(self._years), len(all_students), len(schools)
        logging.info('    District created with %s schools, average of %i students/year, and %i total unique students (expect %s/year)' %
                     (num_schools, avg_year, unique, district.district_type.mean_student_pop))

        self._record_writer.district_finalize(district)

        return avg_year, unique, num_schools
