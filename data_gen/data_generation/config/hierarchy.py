"""
PARCC-specific hierarchy configuration.

@author: nestep
@date: March 4, 2014
"""
from collections import namedtuple

import numpy as np

from data_generation.config.population import Demographics, Grade
from data_generation.util.id_gen import IDGen
from data_generation.util.indexed_enum import NoseSafeEnum
from data_generation.util.integer_partition import approx_random_integer_partition
from data_generation.util.read_only_queue import ReadOnlyQueue
from data_generation.util.stats import TriangularDistribution, \
    GammaPopulationSizeDistribution, UniformPopulationSizeDistribution
from data_generation.util.math import largest_remainder_rounding

DISTRICT_PERFORMANCE_DISTRIBUTION = TriangularDistribution(-0.25, 0, .25)
SCHOOL_PERFORMANCE_DISTRIBUTION = TriangularDistribution(-0.25, 0, .25)
GRADE_SIZE_FUDGE = .05  # used to create a scale of grade sizes within a school
STAFF_MULTISUBJECT_RATE = .2  # how likely a staff member is to be associated w/ multiple subjects (sort of)
CLASS_COUNT_RANGE = range(1, 4)  # number of classes in a grade in a given school is chosen randomly from this. only used to pick a staff_id.


class StateName(NoseSafeEnum):
    ALABAMA = 'AL'
    ALASKA = 'AK'
    ARIZONA = 'AZ'
    ARKANSAS = 'AR'
    CALIFORNIA = 'CA'
    COLORADO = 'CO'
    CONNECTICUT = 'CT'
    DELAWARE = 'DE'
    FLORIDA = 'FL'
    GEORGIA = 'GA'
    HAWAII = 'HI'
    IDAHO = 'ID'
    ILLINOIS = 'IL'
    INDIANA = 'IN'
    IOWA = 'IA'
    KANSAS = 'KS'
    KENTUCKY = 'KY'
    LOUISIANA = 'LA'
    MAINE = 'ME'
    MARYLAND = 'MD'
    MASSACHUSETTS = 'MA'
    MICHIGAN = 'MI'
    MINNESOTA = 'MN'
    MISSISSIPPI = 'MS'
    MISSOURI = 'MO'
    MONTANA = 'MT'
    NEBRASKA = 'NE'
    NEVADA = 'NV'
    NEW_HAMPSHIRE = 'NH'
    NEW_JERSEY = 'NJ'
    NEW_MEXICO = 'NM'
    NEW_YORK = 'NY'
    NORTH_CAROLINA = 'NC'
    NORTH_DAKOTA = 'ND'
    OHIO = 'OH'
    OKLAHOMA = 'OK'
    OREGON = 'OR'
    PENNSYLVANIA = 'PA'
    RHODE_ISLAND = 'RI'
    SOUTH_CAROLINA = 'SC'
    SOUTH_DAKOTA = 'SD'
    TENNESSEE = 'TN'
    TEXAS = 'TX'
    UTAH = 'UT'
    VERMONT = 'VT'
    VIRGINIA = 'VA'
    WASHINGTON = 'WA'
    WEST_VIRGINIA = 'WV'
    WISCONSIN = 'WI'
    WYOMING = 'WY'
    DISTRICT_OF_COLUMBIA = 'DC'

    # added to avoid any potential conflict w/ test state codes
    AMERICAN_SAMOA = 'AS'
    GUAM = 'GU'
    NORTHERN_MARIANA_ISLANDS = 'MP'
    PUERTO_RICO = 'PR'
    VIRGIN_ISLANDS = 'VI'
    US_MINOR_OUTLYING_ISLANDS = 'UM'
    FEDERATED_STATES_OF_MICRONESIA = 'FM'
    MARSHALL_ISLANDS = 'MH'
    PALAU = 'PW'

    EXAMPLE = 'ES'  # example state
    CAT = 'FE'  # FELIS
    DOG = 'CS'  # CANIS
    FISH = 'PS'  # PISCIS
    EXAMPLE_STATE_1 = 'WW'
    EXAMPLE_STATE_2 = 'XX'
    EXAMPLE_STATE_3 = 'YY'
    EXAMPLE_STATE_4 = 'ZZ'

    def __init__(self, code):
        self.code = code

    @property
    def label(self):
        return self.name.replace('_', ' ').title()


class SchoolLevel(NoseSafeEnum):
    ELEM_SCHOOL = ((Grade.KG, Grade.G01, Grade.G02, Grade.G03, Grade.G04, Grade.G05),)
    MIDL_SCHOOL = ((Grade.G06, Grade.G07, Grade.G08),)
    HIGH_SCHOOL = ((Grade.G09, Grade.G10, Grade.G11, Grade.G12),)

    FULL_SCHOOL = ((Grade.KG, Grade.G01, Grade.G02, Grade.G03, Grade.G04, Grade.G05, Grade.G06, Grade.G07, Grade.G08, Grade.G09, Grade.G10, Grade.G11, Grade.G12),)

    def __init__(self, grades: tuple):
        self.grades = grades


class SchoolType:
    def __init__(self, school_level: SchoolLevel, mean_student_pop: float, adjustment: float):
        """

        :param school_level:
        :param mean_student_pop:
        :param adjustment: between -1 and 1 (realistically, not between -.6 and .5)
        :return:
        """
        self.school_level = school_level
        self.mean_student_pop = mean_student_pop
        self.adjustment = adjustment

    def __str__(self):
        return "<SchoolType: %s %s>" % (self.school_level, self.adjustment)

    def __repr__(self):
        return str(self)

    @property
    def grades(self):
        return self.school_level.grades

    def grades_of_interest(self, grades_of_interest):
        return tuple(grade for grade in self.grades if grade in grades_of_interest)

    @property
    def mean_pop(self):
        return self.mean_student_pop

    @property
    def mean_grade_pop(self):
        return int(round(self.mean_student_pop / len(self.school_level.grades)))


class ExampleSchool(NoseSafeEnum):
    TEST_ELEM_TINY = SchoolType(school_level=SchoolLevel.ELEM_SCHOOL,
                                mean_student_pop=138,
                                adjustment=0.0)
    TEST_MIDL_TINY = SchoolType(school_level=SchoolLevel.MIDL_SCHOOL,
                                mean_student_pop=69,
                                adjustment=0.0)
    TEST_HIGH_TINY = SchoolType(school_level=SchoolLevel.HIGH_SCHOOL,
                                mean_student_pop=93,
                                adjustment=0.0)


class DistrictType:
    def __init__(self,
                 mean_student_pop: float,
                 mean_school_count: int,
                 base_performance_offset: float,
                 school_pop_dist_type: type=GammaPopulationSizeDistribution):
        self.mean_student_pop = int(round(mean_student_pop))
        self.mean_school_count = int(round(mean_school_count))
        self.base_performance_offset = base_performance_offset
        self._school_pop_dist_type = school_pop_dist_type

    def generate_schools(self, rng: IDGen):
        num_batches = self.mean_school_count // 3  # indicate eleme -> middle -> high path
        num_remaining = self.mean_school_count % 3  # indicate school has all grades

        min_size = min(50, max(10, (self.mean_student_pop // self.mean_school_count) - 10))
        num_parts = num_batches + num_remaining
        remain_pop = self.mean_student_pop - (num_parts * min_size)

        if self._school_pop_dist_type == UniformPopulationSizeDistribution:
            force_uniform = True

        else:
            try:
                # sometimes this doesn't work due to partition approximation not being good enough.
                # an exception will be raised.
                # if that's the case, just do things uniformly.
                parts = np.array(approx_random_integer_partition(remain_pop, num_parts, rng)) + min_size

            except:
                force_uniform = True

            else:
                force_uniform = False
                rng.shuffle(parts)
                batch_pops, remain_pops = parts[:num_batches], parts[num_batches:]

        if force_uniform:
            school_pop_dist = self._school_pop_dist_type(self.mean_student_pop, self.mean_school_count, min_size=min_size)

            batch_pops = school_pop_dist.get_values(num_batches, rng) * 3
            remain_pops = school_pop_dist.get_values(num_remaining, rng)

        remain_perfs = SCHOOL_PERFORMANCE_DISTRIBUTION.get_values(num_remaining, rng) + self.base_performance_offset

        for batch_pop in batch_pops:
            elem_pop = batch_pop * len(SchoolLevel.ELEM_SCHOOL.grades) / len(SchoolLevel.FULL_SCHOOL.grades)
            midl_pop = batch_pop * len(SchoolLevel.MIDL_SCHOOL.grades) / len(SchoolLevel.FULL_SCHOOL.grades)
            high_pop = batch_pop * len(SchoolLevel.HIGH_SCHOOL.grades) / len(SchoolLevel.FULL_SCHOOL.grades)

            elem_pop, midl_pop, high_pop = largest_remainder_rounding((elem_pop, midl_pop, high_pop), batch_pop)

            yield SchoolType(SchoolLevel.ELEM_SCHOOL, elem_pop, SCHOOL_PERFORMANCE_DISTRIBUTION.get_values(None, rng) + self.base_performance_offset)
            yield SchoolType(SchoolLevel.MIDL_SCHOOL, midl_pop, SCHOOL_PERFORMANCE_DISTRIBUTION.get_values(None, rng) + self.base_performance_offset)
            yield SchoolType(SchoolLevel.HIGH_SCHOOL, high_pop, SCHOOL_PERFORMANCE_DISTRIBUTION.get_values(None, rng) + self.base_performance_offset)

        for remain_pop, remain_perf in zip(remain_pops, remain_perfs):
            yield SchoolType(SchoolLevel.FULL_SCHOOL, remain_pop, remain_perf)


class ExampleDistrict(NoseSafeEnum):
    CHICAGO = DistrictType(mean_student_pop=400000,
                           mean_school_count=888,
                           base_performance_offset=0.0)
    TEST_TINY = DistrictType(mean_student_pop=300,
                             mean_school_count=3,
                             base_performance_offset=0.0,
                             school_pop_dist_type=UniformPopulationSizeDistribution)


class StateDescription:
    def __init__(self,
                 state_name: StateName,
                 mean_student_pop: int,
                 mean_district_count: int,
                 mean_school_count: int,
                 base_performance_offset: float=0.0,
                 required_districts=(),
                 demographics: Demographics=Demographics.TYPICAL1,
                 state_pop_dist_type: type=GammaPopulationSizeDistribution,
                 state_pop_dist_kwargs: dict={},
                 school_count_dist_type: type=GammaPopulationSizeDistribution,
                 school_pop_dist_type: type=GammaPopulationSizeDistribution):
        self.state_name = state_name
        self.demographics = demographics
        self.mean_student_pop = mean_student_pop
        self.mean_district_count = mean_district_count
        self.mean_school_count = mean_school_count
        self.required_districts = required_districts
        self.base_performance_offset = base_performance_offset
        self._state_pop_dist_type = state_pop_dist_type
        self._state_pop_dist_kwargs = state_pop_dist_kwargs
        self._school_count_dist_type = school_count_dist_type
        self._school_pop_dist_type = school_pop_dist_type

    def generate_districts(self, rng: IDGen):
        remaining_dists = self.mean_district_count - len(self.required_districts)
        remaining_pop = self.mean_student_pop - sum(district.mean_student_pop for district in self.required_districts)
        remaining_schools = self.mean_school_count - sum(district.mean_school_count for district in self.required_districts)

        pop_dist = self._state_pop_dist_type(remaining_pop, remaining_dists, **self._state_pop_dist_kwargs)
        avg_school_count = remaining_schools // remaining_dists
        min_school_count = max(1, min(8, avg_school_count // 3))
        school_count_dist = self._school_count_dist_type(remaining_schools, remaining_dists, min_size=min_school_count, scale_ratio=4.5)

        dist_pops = pop_dist.get_values(remaining_dists, rng)
        school_counts = school_count_dist.get_values(remaining_dists, rng)
        dist_base_perfs = DISTRICT_PERFORMANCE_DISTRIBUTION.get_values(remaining_dists, rng) + self.base_performance_offset

        dist_pops.sort()
        school_counts.sort()

        ninety_percent = int(round(.8 * len(dist_pops)))
        small_dist_pops, large_dist_pops = dist_pops[:ninety_percent], dist_pops[ninety_percent:]
        small_school_counts, large_school_counts = school_counts[:ninety_percent], school_counts[ninety_percent:]
        rng.shuffle(small_dist_pops)
        rng.shuffle(large_dist_pops)
        rng.shuffle(small_school_counts)
        rng.shuffle(large_school_counts)
        dist_pops = np.concatenate((small_dist_pops, large_dist_pops))
        school_counts = np.concatenate((small_school_counts, large_school_counts))

        for dist_pop, school_count, dist_base_perf in zip(dist_pops, school_counts, dist_base_perfs):
            yield DistrictType(dist_pop, school_count, dist_base_perf, school_pop_dist_type=self._school_pop_dist_type)

        yield from self.required_districts


class ExampleState(NoseSafeEnum):
    PARCC_LARGE_CAT = StateDescription(StateName.NEW_YORK,
                                       demographics=Demographics.TYPICAL1,
                                       mean_student_pop=2083097,
                                       mean_district_count=866,
                                       mean_school_count=4397,
                                       required_districts=[ExampleDistrict.CHICAGO.value])
    PARCC_LARGE_DOG = StateDescription(StateName.RHODE_ISLAND,
                                       demographics=Demographics.CALIFORNIA,
                                       mean_student_pop=2083097,
                                       mean_district_count=866,
                                       mean_school_count=4397,
                                       required_districts=[ExampleDistrict.CHICAGO.value])
    PARCC_SMALL_CAT = StateDescription(StateName.NEW_YORK,
                                       demographics=Demographics.TYPICAL1,
                                       mean_student_pop=208309,
                                       mean_district_count=86,
                                       mean_school_count=439)
    PARCC_SMALL_DOG = StateDescription(StateName.RHODE_ISLAND,
                                       demographics=Demographics.CALIFORNIA,
                                       mean_student_pop=208309,
                                       mean_district_count=86,
                                       mean_school_count=439)
    DEVEL_TINY = StateDescription(StateName.NEW_YORK,
                                  mean_student_pop=300,
                                  mean_district_count=1,
                                  mean_school_count=3,
                                  state_pop_dist_type=UniformPopulationSizeDistribution,
                                  school_count_dist_type=UniformPopulationSizeDistribution,
                                  school_pop_dist_type=UniformPopulationSizeDistribution)
    TEST_CAT = StateDescription(StateName.CAT,
                                mean_student_pop=300,
                                mean_district_count=1,
                                mean_school_count=3,
                                state_pop_dist_type=UniformPopulationSizeDistribution,
                                school_count_dist_type=UniformPopulationSizeDistribution,
                                school_pop_dist_type=UniformPopulationSizeDistribution)
    TEST_DOG = StateDescription(StateName.DOG,
                                mean_student_pop=300,
                                mean_district_count=1,
                                mean_school_count=3,
                                state_pop_dist_type=UniformPopulationSizeDistribution,
                                school_count_dist_type=UniformPopulationSizeDistribution,
                                school_pop_dist_type=UniformPopulationSizeDistribution)
    TEST_DISTRICTS = StateDescription(StateName.CAT,
                                      mean_student_pop=300 * 8,
                                      mean_district_count=8,
                                      mean_school_count=3 * 8,
                                      state_pop_dist_type=UniformPopulationSizeDistribution,
                                      school_count_dist_type=UniformPopulationSizeDistribution,
                                      school_pop_dist_type=UniformPopulationSizeDistribution)
    TEST_COVERAGE = StateDescription(StateName.CAT,
                                     mean_student_pop=300 * 36,
                                     mean_district_count=36,
                                     mean_school_count=3 * 36,
                                     state_pop_dist_type=UniformPopulationSizeDistribution,
                                     school_count_dist_type=UniformPopulationSizeDistribution,
                                     school_pop_dist_type=UniformPopulationSizeDistribution)
    UAT_STATE_1 = StateDescription(StateName.EXAMPLE_STATE_1,
                                   mean_student_pop=13 * 30 * 120 / 3,  # 13 total grades, 30 students per grade, 120 schools, / 3 grades of interest per school
                                   mean_district_count=20,
                                   mean_school_count=120,
                                   state_pop_dist_type=UniformPopulationSizeDistribution,
                                   school_count_dist_type=UniformPopulationSizeDistribution,
                                   school_pop_dist_type=UniformPopulationSizeDistribution)
    UAT_STATE_2 = StateDescription(StateName.EXAMPLE_STATE_2,
                                   mean_student_pop=13 * 30 * 120 / 3,
                                   mean_district_count=20,
                                   mean_school_count=120,
                                   state_pop_dist_type=UniformPopulationSizeDistribution,
                                   school_count_dist_type=UniformPopulationSizeDistribution,
                                   school_pop_dist_type=UniformPopulationSizeDistribution)
    UAT_STATE_3 = StateDescription(StateName.EXAMPLE_STATE_3,
                                   mean_student_pop=13 * 30 * 120 / 3,
                                   mean_district_count=20,
                                   mean_school_count=120,
                                   state_pop_dist_type=UniformPopulationSizeDistribution,
                                   school_count_dist_type=UniformPopulationSizeDistribution,
                                   school_pop_dist_type=UniformPopulationSizeDistribution)
    UAT_STATE_4 = StateDescription(StateName.EXAMPLE_STATE_4,
                                   mean_student_pop=13 * 30 * 120 / 3,
                                   mean_district_count=20,
                                   mean_school_count=120,
                                   state_pop_dist_type=UniformPopulationSizeDistribution,
                                   school_count_dist_type=UniformPopulationSizeDistribution,
                                   school_pop_dist_type=UniformPopulationSizeDistribution)


ForceDistrictInfo = namedtuple("ForceDistrictInfo", ['id', 'name'])
FORCE_DISTRICT_IDS = {
    StateName.CAT: ReadOnlyQueue((ForceDistrictInfo("FEXMI220CSU0FSA", "Bluebell Noctule Public Schools"),
                                  ForceDistrictInfo("FEXQ1OWCQFEBG0M", "Waterbuck Snowdrop Public Schools"))),
    StateName.DOG: ReadOnlyQueue((ForceDistrictInfo("FEXBXKVB9TVG5FR", "Pintail Caribou County Schools"),
                                  ForceDistrictInfo("FEXIR9H1F2K43BV", "Kingbird Hummingbird County Schools"))),

    StateName.NEW_YORK: ReadOnlyQueue((ForceDistrictInfo("FEXMI220CSU0FSA", "Bluebell Noctule Public Schools"),
                                       ForceDistrictInfo("FEXQ1OWCQFEBG0M", "Waterbuck Snowdrop Public Schools"))),
    StateName.NEW_JERSEY: ReadOnlyQueue((ForceDistrictInfo("FEXMI220CSU0FSA", "Bluebell Noctule Public Schools"),
                                         ForceDistrictInfo("FEXQ1OWCQFEBG0M", "Waterbuck Snowdrop Public Schools"))),
    StateName.RHODE_ISLAND: ReadOnlyQueue((ForceDistrictInfo("FEXBXKVB9TVG5FR", "Pintail Caribou County Schools"),
                                           ForceDistrictInfo("FEXIR9H1F2K43BV", "Kingbird Hummingbird County Schools"))),

    StateName.EXAMPLE_STATE_1: ReadOnlyQueue((ForceDistrictInfo("XXRP2AOF5D0HW0I", "Fabylo County Public Schools"),
                                              ForceDistrictInfo("XXGL43PJAMO888P", "Sycamby SD"),)),
    StateName.EXAMPLE_STATE_2: ReadOnlyQueue((ForceDistrictInfo("WW0AGKEL6NTZEH3", "Paraggina Joint School District"),
                                              ForceDistrictInfo("WWQQYY3DUTGL1BP", "Oologia School Board"),)),
    StateName.EXAMPLE_STATE_3: ReadOnlyQueue((ForceDistrictInfo("YYP2F5QXBPG7R4Y", "Revepora School Board"),
                                              ForceDistrictInfo("YYL1G0U6PUAOKLP", "Quitland Port County School District"),)),
    StateName.EXAMPLE_STATE_4: ReadOnlyQueue((ForceDistrictInfo("ZZ7SD3WODXM220P", "Yancisson Area Schools"),
                                              ForceDistrictInfo("ZZUANPFKK5FQBCO", "Naha Community School District"),)),
}
