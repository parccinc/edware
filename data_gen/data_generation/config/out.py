# -*- coding: utf-8 -*-
"""
Define the output format for PARCC data.

Classes here provide a method (get_row) which, when given one or more model objects, returns a dictionary
which can be written to a JSON file, a line of CSV, or to the DB by a writer.
"""
from datetime import date
from itertools import count

from data_generation.config import config
from data_generation.model.assessment import Assessment
from data_generation.model.assessment_item import AssessmentItem
from data_generation.model.assessment_item_outcome import AssessmentItemOutcome
from data_generation.model.assessment_outcome import AssessmentOutcome
from data_generation.model.assessment_perf_level import AssessmentPerfLevel
from data_generation.util import udl_stored_procs
from data_generation.util.abstract_output_formats import RecordFormat
from data_generation.util.indexed_enum import NoseSafeEnum
from data_generation.util.perf_levels import PerfLevel
from data_generation.util.properties import Properties
from data_generation.writers import filter
from data_generation.writers.csv_writer import CSVWriter
from data_generation.writers.db_writer import DBWriter
from data_generation.writers.lz_writer import LZWriter


class PARCCSummativeJSONFormat(RecordFormat):
    name_format = 'payload'

    @classmethod
    def get_row(cls,
                assessment: Assessment):
        def get_test_levels():
            return [
                dict(test_perf_lvl_id=str(perf_level.index + 1),
                     test_perf_lvl_cutpoint_label=perf_level.name,
                     test_perf_lvl_cutpoint_low=perf_level.start,
                     test_perf_lvl_cutpoint_upper=perf_level.stop - 1)
                for perf_level in assessment.overall_perf_levels
            ]

        metadata = dict(file_name=PARCCSummativeFormat.name_format + '.csv',
                        file_type='asmt_data',
                        summative_period=assessment.period.label,
                        test_code=assessment.assessment_subject.name,
                        test_levels=get_test_levels())

        return metadata


class PARCCSummativeFormat(RecordFormat):
    name_format = 'data'

    @classmethod
    def get_row(cls, assessment_outcome: AssessmentOutcome) -> Properties:
        v = Properties()  # this is an OrderedDict, so the values will come out in the order they are set

        # record info
        v.RecordType = filter.enum_value(assessment_outcome.record_type)
        v.MultipleRecordFlag = filter.yblank(assessment_outcome.is_multi_rec)
        v.ReportedSummativeScoreFlag = filter.yblank(assessment_outcome.is_reported_summative)
        v.ReportedRosterFlag = filter.ynblank(assessment_outcome.reported_roster_flag)
        v.ReportSuppressionCode = filter.enum_value(assessment_outcome.report_suppression_code)
        v.ReportSuppressionAction = filter.enum_value(assessment_outcome.report_suppression_action)
        v.PBA03Category = filter.enum_value(assessment_outcome.pba.category)
        v.EOY03Category = filter.enum_value(assessment_outcome.eoy.category)

        v.StateAbbreviation = assessment_outcome.state.code

        # school/district info
        v.ResponsibleDistrictIdentifier = assessment_outcome.responsible_district.identifier
        v.ResponsibleDistrictName = assessment_outcome.responsible_district.name
        v.ResponsibleSchoolInstitutionIdentifier = assessment_outcome.responsible_school.identifier
        v.ResponsibleSchoolInstitutionName = assessment_outcome.responsible_school.name

        v.PBATestingDistrictIdentifier = assessment_outcome.pba_test_district.identifier
        v.PBATestingDistrictName = assessment_outcome.pba_test_district.name
        v.PBATestingSchoolInstitutionIdentifier = assessment_outcome.pba_test_school.identifier
        v.PBATestingSchoolInstitutionName = assessment_outcome.pba_test_school.name

        v.EOYTestingDistrictIdentifier = assessment_outcome.eoy_test_district.identifier
        v.EOYTestingDistrictName = assessment_outcome.eoy_test_district.name
        v.EOYTestingSchoolInstitutionIdentifier = assessment_outcome.eoy_test_school.identifier
        v.EOYTestingSchoolInstitutionName = assessment_outcome.eoy_test_school.name

        # Student IDs
        v.PARCCStudentIdentifier = assessment_outcome.student.parcc_id
        v.StateStudentIdentifier = assessment_outcome.student.state_id
        v.LocalStudentIdentifier = assessment_outcome.student.local_id

        # Student basic info
        v.FirstName = assessment_outcome.student.first_name
        v.MiddleName = assessment_outcome.student.middle_name
        v.LastName = assessment_outcome.student.last_name
        v.Sex = filter.sex(assessment_outcome.student.sex)
        v.Birthdate = filter.date_y_m_d(assessment_outcome.student.birthdate)
        v.OptionalStateData1 = assessment_outcome.opt_state_data_1
        v.GradeLevelWhenAssessed = filter.zero_padded_grade(assessment_outcome.student.grade)

        # Student Ethnicity
        v.HispanicorLatinoEthnicity = filter.yn(assessment_outcome.student.eth_hispanic)
        v.AmericanIndianorAlaskaNative = filter.yn(assessment_outcome.student.eth_amer_ind)
        v.Asian = filter.yn(assessment_outcome.student.eth_asian)
        v.BlackorAfricanAmerican = filter.yn(assessment_outcome.student.eth_black)
        v.NativeHawaiianorOtherPacificIslander = filter.yn(assessment_outcome.student.eth_pacific)
        v.White = filter.yn(assessment_outcome.student.eth_white)
        v.TwoorMoreRaces = filter.yn(assessment_outcome.student.eth_multi)
        v.FillerRaceField = filter.yn(assessment_outcome.student.eth_filler)
        v.FederalRaceEthnicity = assessment_outcome.student.eth_federal

        # status data
        v.EnglishLearner = filter.yn(assessment_outcome.student.prg_lep)
        v.TitleIIILimitedEnglishProficientParticipationStatus = filter.yn(assessment_outcome.student.prg_lep)
        v.GiftedandTalented = filter.yn(assessment_outcome.student.prg_gifted)
        v.MigrantStatus = filter.yn(assessment_outcome.student.prg_migrant)
        v.EconomicDisadvantageStatus = filter.yn(assessment_outcome.student.prg_econ_disad)
        v.StudentWithDisabilities = filter.yn(assessment_outcome.student.prg_disability)
        v.PrimaryDisabilityType = filter.enum_name(assessment_outcome.student.prg_primary_disability)

        # status accommodations?
        v.AssessmentAccommodationEnglishLearner = filter.yblank(assessment_outcome.student.prg_lep)
        v.AssessmentAccommodation504 = filter.yblank(assessment_outcome.student.prg_sec504)
        v.AssessmentAccommodationIndividualizedEducationalPlanIEP = filter.yblank(assessment_outcome.student.prg_iep)

        # accommodations / pnp (now mixed) - NEW rule - value == Y (or enum val) if either PBA or EOY result is Y else blank
        v.FrequentBreaks = filter.yblank(assessment_outcome.accomod_freq_breaks)
        v.SeparateAlternateLocation = filter.yblank(assessment_outcome.accomod_alt_location)
        v.SmallTestingGroup = filter.yblank(assessment_outcome.accomod_small_group)
        v.SpecializedEquipmentorFurniture = filter.yblank(assessment_outcome.accomod_special_equip)
        v.SpecifiedAreaorSetting = filter.yblank(assessment_outcome.accomod_spec_area)
        v.TimeOfDay = filter.yblank(assessment_outcome.accomod_time_day)
        v.AnswerMasking = filter.yblank(assessment_outcome.accomod_answer_mask)
        v.ColorContrast = filter.enum_value(assessment_outcome.accomod_color_contrast)
        v.TexttoSpeechforMathematics = filter.yblank(assessment_outcome.accomod_text_2_speech_math)
        v.HumanReaderorHumanSignerforMathematics = filter.enum_value(assessment_outcome.accomod_read_math)
        v.ASLVideo = filter.yblank(assessment_outcome.accomod_asl_video)
        v.ScreenReaderORotherAssistiveTechnologyATApplication = filter.yblank(assessment_outcome.accomod_screen_reader)
        v.ClosedCaptioningforELAL = filter.yblank(assessment_outcome.accomod_close_capt_ela)
        v.HumanReaderorHumanSignerforELAL = filter.enum_value(assessment_outcome.accomod_read_ela)
        v.RefreshableBrailleDisplayforELAL = filter.yblank(assessment_outcome.accomod_braille_ela)
        v.TactileGraphics = filter.yblank(assessment_outcome.accomod_tactile_graph)
        v.TexttoSpeechforELAL = filter.yblank(assessment_outcome.accomod_text_2_speech_ela)
        v.AnswersRecordedinTestBook = filter.yblank(assessment_outcome.accomod_answer_rec)
        v.BrailleResponse = filter.enum_value(assessment_outcome.accomod_braille_resp)
        v.CalculationDeviceandMathematicsTools = filter.yblank(assessment_outcome.accomod_calculator)
        v.ELALConstructedResponse = filter.enum_value(assessment_outcome.accomod_construct_resp_ela)
        v.ELALSelectedResponseorTechnologyEnhancedItems = filter.enum_value(assessment_outcome.accomod_select_resp_ela)
        v.MathematicsResponse = filter.enum_value(assessment_outcome.accomod_math_resp)
        v.MonitorTestResponse = filter.yblank(assessment_outcome.accomod_monitor_test_resp)
        v.WordPrediction = filter.yblank(assessment_outcome.accomod_word_predict)
        v.AdministrationDirectionsClarifiedinStudentsNativeLanguage = filter.yblank(assessment_outcome.accomod_native_lang)
        v.AdministrationDirectionsReadAloudinStudentsNativeLanguage = filter.enum_value(assessment_outcome.accomod_loud_native_lang)
        v.MathematicsResponseEL = filter.enum_value(assessment_outcome.accomod_math_rsp)
        v.TranslationoftheMathematicsAssessmentinTexttoSpeech = filter.enum_value(assessment_outcome.accomod_math_text_2_speech)
        v.TranslationoftheMathematicsAssessmentOnline = filter.enum_value(assessment_outcome.accomod_math_trans_online)
        v.WordtoWordDictionaryEnglishNativeLanguage = filter.yblank(assessment_outcome.accomod_w_2_w_dict)
        v.ExtendedTime = filter.enum_value(assessment_outcome.accomod_extend_time)
        v.AlternateRepresentationPaperTest = filter.yblank(assessment_outcome.accomod_alt_paper_test)
        v.TranslationoftheMathematicsAssessmentinPaper = filter.enum_value(assessment_outcome.accomod_paper_trans_math)
        v.HumanReaderorHumanSigner = filter.enum_value(assessment_outcome.accomod_human_read_sign)
        v.LargePrint = filter.yblank(assessment_outcome.accomod_large_print)
        v.BraillewithTactileGraphics = filter.yblank(assessment_outcome.accomod_braille_tactile)

        # other filler fields (1 is above)
        v.OptionalStateData2 = assessment_outcome.opt_state_data_2
        v.OptionalStateData3 = assessment_outcome.opt_state_data_3
        v.OptionalStateData4 = assessment_outcome.opt_state_data_4
        v.OptionalStateData5 = assessment_outcome.opt_state_data_5
        v.OptionalStateData6 = assessment_outcome.opt_state_data_6
        v.OptionalStateData7 = assessment_outcome.opt_state_data_7
        v.OptionalStateData8 = assessment_outcome.opt_state_data_8

        # assessment info
        v.Period = filter.enum_label(assessment_outcome.assessment.period)
        v.TestCode = filter.enum_name(assessment_outcome.assessment.assessment_subject)
        v.Subject = filter.enum_label(assessment_outcome.assessment.assessment_subject)

        # assessment ids
        v.PBAFormID = assessment_outcome.pba.form_id
        v.EOYFormID = assessment_outcome.eoy.form_id
        v.PBAStudentTestUUID = assessment_outcome.pba.student_test_uuid
        v.EOYStudentTestUUID = assessment_outcome.eoy.student_test_uuid
        v.SummativeScoreRecordUUID = assessment_outcome.summative.score_record_uuid

        # item info
        v.PBATotalTestItems = assessment_outcome.pba_assessment.total_item_count
        v.PBATestAttemptednessFlag = filter.ynblank(assessment_outcome.pba.attempt_flag)
        v.EOYTotalTestItems = assessment_outcome.eoy_assessment.total_item_count
        v.EOYTestAttemptednessFlag = filter.ynblank(assessment_outcome.eoy.attempt_flag)

        v.PBATotalTestItemsAttempted = assessment_outcome.pba.total_attempted_count
        v.PBAUnit1TotalNumberofItems = assessment_outcome.pba_assessment.subclaim_1_item_count
        v.PBAUnit1NumberofAttemptedItems = assessment_outcome.pba.subclaim_1_attempted_count
        v.PBAUnit2TotalNumberofItems = assessment_outcome.pba_assessment.subclaim_2_item_count
        v.PBAUnit2NumberofAttemptedItems = assessment_outcome.pba.subclaim_2_attempted_count
        v.PBAUnit3TotalNumberofItems = assessment_outcome.pba_assessment.subclaim_3_item_count
        v.PBAUnit3NumberofAttemptedItems = assessment_outcome.pba.subclaim_3_attempted_count
        v.PBAUnit4TotalNumberofItems = assessment_outcome.pba_assessment.subclaim_4_item_count
        v.PBAUnit4NumberofAttemptedItems = assessment_outcome.pba.subclaim_4_attempted_count
        v.PBAUnit5TotalNumberofItems = assessment_outcome.pba_assessment.subclaim_5_item_count
        v.PBAUnit5NumberofAttemptedItems = assessment_outcome.pba.subclaim_5_attempted_count

        v.EOYTotalTestItemsAttempted = assessment_outcome.eoy.total_attempted_count
        v.EOYUnit1TotalNumberofItems = assessment_outcome.eoy_assessment.subclaim_1_item_count
        v.EOYUnit1NumberofAttemptedItems = assessment_outcome.eoy.subclaim_1_attempted_count
        v.EOYUnit2TotalNumberofItems = assessment_outcome.eoy_assessment.subclaim_2_item_count
        v.EOYUnit2NumberofAttemptedItems = assessment_outcome.eoy.subclaim_2_attempted_count
        v.EOYUnit3TotalNumberofItems = assessment_outcome.eoy_assessment.subclaim_3_item_count
        v.EOYUnit3NumberofAttemptedItems = assessment_outcome.eoy.subclaim_3_attempted_count
        v.EOYUnit4TotalNumberofItems = assessment_outcome.eoy_assessment.subclaim_4_item_count
        v.EOYUnit4NumberofAttemptedItems = assessment_outcome.eoy.subclaim_4_attempted_count
        v.EOYUnit5TotalNumberofItems = assessment_outcome.eoy_assessment.subclaim_5_item_count
        v.EOYUnit5NumberofAttemptedItems = assessment_outcome.eoy.subclaim_5_attempted_count

        v.PBANotTestedReason = assessment_outcome.pba.not_tested_reason
        v.EOYNotTestedReason = assessment_outcome.eoy.not_tested_reason
        v.PBAVoidPBAEOYScoreReason = assessment_outcome.pba.void_reason
        v.EOYVoidPBAEOYScoreReason = assessment_outcome.eoy.void_reason

        # score info
        v.PBARawScore = assessment_outcome.pba.raw_score
        v.EOYRawScore = assessment_outcome.eoy.raw_score

        v.SummativeScaleScore = assessment_outcome.summative.scale_score
        v.SummativeCSEM = assessment_outcome.summative.csem

        v.SummativePerformanceLevel = filter.enum_id(assessment_outcome.summative.perf_level)
        v.SummativeReadingScaleScore = assessment_outcome.summative.reading_scale_score
        v.SummativeReadingCSEM = assessment_outcome.summative.reading_csem
        v.SummativeWritingScaleScore = assessment_outcome.summative.writing_scale_score
        v.SummativeWritingCSEM = assessment_outcome.summative.writing_csem

        v.Subclaim1Category = filter.enum_id(assessment_outcome.summative.subclaim_1_perf_level)
        v.Subclaim2Category = filter.enum_id(assessment_outcome.summative.subclaim_2_perf_level)
        v.Subclaim3Category = filter.enum_id(assessment_outcome.summative.subclaim_3_perf_level)
        v.Subclaim4Category = filter.enum_id(assessment_outcome.summative.subclaim_4_perf_level)
        v.Subclaim5Category = filter.enum_id(assessment_outcome.summative.subclaim_5_perf_level)
        v.Subclaim6Category = filter.enum_id(assessment_outcome.summative.subclaim_6_perf_level)

        # additional score info
        v.StudentGrowthPercentileComparedtoState = assessment_outcome.summative.student_growth_state
        v.StudentGrowthPercentileComparedtoDistrict = assessment_outcome.summative.student_growth_district
        v.StudentGrowthPercentileComparedtoPARCC = assessment_outcome.summative.student_growth_parcc

        # "other"
        v.AssessmentYear = assessment_outcome.assessment_year
        v.AssessmentGrade = filter.enum_label(assessment_outcome.assessment.grade)
        v.StaffMemberIdentifier = assessment_outcome.staff_id

        return v


class PARCCReportingItemPDataFormatDB(RecordFormat):
    """
    for DB output
    """
    name_format = 'rpt_item_p_data'

    @classmethod
    def get_row(cls, assessment_item: AssessmentItem):
        v = Properties()

        v.rec_id = assessment_item.rec_id  # BigInteger, primary_key=True, autoincrement=True, nullable=False
        v.p_value = assessment_item.p_value  # Numeric(5, 2), nullable=True
        v.p_distractor1 = assessment_item.p_distractor1  # Numeric(5, 2), nullable=True
        v.p_distractor2 = assessment_item.p_distractor2  # Numeric(5, 2), nullable=True
        v.p_distractor3 = assessment_item.p_distractor3  # Numeric(5, 2), nullable=True
        v.p_distractor4 = assessment_item.p_distractor4  # Numeric(5, 2), nullable=True
        v.p_distractor5 = assessment_item.p_distractor5  # Numeric(5, 2), nullable=True
        v.p_score0 = assessment_item.p_score0  # Numeric(5, 2), nullable=True
        v.p_score1 = assessment_item.p_score1  # Numeric(5, 2), nullable=True
        v.p_score2 = assessment_item.p_score2  # Numeric(5, 2), nullable=True
        v.p_score3 = assessment_item.p_score3  # Numeric(5, 2), nullable=True
        v.p_score4 = assessment_item.p_score4  # Numeric(5, 2), nullable=True
        v.p_score5 = assessment_item.p_score5  # Numeric(5, 2), nullable=True
        v.p_score6 = assessment_item.p_score6  # Numeric(5, 2), nullable=True
        v.point_biserial_correct = assessment_item.point_biserial_correct  # Numeric(7, 4), nullable=True
        v.point_biserial_1 = assessment_item.point_biserial_1  # Numeric(7, 4), nullable=True
        v.point_biserial_2 = assessment_item.point_biserial_2  # Numeric(7, 4), nullable=True
        v.point_biserial_3 = assessment_item.point_biserial_3  # Numeric(7, 4), nullable=True
        v.point_biserial_4 = assessment_item.point_biserial_4  # Numeric(7, 4), nullable=True
        v.point_biserial_5 = assessment_item.point_biserial_5  # Numeric(7, 4), nullable=True
        v.dif_any = assessment_item.dif_any  # CHAR(1), nullable=True
        v.dif_female = assessment_item.dif_female  # CHAR(1), nullable=True
        v.dif_aa = assessment_item.dif_aa  # CHAR(1), nullable=True
        v.dif_hispanic = assessment_item.dif_hispanic  # CHAR(1), nullable=True
        v.dif_asian = assessment_item.dif_asian  # CHAR(1), nullable=True
        v.dif_na = assessment_item.dif_na  # CHAR(1), nullable=True
        v.dif_swd = assessment_item.dif_swd  # CHAR(1), nullable=True
        v.dif_ell = assessment_item.dif_ell  # CHAR(1), nullable=True
        v.dif_ses = assessment_item.dif_ses  # CHAR(1), nullable=True
        v.p_omit = assessment_item.p_omit  # Numeric(5, 2), nullable=True
        v.a_parameter = assessment_item.a_parameter  # Numeric(7, 4), nullable=True
        v.b_parameter = assessment_item.b_parameter  # Numeric(7, 4), nullable=True
        v.c_parameter = assessment_item.c_parameter  # Numeric(7, 4), nullable=True
        v.step_1 = assessment_item.step_1  # Numeric(5, 2), nullable=True
        v.step_2 = assessment_item.step_2  # Numeric(5, 2), nullable=True
        v.step_3 = assessment_item.step_3  # Numeric(5, 2), nullable=True
        v.step_4 = assessment_item.step_4  # Numeric(5, 2), nullable=True
        v.step_5 = assessment_item.step_5  # Numeric(5, 2), nullable=True
        v.step_6 = assessment_item.step_6  # Numeric(5, 2), nullable=True
        v.n_size = assessment_item.n_size  # Integer, nullable=True

        v.item_guid = assessment_item.item_guid  # String(50), nullable=False, info={'natural_key': True}

        v.item_group_guid = assessment_item.item_group_guid  # String(50), nullable=True
        v.item_key_response1 = assessment_item.item_key_response1  # String(250), nullable=True
        v.item_key_response2 = assessment_item.item_key_response2  # String(250), nullable=True
        v.item_key_response3 = assessment_item.item_key_response3  # String(250), nullable=True
        v.item_key_response4 = assessment_item.item_key_response4  # String(250), nullable=True
        v.item_admin_type = assessment_item.item_admin_type  # String(50), nullable=True
        v.asmt_subject = filter.enum_name(assessment_item.assessment.assessment_subject)
        v.item_grade_course = assessment_item.item_grade_course  # String(20), nullable=True
        v.item_delivery_mode = assessment_item.item_delivery_mode  # String(50), nullable=True

        # this one matters
        v.test_item_type = filter.enum_value(assessment_item.test_item_type)  # String(50), nullable=True

        v.is_calc_allowed = assessment_item.is_calc_allowed  # Boolean, nullable=True

        # these two matter
        v.standard1 = filter.enum_value(assessment_item.standard1)  # String(250), nullable=True
        v.standard2 = filter.enum_value(assessment_item.standard2)  # String(250), nullable=True
        v.standard3 = filter.enum_value(assessment_item.standard3)  # String(250), nullable=True
        v.standard4 = filter.enum_value(assessment_item.standard4)  # String(250), nullable=True
        v.standard5 = filter.enum_value(assessment_item.standard5)  # String(250), nullable=True
        v.standard6 = filter.enum_value(assessment_item.standard6)  # String(250), nullable=True
        v.evidence_statement = filter.enum_value(assessment_item.evidence_statement)  # String(2000), nullable=True
        v.evidence_statement1 = filter.enum_value(assessment_item.evidence_statement1)  # String(250), nullable=True
        v.evidence_statement2 = filter.enum_value(assessment_item.evidence_statement2)  # String(250), nullable=True
        v.evidence_statement3 = filter.enum_value(assessment_item.evidence_statement3)  # String(250), nullable=True
        v.evidence_statement4 = filter.enum_value(assessment_item.evidence_statement4)  # String(250), nullable=True
        v.evidence_statement5 = filter.enum_value(assessment_item.evidence_statement5)  # String(250), nullable=True
        v.evidence_statement6 = filter.enum_value(assessment_item.evidence_statement6)  # String(250), nullable=True

        v.batch_guid = config.GLOBAL_RUN_ID
        v.rec_status = 'C'
        v.create_date = str(date.today())

        return v


class PARCCReportingItemPDataFormatCSV(RecordFormat):
    """
    for CSV output
    """
    name_format = 'rpt_item_p_data'

    @classmethod
    def get_row(cls, assessment_item: AssessmentItem):
        v = Properties()

        v.rec_id = assessment_item.rec_id  # BigInteger, primary_key=True, autoincrement=True, nullable=False
        v.p_value = assessment_item.p_value  # Numeric(5, 2), nullable=True
        v.p_distractor1 = assessment_item.p_distractor1  # Numeric(5, 2), nullable=True
        v.p_distractor2 = assessment_item.p_distractor2  # Numeric(5, 2), nullable=True
        v.p_distractor3 = assessment_item.p_distractor3  # Numeric(5, 2), nullable=True
        v.p_distractor4 = assessment_item.p_distractor4  # Numeric(5, 2), nullable=True
        v.p_distractor5 = assessment_item.p_distractor5  # Numeric(5, 2), nullable=True
        v.p_score0 = assessment_item.p_score0  # Numeric(5, 2), nullable=True
        v.p_score1 = assessment_item.p_score1  # Numeric(5, 2), nullable=True
        v.p_score2 = assessment_item.p_score2  # Numeric(5, 2), nullable=True
        v.p_score3 = assessment_item.p_score3  # Numeric(5, 2), nullable=True
        v.p_score4 = assessment_item.p_score4  # Numeric(5, 2), nullable=True
        v.p_score5 = assessment_item.p_score5  # Numeric(5, 2), nullable=True
        v.p_score6 = assessment_item.p_score6  # Numeric(5, 2), nullable=True
        v.point_biserial_correct = assessment_item.point_biserial_correct  # Numeric(7, 4), nullable=True
        v.point_biserial_1 = assessment_item.point_biserial_1  # Numeric(7, 4), nullable=True
        v.point_biserial_2 = assessment_item.point_biserial_2  # Numeric(7, 4), nullable=True
        v.point_biserial_3 = assessment_item.point_biserial_3  # Numeric(7, 4), nullable=True
        v.point_biserial_4 = assessment_item.point_biserial_4  # Numeric(7, 4), nullable=True
        v.point_biserial_5 = assessment_item.point_biserial_5  # Numeric(7, 4), nullable=True
        v.dif_any = assessment_item.dif_any  # CHAR(1), nullable=True
        v.dif_female = assessment_item.dif_female  # CHAR(1), nullable=True
        v.dif_aa = assessment_item.dif_aa  # CHAR(1), nullable=True
        v.dif_hispanic = assessment_item.dif_hispanic  # CHAR(1), nullable=True
        v.dif_asian = assessment_item.dif_asian  # CHAR(1), nullable=True
        v.dif_na = assessment_item.dif_na  # CHAR(1), nullable=True
        v.dif_swd = assessment_item.dif_swd  # CHAR(1), nullable=True
        v.dif_ell = assessment_item.dif_ell  # CHAR(1), nullable=True
        v.dif_ses = assessment_item.dif_ses  # CHAR(1), nullable=True
        v.p_omit = assessment_item.p_omit  # Numeric(5, 2), nullable=True
        v.a_parameter = assessment_item.a_parameter  # Numeric(7, 4), nullable=True
        v.b_parameter = assessment_item.b_parameter  # Numeric(7, 4), nullable=True
        v.c_parameter = assessment_item.c_parameter  # Numeric(7, 4), nullable=True
        v.step_1 = assessment_item.step_1  # Numeric(5, 2), nullable=True
        v.step_2 = assessment_item.step_2  # Numeric(5, 2), nullable=True
        v.step_3 = assessment_item.step_3  # Numeric(5, 2), nullable=True
        v.step_4 = assessment_item.step_4  # Numeric(5, 2), nullable=True
        v.step_5 = assessment_item.step_5  # Numeric(5, 2), nullable=True
        v.step_6 = assessment_item.step_6  # Numeric(5, 2), nullable=True
        v.n = assessment_item.n_size  # Integer, nullable=True

        v.entity_id = assessment_item.item_guid  # String(50), nullable=False, info={'natural_key': True}

        v.set_id = assessment_item.item_group_guid  # String(50), nullable=True
        v.key_response1 = assessment_item.item_key_response1  # String(250), nullable=True
        v.key_response2 = assessment_item.item_key_response2  # String(250), nullable=True
        v.key_response3 = assessment_item.item_key_response3  # String(250), nullable=True
        v.key_response4 = assessment_item.item_key_response4  # String(250), nullable=True
        v.admin_type = assessment_item.item_admin_type  # String(50), nullable=True
        v.subject = filter.enum_name(assessment_item.assessment.assessment_subject)
        v.grade = assessment_item.item_grade_course  # String(20), nullable=True
        v.delivery_mode = assessment_item.item_delivery_mode  # String(50), nullable=True

        # this one matters
        v.item_type = filter.enum_value(assessment_item.test_item_type)  # String(50), nullable=True

        v.calc_use = assessment_item.is_calc_allowed  # Boolean, nullable=True

        # these two matter
        v.standard1 = filter.enum_value(assessment_item.standard1)  # String(250), nullable=True
        v.standard2 = filter.enum_value(assessment_item.standard2)  # String(250), nullable=True
        v.standard3 = filter.enum_value(assessment_item.standard3)  # String(250), nullable=True
        v.standard4 = filter.enum_value(assessment_item.standard4)  # String(250), nullable=True
        v.standard5 = filter.enum_value(assessment_item.standard5)  # String(250), nullable=True
        v.standard6 = filter.enum_value(assessment_item.standard6)  # String(250), nullable=True
        v.evidence_statement = filter.enum_value(assessment_item.evidence_statement)  # String(2000), nullable=True
        v.evidence_statement1 = filter.enum_value(assessment_item.evidence_statement1)  # String(250), nullable=True
        v.evidence_statement2 = filter.enum_value(assessment_item.evidence_statement2)  # String(250), nullable=True
        v.evidence_statement3 = filter.enum_value(assessment_item.evidence_statement3)  # String(250), nullable=True
        v.evidence_statement4 = filter.enum_value(assessment_item.evidence_statement4)  # String(250), nullable=True
        v.evidence_statement5 = filter.enum_value(assessment_item.evidence_statement5)  # String(250), nullable=True
        v.evidence_statement6 = filter.enum_value(assessment_item.evidence_statement6)  # String(250), nullable=True

        v.batch_guid = config.GLOBAL_RUN_ID
        v.rec_status = 'C'
        v.create_date = str(date.today())

        return v


class PARCCReportingSummativeItemScoreFormat(RecordFormat):
    name_format = 'rpt_{subject}_sum_item_score'

    @classmethod
    def get_row(cls,
                assessment_item_outcome: AssessmentItemOutcome):
        v = Properties()  # this is an OrderedDict, so the values will come out in the order they are set

        v.rec_id = assessment_item_outcome.rec_id  # BigInteger, primary_key=True, nullable=False
        v.year = assessment_item_outcome.assessment_item.assessment.year  # String(9), nullable=False
        v.state_code = assessment_item_outcome.state.code  # CHAR(2), nullable=False
        v.resp_dist_id = assessment_item_outcome.responsible_district.identifier  # String(15), nullable=True
        v.resp_dist_name = assessment_item_outcome.responsible_district.name  # String(60), nullable=True
        v.resp_school_id = assessment_item_outcome.responsible_school.identifier  # String(15), nullable=True
        v.resp_school_name = assessment_item_outcome.responsible_school.name  # String(60), nullable=True
        v.test_dist_id = assessment_item_outcome.test_district.identifier  # String(15), nullable=True
        v.test_dist_name = assessment_item_outcome.test_district.name  # String(60), nullable=True
        v.test_school_id = assessment_item_outcome.test_school.identifier  # String(15), nullable=True
        v.test_school_name = assessment_item_outcome.test_school.name  # String(60), nullable=True
        v.admin_code = assessment_item_outcome.admin_code  # String(15), nullable=False
        v.test_code = filter.enum_name(assessment_item_outcome.assessment_subject)  # String(20), nullable=False
        v.asmt_grade = filter.enum_label(assessment_item_outcome.assessment.grade)  # String(15), nullable=True
        v.asmt_subject = filter.enum_label(assessment_item_outcome.assessment.assessment_subject)  # String(35), nullable=False

        v.form_id = assessment_item_outcome.assessment_outcome.eoy.form_id  # String(14), nullable=False
        v.form_format = assessment_item_outcome.form_format  # String(5), nullable=False
        v.student_parcc_id = assessment_item_outcome.student.parcc_id  # String(40), nullable=False, info={'natural_key': True}
        v.student_local_id = assessment_item_outcome.student.local_id  # String(30), nullable=False
        v.student_grade = filter.zero_padded_grade(assessment_item_outcome.student.grade)  # String(11), nullable=False

        v.test_uuid = assessment_item_outcome.assessment_outcome.summative.score_record_uuid  # String(36), nullable=False, info={'natural_key': True}  # FK -> rpt_{subject}_sum.sum_score_rec_uuid
        v.item_uin = assessment_item_outcome.assessment_item.item_guid  # String(35), nullable=False, info={'natural_key': True}  # FK -> rpt_item_p_data.item_guid

        v.item_max_score = assessment_item_outcome.assessment_item.max_score  # SmallInteger, nullable=False
        v.parent_item_score = assessment_item_outcome.parent_item_score  # SmallInteger, nullable=False

        v.batch_guid = config.GLOBAL_RUN_ID
        v.rec_status = 'C'
        v.create_date = str(date.today())

        return v


class PARCCReportingSummativeFormat(RecordFormat):
    name_format = 'rpt_{subject}_sum'

    @classmethod
    def get_row(cls,
                assessment_outcome: AssessmentOutcome):
        v = Properties()  # this is an OrderedDict, so the values will come out in the order they are set

        v.rec_id = assessment_outcome.rec_id

        # record info
        v.record_type = filter.enum_value(assessment_outcome.record_type)
        v.is_multi_rec = filter.yblank(assessment_outcome.is_multi_rec)
        v.is_reported_summative = filter.yblank(assessment_outcome.is_reported_summative)
        v.is_roster_reported = filter.ynblank(assessment_outcome.reported_roster_flag)
        v.report_suppression_code = filter.enum_value(assessment_outcome.report_suppression_code)
        v.report_suppression_action = filter.enum_value(assessment_outcome.report_suppression_action)
        v.pba_category = filter.enum_value(assessment_outcome.pba.category)
        v.eoy_category = filter.enum_value(assessment_outcome.eoy.category)

        v.state_code = assessment_outcome.state.code

        # school/district info
        v.resp_dist_id = assessment_outcome.responsible_district.identifier
        v.resp_dist_name = assessment_outcome.responsible_district.name
        v.resp_school_id = assessment_outcome.responsible_school.identifier
        v.resp_school_name = assessment_outcome.responsible_school.name

        v.pba_test_dist_id = assessment_outcome.pba_test_district.identifier
        v.pba_test_dist_name = assessment_outcome.pba_test_district.name
        v.pba_test_school_id = assessment_outcome.pba_test_school.identifier
        v.pba_test_school_name = assessment_outcome.pba_test_school.name

        v.eoy_test_dist_id = assessment_outcome.eoy_test_district.identifier
        v.eoy_test_dist_name = assessment_outcome.eoy_test_district.name
        v.eoy_test_school_id = assessment_outcome.eoy_test_school.identifier
        v.eoy_test_school_name = assessment_outcome.eoy_test_school.name

        # Student IDs
        v.student_parcc_id = assessment_outcome.student.parcc_id
        v.student_state_id = assessment_outcome.student.state_id
        v.student_local_id = assessment_outcome.student.local_id

        # Student basic info
        v.student_first_name = assessment_outcome.student.first_name
        v.student_middle_name = assessment_outcome.student.middle_name
        v.student_last_name = assessment_outcome.student.last_name
        v.student_sex = filter.sex(assessment_outcome.student.sex)
        v.student_dob = filter.date_y_m_d(assessment_outcome.student.birthdate)
        v.opt_state_data1 = assessment_outcome.opt_state_data_1
        v.student_grade = filter.zero_padded_grade(assessment_outcome.student.grade)

        # Student Ethnicity
        v.ethn_hisp_latino = filter.yn(assessment_outcome.student.eth_hispanic)
        v.ethn_indian_alaska = filter.yn(assessment_outcome.student.eth_amer_ind)
        v.ethn_asian = filter.yn(assessment_outcome.student.eth_asian)
        v.ethn_black = filter.yn(assessment_outcome.student.eth_black)
        v.ethn_hawai = filter.yn(assessment_outcome.student.eth_pacific)
        v.ethn_white = filter.yn(assessment_outcome.student.eth_white)
        v.ethn_two_or_more_races = filter.yn(assessment_outcome.student.eth_multi)
        v.ethn_filler = filter.yn(assessment_outcome.student.eth_filler)
        v.ethnicity = assessment_outcome.student.eth_federal

        # status data
        v.ell = filter.yn(assessment_outcome.student.prg_lep)
        v.lep_status = filter.yn(assessment_outcome.student.prg_lep)
        v.gift_talent = filter.yn(assessment_outcome.student.prg_gifted)
        v.migrant_status = filter.yn(assessment_outcome.student.prg_migrant)
        v.econo_disadvantage = filter.yn(assessment_outcome.student.prg_econ_disad)
        v.disabil_student = filter.yn(assessment_outcome.student.prg_disability)
        v.primary_disabil_type = filter.enum_name(assessment_outcome.student.prg_primary_disability)

        # for some reason, staff_id goes here
        v.staff_id = assessment_outcome.staff_id

        # status accommodations?
        v.accomod_ell = filter.yblank(assessment_outcome.student.prg_lep)
        v.accomod_504 = filter.yblank(assessment_outcome.student.prg_sec504)
        v.accomod_ind_ed = filter.yblank(assessment_outcome.student.prg_iep)

        # accommodations / pnp (now mixed) - NEW rule - value == Y (or enum val) if either PBA or EOY result is Y else blank
        v.accomod_freq_breaks = filter.yblank(assessment_outcome.accomod_freq_breaks)
        v.accomod_alt_location = filter.yblank(assessment_outcome.accomod_alt_location)
        v.accomod_small_group = filter.yblank(assessment_outcome.accomod_small_group)
        v.accomod_special_equip = filter.yblank(assessment_outcome.accomod_special_equip)
        v.accomod_spec_area = filter.yblank(assessment_outcome.accomod_spec_area)
        v.accomod_time_day = filter.yblank(assessment_outcome.accomod_time_day)
        v.accomod_answer_mask = filter.yblank(assessment_outcome.accomod_answer_mask)
        v.accomod_color_contrast = filter.enum_value(assessment_outcome.accomod_color_contrast)
        v.accomod_text_2_speech_math = filter.yblank(assessment_outcome.accomod_text_2_speech_math)
        v.accomod_read_math = filter.enum_value(assessment_outcome.accomod_read_math)
        v.accomod_asl_video = filter.yblank(assessment_outcome.accomod_asl_video)
        v.accomod_screen_reader = filter.yblank(assessment_outcome.accomod_screen_reader)
        v.accomod_close_capt_ela = filter.yblank(assessment_outcome.accomod_close_capt_ela)
        v.accomod_read_ela = filter.enum_value(assessment_outcome.accomod_read_ela)
        v.accomod_braille_ela = filter.yblank(assessment_outcome.accomod_braille_ela)
        v.accomod_tactile_graph = filter.yblank(assessment_outcome.accomod_tactile_graph)
        v.accomod_text_2_speech_ela = filter.yblank(assessment_outcome.accomod_text_2_speech_ela)
        v.accomod_answer_rec = filter.yblank(assessment_outcome.accomod_answer_rec)
        v.accomod_braille_resp = filter.enum_value(assessment_outcome.accomod_braille_resp)
        v.accomod_calculator = filter.yblank(assessment_outcome.accomod_calculator)
        v.accomod_construct_resp_ela = filter.enum_value(assessment_outcome.accomod_construct_resp_ela)
        v.accomod_select_resp_ela = filter.enum_value(assessment_outcome.accomod_select_resp_ela)
        v.accomod_math_resp = filter.enum_value(assessment_outcome.accomod_math_resp)
        v.accomod_monitor_test_resp = filter.yblank(assessment_outcome.accomod_monitor_test_resp)
        v.accomod_word_predict = filter.yblank(assessment_outcome.accomod_word_predict)
        v.accomod_native_lang = filter.yblank(assessment_outcome.accomod_native_lang)
        v.accomod_loud_native_lang = filter.enum_value(assessment_outcome.accomod_loud_native_lang)
        v.accomod_math_rsp = filter.enum_value(assessment_outcome.accomod_math_rsp)
        v.accomod_math_text_2_speech = filter.enum_value(assessment_outcome.accomod_math_text_2_speech)
        v.accomod_math_trans_online = filter.enum_value(assessment_outcome.accomod_math_trans_online)
        v.accomod_w_2_w_dict = filter.yblank(assessment_outcome.accomod_w_2_w_dict)
        v.accomod_extend_time = filter.enum_value(assessment_outcome.accomod_extend_time)
        v.accomod_alt_paper_test = filter.yblank(assessment_outcome.accomod_alt_paper_test)
        v.accomod_paper_trans_math = filter.enum_value(assessment_outcome.accomod_paper_trans_math)
        v.accomod_human_read_sign = filter.enum_value(assessment_outcome.accomod_human_read_sign)
        v.accomod_large_print = filter.yblank(assessment_outcome.accomod_large_print)
        v.accomod_braille_tactile = filter.yblank(assessment_outcome.accomod_braille_tactile)

        # other filler fields (1 is above)
        v.opt_state_data2 = assessment_outcome.opt_state_data_2
        v.opt_state_data3 = assessment_outcome.opt_state_data_3
        v.opt_state_data4 = assessment_outcome.opt_state_data_4
        v.opt_state_data5 = assessment_outcome.opt_state_data_5
        v.opt_state_data6 = assessment_outcome.opt_state_data_6
        v.opt_state_data7 = assessment_outcome.opt_state_data_7
        v.opt_state_data8 = assessment_outcome.opt_state_data_8

        # assessment info
        v.poy = filter.enum_label(assessment_outcome.assessment.period)
        v.test_code = filter.enum_name(assessment_outcome.assessment.assessment_subject)
        v.asmt_subject = filter.enum_label(assessment_outcome.assessment.assessment_subject)

        # assessment ids
        v.pba_form_id = assessment_outcome.pba.form_id
        v.eoy_form_id = assessment_outcome.eoy.form_id
        v.pba_test_uuid = assessment_outcome.pba.student_test_uuid
        v.eoy_test_uuid = assessment_outcome.eoy.student_test_uuid
        v.sum_score_rec_uuid = assessment_outcome.summative.score_record_uuid

        # item info
        v.pba_total_items = assessment_outcome.pba_assessment.total_item_count
        v.pba_attempt_flag = filter.ynblank(assessment_outcome.pba.attempt_flag)
        v.eoy_total_items = assessment_outcome.eoy_assessment.total_item_count
        v.eoy_attempt_flag = filter.ynblank(assessment_outcome.eoy.attempt_flag)

        v.pba_total_items_attempt = assessment_outcome.pba.total_attempted_count
        v.pba_total_items_unit1 = assessment_outcome.pba_assessment.subclaim_1_item_count
        v.pba_unit1_items_attempt = assessment_outcome.pba.subclaim_1_attempted_count
        v.pba_total_items_unit2 = assessment_outcome.pba_assessment.subclaim_2_item_count
        v.pba_unit2_items_attempt = assessment_outcome.pba.subclaim_2_attempted_count
        v.pba_total_items_unit3 = assessment_outcome.pba_assessment.subclaim_3_item_count
        v.pba_unit3_items_attempt = assessment_outcome.pba.subclaim_3_attempted_count
        v.pba_total_items_unit4 = assessment_outcome.pba_assessment.subclaim_4_item_count
        v.pba_unit4_items_attempt = assessment_outcome.pba.subclaim_4_attempted_count
        v.pba_total_items_unit5 = assessment_outcome.pba_assessment.subclaim_5_item_count
        v.pba_unit5_items_attempt = assessment_outcome.pba.subclaim_5_attempted_count

        v.eoy_total_items_attempt = assessment_outcome.eoy.total_attempted_count
        v.eoy_total_items_unit1 = assessment_outcome.eoy_assessment.subclaim_1_item_count
        v.eoy_unit1_items_attempt = assessment_outcome.eoy.subclaim_1_attempted_count
        v.eoy_total_items_unit2 = assessment_outcome.eoy_assessment.subclaim_2_item_count
        v.eoy_unit2_items_attempt = assessment_outcome.eoy.subclaim_2_attempted_count
        v.eoy_total_items_unit3 = assessment_outcome.eoy_assessment.subclaim_3_item_count
        v.eoy_unit3_items_attempt = assessment_outcome.eoy.subclaim_3_attempted_count
        v.eoy_total_items_unit4 = assessment_outcome.eoy_assessment.subclaim_4_item_count
        v.eoy_unit4_items_attempt = assessment_outcome.eoy.subclaim_4_attempted_count
        v.eoy_total_items_unit5 = assessment_outcome.eoy_assessment.subclaim_5_item_count
        v.eoy_unit5_items_attempt = assessment_outcome.eoy.subclaim_5_attempted_count

        v.pba_not_tested_reason = assessment_outcome.pba.not_tested_reason
        v.eoy_not_tested_reason = assessment_outcome.eoy.not_tested_reason
        v.pba_void_reason = assessment_outcome.pba.void_reason
        v.eoy_void_reason = assessment_outcome.eoy.void_reason

        # score info
        v.pba_raw_score = assessment_outcome.pba.raw_score
        v.eoy_raw_score = assessment_outcome.eoy.raw_score

        v.sum_scale_score = assessment_outcome.summative.scale_score
        v.sum_csem = assessment_outcome.summative.csem

        v.sum_perf_lvl = filter.enum_id(assessment_outcome.summative.perf_level)
        v.sum_read_scale_score = assessment_outcome.summative.reading_scale_score
        v.sum_read_csem = assessment_outcome.summative.reading_csem
        v.sum_write_scale_score = assessment_outcome.summative.writing_scale_score
        v.sum_write_csem = assessment_outcome.summative.writing_csem

        v.subclaim1_category = filter.enum_id(assessment_outcome.summative.subclaim_1_perf_level)
        v.subclaim2_category = filter.enum_id(assessment_outcome.summative.subclaim_2_perf_level)
        v.subclaim3_category = filter.enum_id(assessment_outcome.summative.subclaim_3_perf_level)
        v.subclaim4_category = filter.enum_id(assessment_outcome.summative.subclaim_4_perf_level)
        v.subclaim5_category = filter.enum_id(assessment_outcome.summative.subclaim_5_perf_level)
        v.subclaim6_category = filter.enum_id(assessment_outcome.summative.subclaim_6_perf_level)

        # additional score info
        v.state_growth_percent = assessment_outcome.summative.student_growth_state
        v.district_growth_percent = assessment_outcome.summative.student_growth_district
        v.parcc_growth_percent = assessment_outcome.summative.student_growth_parcc

        # "other"
        v.year = assessment_outcome.assessment_year
        v.asmt_grade = filter.enum_label(assessment_outcome.assessment.grade)

        # reporting schema only bits
        v.include_in_parcc = udl_stored_procs.include_in_aggregate(record_type=v.record_type, is_reported_summative=v.is_reported_summative, report_suppression_code=v.report_suppression_code, report_suppression_action=v.report_suppression_action)
        v.include_in_state = udl_stored_procs.include_in_aggregate(record_type=v.record_type, is_reported_summative=v.is_reported_summative, report_suppression_code=v.report_suppression_code, report_suppression_action=v.report_suppression_action)
        v.include_in_district = udl_stored_procs.include_in_aggregate(record_type=v.record_type, is_reported_summative=v.is_reported_summative, report_suppression_code=v.report_suppression_code, report_suppression_action=v.report_suppression_action)
        v.include_in_school = udl_stored_procs.include_in_school(record_type=v.record_type, is_reported_summative=v.is_reported_summative, report_suppression_code=v.report_suppression_code, report_suppression_action=v.report_suppression_action)
        v.include_in_roster = udl_stored_procs.include_in_roster(record_type=v.record_type, is_reported_summative=v.is_reported_summative, report_suppression_code=v.report_suppression_code, report_suppression_action=v.report_suppression_action, is_roster_reported=v.is_roster_reported)
        v.include_in_isr = udl_stored_procs.include_in_isr(record_type=v.record_type, is_reported_summative=v.is_reported_summative, report_suppression_code=v.report_suppression_code, report_suppression_action=v.report_suppression_action)

        v.batch_guid = config.GLOBAL_RUN_ID
        v.rec_status = 'C'
        v.create_date = str(date.today())

        return v


class PARCCReportingTestScoreFormat(RecordFormat):
    name_format = 'rpt_sum_test_score_m'

    @classmethod
    def get_row(cls,
                assessment: Assessment):
        v = Properties()

        v.rec_id = assessment.rec_id
        v.test_code = assessment.assessment_subject.name
        v.sum_period = assessment.period.label
        v.batch_guid = config.GLOBAL_RUN_ID
        v.rec_status = 'C'
        v.create_date = str(date.today())

        return v


class PARCCReportingTestLevelFormat(RecordFormat):
    name_format = 'rpt_sum_test_level_m'
    _rec_id_gen = count()

    @classmethod
    def get_row(cls,
                perf_level: AssessmentPerfLevel):
        v = Properties()

        v.rec_id = next(cls._rec_id_gen)  # bigserial primary key
        v.test_code = perf_level.assessment_subject.name
        v.perf_lvl_id = perf_level.perf_level.index + 1  # varchar(30) but it needs be an integer (usually between 1 and 5?)
        v.cutpoint_label = perf_level.perf_level.name
        v.cutpoint_low = perf_level.perf_level.start
        v.cutpoint_upper = perf_level.perf_level.stop - 1
        v.batch_guid = config.GLOBAL_RUN_ID
        v.rec_status = 'C'
        v.create_date = str(date.today())

        return v


class OutputFormat(NoseSafeEnum):
    """
    output format options
    """
    UDL_LZ = LZWriter, {AssessmentOutcome: PARCCSummativeFormat, Assessment: PARCCSummativeJSONFormat}
    UDL_NO_GPG = LZWriter, {AssessmentOutcome: PARCCSummativeFormat, Assessment: PARCCSummativeJSONFormat}, \
                           {'no_gpg': True}

    REPORTING_CSV = CSVWriter, {AssessmentOutcome: PARCCReportingSummativeFormat,
                                Assessment: PARCCReportingTestScoreFormat,
                                PerfLevel: PARCCReportingTestLevelFormat,
                                AssessmentItemOutcome: PARCCReportingSummativeItemScoreFormat,
                                AssessmentItem: PARCCReportingItemPDataFormatCSV}

    REPORTING_DB = DBWriter, {AssessmentOutcome: PARCCReportingSummativeFormat,
                              Assessment: PARCCReportingTestScoreFormat,
                              PerfLevel: PARCCReportingTestLevelFormat,
                              AssessmentItemOutcome: PARCCReportingSummativeItemScoreFormat,
                              AssessmentItem: PARCCReportingItemPDataFormatDB}

    ANALYTICS_DB_INTAKE = DBWriter, {AssessmentOutcome: PARCCReportingSummativeFormat}

    def __init__(self,
                 record_writer_class: type,
                 formats: dict,
                 default_kwargs: dict=None):
        self.record_writer_class = record_writer_class
        self.formats = formats
        self.default_kwargs = default_kwargs if default_kwargs else dict()
