from data_generation.config.assessment import AssessmentSubject, DEFAULT_OVERALL_LEVELS, \
    Claim, Subclaim
from data_generation.config.hierarchy import StateName, ExampleDistrict, ExampleState
from data_generation.config.hierarchy import StateDescription
from data_generation.config.population import Demographics, Grade
from data_generation.util.indexed_enum import NoseSafeEnum
from data_generation.util.stats import UniformPopulationSizeDistribution, GammaPopulationSizeDistribution

DEFAULT_YEARS = range(2014, 2017)
DEFAULT_GRADES = (Grade.G03, Grade.G04, Grade.G05, Grade.G06, Grade.G07, Grade.G08, Grade.G09, Grade.G10, Grade.G11)


DEFAULT_ASMT_LEVELS = {
    asmt: (DEFAULT_OVERALL_LEVELS,
           {claim_definition: claim_definition.default_levels
            for claim_definition in Claim
            if claim_definition.subject == asmt.subject},
           {subclaim_definition: subclaim_definition.default_levels
            for subclaim_definition in Subclaim
            if subclaim_definition.subject == asmt.subject})
    for asmt in AssessmentSubject
}


class Batch(NoseSafeEnum):
    def __init__(self,
                 *state_configs,
                 years: tuple=DEFAULT_YEARS,
                 grades_of_concern: tuple=DEFAULT_GRADES,
                 assessment_levels=DEFAULT_ASMT_LEVELS):
        self.state_configs = state_configs
        self.years = years
        self.grades_of_concern = grades_of_concern
        self.assessment_levels = assessment_levels

    CUSTOM = ()

    PARCC_LARGE_CAT = (ExampleState.PARCC_LARGE_CAT.value,)
    PARCC_LARGE_DOG = (ExampleState.PARCC_LARGE_DOG.value,)

    PARCC_SMALL_CAT = (ExampleState.PARCC_SMALL_CAT.value,)
    PARCC_SMALL_DOG = (ExampleState.PARCC_SMALL_DOG.value,)

    DEVEL_TINY = (ExampleState.DEVEL_TINY.value,)
    TEST_STATES = (ExampleState.TEST_CAT.value,
                   ExampleState.TEST_DOG.value,)
    TEST_DISTRICTS = (ExampleState.TEST_DISTRICTS.value,)
    TEST_COVERAGE = (ExampleState.TEST_COVERAGE.value,)

    UAT = (ExampleState.UAT_STATE_1.value,
           ExampleState.UAT_STATE_2.value,
           ExampleState.UAT_STATE_3.value,
           ExampleState.UAT_STATE_4.value,
           )


_POP_SIZE_DIST_BY_NAME = {
    'uniform': UniformPopulationSizeDistribution,
    'gamma': GammaPopulationSizeDistribution
}


class CustomBatch:
    def __init__(self, args):
        required_districts = [ExampleDistrict[district_name].value for district_name in args.custom_batch_required_districts]
        self.state_configs = tuple(StateDescription(
            state_name=StateName.get_by_value(state_code),
            demographics=Demographics[args.custom_batch_demographics],
            mean_student_pop=args.custom_batch_mean_pop,
            mean_district_count=args.custom_batch_mean_district_count,
            mean_school_count=args.custom_batch_mean_school_count,
            required_districts=required_districts,
            state_pop_dist_type=_POP_SIZE_DIST_BY_NAME[args.custom_batch_distribution],
            state_pop_dist_kwargs=args.custom_batch_distribution_kwargs,
            school_count_dist_type=_POP_SIZE_DIST_BY_NAME[args.custom_batch_distribution],
            school_pop_dist_type=_POP_SIZE_DIST_BY_NAME[args.custom_batch_distribution],
        ) for state_code in args.custom_batch_state_codes)
        self.years = args.custom_batch_year_range
        self.grades_of_concern = [Grade[grade_name] for grade_name in args.custom_batch_grades]
        self.assessment_levels = DEFAULT_ASMT_LEVELS  # TODO: this could be customizable, but it's trouble and there's no motivating requirement
