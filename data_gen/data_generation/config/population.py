"""
Configuration pertaining to student properties and the distributions of those properties

@date: March 4, 2014
"""
from data_generation.config.config import Subject
from data_generation.util.indexed_enum import IndexedEnum, NoseSafeEnum
from data_generation.util.properties import Properties
from data_generation.util.weighted_choice import DiscreteDistribution

DISTRICT_LEVEL_STAFF_COUNT = 10  # ~ many staff people to generate per district

STUDENT_HOLD_BACK_RATE = .02  # 2% of students will be held back in their grade for a new year
STUDENT_DROP_OUT_RATE = .5  # 50% of students drop out if not advanced to the next grade
STUDENT_TRANSFER_RATE = .05  # 5% of students will move to a new school in the district without needing to

DISABLED_IEP_PERC = .95  # how likely a student identified as disabled will have IEP status
DISABLED_OTHER_PERC = .05  #

GIFTED_RATE = .05  # how likely a student is to have the "gifted" accommodation
GIFTED_STATUS_CHANGE_RATE = .001  # how likely a student's gifted status is to change year to year
DISABLED_STATUS_CHANGE_RATE = .001  # how likey a student's disabled status is to change year to year
LEP_STATUS_CHANGE_RATE = .1  # how likely a student's LEP status is to change year to year
ECON_DIS_STATUS_CHANGE_RATE = .05  # how likely a student's economic disadvantage status is to change year to year

FORCE_MF = True  # force picking a gender that's either Male or Female

GENERAL_ACCOMMODATION_RATE = .05  # chance that a student has a generic accommodation
UNLIKELY_ACCOMMODATION_RATE = .2  # chance that a student w/ a particular status has certain possible accommodations
LIKELY_ACCOMMODATION_RATE = .8  # chance that a student w/ a particular status has certain likely accommodations
DISABLED_ACCOMMODATION_RATE = .9  # chance that a disabled student has a relevant accommodation

HOLD_BACK_RATE = .04
DROP_OUT_RATE = .08
TRANSFER_RATE = .05
REPOPULATE_ADDITIONAL_STUDENTS = [0, 0, 0, 1, 1, 2, 3]  # used to add n extra students, where n is randomly chosen from this list


class Sex(NoseSafeEnum):
    MALE = 'Male'
    FEMALE = 'Female'
    OTHER = 'Other'


class Ethnicity(NoseSafeEnum):
    AMERIND = 'American Indian or Alaska Native', '01'
    BLACK = 'Black or African American', '03'
    HISPANIC = 'Hispanic or Latino', '04'
    ASIAN = 'Asian', '02'
    HAWAII = 'Native Hawaiian or Other Pacific Islander', '06'
    WHITE = 'White', '05'
    NONE = 'Not Stated', None
    MULTIPLE = 'Two or more races', '07'

    def __init__(self, label: str, federal_code: str):
        self.label = label
        self.federal_code = federal_code


class StudentAccommodation(NoseSafeEnum):
    IEP = 'Individualized Educational Plan'
    SECTION_504 = '504 Plan'


class StudentStatus(NoseSafeEnum):
    LEP = 'Limited English Proficient'
    ECON_DIS = 'Economically Disadvantaged'
    MIGRANT = 'Migrant Status'


class SexDistribution(DiscreteDistribution):
    name = 'SEX'

    def __init__(self, weights_by_object):
        super().__init__(weights_by_object, 1.0, Sex.OTHER)


class EthnicityDistribution(DiscreteDistribution):
    name = 'ETHNICITY'


class YNDstribution(DiscreteDistribution):
    def __init__(self, key, y_value: float):
        self.name = key.name
        n_value = 1.0 - y_value

        super().__init__({True: y_value, False: n_value})


class LEPTitle3Programs(IndexedEnum):
    DualLanguage = 'DualLanguage'
    TwoWayImmersion = 'TwoWayImmersion'
    TransitionalBilingual = 'TransitionalBilingual'
    DevelopmentalBilingual = 'DevelopmentalBilingual'
    HeritageLanguage = 'HeritageLanguage'
    ShelteredEnglishInstruction = 'ShelteredEnglishInstruction'
    StructuredEnglishImmersion = 'StructuredEnglishImmersion'
    SDAIE = 'SDAIE'
    ContentBasedESL = 'ContentBasedESL'
    PullOutESL = 'PullOutESL'
    Other = 'Other'


class PRGDisabilityType(IndexedEnum):
    AUT = 'Autism'
    DB = 'Deaf-blindness'
    DD = 'Developmental delay'
    EMN = 'Emotional disturbance'
    HI = 'Hearing impairment'
    ID = 'Intellectual disability'
    MD = 'Multiple disabilities'
    OI = 'Orthopedic impairment'
    OHI = 'Other health impairment'
    SLD = 'Specific learning disability'
    SLI = 'Speech or language impairment'
    TBI = 'Traumatic brain injury'
    VI = 'Visual impairment'


class AccommodationColorContrast(IndexedEnum):
    BLACK_CREAM = 'black-cream'
    BLACK_LIGHTBLUE = 'black-lblue'
    BLACK_LIGHTMAGENTA = 'black-lmagenta'
    WHITE_BLACK = 'white-black'
    LIGHTBLUE_DARKBLUE = 'lblue-dblue'
    GRAY_GREEN = 'gray-green'
    COLOR_OVERLAY = 'ColorOverlay'


class AccommodationRead(IndexedEnum):
    HUMAN_SIGNER = 'HumanSigner'
    HUMAN_READ_ALOUD = 'HumanReadAloud'


class AccommodationBraille(IndexedEnum):
    BRAILLE_WRITER = "BrailleWriter"
    BRAILLE_NOTETAKER = 'BrailleNotetaker'


class AccommodationSpeechToText(IndexedEnum):
    SPEECH_TO_TEXT = "SpeechToText"
    HUMAN_SCRIBE = "HumanScribe"
    HUMAN_SIGNER = "HumanSigner"
    EXTERNAL_AT_DEVICE = "ExternalATDevice"


class AccommodationMathRAP(IndexedEnum):
    """
    Probably a bug in the spec, but including it anyway
    """
    SPEECH_TO_TEXT = "SpeechToText"
    HUMAN_SCRIBE = "HumanScribe"


class AccommodationLoudNativeLang(IndexedEnum):
    OralScriptReadbyTestAdministratorARA = 'OralScriptReadbyTestAdministratorARA'
    OralScriptReadbyTestAdministratorCHI = 'OralScriptReadbyTestAdministratorCHI'
    OralScriptReadbyTestAdministratorHAT = 'OralScriptReadbyTestAdministratorHAT'
    OralScriptReadbyTestAdministratorMAH = 'OralScriptReadbyTestAdministratorMAH'
    OralScriptReadbyTestAdministratorNAV = 'OralScriptReadbyTestAdministratorNAV'
    OralScriptReadbyTestAdministratorPOL = 'OralScriptReadbyTestAdministratorPOL'
    OralScriptReadbyTestAdministratorPOR = 'OralScriptReadbyTestAdministratorPOR'
    OralScriptReadbyTestAdministratorSOM = 'OralScriptReadbyTestAdministratorSOM'
    OralScriptReadbyTestAdministratorSPA = 'OralScriptReadbyTestAdministratorSPA'
    OralScriptReadbyTestAdministratorVIE = 'OralScriptReadbyTestAdministratorVIE'
    HumanTranslator = 'HumanTranslator'


class AccommodationExtendedTime(IndexedEnum):
    EL = 'EL'
    IEP504 = 'IEP504'
    BOTH = 'Both'


class AccommodationTranslation(IndexedEnum):
    SPANISH = 'SPA'


class Accommodation(IndexedEnum):
    def __init__(self, label: str, ela: bool, math: bool, values):
        self.label = label
        self.code_by_subject = {Subject.ELA: ela, Subject.Math: math}
        self.values = values

    def __getitem__(self, item):
        return self.code_by_subject[item]

    accomod_freq_breaks = 'accomod_freq_breaks', True, True, bool
    accomod_alt_location = 'accomod_alt_location', True, True, bool
    accomod_small_group = 'accomod_small_group', True, True, bool
    accomod_special_equip = 'accomod_special_equip', True, True, bool
    accomod_spec_area = 'accomod_spec_area', True, True, bool
    accomod_time_day = 'accomod_time_day', True, True, bool
    accomod_answer_mask = 'accomod_answer_mask', True, True, bool
    accomod_color_contrast = 'accomod_color_contrast', True, True, AccommodationColorContrast
    accomod_text_2_speech_math = 'accomod_text_2_speech_math', False, True, bool
    accomod_read_math = 'accomod_read_math', False, True, AccommodationRead
    accomod_asl_video = 'accomod_asl_video', True, True, bool
    accomod_screen_reader = 'accomod_screen_reader', True, True, bool
    accomod_close_capt_ela = 'accomod_close_capt_ela', True, False, bool
    accomod_read_ela = 'accomod_read_ela', True, False, AccommodationRead
    accomod_braille_ela = 'accomod_braille_ela', True, False, bool
    accomod_tactile_graph = 'accomod_tactile_graph', True, True, bool
    accomod_text_2_speech_ela = 'accomod_text_2_speech_ela', True, False, bool
    accomod_answer_rec = 'accomod_answer_rec', True, True, bool
    accomod_braille_resp = 'accomod_braille_resp', True, True, AccommodationBraille
    accomod_calculator = 'accomod_calculator', False, True, bool
    accomod_construct_resp_ela = 'accomod_construct_resp_ela', True, False, AccommodationSpeechToText
    accomod_select_resp_ela = 'accomod_select_resp_ela', True, False, AccommodationSpeechToText
    accomod_math_resp = 'accomod_math_resp', False, True, AccommodationSpeechToText
    accomod_monitor_test_resp = 'accomod_monitor_test_resp', True, True, bool
    accomod_word_predict = 'accomod_word_predict', True, True, bool
    accomod_native_lang = 'accomod_native_lang', True, True, bool
    accomod_loud_native_lang = 'accomod_loud_native_lang', True, True, AccommodationLoudNativeLang
    accomod_math_rsp = 'accomod_math_rsp', False, True, AccommodationMathRAP
    accomod_math_text_2_speech = 'accomod_math_text_2_speech', False, True, AccommodationTranslation
    accomod_math_trans_online = 'accomod_math_trans_online', False, True, AccommodationTranslation
    accomod_w_2_w_dict = 'accomod_w_2_w_dict', True, True, bool
    accomod_extend_time = 'accomod_extend_time', True, True, AccommodationExtendedTime
    accomod_alt_paper_test = 'accomod_alt_paper_test', True, True, bool
    accomod_paper_trans_math = 'accomod_paper_trans_math', False, True, AccommodationTranslation
    accomod_human_read_sign = 'accomod_human_read_sign', True, True, AccommodationRead
    accomod_large_print = 'accomod_large_print', True, True, bool
    accomod_braille_tactile = 'accomod_braille_tactile', True, True, bool


class Grade(IndexedEnum):
    IT = "IT", "Infant/toddler"
    PR = "PR", "Preschool"
    PK = "PK", "Prekindergarten"
    TK = "TK", "Transitional Kindergarten"
    KG = "KG", "Kindergarten"
    G01 = 1, "Grade 1"
    G02 = 2, "Grade 2"
    G03 = 3, "Grade 3"
    G04 = 4, "Grade 4"
    G05 = 5, "Grade 5"
    G06 = 6, "Grade 6"
    G07 = 7, "Grade 7"
    G08 = 8, "Grade 8"
    G09 = 9, "Grade 9"
    G10 = 10, "Grade 10"
    G11 = 11, "Grade 11"
    G12 = 12, "Grade 12"
    G13 = 13, "Grade 13"
    PS = "PS", "Postsecondary"
    UG = "UG", "Ungraded"
    Other = "Other", "Other"
    OutOfSchool = "OutOfSchool", "Out of school"

    def __init__(self, code, label):
        self.code = code
        self.label = label

    def __lt__(self, other):
        return Grade.index(self) < Grade.index(other)

    def __gt__(self, other):
        return Grade.index(self) > Grade.index(other)

    def __le__(self, other):
        return Grade.index(self) <= Grade.index(other)

    def __ge__(self, other):
        return Grade.index(self) >= Grade.index(other)

    def __add__(self, other: int):
        index = list(Grade).index(self) + other
        return Grade[index]

    @property
    def birthyear_offset(self) -> int:
        return Grade.index(self) + 2


class Demographics(NoseSafeEnum):
    CALIFORNIA = {Grade.G01: ({Sex.MALE: 0.5134, Sex.FEMALE: 0.4866},
                              {Ethnicity.AMERIND: 0.0059, Ethnicity.BLACK: 0.0594, Ethnicity.HISPANIC: 0.5356, Ethnicity.ASIAN: 0.0890, Ethnicity.HAWAII: 0.0054, Ethnicity.WHITE: 0.2492, Ethnicity.MULTIPLE: 0.0273, Ethnicity.NONE: 0.0057},
                              {StudentAccommodation.IEP: 0.1100, StudentAccommodation.SECTION_504: 0.0700, StudentStatus.LEP: 0.3010, StudentStatus.ECON_DIS: 0.5300, StudentStatus.MIGRANT: 0.0355}),
                  Grade.G02: ({Sex.MALE: 0.5134, Sex.FEMALE: 0.4866},
                              {Ethnicity.AMERIND: 0.0059, Ethnicity.BLACK: 0.0594, Ethnicity.HISPANIC: 0.5356, Ethnicity.ASIAN: 0.0890, Ethnicity.HAWAII: 0.0054, Ethnicity.WHITE: 0.2492, Ethnicity.MULTIPLE: 0.0273, Ethnicity.NONE: 0.0057},
                              {StudentAccommodation.IEP: 0.1100, StudentAccommodation.SECTION_504: 0.0700, StudentStatus.LEP: 0.3010, StudentStatus.ECON_DIS: 0.5300, StudentStatus.MIGRANT: 0.0355}),
                  Grade.G03: ({Sex.MALE: 0.5134, Sex.FEMALE: 0.4866},
                              {Ethnicity.AMERIND: 0.0059, Ethnicity.BLACK: 0.0594, Ethnicity.HISPANIC: 0.5356, Ethnicity.ASIAN: 0.0890, Ethnicity.HAWAII: 0.0054, Ethnicity.WHITE: 0.2492, Ethnicity.MULTIPLE: 0.0273, Ethnicity.NONE: 0.0057},
                              {StudentAccommodation.IEP: 0.1100, StudentAccommodation.SECTION_504: 0.0700, StudentStatus.LEP: 0.3010, StudentStatus.ECON_DIS: 0.5300, StudentStatus.MIGRANT: 0.0355}),
                  Grade.G04: ({Sex.MALE: 0.5125, Sex.FEMALE: 0.4875},
                              {Ethnicity.AMERIND: 0.0060, Ethnicity.BLACK: 0.0607, Ethnicity.HISPANIC: 0.5314, Ethnicity.ASIAN: 0.0893, Ethnicity.HAWAII: 0.0053, Ethnicity.WHITE: 0.2526, Ethnicity.MULTIPLE: 0.0240, Ethnicity.NONE: 0.0060},
                              {StudentAccommodation.IEP: 0.1100, StudentAccommodation.SECTION_504: 0.0700, StudentStatus.LEP: 0.2530, StudentStatus.ECON_DIS: 0.5300, StudentStatus.MIGRANT: 0.0355}),
                  Grade.G05: ({Sex.MALE: 0.5117, Sex.FEMALE: 0.4883},
                              {Ethnicity.AMERIND: 0.0063, Ethnicity.BLACK: 0.0631, Ethnicity.HISPANIC: 0.5309, Ethnicity.ASIAN: 0.0877, Ethnicity.HAWAII: 0.0055, Ethnicity.WHITE: 0.2529, Ethnicity.MULTIPLE: 0.0227, Ethnicity.NONE: 0.0059},
                              {StudentAccommodation.IEP: 0.1100, StudentAccommodation.SECTION_504: 0.0700, StudentStatus.LEP: 0.2030, StudentStatus.ECON_DIS: 0.5300, StudentStatus.MIGRANT: 0.0355}),
                  Grade.G06: ({Sex.MALE: 0.5126, Sex.FEMALE: 0.4874},
                              {Ethnicity.AMERIND: 0.0062, Ethnicity.BLACK: 0.0649, Ethnicity.HISPANIC: 0.5282, Ethnicity.ASIAN: 0.0872, Ethnicity.HAWAII: 0.0053, Ethnicity.WHITE: 0.2536, Ethnicity.MULTIPLE: 0.0216, Ethnicity.NONE: 0.0069},
                              {StudentAccommodation.IEP: 0.1100, StudentAccommodation.SECTION_504: 0.0700, StudentStatus.LEP: 0.1700, StudentStatus.ECON_DIS: 0.5300, StudentStatus.MIGRANT: 0.0355}),
                  Grade.G07: ({Sex.MALE: 0.5132, Sex.FEMALE: 0.4868},
                              {Ethnicity.AMERIND: 0.0066, Ethnicity.BLACK: 0.0652, Ethnicity.HISPANIC: 0.5228, Ethnicity.ASIAN: 0.0900, Ethnicity.HAWAII: 0.0056, Ethnicity.WHITE: 0.2567, Ethnicity.MULTIPLE: 0.0211, Ethnicity.NONE: 0.0058},
                              {StudentAccommodation.IEP: 0.1100, StudentAccommodation.SECTION_504: 0.0700, StudentStatus.LEP: 0.1490, StudentStatus.ECON_DIS: 0.5300, StudentStatus.MIGRANT: 0.0355}),
                  Grade.G08: ({Sex.MALE: 0.5137, Sex.FEMALE: 0.4863},
                              {Ethnicity.AMERIND: 0.0067, Ethnicity.BLACK: 0.0676, Ethnicity.HISPANIC: 0.5182, Ethnicity.ASIAN: 0.0848, Ethnicity.HAWAII: 0.0056, Ethnicity.WHITE: 0.2635, Ethnicity.MULTIPLE: 0.0205, Ethnicity.NONE: 0.0054},
                              {StudentAccommodation.IEP: 0.1100, StudentAccommodation.SECTION_504: 0.0700, StudentStatus.LEP: 0.1280, StudentStatus.ECON_DIS: 0.5300, StudentStatus.MIGRANT: 0.0355}),
                  Grade.G09: ({Sex.MALE: 0.5112, Sex.FEMALE: 0.4888},
                              {Ethnicity.AMERIND: 0.0074, Ethnicity.BLACK: 0.0670, Ethnicity.HISPANIC: 0.4979, Ethnicity.ASIAN: 0.0908, Ethnicity.HAWAII: 0.0057, Ethnicity.WHITE: 0.2758, Ethnicity.MULTIPLE: 0.0196, Ethnicity.NONE: 0.0071},
                              {StudentAccommodation.IEP: 0.1100, StudentAccommodation.SECTION_504: 0.0700, StudentStatus.LEP: 0.1160, StudentStatus.ECON_DIS: 0.5300, StudentStatus.MIGRANT: 0.0355}),
                  Grade.G10: ({Sex.MALE: 0.5112, Sex.FEMALE: 0.4888},
                              {Ethnicity.AMERIND: 0.0074, Ethnicity.BLACK: 0.0670, Ethnicity.HISPANIC: 0.4979, Ethnicity.ASIAN: 0.0908, Ethnicity.HAWAII: 0.0057, Ethnicity.WHITE: 0.2758, Ethnicity.MULTIPLE: 0.0196, Ethnicity.NONE: 0.0071},
                              {StudentAccommodation.IEP: 0.1100, StudentAccommodation.SECTION_504: 0.0700, StudentStatus.LEP: 0.1160, StudentStatus.ECON_DIS: 0.5300, StudentStatus.MIGRANT: 0.0355}),
                  Grade.G11: ({Sex.MALE: 0.5112, Sex.FEMALE: 0.4888},
                              {Ethnicity.AMERIND: 0.0074, Ethnicity.BLACK: 0.0670, Ethnicity.HISPANIC: 0.4979, Ethnicity.ASIAN: 0.0908, Ethnicity.HAWAII: 0.0057, Ethnicity.WHITE: 0.2758, Ethnicity.MULTIPLE: 0.0196, Ethnicity.NONE: 0.0071},
                              {StudentAccommodation.IEP: 0.1100, StudentAccommodation.SECTION_504: 0.0700, StudentStatus.LEP: 0.1160, StudentStatus.ECON_DIS: 0.5300, StudentStatus.MIGRANT: 0.0355}),
                  Grade.G12: ({Sex.MALE: 0.5112, Sex.FEMALE: 0.4888},
                              {Ethnicity.AMERIND: 0.0074, Ethnicity.BLACK: 0.0670, Ethnicity.HISPANIC: 0.4979, Ethnicity.ASIAN: 0.0908, Ethnicity.HAWAII: 0.0057, Ethnicity.WHITE: 0.2758, Ethnicity.MULTIPLE: 0.0196, Ethnicity.NONE: 0.0071},
                              {StudentAccommodation.IEP: 0.1100, StudentAccommodation.SECTION_504: 0.0700, StudentStatus.LEP: 0.1160, StudentStatus.ECON_DIS: 0.5300, StudentStatus.MIGRANT: 0.0355})}
    TYPICAL1 = {Grade.G01: ({Sex.MALE: 0.5000, Sex.FEMALE: 0.4800},
                            {Ethnicity.AMERIND: 0.0100, Ethnicity.BLACK: 0.1800, Ethnicity.HISPANIC: 0.2300, Ethnicity.ASIAN: 0.0700, Ethnicity.HAWAII: 0.0300, Ethnicity.WHITE: 0.4500, Ethnicity.MULTIPLE: 0.0100, Ethnicity.NONE: 0.0200},
                            {StudentAccommodation.IEP: 0.1500, StudentAccommodation.SECTION_504: 0.0700, StudentStatus.LEP: 0.0900, StudentStatus.ECON_DIS: 0.5700, StudentStatus.MIGRANT: 0.0355}),
                Grade.G02: ({Sex.MALE: 0.5000, Sex.FEMALE: 0.4800},
                            {Ethnicity.AMERIND: 0.0100, Ethnicity.BLACK: 0.1800, Ethnicity.HISPANIC: 0.2300, Ethnicity.ASIAN: 0.0700, Ethnicity.HAWAII: 0.0300, Ethnicity.WHITE: 0.4500, Ethnicity.MULTIPLE: 0.0100, Ethnicity.NONE: 0.0200},
                            {StudentAccommodation.IEP: 0.1500, StudentAccommodation.SECTION_504: 0.0700, StudentStatus.LEP: 0.0900, StudentStatus.ECON_DIS: 0.5700, StudentStatus.MIGRANT: 0.0355}),
                Grade.G03: ({Sex.MALE: 0.5000, Sex.FEMALE: 0.4800},
                            {Ethnicity.AMERIND: 0.0100, Ethnicity.BLACK: 0.1800, Ethnicity.HISPANIC: 0.2300, Ethnicity.ASIAN: 0.0700, Ethnicity.HAWAII: 0.0300, Ethnicity.WHITE: 0.4500, Ethnicity.MULTIPLE: 0.0100, Ethnicity.NONE: 0.0200},
                            {StudentAccommodation.IEP: 0.1500, StudentAccommodation.SECTION_504: 0.0700, StudentStatus.LEP: 0.0900, StudentStatus.ECON_DIS: 0.5700, StudentStatus.MIGRANT: 0.0355}),
                Grade.G04: ({Sex.MALE: 0.5000, Sex.FEMALE: 0.4900},
                            {Ethnicity.AMERIND: 0.0100, Ethnicity.BLACK: 0.1700, Ethnicity.HISPANIC: 0.2400, Ethnicity.ASIAN: 0.0700, Ethnicity.HAWAII: 0.0200, Ethnicity.WHITE: 0.4600, Ethnicity.MULTIPLE: 0.0100, Ethnicity.NONE: 0.0200},
                            {StudentAccommodation.IEP: 0.1500, StudentAccommodation.SECTION_504: 0.0700, StudentStatus.LEP: 0.0900, StudentStatus.ECON_DIS: 0.5700, StudentStatus.MIGRANT: 0.0355}),
                Grade.G05: ({Sex.MALE: 0.5000, Sex.FEMALE: 0.4900},
                            {Ethnicity.AMERIND: 0.0000, Ethnicity.BLACK: 0.1900, Ethnicity.HISPANIC: 0.2000, Ethnicity.ASIAN: 0.0800, Ethnicity.HAWAII: 0.0200, Ethnicity.WHITE: 0.4800, Ethnicity.MULTIPLE: 0.0100, Ethnicity.NONE: 0.0200},
                            {StudentAccommodation.IEP: 0.1600, StudentAccommodation.SECTION_504: 0.0800, StudentStatus.LEP: 0.0800, StudentStatus.ECON_DIS: 0.5600, StudentStatus.MIGRANT: 0.0355}),
                Grade.G06: ({Sex.MALE: 0.5000, Sex.FEMALE: 0.4900},
                            {Ethnicity.AMERIND: 0.0000, Ethnicity.BLACK: 0.1900, Ethnicity.HISPANIC: 0.2000, Ethnicity.ASIAN: 0.0900, Ethnicity.HAWAII: 0.0200, Ethnicity.WHITE: 0.4700, Ethnicity.MULTIPLE: 0.0100, Ethnicity.NONE: 0.0200},
                            {StudentAccommodation.IEP: 0.1600, StudentAccommodation.SECTION_504: 0.0800, StudentStatus.LEP: 0.0600, StudentStatus.ECON_DIS: 0.5400, StudentStatus.MIGRANT: 0.0355}),
                Grade.G07: ({Sex.MALE: 0.5100, Sex.FEMALE: 0.4800},
                            {Ethnicity.AMERIND: 0.0100, Ethnicity.BLACK: 0.1900, Ethnicity.HISPANIC: 0.2000, Ethnicity.ASIAN: 0.0800, Ethnicity.HAWAII: 0.0100, Ethnicity.WHITE: 0.4800, Ethnicity.MULTIPLE: 0.0100, Ethnicity.NONE: 0.0200},
                            {StudentAccommodation.IEP: 0.1600, StudentAccommodation.SECTION_504: 0.0800, StudentStatus.LEP: 0.0600, StudentStatus.ECON_DIS: 0.5300, StudentStatus.MIGRANT: 0.0355}),
                Grade.G08: ({Sex.MALE: 0.5000, Sex.FEMALE: 0.4900},
                            {Ethnicity.AMERIND: 0.0100, Ethnicity.BLACK: 0.1800, Ethnicity.HISPANIC: 0.2000, Ethnicity.ASIAN: 0.0800, Ethnicity.HAWAII: 0.0200, Ethnicity.WHITE: 0.4800, Ethnicity.MULTIPLE: 0.0100, Ethnicity.NONE: 0.0200},
                            {StudentAccommodation.IEP: 0.1600, StudentAccommodation.SECTION_504: 0.0800, StudentStatus.LEP: 0.0600, StudentStatus.ECON_DIS: 0.5200, StudentStatus.MIGRANT: 0.0355}),
                Grade.G09: ({Sex.MALE: 0.5000, Sex.FEMALE: 0.4800},
                            {Ethnicity.AMERIND: 0.0100, Ethnicity.BLACK: 0.1800, Ethnicity.HISPANIC: 0.2300, Ethnicity.ASIAN: 0.0700, Ethnicity.HAWAII: 0.0300, Ethnicity.WHITE: 0.4500, Ethnicity.MULTIPLE: 0.0100, Ethnicity.NONE: 0.0200},
                            {StudentAccommodation.IEP: 0.1500, StudentAccommodation.SECTION_504: 0.0700, StudentStatus.LEP: 0.0900, StudentStatus.ECON_DIS: 0.5700, StudentStatus.MIGRANT: 0.0355}),
                Grade.G10: ({Sex.MALE: 0.5000, Sex.FEMALE: 0.4800},
                            {Ethnicity.AMERIND: 0.0100, Ethnicity.BLACK: 0.1800, Ethnicity.HISPANIC: 0.2300, Ethnicity.ASIAN: 0.0700, Ethnicity.HAWAII: 0.0300, Ethnicity.WHITE: 0.4500, Ethnicity.MULTIPLE: 0.0100, Ethnicity.NONE: 0.0200},
                            {StudentAccommodation.IEP: 0.1500, StudentAccommodation.SECTION_504: 0.0700, StudentStatus.LEP: 0.0900, StudentStatus.ECON_DIS: 0.5700, StudentStatus.MIGRANT: 0.0355}),
                Grade.G11: ({Sex.MALE: 0.5100, Sex.FEMALE: 0.4700},
                            {Ethnicity.AMERIND: 0.0100, Ethnicity.BLACK: 0.1900, Ethnicity.HISPANIC: 0.2000, Ethnicity.ASIAN: 0.0800, Ethnicity.HAWAII: 0.0200, Ethnicity.WHITE: 0.4700, Ethnicity.MULTIPLE: 0.0100, Ethnicity.NONE: 0.0200},
                            {StudentAccommodation.IEP: 0.1600, StudentAccommodation.SECTION_504: 0.0800, StudentStatus.LEP: 0.0600, StudentStatus.ECON_DIS: 0.5200, StudentStatus.MIGRANT: 0.0355}),
                Grade.G12: ({Sex.MALE: 0.5100, Sex.FEMALE: 0.4700},
                            {Ethnicity.AMERIND: 0.0100, Ethnicity.BLACK: 0.1900, Ethnicity.HISPANIC: 0.2000, Ethnicity.ASIAN: 0.0800, Ethnicity.HAWAII: 0.0200, Ethnicity.WHITE: 0.4700, Ethnicity.MULTIPLE: 0.0100, Ethnicity.NONE: 0.0200},
                            {StudentAccommodation.IEP: 0.1600, StudentAccommodation.SECTION_504: 0.0800, StudentStatus.LEP: 0.0600, StudentStatus.ECON_DIS: 0.5200, StudentStatus.MIGRANT: 0.0355})}

    def __init__(self, stats_by_grade):
        new_stats_by_grade = {}

        for grade, (sex_stats, eth_stats, other_stats) in stats_by_grade.items():
            new_stats_by_grade[grade] = Properties({Sex: SexDistribution(sex_stats),
                                                    Ethnicity: EthnicityDistribution(eth_stats)})

            for key, y_value in other_stats.items():
                new_stats_by_grade[grade][key] = YNDstribution(key, y_value)

        self.stats_by_grade = new_stats_by_grade

    def __getitem__(self, grade):
        return self.stats_by_grade[grade]
