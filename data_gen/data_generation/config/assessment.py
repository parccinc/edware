from data_generation.config.config import Subject
from data_generation.config.population import Grade
from data_generation.util.indexed_enum import IndexedEnum, NoseSafeEnum
from data_generation.util.perf_levels import PerfLevels

ASMT_ITEM_COUNT_RANGE = range(10, 21)  # range of item counts per assessment
ASMT_SUBCLAIM_ITEM_RANGE = range(1, 5)  # range of item counts in a subclaim
ASMT_ITEM_MAX_SCORE_RANGE = range(5, 13)  # range of possible values for an item's maximum score (minimum always 0)

FORMS_PER_ASSESSMENT_COMPONENT = 3  # how many form ids to have per assessment
TEST_SCHOOL_IS_RESP_SCHOOL_RATE = .9  # how likely a an eoy or pba test school is to be the student's actual school (otherwise, chosen randomly from disrict)

GENERIC_CSEM_RANGE = range(1, 5)  # CSEM is randomly chosen from this range
COMPONENT_NOT_ASSIGNED_RATE = .01  # how likely a student is to not have a PBA or EOY component in a particular outcome
COMPONENT_NOT_TESTED_RATE = .01  # how likely a student is to not have tested at all in a particular outcome
COMPONENT_VOIDED_RATE = .01  # how likely a student's results are to be voided for a particular outcome
MIN_ATTEMPTED_PERCENT = .02  # if student hasn't completed this number of items, not considered to have attempted the test
ATTEMPTED_PERCENT_RANGE = .2  # used to bound a reasonable amount of attempted items given a score
SUMMATIVE_SUPPRESSED_RATE = .01  # how likely a student's sumamative score is to be suppressed

NO_COMPONENT_NO_STAFF_RATE = .95  # how likely a type 03 assessment outcome record is to have no associated staff ID
COMPONENT_NO_STAFF_RATE = .02  # how likely a type 01 or 02 assessment outcome record is to have no associated staff ID


# NOTE: in reality, grades for HS math tests are (Grade.G07, Grade.G08, Grade.G09, Grade.G10, Grade.G11)
#       but we don't simulate it in that much detail. see HS_MATH_PATHS below.
class AssessmentSubject(NoseSafeEnum):
    ELA03 = 'English Language Arts/Literacy', Subject.ELA, (Grade.G03,)
    ELA04 = 'English Language Arts/Literacy', Subject.ELA, (Grade.G04,)
    ELA05 = 'English Language Arts/Literacy', Subject.ELA, (Grade.G05,)
    ELA06 = 'English Language Arts/Literacy', Subject.ELA, (Grade.G06,)
    ELA07 = 'English Language Arts/Literacy', Subject.ELA, (Grade.G07,)
    ELA08 = 'English Language Arts/Literacy', Subject.ELA, (Grade.G08,)
    ELA09 = 'English Language Arts/Literacy', Subject.ELA, (Grade.G09,)
    ELA10 = 'English Language Arts/Literacy', Subject.ELA, (Grade.G10,)
    ELA11 = 'English Language Arts/Literacy', Subject.ELA, (Grade.G11,)
    MAT03 = 'Mathematics', Subject.Math, (Grade.G03,)
    MAT04 = 'Mathematics', Subject.Math, (Grade.G04,)
    MAT05 = 'Mathematics', Subject.Math, (Grade.G05,)
    MAT06 = 'Mathematics', Subject.Math, (Grade.G06,)
    MAT07 = 'Mathematics', Subject.Math, (Grade.G07,)
    MAT08 = 'Mathematics', Subject.Math, (Grade.G08,)
    ALG01 = 'Algebra I', Subject.Math, (Grade.G09,), True
    GEO01 = 'Geometry', Subject.Math, (Grade.G10,), True
    ALG02 = 'Algebra II', Subject.Math, (Grade.G11,), True
    MAT1I = 'Integrated Mathematics I', Subject.Math, (Grade.G09,), True
    MAT2I = 'Integrated Mathematics II', Subject.Math, (Grade.G10,), True
    MAT3I = 'Integrated Mathematics III', Subject.Math, (Grade.G11,), True

    def __init__(self, label: str, subject: Subject, grades: tuple, is_hs_math: bool=False):
        """

        :param label:
        :param subject:
        :param grades: a tuple of valid grades
        """
        self.label = label
        self.subject = subject
        self.grades = grades
        self.is_hs_math = is_hs_math

    @classmethod
    def assessments_by_subject_grade(cls, subject: Subject, grade: int):
        return tuple(asmt_subj
                     for asmt_subj in cls
                     if asmt_subj.subject == subject and grade in asmt_subj.grades)

# There are two HS math paths.
# It's possible for one school to offer both paths, but we don't deal w/ this currently.
# It's also possible for students to take ALG01 or MAT1I as early as 7th grade (and GEO01 and MAT2I in 8th) in lieu of
# the normal test for that grade. We also don't deal w/ that. We're just going to randomly choose a HS math path for
# a given school, and every student at that grade level will take the specified test.
HS_MATH_PATHS = ({Grade.G09: AssessmentSubject.ALG01, Grade.G10: AssessmentSubject.GEO01, Grade.G11: AssessmentSubject.ALG02},
                 {Grade.G09: AssessmentSubject.MAT1I, Grade.G10: AssessmentSubject.MAT2I, Grade.G11: AssessmentSubject.MAT3I})


class TestPeriod(NoseSafeEnum):
    SPRING = 'Spring', .95
    # TODO Uncomment when fix issue with repeated rows in assessment items
    # We can't have more than 1 period for now. Otherwise we'll have repeated data
    # in the table rpt_item_p_data
    # FALL_BLOCK = 'FallBlock', .05

    def __init__(self, label: str, weight: float):
        self.label = label
        self.weight = weight

    @classmethod
    def weight_by_period(cls):
        """
        How likely a test is to fall in a particular period
        :return:
        """
        return {period: period.weight for period in cls}


class Component(NoseSafeEnum):
    EOY = "End-of-Year", .5
    PBA = 'Performance-Based Assessment', .5

    def __init__(self, label, weight):
        self.label = label
        self.weight = weight

    @classmethod
    def weights(cls):
        """
        relative weight of each component in the overall score
        :return:
        """
        return [component.weight for component in cls]


class RecordType(NoseSafeEnum):
    SUMMATIVE = '01'
    COMPONENT = '02'
    NO_COMPONENT = '03'


class ReportSuppressionCode(IndexedEnum):
    NONE = None
    REASON_01 = '01'
    REASON_02 = '02'
    REASON_03 = '03'
    REASON_04 = '04'
    REASON_05 = '05'
    REASON_06 = '06'
    REASON_07 = '07'
    REASON_08 = '08'
    REASON_09 = '09'
    REASON_10 = '10'


class ReportSuppressionAction(IndexedEnum):
    NONE = None
    NO_STUDENT_REPORT_AND_NOT_AGGREGATED = '01'
    STUDENT_REPORT_WITH_SCORES_BUT_NOT_AGGREGATED = '02'
    STUDENT_REPORT_WITH_NON_SCORES_BUT_NOT_AGGREGATED = '03'
    NO_STUDENT_REPORT_BUT_IS_AGGREGATED = '04'
    ISR_BUT_NOT_AGGREGATED = '05'
    ISR_SCHOOL_AGGREGATE = '06'


class ComponentAttemptCategory(NoseSafeEnum):
    """category: Identifies PBA category for type 03 records.
    A = Test Attemptedness flag is blank for both PBA and EOY components test attempts
    B = Test attempts in both PBA and EOY components are Y for Voided PBA/EOY Score Code field
    C = Test attempts in either PBA and EOY components are Y for Voided PBA/EOY Score Code field
    and there are no test assignments or test attempts in the other component
    D = Test attempts in either PBA and EOY components have the Test Attemptedness flag as blank and there are
    no test assignments or test attempts in the other component
    E = Test Assignments exist in both PBA and EOY components
    F = Test Assignment exist in one component (either PBA or EOY) and the other component has a test attempt
    that did not meet attemptedness rules
    G = Test Assignment exist in one component (either PBA or EOY) and the other component has a test attempt
    that has a Y for Voided PBA/EOY Score Code field
    H = Test Assignment exist in one component (either PBA or EOY) and no test assignment or test attempt is
    present in the other component"""
    A = 'A'
    B = 'B'
    C = 'C'
    D = 'D'
    E = 'E'
    F = 'F'
    G = 'G'
    H = 'H'
    UNKNOWN = 'I'  # TODO: this was a stopgap, not sure if it's needed, it's technically wrong


DEFAULT_OVERALL_LEVELS = PerfLevels(range(1, 502, 100),
                                    ('minimal', 'partial', 'adequate', 'strong', 'distinguished'))

DEFAULT_MATH_SUBCLAIM_LEVELS = PerfLevels(range(1, 62, 20),
                                          ('Below Standard', 'At/Near Standard', 'Above Standard'))
DEFAULT_READING_CLAIM_LEVELS = PerfLevels((1, 17, 34, 51),
                                          ('Below Standard', 'At/Near Standard', 'Above Standard'))
DEFAULT_WRITING_CLAIM_LEVELS = PerfLevels(range(1, 62, 20),
                                          ('Below Standard', 'At/Near Standard', 'Above Standard'))


class Claim(NoseSafeEnum):
    READING = Subject.ELA, 'Reading', .5, DEFAULT_READING_CLAIM_LEVELS
    WRITING = Subject.ELA, 'Writing', .5, DEFAULT_WRITING_CLAIM_LEVELS

    def __init__(self, subject: Subject, label: str, weight: float, default_levels):
        self.subject = subject
        self.label = label
        self.weight = weight
        self.default_levels = default_levels

    @classmethod
    def weights(cls, subject: Subject):
        """
        relative weight of each claim / subclaim in the overall score
        :return:
        """
        return {claim: claim.weight for claim in cls if claim.subject == subject}


class Subclaim(NoseSafeEnum):
    MAJOR_CONTENT = Subject.Math, None, 'Major Content', .2, DEFAULT_MATH_SUBCLAIM_LEVELS
    MATHEMATICAL_REASONING = Subject.Math, None, 'Mathematical Reasoning', .2, DEFAULT_MATH_SUBCLAIM_LEVELS
    MATHEMATICAL_FLUENCY = Subject.Math, None, 'Mathematical Fluency', .2, DEFAULT_MATH_SUBCLAIM_LEVELS
    MODELING_PRACTICE = Subject.Math, None, 'Modeling Practice', .2, DEFAULT_MATH_SUBCLAIM_LEVELS
    ADDITIONAL_CONTENT = Subject.Math, None, 'Additional and Supporting Content', .2, DEFAULT_MATH_SUBCLAIM_LEVELS
    READING_RL = Subject.ELA, Claim.READING, 'Reading-RL', 1 / 3, DEFAULT_READING_CLAIM_LEVELS
    READING_RI = Subject.ELA, Claim.READING, 'Reading-RI', 1 / 3, DEFAULT_READING_CLAIM_LEVELS
    READING_RV = Subject.ELA, Claim.READING, 'Reading-RV', 1 / 3, DEFAULT_READING_CLAIM_LEVELS
    WRITING_WE = Subject.ELA, Claim.WRITING, 'Writing-WE', .5, DEFAULT_WRITING_CLAIM_LEVELS
    WRITING_WKL = Subject.ELA, Claim.WRITING, 'Writing-WKL', .5, DEFAULT_WRITING_CLAIM_LEVELS

    def __init__(self, subject: Subject, claim: Claim, label: str, weight: float, default_levels):
        self.subject = subject
        self.claim = claim
        self.label = label
        self.weight = weight
        self.default_levels = default_levels

    @classmethod
    def weights(cls, subject: Subject, claim: Claim=None):
        """
        relative weight of each subclaim in the overall score
        :return:
        """
        return {subclaim: subclaim.weight for subclaim in cls if subclaim.subject == subject and subclaim.claim == claim}

ELA_CLAIMS = (Claim.READING, Claim.WRITING)
MATH_SUBCLAIMS = (Subclaim.MAJOR_CONTENT, Subclaim.MATHEMATICAL_REASONING, Subclaim.MATHEMATICAL_FLUENCY, Subclaim.MODELING_PRACTICE, Subclaim.ADDITIONAL_CONTENT)
ELA_SUBCLAIMS = (Subclaim.READING_RL, Subclaim.READING_RI, Subclaim.READING_RV, Subclaim.WRITING_WE, Subclaim.WRITING_WKL)
ELA_READING_SUBCLAIMS = (Subclaim.READING_RL, Subclaim.READING_RI, Subclaim.READING_RV)
ELA_WRITING_SUBCLAIMS = (Subclaim.WRITING_WE, Subclaim.WRITING_WKL)

SUBCLAIMS_BY_SUBJECT = {Subject.ELA: ELA_SUBCLAIMS, Subject.Math: MATH_SUBCLAIMS}
SUBCLAIMS_BY_CLAIM = {Claim.READING: ELA_READING_SUBCLAIMS, Claim.WRITING: ELA_WRITING_SUBCLAIMS}


class ItemType(IndexedEnum):
    TEI = 'TEI'
    SELECTED_RESPONSE = 'selected response'
    CONSTRUCTED_RESPONSE = 'constructed response'


class ItemEvidenceStatement(IndexedEnum):
    """
    This values grabbed from test file, will be changed when we'll have relevant data
    """
    A = "RI 3.1.1: Provides questions and answers that show understanding of a text"
    B = "L 3.4.1: Demonstrates the ability to determine the meaning of words and phrases` using sentence-level context as a clue to the meaning of a word or phrase"
    C = "L 4.4.1: Demonstrates the ability to determine the meaning of words and phrases` using context (e.g.` definitions` examples` or restatements in text)"
    D = "3.G.2: Partition shapes into parts with equal areas. Express the area of each part as a unit fraction of the whole"
    E = "5.NF.3-1: Interpret a fraction as division of the numerator by the denominator (a/b = a &#247; b)"
    F = "6.Int.1: Solve two-step word problems requiring operations on multi-digit whole numbers or decimals"
    G = "7.NS.2c: Apply and extend previous understandings of multiplication and division and of fractions to multiply and divide rational numbers"


class ItemStandard(IndexedEnum):
    """
    This values grabbed from test file, will be changed when we'll have relevant data
    """
    RI1 = 'RI.3.01'
    RI2 = 'L.3.04'
    RI3 = 'L.4.04'
    RI4 = '3.G.A.02'
    RI5 = '5.NF.B.03'
    RI6 = '6.NS.A.01'
