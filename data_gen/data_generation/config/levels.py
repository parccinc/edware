"""
Performance level distributions, by subject and grade.
"""
from data_generation.util.assessment_stats import GradeLevels, DemographicLevels, Stats
from data_generation.config.config import Subject
from data_generation.config.population import StudentAccommodation, StudentStatus, Sex, Ethnicity, Grade

LEVELS_BY_GRADE_BY_SUBJ = {
    Subject.Math: {
        Grade.G01: GradeLevels((11.20, 20.80, 31.60, 30.80, 5.60),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(36.00, 31.20, 21.60, 10.40, 0.80),
                                    False: Stats(9.04, 19.89, 32.47, 32.57, 6.02)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(36.00, 31.20, 21.60, 10.40, 0.80),
                                        False: Stats(6.82, 18.96, 33.36, 34.40, 6.45)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(16.00, 26.80, 30.80, 24.00, 2.40),
                                        False: Stats(5.09, 13.16, 32.62, 39.46, 9.67)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(30.40, 33.40, 24.80, 11.40, 0.00),
                                        False: Stats(9.30, 19.55, 32.27, 32.72, 6.15)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(8.80, 19.60, 32.40, 32.80, 6.40),
                                        Sex.MALE: Stats(12.80, 23.00, 31.60, 28.60, 4.00),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(7.20, 20.40, 31.20, 30.80, 10.40),
                                        Ethnicity.AMERIND: Stats(14.40, 25.20, 31.20, 26.00, 3.20),
                                        Ethnicity.ASIAN: Stats(6.40, 14.80, 31.60, 36.80, 10.40),
                                        Ethnicity.BLACK: Stats(16.80, 28.20, 30.80, 22.60, 1.60),
                                        Ethnicity.HISPANIC: Stats(16.00, 27.40, 30.80, 23.40, 2.40),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(0.00, 0.00, 0.00, 0.00, 0.00),
                                        Ethnicity.WHITE: Stats(7.20, 16.80, 32.80, 36.00, 7.20)})}),
        Grade.G02: GradeLevels((11.20, 20.80, 31.60, 30.80, 5.60),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(36.00, 31.20, 21.60, 10.40, 0.80),
                                    False: Stats(9.04, 19.89, 32.47, 32.57, 6.02)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(36.00, 31.20, 21.60, 10.40, 0.80),
                                        False: Stats(6.82, 18.96, 33.36, 34.40, 6.45)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(16.00, 26.80, 30.80, 24.00, 2.40),
                                        False: Stats(5.09, 13.16, 32.62, 39.46, 9.67)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(30.40, 33.40, 24.80, 11.40, 0.00),
                                        False: Stats(9.30, 19.55, 32.27, 32.72, 6.15)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(8.80, 19.60, 32.40, 32.80, 6.40),
                                        Sex.MALE: Stats(12.80, 23.00, 31.60, 28.60, 4.00),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(7.20, 20.40, 31.20, 30.80, 10.40),
                                        Ethnicity.AMERIND: Stats(14.40, 25.20, 31.20, 26.00, 3.20),
                                        Ethnicity.ASIAN: Stats(6.40, 14.80, 31.60, 36.80, 10.40),
                                        Ethnicity.BLACK: Stats(16.80, 28.20, 30.80, 22.60, 1.60),
                                        Ethnicity.HISPANIC: Stats(16.00, 27.40, 30.80, 23.40, 2.40),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(0.00, 0.00, 0.00, 0.00, 0.00),
                                        Ethnicity.WHITE: Stats(7.20, 16.80, 32.80, 36.00, 7.20)})}),
        Grade.G03: GradeLevels((11.20, 20.80, 31.60, 30.80, 5.60),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(36.00, 31.20, 21.60, 10.40, 0.80),
                                    False: Stats(9.04, 19.89, 32.47, 32.57, 6.02)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(36.00, 31.20, 21.60, 10.40, 0.80),
                                        False: Stats(6.82, 18.96, 33.36, 34.40, 6.45)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(16.00, 26.80, 30.80, 24.00, 2.40),
                                        False: Stats(5.09, 13.16, 32.62, 39.46, 9.67)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(30.40, 33.40, 24.80, 11.40, 0.00),
                                        False: Stats(9.30, 19.55, 32.27, 32.72, 6.15)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(8.80, 19.60, 32.40, 32.80, 6.40),
                                        Sex.MALE: Stats(12.80, 23.00, 31.60, 28.60, 4.00),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(7.20, 20.40, 31.20, 30.80, 10.40),
                                        Ethnicity.AMERIND: Stats(14.40, 25.20, 31.20, 26.00, 3.20),
                                        Ethnicity.ASIAN: Stats(6.40, 14.80, 31.60, 36.80, 10.40),
                                        Ethnicity.BLACK: Stats(16.80, 28.20, 30.80, 22.60, 1.60),
                                        Ethnicity.HISPANIC: Stats(16.00, 27.40, 30.80, 23.40, 2.40),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(0.00, 0.00, 0.00, 0.00, 0.00),
                                        Ethnicity.WHITE: Stats(7.20, 16.80, 32.80, 36.00, 7.20)})}),
        Grade.G04: GradeLevels((7.20, 21.00, 34.40, 33.40, 4.00),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(28.00, 34.00, 26.00, 12.00, 0.00),
                                    False: Stats(5.39, 19.87, 35.13, 35.26, 4.34)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(28.00, 34.00, 26.00, 12.00, 0.00),
                                        False: Stats(3.24, 18.52, 36.00, 37.48, 4.76)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(10.40, 27.20, 34.00, 26.80, 1.60),
                                        False: Stats(3.13, 13.11, 34.91, 41.80, 7.06)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(24.00, 37.20, 28.00, 10.80, 0.00),
                                        False: Stats(5.74, 19.59, 34.96, 35.36, 4.34)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(5.60, 18.80, 34.80, 36.00, 4.80),
                                        Sex.MALE: Stats(9.60, 22.20, 34.00, 31.80, 2.40),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(4.00, 17.80, 26.00, 28.20, 24.00),
                                        Ethnicity.AMERIND: Stats(10.40, 26.00, 34.00, 28.00, 1.60),
                                        Ethnicity.ASIAN: Stats(4.80, 13.20, 33.60, 40.40, 8.00),
                                        Ethnicity.BLACK: Stats(11.20, 28.60, 33.60, 25.00, 1.60),
                                        Ethnicity.HISPANIC: Stats(10.40, 27.80, 34.00, 26.20, 1.60),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(1.60, 9.40, 27.20, 37.80, 24.00),
                                        Ethnicity.WHITE: Stats(4.80, 15.60, 35.20, 39.60, 4.80)})}),
        Grade.G05: GradeLevels((8.80, 20.80, 33.60, 32.80, 4.00),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(31.20, 34.20, 24.40, 10.20, 0.00),
                                    False: Stats(6.86, 19.64, 34.40, 34.76, 4.34)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(31.20, 34.20, 24.40, 10.20, 0.00),
                                        False: Stats(4.54, 18.25, 35.35, 37.11, 4.76)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(12.00, 27.00, 33.20, 26.20, 1.60),
                                        False: Stats(4.89, 13.22, 34.09, 40.87, 6.94)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(31.20, 36.60, 24.40, 7.80, 0.00),
                                        False: Stats(7.11, 19.61, 34.29, 34.68, 4.30)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(6.40, 19.60, 34.40, 34.80, 4.80),
                                        Sex.MALE: Stats(10.40, 22.40, 33.60, 31.20, 2.40),
                                        Sex.OTHER: Stats(0.00, 0.00, 0.00, 0.00, 0.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(7.20, 19.20, 25.60, 26.40, 21.60),
                                        Ethnicity.AMERIND: Stats(12.00, 28.20, 32.80, 24.60, 2.40),
                                        Ethnicity.ASIAN: Stats(5.60, 13.40, 33.20, 39.80, 8.00),
                                        Ethnicity.BLACK: Stats(13.60, 29.20, 32.40, 23.20, 1.60),
                                        Ethnicity.HISPANIC: Stats(12.00, 27.00, 33.20, 26.20, 1.60),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(1.60, 9.40, 27.20, 37.80, 24.00),
                                        Ethnicity.WHITE: Stats(5.60, 16.40, 34.80, 38.40, 4.80)})}),
        Grade.G06: GradeLevels((8.80, 22.00, 34.80, 32.80, 1.60),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(30.40, 35.80, 24.80, 9.00, 0.00),
                                    False: Stats(6.92, 20.80, 35.67, 34.87, 1.74)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(30.40, 35.80, 24.80, 9.00, 0.00),
                                        False: Stats(4.69, 19.37, 36.70, 37.33, 1.90)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(12.80, 29.00, 33.20, 24.20, 0.80),
                                        False: Stats(4.10, 13.78, 36.68, 42.89, 2.54)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(40.80, 36.60, 19.60, 3.00, 0.00),
                                        False: Stats(6.76, 21.07, 35.77, 34.70, 1.70)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(6.40, 20.20, 35.60, 35.40, 2.40),
                                        Sex.MALE: Stats(10.40, 24.20, 34.00, 29.80, 1.60),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(5.60, 19.40, 24.80, 25.40, 24.80),
                                        Ethnicity.AMERIND: Stats(0.00, 0.00, 0.00, 0.00, 0.00),
                                        Ethnicity.ASIAN: Stats(6.40, 15.40, 34.80, 39.40, 4.00),
                                        Ethnicity.BLACK: Stats(12.80, 31.40, 33.20, 21.80, 0.80),
                                        Ethnicity.HISPANIC: Stats(13.60, 29.80, 32.80, 23.00, 0.80),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(1.60, 9.40, 27.20, 37.80, 24.00),
                                        Ethnicity.WHITE: Stats(4.80, 16.80, 36.00, 39.20, 3.20)})}),
        Grade.G07: GradeLevels((6.40, 25.60, 35.20, 29.60, 3.20),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(26.40, 39.60, 26.80, 7.20, 0.00),
                                    False: Stats(4.66, 24.39, 35.93, 31.55, 3.48)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(26.40, 39.60, 26.80, 7.20, 0.00),
                                        False: Stats(2.59, 22.93, 36.80, 33.87, 3.81)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(10.40, 32.60, 34.40, 21.80, 0.80),
                                        False: Stats(1.89, 17.70, 36.10, 38.39, 5.90)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(33.60, 40.80, 23.20, 2.40, 0.00),
                                        False: Stats(4.97, 24.80, 35.83, 31.03, 3.37)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(4.80, 22.80, 35.60, 32.80, 4.00),
                                        Sex.MALE: Stats(8.80, 27.40, 34.40, 27.00, 2.40),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(5.60, 18.20, 24.80, 26.60, 24.80),
                                        Ethnicity.AMERIND: Stats(8.80, 32.20, 35.20, 23.00, 0.80),
                                        Ethnicity.ASIAN: Stats(4.80, 16.80, 34.40, 37.60, 6.40),
                                        Ethnicity.BLACK: Stats(10.40, 34.40, 34.40, 20.00, 0.80),
                                        Ethnicity.HISPANIC: Stats(10.40, 33.20, 34.40, 21.20, 0.80),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(1.60, 9.40, 27.20, 37.80, 24.00),
                                        Ethnicity.WHITE: Stats(4.00, 19.60, 36.00, 36.40, 4.00)})}),
        Grade.G08: GradeLevels((5.60, 27.20, 36.40, 29.20, 1.60),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(23.20, 41.80, 28.40, 6.60, 0.00),
                                    False: Stats(4.07, 25.93, 37.10, 31.17, 1.74)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(23.20, 41.80, 28.40, 6.60, 0.00),
                                        False: Stats(2.25, 24.42, 37.92, 33.51, 1.90)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(8.80, 34.60, 35.20, 20.60, 0.80),
                                        False: Stats(2.14, 19.18, 37.70, 38.52, 2.46)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(34.40, 41.00, 22.80, 1.80, 0.00),
                                        False: Stats(4.09, 26.47, 37.12, 30.64, 1.69)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(4.00, 24.40, 36.80, 32.40, 2.40),
                                        Sex.MALE: Stats(7.20, 29.40, 36.00, 26.60, 0.80),
                                        Sex.OTHER: Stats(0.00, 0.00, 0.00, 0.00, 0.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(6.40, 19.60, 28.80, 29.20, 16.00),
                                        Ethnicity.AMERIND: Stats(8.00, 33.20, 35.60, 22.40, 0.80),
                                        Ethnicity.ASIAN: Stats(5.60, 18.20, 35.60, 37.40, 3.20),
                                        Ethnicity.BLACK: Stats(8.80, 37.60, 35.60, 18.00, 0.00),
                                        Ethnicity.HISPANIC: Stats(9.60, 35.40, 35.20, 19.80, 0.00),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(1.60, 9.40, 27.20, 37.80, 24.00),
                                        Ethnicity.WHITE: Stats(3.20, 20.60, 37.20, 36.60, 2.40)})}),
        Grade.G09: GradeLevels((11.20, 20.80, 31.60, 30.80, 5.60),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(36.00, 31.20, 21.60, 10.40, 0.80),
                                    False: Stats(9.04, 19.89, 32.47, 32.57, 6.02)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(36.00, 31.20, 21.60, 10.40, 0.80),
                                        False: Stats(6.82, 18.96, 33.36, 34.40, 6.45)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(16.00, 26.80, 30.80, 24.00, 2.40),
                                        False: Stats(5.09, 13.16, 32.62, 39.46, 9.67)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(30.40, 33.40, 24.80, 11.40, 0.00),
                                        False: Stats(9.30, 19.55, 32.27, 32.72, 6.15)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(8.80, 19.60, 32.40, 32.80, 6.40),
                                        Sex.MALE: Stats(12.80, 23.00, 31.60, 28.60, 4.00),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(7.20, 20.40, 31.20, 30.80, 10.40),
                                        Ethnicity.AMERIND: Stats(14.40, 25.20, 31.20, 26.00, 3.20),
                                        Ethnicity.ASIAN: Stats(6.40, 14.80, 31.60, 36.80, 10.40),
                                        Ethnicity.BLACK: Stats(16.80, 28.20, 30.80, 22.60, 1.60),
                                        Ethnicity.HISPANIC: Stats(16.00, 27.40, 30.80, 23.40, 2.40),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(0.00, 0.00, 0.00, 0.00, 0.00),
                                        Ethnicity.WHITE: Stats(7.20, 16.80, 32.80, 36.00, 7.20)})}),
        Grade.G10: GradeLevels((11.20, 20.80, 31.60, 30.80, 5.60),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(36.00, 31.20, 21.60, 10.40, 0.80),
                                    False: Stats(9.04, 19.89, 32.47, 32.57, 6.02)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(36.00, 31.20, 21.60, 10.40, 0.80),
                                        False: Stats(6.82, 18.96, 33.36, 34.40, 6.45)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(16.00, 26.80, 30.80, 24.00, 2.40),
                                        False: Stats(5.09, 13.16, 32.62, 39.46, 9.67)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(30.40, 33.40, 24.80, 11.40, 0.00),
                                        False: Stats(9.30, 19.55, 32.27, 32.72, 6.15)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(8.80, 19.60, 32.40, 32.80, 6.40),
                                        Sex.MALE: Stats(12.80, 23.00, 31.60, 28.60, 4.00),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(7.20, 20.40, 31.20, 30.80, 10.40),
                                        Ethnicity.AMERIND: Stats(14.40, 25.20, 31.20, 26.00, 3.20),
                                        Ethnicity.ASIAN: Stats(6.40, 14.80, 31.60, 36.80, 10.40),
                                        Ethnicity.BLACK: Stats(16.80, 28.20, 30.80, 22.60, 1.60),
                                        Ethnicity.HISPANIC: Stats(16.00, 27.40, 30.80, 23.40, 2.40),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(0.00, 0.00, 0.00, 0.00, 0.00),
                                        Ethnicity.WHITE: Stats(7.20, 16.80, 32.80, 36.00, 7.20)})}),
        Grade.G11: GradeLevels((5.60, 27.20, 36.40, 29.20, 1.60),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(23.20, 41.80, 28.40, 6.60, 0.00),
                                    False: Stats(4.07, 25.93, 37.10, 31.17, 1.74)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(23.20, 41.80, 28.40, 6.60, 0.00),
                                        False: Stats(2.25, 24.42, 37.92, 33.51, 1.90)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(8.80, 34.60, 35.20, 20.60, 0.80),
                                        False: Stats(2.14, 19.18, 37.70, 38.52, 2.46)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(34.40, 41.00, 22.80, 1.80, 0.00),
                                        False: Stats(4.09, 26.47, 37.12, 30.64, 1.69)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(4.00, 24.40, 36.80, 32.40, 2.40),
                                        Sex.MALE: Stats(7.20, 29.40, 36.00, 26.60, 0.80),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(7.20, 20.40, 31.20, 30.80, 10.40),
                                        Ethnicity.AMERIND: Stats(8.00, 33.20, 35.60, 22.40, 0.80),
                                        Ethnicity.ASIAN: Stats(5.60, 18.20, 35.60, 37.40, 3.20),
                                        Ethnicity.BLACK: Stats(8.80, 37.60, 35.60, 18.00, 0.00),
                                        Ethnicity.HISPANIC: Stats(9.60, 35.40, 35.20, 19.80, 0.00),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(1.60, 9.40, 27.20, 37.80, 24.00),
                                        Ethnicity.WHITE: Stats(3.20, 20.60, 37.20, 36.60, 2.40)})}),
        Grade.G12: GradeLevels((11.20, 20.80, 31.60, 30.80, 5.60),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(36.00, 31.20, 21.60, 10.40, 0.80),
                                    False: Stats(9.04, 19.89, 32.47, 32.57, 6.02)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(36.00, 31.20, 21.60, 10.40, 0.80),
                                        False: Stats(6.82, 18.96, 33.36, 34.40, 6.45)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(16.00, 26.80, 30.80, 24.00, 2.40),
                                        False: Stats(5.09, 13.16, 32.62, 39.46, 9.67)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(30.40, 33.40, 24.80, 11.40, 0.00),
                                        False: Stats(9.30, 19.55, 32.27, 32.72, 6.15)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(8.80, 19.60, 32.40, 32.80, 6.40),
                                        Sex.MALE: Stats(12.80, 23.00, 31.60, 28.60, 4.00),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(7.20, 20.40, 31.20, 30.80, 10.40),
                                        Ethnicity.AMERIND: Stats(14.40, 25.20, 31.20, 26.00, 3.20),
                                        Ethnicity.ASIAN: Stats(6.40, 14.80, 31.60, 36.80, 10.40),
                                        Ethnicity.BLACK: Stats(16.80, 28.20, 30.80, 22.60, 1.60),
                                        Ethnicity.HISPANIC: Stats(16.00, 27.40, 30.80, 23.40, 2.40),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(0.00, 0.00, 0.00, 0.00, 0.00),
                                        Ethnicity.WHITE: Stats(7.20, 16.80, 32.80, 36.00, 7.20)})}),
    },
    Subject.ELA: {
        Grade.G01: GradeLevels((7.20, 19.80, 31.20, 31.40, 10.40),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(23.20, 31.00, 27.20, 16.20, 2.40),
                                    False: Stats(5.99, 18.96, 31.50, 32.55, 11.00)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(23.20, 31.00, 27.20, 16.20, 2.40),
                                        False: Stats(4.38, 17.82, 31.90, 34.08, 11.81)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(10.40, 24.80, 31.60, 26.80, 6.40),
                                        False: Stats(2.96, 13.17, 30.67, 37.50, 15.70)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(18.40, 29.80, 29.60, 19.80, 2.40),
                                        False: Stats(6.10, 18.81, 31.36, 32.55, 11.19)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(6.40, 20.20, 32.00, 31.80, 9.60),
                                        Sex.MALE: Stats(8.00, 19.40, 30.40, 31.00, 11.20),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(7.20, 20.40, 31.20, 30.80, 10.40),
                                        Ethnicity.AMERIND: Stats(9.60, 24.00, 31.60, 27.60, 7.20),
                                        Ethnicity.ASIAN: Stats(2.40, 10.20, 27.60, 37.40, 22.40),
                                        Ethnicity.BLACK: Stats(13.60, 27.40, 30.80, 23.40, 4.80),
                                        Ethnicity.HISPANIC: Stats(10.40, 24.80, 32.00, 27.20, 5.60),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(1.60, 9.40, 27.20, 37.80, 24.00),
                                        Ethnicity.WHITE: Stats(4.24, 15.88, 31.72, 35.84, 12.32)})}),
        Grade.G02: GradeLevels((7.20, 19.80, 31.20, 31.40, 10.40),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(23.20, 31.00, 27.20, 16.20, 2.40),
                                    False: Stats(5.99, 18.96, 31.50, 32.55, 11.00)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(23.20, 31.00, 27.20, 16.20, 2.40),
                                        False: Stats(4.38, 17.82, 31.90, 34.08, 11.81)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(10.40, 24.80, 31.60, 26.80, 6.40),
                                        False: Stats(2.96, 13.17, 30.67, 37.50, 15.70)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(18.40, 29.80, 29.60, 19.80, 2.40),
                                        False: Stats(6.10, 18.81, 31.36, 32.55, 11.19)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(6.40, 20.20, 32.00, 31.80, 9.60),
                                        Sex.MALE: Stats(8.00, 19.40, 30.40, 31.00, 11.20),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(7.20, 20.40, 31.20, 30.80, 10.40),
                                        Ethnicity.AMERIND: Stats(9.60, 24.00, 31.60, 27.60, 7.20),
                                        Ethnicity.ASIAN: Stats(2.40, 10.20, 27.60, 37.40, 22.40),
                                        Ethnicity.BLACK: Stats(13.60, 27.40, 30.80, 23.40, 4.80),
                                        Ethnicity.HISPANIC: Stats(10.40, 24.80, 32.00, 27.20, 5.60),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(1.60, 9.40, 27.20, 37.80, 24.00),
                                        Ethnicity.WHITE: Stats(4.24, 15.88, 31.72, 35.84, 12.32)})}),
        Grade.G03: GradeLevels((7.20, 19.80, 31.20, 31.40, 10.40),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(23.20, 31.00, 27.20, 16.20, 2.40),
                                    False: Stats(5.99, 18.96, 31.50, 32.55, 11.00)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(23.20, 31.00, 27.20, 16.20, 2.40),
                                        False: Stats(4.38, 17.82, 31.90, 34.08, 11.81)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(10.40, 24.80, 31.60, 26.80, 6.40),
                                        False: Stats(2.96, 13.17, 30.67, 37.50, 15.70)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(18.40, 29.80, 29.60, 19.80, 2.40),
                                        False: Stats(6.10, 18.81, 31.36, 32.55, 11.19)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(6.40, 20.20, 32.00, 31.80, 9.60),
                                        Sex.MALE: Stats(8.00, 19.40, 30.40, 31.00, 11.20),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(7.20, 20.40, 31.20, 30.80, 10.40),
                                        Ethnicity.AMERIND: Stats(9.60, 24.00, 31.60, 27.60, 7.20),
                                        Ethnicity.ASIAN: Stats(2.40, 10.20, 27.60, 37.40, 22.40),
                                        Ethnicity.BLACK: Stats(13.60, 27.40, 30.80, 23.40, 4.80),
                                        Ethnicity.HISPANIC: Stats(10.40, 24.80, 32.00, 27.20, 5.60),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(1.60, 9.40, 27.20, 37.80, 24.00),
                                        Ethnicity.WHITE: Stats(4.24, 15.88, 31.72, 35.84, 12.32)})}),
        Grade.G04: GradeLevels((4.00, 16.60, 26.00, 29.40, 24.00),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(16.80, 30.60, 28.00, 17.40, 7.20),
                                    False: Stats(2.89, 15.38, 25.82, 30.44, 25.46)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(16.80, 30.60, 28.00, 17.40, 7.20),
                                        False: Stats(1.56, 13.93, 25.62, 31.69, 27.20)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(6.40, 21.40, 28.40, 27.00, 16.80),
                                        False: Stats(0.94, 10.49, 22.94, 32.45, 33.16)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(12.00, 28.20, 30.00, 21.80, 8.00),
                                        False: Stats(3.21, 15.45, 25.60, 30.15, 25.58)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(4.00, 16.60, 26.00, 29.40, 24.00),
                                        Sex.MALE: Stats(4.00, 16.60, 26.00, 29.40, 24.00),
                                        Sex.OTHER: Stats(0.00, 0.00, 0.00, 0.00, 0.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(4.00, 17.80, 26.00, 28.20, 24.00),
                                        Ethnicity.AMERIND: Stats(6.40, 22.00, 28.00, 26.00, 17.60),
                                        Ethnicity.ASIAN: Stats(1.60, 6.40, 16.40, 30.00, 45.60),
                                        Ethnicity.BLACK: Stats(6.40, 25.60, 30.80, 25.20, 12.00),
                                        Ethnicity.HISPANIC: Stats(6.40, 21.40, 29.20, 27.80, 15.20),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(1.60, 9.40, 27.20, 37.80, 24.00),
                                        Ethnicity.WHITE: Stats(2.40, 12.00, 23.78, 31.78, 30.04)})}),
        Grade.G05: GradeLevels((5.60, 17.00, 26.00, 29.00, 22.40),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(21.60, 30.60, 26.80, 16.20, 4.80),
                                    False: Stats(4.21, 15.82, 25.93, 30.11, 23.93)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(21.60, 30.60, 26.80, 16.20, 4.80),
                                        False: Stats(2.55, 14.41, 25.85, 31.44, 25.75)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(8.00, 21.80, 28.00, 26.20, 16.00),
                                        False: Stats(2.54, 10.89, 23.46, 32.57, 30.54)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(16.80, 29.40, 28.00, 18.60, 7.20),
                                        False: Stats(4.62, 15.92, 25.83, 29.91, 23.72)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(5.60, 16.40, 26.00, 29.60, 22.40),
                                        Sex.MALE: Stats(5.68, 17.56, 25.82, 28.26, 22.69),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(7.20, 19.20, 25.60, 26.40, 21.60),
                                        Ethnicity.AMERIND: Stats(10.40, 23.00, 27.60, 24.60, 14.40),
                                        Ethnicity.ASIAN: Stats(2.40, 7.20, 16.40, 29.20, 44.80),
                                        Ethnicity.BLACK: Stats(10.40, 24.20, 28.80, 24.60, 12.00),
                                        Ethnicity.HISPANIC: Stats(8.00, 21.80, 28.40, 26.60, 15.20),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(1.60, 9.40, 27.20, 37.80, 24.00),
                                        Ethnicity.WHITE: Stats(3.88, 13.66, 25.03, 31.37, 26.06)})}),
        Grade.G06: GradeLevels((6.40, 17.80, 24.40, 26.60, 24.80),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(23.20, 32.20, 26.00, 13.80, 4.80),
                                    False: Stats(4.94, 16.55, 24.26, 27.71, 26.54)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(23.20, 32.20, 26.00, 13.80, 4.80),
                                        False: Stats(3.20, 15.06, 24.10, 29.04, 28.61)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(9.60, 23.40, 27.20, 23.80, 16.00),
                                        False: Stats(2.64, 11.23, 21.11, 29.88, 35.13)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(21.60, 31.80, 26.00, 14.20, 6.40),
                                        False: Stats(5.43, 16.90, 24.30, 27.39, 25.98)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(5.60, 17.00, 24.27, 27.27, 25.86),
                                        Sex.MALE: Stats(7.20, 18.60, 24.40, 25.80, 24.00),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(5.60, 19.40, 24.80, 25.40, 24.80),
                                        Ethnicity.AMERIND: Stats(9.60, 23.40, 27.60, 24.20, 15.20),
                                        Ethnicity.ASIAN: Stats(2.40, 7.20, 14.80, 27.60, 48.00),
                                        Ethnicity.BLACK: Stats(11.74, 25.93, 28.13, 22.20, 12.00),
                                        Ethnicity.HISPANIC: Stats(9.60, 24.00, 28.00, 24.00, 14.40),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(1.60, 9.40, 27.20, 37.80, 24.00),
                                        Ethnicity.WHITE: Stats(4.00, 13.76, 22.78, 29.03, 30.43)})}),
        Grade.G07: GradeLevels((7.20, 17.40, 24.00, 26.60, 24.80),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(24.80, 32.00, 25.60, 13.60, 4.00),
                                    False: Stats(5.67, 16.13, 23.86, 27.73, 26.61)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(24.80, 32.00, 25.60, 13.60, 4.00),
                                        False: Stats(3.85, 14.62, 23.70, 29.08, 28.76)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(10.40, 23.60, 27.20, 23.60, 15.20),
                                        False: Stats(3.59, 10.41, 20.39, 29.98, 35.62)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(24.00, 31.80, 25.20, 13.40, 5.60),
                                        False: Stats(6.13, 16.48, 23.92, 27.44, 26.02)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(6.40, 17.20, 24.40, 27.20, 24.80),
                                        Sex.MALE: Stats(8.00, 17.60, 23.49, 25.89, 25.02),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(5.60, 18.20, 24.80, 26.60, 24.80),
                                        Ethnicity.AMERIND: Stats(8.00, 23.00, 27.60, 24.60, 16.80),
                                        Ethnicity.ASIAN: Stats(2.40, 7.80, 14.80, 27.00, 48.00),
                                        Ethnicity.BLACK: Stats(14.70, 26.25, 27.45, 21.20, 10.40),
                                        Ethnicity.HISPANIC: Stats(10.40, 24.20, 28.00, 23.80, 13.60),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(1.60, 9.40, 27.20, 37.80, 24.00),
                                        Ethnicity.WHITE: Stats(4.00, 12.40, 22.17, 29.77, 31.66)})}),
        Grade.G08: GradeLevels((5.60, 20.60, 29.20, 28.60, 16.00),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(21.60, 35.40, 28.40, 13.00, 1.60),
                                    False: Stats(4.21, 19.31, 29.27, 29.96, 17.26)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(21.60, 35.40, 28.40, 13.00, 1.60),
                                        False: Stats(2.55, 17.78, 29.35, 31.57, 18.74)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(8.80, 26.20, 30.80, 24.60, 9.60),
                                        False: Stats(2.14, 14.53, 27.46, 32.93, 22.94)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(17.60, 31.40, 28.80, 17.40, 4.80),
                                        False: Stats(4.83, 19.91, 29.22, 29.31, 16.71)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(4.80, 19.80, 29.13, 29.33, 16.94),
                                        Sex.MALE: Stats(6.40, 21.40, 29.20, 27.80, 15.20),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(6.40, 19.60, 28.80, 29.20, 16.00),
                                        Ethnicity.AMERIND: Stats(7.20, 25.80, 31.60, 25.80, 9.60),
                                        Ethnicity.ASIAN: Stats(1.60, 8.80, 20.40, 31.60, 37.60),
                                        Ethnicity.BLACK: Stats(10.80, 30.00, 31.80, 21.80, 5.60),
                                        Ethnicity.HISPANIC: Stats(8.80, 26.20, 31.60, 25.40, 8.00),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(1.60, 9.40, 27.20, 37.80, 24.00),
                                        Ethnicity.WHITE: Stats(3.20, 16.74, 28.65, 31.91, 19.50)})}),
        Grade.G09: GradeLevels((7.20, 19.80, 31.20, 31.40, 10.40),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(23.20, 31.00, 27.20, 16.20, 2.40),
                                    False: Stats(5.99, 18.96, 31.50, 32.55, 11.00)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(23.20, 31.00, 27.20, 16.20, 2.40),
                                        False: Stats(4.38, 17.82, 31.90, 34.08, 11.81)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(10.40, 24.80, 31.60, 26.80, 6.40),
                                        False: Stats(2.96, 13.17, 30.67, 37.50, 15.70)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(18.40, 29.80, 29.60, 19.80, 2.40),
                                        False: Stats(6.10, 18.81, 31.36, 32.55, 11.19)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(6.40, 20.20, 32.00, 31.80, 9.60),
                                        Sex.MALE: Stats(8.00, 19.40, 30.40, 31.00, 11.20),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(7.20, 20.40, 31.20, 30.80, 10.40),
                                        Ethnicity.AMERIND: Stats(9.60, 24.00, 31.60, 27.60, 7.20),
                                        Ethnicity.ASIAN: Stats(2.40, 10.20, 27.60, 37.40, 22.40),
                                        Ethnicity.BLACK: Stats(13.60, 27.40, 30.80, 23.40, 4.80),
                                        Ethnicity.HISPANIC: Stats(10.40, 24.80, 32.00, 27.20, 5.60),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(1.60, 9.40, 27.20, 37.80, 24.00),
                                        Ethnicity.WHITE: Stats(4.24, 15.88, 31.72, 35.84, 12.32)})}),
        Grade.G10: GradeLevels((7.20, 19.80, 31.20, 31.40, 10.40),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(23.20, 31.00, 27.20, 16.20, 2.40),
                                    False: Stats(5.99, 18.96, 31.50, 32.55, 11.00)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(23.20, 31.00, 27.20, 16.20, 2.40),
                                        False: Stats(4.38, 17.82, 31.90, 34.08, 11.81)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(10.40, 24.80, 31.60, 26.80, 6.40),
                                        False: Stats(2.96, 13.17, 30.67, 37.50, 15.70)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(18.40, 29.80, 29.60, 19.80, 2.40),
                                        False: Stats(6.10, 18.81, 31.36, 32.55, 11.19)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(6.40, 20.20, 32.00, 31.80, 9.60),
                                        Sex.MALE: Stats(8.00, 19.40, 30.40, 31.00, 11.20),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(7.20, 20.40, 31.20, 30.80, 10.40),
                                        Ethnicity.AMERIND: Stats(9.60, 24.00, 31.60, 27.60, 7.20),
                                        Ethnicity.ASIAN: Stats(2.40, 10.20, 27.60, 37.40, 22.40),
                                        Ethnicity.BLACK: Stats(13.60, 27.40, 30.80, 23.40, 4.80),
                                        Ethnicity.HISPANIC: Stats(10.40, 24.80, 32.00, 27.20, 5.60),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(1.60, 9.40, 27.20, 37.80, 24.00),
                                        Ethnicity.WHITE: Stats(4.24, 15.88, 31.72, 35.84, 12.32)})}),
        Grade.G11: GradeLevels((5.60, 20.60, 29.20, 28.60, 16.00),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(21.60, 35.40, 28.40, 13.00, 1.60),
                                    False: Stats(4.21, 19.31, 29.27, 29.96, 17.26)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(21.60, 35.40, 28.40, 13.00, 1.60),
                                        False: Stats(2.55, 17.78, 29.35, 31.57, 18.74)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(8.80, 26.20, 30.80, 24.60, 9.60),
                                        False: Stats(2.14, 14.53, 27.46, 32.93, 22.94)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(17.60, 31.40, 28.80, 17.40, 4.80),
                                        False: Stats(4.83, 19.91, 29.22, 29.31, 16.71)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(4.80, 19.80, 29.60, 29.80, 16.00),
                                        Sex.MALE: Stats(6.40, 21.40, 28.70, 27.30, 16.19),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(7.20, 20.40, 31.20, 30.80, 10.40),
                                        Ethnicity.AMERIND: Stats(7.20, 25.80, 31.60, 25.80, 9.60),
                                        Ethnicity.ASIAN: Stats(1.60, 8.80, 20.40, 31.60, 37.60),
                                        Ethnicity.BLACK: Stats(10.36, 30.22, 32.02, 21.80, 5.60),
                                        Ethnicity.HISPANIC: Stats(8.80, 26.20, 31.60, 25.40, 8.00),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(1.60, 9.40, 27.20, 37.80, 24.00),
                                        Ethnicity.WHITE: Stats(3.20, 16.35, 28.44, 32.09, 19.91)})}),
        Grade.G12: GradeLevels((7.20, 19.80, 31.20, 31.40, 10.40),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(23.20, 31.00, 27.20, 16.20, 2.40),
                                    False: Stats(5.99, 18.96, 31.50, 32.55, 11.00)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(23.20, 31.00, 27.20, 16.20, 2.40),
                                        False: Stats(4.38, 17.82, 31.90, 34.08, 11.81)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(10.40, 24.80, 31.60, 26.80, 6.40),
                                        False: Stats(2.96, 13.17, 30.67, 37.50, 15.70)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(18.40, 29.80, 29.60, 19.80, 2.40),
                                        False: Stats(6.10, 18.81, 31.36, 32.55, 11.19)}),
                                   Sex: DemographicLevels(
                                       {Sex.FEMALE: Stats(6.40, 20.20, 32.00, 31.80, 9.60),
                                        Sex.MALE: Stats(8.00, 19.40, 30.40, 31.00, 11.20),
                                        Sex.OTHER: Stats(7.20, 19.80, 32.40, 32.60, 8.00)}),
                                   Ethnicity: DemographicLevels(
                                       {Ethnicity.MULTIPLE: Stats(7.20, 20.40, 31.20, 30.80, 10.40),
                                        Ethnicity.AMERIND: Stats(9.60, 24.00, 31.60, 27.60, 7.20),
                                        Ethnicity.ASIAN: Stats(2.40, 10.20, 27.60, 37.40, 22.40),
                                        Ethnicity.BLACK: Stats(13.60, 27.40, 30.80, 23.40, 4.80),
                                        Ethnicity.HISPANIC: Stats(10.40, 24.80, 32.00, 27.20, 5.60),
                                        Ethnicity.NONE: Stats(3.20, 29.00, 31.20, 22.20, 14.40),
                                        Ethnicity.HAWAII: Stats(1.60, 9.40, 27.20, 37.80, 24.00),
                                        Ethnicity.WHITE: Stats(4.24, 15.88, 31.72, 35.84, 12.32)})}),
    },
}

LEVELS_BY_GRADE_BY_SUBJ_OLD = {
    Subject.Math: {
        Grade.G01: GradeLevels((14.0, 30.0, 49.0, 7.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(45.0, 37.0, 17.0, 1.0),
                                    False: Stats(11.304347826086957, 29.391304347826086, 51.78260869565217,
                                                 7.521739130434782)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(20.0, 38.0, 39.0, 3.0),
                                        False: Stats(6.363636363636363, 19.818181818181817, 61.72727272727273,
                                                     12.090909090909092)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(45.0, 37.0, 17.0, 1.0),
                                        False: Stats(8.529411764705882, 28.764705882352942, 54.64705882352941,
                                                     8.058823529411764)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(38.0, 43.0, 19.0, 0.0),
                                        False: Stats(11.626373626373626, 28.714285714285715, 51.967032967032964,
                                                     7.6923076923076925)}),
                                   Sex: DemographicLevels(
                                       female=Stats(11.0, 29.0, 52.0, 8.0),
                                       male=Stats(16.0, 33.0, 46.0, 5.0),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(9.0, 31.0, 47.0, 13.0),
                                       dmg_eth_ami=Stats(18.0, 36.0, 42.0, 4.0),
                                       dmg_eth_asn=Stats(8.0, 22.0, 57.0, 13.0),
                                       dmg_eth_blk=Stats(21.0, 40.0, 37.0, 2.0),
                                       dmg_eth_hsp=Stats(20.0, 39.0, 38.0, 3.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(0.0, 0.0, 0.0, 0.0),
                                       dmg_eth_wht=Stats(9.0, 25.0, 57.0, 9.0), )}),
        Grade.G02: GradeLevels((14.0, 30.0, 49.0, 7.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(45.0, 37.0, 17.0, 1.0),
                                    False: Stats(11.304347826086957, 29.391304347826086, 51.78260869565217,
                                                 7.521739130434782)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(20.0, 38.0, 39.0, 3.0),
                                        False: Stats(6.363636363636363, 19.818181818181817, 61.72727272727273,
                                                     12.090909090909092)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(45.0, 37.0, 17.0, 1.0),
                                        False: Stats(8.529411764705882, 28.764705882352942, 54.64705882352941,
                                                     8.058823529411764)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(38.0, 43.0, 19.0, 0.0),
                                        False: Stats(11.626373626373626, 28.714285714285715, 51.967032967032964,
                                                     7.6923076923076925)}),
                                   Sex: DemographicLevels(
                                       female=Stats(11.0, 29.0, 52.0, 8.0),
                                       male=Stats(16.0, 33.0, 46.0, 5.0),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(9.0, 31.0, 47.0, 13.0),
                                       dmg_eth_ami=Stats(18.0, 36.0, 42.0, 4.0),
                                       dmg_eth_asn=Stats(8.0, 22.0, 57.0, 13.0),
                                       dmg_eth_blk=Stats(21.0, 40.0, 37.0, 2.0),
                                       dmg_eth_hsp=Stats(20.0, 39.0, 38.0, 3.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(0.0, 0.0, 0.0, 0.0),
                                       dmg_eth_wht=Stats(9.0, 25.0, 57.0, 9.0), )}),
        Grade.G03: GradeLevels((14.0, 30.0, 49.0, 7.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(45.0, 37.0, 17.0, 1.0),
                                    False: Stats(11.304347826086957, 29.391304347826086, 51.78260869565217,
                                                 7.521739130434782)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(20.0, 38.0, 39.0, 3.0),
                                        False: Stats(6.363636363636363, 19.818181818181817, 61.72727272727273,
                                                     12.090909090909092)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(45.0, 37.0, 17.0, 1.0),
                                        False: Stats(8.529411764705882, 28.764705882352942, 54.64705882352941,
                                                     8.058823529411764)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(38.0, 43.0, 19.0, 0.0),
                                        False: Stats(11.626373626373626, 28.714285714285715, 51.967032967032964,
                                                     7.6923076923076925)}),
                                   Sex: DemographicLevels(
                                       female=Stats(11.0, 29.0, 52.0, 8.0),
                                       male=Stats(16.0, 33.0, 46.0, 5.0),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(9.0, 31.0, 47.0, 13.0),
                                       dmg_eth_ami=Stats(18.0, 36.0, 42.0, 4.0),
                                       dmg_eth_asn=Stats(8.0, 22.0, 57.0, 13.0),
                                       dmg_eth_blk=Stats(21.0, 40.0, 37.0, 2.0),
                                       dmg_eth_hsp=Stats(20.0, 39.0, 38.0, 3.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(0.0, 0.0, 0.0, 0.0),
                                       dmg_eth_wht=Stats(9.0, 25.0, 57.0, 9.0), )}),
        Grade.G04: GradeLevels((9.0, 32.0, 54.0, 5.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(35.0, 45.0, 20.0, 0.0),
                                    False: Stats(6.739130434782608, 30.869565217391305, 56.95652173913044,
                                                 5.434782608695652)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(13.0, 41.0, 44.0, 2.0),
                                        False: Stats(3.909090909090909, 20.545454545454547, 66.72727272727273,
                                                     8.818181818181818)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(35.0, 45.0, 20.0, 0.0),
                                        False: Stats(4.0476190476190474, 29.523809523809526, 60.476190476190474,
                                                     5.9523809523809526)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(30.0, 52.0, 18.0, 0.0),
                                        False: Stats(7.173913043478261, 30.26086956521739, 57.130434782608695,
                                                     5.434782608695652)}),
                                   Sex: DemographicLevels(
                                       female=Stats(7.0, 29.0, 58.0, 6.0),
                                       male=Stats(12.0, 33.0, 52.0, 3.0),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(5.0, 28.0, 37.0, 30.0),
                                       dmg_eth_ami=Stats(13.0, 39.0, 46.0, 2.0),
                                       dmg_eth_asn=Stats(6.0, 20.0, 64.0, 10.0),
                                       dmg_eth_blk=Stats(14.0, 43.0, 41.0, 2.0),
                                       dmg_eth_hsp=Stats(13.0, 42.0, 43.0, 2.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(2.0, 15.0, 53.0, 30.0),
                                       dmg_eth_wht=Stats(6.0, 24.0, 64.0, 6.0), )}),
        Grade.G05: GradeLevels((11.0, 31.0, 53.0, 5.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(39.0, 44.0, 17.0, 0.0),
                                    False: Stats(8.565217391304348, 29.869565217391305, 56.130434782608695,
                                                 5.434782608695652)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(15.0, 40.0, 43.0, 2.0),
                                        False: Stats(6.111111111111111, 20.0, 65.22222222222223, 8.666666666666666)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(39.0, 44.0, 17.0, 0.0),
                                        False: Stats(5.666666666666667, 28.523809523809526, 59.857142857142854,
                                                     5.9523809523809526)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(39.0, 48.0, 13.0, 0.0),
                                        False: Stats(8.89247311827957, 29.72043010752688, 56.01075268817204,
                                                     5.376344086021505)}),
                                   Sex: DemographicLevels(
                                       female=Stats(8.0, 30.0, 56.0, 6.0),
                                       male=Stats(13.0, 33.0, 51.0, 3.0),
                                       not_stated=Stats(0.0, 0.0, 0.0, 0.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(9.0, 29.0, 35.0, 27.0),
                                       dmg_eth_ami=Stats(15.0, 42.0, 40.0, 3.0),
                                       dmg_eth_asn=Stats(7.0, 20.0, 63.0, 10.0),
                                       dmg_eth_blk=Stats(17.0, 43.0, 38.0, 2.0),
                                       dmg_eth_hsp=Stats(15.0, 40.0, 43.0, 2.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(2.0, 15.0, 53.0, 30.0),
                                       dmg_eth_wht=Stats(7.0, 25.0, 62.0, 6.0), )}),
        Grade.G06: GradeLevels((11.0, 33.0, 54.0, 2.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(38.0, 47.0, 15.0, 0.0),
                                    False: Stats(8.652173913043478, 31.782608695652176, 57.391304347826086,
                                                 2.1739130434782608)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(16.0, 43.0, 40.0, 1.0),
                                        False: Stats(5.130434782608695, 21.26086956521739, 70.43478260869566,
                                                     3.1739130434782608)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(38.0, 47.0, 15.0, 0.0),
                                        False: Stats(5.857142857142857, 30.333333333333332, 61.42857142857143,
                                                     2.380952380952381)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(51.0, 44.0, 5.0, 0.0),
                                        False: Stats(8.446808510638299, 32.297872340425535, 57.12765957446808,
                                                     2.127659574468085)}),
                                   Sex: DemographicLevels(
                                       female=Stats(8.0, 31.0, 58.0, 3.0),
                                       male=Stats(13.0, 36.0, 49.0, 2.0),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(7.0, 30.0, 32.0, 31.0),
                                       dmg_eth_ami=Stats(0.0, 0.0, 0.0, 0.0),
                                       dmg_eth_asn=Stats(8.0, 23.0, 64.0, 5.0),
                                       dmg_eth_blk=Stats(16.0, 47.0, 36.0, 1.0),
                                       dmg_eth_hsp=Stats(17.0, 44.0, 38.0, 1.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(2.0, 15.0, 53.0, 30.0),
                                       dmg_eth_wht=Stats(6.0, 26.0, 64.0, 4.0), )}),
        Grade.G07: GradeLevels((8.0, 40.0, 48.0, 4.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(33.0, 55.0, 12.0, 0.0),
                                    False: Stats(5.826086956521739, 38.69565217391305, 51.130434782608695,
                                                 4.3478260869565215)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(13.0, 50.0, 36.0, 1.0),
                                        False: Stats(2.3617021276595747, 28.72340425531915, 61.53191489361702,
                                                     7.382978723404255)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(33.0, 55.0, 12.0, 0.0),
                                        False: Stats(3.238095238095238, 37.142857142857146, 54.857142857142854,
                                                     4.761904761904762)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(42.0, 54.0, 4.0, 0.0),
                                        False: Stats(6.2105263157894735, 39.26315789473684, 50.31578947368421,
                                                     4.2105263157894735)}),
                                   Sex: DemographicLevels(
                                       female=Stats(6.0, 36.0, 53.0, 5.0),
                                       male=Stats(11.0, 42.0, 44.0, 3.0),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(7.0, 28.0, 34.0, 31.0),
                                       dmg_eth_ami=Stats(11.0, 50.0, 38.0, 1.0),
                                       dmg_eth_asn=Stats(6.0, 26.0, 60.0, 8.0),
                                       dmg_eth_blk=Stats(13.0, 53.0, 33.0, 1.0),
                                       dmg_eth_hsp=Stats(13.0, 51.0, 35.0, 1.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(2.0, 15.0, 53.0, 30.0),
                                       dmg_eth_wht=Stats(5.0, 31.0, 59.0, 5.0), )}),
        Grade.G08: GradeLevels((7.0, 43.0, 48.0, 2.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(29.0, 60.0, 11.0, 0.0),
                                    False: Stats(5.086956521739131, 41.52173913043478, 51.21739130434783,
                                                 2.1739130434782608)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(11.0, 54.0, 34.0, 1.0),
                                        False: Stats(2.6666666666666665, 31.083333333333332, 63.166666666666664,
                                                     3.0833333333333335)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(29.0, 60.0, 11.0, 0.0),
                                        False: Stats(2.8095238095238093, 39.76190476190476, 55.04761904761905,
                                                     2.380952380952381)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(43.0, 54.0, 3.0, 0.0),
                                        False: Stats(5.105263157894737, 42.421052631578945, 50.36842105263158,
                                                     2.1052631578947367)}),
                                   Sex: DemographicLevels(
                                       female=Stats(5.0, 39.0, 53.0, 3.0),
                                       male=Stats(9.0, 46.0, 44.0, 1.0),
                                       not_stated=Stats(0.0, 0.0, 0.0, 0.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(8.0, 30.0, 42.0, 20.0),
                                       dmg_eth_ami=Stats(10.0, 52.0, 37.0, 1.0),
                                       dmg_eth_asn=Stats(7.0, 28.0, 61.0, 4.0),
                                       dmg_eth_blk=Stats(11.0, 59.0, 30.0, 0.0),
                                       dmg_eth_hsp=Stats(12.0, 55.0, 33.0, 0.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(2.0, 15.0, 53.0, 30.0),
                                       dmg_eth_wht=Stats(4.0, 33.0, 60.0, 3.0), )}),
        Grade.G09: GradeLevels((14.0, 30.0, 49.0, 7.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(45.0, 37.0, 17.0, 1.0),
                                    False: Stats(11.304347826086957, 29.391304347826086, 51.78260869565217,
                                                 7.521739130434782)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(20.0, 38.0, 39.0, 3.0),
                                        False: Stats(6.363636363636363, 19.818181818181817, 61.72727272727273,
                                                     12.090909090909092)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(45.0, 37.0, 17.0, 1.0),
                                        False: Stats(8.529411764705882, 28.764705882352942, 54.64705882352941,
                                                     8.058823529411764)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(38.0, 43.0, 19.0, 0.0),
                                        False: Stats(11.626373626373626, 28.714285714285715, 51.967032967032964,
                                                     7.6923076923076925)}),
                                   Sex: DemographicLevels(
                                       female=Stats(11.0, 29.0, 52.0, 8.0),
                                       male=Stats(16.0, 33.0, 46.0, 5.0),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(9.0, 31.0, 47.0, 13.0),
                                       dmg_eth_ami=Stats(18.0, 36.0, 42.0, 4.0),
                                       dmg_eth_asn=Stats(8.0, 22.0, 57.0, 13.0),
                                       dmg_eth_blk=Stats(21.0, 40.0, 37.0, 2.0),
                                       dmg_eth_hsp=Stats(20.0, 39.0, 38.0, 3.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(0.0, 0.0, 0.0, 0.0),
                                       dmg_eth_wht=Stats(9.0, 25.0, 57.0, 9.0), )}),
        Grade.G10: GradeLevels((14.0, 30.0, 49.0, 7.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(45.0, 37.0, 17.0, 1.0),
                                    False: Stats(11.304347826086957, 29.391304347826086, 51.78260869565217,
                                                 7.521739130434782)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(20.0, 38.0, 39.0, 3.0),
                                        False: Stats(6.363636363636363, 19.818181818181817, 61.72727272727273,
                                                     12.090909090909092)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(45.0, 37.0, 17.0, 1.0),
                                        False: Stats(8.529411764705882, 28.764705882352942, 54.64705882352941,
                                                     8.058823529411764)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(38.0, 43.0, 19.0, 0.0),
                                        False: Stats(11.626373626373626, 28.714285714285715, 51.967032967032964,
                                                     7.6923076923076925)}),
                                   Sex: DemographicLevels(
                                       female=Stats(11.0, 29.0, 52.0, 8.0),
                                       male=Stats(16.0, 33.0, 46.0, 5.0),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(9.0, 31.0, 47.0, 13.0),
                                       dmg_eth_ami=Stats(18.0, 36.0, 42.0, 4.0),
                                       dmg_eth_asn=Stats(8.0, 22.0, 57.0, 13.0),
                                       dmg_eth_blk=Stats(21.0, 40.0, 37.0, 2.0),
                                       dmg_eth_hsp=Stats(20.0, 39.0, 38.0, 3.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(0.0, 0.0, 0.0, 0.0),
                                       dmg_eth_wht=Stats(9.0, 25.0, 57.0, 9.0), )}),
        Grade.G11: GradeLevels((7.0, 43.0, 48.0, 2.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(29.0, 60.0, 11.0, 0.0),
                                    False: Stats(5.086956521739131, 41.52173913043478, 51.21739130434783,
                                                 2.1739130434782608)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(11.0, 54.0, 34.0, 1.0),
                                        False: Stats(2.6666666666666665, 31.083333333333332, 63.166666666666664,
                                                     3.0833333333333335)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(29.0, 60.0, 11.0, 0.0),
                                        False: Stats(2.8095238095238093, 39.76190476190476, 55.04761904761905,
                                                     2.380952380952381)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(43.0, 54.0, 3.0, 0.0),
                                        False: Stats(5.105263157894737, 42.421052631578945, 50.36842105263158,
                                                     2.1052631578947367)}),
                                   Sex: DemographicLevels(
                                       female=Stats(5.0, 39.0, 53.0, 3.0),
                                       male=Stats(9.0, 46.0, 44.0, 1.0),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(9.0, 31.0, 47.0, 13.0),
                                       dmg_eth_ami=Stats(10.0, 52.0, 37.0, 1.0),
                                       dmg_eth_asn=Stats(7.0, 28.0, 61.0, 4.0),
                                       dmg_eth_blk=Stats(11.0, 59.0, 30.0, 0.0),
                                       dmg_eth_hsp=Stats(12.0, 55.0, 33.0, 0.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(2.0, 15.0, 53.0, 30.0),
                                       dmg_eth_wht=Stats(4.0, 33.0, 60.0, 3.0), )}),
        Grade.G12: GradeLevels((14.0, 30.0, 49.0, 7.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(45.0, 37.0, 17.0, 1.0),
                                    False: Stats(11.304347826086957, 29.391304347826086, 51.78260869565217,
                                                 7.521739130434782)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(20.0, 38.0, 39.0, 3.0),
                                        False: Stats(6.363636363636363, 19.818181818181817, 61.72727272727273,
                                                     12.090909090909092)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(45.0, 37.0, 17.0, 1.0),
                                        False: Stats(8.529411764705882, 28.764705882352942, 54.64705882352941,
                                                     8.058823529411764)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(38.0, 43.0, 19.0, 0.0),
                                        False: Stats(11.626373626373626, 28.714285714285715, 51.967032967032964,
                                                     7.6923076923076925)}),
                                   Sex: DemographicLevels(
                                       female=Stats(11.0, 29.0, 52.0, 8.0),
                                       male=Stats(16.0, 33.0, 46.0, 5.0),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(9.0, 31.0, 47.0, 13.0),
                                       dmg_eth_ami=Stats(18.0, 36.0, 42.0, 4.0),
                                       dmg_eth_asn=Stats(8.0, 22.0, 57.0, 13.0),
                                       dmg_eth_blk=Stats(21.0, 40.0, 37.0, 2.0),
                                       dmg_eth_hsp=Stats(20.0, 39.0, 38.0, 3.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(0.0, 0.0, 0.0, 0.0),
                                       dmg_eth_wht=Stats(9.0, 25.0, 57.0, 9.0), )}),
    },
    Subject.ELA: {
        Grade.G01: GradeLevels((9.0, 30.0, 48.0, 13.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(29.0, 42.0, 26.0, 3.0),
                                    False: Stats(7.494623655913978, 29.096774193548388, 49.655913978494624,
                                                 13.75268817204301)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(13.0, 37.0, 42.0, 8.0),
                                        False: Stats(3.697674418604651, 20.72093023255814, 55.95348837209303,
                                                     19.627906976744185)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(29.0, 42.0, 26.0, 3.0),
                                        False: Stats(5.470588235294118, 27.88235294117647, 51.88235294117647,
                                                     14.764705882352942)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(23.0, 42.0, 32.0, 3.0),
                                        False: Stats(7.615384615384615, 28.813186813186814, 49.582417582417584,
                                                     13.989010989010989)}),
                                   Sex: DemographicLevels(
                                       female=Stats(8.0, 31.0, 49.0, 12.0),
                                       male=Stats(10.0, 29.0, 47.0, 14.0),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(9.0, 31.0, 47.0, 13.0),
                                       dmg_eth_ami=Stats(12.0, 36.0, 43.0, 9.0),
                                       dmg_eth_asn=Stats(3.0, 16.0, 53.0, 28.0),
                                       dmg_eth_blk=Stats(17.0, 40.0, 37.0, 6.0),
                                       dmg_eth_hsp=Stats(13.0, 37.0, 43.0, 7.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(2.0, 15.0, 53.0, 30.0),
                                       dmg_eth_wht=Stats(5.3, 24.7, 54.6, 15.4), )}),
        Grade.G02: GradeLevels((9.0, 30.0, 48.0, 13.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(29.0, 42.0, 26.0, 3.0),
                                    False: Stats(7.494623655913978, 29.096774193548388, 49.655913978494624,
                                                 13.75268817204301)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(13.0, 37.0, 42.0, 8.0),
                                        False: Stats(3.697674418604651, 20.72093023255814, 55.95348837209303,
                                                     19.627906976744185)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(29.0, 42.0, 26.0, 3.0),
                                        False: Stats(5.470588235294118, 27.88235294117647, 51.88235294117647,
                                                     14.764705882352942)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(23.0, 42.0, 32.0, 3.0),
                                        False: Stats(7.615384615384615, 28.813186813186814, 49.582417582417584,
                                                     13.989010989010989)}),
                                   Sex: DemographicLevels(
                                       female=Stats(8.0, 31.0, 49.0, 12.0),
                                       male=Stats(10.0, 29.0, 47.0, 14.0),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(9.0, 31.0, 47.0, 13.0),
                                       dmg_eth_ami=Stats(12.0, 36.0, 43.0, 9.0),
                                       dmg_eth_asn=Stats(3.0, 16.0, 53.0, 28.0),
                                       dmg_eth_blk=Stats(17.0, 40.0, 37.0, 6.0),
                                       dmg_eth_hsp=Stats(13.0, 37.0, 43.0, 7.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(2.0, 15.0, 53.0, 30.0),
                                       dmg_eth_wht=Stats(5.3, 24.7, 54.6, 15.4), )}),
        Grade.G03: GradeLevels((9.0, 30.0, 48.0, 13.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(29.0, 42.0, 26.0, 3.0),
                                    False: Stats(7.494623655913978, 29.096774193548388, 49.655913978494624,
                                                 13.75268817204301)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(13.0, 37.0, 42.0, 8.0),
                                        False: Stats(3.697674418604651, 20.72093023255814, 55.95348837209303,
                                                     19.627906976744185)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(29.0, 42.0, 26.0, 3.0),
                                        False: Stats(5.470588235294118, 27.88235294117647, 51.88235294117647,
                                                     14.764705882352942)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(23.0, 42.0, 32.0, 3.0),
                                        False: Stats(7.615384615384615, 28.813186813186814, 49.582417582417584,
                                                     13.989010989010989)}),
                                   Sex: DemographicLevels(
                                       female=Stats(8.0, 31.0, 49.0, 12.0),
                                       male=Stats(10.0, 29.0, 47.0, 14.0),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(9.0, 31.0, 47.0, 13.0),
                                       dmg_eth_ami=Stats(12.0, 36.0, 43.0, 9.0),
                                       dmg_eth_asn=Stats(3.0, 16.0, 53.0, 28.0),
                                       dmg_eth_blk=Stats(17.0, 40.0, 37.0, 6.0),
                                       dmg_eth_hsp=Stats(13.0, 37.0, 43.0, 7.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(2.0, 15.0, 53.0, 30.0),
                                       dmg_eth_wht=Stats(5.3, 24.7, 54.6, 15.4), )}),
        Grade.G04: GradeLevels((5.0, 26.0, 39.0, 30.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(21.0, 44.0, 26.0, 9.0),
                                    False: Stats(3.608695652173913, 24.434782608695652, 40.130434782608695,
                                                 31.82608695652174)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(8.0, 33.0, 38.0, 21.0),
                                        False: Stats(1.1818181818181819, 17.09090909090909, 40.27272727272727,
                                                     41.45454545454545)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(21.0, 44.0, 26.0, 9.0),
                                        False: Stats(1.9523809523809523, 22.571428571428573, 41.476190476190474,
                                                     34.0)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(15.0, 42.0, 33.0, 10.0),
                                        False: Stats(4.010989010989011, 24.417582417582416, 39.59340659340659,
                                                     31.978021978021978)}),
                                   Sex: DemographicLevels(
                                       female=Stats(5.0, 26.0, 39.0, 30.0),
                                       male=Stats(5.0, 26.0, 39.0, 30.0),
                                       not_stated=Stats(0.0, 0.0, 0.0, 0.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(5.0, 28.0, 37.0, 30.0),
                                       dmg_eth_ami=Stats(8.0, 34.0, 36.0, 22.0),
                                       dmg_eth_asn=Stats(2.0, 10.0, 31.0, 57.0),
                                       dmg_eth_blk=Stats(8.0, 40.0, 37.0, 15.0),
                                       dmg_eth_hsp=Stats(8.0, 33.0, 40.0, 19.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(2.0, 15.0, 53.0, 30.0),
                                       dmg_eth_wht=Stats(3.0, 19.0, 40.4468, 37.5532), )}),
        Grade.G05: GradeLevels((7.0, 26.0, 39.0, 28.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(27.0, 42.0, 25.0, 6.0),
                                    False: Stats(5.260869565217392, 24.608695652173914, 40.21739130434783,
                                                 29.91304347826087)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(10.0, 33.0, 37.0, 20.0),
                                        False: Stats(3.1818181818181817, 17.09090909090909, 41.54545454545455,
                                                     38.18181818181818)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(27.0, 42.0, 25.0, 6.0),
                                        False: Stats(3.1904761904761907, 22.952380952380953, 41.666666666666664,
                                                     32.19047619047619)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(21.0, 42.0, 28.0, 9.0),
                                        False: Stats(5.782608695652174, 24.608695652173914, 39.95652173913044,
                                                     29.652173913043477)}),
                                   Sex: DemographicLevels(
                                       female=Stats(7.0, 25.0, 40.0, 28.0),
                                       male=Stats(7.1, 26.9, 37.64, 28.36),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(9.0, 29.0, 35.0, 27.0),
                                       dmg_eth_ami=Stats(13.0, 34.0, 35.0, 18.0),
                                       dmg_eth_asn=Stats(3.0, 11.0, 30.0, 56.0),
                                       dmg_eth_blk=Stats(13.0, 36.0, 36.0, 15.0),
                                       dmg_eth_hsp=Stats(10.0, 33.0, 38.0, 19.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(2.0, 15.0, 53.0, 30.0),
                                       dmg_eth_wht=Stats(4.8542, 21.145799999999998, 41.4167, 32.5833), )}),
        Grade.G06: GradeLevels((8.0, 27.0, 34.0, 31.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(29.0, 44.0, 21.0, 6.0),
                                    False: Stats(6.173913043478261, 25.52173913043478, 35.130434782608695,
                                                 33.17391304347826)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(12.0, 35.0, 33.0, 20.0),
                                        False: Stats(3.3043478260869565, 17.608695652173914, 35.17391304347826,
                                                     43.91304347826087)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(29.0, 44.0, 21.0, 6.0),
                                        False: Stats(4.0, 23.761904761904763, 36.476190476190474, 35.76190476190476)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(27.0, 44.0, 21.0, 8.0),
                                        False: Stats(6.787234042553192, 25.914893617021278, 34.829787234042556,
                                                     32.46808510638298)}),
                                   Sex: DemographicLevels(
                                       female=Stats(7.0, 26.0, 34.6735, 32.3265),
                                       male=Stats(9.0, 28.0, 33.0, 30.0),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(7.0, 30.0, 32.0, 31.0),
                                       dmg_eth_ami=Stats(12.0, 35.0, 34.0, 19.0),
                                       dmg_eth_asn=Stats(3.0, 11.0, 26.0, 60.0),
                                       dmg_eth_blk=Stats(14.6842, 38.3158, 32.0, 15.0),
                                       dmg_eth_hsp=Stats(12.0, 36.0, 34.0, 18.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(2.0, 15.0, 53.0, 30.0),
                                       dmg_eth_wht=Stats(5.0, 21.2553, 35.7021, 38.0426), )}),
        Grade.G07: GradeLevels((9.0, 26.0, 34.0, 31.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(31.0, 43.0, 21.0, 5.0),
                                    False: Stats(7.086956521739131, 24.52173913043478, 35.130434782608695,
                                                 33.26086956521739)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(13.0, 35.0, 33.0, 19.0),
                                        False: Stats(4.48936170212766, 15.851063829787234, 35.12765957446808,
                                                     44.53191489361702)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(31.0, 43.0, 21.0, 5.0),
                                        False: Stats(4.809523809523809, 22.761904761904763, 36.476190476190474,
                                                     35.95238095238095)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(30.0, 43.0, 20.0, 7.0),
                                        False: Stats(7.659574468085107, 24.914893617021278, 34.8936170212766,
                                                     32.53191489361702)}),
                                   Sex: DemographicLevels(
                                       female=Stats(8.0, 26.0, 35.0, 31.0),
                                       male=Stats(10.0, 26.0, 32.7255, 31.2745),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(7.0, 28.0, 34.0, 31.0),
                                       dmg_eth_ami=Stats(10.0, 35.0, 34.0, 21.0),
                                       dmg_eth_asn=Stats(3.0, 12.0, 25.0, 60.0),
                                       dmg_eth_blk=Stats(18.3684, 37.6316, 31.0, 13.0),
                                       dmg_eth_hsp=Stats(13.0, 36.0, 34.0, 17.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(2.0, 15.0, 53.0, 30.0),
                                       dmg_eth_wht=Stats(5.0, 19.0, 36.4167, 39.5833), )}),
        Grade.G08: GradeLevels((7.0, 32.0, 41.0, 20.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(27.0, 50.0, 21.0, 2.0),
                                    False: Stats(5.260869565217392, 30.434782608695652, 42.73913043478261,
                                                 21.565217391304348)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(11.0, 40.0, 37.0, 12.0),
                                        False: Stats(2.6666666666666665, 23.333333333333332, 45.333333333333336,
                                                     28.666666666666668)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(27.0, 50.0, 21.0, 2.0),
                                        False: Stats(3.1904761904761907, 28.571428571428573, 44.80952380952381,
                                                     23.428571428571427)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(22.0, 45.0, 27.0, 6.0),
                                        False: Stats(6.042553191489362, 31.170212765957448, 41.8936170212766,
                                                     20.893617021276597)}),
                                   Sex: DemographicLevels(
                                       female=Stats(6.0, 31.0, 41.8163, 21.1837),
                                       male=Stats(8.0, 33.0, 40.0, 19.0),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(8.0, 30.0, 42.0, 20.0),
                                       dmg_eth_ami=Stats(9.0, 40.0, 39.0, 12.0),
                                       dmg_eth_asn=Stats(2.0, 14.0, 37.0, 47.0),
                                       dmg_eth_blk=Stats(13.5, 45.5, 34.0, 7.0),
                                       dmg_eth_hsp=Stats(11.0, 40.0, 39.0, 10.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(2.0, 15.0, 53.0, 30.0),
                                       dmg_eth_wht=Stats(4.0, 26.5625, 45.0625, 24.375), )}),
        Grade.G09: GradeLevels((9.0, 30.0, 48.0, 13.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(29.0, 42.0, 26.0, 3.0),
                                    False: Stats(7.494623655913978, 29.096774193548388, 49.655913978494624,
                                                 13.75268817204301)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(13.0, 37.0, 42.0, 8.0),
                                        False: Stats(3.697674418604651, 20.72093023255814, 55.95348837209303,
                                                     19.627906976744185)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(29.0, 42.0, 26.0, 3.0),
                                        False: Stats(5.470588235294118, 27.88235294117647, 51.88235294117647,
                                                     14.764705882352942)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(23.0, 42.0, 32.0, 3.0),
                                        False: Stats(7.615384615384615, 28.813186813186814, 49.582417582417584,
                                                     13.989010989010989)}),
                                   Sex: DemographicLevels(
                                       female=Stats(8.0, 31.0, 49.0, 12.0),
                                       male=Stats(10.0, 29.0, 47.0, 14.0),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(9.0, 31.0, 47.0, 13.0),
                                       dmg_eth_ami=Stats(12.0, 36.0, 43.0, 9.0),
                                       dmg_eth_asn=Stats(3.0, 16.0, 53.0, 28.0),
                                       dmg_eth_blk=Stats(17.0, 40.0, 37.0, 6.0),
                                       dmg_eth_hsp=Stats(13.0, 37.0, 43.0, 7.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(2.0, 15.0, 53.0, 30.0),
                                       dmg_eth_wht=Stats(5.3, 24.7, 54.6, 15.4), )}),
        Grade.G10: GradeLevels((9.0, 30.0, 48.0, 13.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(29.0, 42.0, 26.0, 3.0),
                                    False: Stats(7.494623655913978, 29.096774193548388, 49.655913978494624,
                                                 13.75268817204301)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(13.0, 37.0, 42.0, 8.0),
                                        False: Stats(3.697674418604651, 20.72093023255814, 55.95348837209303,
                                                     19.627906976744185)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(29.0, 42.0, 26.0, 3.0),
                                        False: Stats(5.470588235294118, 27.88235294117647, 51.88235294117647,
                                                     14.764705882352942)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(23.0, 42.0, 32.0, 3.0),
                                        False: Stats(7.615384615384615, 28.813186813186814, 49.582417582417584,
                                                     13.989010989010989)}),
                                   Sex: DemographicLevels(
                                       female=Stats(8.0, 31.0, 49.0, 12.0),
                                       male=Stats(10.0, 29.0, 47.0, 14.0),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(9.0, 31.0, 47.0, 13.0),
                                       dmg_eth_ami=Stats(12.0, 36.0, 43.0, 9.0),
                                       dmg_eth_asn=Stats(3.0, 16.0, 53.0, 28.0),
                                       dmg_eth_blk=Stats(17.0, 40.0, 37.0, 6.0),
                                       dmg_eth_hsp=Stats(13.0, 37.0, 43.0, 7.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(2.0, 15.0, 53.0, 30.0),
                                       dmg_eth_wht=Stats(5.3, 24.7, 54.6, 15.4), )}),
        Grade.G11: GradeLevels((7.0, 32.0, 41.0, 20.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(27.0, 50.0, 21.0, 2.0),
                                    False: Stats(5.260869565217392, 30.434782608695652, 42.73913043478261,
                                                 21.565217391304348)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(11.0, 40.0, 37.0, 12.0),
                                        False: Stats(2.6666666666666665, 23.333333333333332, 45.333333333333336,
                                                     28.666666666666668)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(27.0, 50.0, 21.0, 2.0),
                                        False: Stats(3.1904761904761907, 28.571428571428573, 44.80952380952381,
                                                     23.428571428571427)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(22.0, 45.0, 27.0, 6.0),
                                        False: Stats(6.042553191489362, 31.170212765957448, 41.8936170212766,
                                                     20.893617021276597)}),
                                   Sex: DemographicLevels(
                                       female=Stats(6.0, 31.0, 43.0, 20.0),
                                       male=Stats(8.0, 33.0, 38.7647, 20.2353),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(9.0, 31.0, 47.0, 13.0),
                                       dmg_eth_ami=Stats(9.0, 40.0, 39.0, 12.0),
                                       dmg_eth_asn=Stats(2.0, 14.0, 37.0, 47.0),
                                       dmg_eth_blk=Stats(12.9474, 46.052600000000005, 34.0, 7.0),
                                       dmg_eth_hsp=Stats(11.0, 40.0, 39.0, 10.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(2.0, 15.0, 53.0, 30.0),
                                       dmg_eth_wht=Stats(4.0, 25.9149, 45.1915, 24.893600000000003), )}),
        Grade.G12: GradeLevels((9.0, 30.0, 48.0, 13.0),
                               {StudentAccommodation.SECTION_504: DemographicLevels(
                                   {True: Stats(29.0, 42.0, 26.0, 3.0),
                                    False: Stats(7.494623655913978, 29.096774193548388, 49.655913978494624,
                                                 13.75268817204301)}),
                                   StudentStatus.ECON_DIS: DemographicLevels(
                                       {True: Stats(13.0, 37.0, 42.0, 8.0),
                                        False: Stats(3.697674418604651, 20.72093023255814, 55.95348837209303,
                                                     19.627906976744185)}),
                                   StudentAccommodation.IEP: DemographicLevels(
                                       {True: Stats(29.0, 42.0, 26.0, 3.0),
                                        False: Stats(5.470588235294118, 27.88235294117647, 51.88235294117647,
                                                     14.764705882352942)}),
                                   StudentStatus.LEP: DemographicLevels(
                                       {True: Stats(23.0, 42.0, 32.0, 3.0),
                                        False: Stats(7.615384615384615, 28.813186813186814, 49.582417582417584,
                                                     13.989010989010989)}),
                                   Sex: DemographicLevels(
                                       female=Stats(8.0, 31.0, 49.0, 12.0),
                                       male=Stats(10.0, 29.0, 47.0, 14.0),
                                       not_stated=Stats(9.0, 30.0, 51.0, 10.0), ),
                                   Ethnicity: DemographicLevels(
                                       dmg_eth_2mr=Stats(9.0, 31.0, 47.0, 13.0),
                                       dmg_eth_ami=Stats(12.0, 36.0, 43.0, 9.0),
                                       dmg_eth_asn=Stats(3.0, 16.0, 53.0, 28.0),
                                       dmg_eth_blk=Stats(17.0, 40.0, 37.0, 6.0),
                                       dmg_eth_hsp=Stats(13.0, 37.0, 43.0, 7.0),
                                       dmg_eth_nst=Stats(4.0, 47.0, 31.0, 18.0),
                                       dmg_eth_pcf=Stats(2.0, 15.0, 53.0, 30.0),
                                       dmg_eth_wht=Stats(5.3, 24.7, 54.6, 15.4), )}),
    },
}
