"""
Miscellaneous configuration for the PARCC project.

@date: February 25, 2014
"""
import uuid

from data_generation.util.indexed_enum import NoseSafeEnum

GLOBAL_RUN_ID = uuid.uuid4()  # randomly generated global run ID
# TODO this isn't generated deterministically, but there's no sensible place to put it ATM

OPTIONAL_DATA_FILL_RATE = .1  # chance that a given optional data field will be filled


class Subject(NoseSafeEnum):
    ELA = 'ELA'
    Math = 'Math'
