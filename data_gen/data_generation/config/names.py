"""
configuration for name generators
"""
from pathlib import Path
from data_generation.config.hierarchy import SchoolLevel
from data_generation.util.indexed_enum import NoseSafeEnum

NAME_FILES_PATH = Path(__file__).parent.parent.resolve() / 'datafiles'


class NamePath(NoseSafeEnum):
    LAST_DIST = NAME_FILES_PATH / 'dist.all.last'
    FEMALE_FIRST_DIST = NAME_FILES_PATH / 'dist.female.first'
    MALE_FIRST_DIST = NAME_FILES_PATH / 'dist.male.first'
    BIRDS = NAME_FILES_PATH / 'birds.txt'
    FISH = NAME_FILES_PATH / 'fish.txt'
    MAMMALS = NAME_FILES_PATH / 'mammals.txt'
    ANIMALS = NAME_FILES_PATH / 'one-word-animal-names.txt'
    CITIES = NAME_FILES_PATH / 'cities.markov'


def read_names(path):
    with path.open() as fh:
        return tuple(map(str.strip, fh))


class Names(NoseSafeEnum):
    BIRDS = read_names(NamePath.BIRDS.value)
    FISH = read_names(NamePath.FISH.value)
    MAMMALS = read_names(NamePath.MAMMALS.value)
    ANIMALS = read_names(NamePath.ANIMALS.value)
    CITIES = read_names(NamePath.CITIES.value)


DISTRICT_SUFFIXES = ('District',
                     'School District',
                     'Schools',
                     'County Schools',
                     'Public Schools',
                     'SD',
                     'Unified School District',
                     'Independent School District',
                     'Central School District',
                     'Public School District',
                     'School Department',
                     'Local School District',
                     'Area School District',
                     'County School District',
                     'County Public Schools',
                     'Community Schools',
                     'School Corporation',
                     'Community School District',
                     'Regional School District',
                     'Free School District',
                     'Community School Corporation',
                     'School Board',
                     'Area Schools',
                     'Borough School District',
                     'School System',
                     'Municipal Schools',
                     'Consolidated Schools',
                     'Joint School District',
                     'Consolidated Independent School District')

_SCHOOL_SUFFIXES_ALL = ['Ctr', 'Center', 'Sch', 'School', 'Community', 'Charter', 'Charter School', 'Learning Center', 'Center', 'Academy', 'Acad']
SCHOOL_SUFFIXES = {
    SchoolLevel.ELEM_SCHOOL: ['El Sch', 'Elem', 'Elementary School', 'Primary', 'Elementary', 'Elem', 'Elem Sch'] + _SCHOOL_SUFFIXES_ALL,
    SchoolLevel.MIDL_SCHOOL: ['Middle School', 'Middle Sch', 'Community Middle', 'Middle', 'Junior High', 'Junior High School', 'Intermediate', 'Intermediate School', 'Jr Middle', 'MS'] + _SCHOOL_SUFFIXES_ALL,
    SchoolLevel.HIGH_SCHOOL: ['High Sch', 'High School', 'High', 'HS', 'Senior High'] + _SCHOOL_SUFFIXES_ALL,

    SchoolLevel.FULL_SCHOOL: _SCHOOL_SUFFIXES_ALL
}

STREET_SUFFIXES = ['Road', 'Rd.', 'RD', 'Avenue', 'Ave.', 'AVE', 'Street', 'St.', 'ST', 'Parkway', 'Pkwy.', 'PWKY', 'Way', 'WAY', 'Blvd.', 'BLVD']

APARTMENT_PREFIXES = ['#', 'Apt', 'Suite']

NO_MIDDLE_NAME_CHANCE = .3
