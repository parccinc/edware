import logging
import multiprocessing

from data_generation.model.district import District
from data_generation.model.state import State
from data_generation.util.id_gen import IDGen
from data_generation.main import ParccDataGen


class ParccDataGenMulti(ParccDataGen):
    __slots__ = (
        '_out_path',
        '_id_gen',
        '_batch',
        '_record_writer',
        '_output_chunk_size',
        '_thread_count',
    )

    def __init__(self, args):
        super().__init__(args)
        self._thread_count = args.thread_count

    def _generate_state_data(self, state: State) -> dict:
        """
        Generate an entire data set for a single state.

        @param state: State to generate data for
        @return: a dict of assessments by id, needed for generating the LZ JSON file.
        """
        logging.info("  expected number of students in a year: %s" % (state.state_type.mean_student_pop,))
        logging.info("  expected number of districts: %s" % (state.state_type.mean_district_count,))

        self._record_writer.state_initialize(state)

        # Create the assessment objects
        assessments_by_key = self._generate_state_assessments(state)  # dict of assessments by key

        logging.info("    Created %s assessments" % (len(assessments_by_key),))

        # Build the districts
        student_avg_count = 0
        student_unique_count = 0

        # sort so the largest districts are first
        districts = sorted((District(district_type, state, self._id_gen)
                            for district_type in state.state_type.generate_districts(self._id_gen)),
                           key=lambda district: district.district_type.mean_student_pop, reverse=True)

        def get_next_seed():
            while True:
                yield self._id_gen.getrandbits(128)

        with multiprocessing.Pool(initializer=pool_init,
                                  initargs=(self, assessments_by_key),
                                  processes=self._thread_count,
                                  maxtasksperchild=1) as pool:
            for avg_year, unique, num_schools in pool.imap_unordered(_generate_district_data, zip(districts, get_next_seed())):
                student_avg_count += avg_year
                student_unique_count += unique

        self._record_writer.state_finalize(state)

        logging.info('    Created state with average of %i students/year and %i total unique (expected %s /year)' %
                     (student_avg_count, student_unique_count, state.state_type.mean_student_pop))


def pool_init(pdg: ParccDataGenMulti, assessments_by_key):
    """
    initializing a multiprocessing pool thread is vaguely obnoxious. This pattern (globals) is common in some worlds,
    but I'm thinking it might be more clear if we were using threading.local()...

    :param pdg:
    :param assessments_by_key:
    :return:
    """
    global PDG, ASSESSMENTS_BY_KEY
    PDG = pdg
    ASSESSMENTS_BY_KEY = assessments_by_key


def _generate_district_data(args) -> tuple:
    """
    Generate an entire data set for a single district.

    :param args: a tuple consiting of the district and a random seed
    :return: Some statistics for console output
    """
    district, random_seed = args

    global PDG, ASSESSMENTS_BY_KEY  # load values from thread namespace

    # we have to do something to the RNG because otherwise the seed is identical and we generate the same data in each thread...
    # this doesn't guarantee determinacy because the order in which things complete isn't guaranteed (also, iterating over hashes...)
    try:
        PDG._id_gen = IDGen(seed=random_seed)

        results = PDG._generate_district_data(district, ASSESSMENTS_BY_KEY)

        return results

    except Exception as e:
        logging.exception(e)
        raise
