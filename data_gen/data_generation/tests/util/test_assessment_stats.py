import unittest
from data_generation.config.assessment import DEFAULT_OVERALL_LEVELS, DEFAULT_MATH_SUBCLAIM_LEVELS, MATH_SUBCLAIMS
from data_generation.config.config import Subject
from data_generation.config.levels import LEVELS_BY_GRADE_BY_SUBJ
from data_generation.config.population import StudentAccommodation, StudentStatus, Sex, Ethnicity, Demographics, Grade
from data_generation.util import assessment_stats
from data_generation.util.id_gen import IDGen
from collections import Counter
from data_generation.util.indexed_enum import IndexedEnum
import data_generation.util.properties


class TestProperties(unittest.TestCase):
    def test_get(self):
        a = data_generation.util.properties.Properties(a=1)
        self.assertEqual(a.a, 1)

    def test_set(self):
        a = data_generation.util.properties.Properties(a=1)
        a.a = 2
        self.assertEqual(a['a'], 2)
        self.assertEqual(a.a, 2)


class TestStats(unittest.TestCase):
    def test_some_stuff(self):
        a = assessment_stats.Stats(1, 1, 1, 1)
        self.assertEqual(len(a), 4)
        self.assertEqual(a[0], .25)
        self.assertEqual(sum(a), 1)

    def test_absent_stats(self):
        a = assessment_stats.Stats(0, 0, 0, 0)
        self.assertFalse(a)


class TestDemographicLevels(unittest.TestCase):
    def test_init(self):
        assessment_stats.DemographicLevels(
            {True: assessment_stats.Stats(45.00, 37.00, 17.00, 1.00),
             False: assessment_stats.Stats(11.30, 29.39, 51.78, 7.52)})

    def test_values(self):
        a = assessment_stats.DemographicLevels(
            {True: assessment_stats.Stats(45.00, 37.00, 17.00, 1.00),
             False: assessment_stats.Stats(11.30, 29.39, 51.78, 7.52)})

        self.assertEqual(a[True], assessment_stats.Stats(45.00, 37.00, 17.00, 1.00))


class TestGradeLevels(unittest.TestCase):
    def test_init(self):
        assessment_stats.GradeLevels((14.00, 30.00, 49.00, 7.00),
                                     {StudentAccommodation.SECTION_504: assessment_stats.DemographicLevels(
                                         {True: assessment_stats.Stats(45.00, 37.00, 17.00, 1.00),
                                          False: assessment_stats.Stats(11.30, 29.39, 51.78, 7.52)}),
                                      StudentAccommodation.IEP: assessment_stats.DemographicLevels(
                                          {True: assessment_stats.Stats(45.00, 37.00, 17.00, 1.00),
                                           False: assessment_stats.Stats(8.53, 28.76, 54.65, 8.06)}),
                                      StudentStatus.ECON_DIS: assessment_stats.DemographicLevels(
                                          {True: assessment_stats.Stats(20.00, 38.00, 39.00, 3.00),
                                           False: assessment_stats.Stats(6.36, 19.82, 61.73, 12.09)}),
                                      StudentStatus.LEP: assessment_stats.DemographicLevels(
                                          {True: assessment_stats.Stats(38.00, 43.00, 19.00, 0.00),
                                           False: assessment_stats.Stats(11.63, 28.71, 51.97, 7.69)}),
                                      Sex: assessment_stats.DemographicLevels(
                                          {Sex.FEMALE: assessment_stats.Stats(11.00, 29.00, 52.00, 8.00),
                                           Sex.MALE: assessment_stats.Stats(16.00, 33.00, 46.00, 5.00),
                                           Sex.OTHER: assessment_stats.Stats(9.00, 30.00, 51.00, 10.00)}),
                                      Ethnicity: assessment_stats.DemographicLevels(
                                          {Ethnicity.MULTIPLE: assessment_stats.Stats(9.00, 31.00, 47.00, 13.00),
                                           Ethnicity.AMERIND: assessment_stats.Stats(18.00, 36.00, 42.00, 4.00),
                                           Ethnicity.ASIAN: assessment_stats.Stats(8.00, 22.00, 57.00, 13.00),
                                           Ethnicity.BLACK: assessment_stats.Stats(21.00, 40.00, 37.00, 2.00),
                                           Ethnicity.HISPANIC: assessment_stats.Stats(20.00, 39.00, 38.00, 3.00),
                                           Ethnicity.NONE: assessment_stats.Stats(4.00, 47.00, 31.00, 18.00),
                                           Ethnicity.HAWAII: assessment_stats.Stats(0.00, 0.00, 0.00, 0.00),
                                           Ethnicity.WHITE: assessment_stats.Stats(9.00, 25.00, 57.00, 9.00)})})

    def test_values(self):
        a = assessment_stats.GradeLevels((14.00, 30.00, 49.00, 7.00),
                                         {StudentAccommodation.SECTION_504: assessment_stats.DemographicLevels(
                                             {True: assessment_stats.Stats(45.00, 37.00, 17.00, 1.00),
                                              False: assessment_stats.Stats(11.30, 29.39, 51.78, 7.52)}),
                                          StudentAccommodation.IEP: assessment_stats.DemographicLevels(
                                              {True: assessment_stats.Stats(45.00, 37.00, 17.00, 1.00),
                                               False: assessment_stats.Stats(8.53, 28.76, 54.65, 8.06)}),
                                          StudentStatus.ECON_DIS: assessment_stats.DemographicLevels(
                                              {True: assessment_stats.Stats(20.00, 38.00, 39.00, 3.00),
                                               False: assessment_stats.Stats(6.36, 19.82, 61.73, 12.09)}),
                                          StudentStatus.LEP: assessment_stats.DemographicLevels(
                                              {True: assessment_stats.Stats(38.00, 43.00, 19.00, 0.00),
                                               False: assessment_stats.Stats(11.63, 28.71, 51.97, 7.69)}),
                                          Sex: assessment_stats.DemographicLevels(
                                              {Sex.FEMALE: assessment_stats.Stats(11.00, 29.00, 52.00, 8.00),
                                               Sex.MALE: assessment_stats.Stats(16.00, 33.00, 46.00, 5.00),
                                               Sex.OTHER: assessment_stats.Stats(9.00, 30.00, 51.00, 10.00)}),
                                          Ethnicity: assessment_stats.DemographicLevels(
                                              {Ethnicity.MULTIPLE: assessment_stats.Stats(9.00, 31.00, 47.00, 13.00),
                                               Ethnicity.AMERIND: assessment_stats.Stats(18.00, 36.00, 42.00, 4.00),
                                               Ethnicity.ASIAN: assessment_stats.Stats(8.00, 22.00, 57.00, 13.00),
                                               Ethnicity.BLACK: assessment_stats.Stats(21.00, 40.00, 37.00, 2.00),
                                               Ethnicity.HISPANIC: assessment_stats.Stats(20.00, 39.00, 38.00, 3.00),
                                               Ethnicity.NONE: assessment_stats.Stats(4.00, 47.00, 31.00, 18.00),
                                               Ethnicity.HAWAII: assessment_stats.Stats(0.00, 0.00, 0.00, 0.00),
                                               Ethnicity.WHITE: assessment_stats.Stats(9.00, 25.00, 57.00, 9.00)})})

        self.assertEqual(a.num_levels, 4)
        self.assertEqual(a[StudentAccommodation.SECTION_504][True][0], .45)
        self.assertEqual(a[Ethnicity][Ethnicity.WHITE][2], .57)


class TestRandomLevelByDemographics(unittest.TestCase):
    def test_init(self):
        grade = Grade.G11
        subject = Subject.ELA
        demographics = Demographics.CALIFORNIA[grade]
        level_breakdowns = LEVELS_BY_GRADE_BY_SUBJ[subject][grade]
        assessment_stats.RandomLevelByDemographics(demographics, level_breakdowns)

    def test_random_levels(self):
        grade = Grade.G11
        subject = Subject.ELA
        demographics = Demographics.CALIFORNIA[grade]
        level_breakdowns = LEVELS_BY_GRADE_BY_SUBJ[subject][grade]
        thing = assessment_stats.RandomLevelByDemographics(demographics, level_breakdowns)

        entity = {StudentAccommodation.SECTION_504: False,
                  StudentAccommodation.IEP: False,
                  StudentStatus.ECON_DIS: False,
                  StudentStatus.LEP: False,
                  Sex: Sex.MALE,
                  Ethnicity: Ethnicity.WHITE}
        id_gen = IDGen()

        num_iterations = 1000
        level_counts = Counter(thing.random_level(entity, id_gen) for _ in range(num_iterations))

        self.assertGreater(len(level_counts), 1)
        self.assertTrue(set(level_counts).issubset({0, 1, 2, 3, 4}), "level counts %s is not a subset of %s" % (set(level_counts), {0, 1, 2, 3, 4}))


class TestRandomScoreGivenLevel(unittest.TestCase):
    def test_random_score_given_level(self):
        id_gen = IDGen()
        for _ in range(100):
            index = id_gen.randrange(0, len(DEFAULT_OVERALL_LEVELS))
            level = DEFAULT_OVERALL_LEVELS[index]
            score = assessment_stats.random_score_given_level(DEFAULT_OVERALL_LEVELS[index], id_gen)
            self.assertIn(score, level)


class TestAdjustScore(unittest.TestCase):
    def test_adjust_score(self):
        id_gen = IDGen()
        for _ in range(100):
            index = id_gen.randrange(0, len(DEFAULT_OVERALL_LEVELS))
            level = DEFAULT_OVERALL_LEVELS[index]
            score = assessment_stats.random_score_given_level(DEFAULT_OVERALL_LEVELS[index], id_gen)
            scale_factor = id_gen.uniform(-1, 1)
            scaled_score = assessment_stats.adjust_score(score, scale_factor, level, id_gen)
            self.assertIn(scaled_score, level)
