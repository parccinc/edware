import unittest
from data_generation.util.id_gen import IDGen
import uuid
from multiprocessing import Pool, cpu_count
from collections import Counter


class TestIDGen(unittest.TestCase):
    def test_get_uuid(self):
        id_gen = IDGen()
        v = id_gen.get_uuid()
        self.assertIsInstance(v, str)
        self.assertEqual(len(v), 36)
        try:
            uuid.UUID(v)

        except ValueError:
            self.fail("could not coerce %s as a UUID" % (v,))

    def test_get_sr_uuid(self):
        id_gen = IDGen()
        v = id_gen.get_sr_uuid()
        self.assertIsInstance(v, str)
        self.assertEqual(len(v), 32)
        try:
            uuid.UUID(v)

        except ValueError:
            self.fail("could not coerce %s as a UUID" % (v,))

    def test_get_safe_rec_id(self):
        id_gen = IDGen()

        with Pool(initializer=_init_pool, initargs=(id_gen,)) as pool:
            generated_ids = Counter(pool.imap_unordered(_get_safe_rec_id, ('x' for _ in range(100 * cpu_count()))))

        if any(value != 1 for value in generated_ids.values()):
            self.fail("get_safe_rec_id generated duplicate IDs")


def _init_pool(id_gen: IDGen):
    global _ID_GEN
    _ID_GEN = id_gen


def _get_safe_rec_id(key):
    global _ID_GEN
    return _ID_GEN.get_safe_rec_id(key)
