import unittest
import data_generation.util.stats as stats
from data_generation.util.math import largest_remainder_rounding


class TestStats(unittest.TestCase):
    def test_counts_mean(self):
        self.assertAlmostEqual(stats.counts_mean([4, 5, 6, 7]), 1.7272727272727273)
        self.assertAlmostEqual(stats.counts_mean([10, 0, 0, 10, 0, 1]), 1.6666666666666667)
        self.assertAlmostEqual(stats.counts_mean([0, 0, 0, 10, 0, 1]), 3.1818181818181817)

    def test_counts_stdv(self):
        self.assertAlmostEqual(stats.counts_stdv([4, 5, 6, 7]), 1.0946904162538451)
        self.assertAlmostEqual(stats.counts_stdv([10, 0, 0, 10, 0, 1]), 1.6426846010152705)
        self.assertAlmostEqual(stats.counts_stdv([0, 0, 0, 10, 0, 1]), 0.5749595745760689)
        self.assertAlmostEqual(stats.counts_stdv([0, 0, 0, 10, 0, 1], 3.1818181818181817), 0.5749595745760689)

    def test_normalize(self):
        for vals, norm in [[[1, 2, 3, 4, 5], (0.06666666666666667, 0.13333333333333333, 0.2, 0.26666666666666666, 0.3333333333333333)],
                           [[1, 2, 3], (0.16666666666666666, 0.3333333333333333, 0.5)],
                           [[10, 0, 0, 10], (0.5, 0.0, 0.0, 0.5)],
                           [[1], (1.0,)],
                           [[1, 1], (.5, .5)],
                           [[0], (0.0,)],
                           [[], ()],
                           [[0, 0, 0, 0], (0.0, 0.0, 0.0, 0.0)]]:
            normed = stats.normalize(vals)
            self.assertEqual(normed, norm, "%s; expected %s" % (normed, norm))
            if sum(vals):
                self.assertAlmostEqual(sum(normed), 1.0)

            else:
                self.assertEqual(sum(normed), 0.0)

    def test_largest_remainder_rounding(self):
        self.assertListEqual(largest_remainder_rounding([1.0, 1.5, 2.0, 2.5], 7), [1, 1, 2, 3])
        self.assertListEqual(largest_remainder_rounding([2.5925925925925926, 1.037037037037037, 0.5185185185185185, 2.8518518518518516], 7), [3, 1, 0, 3])
