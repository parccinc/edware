import unittest
from data_generation.util import weighted_choice as wc
from collections import Counter
from data_generation.util.id_gen import IDGen


class TestWeightedChoice(unittest.TestCase):
    def test_weighted_choice_even(self):
        weights = {1: .5, 2: .5}
        counts = Counter(wc.weighted_choice(weights) for _ in range(1000))
        self.assertEqual(len(counts), 2)
        self.assertEqual(set(counts), {1, 2})


class TestWeightedChooser(unittest.TestCase):
    def test_weighted_chooser(self):
        id_gen = IDGen()
        weights = {'a': 1, 'b': 2, 'c': 3, 'd': 4}
        chooser = wc.WeightedChooser(weights)
        counts = Counter(chooser.choose(id_gen) for _ in range(1000))
        self.assertEqual(len(counts), 4)
        self.assertEqual(set(counts), set('abcd'))


class TestDiscreteDistribution(unittest.TestCase):
    def test_discrete_distribution(self):
        weights = {'a': 1, 'b': 2, 'c': 3, 'd': 4}
        distribution = wc.DiscreteDistribution(weights)
        self.assertEqual(set(distribution), set(weights))
        self.assertEqual(distribution['a'], 1)
