import unittest
from data_generation.util.indexed_enum import IndexedEnum


class TestIndexedEnum(unittest.TestCase):
    def test_indexed_enum(self):
        class TestEnum(IndexedEnum):
            ONE = "foo 1"
            TWO = "bar 2"
            THREE = "baz 3"
            FOUR = "qux 4"
            FIVE = "fnord 5"

        self.assertEqual(TestEnum[0], TestEnum.ONE)
        self.assertEqual(TestEnum[1], TestEnum.TWO)
        self.assertEqual(TestEnum[2], TestEnum.THREE)
        self.assertEqual(TestEnum[3], TestEnum.FOUR)
        self.assertEqual(TestEnum[4], TestEnum.FIVE)

        self.assertEqual(TestEnum['ONE'], TestEnum.ONE)
        self.assertEqual(TestEnum['TWO'], TestEnum.TWO)
        self.assertEqual(TestEnum['THREE'], TestEnum.THREE)
        self.assertEqual(TestEnum['FOUR'], TestEnum.FOUR)
        self.assertEqual(TestEnum['FIVE'], TestEnum.FIVE)
