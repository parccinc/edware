import unittest
import data_generation.util.integer_partition as ip


class TestPParts(unittest.TestCase):
    def test_p_parts(self):
        self.assertEqual(tuple(ip._p_parts(0)), ())
        self.assertEqual(tuple(ip._p_parts(1)), (1,))
        self.assertEqual(tuple(ip._p_parts(2)), (2,))
        self.assertEqual(tuple(ip._p_parts(3)), (3,))
        self.assertEqual(tuple(ip._p_parts(4)), (5,))
        self.assertEqual(tuple(ip._p_parts(5)), (8, -1))
        self.assertEqual(tuple(ip._p_parts(6)), (12, -1))
        self.assertEqual(tuple(ip._p_parts(7)), (18, -3))
        self.assertEqual(tuple(ip._p_parts(100)), (319428011, -186661596, 74275466, -19221664, 2979025, -236786, 6859, -23))

    def test_p(self):
        self.assertEqual(ip.p(0), 1)
        self.assertEqual(ip.p(1), 1)
        self.assertEqual(ip.p(2), 2)
        self.assertEqual(ip.p(3), 3)
        self.assertEqual(ip.p(4), 5)
        self.assertEqual(ip.p(5), 7)
        self.assertEqual(ip.p(6), 11)
        self.assertEqual(ip.p(7), 15)
        self.assertEqual(ip.p(130), 5371315400)

    def test_q(self):
        self.assertEqual(ip.q(0, 0), 1)
        self.assertEqual(ip.q(10, 10), 1)
        self.assertEqual(ip.q(10, 2), 5)
        self.assertEqual(ip.q(30, 12), 366)
        self.assertEqual(ip.q(130, 15), 161078251)

    def test_random_integer_partition(self):
        for _ in range(100):
            parts = ip.random_integer_partition(130, 15)
            self.assertEqual(len(parts), 15)
            self.assertEqual(sum(parts), 130)

    def test_random_integer_partition_1(self):
        for _ in range(100):
            parts = ip.random_integer_partition_1(130)
            self.assertIn(len(parts), range(131))
            self.assertEqual(sum(parts), 130)

    def test_partition_by_index(self):
        self.assertEqual(ip.partition_by_index(0, 130), [130])
        self.assertEqual(ip.partition_by_index(50, 130), [80, 50])
        self.assertEqual(ip.partition_by_index(5371315400 - 1, 130), [1] * 130)
