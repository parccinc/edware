from collections import defaultdict, Counter
import os
import unittest
from pathlib import Path
from data_generation.config.assessment import AssessmentSubject, RecordType, ReportSuppressionCode, \
    ReportSuppressionAction, ComponentAttemptCategory, TestPeriod
from data_generation.config.batch import Batch
from data_generation.config.config import Subject
from data_generation.config.hierarchy import StateName
from data_generation.config.population import Ethnicity, PRGDisabilityType, AccommodationColorContrast, \
    AccommodationRead, AccommodationBraille, AccommodationSpeechToText, AccommodationMathRAP, \
    AccommodationLoudNativeLang, AccommodationExtendedTime, AccommodationTranslation
from data_generation.util.properties import Properties
from generate_data import parse_args, main
import csv
from collections import namedtuple
import re
from datetime import datetime
from uuid import UUID
from tempfile import TemporaryDirectory
import shlex


SpecInfo = namedtuple('SpecInfo', ('data_type', 'allowed', 'required', 'values'))


class TestMain(unittest.TestCase):
    def _load_spec(self):
        spec_path = Path(__file__).parent / '../../specs/summative_intake_spec.csv'

        spec_data = {}
        with spec_path.open() as fh:
            spec = csv.DictReader(fh)
            for row in spec:
                name = row['CSV Name'].strip()
                if not name or len(name) > 64:
                    raise Exception("csv field name too long!: %s" % (name,))
                datatype = row['Datatype'].strip()
                allowed = row['Allowed'].strip()
                required = row['Required'].strip()
                values = row['Values'].strip()

                spec_data[name] = SpecInfo(datatype, allowed, required, values)

        return spec_data

    def setUp(self):
        self._spec = self._load_spec()
        self._tmp_dir = TemporaryDirectory()
        self._tmp_path = Path(self._tmp_dir.__enter__())

    def tearDown(self):
        self._tmp_dir.__exit__(None, None, None)
        del self._tmp_dir

    def _validate_value(self, key, value, spec_info: SpecInfo, row_type):
        required = spec_info.required == 'Y' or row_type in spec_info.required.split(', ')
        allowed = spec_info.allowed == 'Y' or row_type in spec_info.allowed.split(', ')  # is the field required to be None here?

        if required and not value:
            self.fail("cannot have empty value for %r in a record of type %r" % (key, row_type))

        if not allowed and value:
            self.fail("cannot have value %r for %r in a record of type %r" % (value, key, row_type))

        if value:
            if re.match('(?:var)?char', spec_info.data_type):
                max_len = int(re.search('\d+', spec_info.data_type).group())
                if len(value) > max_len:
                    self.fail("value for %s too long: %s (max %s, is %s)" % (key, value, max_len, len(value)))

            elif re.match('numeric', spec_info.data_type):
                all_len, dec_len = map(int, re.findall('\d+', spec_info.data_type))
                if dec_len != 0:
                    pre_len = all_len - dec_len
                    if not re.match('^\d{,%s}(?:\.\d{,%s})' % (pre_len, dec_len), value):
                        self.fail("invalid value for %s: %s" % (key, value))

                else:
                    if int(value) >= 10 ** all_len:
                        self.fail("invalid value for %s: %s" % (key, value))

            elif spec_info.data_type == 'int2':
                v = int(value)
                if v > 2 ** 15 or v < -(2 ** 15):
                    self.fail("invalid value for %s: %s" % (key, value))

            elif spec_info.data_type == 'date':
                datetime.strptime(value, '%Y-%m-%d')

            else:
                raise NotImplementedError("unknown data type? %s" % (spec_info.data_type,))

            if spec_info.values:
                if spec_info.values == '_UUID_':
                    UUID(value)

                elif spec_info.values == '_DATE_':
                    datetime.strptime(value, '%Y-%m-%d')

                elif spec_info.values == '_FLOAT_':
                    float(value)

                elif not re.match("^" + spec_info.values + "$", value):
                    self.fail("invalid value for %s: %s" % (key, value))

    def _validate_row(self, row: dict):
        row_type = row['RecordType']
        for key, value in row.items():
            spec_info = self._spec[key]
            self._validate_value(key, value, spec_info, row_type)

    def _lz_csv_iter(self, subject: Subject, state: StateName, fail_if_none_found=True):
        """
        get an iterator of all csv files output by the LZ writer for a particular subject

        assumes that they have the "name" of the assessment subject in the path somewhere.
        """
        base_path = self._tmp_path / 'LZ' / state.code

        files_in_base = []
        for dirpath, dirnames, filenames in os.walk(str(self._tmp_path)):
            for dirname in dirnames:
                files_in_base.append(os.path.join(dirpath, dirname, ''))

            for filename in filenames:
                files_in_base.append(os.path.join(dirpath, filename))

        if not base_path.exists():
            raise Exception("%s does not exist?\n%s"
                            % (base_path, files_in_base))

        assessment_subjects = [
            assessment_subject for assessment_subject in AssessmentSubject
            if assessment_subject.subject == subject
        ]
        path_re = re.compile('|'.join(assessment_subject.name for assessment_subject in assessment_subjects))

        anything_found = False
        for csv_file in base_path.rglob('*.csv'):
            if path_re.search(str(csv_file.relative_to(base_path))):
                yield csv_file
                anything_found = True

        if not anything_found and fail_if_none_found:
            raise Exception("No lz CSV files found w/ params\n%s\n%s\n%s"
                            % (base_path, path_re.pattern, files_in_base))

    def _get_column_value_counts(self, subject: Subject, state: StateName):
        column_sets = defaultdict(Properties)

        for csv_path in self._lz_csv_iter(subject, state):
            with csv_path.open() as data_fh:
                data = csv.DictReader(data_fh)
                for row in data:
                    record_type = row['RecordType']
                    for k, v in row.items():
                        column_sets[record_type].setdefault(k, Counter())
                        column_sets[record_type][k][v] += 1

                        column_sets["ALL"].setdefault(k, Counter())
                        column_sets["ALL"][k][v] += 1

        return column_sets

    def _validate_rows(self, subject: Subject, state: StateName):
        for csv_path in self._lz_csv_iter(subject, state):
            with csv_path.open() as data_fh:
                data = csv.DictReader(data_fh)
                for row in data:
                    self._validate_row(row)

    def test_generate(self):
        args = parse_args(shlex.split('-f -O UDL_NO_GPG -o {out_dir} -b DEVEL_TINY'
                                      .format(out_dir=shlex.quote(str(self._tmp_path)))))
        main(args)

        spec_set = set(self._spec)

        math_column_value_counts = self._get_column_value_counts(Subject.Math, Batch.DEVEL_TINY.state_configs[0].state_name)
        ela_column_value_counts = self._get_column_value_counts(Subject.ELA, Batch.DEVEL_TINY.state_configs[0].state_name)

        self.assertEqual(set(math_column_value_counts["ALL"]), spec_set)
        self.assertEqual(set(ela_column_value_counts["ALL"]), spec_set)

        self._validate_rows(Subject.Math, Batch.DEVEL_TINY.state_configs[0].state_name)
        self._validate_rows(Subject.ELA, Batch.DEVEL_TINY.state_configs[0].state_name)

    def test_generate_multi(self):
        args = parse_args(shlex.split('-f -O UDL_NO_GPG -o {out_dir} -b TEST_DISTRICTS -m'
                                      .format(out_dir=shlex.quote(str(self._tmp_path)))))
        main(args)

        spec_set = set(self._spec)

        math_column_value_counts = self._get_column_value_counts(Subject.Math, Batch.TEST_DISTRICTS.state_configs[0].state_name)
        ela_column_value_counts = self._get_column_value_counts(Subject.ELA, Batch.TEST_DISTRICTS.state_configs[0].state_name)

        self.assertEqual(set(math_column_value_counts["ALL"]), spec_set)
        self.assertEqual(set(ela_column_value_counts["ALL"]), spec_set)

        self._validate_rows(Subject.Math, Batch.TEST_DISTRICTS.state_configs[0].state_name)
        self._validate_rows(Subject.ELA, Batch.TEST_DISTRICTS.state_configs[0].state_name)

    def test_custom_batch(self):
        args = parse_args(shlex.split('-f -O UDL_NO_GPG -o {out_dir} -b CUSTOM '
                                      '--custom-batch-mean-pop 1000 '
                                      '--custom-batch-mean-district-count 1 '
                                      '--custom-batch-mean-school-count 1 '
                                      '--custom-batch-state-codes NY'
                                      .format(out_dir=shlex.quote(str(self._tmp_path)))))
        main(args)

        spec_set = set(self._spec)

        math_column_value_counts = self._get_column_value_counts(Subject.Math, StateName.NEW_YORK)
        ela_column_value_counts = self._get_column_value_counts(Subject.ELA, StateName.NEW_YORK)

        self.assertEqual(set(math_column_value_counts["ALL"]), spec_set)
        self.assertEqual(set(ela_column_value_counts["ALL"]), spec_set)

        self._validate_rows(Subject.Math, StateName.NEW_YORK)
        self._validate_rows(Subject.ELA, StateName.NEW_YORK)

    def test_value_coverage(self):
        args = parse_args(shlex.split('-f -O UDL_NO_GPG -o {out_dir} -b TEST_COVERAGE -m'
                                      .format(out_dir=shlex.quote(str(self._tmp_path)))))
        main(args)

        math_column_value_counts = self._get_column_value_counts(Subject.Math, Batch.TEST_COVERAGE.state_configs[0].state_name)
        ela_column_value_counts = self._get_column_value_counts(Subject.ELA, Batch.TEST_COVERAGE.state_configs[0].state_name)

        for subject, column_value_counts in zip((Subject.Math, Subject.ELA), (math_column_value_counts, ela_column_value_counts)):
            if RecordType.SUMMATIVE.value not in column_value_counts:
                self.fail("%s not in %s?" % (RecordType.SUMMATIVE.value, set(column_value_counts)))

            all_column_value_counts = column_value_counts["ALL"]
            summative_column_value_counts = column_value_counts[RecordType.SUMMATIVE.value]
            component_column_value_counts = column_value_counts[RecordType.COMPONENT.value]
            no_component_column_value_counts = column_value_counts[RecordType.NO_COMPONENT.value]

            self.assertIn('RecordType', summative_column_value_counts, msg="Record type not in %s?" % (summative_column_value_counts,))

            self.assertEqual(set(all_column_value_counts.RecordType), {record_type.value for record_type in RecordType})

            self.assertEqual(set(summative_column_value_counts.ReportedSummativeScoreFlag), {'Y'})
            self.assertEqual(set(component_column_value_counts.ReportedSummativeScoreFlag), {''})
            self.assertEqual(set(no_component_column_value_counts.ReportedSummativeScoreFlag), {''})

            self.assertEqual(set(summative_column_value_counts.ReportedRosterFlag), {''})
            self.assertEqual(set(component_column_value_counts.ReportedRosterFlag), {'Y'})
            self.assertEqual(set(no_component_column_value_counts.ReportedRosterFlag), {'Y'})

            self.assertEqual(set(summative_column_value_counts.ReportSuppressionCode), {report_suppresion_code.value if report_suppresion_code.value else '' for report_suppresion_code in ReportSuppressionCode})
            self.assertEqual(set(component_column_value_counts.ReportSuppressionCode), {''})
            self.assertEqual(set(no_component_column_value_counts.ReportSuppressionCode), {''})

            self.assertEqual(set(summative_column_value_counts.ReportSuppressionAction), {report_suppresion_action.value if report_suppresion_action.value else '' for report_suppresion_action in ReportSuppressionAction})
            self.assertEqual(set(component_column_value_counts.ReportSuppressionAction), {''})
            self.assertEqual(set(no_component_column_value_counts.ReportSuppressionAction), {''})

            self.assertEqual(len(all_column_value_counts.AssessmentYear), 3)

            self.assertEqual(set(summative_column_value_counts.PBA03Category), {''})
            self.assertEqual(set(component_column_value_counts.PBA03Category), {''})
            self.assertTrue(set(no_component_column_value_counts.PBA03Category).issubset({category.value for category in ComponentAttemptCategory}))

            self.assertEqual(set(summative_column_value_counts.EOY03Category), {''})
            self.assertEqual(set(component_column_value_counts.EOY03Category), {''})
            self.assertTrue(set(no_component_column_value_counts.EOY03Category).issubset({category.value for category in ComponentAttemptCategory}))

            self.assertEqual(set(all_column_value_counts.StateAbbreviation), {'FE'})

            self.assertEqual(len(all_column_value_counts.ResponsibleDistrictIdentifier), len(all_column_value_counts.ResponsibleDistrictName))
            self.assertEqual(len(all_column_value_counts.PBATestingDistrictIdentifier), len(all_column_value_counts.PBATestingDistrictName))
            self.assertEqual(len(all_column_value_counts.EOYTestingDistrictIdentifier), len(all_column_value_counts.EOYTestingDistrictName))

            self.assertEqual(len(all_column_value_counts.ResponsibleSchoolInstitutionIdentifier), len(all_column_value_counts.ResponsibleSchoolInstitutionName))
            self.assertEqual(len(all_column_value_counts.PBATestingSchoolInstitutionIdentifier), len(all_column_value_counts.PBATestingSchoolInstitutionName))
            self.assertEqual(len(all_column_value_counts.EOYTestingSchoolInstitutionIdentifier), len(all_column_value_counts.EOYTestingSchoolInstitutionName))

            self.assertEqual(set(all_column_value_counts.Sex), {'M', 'F'})
            self.assertEqual(set(all_column_value_counts.GradeLevelWhenAssessed), {'03', '04', '05', '06', '07', '08', '09', '10', '11'})

            if subject == Subject.ELA:
                self.assertEqual(set(all_column_value_counts.AssessmentGrade), {'Grade 3', 'Grade 4', 'Grade 5', 'Grade 6', 'Grade 7', 'Grade 8', 'Grade 9', 'Grade 10', 'Grade 11'})

            else:
                self.assertEqual(set(all_column_value_counts.AssessmentGrade), {'Grade 3', 'Grade 4', 'Grade 5', 'Grade 6', 'Grade 7', 'Grade 8', ''})

            self.assertEqual(set(all_column_value_counts.HispanicorLatinoEthnicity), {'Y', 'N'})
            self.assertEqual(set(all_column_value_counts.AmericanIndianorAlaskaNative), {'Y', 'N'})
            self.assertEqual(set(all_column_value_counts.Asian), {'Y', 'N'})
            self.assertEqual(set(all_column_value_counts.BlackorAfricanAmerican), {'Y', 'N'})
            self.assertEqual(set(all_column_value_counts.NativeHawaiianorOtherPacificIslander), {'Y', 'N'})
            self.assertEqual(set(all_column_value_counts.White), {'Y', 'N'})
            self.assertEqual(set(all_column_value_counts.TwoorMoreRaces), {'Y', 'N'})
            self.assertEqual(set(all_column_value_counts.FederalRaceEthnicity), {ethnicity.federal_code if ethnicity.federal_code else '' for ethnicity in Ethnicity})

            self.assertEqual(set(all_column_value_counts.EnglishLearner), {'Y', 'N'})
            self.assertEqual(set(all_column_value_counts.GiftedandTalented), {'Y', 'N'})
            self.assertEqual(set(all_column_value_counts.MigrantStatus), {'Y', 'N'})
            self.assertEqual(set(all_column_value_counts.EconomicDisadvantageStatus), {'Y', 'N'})
            self.assertEqual(set(all_column_value_counts.StudentWithDisabilities), {'Y', 'N'})
            self.assertEqual(set(all_column_value_counts.PrimaryDisabilityType), set([dt.name for dt in PRGDisabilityType] + ['']))

            self.assertEqual(set(all_column_value_counts.AssessmentAccommodationEnglishLearner), {'Y', ''})
            self.assertEqual(set(all_column_value_counts.AssessmentAccommodation504), {'Y', ''})
            self.assertEqual(set(all_column_value_counts.AssessmentAccommodationIndividualizedEducationalPlanIEP), {'Y', ''})

            self.assertEqual(set(all_column_value_counts.FrequentBreaks), {"Y", ""})
            self.assertEqual(set(all_column_value_counts.SeparateAlternateLocation), {"Y", ""})
            self.assertEqual(set(all_column_value_counts.SmallTestingGroup), {"Y", ""})
            self.assertEqual(set(all_column_value_counts.SpecializedEquipmentorFurniture), {"Y", ""})
            self.assertEqual(set(all_column_value_counts.SpecifiedAreaorSetting), {"Y", ""})
            self.assertEqual(set(all_column_value_counts.TimeOfDay), {"Y", ""})
            self.assertEqual(set(all_column_value_counts.AnswerMasking), {"Y", ""})
            self.assertEqual(set(all_column_value_counts.ColorContrast), set([cc.value for cc in AccommodationColorContrast] + ['']))
            self.assertEqual(set(all_column_value_counts.ASLVideo), {"Y", ""})
            self.assertEqual(set(all_column_value_counts.ScreenReaderORotherAssistiveTechnologyATApplication), {"Y", ""})
            self.assertEqual(set(all_column_value_counts.TactileGraphics), {"Y", ""})
            self.assertEqual(set(all_column_value_counts.AnswersRecordedinTestBook), {"Y", ""})
            self.assertEqual(set(all_column_value_counts.BrailleResponse), set([ab.value for ab in AccommodationBraille] + ['']))
            self.assertEqual(set(all_column_value_counts.MonitorTestResponse), {"Y", ""})
            self.assertEqual(set(all_column_value_counts.WordPrediction), {"Y", ""})
            self.assertEqual(set(all_column_value_counts.AdministrationDirectionsClarifiedinStudentsNativeLanguage), {"Y", ""})
            self.assertEqual(set(all_column_value_counts.AdministrationDirectionsReadAloudinStudentsNativeLanguage), set([alnl.value for alnl in AccommodationLoudNativeLang] + ['']))
            self.assertEqual(set(all_column_value_counts.WordtoWordDictionaryEnglishNativeLanguage), {"Y", ""})
            self.assertEqual(set(all_column_value_counts.ExtendedTime), set([aet.value for aet in AccommodationExtendedTime] + ['']))
            self.assertEqual(set(all_column_value_counts.AlternateRepresentationPaperTest), {"Y", ""})
            self.assertEqual(set(all_column_value_counts.HumanReaderorHumanSigner), set([ar.value for ar in AccommodationRead] + ['']))
            self.assertEqual(set(all_column_value_counts.LargePrint), {"Y", ""})
            self.assertEqual(set(all_column_value_counts.BraillewithTactileGraphics), {"Y", ""})

            if subject == Subject.Math:
                self.assertEqual(set(all_column_value_counts.MathematicsResponse), set([as2t.value for as2t in AccommodationSpeechToText] + ['']))
                self.assertEqual(set(all_column_value_counts.MathematicsResponseEL), set([amr.value for amr in AccommodationMathRAP] + ['']))
                self.assertEqual(set(all_column_value_counts.TexttoSpeechforMathematics), {"Y", ""})
                self.assertEqual(set(all_column_value_counts.CalculationDeviceandMathematicsTools), {"Y", ""})
                self.assertEqual(set(all_column_value_counts.HumanReaderorHumanSignerforMathematics), set([ar.value for ar in AccommodationRead] + ['']))
                self.assertEqual(set(all_column_value_counts.TranslationoftheMathematicsAssessmentinPaper), set([at.value for at in AccommodationTranslation] + ['']))
                self.assertEqual(set(all_column_value_counts.TranslationoftheMathematicsAssessmentinTexttoSpeech), set([at.value for at in AccommodationTranslation] + ['']))
                self.assertEqual(set(all_column_value_counts.TranslationoftheMathematicsAssessmentOnline), set([at.value for at in AccommodationTranslation] + ['']))

                self.assertEqual(set(all_column_value_counts.ClosedCaptioningforELAL), {''})
                self.assertEqual(set(all_column_value_counts.HumanReaderorHumanSignerforELAL), {''})
                self.assertEqual(set(all_column_value_counts.RefreshableBrailleDisplayforELAL), {''})
                self.assertEqual(set(all_column_value_counts.TexttoSpeechforELAL), {''})
                self.assertEqual(set(all_column_value_counts.ELALConstructedResponse), {''})
                self.assertEqual(set(all_column_value_counts.ELALSelectedResponseorTechnologyEnhancedItems), {''})

            else:
                self.assertEqual(set(all_column_value_counts.MathematicsResponse), {''})
                self.assertEqual(set(all_column_value_counts.MathematicsResponseEL), {''})
                self.assertEqual(set(all_column_value_counts.TexttoSpeechforMathematics), {''})
                self.assertEqual(set(all_column_value_counts.CalculationDeviceandMathematicsTools), {''})
                self.assertEqual(set(all_column_value_counts.HumanReaderorHumanSignerforMathematics), {''})
                self.assertEqual(set(all_column_value_counts.TranslationoftheMathematicsAssessmentinPaper), {''})
                self.assertEqual(set(all_column_value_counts.TranslationoftheMathematicsAssessmentinTexttoSpeech), {''})
                self.assertEqual(set(all_column_value_counts.TranslationoftheMathematicsAssessmentOnline), {''})

                self.assertEqual(set(all_column_value_counts.ClosedCaptioningforELAL), {"Y", ""})
                self.assertEqual(set(all_column_value_counts.HumanReaderorHumanSignerforELAL), set([ar.value for ar in AccommodationRead] + ['']))
                self.assertEqual(set(all_column_value_counts.RefreshableBrailleDisplayforELAL), {"Y", ""})
                self.assertEqual(set(all_column_value_counts.TexttoSpeechforELAL), {"Y", ""})
                self.assertEqual(set(all_column_value_counts.ELALConstructedResponse), set([as2t.value for as2t in AccommodationSpeechToText] + ['']))
                self.assertEqual(set(all_column_value_counts.ELALSelectedResponseorTechnologyEnhancedItems), set([as2t.value for as2t in AccommodationSpeechToText] + ['']))

            self.assertEqual(set(all_column_value_counts.Period), set(period.label for period in TestPeriod))
            self.assertEqual(set(all_column_value_counts.TestCode), set(tc.name for tc in AssessmentSubject if tc.subject == subject))
            self.assertEqual(set(all_column_value_counts.Subject), set(tc.label for tc in AssessmentSubject if tc.subject == subject))
