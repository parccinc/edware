import unittest
import uuid
from data_generation.config import config
import data_generation.config.assessment
import data_generation.config.config
import data_generation.config.population


def bigrams(iterable):
    """
    iterate over pairs of things

    :param iterable:
    :return:
    """
    i = iter(iterable)
    a = next(i)
    while True:
        b = next(i)
        yield a, b
        a = b


class TestConfig(unittest.TestCase):
    def test_global_run_id(self):
        self.assertIsInstance(config.GLOBAL_RUN_ID, uuid.UUID)

    def test_subject(self):
        subjects = set(data_generation.config.config.Subject)
        self.assertEqual(len(subjects), 2)
        self.assertIn('Math', [subject.name for subject in subjects])
        self.assertIn('ELA', [subject.name for subject in subjects])
