import unittest
import random
from data_generation.config.hierarchy import SchoolType, SchoolLevel
from data_generation.config.population import Sex
from data_generation.util.names import generate_person_name, generate_district_name, generate_school_name, \
    generate_street_address_line_1, generate_street_address_line_2, generate_street_address_city
import re


class TestGeneratePersonName(unittest.TestCase):
    def test_generate_male(self):
        for _ in range(100):
            name = generate_person_name(Sex.MALE, random, no_middle_name_chance=0.0)
            self.assertEqual(len(name), 3)
            first, middle, last = name
            self.assertTrue(bool(re.match('^[A-Za-z]+$', first)))
            self.assertTrue(bool(re.match('^[A-Za-z]+$', middle)))
            self.assertTrue(bool(re.match('^[A-Za-z]+$', last)))

    def test_generate_female(self):
        for _ in range(100):
            name = generate_person_name(Sex.FEMALE, random, no_middle_name_chance=0.0)
            self.assertEqual(len(name), 3)
            first, middle, last = name
            self.assertTrue(bool(re.match('^[A-Za-z]+$', first)))
            self.assertTrue(bool(re.match('^[A-Za-z]+$', middle)))
            self.assertTrue(bool(re.match('^[A-Za-z]+$', last)))

    def test_generate_no_sex(self):
        for _ in range(100):
            name = generate_person_name(Sex.OTHER, random, no_middle_name_chance=0.0)
            self.assertEqual(len(name), 3)
            first, middle, last = name
            self.assertTrue(bool(re.match('^[A-Za-z]+$', first)))
            self.assertTrue(bool(re.match('^[A-Za-z]+$', middle)))
            self.assertTrue(bool(re.match('^[A-Za-z]+$', last)))

    def test_generate_no_middle(self):
        for _ in range(100):
            name = generate_person_name(Sex.OTHER, random, empty_middle_name=None, no_middle_name_chance=1.0)
            self.assertEqual(len(name), 3)
            first, middle, last = name
            self.assertTrue(bool(re.match('^[A-Za-z]+$', first)))
            self.assertIsNone(middle)
            self.assertTrue(bool(re.match('^[A-Za-z]+$', last)))


class _TestGenerateName:
    def test_generate_name(self):
        for _ in range(100):
            name = self._generate_name(id_gen=random)
            self.assertIsInstance(name, str)
            self.assertGreater(len(name), 1)

    def test_max_len(self):
        for _ in range(100):
            name = self._generate_name(id_gen=random, max_name_length=30)
            self.assertIsInstance(name, str)
            self.assertGreater(len(name), 1)
            self.assertLessEqual(len(name), 30)


class TestDistrictName(unittest.TestCase, _TestGenerateName):
    _generate_name = staticmethod(generate_district_name)


class TestStreetAddress1(unittest.TestCase, _TestGenerateName):
    _generate_name = staticmethod(generate_street_address_line_1)


class TestStreetAddress2(unittest.TestCase, _TestGenerateName):
    _generate_name = staticmethod(generate_street_address_line_2)


class TestCity(unittest.TestCase, _TestGenerateName):
    _generate_name = staticmethod(generate_street_address_city)


class TestSchoolName(unittest.TestCase):
    def test_generate_name(self):
        for _ in range(100):
            for school_type in SchoolLevel:
                name = generate_school_name(school_type, id_gen=random)
                self.assertIsInstance(name, str)
                self.assertGreater(len(name), 1)

    def test_max_len(self):
        for _ in range(100):
            for school_type in SchoolLevel:
                name = generate_school_name(school_type, id_gen=random, max_name_length=30)
                self.assertIsInstance(name, str)
                self.assertGreater(len(name), 1)
                self.assertLessEqual(len(name), 30)
