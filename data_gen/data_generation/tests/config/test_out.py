import unittest
from data_generation.config import out


class TestOut(unittest.TestCase):
    def test_lz_columns(self):
        self.assertEquals(len(out.PARCCSummativeFormat.get_columns()), 153)

    def test_rpt_columns(self):
        self.assertEquals(len(out.PARCCReportingSummativeFormat.get_columns()), 163)
