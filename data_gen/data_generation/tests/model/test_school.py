import unittest
from data_generation.config.assessment import AssessmentSubject
from data_generation.config.batch import DEFAULT_GRADES
from data_generation.config.hierarchy import ExampleState, ExampleDistrict, \
    ExampleSchool, GRADE_SIZE_FUDGE
from data_generation.model.district import District
from data_generation.model.school import School
from data_generation.model.state import State
from data_generation.model.student import Student
from data_generation.util.id_gen import IDGen


class TestSchool(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._id_gen = IDGen()
        cls._state = State(ExampleState.DEVEL_TINY.value, cls._id_gen)
        cls._district = District(ExampleDistrict.TEST_TINY.value, cls._state, cls._id_gen)

    def test_initialize(self):
        school = School(ExampleSchool.TEST_ELEM_TINY.value, self._district, DEFAULT_GRADES, self._id_gen)
        self.assertIsInstance(school.name, str)
        self.assertTrue(bool(school.name))
        self.assertIsInstance(school.identifier, str)
        self.assertTrue(bool(school.identifier))
        self.assertEqual(school.district, self._district)
        self.assertEqual(school.school_type, ExampleSchool.TEST_ELEM_TINY.value)
        self.assertEqual(school.demographics, ExampleState.DEVEL_TINY.value.demographics)
        self.assertEqual(school.grades, ExampleSchool.TEST_ELEM_TINY.value.grades_of_interest(DEFAULT_GRADES))

    def test_get_hs_math_assessment_for_grade(self):
        all_found_hs_math_assessments = set()

        for _ in range(100):
            school = School(ExampleSchool.TEST_HIGH_TINY.value, self._district, DEFAULT_GRADES, self._id_gen)
            for grade in school.grades:
                hs_math_asmt = school.get_hs_math_assessment_for_grade(grade)
                all_found_hs_math_assessments.add(hs_math_asmt)
                self.assertIsInstance(hs_math_asmt, AssessmentSubject)

        self.assertEqual({AssessmentSubject.ALG01, AssessmentSubject.ALG02, AssessmentSubject.GEO01,
                          AssessmentSubject.MAT1I, AssessmentSubject.MAT2I, AssessmentSubject.MAT3I},
                         all_found_hs_math_assessments)

    def test_repopulate_school_grade_empty(self):
        for _ in range(100):
            school = School(ExampleSchool.TEST_HIGH_TINY.value, self._district, DEFAULT_GRADES, self._id_gen)

            grade = school.grades[0]
            grade_students = []
            school.repopulate_school_grade(grade,
                                           grade_students,
                                           self._id_gen,
                                           2012,
                                           additional_student_choice=[0])

            self.assertTrue(all(isinstance(student, Student) for student in grade_students))
            abs_min_grade_size = round(ExampleSchool.TEST_HIGH_TINY.value.mean_grade_pop * ((1 - GRADE_SIZE_FUDGE) ** 2))
            abs_max_grade_size = round(ExampleSchool.TEST_HIGH_TINY.value.mean_grade_pop * ((1 + GRADE_SIZE_FUDGE) ** 2))
            self.assertTrue(abs_min_grade_size <= len(grade_students) <= abs_max_grade_size,
                            "num students not in expected range: %s <= %s <= %s" % (abs_min_grade_size, len(grade_students), abs_max_grade_size))

    def test_repopulate_school_grade_part_full(self):
        for _ in range(100):
            school = School(ExampleSchool.TEST_HIGH_TINY.value, self._district, DEFAULT_GRADES, self._id_gen)
            grade = school.grades[0]

            abs_min_grade_size = round(ExampleSchool.TEST_HIGH_TINY.value.mean_grade_pop * ((1 - GRADE_SIZE_FUDGE) ** 2))
            abs_max_grade_size = round(ExampleSchool.TEST_HIGH_TINY.value.mean_grade_pop * ((1 + GRADE_SIZE_FUDGE) ** 2))

            grade_students = [Student(school, grade, self._id_gen, 2012)
                              for _ in range(abs_min_grade_size // 2)]

            school.repopulate_school_grade(grade,
                                           grade_students,
                                           self._id_gen,
                                           2012,
                                           additional_student_choice=[0])

            self.assertTrue(all(isinstance(student, Student) for student in grade_students))
            self.assertTrue(abs_min_grade_size <= len(grade_students) <= abs_max_grade_size,
                            "num students not in expected range: %s <= %s <= %s" % (abs_min_grade_size, len(grade_students), abs_max_grade_size))

    def test_repopulate_school_grade_full(self):
        for _ in range(100):
            school = School(ExampleSchool.TEST_HIGH_TINY.value, self._district, DEFAULT_GRADES, self._id_gen)

            abs_max_grade_size = round(ExampleSchool.TEST_HIGH_TINY.value.mean_grade_pop * ((1 + GRADE_SIZE_FUDGE) ** 2))

            grade = school.grades[0]
            grade_students = [Student(school, grade, self._id_gen, 2012)
                              for _ in range(abs_max_grade_size + 1)]

            school.repopulate_school_grade(grade,
                                           grade_students,
                                           self._id_gen,
                                           2012,
                                           additional_student_choice=[0])

            self.assertTrue(all(isinstance(student, Student) for student in grade_students))
            self.assertEqual(len(grade_students), abs_max_grade_size + 1)

    def test_repopulate_school_grade_full_additional(self):
        for _ in range(100):
            school = School(ExampleSchool.TEST_HIGH_TINY.value, self._district, DEFAULT_GRADES, self._id_gen)
            grade = school.grades[0]
            abs_max_grade_size = round(ExampleSchool.TEST_HIGH_TINY.value.mean_grade_pop * ((1 + GRADE_SIZE_FUDGE) ** 2))

            grade_students = [Student(school, grade, self._id_gen, 2012)
                              for _ in range(abs_max_grade_size)]
            school.repopulate_school_grade(grade,
                                           grade_students,
                                           self._id_gen,
                                           2012,
                                           additional_student_choice=[5])
            self.assertTrue(all(isinstance(student, Student) for student in grade_students))
            self.assertEqual(len(grade_students), abs_max_grade_size + 5)
