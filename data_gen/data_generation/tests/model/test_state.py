import unittest
from data_generation.config.hierarchy import ExampleState
from data_generation.model.state import State
from data_generation.util.id_gen import IDGen


class TestState(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._id_gen = IDGen()

    def test_init(self):
        state = State(ExampleState.DEVEL_TINY.value, self._id_gen)
        self.assertEqual(state.code, ExampleState.DEVEL_TINY.value.state_name.code)
        self.assertEqual(state.state_type, ExampleState.DEVEL_TINY.value)
        self.assertEqual(state.demographics, ExampleState.DEVEL_TINY.value.demographics)
