import unittest
import data_generation.config.assessment
from data_generation.config.assessment import AssessmentSubject, Component, TestPeriod, DEFAULT_OVERALL_LEVELS
from data_generation.config.batch import DEFAULT_ASMT_LEVELS
from data_generation.config.config import Subject
from data_generation.config.hierarchy import ExampleState
from data_generation.config.population import Grade
from data_generation.model.assessment import Assessment
from data_generation.model.state import State
from data_generation.util.id_gen import IDGen


class TestAssessment(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._id_gen = IDGen()
        cls._state_description = ExampleState.TEST_CAT.value
        cls._state = State(cls._state_description, cls._id_gen)

    def test_initialize(self):
        for _ in range(100):
            assessment_subject = AssessmentSubject.ELA03
            assessment = Assessment(2012,
                                    TestPeriod.SPRING,
                                    self._state,
                                    assessment_subject,
                                    DEFAULT_ASMT_LEVELS[assessment_subject],
                                    self._id_gen)
            self.assertEqual(assessment.grade, Grade.G03)
            self.assertEqual(assessment.subject, Subject.ELA)
            self.assertEqual(assessment.assessment_subject, AssessmentSubject.ELA03)
            self.assertEqual(assessment.period_year, 2012)

            # PBA
            self.assertEqual(assessment.pba.component, Component.PBA)
            self.assertIn(assessment.pba.total_item_count, data_generation.config.assessment.ASMT_ITEM_COUNT_RANGE)
            pba_item_counts = (assessment.pba.subclaim_1_item_count,
                               assessment.pba.subclaim_2_item_count,
                               assessment.pba.subclaim_3_item_count,
                               assessment.pba.subclaim_4_item_count,
                               assessment.pba.subclaim_5_item_count)
            self.assertTrue(all(subclaim_item_count in data_generation.config.assessment.ASMT_SUBCLAIM_ITEM_RANGE
                                for subclaim_item_count in pba_item_counts))
            self.assertEqual(sum(pba_item_counts), assessment.pba.total_item_count)
            self.assertEqual(len(assessment.pba.form_ids),
                             data_generation.config.assessment.FORMS_PER_ASSESSMENT_COMPONENT)
            self.assertTrue(all(bool(form_id) for form_id in assessment.pba.form_ids))
            self.assertEqual(assessment.pba.subject, Subject.ELA)

            # EOY
            self.assertEqual(assessment.eoy.component, Component.EOY)
            self.assertIn(assessment.eoy.total_item_count, data_generation.config.assessment.ASMT_ITEM_COUNT_RANGE)
            eoy_item_counts = (assessment.eoy.subclaim_1_item_count,
                               assessment.eoy.subclaim_2_item_count,
                               assessment.eoy.subclaim_3_item_count,
                               assessment.eoy.subclaim_4_item_count,
                               assessment.eoy.subclaim_5_item_count)
            self.assertTrue(all(subclaim_item_count in data_generation.config.assessment.ASMT_SUBCLAIM_ITEM_RANGE
                                for subclaim_item_count in eoy_item_counts))
            self.assertEqual(sum(eoy_item_counts), assessment.eoy.total_item_count)
            self.assertEqual(len(assessment.eoy.form_ids),
                             data_generation.config.assessment.FORMS_PER_ASSESSMENT_COMPONENT)
            self.assertTrue(all(bool(form_id) for form_id in assessment.eoy.form_ids))
            self.assertEqual(assessment.eoy.subject, Subject.ELA)

    def test_hs_math(self):
        for _ in range(100):
            assessment_subject = AssessmentSubject.ALG01
            assessment = Assessment(2012,
                                    TestPeriod.SPRING,
                                    self._state,
                                    AssessmentSubject.ALG01,
                                    DEFAULT_ASMT_LEVELS[assessment_subject],
                                    self._id_gen)

            self.assertEqual(assessment.grade, None)
