import unittest
from data_generation.config import levels as level_config
import data_generation.config.assessment
from data_generation.config.assessment import TestPeriod, DEFAULT_OVERALL_LEVELS
from data_generation.config.batch import DEFAULT_GRADES, DEFAULT_ASMT_LEVELS
import data_generation.config.config
from data_generation.config.hierarchy import ExampleState, ExampleDistrict, \
    ExampleSchool
import data_generation.config.population
from data_generation.model.assessment import Assessment
from data_generation.model.assessment_outcome import AssessmentOutcome
from data_generation.model.assessment_outcome_component import AssessmentOutcomeComponent, AssessmentOutcomeSummative
from data_generation.model.district import District
from data_generation.model.school import School, NULL_SCHOOL
from data_generation.model.staff import Staff
from data_generation.model.state import State
from data_generation.model.student import Student
from data_generation.util.assessment_stats import RandomLevelByDemographics
from data_generation.util.id_gen import IDGen


class TestAssessmentOutcome(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._id_gen = IDGen()
        cls._state = State(ExampleState.DEVEL_TINY.value, cls._id_gen)
        cls._district = District(ExampleDistrict.TEST_TINY.value, cls._state, cls._id_gen)
        cls._base_elem_school = School(ExampleSchool.TEST_ELEM_TINY.value, cls._district, DEFAULT_GRADES, cls._id_gen)
        cls._staff_in_subject_in_grade_in_school = [Staff(cls._id_gen)]

    def test_initialize(self):
        for _ in range(100):
            school = self._base_elem_school
            year = 2012
            subject = self._id_gen.choice(tuple(data_generation.config.config.Subject))
            grade = self._id_gen.choice(school.grades)
            assessment_subject = data_generation.config.assessment.AssessmentSubject.assessments_by_subject_grade(subject, grade)[0]
            student = Student(school, grade, self._id_gen, year)
            grade_demographics = school.demographics[grade]
            level_breakdowns = level_config.LEVELS_BY_GRADE_BY_SUBJ[subject][grade]
            level_generator = RandomLevelByDemographics(grade_demographics, level_breakdowns)

            assessment = Assessment(year,
                                    TestPeriod.SPRING,
                                    self._state,
                                    assessment_subject,
                                    DEFAULT_ASMT_LEVELS[assessment_subject],
                                    self._id_gen)

            assessment_outcome = AssessmentOutcome(assessment, student, school, school, level_generator, True,
                                                   self._staff_in_subject_in_grade_in_school,
                                                   self._id_gen,
                                                   summative_suppressed_rate=0.0,
                                                   disabled_accommodation_rate=0.0,
                                                   general_accommodation_rate=0.0,
                                                   pba_component_not_assigned_rate=0.0,
                                                   pba_component_not_tested_rate=0.0,
                                                   pba_min_attempted_percent=0.0,
                                                   pba_component_voided_rate=0.0,
                                                   pba_attempted_percent_range=0.0,
                                                   eoy_component_not_assigned_rate=0.0,
                                                   eoy_component_not_tested_rate=0.0,
                                                   eoy_min_attempted_percent=0.0,
                                                   eoy_component_voided_rate=0.0,
                                                   eoy_attempted_percent_range=0.0)

            self.assertEqual(assessment_outcome.student, student)
            self.assertEqual(assessment_outcome.assessment, assessment)
            self.assertIsNone(assessment_outcome.is_multi_rec)
            self.assertIsInstance(assessment_outcome.pba, AssessmentOutcomeComponent)
            self.assertIsInstance(assessment_outcome.eoy, AssessmentOutcomeComponent)
            self.assertEqual(assessment_outcome.record_type, data_generation.config.assessment.RecordType.SUMMATIVE)
            self.assertTrue(assessment_outcome.is_reported_summative)
            self.assertEqual(assessment_outcome.report_suppression_code, data_generation.config.assessment.ReportSuppressionCode.NONE)
            self.assertEqual(assessment_outcome.report_suppression_action, data_generation.config.assessment.ReportSuppressionAction.NONE)
            self.assertIsInstance(assessment_outcome.summative, AssessmentOutcomeSummative)
            self.assertEqual(assessment_outcome.assessment_year, '%s-%s' % (year - 1, year))
            self.assertIsInstance(assessment_outcome.assessment.period, data_generation.config.assessment.TestPeriod)
            for accommodation in data_generation.config.population.Accommodation:
                self.assertTrue(hasattr(assessment_outcome, accommodation.name))

            self.assertEqual(assessment_outcome.pba.assessment_component, assessment.pba)
            self.assertEqual(assessment_outcome.pba.student, student)
            self.assertTrue(assessment_outcome.pba.has_assignment)
            self.assertEqual(assessment_outcome.pba.test_school, school)
            self.assertIsNone(assessment_outcome.pba.not_tested_reason)
            self.assertIn(assessment_outcome.pba.form_id, assessment.pba.form_ids)
            self.assertTrue(assessment_outcome.pba.attempt_flag)
            self.assertIsNotNone(assessment_outcome.pba.student_test_uuid)
            self.assertIsNone(assessment_outcome.pba.void_reason)
            self.assertIsNone(assessment_outcome.pba.category)
            self.assertTrue(assessment_outcome.pba.is_valid())
            self.assertIsNotNone(assessment_outcome.pba.raw_score)
            # we're computing a bunch of other stuff, but only the raw score matters
            overall_range = range(DEFAULT_OVERALL_LEVELS.min, DEFAULT_OVERALL_LEVELS.max)
            self.assertIn(assessment_outcome.pba.raw_score, overall_range)
            self.assertIn(assessment_outcome.pba.subclaim_1_attempted_count, range(0, assessment.pba.subclaim_1_item_count + 1))
            self.assertIn(assessment_outcome.pba.subclaim_2_attempted_count, range(0, assessment.pba.subclaim_2_item_count + 1))
            self.assertIn(assessment_outcome.pba.subclaim_3_attempted_count, range(0, assessment.pba.subclaim_3_item_count + 1))
            self.assertIn(assessment_outcome.pba.subclaim_4_attempted_count, range(0, assessment.pba.subclaim_4_item_count + 1))
            self.assertIn(assessment_outcome.pba.subclaim_5_attempted_count, range(0, assessment.pba.subclaim_5_item_count + 1))

            self.assertEqual(assessment_outcome.eoy.assessment_component, assessment.eoy)
            self.assertEqual(assessment_outcome.eoy.student, student)
            self.assertTrue(assessment_outcome.eoy.has_assignment)
            self.assertEqual(assessment_outcome.eoy.test_school, school)
            self.assertIsNone(assessment_outcome.eoy.not_tested_reason)
            self.assertIn(assessment_outcome.eoy.form_id, assessment.eoy.form_ids)
            self.assertTrue(assessment_outcome.eoy.attempt_flag)
            self.assertIsNotNone(assessment_outcome.eoy.student_test_uuid)
            self.assertIsNone(assessment_outcome.eoy.void_reason)
            self.assertIsNone(assessment_outcome.eoy.category)
            self.assertTrue(assessment_outcome.eoy.is_valid())
            self.assertIsNotNone(assessment_outcome.eoy.raw_score)
            # we're computing a bunch of other stuff, but only the raw score matters
            self.assertIn(assessment_outcome.eoy.raw_score, overall_range)
            self.assertIn(assessment_outcome.eoy.subclaim_1_attempted_count, range(0, assessment.eoy.subclaim_1_item_count + 1))
            self.assertIn(assessment_outcome.eoy.subclaim_2_attempted_count, range(0, assessment.eoy.subclaim_2_item_count + 1))
            self.assertIn(assessment_outcome.eoy.subclaim_3_attempted_count, range(0, assessment.eoy.subclaim_3_item_count + 1))
            self.assertIn(assessment_outcome.eoy.subclaim_4_attempted_count, range(0, assessment.eoy.subclaim_4_item_count + 1))
            self.assertIn(assessment_outcome.eoy.subclaim_5_attempted_count, range(0, assessment.eoy.subclaim_5_item_count + 1))

            self.assertIsNotNone(assessment_outcome.summative.score_record_uuid)
            self.assertIn(assessment_outcome.summative.scale_score, overall_range)
            self.assertIsNotNone(assessment_outcome.summative.csem)
            if subject == data_generation.config.config.Subject.ELA:
                self.assertIsNotNone(assessment_outcome.summative.reading_scale_score)
                self.assertIsNotNone(assessment_outcome.summative.writing_scale_score)
                self.assertIsNotNone(assessment_outcome.summative.reading_csem)
                self.assertIsNotNone(assessment_outcome.summative.writing_csem)

            else:
                self.assertIsNone(assessment_outcome.summative.reading_scale_score)
                self.assertIsNone(assessment_outcome.summative.writing_scale_score)
                self.assertIsNone(assessment_outcome.summative.reading_csem)
                self.assertIsNone(assessment_outcome.summative.writing_csem)

            if assessment.subject == data_generation.config.config.Subject.Math:
                self.assertIn(assessment_outcome.summative.subclaim_1_perf_level,
                              data_generation.config.assessment.DEFAULT_MATH_SUBCLAIM_LEVELS)
                self.assertIn(assessment_outcome.summative.subclaim_2_perf_level,
                              data_generation.config.assessment.DEFAULT_MATH_SUBCLAIM_LEVELS)
                self.assertIn(assessment_outcome.summative.subclaim_3_perf_level,
                              data_generation.config.assessment.DEFAULT_MATH_SUBCLAIM_LEVELS)
                self.assertIn(assessment_outcome.summative.subclaim_4_perf_level,
                              data_generation.config.assessment.DEFAULT_MATH_SUBCLAIM_LEVELS)
                self.assertIn(assessment_outcome.summative.subclaim_5_perf_level,
                              data_generation.config.assessment.DEFAULT_MATH_SUBCLAIM_LEVELS)

            else:
                self.assertIn(assessment_outcome.summative.subclaim_1_perf_level,
                              data_generation.config.assessment.DEFAULT_READING_CLAIM_LEVELS)
                self.assertIn(assessment_outcome.summative.subclaim_2_perf_level,
                              data_generation.config.assessment.DEFAULT_READING_CLAIM_LEVELS)
                self.assertIn(assessment_outcome.summative.subclaim_3_perf_level,
                              data_generation.config.assessment.DEFAULT_READING_CLAIM_LEVELS)
                self.assertIn(assessment_outcome.summative.subclaim_4_perf_level,
                              data_generation.config.assessment.DEFAULT_WRITING_CLAIM_LEVELS)
                self.assertIn(assessment_outcome.summative.subclaim_5_perf_level,
                              data_generation.config.assessment.DEFAULT_WRITING_CLAIM_LEVELS)

    def test_second_year(self):
        for _ in range(100):
            school = self._base_elem_school
            year = 2012
            subject = self._id_gen.choice(tuple(data_generation.config.config.Subject))
            grade = self._id_gen.choice(school.grades)
            assessment_subject = data_generation.config.assessment.AssessmentSubject.assessments_by_subject_grade(subject, grade)[0]
            student = Student(school, grade, self._id_gen, year)
            grade_demographics = school.demographics[grade]
            level_breakdowns = level_config.LEVELS_BY_GRADE_BY_SUBJ[subject][grade]
            level_generator = RandomLevelByDemographics(grade_demographics, level_breakdowns)
            assessment = Assessment(year,
                                    TestPeriod.SPRING,
                                    self._state,
                                    assessment_subject,
                                    DEFAULT_ASMT_LEVELS[assessment_subject],
                                    self._id_gen)

            assessment_outcome = AssessmentOutcome(assessment, student, school, school, level_generator, False,
                                                   self._staff_in_subject_in_grade_in_school,
                                                   self._id_gen,
                                                   summative_suppressed_rate=0.0,
                                                   disabled_accommodation_rate=0.0,
                                                   general_accommodation_rate=0.0,
                                                   pba_component_not_assigned_rate=0.0,
                                                   pba_component_not_tested_rate=0.0,
                                                   pba_min_attempted_percent=0.0,
                                                   pba_component_voided_rate=0.0,
                                                   pba_attempted_percent_range=0.0,
                                                   eoy_component_not_assigned_rate=0.0,
                                                   eoy_component_not_tested_rate=0.0,
                                                   eoy_min_attempted_percent=0.0,
                                                   eoy_component_voided_rate=0.0,
                                                   eoy_attempted_percent_range=0.0)
            self.assertIsNotNone(assessment_outcome.summative.student_growth_district)
            self.assertIsNotNone(assessment_outcome.summative.student_growth_state)
            self.assertIsNotNone(assessment_outcome.summative.student_growth_parcc)

    def test_suppress(self):
        for _ in range(100):
            school = self._base_elem_school
            year = 2012
            subject = self._id_gen.choice(tuple(data_generation.config.config.Subject))
            grade = self._id_gen.choice(school.grades)
            assessment_subject = data_generation.config.assessment.AssessmentSubject.assessments_by_subject_grade(subject, grade)[0]
            student = Student(school, grade, self._id_gen, year)
            grade_demographics = school.demographics[grade]
            level_breakdowns = level_config.LEVELS_BY_GRADE_BY_SUBJ[subject][grade]
            level_generator = RandomLevelByDemographics(grade_demographics, level_breakdowns)
            assessment = Assessment(year,
                                    TestPeriod.SPRING,
                                    self._state,
                                    assessment_subject,
                                    DEFAULT_ASMT_LEVELS[assessment_subject],
                                    self._id_gen)
            assessment_outcome = AssessmentOutcome(assessment, student, school, school, level_generator, True,
                                                   self._staff_in_subject_in_grade_in_school,
                                                   self._id_gen,
                                                   summative_suppressed_rate=1.0,
                                                   disabled_accommodation_rate=0.0,
                                                   general_accommodation_rate=0.0,
                                                   pba_component_not_assigned_rate=0.0,
                                                   pba_component_not_tested_rate=0.0,
                                                   pba_min_attempted_percent=0.0,
                                                   pba_component_voided_rate=0.0,
                                                   pba_attempted_percent_range=0.0,
                                                   eoy_component_not_assigned_rate=0.0,
                                                   eoy_component_not_tested_rate=0.0,
                                                   eoy_min_attempted_percent=0.0,
                                                   eoy_component_voided_rate=0.0,
                                                   eoy_attempted_percent_range=0.0)

            self.assertEqual(assessment_outcome.record_type, data_generation.config.assessment.RecordType.SUMMATIVE)
            self.assertIsInstance(assessment_outcome.report_suppression_code,
                                  data_generation.config.assessment.ReportSuppressionCode)
            self.assertNotEqual(assessment_outcome.report_suppression_code, data_generation.config.assessment.ReportSuppressionCode.NONE)
            self.assertIsInstance(assessment_outcome.report_suppression_action,
                                  data_generation.config.assessment.ReportSuppressionAction)
            self.assertNotEqual(assessment_outcome.report_suppression_action, data_generation.config.assessment.ReportSuppressionAction.NONE)

    def test_pba_component_not_assigned(self):
        for _ in range(100):
            school = self._base_elem_school
            year = 2012
            subject = self._id_gen.choice(tuple(data_generation.config.config.Subject))
            grade = self._id_gen.choice(school.grades)
            assessment_subject = data_generation.config.assessment.AssessmentSubject.assessments_by_subject_grade(subject, grade)[0]
            student = Student(school, grade, self._id_gen, year)
            grade_demographics = school.demographics[grade]
            level_breakdowns = level_config.LEVELS_BY_GRADE_BY_SUBJ[subject][grade]
            level_generator = RandomLevelByDemographics(grade_demographics, level_breakdowns)
            assessment = Assessment(year,
                                    TestPeriod.SPRING,
                                    self._state,
                                    assessment_subject,
                                    DEFAULT_ASMT_LEVELS[assessment_subject],
                                    self._id_gen)
            assessment_outcome = AssessmentOutcome(assessment, student, school, school, level_generator, True,
                                                   self._staff_in_subject_in_grade_in_school,
                                                   self._id_gen,
                                                   summative_suppressed_rate=0.0,
                                                   disabled_accommodation_rate=0.0,
                                                   general_accommodation_rate=0.0,
                                                   pba_component_not_assigned_rate=1.0,
                                                   pba_component_not_tested_rate=0.0,
                                                   pba_min_attempted_percent=0.0,
                                                   pba_component_voided_rate=0.0,
                                                   pba_attempted_percent_range=0.0,
                                                   eoy_component_not_assigned_rate=0.0,
                                                   eoy_component_not_tested_rate=0.0,
                                                   eoy_min_attempted_percent=0.0,
                                                   eoy_component_voided_rate=0.0,
                                                   eoy_attempted_percent_range=0.0)

            self.assertEqual(assessment_outcome.record_type, data_generation.config.assessment.RecordType.COMPONENT)
            self.assertFalse(assessment_outcome.pba.has_assignment)
            self.assertEqual(assessment_outcome.pba.test_school, NULL_SCHOOL)
            self.assertIsNone(assessment_outcome.pba.form_id)
            self.assertIsNone(assessment_outcome.pba.raw_score)
            self.assertIsNone(assessment_outcome.pba.subclaim_1_attempted_count)
            self.assertIsNone(assessment_outcome.pba.subclaim_2_attempted_count)
            self.assertIsNone(assessment_outcome.pba.subclaim_3_attempted_count)
            self.assertIsNone(assessment_outcome.pba.subclaim_4_attempted_count)
            self.assertIsNone(assessment_outcome.pba.subclaim_5_attempted_count)

            self.assertTrue(assessment_outcome.eoy.has_assignment)
            self.assertEqual(assessment_outcome.eoy.test_school, school)
            self.assertIsNotNone(assessment_outcome.eoy.form_id)
            self.assertIsNotNone(assessment_outcome.eoy.raw_score)
            self.assertIsNotNone(assessment_outcome.eoy.subclaim_1_attempted_count)
            self.assertIsNotNone(assessment_outcome.eoy.subclaim_2_attempted_count)
            self.assertIsNotNone(assessment_outcome.eoy.subclaim_3_attempted_count)
            self.assertIsNotNone(assessment_outcome.eoy.subclaim_4_attempted_count)
            self.assertIsNotNone(assessment_outcome.eoy.subclaim_5_attempted_count)

            self.assertIsNotNone(assessment_outcome.summative.score_record_uuid)

    def test_pba_eoy_component_not_assigned(self):
        for _ in range(100):
            school = self._base_elem_school
            year = 2012
            subject = self._id_gen.choice(tuple(data_generation.config.config.Subject))
            grade = self._id_gen.choice(school.grades)
            assessment_subject = data_generation.config.assessment.AssessmentSubject.assessments_by_subject_grade(subject, grade)[0]
            student = Student(school, grade, self._id_gen, year)
            grade_demographics = school.demographics[grade]
            level_breakdowns = level_config.LEVELS_BY_GRADE_BY_SUBJ[subject][grade]
            level_generator = RandomLevelByDemographics(grade_demographics, level_breakdowns)
            assessment = Assessment(year,
                                    TestPeriod.SPRING,
                                    self._state,
                                    assessment_subject,
                                    DEFAULT_ASMT_LEVELS[assessment_subject],
                                    self._id_gen)
            assessment_outcome = AssessmentOutcome(assessment, student, school, school, level_generator, True,
                                                   self._staff_in_subject_in_grade_in_school,
                                                   self._id_gen,
                                                   summative_suppressed_rate=0.0,
                                                   disabled_accommodation_rate=0.0,
                                                   general_accommodation_rate=0.0,
                                                   pba_component_not_assigned_rate=1.0,
                                                   pba_component_not_tested_rate=0.0,
                                                   pba_min_attempted_percent=0.0,
                                                   pba_component_voided_rate=0.0,
                                                   pba_attempted_percent_range=0.0,
                                                   eoy_component_not_assigned_rate=1.0,
                                                   eoy_component_not_tested_rate=0.0,
                                                   eoy_min_attempted_percent=0.0,
                                                   eoy_component_voided_rate=0.0,
                                                   eoy_attempted_percent_range=0.0)

            self.assertEqual(assessment_outcome.record_type, data_generation.config.assessment.RecordType.NO_COMPONENT)
            self.assertFalse(assessment_outcome.pba.has_assignment)
            self.assertEqual(assessment_outcome.pba.test_school, NULL_SCHOOL)
            self.assertIsNone(assessment_outcome.pba.form_id)
            self.assertIsNone(assessment_outcome.pba.raw_score)
            self.assertIsNone(assessment_outcome.pba.subclaim_1_attempted_count)
            self.assertIsNone(assessment_outcome.pba.subclaim_2_attempted_count)
            self.assertIsNone(assessment_outcome.pba.subclaim_3_attempted_count)
            self.assertIsNone(assessment_outcome.pba.subclaim_4_attempted_count)
            self.assertIsNone(assessment_outcome.pba.subclaim_5_attempted_count)

            self.assertFalse(assessment_outcome.eoy.has_assignment)
            self.assertEqual(assessment_outcome.eoy.test_school, NULL_SCHOOL)
            self.assertIsNone(assessment_outcome.eoy.form_id)
            self.assertIsNone(assessment_outcome.eoy.raw_score)
            self.assertIsNone(assessment_outcome.eoy.subclaim_1_attempted_count)
            self.assertIsNone(assessment_outcome.eoy.subclaim_2_attempted_count)
            self.assertIsNone(assessment_outcome.eoy.subclaim_3_attempted_count)
            self.assertIsNone(assessment_outcome.eoy.subclaim_4_attempted_count)
            self.assertIsNone(assessment_outcome.eoy.subclaim_5_attempted_count)

            self.assertIsNotNone(assessment_outcome.summative.score_record_uuid)

    def test_pba_component_not_tested(self):
        for _ in range(100):
            school = self._base_elem_school
            year = 2012
            subject = self._id_gen.choice(tuple(data_generation.config.config.Subject))
            grade = self._id_gen.choice(school.grades)
            assessment_subject = data_generation.config.assessment.AssessmentSubject.assessments_by_subject_grade(subject, grade)[0]
            student = Student(school, grade, self._id_gen, year)
            grade_demographics = school.demographics[grade]
            level_breakdowns = level_config.LEVELS_BY_GRADE_BY_SUBJ[subject][grade]
            level_generator = RandomLevelByDemographics(grade_demographics, level_breakdowns)
            assessment = Assessment(year,
                                    TestPeriod.SPRING,
                                    self._state,
                                    assessment_subject,
                                    DEFAULT_ASMT_LEVELS[assessment_subject],
                                    self._id_gen)
            assessment_outcome = AssessmentOutcome(assessment, student, school, school, level_generator, True,
                                                   self._staff_in_subject_in_grade_in_school,
                                                   self._id_gen,
                                                   summative_suppressed_rate=0.0,
                                                   disabled_accommodation_rate=0.0,
                                                   general_accommodation_rate=0.0,
                                                   pba_component_not_assigned_rate=0.0,
                                                   pba_component_not_tested_rate=1.0,
                                                   pba_min_attempted_percent=0.0,
                                                   pba_component_voided_rate=0.0,
                                                   pba_attempted_percent_range=0.0,
                                                   eoy_component_not_assigned_rate=0.0,
                                                   eoy_component_not_tested_rate=0.0,
                                                   eoy_min_attempted_percent=0.0,
                                                   eoy_component_voided_rate=0.0,
                                                   eoy_attempted_percent_range=0.0)

            self.assertEqual(assessment_outcome.record_type, data_generation.config.assessment.RecordType.COMPONENT)
            self.assertTrue(assessment_outcome.pba.has_assignment)
            self.assertIsNotNone(assessment_outcome.pba.not_tested_reason)
            self.assertEqual(assessment_outcome.pba.test_school, school)
            self.assertIsNone(assessment_outcome.pba.form_id)
            self.assertIsNone(assessment_outcome.pba.raw_score)
            self.assertIsNone(assessment_outcome.pba.subclaim_1_attempted_count)
            self.assertIsNone(assessment_outcome.pba.subclaim_2_attempted_count)
            self.assertIsNone(assessment_outcome.pba.subclaim_3_attempted_count)
            self.assertIsNone(assessment_outcome.pba.subclaim_4_attempted_count)
            self.assertIsNone(assessment_outcome.pba.subclaim_5_attempted_count)

            self.assertTrue(assessment_outcome.eoy.has_assignment)
            self.assertIsNone(assessment_outcome.eoy.not_tested_reason)
            self.assertEqual(assessment_outcome.eoy.test_school, school)
            self.assertIsNotNone(assessment_outcome.eoy.form_id)
            self.assertIsNotNone(assessment_outcome.eoy.raw_score)
            self.assertIsNotNone(assessment_outcome.eoy.subclaim_1_attempted_count)
            self.assertIsNotNone(assessment_outcome.eoy.subclaim_2_attempted_count)
            self.assertIsNotNone(assessment_outcome.eoy.subclaim_3_attempted_count)
            self.assertIsNotNone(assessment_outcome.eoy.subclaim_4_attempted_count)
            self.assertIsNotNone(assessment_outcome.eoy.subclaim_5_attempted_count)

            self.assertIsNotNone(assessment_outcome.summative.score_record_uuid)

    def test_pba_not_attempted(self):
        for _ in range(100):
            school = self._base_elem_school
            year = 2012
            subject = self._id_gen.choice(tuple(data_generation.config.config.Subject))
            grade = self._id_gen.choice(school.grades)
            assessment_subject = data_generation.config.assessment.AssessmentSubject.assessments_by_subject_grade(subject, grade)[0]
            student = Student(school, grade, self._id_gen, year)
            grade_demographics = school.demographics[grade]
            level_breakdowns = level_config.LEVELS_BY_GRADE_BY_SUBJ[subject][grade]
            level_generator = RandomLevelByDemographics(grade_demographics, level_breakdowns)
            assessment = Assessment(year,
                                    TestPeriod.SPRING,
                                    self._state,
                                    assessment_subject,
                                    DEFAULT_ASMT_LEVELS[assessment_subject],
                                    self._id_gen)
            assessment_outcome = AssessmentOutcome(assessment, student, school, school, level_generator, True,
                                                   self._staff_in_subject_in_grade_in_school,
                                                   self._id_gen,
                                                   summative_suppressed_rate=0.0,
                                                   disabled_accommodation_rate=0.0,
                                                   general_accommodation_rate=0.0,
                                                   pba_component_not_assigned_rate=0.0,
                                                   pba_component_not_tested_rate=0.0,
                                                   pba_min_attempted_percent=2.0,  # just make sure it fails...
                                                   pba_component_voided_rate=0.0,
                                                   pba_attempted_percent_range=0.0,
                                                   eoy_component_not_assigned_rate=0.0,
                                                   eoy_component_not_tested_rate=0.0,
                                                   eoy_min_attempted_percent=0.0,
                                                   eoy_component_voided_rate=0.0,
                                                   eoy_attempted_percent_range=0.0)

            self.assertEqual(assessment_outcome.record_type, data_generation.config.assessment.RecordType.COMPONENT)
            self.assertTrue(assessment_outcome.pba.has_assignment)
            self.assertIsNone(assessment_outcome.pba.not_tested_reason)
            self.assertFalse(assessment_outcome.pba.attempt_flag)
            self.assertIsNone(assessment_outcome.pba.student_test_uuid)
            self.assertEqual(assessment_outcome.pba.test_school, school)
            self.assertIsNone(assessment_outcome.pba.raw_score)

            self.assertTrue(assessment_outcome.eoy.has_assignment)
            self.assertIsNone(assessment_outcome.eoy.not_tested_reason)
            self.assertEqual(assessment_outcome.eoy.test_school, school)
            self.assertIsNotNone(assessment_outcome.eoy.form_id)
            self.assertIsNotNone(assessment_outcome.eoy.raw_score)
            self.assertIsNotNone(assessment_outcome.eoy.subclaim_1_attempted_count)
            self.assertIsNotNone(assessment_outcome.eoy.subclaim_2_attempted_count)
            self.assertIsNotNone(assessment_outcome.eoy.subclaim_3_attempted_count)
            self.assertIsNotNone(assessment_outcome.eoy.subclaim_4_attempted_count)
            self.assertIsNotNone(assessment_outcome.eoy.subclaim_5_attempted_count)

            self.assertIsNotNone(assessment_outcome.summative.score_record_uuid)

    def test_set_accommodations(self):
        for _ in range(100):
            school = self._base_elem_school
            year = 2012
            subject = self._id_gen.choice(tuple(data_generation.config.config.Subject))
            grade = self._id_gen.choice(school.grades)
            assessment_subject = data_generation.config.assessment.AssessmentSubject.assessments_by_subject_grade(subject, grade)[0]
            student = Student(school, grade, self._id_gen, year)
            grade_demographics = school.demographics[grade]
            level_breakdowns = level_config.LEVELS_BY_GRADE_BY_SUBJ[subject][grade]
            level_generator = RandomLevelByDemographics(grade_demographics, level_breakdowns)
            assessment = Assessment(year,
                                    TestPeriod.SPRING,
                                    self._state,
                                    assessment_subject,
                                    DEFAULT_ASMT_LEVELS[assessment_subject],
                                    self._id_gen)
            assessment_outcome = AssessmentOutcome(assessment, student, school, school, level_generator, True,
                                                   self._staff_in_subject_in_grade_in_school,
                                                   self._id_gen,
                                                   summative_suppressed_rate=0.0,
                                                   disabled_accommodation_rate=1.0,
                                                   general_accommodation_rate=1.0,
                                                   pba_component_not_assigned_rate=0.0,
                                                   pba_component_not_tested_rate=0.0,
                                                   pba_min_attempted_percent=0.0,
                                                   pba_component_voided_rate=0.0,
                                                   pba_attempted_percent_range=0.0,
                                                   eoy_component_not_assigned_rate=0.0,
                                                   eoy_component_not_tested_rate=0.0,
                                                   eoy_min_attempted_percent=0.0,
                                                   eoy_component_voided_rate=0.0,
                                                   eoy_attempted_percent_range=0.0)

            # TODO: this would better be checked in the output tests, not here..

    def test_random_non_components(self):
        for _ in range(100):
            school = self._base_elem_school
            year = 2012
            subject = self._id_gen.choice(tuple(data_generation.config.config.Subject))
            grade = self._id_gen.choice(school.grades)
            assessment_subject = data_generation.config.assessment.AssessmentSubject.assessments_by_subject_grade(subject, grade)[0]
            student = Student(school, grade, self._id_gen, year)
            grade_demographics = school.demographics[grade]
            level_breakdowns = level_config.LEVELS_BY_GRADE_BY_SUBJ[subject][grade]
            level_generator = RandomLevelByDemographics(grade_demographics, level_breakdowns)
            assessment = Assessment(year,
                                    TestPeriod.SPRING,
                                    self._state,
                                    assessment_subject,
                                    DEFAULT_ASMT_LEVELS[assessment_subject],
                                    self._id_gen)

            kwargs = {key: self._id_gen.choice((0.0, 1.0))
                      for key in ('pba_component_not_assigned_rate',
                                  'pba_component_not_tested_rate',
                                  'pba_min_attempted_percent',
                                  'pba_component_voided_rate',
                                  'pba_attempted_percent_range',
                                  'eoy_component_not_assigned_rate',
                                  'eoy_component_not_tested_rate',
                                  'eoy_min_attempted_percent',
                                  'eoy_component_voided_rate',
                                  'eoy_attempted_percent_range')}
            assessment_outcome = AssessmentOutcome(assessment, student, school, school, level_generator, True,
                                                   self._staff_in_subject_in_grade_in_school,
                                                   self._id_gen,
                                                   summative_suppressed_rate=0.0,
                                                   disabled_accommodation_rate=1.0,
                                                   general_accommodation_rate=1.0,
                                                   **kwargs)

            # TODO: don't really have time to test all the logic now, just set a bunch of values and see if anything crashes..
