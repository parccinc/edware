import unittest
from data_generation.config.hierarchy import ExampleState, ExampleDistrict
from data_generation.model.district import District
from data_generation.model.state import State
from data_generation.util.id_gen import IDGen


class TestDistrict(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._id_gen = IDGen()
        cls._state = State(ExampleState.DEVEL_TINY.value, cls._id_gen)

    def test_initialize(self):
        district = District(ExampleDistrict.TEST_TINY.value, self._state, self._id_gen)
        self.assertEqual(district.district_type, ExampleDistrict.TEST_TINY.value)
        self.assertEqual(district.state, self._state)
        self.assertEqual(district.demographics, self._state.demographics)
        self.assertIsInstance(district.identifier, str)
        self.assertTrue(bool(district.identifier))
        self.assertIsInstance(district.name, str)
        self.assertTrue(bool(district.name))
