import unittest
from functools import reduce
from data_generation.config.batch import DEFAULT_GRADES
from data_generation.config.hierarchy import SchoolLevel, \
    ExampleState, ExampleDistrict, ExampleSchool
from data_generation.config.population import Sex, Ethnicity, StudentAccommodation, StudentStatus, Grade
from data_generation.model.district import District
from data_generation.model.school import School
from data_generation.model.state import State
from data_generation.model.student import Student
from data_generation.util.id_gen import IDGen


class TestStudent(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._id_gen = IDGen()
        cls._state_type = ExampleState.DEVEL_TINY.value
        cls._district_type = ExampleDistrict.TEST_TINY.value
        cls._elem_school_type = ExampleSchool.TEST_ELEM_TINY.value
        cls._midl_school_type = ExampleSchool.TEST_MIDL_TINY.value
        cls._high_school_type = ExampleSchool.TEST_HIGH_TINY.value
        cls._state = State(cls._state_type, cls._id_gen)
        cls._district = District(cls._district_type, cls._state, cls._id_gen)
        cls._base_elem_school = School(cls._elem_school_type, cls._district, DEFAULT_GRADES, cls._id_gen)

    def test_student_initialize(self):
        for _ in range(100):
            grade = self._id_gen.choice(self._base_elem_school.grades)
            student = Student(self._base_elem_school, grade, self._id_gen, 2012)

            self.assertIsInstance(student.state_id, str)
            self.assertTrue(1 <= len(student.state_id) <= 30)
            self.assertTrue(1 <= len(student.local_id) <= 30)
            self.assertEqual(student.grade, grade)
            self.assertEqual(student.school, self._base_elem_school)
            self.assertEqual(student.birthdate.year, 2012 - grade.birthyear_offset)
            self.assertIn(student.sex, Sex)
            self.assertTrue(isinstance(student.first_name, str) and student.first_name)
            self.assertTrue(isinstance(student.last_name, str) and bool(student.last_name))
            self.assertTrue((isinstance(student.middle_name, str) and student.middle_name) or student.middle_name is None)
            self.assertIsInstance(student.prg_iep, bool)
            self.assertIsInstance(student.prg_sec504, bool)
            self.assertIsInstance(student.prg_lep, bool)
            self.assertIsInstance(student.prg_econ_disad, bool)
            self.assertIsInstance(student.prg_migrant, bool)
            self.assertIn(student.level_ethnicity, Ethnicity)
            self.assertIsInstance(student.eth_hispanic, bool)
            self.assertIsInstance(student.eth_amer_ind, bool)
            self.assertIsInstance(student.eth_asian, bool)
            self.assertIsInstance(student.eth_black, bool)
            self.assertIsInstance(student.eth_pacific, bool)
            self.assertIsInstance(student.eth_white, bool)
            self.assertIn(student.eth_federal, {ethnicity.federal_code for ethnicity in Ethnicity})

            if student.level_ethnicity is Ethnicity.MULTIPLE:
                self.assertGreater(sum((student.eth_hispanic,
                                        student.eth_amer_ind,
                                        student.eth_asian,
                                        student.eth_black,
                                        student.eth_pacific,
                                        student.eth_white)), 1)
                if student.eth_hispanic:
                    self.assertEqual(student.eth_federal, Ethnicity.HISPANIC.federal_code)

                else:
                    self.assertEqual(student.eth_federal, Ethnicity.MULTIPLE.federal_code)

            else:
                self.assertEqual(student.level_ethnicity == Ethnicity.HISPANIC, student.eth_hispanic)
                self.assertEqual(student.level_ethnicity == Ethnicity.AMERIND, student.eth_amer_ind)
                self.assertEqual(student.level_ethnicity == Ethnicity.ASIAN, student.eth_asian)
                self.assertEqual(student.level_ethnicity == Ethnicity.BLACK, student.eth_black)
                self.assertEqual(student.level_ethnicity == Ethnicity.HAWAII, student.eth_pacific)
                self.assertEqual(student.level_ethnicity == Ethnicity.WHITE, student.eth_white)
                self.assertEqual(student.level_ethnicity.federal_code, student.eth_federal)

            self.assertEqual(student.prg_disability, student.prg_primary_disability is not None)
            self.assertIsInstance(student.prg_gifted, bool)
            self.assertTrue(student.parcc_growth_percent is None or isinstance(student.parcc_growth_percent, (int, float)))
            self.assertTrue(student.state_growth_percent is None or isinstance(student.state_growth_percent, (int, float)))
            self.assertTrue(student.district_growth_percent is None or isinstance(student.district_growth_percent, (int, float)))

            self.assertIsInstance(student.is_roster_reported, bool)

            self.assertIsInstance(student.name, str)
            self.assertEqual(student.state, self._state)
            self.assertEqual(student.district, self._district)

    def test_get_level_demographics(self):
        for _ in range(100):
            grade = self._id_gen.choice(self._base_elem_school.grades)
            student = Student(self._base_elem_school, grade, self._id_gen, 2012)

            level_demos = student.get_level_demographics()

            self.assertEqual(student.level_ethnicity, level_demos[Ethnicity])
            self.assertEqual(student.sex, level_demos[Sex])
            self.assertEqual(student.prg_sec504, level_demos[StudentAccommodation.SECTION_504])
            self.assertEqual(student.prg_iep, level_demos[StudentAccommodation.IEP])
            self.assertEqual(student.prg_lep, level_demos[StudentStatus.LEP])
            self.assertEqual(student.prg_econ_disad, level_demos[StudentStatus.ECON_DIS])


class TestAdvance(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._id_gen = IDGen()
        cls._state_type = ExampleState.DEVEL_TINY.value
        cls._district_type = ExampleDistrict.TEST_TINY.value
        cls._elem_school_type = ExampleSchool.TEST_ELEM_TINY.value
        cls._midl_school_type = ExampleSchool.TEST_MIDL_TINY.value
        cls._high_school_type = ExampleSchool.TEST_HIGH_TINY.value
        cls._state = State(cls._state_type, cls._id_gen)
        cls._district = District(cls._district_type, cls._state, cls._id_gen)
        cls._schools = [School(school_type, cls._district, DEFAULT_GRADES, cls._id_gen)
                        for school_type
                        in [cls._elem_school_type,
                            cls._midl_school_type,
                            cls._high_school_type] * 4]

        cls._schools_by_grade = {grade: [school for school in cls._schools if grade in school.grades]
                                 for school_level in SchoolLevel
                                 for grade in school_level.grades
                                 if grade in DEFAULT_GRADES}
        if any(len(schools) == 0 for schools in cls._schools_by_grade.values()):
            raise AssertionError("couldn't figure out school grades? %s" % (cls._schools_by_grade,))

    def test_advance_student_same_school(self):
        for _ in range(100):
            school = self._id_gen.choice(self._schools)
            grade = min(school.grades)
            student = Student(school, grade, self._id_gen, 2012)
            advanced = student.advance_student(self._schools_by_grade, self._id_gen, hold_back_rate=0, drop_out_rate=0, transfer_rate=0)
            self.assertTrue(advanced)
            self.assertEqual(student.school, school)
            self.assertEqual(student.grade, Grade[Grade.index(grade) + 1])

    def test_advance_student_next_school(self):
        for _ in range(100):
            # choose a school which doesn't contain the maximum grade
            school = self._id_gen.choice([school for school in self._schools
                                          if max(self._schools_by_grade) not in school.grades])
            grade = max(school.grades)
            student = Student(school, grade, self._id_gen, 2012)
            advanced = student.advance_student(self._schools_by_grade, self._id_gen, hold_back_rate=0, drop_out_rate=0, transfer_rate=0)
            self.assertTrue(advanced)
            self.assertNotEqual(student.school, school)
            self.assertEqual(student.grade, Grade[Grade.index(grade) + 1])

    def test_advance_student_out(self):
        for _ in range(100):
            grade = max(self._schools_by_grade)
            school = self._id_gen.choice(self._schools_by_grade[grade])
            student = Student(school, grade, self._id_gen, 2012)
            advanced = student.advance_student(self._schools_by_grade, self._id_gen, hold_back_rate=0, drop_out_rate=0, transfer_rate=0)
            self.assertFalse(advanced)

    def test_advance_student_drop_out(self):
        for _ in range(100):
            grade = self._id_gen.choice(list(self._schools_by_grade))
            school = self._id_gen.choice(self._schools_by_grade[grade])
            student = Student(school, grade, self._id_gen, 2012)
            advanced = student.advance_student(self._schools_by_grade, self._id_gen, hold_back_rate=0, drop_out_rate=1.0, transfer_rate=0)
            self.assertFalse(advanced)

    def test_advance_student_hold_back(self):
        for _ in range(100):
            grade = self._id_gen.choice(list(self._schools_by_grade))
            school = self._id_gen.choice(self._schools_by_grade[grade])
            student = Student(school, grade, self._id_gen, 2012)
            advanced = student.advance_student(self._schools_by_grade, self._id_gen, hold_back_rate=1.0, drop_out_rate=0.0, transfer_rate=0)
            self.assertTrue(advanced)
            self.assertEqual(student.school, school)
            self.assertEqual(student.grade, grade)

    def test_advance_student_transfer(self):
        for _ in range(100):
            grade = self._id_gen.choice([grade
                                         for grade in self._schools_by_grade
                                         if grade != max(self._schools_by_grade)])
            school = self._id_gen.choice(self._schools_by_grade[grade])
            student = Student(school, grade, self._id_gen, 2012)
            advanced = student.advance_student(self._schools_by_grade, self._id_gen, hold_back_rate=0.0, drop_out_rate=0.0, transfer_rate=1.0)
            self.assertTrue(advanced)
            self.assertEqual(student.grade, Grade[Grade.index(grade) + 1])
            self.assertIn(student.grade, student.school.grades)
