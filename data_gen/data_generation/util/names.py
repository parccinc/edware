"""Define a couple helper classes for name generation.

:author: swimberly
:date: January 5, 2013
"""
from pathlib import Path
import math
from data_generation.util.assessment_stats import product

from data_generation.util.properties import Properties
from data_generation.util.weighted_choice import WeightedChooser
from data_generation.config.hierarchy import SchoolType, SchoolLevel
from data_generation.config.names import Names, DISTRICT_SUFFIXES, SCHOOL_SUFFIXES, STREET_SUFFIXES, APARTMENT_PREFIXES, \
    NamePath, NO_MIDDLE_NAME_CHANCE
from data_generation.config.population import Sex
from data_generation.util.id_gen import IDGen


def _read_dist(dist_path: Path) -> WeightedChooser:
    """
    the dist files are space separated values, with the following format:

        name name_percentage cumulative_name_percentage rank

    we only care about the first two values

    :param dist_path:
    :return:
    """
    with dist_path.open() as fh:
        return WeightedChooser({name: float(ratio)
                                for name, ratio, _, _
                                in map(str.split, fh)})


_male_first_dist = _read_dist(NamePath.MALE_FIRST_DIST.value)
_female_first_dist = _read_dist(NamePath.FEMALE_FIRST_DIST.value)
_last_dist = _read_dist(NamePath.LAST_DIST.value)


def generate_person_name(sex: Sex,
                         id_gen: IDGen,
                         max_name_length: int=None,
                         max_retries: int=5,
                         empty_middle_name='NONE',  # TODO: change to None when schema is fixed
                         no_middle_name_chance: float=NO_MIDDLE_NAME_CHANCE):
    """Generate a gender-appropriate name for a person.

    :param sex: The gender of the person
    :returns: A tuple of (first, middle, last) name pieces
    """
    if sex == Sex.MALE:
        fm_names = _male_first_dist

    elif sex == Sex.FEMALE:
        fm_names = _female_first_dist

    else:
        fm_names = id_gen.choice([_male_first_dist, _female_first_dist])

    return (_choose_name(id_gen, fm_names, max_length=max_name_length, max_retries=max_retries),
            (empty_middle_name
             if id_gen.random() < no_middle_name_chance
             else _choose_name(id_gen, fm_names, max_length=max_name_length, max_retries=max_retries)),
            _choose_name(id_gen, _last_dist, max_length=max_name_length, max_retries=max_retries))


def _choose_name(id_gen: IDGen, name_dist: WeightedChooser, max_length: int=None, max_retries: int=5):
    name = None
    for _ in range(max_retries):
        if name is None or (max_length is not None and len(name) > max_length):
            name = name_dist.choose(id_gen)

        else:
            return name

    if max_length is not None:
        return name[:max_length]

    else:
        return name


def _generate_name(id_gen: IDGen, *name_lists, max_length: int=None, max_retries: int=5):
    name = None
    for _ in range(max_retries):
        if name is None or name.lower().count('fish') > 1 or (max_length is not None and len(name) > max_length):
            name = ' '.join(map(str, map(id_gen.choice, name_lists)))

        else:
            return name

    if max_length is not None:
        return name[:max_length]

    else:
        return name


NAME_LISTS = Properties(
    district=(Names.CITIES.value, DISTRICT_SUFFIXES),
    school=(Names.CITIES.value, ),
    street_line_1=(range(1, 5001), Names.BIRDS.value, STREET_SUFFIXES),
    street_line_2=(APARTMENT_PREFIXES, range(1, 100)),
    street_address_city=(Names.FISH.value, Names.FISH.value)
)


def generate_district_name(id_gen: IDGen, max_name_length=None):
    """Generate a name for a district.

    :param max_name_length: The longest a name can be
    :returns: New district name
    """
    return _generate_name(id_gen, *NAME_LISTS.district, max_length=max_name_length)


def generate_school_name(school_level: SchoolLevel, id_gen: IDGen, max_name_length=None):
    """Generate a name for a school by combining a word from each provided list, taking length into consideration.

    :param school_level: (High School, Middle School, Elementary School) used to determine appropriate suffix for name.
    :param max_name_length: The length of the longest acceptable name
    :returns: New school name
    """
    name_list = NAME_LISTS.school + (SCHOOL_SUFFIXES[school_level],)
    return _generate_name(id_gen, *name_list, max_length=max_name_length)


def generate_street_address_line_1(id_gen: IDGen, max_name_length=None):
    """Generate a street address (a.k.a. address line 1).

    :returns: The street address
    """
    return _generate_name(id_gen, *NAME_LISTS.street_line_1, max_length=max_name_length)


def generate_street_address_line_2(id_gen: IDGen, max_name_length=None):
    """Generate the second line of a street address.

    :returns: The second line of a street address
    """
    return _generate_name(id_gen, *NAME_LISTS.street_line_2, max_length=max_name_length)


def generate_street_address_city(id_gen: IDGen, max_name_length=None):
    """Generate the city name of a street address.

    :returns: The city name of a street address
    """
    return _generate_name(id_gen, *NAME_LISTS.street_address_city, max_length=max_name_length)


def probability_of_collision(n: int, *name_lists):
    """
    useful in figuring out how many names you'll need in order to avoid having two entities randomly getting
    the same name.

    for instance, i've just realized that if i generate 900 districts, there's a ~30% chance of a
    collision, given 402 animal names * 2, and 6 suffixes.

    see e.g. https://en.wikipedia.org/wiki/Birthday_problem

    :param n: number of values we'll be generating
    :return:
    """
    m = product(map(len, name_lists))

    return 1 - math.exp(-(n * (n - 1)) / (2 * m))
