from collections import OrderedDict


class Properties(OrderedDict):
    """ Wrapper for accessing a dict's values as attributes. """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._initialized = True

    def __getattr__(self, item):
        if item == '_initialized' or not hasattr(self, '_initialized'):
            super().__getattr__(item)

        else:
            try:
                return self[item]

            except KeyError as e:
                raise AttributeError() from e

    def __setattr__(self, key, value):
        if key == '_initialized' or not hasattr(self, '_initialized'):
            super().__setattr__(key, value)

        else:
            self[key] = value
