class PerfLevels:
    def __init__(self, cutpoints: tuple, level_names: tuple):
        if len(cutpoints) != len(level_names) + 1:
            raise ValueError("There must be exactly one more cut point than level")

        self.cutpoints = cutpoints
        self.levels = tuple(
            PerfLevel(self, level_name, index)
            for index, level_name in enumerate(level_names)
        )

    @property
    def min(self):
        return self.cutpoints[0]

    @property
    def max(self):
        return self.cutpoints[-1]

    def __len__(self):
        return len(self.levels)

    def __getitem__(self, item):
        return self.levels[item]

    def __iter__(self):
        yield from self.levels

    def __contains__(self, item):
        return item in self.levels

    def get_level_given_score(self, score: int):
        for level in self.levels:
            if score in level:
                return level

        else:
            raise ValueError("score %s not in range [%s, %s)" % (score, self.min, self.max))


class PerfLevel:
    def __init__(self, _levels: PerfLevels, name: str, index: int):
        self._levels = _levels
        self.name = name
        self.index = index

    @property
    def id(self):
        return self.index + 1

    @property
    def start(self):
        return self._levels.cutpoints[self.index]

    @property
    def stop(self):
        return self._levels.cutpoints[self.index + 1]

    @property
    def min(self):
        return self._levels.cutpoints[self.index]

    @property
    def max(self):
        return self._levels.cutpoints[self.index + 1]

    def __len__(self):
        return self.stop - self.start

    def __contains__(self, item):
        return self.start <= item < self.stop
