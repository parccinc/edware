import math

import numpy as np
from scipy.stats import poisson, gamma


def counts_mean(counts: [int]) -> float:
    """ find the mean *value* of the given *counts*

    @param counts: the 0th element is taken to be the relative count of the value "0", the 1st element the counts of
    the value "1", and so on.
    """
    return sum(value * count for value, count in enumerate(counts)) / sum(counts)


def counts_stdv(counts: [int], mean_: int=None) -> float:
    """ find the stdv *value* of the given *counts*

    @param counts: the 0th element is taken to be the relative count of the value "0", the 1st element the counts of
    the value "1", and so on.
    @param mean_: If you already know the mean, save some computation by supplying it!"""
    if mean_ is None:
        mean_ = counts_mean(counts)

    return math.sqrt(sum(((value - mean_) ** 2) * count
                         for value, count in enumerate(counts)
                         ) / sum(counts))


def normalize(values: [float], total: float=1.0) -> [float]:
    """normalize an array of numeric values, so that the sum of the elements corresponds to the given total.
    """
    assert(all(value >= 0 for value in values))
    sum_ = sum(values)

    if sum_ == 0.0:
        return tuple(values)

    else:
        return tuple(total * value / sum_ for value in values)


class StatDistribution:
    @property
    def mean(self):
        raise NotImplementedError()

    def get_values(self, size: int, rng: np.random.RandomState):
        raise NotImplementedError()


class ScipyStatDistribution(StatDistribution):
    def __init__(self, rv):
        self._rv = rv

    @property
    def mean(self):
        return self._rv.mean()

    def get_values(self, size: int, rng: np.random.RandomState):
        return self._rv.ppf(rng.rand(size))


class PoissonDistribution(ScipyStatDistribution):
    def __init__(self, mean):
        super().__init__(poisson(mean))


class GammaDistribution(ScipyStatDistribution):
    def __init__(self, shape, scale):
        super().__init__(gamma(shape, scale))


class PopulationSizeDistribution(StatDistribution):
    def __init__(self, population, count, **kwargs):
        self._mean = population / count

    @property
    def mean(self):
        return self._mean


class UniformPopulationSizeDistribution(PopulationSizeDistribution):
    def __init__(self, population, count, **kwargs):
        super().__init__(population, count, **kwargs)
        self._low = np.floor_divide(population, count)
        self._cutoff = self._mean - self._low

    def get_values(self, size: int, rng: np.random.RandomState):
        bits = rng.uniform(size=size) < self._cutoff
        return bits + self._low


class GammaPopulationSizeDistribution(PopulationSizeDistribution):
    def __init__(self, population, count, scale_ratio=4, min_size=100, **kwargs):
        super().__init__(population, count, **kwargs)

        offset_pop = population - (min_size * count)
        if offset_pop < 0:
            raise ValueError("given total pop %s, item count %s, and minimum size %s, cannot build distribution"
                             % (population, count, min_size))

        self._scale = self.mean * scale_ratio
        self._k = self.mean / self._scale
        self._min_size = min_size

    def get_values(self, size: int, rng: np.random.RandomState):
        return (rng.gamma(self._k, self._scale, size=size) + self._min_size).round()


class TriangularDistribution(StatDistribution):
    def __init__(self, min_, mode, max_):
        self._min = min_
        self._mode = mode
        self._max = max_

    def get_values(self, size: int, rng: np.random.RandomState):
        return rng.triangular(self._min, self._mode, self._max, size)

    @property
    def mean(self):
        return (self._min + self._mode + self._max) / 3
