"""
note: this code is a manual translation of the current stored procedures in
edudl2/edudl2/database/metadata/rsa_stored_procs.py

we use the SPs when we can, but when we're writing reporting data to CSV files, we can't use them, hence this file.

TODO: write code to automatically parse and translate postgres stored procedures to python
      (this will never actually be done)
"""


def include_in_aggregate(record_type: str, is_reported_summative: str, report_suppression_code: str, report_suppression_action: str) -> bool:
    if record_type == '01' and is_reported_summative == 'Y':
        if report_suppression_code in ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10'):
            if report_suppression_action in ('01', '02', '03', '05'):
                return False

            elif report_suppression_action in ('04', '06'):
                return True

            else:  # case for suppression actions 07-99
                return True

        else:  # report suppression code > 10
            return True

    else:  # case for report type 02 and 03; summative = N
        return False


def include_in_school(record_type: str, is_reported_summative: str, report_suppression_code: str, report_suppression_action: str) -> bool:
    if record_type == '01' and is_reported_summative == 'Y':
        if report_suppression_code in ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10'):
            if report_suppression_action in ('01', '02', '03', '05', '06'):
                return False

            elif report_suppression_action == '04':
                return True

            else:  # case for suppression actions 07-99
                return True

        else:  # report suppression code > 10
            return True

    else:  # case for report type 02 and 03; summative = N
        return False


def include_in_roster(record_type: str, is_reported_summative: str, report_suppression_code: str, report_suppression_action: str, is_roster_reported: str) -> str:
    if record_type == '01':
        if is_reported_summative == 'Y':
            if report_suppression_code in ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10'):
                if report_suppression_action in ('01', '04', '05'):
                    return 'N'

                elif report_suppression_action in ('02', '06'):
                    return 'Y'

                elif report_suppression_action == '03':
                    return 'W'

                else:  # case for suppression actions 07-99
                    return 'Y'

            else:  # case for report suppression code > 10
                return 'Y'

        else:  # case for summative flag N
            return 'N'

    else:  # case for report type 02 and 03
        if is_roster_reported == 'Y':
            return 'W'

        else:
            return 'N'


def include_in_isr(record_type: str, is_reported_summative: str, report_suppression_code: str, report_suppression_action: str):
    if record_type == '01' and is_reported_summative == 'Y':
        if report_suppression_code in ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10'):
            if report_suppression_action in ('01', '03', '04'):
                return False

            elif report_suppression_action in ('02', '05', '06'):
                return True

            else:  # case for suppression actions 07-99
                return True

        else:  # report suppression code > 10
            return True

    else:  # case for report type 02 and 03; summative = N
        return False
