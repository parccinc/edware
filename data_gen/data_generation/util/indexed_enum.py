from enum import Enum, EnumMeta


class NoseSafeEnumMeta(EnumMeta):
    """
    nose clones existing classes for some weird reason involving reflection and fixing metadata it borks.

    so we have to detect this and subvert it.

    i can't decide if it'd be more or less hack-y to check if nose is a level or few up the stack from here instead
    of just checking the class name
    """
    def __new__(metacls, cls, bases, classdict):
        try:
            return EnumMeta.__new__(metacls, cls, bases, classdict)

        except TypeError as e:
            if cls == 'C':
                old_cls = bases[0]
                new_cls = old_cls.__name__
                new_bases = old_cls.__bases__
                return EnumMeta.__new__(metacls, new_cls, new_bases, classdict)

            else:
                raise Exception("Can't extend Enums, unless you're nose (%s %s %s)" % (metacls, cls, bases)) from e


class NoseSafeEnum(Enum, metaclass=NoseSafeEnumMeta):
    @classmethod
    def get_by_value(cls, value):
        for item in cls:
            if item.value == value:
                return item

        raise KeyError(value)

    @classmethod
    def get_by_label(cls, label):
        for item in cls:
            if item.label == label:
                return item

        raise KeyError(label)


class IndexedEnumMeta(NoseSafeEnumMeta):
    def __init__(cls, *args, **kwargs):
        EnumMeta.__init__(cls, *args, **kwargs)
        cls._element_tuple = tuple(cls)

    def __getitem__(self, index):
        if isinstance(index, int):
            return self._element_tuple[index]

        else:
            return super().__getitem__(index)

    def index(self, element):
        return self._element_tuple.index(element)


class IndexedEnum(NoseSafeEnum, metaclass=IndexedEnumMeta):
    """
    Enumeration type that supports indexing elements by the order in which they are declared
    """
    pass
