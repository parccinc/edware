"""Method for choosing an object randomly, given weights.

If many choices are to be made using the same weights, WeightedChooser might be more efficient.

:author: mjacob
:date: February 25, 2014
"""
import random
import itertools
import bisect


def weighted_choice(counter: {object: float},
                    rng: random.Random=random.Random(),
                    seed=None) -> object:
    """Choose a random item based on a weight.

    :param counter: a mapping from objects to their respective weights"""
    if seed is not None:
        rng.seed(seed)

    elements, weights = zip(*counter.items())

    assert(all(weight >= 0 for weight in weights))

    breaks = list(itertools.accumulate(weights))
    value = rng.uniform(0, breaks[-1])
    i = bisect.bisect(breaks, value)

    return elements[i]


class WeightedChooser:
    def __init__(self,
                 weights_by_object: {object: float}):
        self.elements, weights = zip(*weights_by_object.items())
        self.breaks = tuple(itertools.accumulate(weights))

    def choose(self, rng: random.Random):
        value = rng.uniform(0, self.breaks[-1])  # a random float between 0 and the sum of the weights
        i = bisect.bisect(self.breaks, value)  # the index

        return self.elements[i]


class DiscreteDistribution(WeightedChooser):
    def __init__(self, weights_by_object: dict, should_sum_to: float=None, other_value=None):
        if should_sum_to:
            s = sum(weights_by_object.values())
            if s < should_sum_to:
                weights_by_object = weights_by_object.copy()
                weights_by_object[other_value] = should_sum_to - s

        self._weights_by_object = weights_by_object
        super().__init__(weights_by_object)

    def __iter__(self):
        return iter(self._weights_by_object)

    def __getitem__(self, item):
        return self._weights_by_object[item]
