from csv import DictWriter


class DictWriterWithFileHandle(DictWriter):
    """
    A DictWriter w/ a reference to the file handle its using, so we don't have to keep a separate ref to it around.
    """

    def __init__(self, fh, *args, **kwargs):
        self.fh = fh
        super().__init__(fh, *args, **kwargs)
