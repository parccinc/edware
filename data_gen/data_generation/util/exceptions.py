
class DataGenExceptionBase(Exception):
    pass


class DataGenConfigurationError(DataGenExceptionBase):
    pass


class DataGenDBError(DataGenExceptionBase):
    pass
