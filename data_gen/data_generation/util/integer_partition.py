# -*- coding: utf-8 -*-
"""
code for randomly, uniformly dividing an integer in multiple parts
"""
from numpy.random import RandomState
from random import Random
import math
from pathlib import Path


_HERE = Path(__file__).parent


def _read_p_memo():
    memo_path = _HERE / '..' / 'datafiles' / 'p.txt'
    with memo_path.open() as fh:
        return list(map(int, fh))


def _read_q_memo():
    """
    read a linearized triangulation of values.

    we need to skip values which we don't include in our memo, because they're trivial to compute.
    :return:
    """
    memo_path = _HERE / '..' / 'datafiles' / 'q.txt'

    with memo_path.open() as fh:
        q_bits = list(map(int, fh))

    memo_part = []
    memo = []

    n = 0
    m = 0
    for q_n_m in q_bits:
        if m == 0 and n == 0:
            continue  # skip first row

        m += 1
        if m > n:
            m = 0
            n += 1
            memo_part = list()
            memo.append(memo_part)

        if m == 0 or m == 1 or m == n:
            continue

        memo_part.append(q_n_m)

    return memo


_P_MEMO = _read_p_memo()
_Q_MEMO = _read_q_memo()


def _p_parts(m):
    """ parts of euler's formula for p(m) """
    for k in range(1, m + 1):
        if (k * (3 * k - 1)) // 2 > m:
            return

        elif (k * (3 * k + 1)) // 2 > m:
            yield ((-1) ** (k + 1)) * (p(m - (k * (3 * k - 1)) // 2))

        else:
            yield ((-1) ** (k + 1)) * (p(m - (k * (3 * k + 1)) // 2) +
                                       p(m - (k * (3 * k - 1)) // 2))


def approx_p(n: int) -> int:
    """
    approximation of the partition number p(n)

    returns an exact value if it's already available.
    :param n:
    :return:
    """
    if n < 0:
        raise Exception("error: %s is negative" % (n))

    elif n == 0:
        return 1

    elif n == 1:
        return 1

    elif n < len(_P_MEMO):
        return _P_MEMO[n]

    else:
        return int(round((1 / (4 * n * math.sqrt(3))) * math.exp(math.pi * math.sqrt(2 * n / 3))))


def approx_q(n: int, m: int) -> int:
    """
    approximation of the partition number q(n, m)

    returns an exact value if it's already available.
    :param n:
    :return:
    """
    if n < 0 or m < 0:
        # while this is defined, we don't care about it here
        raise Exception("error: negative param (n = %s, m = %s)" % (n, m))

    elif n == m:
        return 1

    elif m == 0 or n < m:
        return 0

    elif m == 1:
        return 1

    elif n - 3 < len(_Q_MEMO):
        return _Q_MEMO[n - 3][m - 2]

    else:
        return int(round(math.pow(n, m - 1) / (math.factorial(m) * math.factorial(m - 1))))


def p(n, memo=_P_MEMO):
    """ number of integer partitions of n

    that is, the number of distinct multisets C of positive integers such that

     ∑  x = n
    x∈C

    this is computed here through an identity discovered by euler:


    p(n) =   ∑  -1ⁿ * p(n - (3k² - k) / 2)
            n∈ℤ

    as p(n) = 0 for negative n, this sum is finite.

    for efficiency, this algorithm memoizes computed values. with memoization
    enabled, the cost of computing the first n values is O(n * √n).

    more info:
     1. http://oeis.org/A000041
     2. https://en.wikipedia.org/wiki/Partition_%28number_theory%29
     3. Knuth, D. E. The Art of Computer Programming, Vol. 4 Fascicle 3,
        Generating All Combinations and Partitions (2005), vi+ 150pp. ISBN
        0-201-85394-9. § 7.2.1.4
    """
    if n < 0:
        raise Exception("error: %s is negative" % (n))

    elif n == 0:
        return 1

    elif n == 1:
        return 1

    else:
        if n < len(memo):
            return memo[n]

        else:
            for m in range(len(memo), n + 1):
                memo.append(sum(_p_parts(m)))

            return memo[n]


def q(n: int, m: int, memo=_Q_MEMO):
    """ the number of partitions of n with largest part m

    alternatively:
        the number of partitions on n in into m parts.

    more info:
        http://oeis.org/A008284
        https://en.wikipedia.org/wiki/Partition_%28number_theory%29#Restricted_part_size_or_number_of_parts
    """
    if n < 0 or m < 0:
        # while this is defined, we don't care about it here
        raise Exception("error: negative param (n = %s, m = %s)" % (n, m))

    elif n == m:
        return 1

    elif m == 0 or n < m:
        return 0

    elif m == 1:
        return 1

    else:
        if len(memo) <= n - 3:
            for i in range(len(memo) + 3, n + 1):
                memo.append([q(i - 1, j - 1) + q(i - j, j)
                             for j in range(2, i)])

        return memo[n - 3][m - 2]


def approx_random_integer_partition(n, m, rng: RandomState, tries: int=100) -> int:
    """
    This method attempts to create a random integer partition of n into m parts.
    It uses approximations of p(n) and q(n, m) instead of exact values though, so it can neither produce all
    possible partitions, nor produce partitions w/ equal probability. It seems to work well enough...

    If we cannot generate a valid partition after the given number of tries, raise an exception
    """
    if n - 3 < len(_Q_MEMO):
        return random_integer_partition(n, m, rng)

    for _ in range(tries):
        base = sum(approx_q(n, i) for i in range(m))
        index = base + rng.randrange(approx_q(n, m))

        part = approx_partition_by_index(index, n)

        if len(part) == m:
            return part

    raise Exception("too many tries to approximate partition q(%s, %s)" % (n, m))


def random_integer_partition(n, m, random: Random=Random()):
    """ construct a uniformly distributed random integer partition.

    with memoization, this requires Θ(n) time, at the cost of Θ(n²) space
    precomputation.
    """
    base = sum(q(n, i) for i in range(m))
    index = base + random.randrange(q(n, m))  # index of the partition
    return partition_by_index(index, n)


def random_integer_partition_1(n, random: Random=Random()):
    """ construct a uniformly distributed random integer partition.

    with memoization, this requires Θ(n) time, at the cost of Θ(n²) space
    precomputation.
    """
    index = random.randrange(p(n))  # index of the partition
    return partition_by_index(index, n)


def partition_by_index(index: int, n: int):
    """
    translate an number into a partition given on a particular tree, in
    preorder.

    throughout, we keep track of the size of the generally smaller "left"
    branch, which can be computed conveniently as q(n,m) for some n and m.

    if the q table is pre-computed, running time is O(n). however, the q
    table requires Θ(n²) space.

    partition tree for n = 8:

        0                   11111111
                           /
        1           2111111
                   /      \
        2    221111        311111
              /           /      \
        3  22211     32111        41111
            /       /    \       /     \
        4 2222  3221    3311  4211      5111
                         /    /  \     /    \
        5              332  422   431 521    611
                                   \   \     /  \
        6                          44  53   62   71
                                                  \
        7                                          8

    from:
        Knuth, D. E. The Art of Computer Programming, Vol. 4 Fascicle 3,
        Generating All Combinations and Partitions (2005), vi+ 150pp. ISBN
        0-201-85394-9. § 7.2.1.4 Excercise 10.b (p. 54)

    a) the root of the tree is [1] * n
    b) the left child of the current node is formed by changing '11' into '2'
       if possible.
    c) the right child of the current node is formed by merging a '1'
       with the smallest part larger than 1, if it is unique.

    the tree for n = 10, but laid out sideways to make it clearer:

        i:
        0   1111111111-211111111-31111111-----------4111111--------------
                       |         |                  |
        1              22111111  3211111-331111     421111-43111---4411
                       |         |       |          |     |        |
        2              2221111   322111  33211-3331 42211 4321-433 442
                       |         |       |          |
        3              222211    32221   3322       4222
                       |
        4              22222

        continued:

        0  --511111------------61111-------7111---811-91-A
             |                 |           |      |
        1    52111-5311-541-55 6211-631-64 721-73 82
             |     |           |
        2    5221  532         622

    from this layout, it's clear that the size of the "left" branch a partition

        part = part[0], part[1], ...

    in row "i" is:

        q(  ∑  part[j] , part[i] )
           j>i

    indices in the tree for n = 10:

        0-1-6-------14----------23----------30-------35----38-40-41
          | |       |           |           |        |     |
          2 7-10    15-18----21 24-26-28-29 31-33-34 36-37 39
          | | |     |  |     |  |  |        |
          3 8 11-13 16 19-20 22 25 27       32
          | | |     |
          4 9 12    17
          |
          5

    in practice, constructing partitions is actually easier for the conjugate
    of the above tree:

             A
             |
            91-82-73-64-55
            |
           811-----------------------------------------721-631-541
           |                                             |
          7111----------------------------6211-5311-4411 622-532-442
          |                                  |               |
         61111------------------52111-43111  5221------4321  433
         |                          |           |         |
        511111--------421111-331111 42211-33211 4222-3322 3331
        |                  |          |
       4111111----3211111  322111     32221
       |                |       |      |
      31111111-22111111 2221111 222211 22222
      |
     211111111
     |
    1111111111

    """
    part = [n, 0, 0]
    pointer = 1  # keeps track of which bin to increment

    while index:  # if index == 0, we've found the partition we're looking for.
        left = q(n, pointer)  # the size of the left branch
        part[0] -= 1  # decrement the first number on each step

        if index >= left:  # the node we're looking for isn't in the left branch
            # move down
            part[pointer] += 1  # add 1 to the current bin
            pointer += 1  # move the bin to increment to the right
            index -= left  # we've jumped this many indices in the tree

            if pointer == len(part):
                part.append(0)

        else:
            # look into the left branch
            n -= pointer  # decrease the freedom in the partition

            part[1] += 1  # increment the 2nd bin
            pointer = 2  # start counting at the 2nd bin
            index -= 1  # jump 1 index

    return part[:part.index(0)]


def approx_partition_by_index(index: int, n: int):
    part = [n, 0, 0]
    pointer = 1  # keeps track of which bin to increment

    while index:  # if index == 0, we've found the partition we're looking for.
        left = approx_q(n, pointer)  # the size of the left branch
        part[0] -= 1  # decrement the first number on each step

        if index >= left:  # the node we're looking for isn't in the left branch
            # move down
            part[pointer] += 1  # add 1 to the current bin
            pointer += 1  # move the bin to increment to the right
            index -= left  # we've jumped this many indices in the tree

            if pointer == len(part):
                part.append(0)

        else:
            # look into the left branch
            n -= pointer  # decrease the freedom in the partition

            part[1] += 1  # increment the 2nd bin
            pointer = 2  # start counting at the 2nd bin
            index -= 1  # jump 1 index

    return part[:part.index(0)]
