import math


def largest_remainder_rounding(values_to_round, total: int):
    """
    given a list of fractional values that sum to a given total, round all of these values to integers in a fair way,
    while keeping the same total

    see https://en.wikipedia.org/wiki/Largest_remainder_method

    :param values_to_round:
    :param total: the integer total of the given list
    :return: a list of integers that sum to the given total
    """
    fractions_values, int_values = zip(*map(math.modf, values_to_round))
    int_values = list(map(int, map(round, int_values)))
    ordered_fractional_value_index = sorted(range(len(fractions_values)),
                                            key=fractions_values.__getitem__)
    running_total = sum(int_values)

    while running_total < total and ordered_fractional_value_index:
        index = ordered_fractional_value_index.pop()
        int_values[index] += 1
        running_total += 1

    return int_values


def scale_value(value, value_min, value_max, scale_min, scale_max):
    return ((value - value_min) * (scale_max - scale_min) / (value_max - value_min)) + scale_min


def scale_value_in_range(value, value_range, scale_range):
    return scale_value(value, value_range.min, value_range.max - 1, scale_range.min, scale_range.max - 1)


def bound(value, min_value, max_value):
    return min(max_value, max(min_value, value))


def bound_range(value, value_range):
    return bound(value, value_range.min, value_range.max - 1)
