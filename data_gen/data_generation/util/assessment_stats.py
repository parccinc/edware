import math
from functools import partial, reduce
from operator import mul

from data_generation.util.id_gen import IDGen
from data_generation.util.math import scale_value
from data_generation.util.perf_levels import PerfLevel
from data_generation.util.stats import normalize
from data_generation.util.weighted_choice import weighted_choice


# like sum, but with multiplication
product = partial(reduce, mul)


class Stats:
    """
    stats for a specific demographic value

    implemented as pre-normalized list
    """
    def __init__(self, *values: [float]):
        self._values = normalize(values)

    def __bool__(self):
        return bool(sum(self._values))

    def __eq__(self, other):
        return isinstance(other, Stats) and self._values == other._values

    def __hash__(self):
        return hash(self._values)

    def __len__(self):
        return len(self._values)

    def __iter__(self):
        return iter(self._values)

    def __getattr__(self, item):
        return getattr(self._values, item)

    def __getitem__(self, item):
        return self._values[item]

    def __repr__(self):
        return "Stats(%s)" % (', '.join("%.4f" % value for value in self._values))


class DemographicLevels(dict):
    """ an individual demographic """
    def __init__(self, values: dict=None, **stats: {str: Stats}):
        super().__init__(stats)
        if values is not None:
            self.update(values)


class GradeLevels(dict):
    """ dictionary of demographics by name """
    def __init__(self, totals: [float], demographic_levels: dict):
        super().__init__(demographic_levels)
        self.totals = normalize(totals)

    @property
    def num_levels(self):
        """"""
        return len(self.totals)


class RandomLevelByDemographics:
    """
    We want to be able to generate assessment levels for a student in such a way that the distribution of
    the results matches our expectations for the distribution of levels for various demographics. Unfortunately,
    we only have data about the distribution of levels for individual demographics. However, We can use basic
    statistics to create a reasonable approximate distribution.

    By Bayes' law,

        P[student's score is a certain level | student demographics]

        = P[student demographics | student's score is a certain level]
          * P[student's score is a certain level]
          / P[student demographics]

    We make the simplifying assumption that the demographics are independent (the Naive Bayes assumption)

          P[demo1=val1 & demo2=val2 | level=X]
        = P[demo1=val1 | level=X) * P(demo2=val2 | level=X]

    This allows us to construct an approximate distribution, which we can use to choose a random level. In
    practice, this approximation seems to conform to the demographic distributions within a couple of percent.
    """

    def __init__(self,
                 demographics: dict,
                 level_breakdowns: GradeLevels):
        self.demographics = demographics
        self.level_breakdowns = level_breakdowns

    def _p_demo_is_val_given_level(self,
                                   demo_name: str,
                                   demo_val: str,
                                   level: int) -> float:
        """
          P(demo_name = demo_val | level)
        = P(level | demo_name = demo_val) / ( P(level | demo_name = val1) + P(level | demo_name = val2) + ... )
        """
        sum_ = sum(self.level_breakdowns[demo_name][demo_val2][level] * self.demographics[demo_name][demo_val2]
                   for demo_val2 in self.demographics[demo_name])

        return self.level_breakdowns[demo_name][demo_val][level] * self.demographics[demo_name][demo_val] / sum_

    def _p_score_is_level(self, level: int) -> float:
        return self.level_breakdowns.totals[level]

    def distribution(self, entity: dict) -> [float]:
        """
        Generate a probability distribution for a student.

        The student is assumed to be a dict-like structure, where the keys are demographic names, and the
        values are the student's specific demographic group.

        @returns a list of floats, which sums to 1.
        """
        probs = [(self._p_score_is_level(level) *
                  product(self._p_demo_is_val_given_level(demo_name, demo_val, level)
                          for demo_name, demo_val in entity.items()))
                 for level in range(self.level_breakdowns.num_levels)]

        # normalize (instead of computing the denominator, which is fixed)
        return normalize(probs)

    def random_level(self,
                     entity: dict,
                     id_gen: IDGen,
                     seed=None) -> int:
        """
        Given a student, return a random level chosen according to their demographic values
        """
        probs = {i: prob for i, prob in enumerate(self.distribution(entity))}

        if all(prob == 0.0 for prob in probs.values()):
            # the demographics say this student doesn't exist... just roll with it and choose something uniformly
            return id_gen.choice(list(probs.keys()))

        return weighted_choice(probs, rng=id_gen, seed=seed)


def random_score_given_level(level: PerfLevel, id_gen: IDGen) -> int:
    """
    Generate a random score given a level.
    Note that we are generating scores uniformly within a level, as there is
    no requirement to do anything more complex.

    @param cuts: the cut points for the levels.
    """
    return id_gen.randrange(level.start, level.stop)


def adjust_score(score: int, scale_factor: float, score_range, id_gen: IDGen) -> int:
    """
    "scale" a score by a factor. This is used to alter scores of students who attend schools which are generally
    considered to perform better or worse than average.

    We use borrow here from the idea of "gamma correction" to shift the whole distribution without bunching scores up
    at the extrema or creating gaps around the extrema, as fixed subtraction or multiplication would.
    """
    min_score = score_range.min
    max_score = score_range.max - 1

    if score != max_score:
        score += id_gen.random()  # to fill in gaps in the range

    score_perc = (score - min_score) / (max_score - min_score)
    scaled_perc = pow(score_perc, 1 - scale_factor)
    return max(min_score, min(max_score, int(min_score + (max_score - min_score) * scaled_perc)))


def random_claims(score: int, score_range, claim_weights_maybe_dict, claim_range, id_gen: IDGen):
    """
    generate random claim scores such that

    score == sum(claim_weight[i] * claim[i] for i in NUMBER_OF_CLAIMS)

    This has been extended to the case when the weights are given as values in a dict. In this case, this will return
    a dict of keys to calculated claims.
    Otherwise, it will return a list, in the same order as the weight list.

    TODO: This doesn't generate a uniform distribution of possible claims.
          That could probably be fixed somehow using the integer partitioning code, but it's expensive to use on
          large numbers, and I don't think that level of detail is strictly necessary at the moment...
    """
    if isinstance(claim_weights_maybe_dict, dict):
        claim_keys, claim_weights = zip(*claim_weights_maybe_dict.items())

    else:
        claim_keys = None
        claim_weights = claim_weights_maybe_dict

    if not (.999 < sum(claim_weights) < 1.001):  # python 3.5 has a math.isclose...
        raise ValueError("claim weights %s do not sum to 1!" % (claim_weights,))

    # shuffle the order of claims to try to even out the distribution
    # note: I don't think this actually produces a uniform distribution, but at least it doesn't treat claims
    # with the same weight differently depending on their order
    ordered = list(enumerate(claim_weights))
    id_gen.shuffle(ordered)
    order, claim_weights = zip(*ordered)

    claims = []
    remaining_weight = 1.0

    score_min = score_range.min
    score_max = score_range.max - 1
    claim_min = claim_range.min
    claim_max = claim_range.max - 1

    # scale the score so it looks like a claim score
    remaining_score = scale_value(score, score_min, score_max, claim_min, claim_max)

    for claim_weight in claim_weights:
        remaining_weight -= claim_weight

        min_ = min(claim_max, max(claim_min, math.floor((remaining_score - remaining_weight * claim_max) / claim_weight)))
        max_ = max(claim_min, min(claim_max, math.ceil((remaining_score - remaining_weight * claim_min) / claim_weight)))

        claim = id_gen.randint(min_, max_)
        claims.append(claim)

        remaining_score -= claim * claim_weight

    # unshuffle the claims
    unshuffled_claims = tuple(claims[order.index(i)] for i in range(len(claim_weights)))

    if claim_keys is None:
        return unshuffled_claims

    else:
        return {claim_key: claim_value for claim_key, claim_value in zip(claim_keys, unshuffled_claims)}
