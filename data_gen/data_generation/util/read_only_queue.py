from multiprocessing.queues import Queue
import queue


class ReadOnlyQueue(Queue):
    """
    used to initialize a data structure from which items can be extracted, and which returns None when it is empty
    """
    def __init__(self, iterable):
        super().__init__(len(iterable))
        for item in iterable:
            super().put(item, False)

    def put(self, *args, **kwargs):
        raise AttributeError("cannot put to read-only queue")

    def put_nowait(self, *args, **kwargs):
        raise AttributeError("cannot put to read-only queue")

    def get(self):
        try:
            return super().get(False)

        except queue.Empty:
            return None
