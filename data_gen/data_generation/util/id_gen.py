"""
Generate IDs that conform to student registration (PARCC) requirements.

@author: nestep
@date: February 22, 2014
"""

import multiprocessing
from collections import defaultdict
from pathlib import Path

from random import Random
from numpy.random import RandomState
from uuid import UUID
from data_generation.util.weighted_choice import weighted_choice
import string


def _get_max_proc_id() -> int:
    """
    Get the maximum process ID of the system. This only works on Linux and OS X, but that's all we care about at the moment.
    :return:
    """
    path = Path('/proc/sys/kernel/pid_max')
    if path.exists():
        return int(path.open('r').read().strip()) - 1

    else:
        return 99999  # assume OS X


class IDGen(Random):
    def __init__(self, seed=None):
        """

        :param seed: random initialization seed for deterministic output
        """
        self._r = RandomState(self._seed_to_numpy_seed(seed))
        super().__init__(seed)
        self._proc_id_bits = _get_max_proc_id().bit_length()
        self._rec_id_by_key = defaultdict(int)

    def _seed_to_numpy_seed(self, value):
        # numpy random seeds must be a 32 bit integer or array of such
        if value is None:
            return None

        elif isinstance(value, int):
            if 0 <= value < 2 ** 32:
                return value

            else:
                if value < 0:
                    value = -value

                ints = []
                mask = (2 ** 32) - 1

                while value:
                    ints.append(value & mask)
                    value >>= 32

                return ints

        else:
            raise NotImplementedError("unsupported seed type '%s'. use an integer for now" % (type(value)))

    def seed(self, value=None, version=2):
        super().seed(value, version=version)
        self._r.seed(self._seed_to_numpy_seed(value))

    def get_safe_rec_id(self, key, max_bits=63):
        """
        get a record ID that's safe to use w/ python's multiprocessing module.
        (unless we cycle all the way through the process ids during a run, in which case it will throw an exception.)

        This works by prefixing a generated ID where the most significant bits are populated from the process ID, and
        the rest are kept track of a thread-specific value.
        :param key:
        :param max_bits:
        :return:
        """
        cur_id = multiprocessing.current_process().ident
        max_key_bits = (max_bits - self._proc_id_bits)
        proc_id_part = cur_id << max_key_bits
        key_id_part = self._rec_id_by_key[key]

        if key_id_part.bit_length() >= max_key_bits:
            raise ValueError("Out of unique IDs")

        self._rec_id_by_key[key] += 1
        return proc_id_part + key_id_part

    def gen_student_id(self):
        return self.get_rand_chars(string.ascii_uppercase + string.digits, 30)

    def get_form_id(self):
        return self.get_rand_chars(string.ascii_uppercase + string.digits, 14)

    def get_rand_chars(self, chars: str, length: int):
        """
        get a string of several random chars
        :param chars:
        :param length:
        :return:
        """
        return ''.join(self.choice(chars) for _ in range(length))

    def _uuid4(self):
        return UUID(int=self.getrandbits(128), version=4)

    def get_uuid(self):
        """
        Get a UUID.

        @returns: New UUID - with dashes (len = 36)
        """
        return str(self._uuid4())

    def get_sr_uuid(self):
        """
        Get a UUID that conforms to student registration requirements.

        @returns: New UUID for student registration (without dashes - len = 32)
        """
        return self._uuid4().hex  # [:30]

    def weighted_choice(self, objects_by_weight):
        return weighted_choice(objects_by_weight, self)

    def uniform(self, a=0.0, b=1.0, size=None):
        if size is not None:
            return self._r.uniform(a, b, size)

        else:
            return super().uniform(a, b)

    def triangular(self, low=0.0, high=1.0, mode=None, size=None):
        if size is not None:
            return self._r.triangular(low, high, mode, size)

        else:
            return super().triangular(low, high, mode)

    def gamma(self, shape, scale=1.0, size=None):
        return self._r.gamma(shape, scale, size)
