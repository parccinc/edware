from unittest.mock import MagicMock
from data_generation.util.properties import Properties


class RecordFormat:
    name_format = None
    _args = 1

    @classmethod
    def get_row(cls, *entities) -> Properties:
        raise NotImplementedError()

    @classmethod
    def get_columns(cls) -> tuple:
        """
        :return: a tuple of names of fields in the record
        """
        return tuple(cls.get_row(*[MagicMock() for _ in range(cls._args)]).keys())
