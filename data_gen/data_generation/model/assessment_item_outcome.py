from data_generation.model.assessment_item import AssessmentItem
from data_generation.model.model_base import ModelBase
from data_generation.util.id_gen import IDGen


class AssessmentItemOutcome(ModelBase):
    """
    The PARCC-specific assessment class.
    """

    __slots__ = (
        'rec_id',
        'assessment_item',
        'assessment_outcome_component',
        'admin_code',
        'form_format',
        'parent_item_score',
        'attempted',
    )

    def __init__(self, assessment_item: AssessmentItem, assessment_outcome_component, attempted: bool, id_gen: IDGen):
        super().__init__(id_gen)

        self.rec_id = id_gen.get_safe_rec_id('item')
        self.assessment_item = assessment_item
        self.assessment_outcome_component = assessment_outcome_component
        self.admin_code = "ignore me"  # String(15), nullable=False
        self.form_format = id_gen.choice(('O', 'P'))  # String(5), nullable=False
        self.attempted = attempted

        if attempted:
            # it doesn't matter whether or not these reflect the student's overall score, so just pick something
            self.parent_item_score = id_gen.randint(0, assessment_item.max_score)

        else:
            self.parent_item_score = 0

    @property
    def assessment_outcome(self):
        return self.assessment_outcome_component.assessment_outcome

    @property
    def state(self):
        return self.assessment_outcome.state

    @property
    def responsible_district(self):
        return self.assessment_outcome.responsible_district

    @property
    def responsible_school(self):
        return self.assessment_outcome.responsible_school

    @property
    def test_district(self):
        return self.assessment_outcome.eoy_test_district

    @property
    def test_school(self):
        return self.assessment_outcome.eoy_test_school

    @property
    def assessment(self):
        return self.assessment_item.assessment

    @property
    def assessment_subject(self):
        return self.assessment.assessment_subject

    @property
    def student(self):
        return self.assessment_outcome.student
