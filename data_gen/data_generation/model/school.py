"""
Model the PARCC-specific items of a school.

"""
import logging
import string
import datetime

from data_generation.config.assessment import HS_MATH_PATHS
from data_generation.config.hierarchy import SchoolLevel, GRADE_SIZE_FUDGE
import data_generation.config.population
from data_generation.config.population import Grade
from data_generation.model.model_base import ModelBase
from data_generation.model.student import Student
import data_generation.config.hierarchy as parcc_hier_config
from data_generation.model.district import District, NULL_DISTRICT
from data_generation.util.properties import Properties
from data_generation.util.id_gen import IDGen
from data_generation.util.names import generate_school_name

NULL_SCHOOL = Properties(name=None,
                         identifier=None,
                         district=NULL_DISTRICT)


class School(ModelBase):
    """
    The PARCC-specific school class.
    """
    __slots__ = ('run_id',
                 'guid',
                 'guid_sr',
                 'name',
                 'identifier',
                 'district',
                 'school_type',
                 'grades',
                 '_expected_grade_sizes',
                 '_hs_math_path',
                 '_grade_size_fudge',
                 )

    def __init__(self,
                 school_type: parcc_hier_config.SchoolType,
                 district: District,
                 grades_of_concern: tuple,
                 id_gen: IDGen,
                 grade_size_fudge: float=GRADE_SIZE_FUDGE):
        if not isinstance(school_type, parcc_hier_config.SchoolType):
            raise ValueError("%s id'nt %s" % (school_type, parcc_hier_config.SchoolType))

        super().__init__(id_gen)
        self.name = generate_school_name(school_type.school_level, id_gen, max_name_length=60)
        self.identifier = '%s%s' % (district.identifier[:4], id_gen.get_rand_chars(string.ascii_uppercase + string.digits, 11))
        self.district = district
        self.school_type = school_type
        self._grade_size_fudge = grade_size_fudge

        self.grades = tuple(sorted(
            grade for grade in school_type.grades if grade in grades_of_concern
        ))

        if not self.grades:
            raise ValueError("no grades of concern for this school?")

        # from experience, successive grade sizes are smaller than previous ones, so we want to sort the sizes
        grade_sizes = sorted((max(1,
                                  int(round(id_gen.triangular(school_type.mean_grade_pop * (1 - self._grade_size_fudge),
                                                              school_type.mean_grade_pop,
                                                              school_type.mean_grade_pop * (1 + self._grade_size_fudge)))))
                              for _ in self.grades),
                             reverse=True)

        self._expected_grade_sizes = dict(zip(self.grades, grade_sizes))

        logging.info("      Initialized school %s w/ grades %s, expected grade size %s" % (self.name, [grade.code for grade in self.grades], school_type.mean_grade_pop))

        if self.school_type.school_level in (SchoolLevel.HIGH_SCHOOL, SchoolLevel.FULL_SCHOOL):
            self._hs_math_path = id_gen.choice(HS_MATH_PATHS)

        else:
            self._hs_math_path = None

    def expected_grade_size(self, grade: int) -> int:
        return self._expected_grade_sizes[grade]

    def get_hs_math_assessment_for_grade(self, assessment_grade: int):
        """
        get the chosen HS math test for the given grade

        :param assessment_grade:
        :return:
        """
        return self._hs_math_path[assessment_grade]

    def repopulate_school_grade(self,
                                grade: Grade,
                                grade_students,
                                id_gen: IDGen,
                                acad_year=datetime.datetime.now().year,
                                additional_student_choice=data_generation.config.population.REPOPULATE_ADDITIONAL_STUDENTS):
        """
        Take a school grade and make sure it has enough students. The list of students is updated in-place.

        @param grade: The grade in the school to potentially re-populate
        @param grade_students: The students currently in the grade for this school
        @param id_gen: ID generator
        @param acad_year: The current academic year that the repopulation is occurring within (optional, defaults to your
                          machine clock's current year)
        @param additional_student_choice: Array of values for additional students to create in the grade
        """
        # fudge the grade size a bit more
        # make sure it's an integer because numpy floats don't convert to ints when rounded
        grade_size = max(1,
                         int(round(self._expected_grade_sizes[grade] *
                                   id_gen.uniform(1 - self._grade_size_fudge, 1 + self._grade_size_fudge))))

        initial_size = len(grade_students)
        if initial_size < grade_size:
            grade_students.extend(Student(self, grade, id_gen, acad_year)
                                  for _ in range(grade_size - initial_size))
            logging.debug("        expected size of grade %s: %s; actual size: %s; initial size: %s" %
                          (grade.code, self._expected_grade_sizes[grade], len(grade_students), initial_size))

        else:
            # at this point, grade_students is probably mostly full of advancements

            # occasionally force-add in some novel students
            grade_students.extend(Student(self, grade, id_gen, acad_year)
                                  for _ in range(id_gen.choice(additional_student_choice)))
            force_adds = len(grade_students) - initial_size

            # ensure it meets the expected grade size

            logging.debug("        expected size of grade %s: %s; actual size: %s; initial_size: %s; force_adds: %s" %
                          (grade.code, self._expected_grade_sizes[grade], len(grade_students), initial_size, force_adds))

    @property
    def demographics(self):
        return self.district.demographics
