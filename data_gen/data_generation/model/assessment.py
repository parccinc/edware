"""
Model an assessment for the PARCC assessment.

@author: nestep
@date: February 24, 2014
"""
from data_generation.config.assessment import AssessmentSubject, TestPeriod
from data_generation.config.config import Subject
from data_generation.model.assessment_component import AssessmentComponent, Component
from data_generation.model.assessment_item import AssessmentItem
from data_generation.model.model_base import ModelBase
from data_generation.model.state import State
from data_generation.util.id_gen import IDGen


class Assessment(ModelBase):
    """
    The PARCC-specific assessment class.
    """

    __slots__ = ('guid',
                 'rec_id',
                 'state',
                 'pba',
                 'eoy',
                 'assessment_subject',
                 'grade',
                 'period',
                 'period_year',
                 'year',
                 'overall_perf_levels',
                 'claim_perf_levels',
                 'subclaim_perf_levels',
                 )

    def __init__(self,
                 asmt_year: int,
                 period: TestPeriod,
                 state: State,
                 assessment_subject: AssessmentSubject,
                 perf_levels: tuple,
                 id_gen: IDGen):
        super().__init__(id_gen)

        self.rec_id = id_gen.get_safe_rec_id('asmt')

        self.state = state
        self.assessment_subject = assessment_subject

        if assessment_subject.is_hs_math or len(assessment_subject.grades) != 1:
            self.grade = None

        else:
            self.grade = assessment_subject.grades[0]

        self.period = period
        self.period_year = asmt_year
        self.year = "%s-%s" % (asmt_year - 1, asmt_year)

        self.overall_perf_levels, self.claim_perf_levels, self.subclaim_perf_levels = perf_levels

        self.pba = AssessmentComponent(self, Component.PBA, id_gen)
        self.eoy = AssessmentComponent(self, Component.EOY, id_gen)

    @property
    def subject(self) -> Subject:
        return self.assessment_subject.subject

    @property
    def key(self) -> str:
        return "%s-%s-%s-%s" % (self.assessment_subject.name, self.year, self.period.label, self.state.code)
