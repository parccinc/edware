"""
Model the PARCC-specific items of a state.

@author: nestep
@date: February 22, 2014
"""
from data_generation.config import hierarchy as hierarchy_config
from data_generation.model.model_base import ModelBase
from data_generation.util.id_gen import IDGen


class State(ModelBase):
    """
    The PARCC-specific state class.
    """
    __slots__ = ('guid', 'state_type')

    def __init__(self,
                 state_type: hierarchy_config.StateDescription,
                 id_gen: IDGen):
        super().__init__(id_gen)
        self.state_type = state_type

    @property
    def state_name(self):
        return self.state_type.state_name

    @property
    def code(self):
        return self.state_type.state_name.code

    @property
    def demographics(self):
        return self.state_type.demographics

    @property
    def identifier(self):
        return self.code
