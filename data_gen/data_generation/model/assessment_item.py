from data_generation.config.assessment import ItemType, ItemStandard, ItemEvidenceStatement, ASMT_ITEM_MAX_SCORE_RANGE
from data_generation.model.model_base import ModelBase
from data_generation.util.id_gen import IDGen


class AssessmentItem(ModelBase):
    """
    The PARCC-specific assessment class.
    """

    __slots__ = (
        'guid',
        'assessment_component',
        'max_score',

        'rec_id',
        'batch_guid',
        'p_value',
        'p_distractor1',
        'p_distractor2',
        'p_distractor3',
        'p_distractor4',
        'p_distractor5',
        'p_score0',
        'p_score1',
        'p_score2',
        'p_score3',
        'p_score4',
        'p_score5',
        'p_score6',
        'point_biserial_correct',
        'point_biserial_1',
        'point_biserial_2',
        'point_biserial_3',
        'point_biserial_4',
        'point_biserial_5',
        'dif_any',
        'dif_female',
        'dif_aa',
        'dif_hispanic',
        'dif_asian',
        'dif_na',
        'dif_swd',
        'dif_ell',
        'dif_ses',
        'p_omit',
        'a_parameter',
        'b_parameter',
        'c_parameter',
        'step_1',
        'step_2',
        'step_3',
        'step_4',
        'step_5',
        'step_6',
        'n_size',
        'item_guid',
        'item_group_guid',
        'item_key_response1',
        'item_key_response2',
        'item_key_response3',
        'item_key_response4',
        'item_admin_type',
        'item_grade_course',
        'item_delivery_mode',
        'test_item_type',
        'is_calc_allowed',
        'standard1',
        'standard2',
        'standard3',
        'standard4',
        'standard5',
        'standard6',
        'evidence_statement',
        'evidence_statement1',
        'evidence_statement2',
        'evidence_statement3',
        'evidence_statement4',
        'evidence_statement5',
        'evidence_statement6',
        'create_date',
        'rec_status',
        'status_change_date',
    )

    def __init__(self, assessment_component, subclaim, item_number: int, id_gen: IDGen):
        super().__init__(id_gen)
        self.rec_id = id_gen.get_safe_rec_id('item')
        self.assessment_component = assessment_component
        self.max_score = id_gen.choice(ASMT_ITEM_MAX_SCORE_RANGE)  # min is always 0

        self.p_value = None
        self.p_distractor1 = None
        self.p_distractor2 = None
        self.p_distractor3 = None
        self.p_distractor4 = None
        self.p_distractor5 = None
        self.p_score0 = None
        self.p_score1 = None
        self.p_score2 = None
        self.p_score3 = None
        self.p_score4 = None
        self.p_score5 = None
        self.p_score6 = None
        self.point_biserial_correct = None
        self.point_biserial_1 = None
        self.point_biserial_2 = None
        self.point_biserial_3 = None
        self.point_biserial_4 = None
        self.point_biserial_5 = None
        self.dif_any = None
        self.dif_female = None
        self.dif_aa = None
        self.dif_hispanic = None
        self.dif_asian = None
        self.dif_na = None
        self.dif_swd = None
        self.dif_ell = None
        self.dif_ses = None
        self.p_omit = None
        self.a_parameter = None
        self.b_parameter = None
        self.c_parameter = None
        self.step_1 = None
        self.step_2 = None
        self.step_3 = None
        self.step_4 = None
        self.step_5 = None
        self.step_6 = None
        self.n_size = None

        self.item_guid = "%s_%s" % (_format_subclaim(subclaim), item_number,)  # it may be called "guid" but it's used for sorting and display...

        self.item_group_guid = None
        self.item_key_response1 = None
        self.item_key_response2 = None
        self.item_key_response3 = None
        self.item_key_response4 = None
        self.item_admin_type = None
        self.item_grade_course = None
        self.item_delivery_mode = None

        self.test_item_type = id_gen.choice(ItemType)
        self.is_calc_allowed = None

        self.standard1 = id_gen.choice(ItemStandard)
        self.standard2 = id_gen.choice(ItemStandard)
        self.standard3 = id_gen.choice(ItemStandard)
        self.standard4 = id_gen.choice(ItemStandard)
        self.standard5 = id_gen.choice(ItemStandard)
        self.standard6 = id_gen.choice(ItemStandard)

        self.evidence_statement = id_gen.choice(ItemEvidenceStatement)
        self.evidence_statement1 = id_gen.choice(ItemEvidenceStatement)
        self.evidence_statement2 = id_gen.choice(ItemEvidenceStatement)
        self.evidence_statement3 = id_gen.choice(ItemEvidenceStatement)
        self.evidence_statement4 = id_gen.choice(ItemEvidenceStatement)
        self.evidence_statement5 = id_gen.choice(ItemEvidenceStatement)
        self.evidence_statement6 = id_gen.choice(ItemEvidenceStatement)

    @property
    def assessment(self):
        return self.assessment_component.assessment


def _format_subclaim(subclaim):
    return '_'.join(part[:2] for part in subclaim.name.split('_'))
