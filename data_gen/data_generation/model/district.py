"""
Model the PARCC-specific items of a district.

@author: nestep
@date: February 22, 2014
"""
import string
import data_generation.config.config
import data_generation.config.hierarchy as hier_config
from data_generation.model.model_base import ModelBase
from data_generation.model.state import State
from data_generation.util.properties import Properties
from data_generation.util.id_gen import IDGen
from data_generation.util.names import generate_district_name


NULL_DISTRICT = Properties(name=None,
                           identifier=None)


class District(ModelBase):
    """
    The PARCC-specific district class.
    """

    __slots__ = ('run_id', 'guid', 'guid_sr',
                 'name', 'state', 'district_type', 'identifier')

    def __init__(self,
                 district_type: hier_config.DistrictType,
                 state: State,
                 id_gen: IDGen):
        if not isinstance(district_type, hier_config.DistrictType):
            raise ValueError("%s is not of type %s" % (district_type, hier_config.DistrictType))

        super().__init__(id_gen)

        # there's now a use case in having certain specific IDs available in all generated data, for SSO permissions testing
        if state.state_name in data_generation.config.hierarchy.FORCE_DISTRICT_IDS:
            forced_id = data_generation.config.hierarchy.FORCE_DISTRICT_IDS[state.state_name].get()

        else:
            forced_id = None

        if forced_id:
            self.identifier = forced_id.id
            self.name = forced_id.name

        else:
            self.identifier = "%s%s" % (state.code, id_gen.get_rand_chars(string.ascii_uppercase + string.digits, 13))
            self.name = generate_district_name(id_gen, max_name_length=60)

        self.state = state
        self.district_type = district_type

    @property
    def demographics(self):
        return self.state.demographics
