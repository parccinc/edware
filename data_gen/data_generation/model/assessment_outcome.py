"""
Model an assessment outcome (an instance of a student taking an assessment) for the PARCC assessment.

"""
import data_generation.config.assessment
import data_generation.config.config as config
from data_generation.config.population import PRGDisabilityType, AccommodationExtendedTime, Accommodation
from data_generation.config.assessment import RecordType, ReportSuppressionCode, ReportSuppressionAction, \
    ComponentAttemptCategory
from data_generation.config.config import Subject
import data_generation.config.population

from data_generation.model.assessment import Assessment
from data_generation.model.assessment_component import AssessmentComponent
from data_generation.model.assessment_outcome_component import AssessmentOutcomeComponent, AssessmentOutcomeSummative
from data_generation.model.district import District
from data_generation.model.model_base import ModelBase
from data_generation.model.school import School
from data_generation.model.state import State
from data_generation.model.student import Student
from data_generation.util import names

from data_generation.util.assessment_stats import RandomLevelByDemographics
from data_generation.util.id_gen import IDGen


class AssessmentOutcome(ModelBase):
    """
    The PARCC-specific assessment outcome class.
    """

    __slots__ = ('guid',
                 'rec_id',

                 'student',
                 'staff',
                 'assessment',
                 'assessment_year',

                 'pba',
                 'eoy',
                 'summative',

                 'is_multi_rec',
                 'record_type',
                 'is_reported_summative',
                 'reported_roster_flag',
                 'report_suppression_code',
                 'report_suppression_action',

                 'accomod_freq_breaks',
                 'accomod_alt_location',
                 'accomod_small_group',
                 'accomod_special_equip',
                 'accomod_spec_area',
                 'accomod_time_day',
                 'accomod_answer_mask',
                 'accomod_color_contrast',
                 'accomod_text_2_speech_math',
                 'accomod_read_math',
                 'accomod_asl_video',
                 'accomod_screen_reader',
                 'accomod_close_capt_ela',
                 'accomod_read_ela',
                 'accomod_braille_ela',
                 'accomod_tactile_graph',
                 'accomod_text_2_speech_ela',
                 'accomod_answer_rec',
                 'accomod_braille_resp',
                 'accomod_calculator',
                 'accomod_construct_resp_ela',
                 'accomod_select_resp_ela',
                 'accomod_math_resp',
                 'accomod_monitor_test_resp',
                 'accomod_word_predict',
                 'accomod_native_lang',
                 'accomod_loud_native_lang',
                 'accomod_math_rsp',
                 'accomod_math_text_2_speech',
                 'accomod_math_trans_online',
                 'accomod_w_2_w_dict',
                 'accomod_extend_time',
                 'accomod_alt_paper_test',
                 'accomod_paper_trans_math',
                 'accomod_human_read_sign',
                 'accomod_large_print',
                 'accomod_braille_tactile',

                 'opt_state_data_1',
                 'opt_state_data_2',
                 'opt_state_data_3',
                 'opt_state_data_4',
                 'opt_state_data_5',
                 'opt_state_data_6',
                 'opt_state_data_7',
                 'opt_state_data_8',

                 )

    def __init__(self,
                 assessment: Assessment,
                 student: Student,
                 pba_test_school: School,
                 eoy_test_school: School,
                 level_generator: RandomLevelByDemographics,
                 is_first_year: bool,
                 staff_in_subject_in_grade_in_school: list,
                 id_gen: IDGen,
                 summative_suppressed_rate: float=data_generation.config.assessment.SUMMATIVE_SUPPRESSED_RATE,
                 disabled_accommodation_rate: float=data_generation.config.population.DISABLED_ACCOMMODATION_RATE,
                 unlikely_accommodation_rate: float=data_generation.config.population.UNLIKELY_ACCOMMODATION_RATE,
                 likely_accommodation_rate: float=data_generation.config.population.LIKELY_ACCOMMODATION_RATE,
                 general_accommodation_rate: float=data_generation.config.population.GENERAL_ACCOMMODATION_RATE,
                 optional_data_fill_rate: float=config.OPTIONAL_DATA_FILL_RATE,
                 pba_component_not_assigned_rate: float=data_generation.config.assessment.COMPONENT_NOT_ASSIGNED_RATE,
                 pba_component_not_tested_rate: float=data_generation.config.assessment.COMPONENT_NOT_TESTED_RATE,
                 pba_min_attempted_percent: float=data_generation.config.assessment.MIN_ATTEMPTED_PERCENT,
                 pba_component_voided_rate: float=data_generation.config.assessment.COMPONENT_VOIDED_RATE,
                 pba_attempted_percent_range: float=data_generation.config.assessment.ATTEMPTED_PERCENT_RANGE,
                 eoy_component_not_assigned_rate: float=data_generation.config.assessment.COMPONENT_NOT_ASSIGNED_RATE,
                 eoy_component_not_tested_rate: float=data_generation.config.assessment.COMPONENT_NOT_TESTED_RATE,
                 eoy_min_attempted_percent: float=data_generation.config.assessment.MIN_ATTEMPTED_PERCENT,
                 eoy_component_voided_rate: float=data_generation.config.assessment.COMPONENT_VOIDED_RATE,
                 eoy_attempted_percent_range: float=data_generation.config.assessment.ATTEMPTED_PERCENT_RANGE,
                 no_component_no_staff_rate: float=data_generation.config.assessment.NO_COMPONENT_NO_STAFF_RATE,
                 component_no_staff_rate: float=data_generation.config.assessment.COMPONENT_NO_STAFF_RATE,
                 ):
        super().__init__(id_gen)

        self.rec_id = id_gen.get_safe_rec_id('ao')

        self.student = student
        self.assessment = assessment

        self.is_multi_rec = None  # we're not doing multi record anything

        self.pba = AssessmentOutcomeComponent(assessment.pba, self, student, pba_test_school, level_generator, id_gen,
                                              component_not_assigned_rate=pba_component_not_assigned_rate,
                                              component_not_tested_rate=pba_component_not_tested_rate,
                                              min_attempted_percent=pba_min_attempted_percent,
                                              component_voided_rate=pba_component_voided_rate,
                                              attempted_percent_range=pba_attempted_percent_range)
        self.eoy = AssessmentOutcomeComponent(assessment.eoy, self, student, eoy_test_school, level_generator, id_gen,
                                              component_not_assigned_rate=eoy_component_not_assigned_rate,
                                              component_not_tested_rate=eoy_component_not_tested_rate,
                                              min_attempted_percent=eoy_min_attempted_percent,
                                              component_voided_rate=eoy_component_voided_rate,
                                              attempted_percent_range=eoy_attempted_percent_range)

        if self.pba.is_valid() and self.eoy.is_valid():
            self.record_type = RecordType.SUMMATIVE
            self.is_reported_summative = True

            if id_gen.random() < summative_suppressed_rate:
                # TODO: report suppression actions actually have a meaning, but figuring it out is low priority
                self.report_suppression_code = id_gen.choice([rsc for rsc in ReportSuppressionCode if rsc != ReportSuppressionCode.NONE])
                self.report_suppression_action = id_gen.choice([rsa for rsa in ReportSuppressionAction if rsa != ReportSuppressionAction.NONE])

            else:
                self.report_suppression_code = ReportSuppressionCode.NONE
                self.report_suppression_action = ReportSuppressionAction.NONE

        elif self.pba.is_valid() or self.eoy.is_valid():
            self.record_type = RecordType.COMPONENT
            self.is_reported_summative = False
            self.report_suppression_code = None
            self.report_suppression_action = None

        else:
            self.record_type = RecordType.NO_COMPONENT
            self.is_reported_summative = False
            self.report_suppression_code = None
            self.report_suppression_action = None

            # ensure these aren't set...
            self.eoy.raw_score = None
            self.pba.raw_score = None

            # crazy logic for the 03 category that may be somewhat wrong
            if self.pba.attempt_flag is None and self.eoy.attempt_flag is None:
                self.pba.category = ComponentAttemptCategory.A
                self.eoy.category = ComponentAttemptCategory.A

            elif self.pba.void_reason and self.eoy.void_reason:
                self.pba.category = ComponentAttemptCategory.B
                self.eoy.category = ComponentAttemptCategory.B

            elif (self.pba.void_reason and self.eoy.has_assignment) or (self.eoy.void_reason and self.pba.has_assignment):
                self.pba.category = ComponentAttemptCategory.C
                self.eoy.category = ComponentAttemptCategory.C

            elif ((self.pba.attempt_flag is None and (self.eoy.attempt_flag is None or not self.eoy.has_assignment)) and
                  (self.eoy.attempt_flag is None and (self.pba.attempt_flag is None or not self.pba.has_assignment))):
                self.pba.category = ComponentAttemptCategory.D
                self.eoy.category = ComponentAttemptCategory.D

            elif self.pba.has_assignment and self.eoy.has_assignment:
                self.pba.category = ComponentAttemptCategory.E
                self.eoy.category = ComponentAttemptCategory.E

            elif (self.pba.has_assignment and not self.eoy.attempt_flag) or (self.eoy.has_assignment and not self.pba.attempt_flag):
                self.pba.category = ComponentAttemptCategory.F
                self.eoy.category = ComponentAttemptCategory.F

            elif (self.pba.has_assignment and self.eoy.void_reason) or (self.eoy.has_assignment and self.pba.void_reason):
                self.pba.category = ComponentAttemptCategory.G
                self.eoy.category = ComponentAttemptCategory.G

            elif ((self.pba.has_assignment and (not self.eoy.has_assignment or not self.eoy.attempt_flag)) or
                  (self.eoy.has_assignment and (not self.pba.has_assignment or not self.pba.attempt_flag))):
                self.pba.category = ComponentAttemptCategory.H
                self.eoy.category = ComponentAttemptCategory.H

            else:
                # I'm not 100% sure the above logic is correct, set a different value and hope it gets through.
                self.pba.category = ComponentAttemptCategory.UNKNOWN
                self.eoy.category = ComponentAttemptCategory.UNKNOWN

        if self.record_type == RecordType.SUMMATIVE:
            self.reported_roster_flag = None

        else:
            self.reported_roster_flag = student.is_roster_reported

        if self.record_type == RecordType.NO_COMPONENT:
            if id_gen.random() < no_component_no_staff_rate:
                self.staff = None

            else:
                self.staff = id_gen.choice(staff_in_subject_in_grade_in_school)

        else:
            if id_gen.random() < component_no_staff_rate:
                self.staff = None

            else:
                self.staff = id_gen.choice(staff_in_subject_in_grade_in_school)

        self.summative = AssessmentOutcomeSummative(self.is_reported_summative,
                                                    self.assessment,
                                                    self.pba,
                                                    self.eoy,
                                                    is_first_year,
                                                    id_gen)

        # Create the date taken
        self.assessment_year = '%s-%s' % (assessment.period_year - 1, assessment.period_year)

        self.opt_state_data_1 = self._optional_fill(optional_data_fill_rate, id_gen)
        self.opt_state_data_2 = self._optional_fill(optional_data_fill_rate, id_gen)
        self.opt_state_data_3 = self._optional_fill(optional_data_fill_rate, id_gen)
        self.opt_state_data_4 = self._optional_fill(optional_data_fill_rate, id_gen)
        self.opt_state_data_5 = self._optional_fill(optional_data_fill_rate, id_gen)
        self.opt_state_data_6 = self._optional_fill(optional_data_fill_rate, id_gen)
        self.opt_state_data_7 = self._optional_fill(optional_data_fill_rate, id_gen)
        self.opt_state_data_8 = self._optional_fill(optional_data_fill_rate, id_gen)

        # now, accommodations

        # set default values for all accommodations
        for accommodation in Accommodation:
            setattr(self, accommodation.name, None)

        disability_type = student.prg_primary_disability or ''

        if student.prg_disability:
            # SIGN
            if disability_type in (PRGDisabilityType.HI,):
                if id_gen.random() < disabled_accommodation_rate:  # is_video
                    self.accomod_asl_video = True

                if id_gen.random() < disabled_accommodation_rate:
                    self.accomod_human_read_sign = id_gen.choice(Accommodation.accomod_human_read_sign.values)

                else:
                    if assessment.subject == Subject.ELA:
                        self.accomod_read_ela = id_gen.choice(Accommodation.accomod_read_ela.values)

                    else:
                        self.accomod_read_math = id_gen.choice(Accommodation.accomod_read_math.values)

            if disability_type in (PRGDisabilityType.DB,):
                self.accomod_braille_tactile = True
                self.accomod_braille_resp = id_gen.choice(Accommodation.accomod_braille_resp.values)

                if assessment.subject == Subject.ELA:
                    self.accomod_braille_ela = True

            if disability_type in (PRGDisabilityType.VI,):
                if assessment.subject == Subject.ELA:
                    if id_gen.random() < disabled_accommodation_rate:
                        self.accomod_close_capt_ela = True

                    else:
                        self.accomod_text_2_speech_ela = True

                else:
                    self.accomod_text_2_speech_math = True

                if id_gen.random() < general_accommodation_rate:
                    self.accomod_screen_reader = True

            else:
                if id_gen.random() < general_accommodation_rate:
                    self.accomod_answer_mask = True

                if id_gen.random() < general_accommodation_rate:
                    self.accomod_color_contrast = id_gen.choice(Accommodation.accomod_color_contrast.values)

                if id_gen.random() < general_accommodation_rate:
                    self.accomod_large_print = True

                if id_gen.random() < general_accommodation_rate:
                    self.accomod_tactile_graph = True

            if id_gen.random() < general_accommodation_rate:
                self.accomod_answer_rec = True

            if id_gen.random() < general_accommodation_rate:
                self.accomod_monitor_test_resp = True

            if assessment.subject == Subject.Math and id_gen.random() < disabled_accommodation_rate:
                self.accomod_calculator = True  # I think this is the 'abacus' for the blind option, but ?

            if assessment.subject == Subject.ELA:
                if id_gen.random() < general_accommodation_rate:
                    self.accomod_construct_resp_ela = id_gen.choice(Accommodation.accomod_construct_resp_ela.values)

                if id_gen.random() < general_accommodation_rate:
                    self.accomod_select_resp_ela = id_gen.choice(Accommodation.accomod_select_resp_ela.values)

            if assessment.subject == Subject.Math:
                if id_gen.random() < general_accommodation_rate:
                    self.accomod_math_resp = id_gen.choice(Accommodation.accomod_math_resp.values)

            if id_gen.random() < general_accommodation_rate:
                self.accomod_word_predict = True

        if student.prg_lep:
            self.accomod_w_2_w_dict = id_gen.random() < unlikely_accommodation_rate
            if id_gen.random() < likely_accommodation_rate:
                # half the time we set "native_lang", the other half "load_native_lang"
                if id_gen.random() < .5:
                    self.accomod_native_lang = True

                else:
                    self.accomod_loud_native_lang = id_gen.choice(Accommodation.accomod_loud_native_lang.values)

            if assessment.subject == Subject.Math and id_gen.random() < disabled_accommodation_rate:
                # choose one of accomod_math_text_2_speech, accomod_math_trans_online, or accomod_paper_trans_math
                rv = id_gen.random()
                if rv < 1 / 3:
                    self.accomod_math_text_2_speech = id_gen.choice(Accommodation.accomod_math_text_2_speech.values)

                elif rv < 2 / 3:
                    self.accomod_math_trans_online = id_gen.choice(Accommodation.accomod_math_trans_online.values)

                else:
                    self.accomod_paper_trans_math = id_gen.choice(Accommodation.accomod_paper_trans_math.values)

                if id_gen.random() < unlikely_accommodation_rate:
                    self.accomod_math_rsp = id_gen.choice(Accommodation.accomod_math_rsp.values)

        if student.prg_lep or student.prg_iep or student.prg_sec504:
            if id_gen.random() < disabled_accommodation_rate:
                if student.prg_lep and (student.prg_iep or student.prg_sec504):
                    self.accomod_extend_time = AccommodationExtendedTime.BOTH

                elif student.prg_lep:
                    self.accomod_extend_time = AccommodationExtendedTime.EL

                else:
                    self.accomod_extend_time = AccommodationExtendedTime.IEP504

        self.accomod_alt_location = id_gen.random() < general_accommodation_rate
        self.accomod_small_group = id_gen.random() < general_accommodation_rate
        self.accomod_special_equip = id_gen.random() < general_accommodation_rate
        self.accomod_spec_area = id_gen.random() < general_accommodation_rate
        self.accomod_time_day = id_gen.random() < general_accommodation_rate
        self.accomod_alt_paper_test = id_gen.random() < general_accommodation_rate
        self.accomod_freq_breaks = id_gen.random() < general_accommodation_rate

    @staticmethod
    def _optional_fill(optional_data_fill_rate: float, id_gen: IDGen):
        """
        Generate some random data for the "optional" fields.

        We don't really know what the possible values of these fields are.

        Currently this returns either a 3-digit number or a truncated random district-like name.
        """
        if id_gen.random() < optional_data_fill_rate:
            fill_type = id_gen.choice(('number', 'name'))
            if fill_type == 'number':
                return str(id_gen.randrange(100, 1000))

            elif fill_type == 'name':
                return names.generate_district_name(id_gen, 20)

            else:
                return None

        else:
            return None

    @property
    def staff_id(self):
        if self.staff:
            return self.staff.staff_id

        else:
            return None

    @property
    def subject(self) -> Subject:
        return self.assessment.subject

    @property
    def state(self) -> State:
        return self.student.school.district.state

    @property
    def pba_test_district(self) -> District:
        return self.pba.test_school.district

    @property
    def pba_test_school(self) -> School:
        return self.pba.test_school

    @property
    def eoy_test_district(self) -> District:
        return self.pba.test_school.district

    @property
    def eoy_test_school(self) -> School:
        return self.pba.test_school

    @property
    def responsible_district(self) -> District:
        return self.student.school.district

    @property
    def responsible_school(self) -> School:
        return self.student.school

    @property
    def pba_assessment(self) -> AssessmentComponent:
        return self.assessment.pba

    @property
    def eoy_assessment(self) -> AssessmentComponent:
        return self.assessment.eoy
