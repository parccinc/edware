from data_generation.util.id_gen import IDGen


class ModelBase:
    """ base class for models """
    __slots__ = ('guid',)

    def __init__(self, id_gen: IDGen):
        self.guid = id_gen.get_uuid()

    def __hash__(self):
        return hash(self.guid)

    def __eq__(self, othr):
        return type(self) is type(othr) and self.guid == othr.guid
