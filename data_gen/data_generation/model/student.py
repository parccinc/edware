"""
Model the PARCC-specific items of a student.

@author: nestep
@date: March 3, 2014
"""
import data_generation.config.population as pop_config
from data_generation.config.population import Sex, Ethnicity, StudentStatus, StudentAccommodation, Grade
from data_generation.model.model_base import ModelBase
import datetime
from data_generation.util.weighted_choice import weighted_choice
from data_generation.util.id_gen import IDGen
import calendar
from data_generation.util.names import generate_person_name


class Student(ModelBase):
    """
    The PARCC-specific student class.
    """
    __slots__ = ('guid',

                 'parcc_id',
                 'state_id',
                 'local_id',

                 'school',
                 'grade',

                 'first_name',
                 'middle_name',
                 'last_name',
                 'sex',
                 'birthdate',

                 'level_ethnicity',
                 'eth_hispanic',
                 'eth_amer_ind',
                 'eth_asian',
                 'eth_black',
                 'eth_pacific',
                 'eth_white',
                 'eth_multi',
                 'eth_filler',
                 'eth_federal',

                 'prg_lep',
                 'prg_gifted',
                 'prg_migrant',
                 'prg_econ_disad',
                 'prg_disability',
                 'prg_primary_disability',

                 'prg_sec504',  # assessment accommodation
                 'prg_iep',  # assessment accommodation

                 'parcc_growth_percent',
                 'state_growth_percent',
                 'district_growth_percent',

                 'is_roster_reported',
                 )

    def __init__(self,
                 school,
                 grade: Grade,
                 id_gen: IDGen,
                 acad_year: int=datetime.datetime.now().year):
        """

        :param school:
        :param grade: initial student grade. note that we will (usually) keep this student (usually) and promote the grade from year to year.
        :param id_gen:
        :param acad_year:
        """
        super().__init__(id_gen)

        self.parcc_id = id_gen.get_uuid()
        self.state_id = id_gen.gen_student_id()
        self.local_id = id_gen.gen_student_id()

        self.grade = grade
        self.school = school

        self.birthdate = self._determine_student_dob(id_gen, acad_year)

        # Determine demographics
        (sex, level_ethnicity, ethnicities, iep, sec504, lep, ed, migrant) = self._determine_demographics(id_gen)
        self.sex = sex
        self.first_name, self.middle_name, self.last_name = generate_person_name(sex, id_gen, max_name_length=35)

        self.prg_iep = iep
        self.prg_sec504 = sec504
        self.prg_lep = lep
        self.prg_econ_disad = ed
        self.prg_migrant = migrant

        self.level_ethnicity = level_ethnicity
        self.eth_hispanic = Ethnicity.HISPANIC in ethnicities
        self.eth_amer_ind = Ethnicity.AMERIND in ethnicities
        self.eth_asian = Ethnicity.ASIAN in ethnicities
        self.eth_black = Ethnicity.BLACK in ethnicities
        self.eth_pacific = Ethnicity.HAWAII in ethnicities
        self.eth_white = Ethnicity.WHITE in ethnicities
        self.eth_multi = len(ethnicities) > 1
        self.eth_filler = None
        self.eth_federal = self._get_federal_ethnicity()

        if ((self.prg_iep or self.prg_sec504) and id_gen.random() < pop_config.DISABLED_IEP_PERC) or id_gen.random() < pop_config.DISABLED_OTHER_PERC:
            self.prg_disability = True
            self.prg_primary_disability = id_gen.choice(pop_config.PRGDisabilityType)

        else:
            self.prg_disability = False
            self.prg_primary_disability = None

        self.prg_gifted = True if id_gen.random() < pop_config.GIFTED_RATE else False

        self.parcc_growth_percent = None  # None for the first year
        self.state_growth_percent = None  # None for the first year
        self.district_growth_percent = None  # None for the first year

        self.is_roster_reported = True

    def get_level_demographics(self) -> dict:
        """
        :return: dictionary for use in computing a student's performance level given demographics
        """
        return {Ethnicity: self.level_ethnicity,
                Sex: self.sex,
                StudentAccommodation.SECTION_504: self.prg_sec504,
                StudentAccommodation.IEP: self.prg_iep,
                StudentStatus.LEP: self.prg_lep,
                StudentStatus.ECON_DIS: self.prg_econ_disad}

    def _determine_student_dob(self, id_gen: IDGen, acad_year=datetime.datetime.now().year):
        """Generates an appropriate date of birth given the student's current grade

        :param acad_year: The current academic year this student is being created for (optional, defaults to your machine
                          clock's current year)
        :return: A string representation of the student's date of birth
        """
        approx_age = self.grade.code + 6
        birth_year = acad_year - approx_age

        if calendar.isleap(birth_year):
            bday_offset = id_gen.randint(0, 365)

        else:
            bday_offset = id_gen.randint(0, 364)

        # construct a birth date as an offset from January 1st
        return datetime.date(birth_year, 1, 1) + datetime.timedelta(days=bday_offset)

    def _determine_demographics(self, id_gen: IDGen):
        """Determine the demographic characteristics for a student based on the configuration dictionary.

        :returns: A tuple of characteristics
        """
        demographics = self.school.demographics[self.grade]

        # Determine characteristics
        if pop_config.FORCE_MF:
            sex = demographics[Sex].choose(id_gen)
            while sex not in (Sex.MALE, Sex.FEMALE):
                sex = demographics[Sex].choose(id_gen)

        else:
            sex = demographics[Sex].choose(id_gen)
        ethnicity = demographics[Ethnicity].choose(id_gen)

        iep = demographics[StudentAccommodation.IEP].choose(id_gen)
        sec504 = demographics[StudentAccommodation.SECTION_504].choose(id_gen)
        lep = demographics[StudentStatus.LEP].choose(id_gen)
        ed = demographics[StudentStatus.ECON_DIS].choose(id_gen)
        migrant = demographics[StudentStatus.MIGRANT].choose(id_gen)

        # construct a set of ethnicities
        if ethnicity == Ethnicity.NONE:
            ethnicities = set()

        elif ethnicity == Ethnicity.MULTIPLE:
            ethnicities = set()
            eth_demos = demographics[Ethnicity]

            while len(ethnicities) < 2:
                # TODO: perhaps do something to ensure this doesn't loop forever on bad input
                # TODO: make sure this doesn't loop forever on actual possible input in the unit tests
                for other_eth in Ethnicity:
                    if other_eth in (Ethnicity.MULTIPLE, Ethnicity.NONE):
                        continue

                    if id_gen.random() < eth_demos[other_eth]:
                        ethnicities.add(other_eth)

        else:
            ethnicities = {ethnicity}  # just the one

        # Return the characteristics
        return sex, ethnicity, ethnicities, iep, sec504, lep, ed, migrant

    def _get_federal_ethnicity(self) -> str:
        """
        Generate the federal demographic value for a student.

        @returns: Derived demographic value
        """
        if self.eth_hispanic:
            return Ethnicity.HISPANIC.federal_code

        else:
            demos = {Ethnicity.AMERIND.federal_code: self.eth_amer_ind,
                     Ethnicity.ASIAN.federal_code: self.eth_asian,
                     Ethnicity.BLACK.federal_code: self.eth_black,
                     Ethnicity.WHITE.federal_code: self.eth_white,
                     Ethnicity.HAWAII.federal_code: self.eth_pacific}

            races = set(filter(demos.get, demos))
            if len(races) > 1:
                return Ethnicity.MULTIPLE.federal_code

            elif races:
                return races.pop()

            else:
                return Ethnicity.NONE.federal_code

    def advance_student(self,
                        schools_by_grade,
                        id_gen: IDGen,
                        hold_back_rate=pop_config.HOLD_BACK_RATE,
                        drop_out_rate=pop_config.DROP_OUT_RATE,
                        transfer_rate=pop_config.TRANSFER_RATE) -> bool:
        """
        Take a student and advance them to the next grade. If the next grade takes the student out of the current school,
        pick a new school for them to go to.

        @param id_gen:
        @param schools_by_grade: Potential new schools for a student to be enrolled in
        @param hold_back_rate:
        @param drop_out_rate: The rate (chance) that a student will drop out at if they are not advanced
        @param transfer_rate:
        @returns: True if the student still exists in the system, False if they do not
        """
        # first, mung some of the student's properties so we can test behaviour when these change
        if id_gen.random() < pop_config.DISABLED_STATUS_CHANGE_RATE:
            if self.prg_disability:
                self.prg_disability = False
                self.prg_primary_disability = None

            else:
                self.prg_disability = True
                self.prg_primary_disability = id_gen.choice(pop_config.PRGDisabilityType)

        if id_gen.random() < pop_config.GIFTED_STATUS_CHANGE_RATE:
            self.prg_gifted = not self.prg_gifted

        if self.prg_lep and id_gen.random() < pop_config.LEP_STATUS_CHANGE_RATE:
            self.prg_lep = False

        if id_gen.random() < pop_config.ECON_DIS_STATUS_CHANGE_RATE:
            self.prg_econ_disad = not self.prg_econ_disad

        if id_gen.random() < drop_out_rate:
            # The student is being dropped out, so make them go away
            return False

        # Use the general generator to advance the student
        if id_gen.random() < hold_back_rate:
            # If the student is not being advanced, but is still in a valid grade, return True
            return self.grade in schools_by_grade

        # Bump the grade
        self.grade += 1

        # If the new grade is not available in any school, drop the student
        if self.grade not in schools_by_grade:
            return False

        # If the new grade of the student is not available in the school, pick a new school
        if self.grade not in self.school.grades or id_gen.random() < transfer_rate:
            self.school = self._choose_school(self.grade, schools_by_grade[self.grade], id_gen)

        return True

    def _choose_school(self, grade, schools_in_grade, id_gen: IDGen):
        """
        choose a school for the student based upon the expected sizes, to somewhat mitigate the issue of
        filling small schools w/ students from large schools

        :param grade:
        :param schools_in_grade:
        :param id_gen:
        :return:
        """
        weights = {school: school.expected_grade_size(grade) for school in schools_in_grade}
        return weighted_choice(weights, id_gen)

    @property
    def name(self):
        """The full name of student.
        """
        if self.middle_name is not None:
            return self.first_name + ' ' + self.middle_name + ' ' + self.last_name
        else:
            return self.first_name + ' ' + self.last_name

    @property
    def state(self):
        """ state """
        return self.school.district.state

    @property
    def district(self):
        """ district """
        return self.school.district
