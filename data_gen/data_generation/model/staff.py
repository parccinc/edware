from data_generation.model.model_base import ModelBase
from data_generation.util.id_gen import IDGen


class Staff(ModelBase):
    """
    cases:
    - staff associated w/ all results @ a school
    - staff associated w/ multiple schools
    - multiple staff associated w/ a school
    """

    def __init__(self, id_gen: IDGen):
        super().__init__(id_gen)
        self.staff_id = id_gen.get_uuid()
