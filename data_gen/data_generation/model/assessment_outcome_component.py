import data_generation.config.assessment
from data_generation.config.assessment import Claim, Subclaim, MATH_SUBCLAIMS, ELA_SUBCLAIMS, SUBCLAIMS_BY_SUBJECT, \
    ELA_CLAIMS
from data_generation.config.config import Subject
from data_generation.model.assessment_component import Component, AssessmentComponent
from data_generation.model.assessment_item_outcome import AssessmentItemOutcome
from data_generation.model.model_base import ModelBase
from data_generation.model.school import School, NULL_SCHOOL
from data_generation.model.student import Student
from data_generation.util.assessment_stats import RandomLevelByDemographics, random_score_given_level, adjust_score, \
    random_claims
from data_generation.util.id_gen import IDGen
from data_generation.util.math import scale_value_in_range, bound_range
from data_generation.util.perf_levels import PerfLevels


class AssessmentOutcomeComponent(ModelBase):
    """ assessment outcome component model """

    __slots__ = ('run_id', 'guid', 'guid_sr',
                 'assessment_component',
                 'assessment_outcome',
                 'student',
                 'test_school',

                 'form_id',
                 'student_test_uuid',

                 'raw_score',
                 'claim_raw_score',
                 'subclaim_raw_score',
                 'total_attempted_count',
                 'subclaim_attempt_count',
                 'has_assignment',
                 'not_tested_reason',
                 'attempt_flag',
                 'void_reason',
                 'category',

                 'item_outcomes_by_subclaim',
                 )

    def __init__(self,
                 assessment_component: AssessmentComponent,
                 assessment_outcome,
                 student: Student,
                 test_school: School,
                 level_generator: RandomLevelByDemographics,
                 id_gen: IDGen,
                 component_not_assigned_rate: float=data_generation.config.assessment.COMPONENT_NOT_ASSIGNED_RATE,
                 component_not_tested_rate: float=data_generation.config.assessment.COMPONENT_NOT_TESTED_RATE,
                 min_attempted_percent: float=data_generation.config.assessment.MIN_ATTEMPTED_PERCENT,
                 component_voided_rate: float=data_generation.config.assessment.COMPONENT_VOIDED_RATE,
                 attempted_percent_range: float=data_generation.config.assessment.ATTEMPTED_PERCENT_RANGE):
        super().__init__(id_gen)
        self.claim_raw_score = {}
        self.subclaim_raw_score = {}
        self.subclaim_attempt_count = {}
        self.item_outcomes_by_subclaim = {}

        self.assessment_component = assessment_component
        self.assessment_outcome = assessment_outcome
        self.student = student

        self.has_assignment = id_gen.random() > component_not_assigned_rate

        if self.has_assignment:
            self.test_school = test_school
            if id_gen.random() < component_not_tested_rate:
                self.not_tested_reason = '01'  # in range 00-99, state specific, details not provided

            else:
                self.not_tested_reason = None

        else:
            self.test_school = NULL_SCHOOL
            self.not_tested_reason = None

        if self.has_assignment and self.not_tested_reason is None:
            # compute their scores
            self.form_id = id_gen.choice(assessment_component.form_ids)
            self._generate_scores(level_generator, id_gen, attempted_percent_range=attempted_percent_range)

        else:
            self.form_id = None
            self.raw_score = None
            self.total_attempted_count = None

        if self.total_attempted_count is not None:
            if self.total_attempted_count < min_attempted_percent * assessment_component.total_item_count:
                # attempt count too low, cancel the computed score info
                self.attempt_flag = False
                self.student_test_uuid = None
                self.raw_score = None

            else:
                self.attempt_flag = True
                self.student_test_uuid = id_gen.get_uuid()

        else:
            self.attempt_flag = None
            self.student_test_uuid = None

        if self.attempt_flag:
            if id_gen.random() < component_voided_rate:
                self.void_reason = '01'  # in range 00-99, state specific, details not provided

            else:
                self.void_reason = None

        else:
            self.void_reason = None

        self.category = None  # we can't calculate these until we've got both pba and eoy

    def is_valid(self) -> bool:
        return self.has_assignment and self.not_tested_reason is None and self.attempt_flag and not self.void_reason

    def _generate_scores(self,
                         level_generator: RandomLevelByDemographics,
                         id_gen: IDGen,
                         attempted_percent_range=data_generation.config.assessment.ATTEMPTED_PERCENT_RANGE):
        """

        :param level_generator:
        :param id_gen:
        :return:
        """

        student_demographics = self.student.get_level_demographics()

        base_perf_lvl = int(level_generator.random_level(student_demographics, id_gen))
        base_raw_score = random_score_given_level(self.overall_perf_levels[base_perf_lvl], id_gen)
        adjustment = self.student.school.school_type.adjustment

        self.raw_score = adjust_score(base_raw_score, adjustment, self.overall_perf_levels, id_gen)

        if self.assessment_component.subject == Subject.ELA:
            # random_claims assumes that the various elements have the same scale, which they don't, so we have to fix that later
            unscaled_claim_scores = random_claims(self.raw_score, self.overall_perf_levels, Claim.weights(Subject.ELA), self.overall_perf_levels, id_gen)

            # note that the order here is the same as listed in the claim definition
            unscaled_subclaim_scores_by_claim = {}
            for claim in ELA_CLAIMS:
                self.claim_raw_score[claim] = scale_value_in_range(unscaled_claim_scores[claim], self.overall_perf_levels, self.claim_perf_levels[claim])
                unscaled_subclaim_scores_by_claim[claim] = random_claims(unscaled_claim_scores[claim], self.overall_perf_levels, Subclaim.weights(Subject.ELA, claim), self.overall_perf_levels, id_gen)

            for subclaim in ELA_SUBCLAIMS:
                self.subclaim_raw_score[subclaim] = scale_value_in_range(unscaled_subclaim_scores_by_claim[subclaim.claim][subclaim], self.overall_perf_levels, self.subclaim_perf_levels[subclaim])
                self.subclaim_attempt_count[subclaim] = _get_random_attempt_count(self.assessment_component.subclaim_item_count(subclaim), unscaled_subclaim_scores_by_claim[subclaim.claim][subclaim], self.overall_perf_levels, attempted_percent_range, id_gen)

        else:  # is math
            unscaled_subclaim_scores = random_claims(self.raw_score, self.overall_perf_levels, Subclaim.weights(Subject.Math), self.overall_perf_levels, id_gen)

            for subclaim in MATH_SUBCLAIMS:
                self.subclaim_raw_score[subclaim] = scale_value_in_range(unscaled_subclaim_scores[subclaim], self.overall_perf_levels, self.subclaim_perf_levels[subclaim])
                self.subclaim_attempt_count[subclaim] = _get_random_attempt_count(self.assessment_component.subclaim_item_count(subclaim), unscaled_subclaim_scores[subclaim], self.overall_perf_levels, attempted_percent_range, id_gen)

        self.total_attempted_count = sum(self.subclaim_attempt_count.values())

        for subclaim, subclaim_attempt_count in self.subclaim_attempt_count.items():
            attempts = [True for _ in range(subclaim_attempt_count)] + [False for _ in range(self.assessment_component.subclaim_item_count(subclaim) - subclaim_attempt_count)]
            id_gen.shuffle(attempts)
            items = self.assessment_component.items_by_subclaim[subclaim]

            if len(items) != len(attempts):
                raise AssertionError(items, attempts)

            self.item_outcomes_by_subclaim[subclaim] = tuple(
                AssessmentItemOutcome(assessment_item, self, attempted, id_gen)
                for assessment_item, attempted in zip(items, attempts)
            )

    @property
    def overall_perf_levels(self) -> PerfLevels:
        return self.assessment.overall_perf_levels

    @property
    def claim_perf_levels(self):
        return self.assessment.claim_perf_levels

    @property
    def subclaim_perf_levels(self):
        return self.assessment.subclaim_perf_levels

    @property
    def assessment(self):
        return self.assessment_component.assessment

    @property
    def subject(self) -> Subject:
        return self.assessment.subject

    @property
    def _subclaim(self):
        return SUBCLAIMS_BY_SUBJECT[self.subject]

    @property
    def reading_raw_score(self):
        return self.claim_raw_score.get(Claim.READING)

    @property
    def writing_raw_score(self):
        return self.claim_raw_score.get(Claim.WRITING)

    @property
    def subclaim_1_raw_score(self):
        return self.subclaim_raw_score.get(self._subclaim[0])

    @property
    def subclaim_2_raw_score(self):
        return self.subclaim_raw_score.get(self._subclaim[1])

    @property
    def subclaim_3_raw_score(self):
        return self.subclaim_raw_score.get(self._subclaim[2])

    @property
    def subclaim_4_raw_score(self):
        return self.subclaim_raw_score.get(self._subclaim[3])

    @property
    def subclaim_5_raw_score(self):
        return self.subclaim_raw_score.get(self._subclaim[4])

    @property
    def subclaim_1_attempted_count(self):
        return self.subclaim_attempt_count.get(self._subclaim[0])

    @property
    def subclaim_2_attempted_count(self):
        return self.subclaim_attempt_count.get(self._subclaim[1])

    @property
    def subclaim_3_attempted_count(self):
        return self.subclaim_attempt_count.get(self._subclaim[2])

    @property
    def subclaim_4_attempted_count(self):
        return self.subclaim_attempt_count.get(self._subclaim[3])

    @property
    def subclaim_5_attempted_count(self):
        return self.subclaim_attempt_count.get(self._subclaim[4])


def _get_random_attempt_count(item_count: int,
                              subclaim_raw_score: int,
                              subclaim_range,
                              attempted_percent_range,
                              id_gen: IDGen) -> int:
    subclaim_min = subclaim_range.min
    subclaim_max = subclaim_range.max - 1

    subclaim_perc = (subclaim_raw_score - subclaim_min) / (subclaim_max - subclaim_min)
    absolute_min_attempt = 1 if subclaim_raw_score != 0 else 0
    min_attempt_count = max(absolute_min_attempt,
                            int(round(item_count * subclaim_perc * (1 - attempted_percent_range))))
    max_attempt_count = min(item_count,
                            int(round(item_count * subclaim_perc * (1 + attempted_percent_range))))
    if min_attempt_count >= max_attempt_count:
        return min_attempt_count

    else:
        return id_gen.randint(min_attempt_count, max_attempt_count)


class AssessmentOutcomeSummative(ModelBase):
    """ ... """
    __slots__ = ('run_id', 'guid', 'guid_sr',

                 'assessment',
                 'score_record_uuid',
                 'scale_score',
                 'csem',
                 'perf_level',
                 'reading_csem',
                 'writing_csem',
                 'claim_scale_score',
                 'subclaim_perf_level',
                 'student_growth_state',
                 'student_growth_district',
                 'student_growth_parcc',)

    def __init__(self,
                 compute_summative: bool,
                 assessment,
                 pba: AssessmentOutcomeComponent,
                 eoy: AssessmentOutcomeComponent,
                 is_first_year: bool,
                 id_gen: IDGen):
        super().__init__(id_gen)
        self.claim_scale_score = {}
        self.subclaim_perf_level = {}

        self.assessment = assessment

        # summative score uuid is always required, even for non-summative records (? - but this is in the spec)
        self.score_record_uuid = id_gen.get_uuid()

        if compute_summative:
            self.scale_score = bound_range(int(round(pba.raw_score * Component.PBA.weight +
                                                     eoy.raw_score * Component.EOY.weight)),
                                           self.overall_perf_levels)
            self.perf_level = self.overall_perf_levels.get_level_given_score(self.scale_score)
            self.csem = id_gen.choice(data_generation.config.assessment.GENERIC_CSEM_RANGE)

            if assessment.subject == Subject.ELA:
                self.reading_csem = id_gen.choice(data_generation.config.assessment.GENERIC_CSEM_RANGE)
                self.writing_csem = id_gen.choice(data_generation.config.assessment.GENERIC_CSEM_RANGE)

                for claim in ELA_CLAIMS:
                    self.claim_scale_score[claim] = bound_range(int(round(pba.claim_raw_score[claim] * Component.PBA.weight +
                                                                          eoy.claim_raw_score[claim] * Component.EOY.weight)),
                                                                self.claim_perf_levels[claim])

                for subclaim in ELA_SUBCLAIMS:
                    subclaim_scale_score = bound_range(int(round(pba.subclaim_raw_score[subclaim] * Component.PBA.weight +
                                                                 eoy.subclaim_raw_score[subclaim] * Component.EOY.weight)),
                                                       self.subclaim_perf_levels[subclaim])
                    self.subclaim_perf_level[subclaim] = self.subclaim_perf_levels[subclaim].get_level_given_score(subclaim_scale_score)

            else:
                self.reading_csem = None
                self.writing_csem = None

                for subclaim in MATH_SUBCLAIMS:
                    subclaim_scale_score = bound_range(int(round(pba.subclaim_raw_score[subclaim] * Component.PBA.weight +
                                                                 eoy.subclaim_raw_score[subclaim] * Component.EOY.weight)),
                                                       self.subclaim_perf_levels[subclaim])
                    self.subclaim_perf_level[subclaim] = self.subclaim_perf_levels[subclaim].get_level_given_score(subclaim_scale_score)

            if not is_first_year:
                self.student_growth_state = "%.2f" % (id_gen.uniform(0, 100),)
                self.student_growth_district = "%.2f" % (id_gen.uniform(0, 100),)
                self.student_growth_parcc = "%.2f" % (id_gen.uniform(0, 100),)

            else:
                self.student_growth_state = None
                self.student_growth_district = None
                self.student_growth_parcc = None

        else:
            self.scale_score = None
            self.csem = None
            self.perf_level = None
            self.reading_csem = None
            self.writing_csem = None
            self.student_growth_state = None
            self.student_growth_district = None
            self.student_growth_parcc = None

    @property
    def overall_perf_levels(self) -> PerfLevels:
        return self.assessment.overall_perf_levels

    @property
    def claim_perf_levels(self):
        return self.assessment.claim_perf_levels

    @property
    def subclaim_perf_levels(self):
        return self.assessment.subclaim_perf_levels

    @property
    def subject(self) -> Subject:
        return self.assessment.subject

    @property
    def _subclaim(self):
        return SUBCLAIMS_BY_SUBJECT[self.subject]

    @property
    def reading_scale_score(self):
        return self.claim_scale_score.get(Claim.READING)

    @property
    def writing_scale_score(self):
        return self.claim_scale_score.get(Claim.WRITING)

    @property
    def subclaim_1_perf_level(self):
        return self.subclaim_perf_level.get(self._subclaim[0])

    @property
    def subclaim_2_perf_level(self):
        return self.subclaim_perf_level.get(self._subclaim[1])

    @property
    def subclaim_3_perf_level(self):
        return self.subclaim_perf_level.get(self._subclaim[2])

    @property
    def subclaim_4_perf_level(self):
        return self.subclaim_perf_level.get(self._subclaim[3])

    @property
    def subclaim_5_perf_level(self):
        return self.subclaim_perf_level.get(self._subclaim[4])

    @property
    def subclaim_6_perf_level(self):
        return None
