from data_generation.model.model_base import ModelBase
from data_generation.util.id_gen import IDGen


class AssessmentPerfLevel(ModelBase):
    """
    The PARCC-specific assessment class.
    """

    __slots__ = (
        'assessment_subject',
        'state',
        'perf_level',
    )

    def __init__(self, assessment_subject, state, perf_level, id_gen: IDGen):
        super().__init__(id_gen)
        self.assessment_subject = assessment_subject
        self.state = state
        self.perf_level = perf_level
