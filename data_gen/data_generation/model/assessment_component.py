import data_generation.config.assessment
from data_generation.config.assessment import Component, SUBCLAIMS_BY_SUBJECT
from data_generation.config.config import Subject
from data_generation.model.assessment_item import AssessmentItem
from data_generation.model.model_base import ModelBase
from data_generation.util.id_gen import IDGen
from data_generation.util.integer_partition import random_integer_partition
from data_generation.util.math import largest_remainder_rounding


class AssessmentComponent(ModelBase):
    """ assessment component model """

    __slots__ = ('run_id', 'guid', 'guid_sr',
                 'assessment',
                 'component',
                 'form_ids',

                 'items_by_subclaim',
                 )

    def __init__(self, assessment, component: Component, id_gen: IDGen):
        super().__init__(id_gen)
        self.assessment = assessment
        self.component = component

        # TODO: item counts are actually defined instead of just random numbers, but we're not doing item level data generation yet.
        # Generate Assessment Item Bank
        total_item_count = id_gen.choice(data_generation.config.assessment.ASMT_ITEM_COUNT_RANGE)

        subclaim_counts = [0]
        for _ in range(20):  # some arbitrary value so we don't spend too long looking for something "correct"
            # TODO: code up way to get partitions w/ # of parts and maximum values...
            if any(subclaim_count not in data_generation.config.assessment.ASMT_SUBCLAIM_ITEM_RANGE
                   for subclaim_count in subclaim_counts):
                subclaim_counts = random_integer_partition(total_item_count, len(self._subclaims), id_gen)

            else:
                break

        else:
            # just give up and break it up evenly
            flat_counts = [total_item_count / len(self._subclaims) for _ in range(len(self._subclaims))]
            subclaim_counts = largest_remainder_rounding(flat_counts, total_item_count)

        id_gen.shuffle(subclaim_counts)

        self.items_by_subclaim = {
            subclaim: tuple(
                AssessmentItem(self, subclaim, item_number, id_gen) for item_number in range(1, subclaim_count + 1)
            )
            for subclaim, subclaim_count in zip(self._subclaims, subclaim_counts)
        }

        self.form_ids = tuple(id_gen.get_form_id() for _ in range(
            data_generation.config.assessment.FORMS_PER_ASSESSMENT_COMPONENT))
        # there's also a form type mentioned in the current item level data, but ignore this for now

    @property
    def subject(self) -> Subject:
        """ subject """
        return self.assessment.subject

    def subclaim_item_count(self, subclaim):
        return len(self.items_by_subclaim[subclaim])

    @property
    def _subclaims(self):
        return SUBCLAIMS_BY_SUBJECT[self.subject]

    @property
    def total_item_count(self):
        return sum(len(items) for items in self.items_by_subclaim.values())

    @property
    def subclaim_1_item_count(self):
        return len(self.items_by_subclaim[self._subclaims[0]])

    @property
    def subclaim_2_item_count(self):
        return len(self.items_by_subclaim[self._subclaims[1]])

    @property
    def subclaim_3_item_count(self):
        return len(self.items_by_subclaim[self._subclaims[2]])

    @property
    def subclaim_4_item_count(self):
        return len(self.items_by_subclaim[self._subclaims[3]])

    @property
    def subclaim_5_item_count(self):
        return len(self.items_by_subclaim[self._subclaims[4]])
