from data_generation.model.assessment_item import AssessmentItem
from data_generation.model.assessment_item_outcome import AssessmentItemOutcome
from data_generation.model.state import State
from data_generation.util.abstract_output_formats import RecordFormat
from data_generation.model.assessment import Assessment
from data_generation.model.assessment_outcome import AssessmentOutcome
from data_generation.model.district import District
from data_generation.util.perf_levels import PerfLevel


class WriterBase:
    __slots__ = (
        '_formats',
    )

    def __init__(self,
                 formats: dict):
        self._formats = formats

    def state_initialize(self, state: State):
        raise NotImplementedError()

    def district_initialize(self, district: District):
        raise NotImplementedError()

    def state_finalize(self, state: State):
        raise NotImplementedError()

    def district_finalize(self, district: District):
        raise NotImplementedError()

    def write_assessments(self, assessments):
        raise NotImplementedError()

    def write_assessment_outcomes(self, assessment_outcomes):
        raise NotImplementedError()

    def write_assessment_items(self, assessment_items):
        raise NotImplementedError()

    def write_assessment_item_outcomes(self, assessment_item_outcomes):
        raise NotImplementedError()

    def write_perf_levels(self, assessment_perf_levels):
        raise NotImplementedError()

    @property
    def _assessment_format(self) -> RecordFormat:
        return self._formats.get(Assessment)

    @property
    def _assessment_outcome_format(self) -> RecordFormat:
        return self._formats.get(AssessmentOutcome)

    @property
    def _perf_level_format(self) -> RecordFormat:
        return self._formats.get(PerfLevel)

    @property
    def _assessment_item_format(self) -> RecordFormat:
        return self._formats.get(AssessmentItem)

    @property
    def _assessment_item_outcome_format(self) -> RecordFormat:
        return self._formats.get(AssessmentItemOutcome)
