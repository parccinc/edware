"""
Create output filters for data values that are specific to the PARCC project.

@author: nestep
@date: February 28, 2014
"""
from data_generation.config.population import Sex, Grade


def date_y_m_d(val):
    return str(val)


def yn(val):
    return 'Y' if val else 'N'


def yblank(val):
    return 'Y' if val else None


def ynblank(val):
    return None if val is None else 'Y' if val else 'N'


def zero_padded_grade(val: Grade):
    """
    Zero-pad a grade value so that it is always a two digit string.

    @param val: The value to pad
    @returns: The value as a two-digit string
    """
    if val is None:
        return None
    return '%02d' % val.code


def sex(val):
    """

    :param val:
    :return:
    """
    if val is None or val == Sex.OTHER:
        return None

    elif val == Sex.FEMALE:
        return 'F'

    else:
        return 'M'


def enum_name(val):
    if val:
        return val.name

    else:
        return None


def enum_value(val):
    if val:
        return val.value

    else:
        return None


def enum_label(val):
    if val:
        return val.label

    else:
        return None


def enum_id(val):
    if val:
        return val.id

    else:
        return None
