from pathlib import Path
import shutil
import re
from data_generation.model.assessment import Assessment
from data_generation.model.assessment_item import AssessmentItem
from data_generation.model.assessment_item_outcome import AssessmentItemOutcome
from data_generation.model.assessment_outcome import AssessmentOutcome
from data_generation.model.district import District
from data_generation.model.state import State
from data_generation.util.csv import DictWriterWithFileHandle
from data_generation.writers.writer_base import WriterBase
import csv
import logging
from collections import defaultdict


PER_DISTRICT = 'districts'


def _get_dialect(csv_delimiter: str=',',
                 csv_quoting=csv.QUOTE_MINIMAL,
                 csv_lineterminator='\n') -> type:
    """
    create a python csv dialect that looks like what we want.

    the way you define these things is insane. wtf.
    # TODO: is this going to make nose have a hissy?
    """
    class CSVWriterDialect(csv.unix_dialect):
        delimiter = csv_delimiter
        quoting = csv_quoting
        lineterminator = csv_lineterminator

    return CSVWriterDialect


class CSVWriter(WriterBase):
    """
    if per-state:
        end up w/ files:
            OUT/CSV/STATE_ID/rpt_ela_sum.csv
            OUT/CSV/STATE_ID/rpt_math_sum.csv
            OUT/CSV/STATE_ID/rpt_sum_test_level_m.csv
            OUT/CSV/STATE_ID/rpt_sum_test_score_m.csv

    if per-district and not consolidate:
        end up w/ files:
            OUT/CSV/STATE_ID/DIST_ID/rpt_ela_sum.csv
            OUT/CSV/STATE_ID/DIST_ID/rpt_math_sum.csv
            OUT/CSV/STATE_ID/DIST_ID/rpt_sum_test_level_m.csv
            OUT/CSV/STATE_ID/DIST_ID/rpt_sum_test_score_m.csv

    if consolidate:
        end up w/ files:
            OUT/CSV/STATE_ID/rpt_ela_sum.csv
            OUT/CSV/STATE_ID/rpt_math_sum.csv
            OUT/CSV/STATE_ID/rpt_sum_test_level_m.csv
            OUT/CSV/STATE_ID/rpt_sum_test_score_m.csv

        intermediate files, before consolidation:
            OUT/CSV/STATE_ID/DIST_ID/rpt_ela_sum.csv
            OUT/CSV/STATE_ID/DIST_ID/rpt_math_sum.csv

            w/out headers!
    """

    __slots__ = (
        '_formats',
        '_base_path',
        '_per_district',
        '_consolidate',
        '_csv_dialect',
        '_csv_suffix',
        '_assessment_writers_by_path',
        '_assessment_outcome_writers_by_path',
        '_assessment_item_writers_by_path',
        '_assessment_item_outcome_writers_by_path',
        '_perf_level_writers_by_path',
    )

    def __init__(self,
                 formats: dict,
                 out_path: Path,
                 per_district: bool=False,
                 consolidate: bool=False,
                 csv_delimiter: str=',',
                 csv_quoting=csv.QUOTE_MINIMAL,
                 csv_lineterminator='\n',
                 csv_suffix='.csv',
                 _writer_path_prefix='CSV',
                 **kwargs):
        """
        If we're running in multithread mode, we break up work chunks by district.
        thus in that case, per_district must be set to True. If we want a single file at the end of the day, we
        also need to specify "consolidate".

        :param base_path: base output file path
        :param formats: dictionary of output formats to use
        :param per_district: if True, create output files per-district instead of per-state
        :param consolidate: if True, consolidate per-district files (per_district MUST be True if this is True)
        :param csv_delimiter: in case we need to configure CSV delimiters for some reason (default should be fine)
        :param csv_quoting: in case we need to configure CSV quoting for some reason (default should be fine)
        :param _writer_path_prefix: intended for internal use, for LZ writer
        :return:
        """
        super().__init__(formats)
        if consolidate and not per_district:
            raise ValueError("cannot consolidate if files not generated per-district")

        self._base_path = out_path / _writer_path_prefix
        self._per_district = per_district

        self._consolidate = consolidate
        self._csv_dialect = _get_dialect(csv_delimiter, csv_quoting, csv_lineterminator)
        self._csv_suffix = csv_suffix

        self._assessment_writers_by_path = None
        self._assessment_outcome_writers_by_path = None
        self._assessment_item_writers_by_path = None
        self._assessment_item_outcome_writers_by_path = None
        self._perf_level_writers_by_path = None

    def state_initialize(self, state: State):
        if not self._base_path.exists():
            self._base_path.mkdir(parents=True)

        if any(col is not None for col in
               (self._assessment_writers_by_path,
                self._perf_level_writers_by_path,
                self._assessment_item_writers_by_path)):
            raise Exception("already initialized? %s %s %s" %
                            (self._assessment_writers_by_path,
                             self._perf_level_writers_by_path,
                             self._assessment_item_writers_by_path))

        self._assessment_writers_by_path = {}
        self._perf_level_writers_by_path = {}
        self._assessment_item_writers_by_path = {}

        if not self._per_district:
            if self._assessment_outcome_writers_by_path is not None or self._assessment_item_outcome_writers_by_path is not None:
                raise Exception("district already initialized?")

            self._assessment_outcome_writers_by_path = {}
            self._assessment_item_outcome_writers_by_path = {}

    def state_finalize(self, state: State):
        logging.debug("finalizing state %s" % (state.code,))
        self._close_main_fhs()

        if not self._per_district:
            self._close_district_fhs()

        if self._consolidate:
            base_path = self._base_path / state.code
            self._do_consolidate(base_path)

    def district_initialize(self, district: District):
        if self._per_district:
            if self._assessment_outcome_writers_by_path is not None or self._assessment_item_outcome_writers_by_path is not None:
                raise Exception("district already initialized?")

            self._assessment_outcome_writers_by_path = {}
            self._assessment_item_outcome_writers_by_path = {}

    def district_finalize(self, district: District):
        if self._per_district:
            self._close_district_fhs()

    def _assessment_base_path(self, assessment: Assessment) -> Path:
        return self._base_path / assessment.state.code

    def _assessment_outcome_base_path(self, assessment_outcome: AssessmentOutcome) -> Path:
        if self._per_district:
            return self._base_path / assessment_outcome.state.code / PER_DISTRICT / assessment_outcome.responsible_district.identifier

        else:
            return self._base_path / assessment_outcome.state.code

    def _assessment_item_base_path(self, assessment_item: AssessmentItem) -> Path:
        return self._assessment_base_path(assessment_item.assessment)

    def _assessment_item_outcome_base_path(self, assessment_item_outcome: AssessmentItemOutcome) -> Path:
        return self._assessment_outcome_base_path(assessment_item_outcome.assessment_outcome)

    def _get_files_to_consolidate(self, base_path: Path, name_format: str):
        # replace "format" named params w/ wildcards
        ao_name_matcher = re.compile(re.sub('{[^{}]+}', '[^_]+', name_format) + '\\' + self._csv_suffix + '$')

        for district_path in base_path.iterdir():
            if not district_path.is_dir():
                continue

            for file in district_path.rglob("*%s" % (self._csv_suffix,)):
                filename = file.relative_to(district_path)

                # fullmatch in 3.4...
                if ao_name_matcher.search(str(filename)):
                    yield filename, file

    def _do_consolidate(self, base_path: Path, _unlink_district_data: bool=True):
        """
        looks for CSV files in subdirs of the given base_path, and concatenates them into a single file in the
        base_path.

        :param _unlink_district_data: Should be "True". Used for debugging.
        """
        district_base_path = base_path / PER_DISTRICT
        logging.debug("Consolidating %s" % (district_base_path,))

        dist_csv_paths_to_unlink = []

        if self._assessment_outcome_format:
            district_csv_paths_by_filename = defaultdict(list)

            for filename, csv_path in self._get_files_to_consolidate(district_base_path, self._assessment_outcome_format.name_format):
                district_csv_paths_by_filename[filename].append(csv_path)

            field_names = self._assessment_outcome_format.get_columns()

            for filename, district_csv_paths in district_csv_paths_by_filename.items():
                dest_path = base_path / filename

                with dest_path.open('x') as dest_fh:
                    dest_writer = DictWriterWithFileHandle(dest_fh, field_names, dialect=self._csv_dialect)
                    dest_writer.writeheader()

                    for district_csv_path in district_csv_paths:
                        with district_csv_path.open('r') as district_fh:
                            logging.debug("consolidating %s into %s" % (district_csv_path, dest_path))
                            shutil.copyfileobj(district_fh, dest_fh)

                        dist_csv_paths_to_unlink.append(district_csv_path)

        if self._assessment_item_outcome_format:
            district_csv_paths_by_filename = defaultdict(list)

            for filename, csv_path in self._get_files_to_consolidate(district_base_path, self._assessment_item_outcome_format.name_format):
                district_csv_paths_by_filename[filename].append(csv_path)

            field_names = self._assessment_item_outcome_format.get_columns()

            for filename, district_csv_paths in district_csv_paths_by_filename.items():
                dest_path = base_path / filename

                with dest_path.open('x') as dest_fh:
                    dest_writer = DictWriterWithFileHandle(dest_fh, field_names, dialect=self._csv_dialect)
                    dest_writer.writeheader()

                    for district_csv_path in district_csv_paths:
                        with district_csv_path.open('r') as district_fh:
                            logging.debug("consolidating %s into %s" % (district_csv_path, dest_path))
                            shutil.copyfileobj(district_fh, dest_fh)

                        dist_csv_paths_to_unlink.append(district_csv_path)

        # unlink directories
        if _unlink_district_data:
            for dist_csv_path in dist_csv_paths_to_unlink:
                if dist_csv_path.exists():
                    dist_csv_path.unlink()

            # TODO: rmtree if the whole thing is empty, otherwise print a warning
            #       currently can't just delete it due to possible multiple instances of CSVWriter running at the same time
            # shutil.rmtree(str(district_base_path))

    def _get_assessment_outcome_writer(self, assessment_outcome: AssessmentOutcome):
        filename = self._assessment_outcome_format.name_format.format(subject=assessment_outcome.subject.value.lower())

        path = (self._assessment_outcome_base_path(assessment_outcome) /
                filename).with_suffix(self._csv_suffix)

        if not path.parent.exists():
            path.parent.mkdir(parents=True)

        if not path.exists():
            field_names = self._assessment_outcome_format.get_columns()

            csv_fh = path.open('x+')  # fail if exists; allow seeking & reading (for consolidation)
            writer = DictWriterWithFileHandle(csv_fh, field_names, dialect=self._csv_dialect)
            self._assessment_outcome_writers_by_path[path] = writer

            # we don't want to write headers if we're consolidating the files later.
            if not self._consolidate:
                writer.writeheader()

        else:
            writer = self._assessment_outcome_writers_by_path[path]

        return writer

    def _get_assessment_writer(self, assessment: Assessment):
        filename = self._assessment_format.name_format

        path = (self._assessment_base_path(assessment) /
                filename).with_suffix(self._csv_suffix)

        if not path.parent.exists():
            path.parent.mkdir(parents=True)

        if not path.exists():
            field_names = self._assessment_format.get_columns()

            csv_fh = path.open('x+')  # fail if exists; allow seeking & reading (for consolidation)
            writer = DictWriterWithFileHandle(csv_fh, field_names, dialect=self._csv_dialect)
            self._assessment_writers_by_path[path] = writer

            writer.writeheader()

        else:
            writer = self._assessment_writers_by_path[path]

        return writer

    def _get_perf_level_writer(self, assessment_perf_level):
        filename = self._perf_level_format.name_format

        path = (self._assessment_base_path(assessment_perf_level) /
                filename).with_suffix(self._csv_suffix)

        if not path.parent.exists():
            path.parent.mkdir(parents=True)

        if not path.exists():
            field_names = self._perf_level_format.get_columns()

            csv_fh = path.open('x+')  # fail if exists; allow seeking & reading (for consolidation)
            writer = DictWriterWithFileHandle(csv_fh, field_names, dialect=self._csv_dialect)
            self._perf_level_writers_by_path[path] = writer

            writer.writeheader()

        else:
            writer = self._perf_level_writers_by_path[path]

        return writer

    def write_assessments(self, assessments):
        """
        this needs to write two CSVs:
            rpt_sum_test_score_m
                - one row per assessment
            rpt_sum_test_level_m
                - one row per assessment per level of assessment
        """
        if not self._assessment_format:
            return

        score_rows_by_writer = defaultdict(list)
        for assessment in assessments:
            score_row = self._assessment_format.get_row(assessment)
            score_writer = self._get_assessment_writer(assessment)

            score_rows_by_writer[score_writer].append(score_row)

        for score_writer, score_rows in score_rows_by_writer.items():
            score_writer.writerows(score_rows)

    def write_perf_levels(self, assessment_perf_levels):
        if not self._perf_level_format:
            return

        level_rows_by_writer = defaultdict(list)

        for assessment_perf_level in assessment_perf_levels:
            level_writer = self._get_perf_level_writer(assessment_perf_level)
            level_row = self._perf_level_format.get_row(assessment_perf_level)

            level_rows_by_writer[level_writer].append(level_row)

        for level_writer, level_rows in level_rows_by_writer.items():
            level_writer.writerows(level_rows)

    def write_assessment_outcomes(self, assessment_outcomes):
        if not self._assessment_outcome_format:
            return

        rows_by_writer = defaultdict(list)

        for assessment_outcome in assessment_outcomes:
            writer = self._get_assessment_outcome_writer(assessment_outcome)
            row = self._assessment_outcome_format.get_row(assessment_outcome)

            rows_by_writer[writer].append(row)

        for writer, rows in rows_by_writer.items():
            writer.writerows(rows)

    def _close_district_fhs(self):
        logging.debug("closing district CSV FHs...")

        if self._assessment_outcome_writers_by_path is not None:
            for path, writer in self._assessment_outcome_writers_by_path.items():
                logging.debug("    closing %s" % (path,))
                writer.fh.close()

            self._assessment_outcome_writers_by_path = None

        if self._assessment_item_outcome_writers_by_path is not None:
            for path, writer in self._assessment_item_outcome_writers_by_path.items():
                logging.debug("    closing %s" % (path,))
                writer.fh.close()

            self._assessment_item_outcome_writers_by_path = None

        logging.debug("closing district CSV FHs actually worked...")

    def _close_main_fhs(self):
        logging.debug("closing CSV FHs...")

        if self._assessment_writers_by_path is not None:
            for path, writer in self._assessment_writers_by_path.items():
                logging.debug("    closing %s" % (path,))
                writer.fh.close()

            self._assessment_writers_by_path = None

        if self._assessment_item_writers_by_path is not None:
            for path, writer in self._assessment_item_writers_by_path.items():
                logging.debug("    closing %s" % (path,))
                writer.fh.close()

            self._assessment_item_writers_by_path = None

        if self._perf_level_writers_by_path is not None:
            for path, writer in self._perf_level_writers_by_path.items():
                logging.debug("    closing %s" % (path,))
                writer.fh.close()

            self._perf_level_writers_by_path = None

        logging.debug("closing CSV FHs actually worked...")

    def write_assessment_items(self, assessment_items):
        if not self._assessment_item_format:
            return

        rows_by_writer = defaultdict(list)

        for assessment_item in assessment_items:
            writer = self._get_assessment_item_writer(assessment_item)
            row = self._assessment_item_format.get_row(assessment_item)

            rows_by_writer[writer].append(row)

        for writer, rows in rows_by_writer.items():
            writer.writerows(rows)

    def _get_assessment_item_writer(self, assessment_item: AssessmentItem):
        filename = self._assessment_item_format.name_format

        path = (self._assessment_item_base_path(assessment_item) /
                filename).with_suffix(self._csv_suffix)

        if not path.parent.exists():
            path.parent.mkdir(parents=True)

        if not path.exists():
            field_names = self._assessment_item_format.get_columns()

            csv_fh = path.open('x+')  # fail if exists; allow seeking & reading (for consolidation)
            writer = DictWriterWithFileHandle(csv_fh, field_names, dialect=self._csv_dialect)
            self._assessment_item_writers_by_path[path] = writer

            writer.writeheader()

        else:
            writer = self._assessment_item_writers_by_path[path]

        return writer

    def write_assessment_item_outcomes(self, assessment_item_outcomes):
        if not self._assessment_item_outcome_format:
            return

        rows_by_writer = defaultdict(list)

        for assessment_item_outcome in assessment_item_outcomes:
            writer = self._get_assessment_item_outcome_writer(assessment_item_outcome)
            row = self._assessment_item_outcome_format.get_row(assessment_item_outcome)

            rows_by_writer[writer].append(row)

        for writer, rows in rows_by_writer.items():
            writer.writerows(rows)

    def _get_assessment_item_outcome_writer(self, assessment_item_outcome: AssessmentItemOutcome):
        filename = self._assessment_item_outcome_format.name_format.format(subject=assessment_item_outcome.assessment_outcome.subject.value.lower())

        path = (self._assessment_item_outcome_base_path(assessment_item_outcome) /
                filename).with_suffix(self._csv_suffix)

        if not path.parent.exists():
            path.parent.mkdir(parents=True)

        if not path.exists():
            field_names = self._assessment_item_outcome_format.get_columns()

            csv_fh = path.open('x+')  # fail if exists; allow seeking & reading (for consolidation)
            writer = DictWriterWithFileHandle(csv_fh, field_names, dialect=self._csv_dialect)
            self._assessment_item_outcome_writers_by_path[path] = writer

            # we don't want to write headers if we're consolidating the files later.
            if not self._consolidate:
                writer.writeheader()

        else:
            writer = self._assessment_item_outcome_writers_by_path[path]

        return writer

    def __del__(self):
        try:
            self._close_district_fhs()
            self._close_main_fhs()

        except Exception as e:
            logging.exception(e)
            raise
