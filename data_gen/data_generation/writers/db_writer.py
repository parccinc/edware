from collections import defaultdict

from sqlalchemy.engine import create_engine
from sqlalchemy.engine.url import URL
from sqlalchemy.sql.schema import MetaData
from data_generation.model.assessment import Assessment
from data_generation.model.assessment_item import AssessmentItem
from data_generation.model.assessment_item_outcome import AssessmentItemOutcome

from data_generation.model.assessment_outcome import AssessmentOutcome
from data_generation.model.district import District
from data_generation.model.state import State
from data_generation.util.exceptions import DataGenDBError
from data_generation.writers.writer_base import WriterBase


class DBWriter(WriterBase):
    def __init__(self,
                 formats: dict,
                 db_connector,
                 db_username,
                 db_password,
                 db_host,
                 db_port,
                 db_database,
                 db_schema,
                 db_use_transactions: bool=False,
                 per_district: bool=False,
                 **kwargs):
        super().__init__(formats)
        self._use_transactions = db_use_transactions
        self._per_district = per_district

        self._engine = None
        self._metadata = None

        self._db_connect_string = URL(drivername=db_connector,
                                      username=db_username,
                                      password=db_password,
                                      host=db_host,
                                      port=db_port,
                                      database=db_database)
        self._schema = db_schema

        self._tables = {}

    def _get_connection(self):
        if self._use_transactions:
            return self._engine.begin()

        else:
            return self._engine.connect()

    def _initialize(self):
        if self._engine is not None:
            raise Exception("Already initialized?")

        self._engine = create_engine(self._db_connect_string)
        self._metadata = MetaData(self._engine, schema=self._schema)
        self._metadata.reflect()

    def _finalize(self):
        self._tables = None
        self._engine = None
        self._metadata = None

    def state_initialize(self, state: State):
        self._initialize()

    def state_finalize(self, state: State):
        self._finalize()

    def district_initialize(self, district: District):
        if self._per_district and self._engine is None:
            self._initialize()

    def district_finalize(self, district: District):
        if self._per_district:
            self._finalize()

    def _get_assessment_table(self, assessment: Assessment):
        table_name = self._assessment_format.name_format
        full_table_name = "%s.%s" % (self._schema, table_name)

        if full_table_name in self._tables:
            return self._tables[full_table_name]

        if full_table_name not in self._metadata.tables:
            raise DataGenDBError("table %s not in schema %s. please ensure DB is initialized with the correct schema before generating data. (existing tables: %s)"
                                 % (table_name, self._schema, sorted(self._metadata.tables)))

        self._tables[full_table_name] = table = self._metadata.tables[full_table_name]

        return table

    def _get_assessment_outcome_table(self, assessment_outcome: AssessmentOutcome):
        table_name = self._assessment_outcome_format.name_format.format(subject=assessment_outcome.subject.name.lower())
        full_table_name = "%s.%s" % (self._schema, table_name)

        if full_table_name in self._tables:
            return self._tables[full_table_name]

        if full_table_name not in self._metadata.tables:
            raise DataGenDBError("table %s not in schema %s. please ensure DB is initialized with the correct schema before generating data. (existing tables: %s)"
                                 % (table_name, self._schema, sorted(self._metadata.tables)))
        table = self._metadata.tables[full_table_name]
        self._tables[full_table_name] = table

        return table

    def _get_assessment_item_table(self, assessment_item: AssessmentItem):
        table_name = self._assessment_item_format.name_format
        full_table_name = "%s.%s" % (self._schema, table_name)

        if full_table_name in self._tables:
            return self._tables[full_table_name]

        if full_table_name not in self._metadata.tables:
            raise DataGenDBError("table %s not in schema %s. please ensure DB is initialized with the correct schema before generating data. (existing tables: %s)"
                                 % (table_name, self._schema, sorted(self._metadata.tables)))

        self._tables[full_table_name] = table = self._metadata.tables[full_table_name]

        return table

    def _get_assessment_item_outcome_table(self, assessment_item_outcome: AssessmentItemOutcome):
        table_name = self._assessment_item_outcome_format.name_format.format(subject=assessment_item_outcome.assessment_outcome.subject.name.lower())
        full_table_name = "%s.%s" % (self._schema, table_name)

        if full_table_name in self._tables:
            return self._tables[full_table_name]

        if full_table_name not in self._metadata.tables:
            raise DataGenDBError("table %s not in schema %s. please ensure DB is initialized with the correct schema before generating data. (existing tables: %s)"
                                 % (table_name, self._schema, sorted(self._metadata.tables)))
        table = self._metadata.tables[full_table_name]
        self._tables[full_table_name] = table

        return table

    def _get_perf_level_table(self, assessment: Assessment):
        table_name = self._perf_level_format.name_format
        full_table_name = "%s.%s" % (self._schema, table_name)

        if full_table_name in self._tables:
            return self._tables[full_table_name]

        if full_table_name not in self._metadata.tables:
            raise DataGenDBError("table %s not in schema %s. please ensure DB is initialized with the correct schema before generating data. (existing tables: %s)"
                                 % (table_name, self._schema, sorted(self._metadata.tables)))

        self._tables[full_table_name] = table = self._metadata.tables[full_table_name]

        return table

    def write_assessments(self, assessments):
        if not self._assessment_format:
            return

        rows_by_table = defaultdict(list)

        for assessment in assessments:
            row = self._assessment_format.get_row(assessment)
            table = self._get_assessment_table(assessment)

            missing_columns = [column for column in row if column not in table.columns]

            # we do this here because either SQLAlchemy doesn't check or the error message is impossible to read.
            if missing_columns:
                raise DataGenDBError("generated columns %s not in table %s" % (missing_columns, table.name))

            rows_by_table[table].append(row)

        with self._get_connection() as connection:
            for table, rows in rows_by_table.items():
                connection.execute(table.insert().values(rows))

    def write_perf_levels(self, assessment_perf_levels):
        if not self._perf_level_format:
            return

        rows_by_table = defaultdict(list)

        for perf_level in assessment_perf_levels:
            row = self._perf_level_format.get_row(perf_level)
            table = self._get_perf_level_table(perf_level)

            missing_columns = [column for column in row if column not in table.columns]

            # we do this here because either SQLAlchemy doesn't check or the error message is impossible to read.
            if missing_columns:
                raise DataGenDBError("generated columns %s not in table %s" % (missing_columns, table.name))

            rows_by_table[table].append(row)

        with self._get_connection() as connection:
            for table, rows in rows_by_table.items():
                connection.execute(table.insert().values(rows))

    def write_assessment_outcomes(self, assessment_outcomes):
        if not self._assessment_outcome_format:
            return

        rows_by_table = defaultdict(list)

        for assessment_outcome in assessment_outcomes:
            row = self._assessment_outcome_format.get_row(assessment_outcome)
            table = self._get_assessment_outcome_table(assessment_outcome)

            missing_columns = [column for column in row if column not in table.columns]

            # we do this here because either SQLAlchemy doesn't check or the error message is impossible to read.
            if missing_columns:
                raise DataGenDBError("generated columns %s not in table %s" % (missing_columns, table.name))

            rows_by_table[table].append(row)

        with self._get_connection() as connection:
            for table, rows in rows_by_table.items():
                connection.execute(table.insert().values(rows))

    def write_assessment_items(self, assessment_items):
        if not self._assessment_item_format:
            return

        rows_by_table = defaultdict(list)

        for assessment_item in assessment_items:
            row = self._assessment_item_format.get_row(assessment_item)
            table = self._get_assessment_item_table(assessment_item)

            missing_columns = [column for column in row if column not in table.columns]

            # we do this here because either SQLAlchemy doesn't check or the error message is impossible to read.
            if missing_columns:
                raise DataGenDBError("generated columns %s not in table %s" % (missing_columns, table.name))

            rows_by_table[table].append(row)

        with self._get_connection() as connection:
            for table, rows in rows_by_table.items():
                connection.execute(table.insert().values(rows))

    def write_assessment_item_outcomes(self, assessment_item_outcomes):
        if not self._assessment_item_outcome_format:
            return

        rows_by_table = defaultdict(list)

        for assessment_item_outcome in assessment_item_outcomes:
            row = self._assessment_item_outcome_format.get_row(assessment_item_outcome)
            table = self._get_assessment_item_outcome_table(assessment_item_outcome)

            missing_columns = [column for column in row if column not in table.columns]

            # we do this here because either SQLAlchemy doesn't check or the error message is impossible to read.
            if missing_columns:
                raise DataGenDBError("generated columns %s not in table %s" % (missing_columns, table.name))

            rows_by_table[table].append(row)

        with self._get_connection() as connection:
            for table, rows in rows_by_table.items():
                connection.execute(table.insert().values(rows))

    def __del__(self):
        self._finalize()
