import logging
from data_generation.model.district import District
from data_generation.model.state import State
from data_generation.writers.writer_base import WriterBase


class MultiWriter(WriterBase):
    def __init__(self, writer_instances):
        super().__init__({})
        self._writer_instances = writer_instances

    def state_initialize(self, state: State):
        for writer_instance in self._writer_instances:
            writer_instance.state_initialize(state)

    def district_initialize(self, district: District):
        for writer_instance in self._writer_instances:
            writer_instance.district_initialize(district)

    def state_finalize(self, state: State):
        for writer_instance in self._writer_instances:
            writer_instance.state_finalize(state)

    def district_finalize(self, district: District):
        for writer_instance in self._writer_instances:
            writer_instance.district_finalize(district)

    def write_assessments(self, assessments):
        for writer_instance in self._writer_instances:
            writer_instance.write_assessments(assessments)

    def write_assessment_outcomes(self, assessment_outcomes):
        logging.debug("      writing %s assessment outcomes..." % (len(assessment_outcomes),))
        for writer_instance in self._writer_instances:
            writer_instance.write_assessment_outcomes(assessment_outcomes)

    def write_assessment_items(self, assessment_items):
        for writer_instance in self._writer_instances:
            writer_instance.write_assessment_items(assessment_items)

    def write_assessment_item_outcomes(self, assessment_item_outcomes):
        for writer_instance in self._writer_instances:
            writer_instance.write_assessment_item_outcomes(assessment_item_outcomes)

    def write_perf_levels(self, assessment_perf_levels):
        for writer_instance in self._writer_instances:
            writer_instance.write_perf_levels(assessment_perf_levels)
