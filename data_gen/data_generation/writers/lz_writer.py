import csv
import json
import logging
import tarfile
import gnupg
import jsonschema
from pathlib import Path
from data_generation.model.assessment import Assessment
from data_generation.model.assessment_outcome import AssessmentOutcome
from data_generation.model.district import District
from data_generation.model.state import State
from data_generation.writers.csv_writer import CSVWriter


class LZWriter(CSVWriter):
    """
    if per-state:
        for each assessment ASMT:
            end up w/ files:
                OUT/CSV/STATE_ID/ASMT_ID.tar.gz.gpg

    if per-district:
        for each assessment ASMT:
            end up w/ files:
                OUT/CSV/STATE_ID/DIST_ID/ASMT_ID.tar.gz.gpg
    """

    __slots__ = (
        '_formats',
        '_base_path',
        '_per_district',
        '_consolidate',
        '_csv_dialect',
        '_csv_suffix',
        '_assessment_outcome_writers_by_path',
        '_assessment_writers_by_path',
        '_json_schema',
        '_json_indent',
        '_json_sort_keys',
        '_json_suffix',
        '_no_gpg',
        '_gpg_home_dir',
        '_gpg_recipient',
        '_gpg_signature_key',
        '_gpg_passphrase',
        '_gpg',
    )

    def __init__(self,
                 formats: dict,
                 out_path: Path,
                 json_schema_path: Path,
                 gpg_home_dir: Path,
                 gpg_recipient: str,
                 gpg_signature_key: str,
                 gpg_passphrase: str,
                 json_indent: int=4,
                 json_sort_keys: bool=True,
                 json_suffix: str='.json',

                 per_district: bool=False,
                 consolidate: bool=False,
                 csv_delimiter: str=',',
                 csv_quoting=csv.QUOTE_MINIMAL,
                 csv_lineterminator='\n',
                 csv_suffix='.csv',

                 no_gpg: bool=False,

                 **kwargs):
        super().__init__(formats,
                         out_path,
                         per_district=per_district,
                         consolidate=consolidate,
                         csv_delimiter=csv_delimiter,
                         csv_quoting=csv_quoting,
                         csv_lineterminator=csv_lineterminator,
                         csv_suffix=csv_suffix,
                         _writer_path_prefix='LZ')

        self._json_schema = self._load_json_schema(json_schema_path)
        self._json_indent = json_indent
        self._json_sort_keys = json_sort_keys
        self._json_suffix = json_suffix

        self._no_gpg = no_gpg
        self._gpg_home_dir = gpg_home_dir
        self._gpg_recipient = gpg_recipient
        self._gpg_signature_key = gpg_signature_key
        self._gpg_passphrase = gpg_passphrase

        if no_gpg:
            self._gpg = None

        else:
            self._gpg = gnupg.GPG(gnupghome=str(gpg_home_dir), verbose=False)

    @staticmethod
    def _load_json_schema(json_schema_path: Path):
        with json_schema_path.open() as fh:
            return json.load(fh)

    def _assessment_outcome_base_path(self, assessment_outcome: AssessmentOutcome) -> Path:
        return super()._assessment_outcome_base_path(assessment_outcome) / assessment_outcome.assessment.key

    def _assessment_base_path(self, assessment: Assessment) -> Path:
        return super()._assessment_base_path(assessment) / assessment.key

    def state_finalize(self, state: State):
        super().state_finalize(state)
        if not self._no_gpg:
            for subpath in (self._base_path / state.code).iterdir():
                self._create_gpg(subpath)

    def _create_gpg(self, base_path: Path, _unlink_unencrypted_files: bool=True):
        tar_file_path = base_path.with_suffix('.tar.gz')
        json_file_path = (base_path / self._assessment_format.name_format).with_suffix(self._json_suffix)
        data_file_path = (base_path / self._assessment_outcome_format.name_format).with_suffix(self._csv_suffix)

        if not data_file_path.exists():
            logging.info("%s doesn't have any associated data, skipping" % (base_path,))
            json_file_path.unlink()
            base_path.rmdir()
            return

        with tarfile.open(name=str(tar_file_path), mode='w:gz') as archive_file:
            archive_file.add(str(data_file_path), arcname=data_file_path.name)
            archive_file.add(str(json_file_path), arcname=json_file_path.name)

        if _unlink_unencrypted_files:
            data_file_path.unlink()
            json_file_path.unlink()
            base_path.rmdir()

        gpg_file_path = base_path.with_suffix('.tar.gz.gpg')
        with tar_file_path.open('rb') as tar_fh:
            self._gpg.encrypt_file(tar_fh,
                                   recipients=[self._gpg_recipient],
                                   sign=self._gpg_signature_key,
                                   passphrase=self._gpg_passphrase,
                                   output=str(gpg_file_path))

        if not gpg_file_path.exists():
            raise Exception("Error writing gpg file (look for error above)")

        if _unlink_unencrypted_files:
            tar_file_path.unlink()

    def district_finalize(self, district: District):
        super().district_finalize(district)
        if not self._no_gpg and self._per_district and not self._consolidate:
            raise NotImplementedError("per-district lz gpg not implemented")

    def _get_json_path(self, assessment) -> Path:
        file_name = self._assessment_format.name_format
        base_path = self._assessment_base_path(assessment)

        json_path = (base_path / file_name).with_suffix(self._json_suffix)

        if not json_path.parent.exists():
            json_path.parent.mkdir(parents=True)

        return json_path

    def write_assessments(self, assessments):
        """
        TODO: create JSON files here instead of writing assessment data as CSV
        """
        for assessment in assessments:
            json_path = self._get_json_path(assessment)
            json_contents = self._assessment_format.get_row(assessment)
            jsonschema.validate(json_contents, self._json_schema)

            with json_path.open('x') as json_fh:
                json.dump(json_contents, json_fh, indent=self._json_indent, sort_keys=self._json_sort_keys)

    def write_assessment_items(self, assessment_items):
        pass  # TODO: create LZ files for item level data, if this is ever necessary

    def write_assessment_item_outcomes(self, assessment_item_outcomes):
        pass  # TODO: create LZ files for item level data, if this is ever necessary
