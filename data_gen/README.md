DATA GENERATION UTILITY

This is a data generation tool for the PARCC project. The code is a heavily modified version of a similar tool that
was used for the SBACC project. This version was originally intended to produce somewhat-realistic looking data for
developing the analytics UI (pentaho) component, as well as load testing the analytics ETL (starmigrate). Its role
has expanded a bit since then, to load testing summative UDL intakes and load testing the reporting application.

This tool currently produces summative ELA and Math data in 3 formats:
- .tar.gz.gpg files suitable for intake via UDL
- CSV files in reporting DB or UDL intake format
- Direct write to a reporting or analytics DB w/ appropriate schema

On a machine with enough processing power (i.e. an AWS c4.8xlarge) it can generate 12 million rows of data in about
2 hours.

The tool is highly configurable, though most options do not need to be tweaked w/out a specific requirement.

##Installation & Requirements

Like the rest of the PARCC codebase, this project assumes python 3.3, though it makes use of backported libraries from
python 3.4.

In order to output to UDL LZ format, this project requires GPG to be installed. It seems to work fine w/ gpg 2.0.2 that
comes w/ Centos 7, but won't work w/ the gpg 2.1.3 that I installed on my Mac. Knowing me, there was something wonky
about my Mac GPG install, but I haven't looked into it too much. As the python GPG module used has no C extensions,
everything else works fine if you don't have GPG installed.

This project requires numpy and scipy be installed. Due to bugs in setuptools/distutils, these packages cannot be
installed using setup.py. Instructions for installing the prerequisites for these packages can be found on their
respective home pages:

    http://docs.scipy.org/doc/numpy/user/install.html
    http://www.scipy.org/install.html

This project requires postgres libraries to be installed, for writing data directly to a database w/ psycopg2 and
SQLAlchemy. psycopg2 won't install without these libraries, but as it is only used indirectly through SQLAlchemy,
you might be able to get away w/ installing the rest of the requirements if you don't want to use this functionality.

Other requirements are most directly met by (instantiating a virtual environment, and) running

    python setup-developer.py develop

##Usage

These instructions may be slightly out of date; run the following for more info:

    python generate_data.py --help


usage: 

       generate_data.py [-h]
                        [--quiet]
                        [--log-file LOG_FILE]
                        [--random-seed RANDOM_SEED]
                        [--multithread]
                        [--thread-count THREAD_COUNT]
                        [--per-district]
                        [--output-formats {UDL_LZ,UDL_NO_GPG,REPORTING_CSV,REPORTING_DB} [{UDL_LZ,UDL_NO_GPG,REPORTING_CSV,REPORTING_DB} ...]]
                        [--batch {CUSTOM,PARCC_LARGE_CAT,PARCC_LARGE_DOG,PARCC_SMALL_CAT,PARCC_SMALL_DOG,DEVEL_TINY,TEST_STATES,TEST_DISTRICTS,TEST_COVERAGE,UAT}]
                        [--custom-batch-state-code {AL,AK,AZ,AR,CA,CO,CT,DE,FL,GA,HI,ID,IL,IN,IA,KS,KY,LA,ME,MD,MA,MI,MN,MS,MO,MT,NE,NV,NH,NJ,NM,NY,NC,ND,OH,OK,OR,PA,RI,SC,SD,TN,TX,UT,VT,VA,WA,WV,WI,WY,DC,AS,GU,MP,PR,VI,UM,FM,MH,PW,ES,FE,CS,PS,WW,XX,YY,ZZ}]
                        [--custom-batch-mean-pop CUSTOM_BATCH_MEAN_POP]
                        [--custom-batch-mean-district-count CUSTOM_BATCH_MEAN_DISTRICT_COUNT]
                        [--custom-batch-mean-school-count CUSTOM_BATCH_MEAN_SCHOOL_COUNT]
                        [--custom-batch-required-districts [{CHICAGO,TEST_TINY} [{CHICAGO,TEST_TINY} ...]]]
                        [--custom-batch-distribution {uniform,gamma}]
                        [--custom-batch-distribution-kwargs CUSTOM_BATCH_DISTRIBUTION_KWARGS]
                        [--custom-batch-demographics {CALIFORNIA,TYPICAL1}]
                        [--custom-batch-year-range CUSTOM_BATCH_YEAR_RANGE]
                        [--custom-batch-grades {IT,PR,PK,TK,KG,G01,G02,G03,G04,G05,G06,G07,G08,G09,G10,G11,G12,G13,PS,UG,Other,OutOfSchool} [{IT,PR,PK,TK,KG,G01,G02,G03,G04,G05,G06,G07,G08,G09,G10,G11,G12,G13,PS,UG,Other,OutOfSchool} ...]]
                        [--out-path OUT_PATH]
                        [--overwrite]
                        [--udl-json-schema JSON_SCHEMA]
                        [--udl-gpg-home-dir GPG_HOME_DIR]
                        [--udl-gpg-recipient GPG_RECIPIENT]
                        [--udl-gpg-passphrase GPG_PASSPHRASE]
                        [--udl-gpg-signature-key GPG_SIGNATURE_KEY]
                        [--db-connector DB_CONNECTOR]
                        [--db-host DB_HOST]
                        [--db-port DB_PORT]
                        [--db-username DB_USERNAME]
                        [--db-password DB_PASSWORD]
                        [--db-database DB_DATABASE]
                        [--db-schema DB_SCHEMA]
                        [--db-delete]
                        [--db-use-transactions]
                        [--output-chunk-size OUTPUT_CHUNK_SIZE]


optional arguments:

      -h, --help            show this help message and exit
      
      --quiet, -q           Don't log to the console.
      
      --log-file LOG_FILE, -L LOG_FILE
                            log to a specified file (in addition to the console, unless --quiet is specified)
                            
      --random-seed RANDOM_SEED, -R RANDOM_SEED
                            Use a specific random seed, for deterministic generation. Doesn't work w/ multithreading.
                            
      --multithread, -m     generate data using multiple threads. work is broken up into districts. 
                            default: False
      
      --thread-count THREAD_COUNT, -j THREAD_COUNT
                            number of threads to use when multithreading 
                            default: 8
                            
      --per-district, -P    generate a separate CSV or UDL file for each district instead of for each state
      
      --output-formats {UDL_LZ,UDL_NO_GPG,REPORTING_CSV,REPORTING_DB} [{UDL_LZ,UDL_NO_GPG,REPORTING_CSV,REPORTING_DB} ...], -O {UDL_LZ,UDL_NO_GPG,REPORTING_CSV,REPORTING_DB} [{UDL_LZ,UDL_NO_GPG,REPORTING_CSV,REPORTING_DB} ...]
                            output formats 
                            default: ['UDL_LZ']
                            
      --batch {CUSTOM,PARCC_LARGE_CAT,PARCC_LARGE_DOG,PARCC_SMALL_CAT,PARCC_SMALL_DOG,DEVEL_TINY,TEST_STATES,TEST_DISTRICTS,TEST_COVERAGE,UAT}, -b {CUSTOM,PARCC_LARGE_CAT,PARCC_LARGE_DOG,PARCC_SMALL_CAT,PARCC_SMALL_DOG,DEVEL_TINY,TEST_STATES,TEST_DISTRICTS,TEST_COVERAGE,UAT}
                            which batch of results to generate. for custom configuration, specify CUSTOM 
                            default: DEVEL_TINY
                            
      --custom-batch-state-code {AL,AK,AZ,AR,CA,CO,CT,DE,FL,GA,HI,ID,IL,IN,IA,KS,KY,LA,ME,MD,MA,MI,MN,MS,MO,MT,NE,NV,NH,NJ,NM,NY,NC,ND,OH,OK,OR,PA,RI,SC,SD,TN,TX,UT,VT,VA,WA,WV,WI,WY,DC,AS,GU,MP,PR,VI,UM,FM,MH,PW,ES,FE,CS,PS,WW,XX,YY,ZZ}
                            state code to use in custom batch 
                            default: NY
                            
      --custom-batch-mean-pop CUSTOM_BATCH_MEAN_POP
                            mean size of student population in custom batch. Note that this is represents the ENTIRE student population,
                            including students not in grades of concern. 
                            default: 2083097
                            
      --custom-batch-mean-district-count CUSTOM_BATCH_MEAN_DISTRICT_COUNT
                            mean number of districts in custom batch 
                            default: 866
                            
      --custom-batch-mean-school-count CUSTOM_BATCH_MEAN_SCHOOL_COUNT
                            mean number of districts in custom batch 
                            default: 4397
                            
      --custom-batch-required-districts [{CHICAGO,TEST_TINY} [{CHICAGO,TEST_TINY} ...]]
                            required districts in custom batch 
                            default: []
                            
      --custom-batch-distribution {uniform,gamma}
                            statistical distribution when dividing populations in custom batch. uniform -> everything the same size;
                            gamma -> more "realistic" 
                            default: gamma
                            
      --custom-batch-distribution-kwargs CUSTOM_BATCH_DISTRIBUTION_KWARGS
                            extra args for statistical distribution for custom batch. for "uniform", these are not used; for gamma, see defaults
                            default: {'min_size': 100, 'scale_ratio': 4}
                            
      --custom-batch-demographics {CALIFORNIA,TYPICAL1}
                            demographics to use in custom batch
                            default: TYPICAL1
                            
      --custom-batch-year-range CUSTOM_BATCH_YEAR_RANGE
                            range of years to use in custom batch.
                            default: range(2014, 2017)
                            
      --custom-batch-grades {IT,PR,PK,TK,KG,G01,G02,G03,G04,G05,G06,G07,G08,G09,G10,G11,G12,G13,PS,UG,Other,OutOfSchool} [{IT,PR,PK,TK,KG,G01,G02,G03,G04,G05,G06,G07,G08,G09,G10,G11,G12,G13,PS,UG,Other,OutOfSchool} ...]
                            grades to use in custom batch
                            default: ['G03', 'G04', 'G05', 'G06', 'G07', 'G08', 'G09', 'G10', 'G11']
                            
      --out-path OUT_PATH, -o OUT_PATH
                            specify the root directory for writing output files to 
                            default: out
                            
      --overwrite, -f       wipe existing output data directory if it exists. 
                            default: False
      
      --udl-json-schema JSON_SCHEMA, -S JSON_SCHEMA
                            the schema for validating the output payload.js file for UDL jobs
                            
      --udl-gpg-home-dir GPG_HOME_DIR, -l GPG_HOME_DIR
                            location of your gpg keys 
                            default: ../component_tests/components_tests/data/keys/dev
                            
      --udl-gpg-recipient GPG_RECIPIENT, -r GPG_RECIPIENT
                            recipient is who you are encrypting the file for 
                            default: sbac_data_provider@sbac.com
                            
      --udl-gpg-passphrase GPG_PASSPHRASE, -p GPG_PASSPHRASE
                            passphrase 
                            default: sbac udl2
                            
      --udl-gpg-signature-key GPG_SIGNATURE_KEY, -k GPG_SIGNATURE_KEY
                            owner of your key for signing the output 
                            default: ca_user@ca.com
                            
      --db-connector DB_CONNECTOR
                            name of the sqlalchemy connector to use when writing directly to DB 
                            default: postgresql
                            
      --db-host DB_HOST     name of db host when writing directly to db. 
                            default: 127.0.0.1
      
      --db-port DB_PORT     port to use when writing directly to db. 
                            default: 5432
      
      --db-username DB_USERNAME
                            user name to use when writing directly to db. 
                            default: postgres
                            
      --db-password DB_PASSWORD
                            password to use when writing directly to db. 
                            default: ""
                            
      --db-database DB_DATABASE
                            database to use when writing directly to db. 
                            default: analytics
                            
      --db-schema DB_SCHEMA
                            schema to use when writing directly to db. 
                            default: edware_prod
                            
      --db-delete           if present, delete existing data in tables before writing directly to db. 
                            default: False
      
      --db-use-transactions, -X
                            if present, use db transactions for batches 
                            default: False
                            
      --output-chunk-size OUTPUT_CHUNK_SIZE
                            how many assessment outcome objects to collect before flushing them to disk/db

EXAMPLES:

    Generate the DEVEL_TINY data set in UDL_LZ format into the "out" directory:

        python generate_data.py

    Generate the "PARCC_LARGE" data set, writing directly to a local db:

        python generate_data.py -b PARCC_LARGE -O REPORTING_DB

    The above, using multiple threads/db connections:

        python generate_data.py -b PARCC_LARGE -O REPORTING_DB -m

    Generate output for LZ, overwriting any existing data:

        python generate_data.py -b PARCC_LARGE -O UDL_LZ -f -o out-large

    Generate UDL LZ data for a state w/ parameters something like Illinois (the largest PARCC state):

        python generate_data.py -m -f \
                                -o udl_2mil_3year \
                                -O UDL_LZ \
                                -b CUSTOM \
                                --custom-batch-state-code NY \
                                --custom-batch-mean-pop 2083097 \
                                --custom-batch-mean-district-count 866 \
                                --custom-batch-required-districts CHICAGO \
                                --custom-batch-mean-school-count 4397 \
                                --custom-batch-year-range 2013,2016


##Unit Tests

    nosetests -w data_generation
