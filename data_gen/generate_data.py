"""
The data generator for the PARCC project.
"""
import argparse
from ast import literal_eval
import datetime
import multiprocessing
import shutil
from pathlib import Path
import logging

from sqlalchemy.engine import create_engine
from sqlalchemy.engine.url import URL
from sqlalchemy.sql.schema import MetaData

from data_generation.config.batch import Batch, DEFAULT_YEARS, DEFAULT_GRADES
from data_generation.config.hierarchy import StateName, ExampleDistrict
from data_generation.config.out import OutputFormat
from data_generation.config.population import Demographics, Grade
from data_generation.main import ParccDataGen
from data_generation.main_multi import ParccDataGenMulti
from data_generation.writers.db_writer import DBWriter


def parse_args(args: list=None,
               namespace: argparse.Namespace=None) -> argparse.Namespace:
    """

    :param args: optional: manually specify arguments
    :param namespace: optiona: augment namespace
    :return: command line argument namespace
    """
    main_path = Path(__file__).parent  # path of the current file

    parser = argparse.ArgumentParser(description='PARCC data generation task.')

    # placeholder in case we need to generate other kinds of data
    parser.add_argument('--quiet', '-q', dest='verbose', action='store_false', default=True,
                        help="Don't log to the console.")
    parser.add_argument('--log-file', '-L', type=Path, default=None,
                        help="log to a specified file (in addition to the console, unless --quiet is specified)")
    parser.add_argument('--log-level', choices=['ERROR', 'WARNING', 'INFO', 'DEBUG'], default='INFO',
                        help="set logging level")
    parser.add_argument('--random-seed', '-R', type=str, default=None,
                        help='Use a specific random seed, for deterministic generation. Doesn\'t work w/ multithreading.')

    parser.add_argument('--multithread', '-m', default=False, action='store_true',
                        help='generate data using multiple threads. work is broken up into districts. (default: %(default)s)')
    parser.add_argument('--thread-count', '-j', type=int, default=multiprocessing.cpu_count(),
                        help='number of threads to use when multithreading (default: %(default)s)')
    parser.add_argument('--per-district', '-P', default=False, action='store_true',
                        help='generate a separate CSV or UDL file for each district instead of for each state')

    parser.add_argument('--output-formats', '-O', nargs='+', default=[OutputFormat.UDL_LZ.name], choices=[of.name for of in OutputFormat],
                        help="output formats (default: %(default)s)")
    parser.add_argument('--batch', '-b', choices=[batch.name for batch in Batch], default=Batch.DEVEL_TINY.name,
                        help='which batch of results to generate. for custom configuration, specify CUSTOM (default: %(default)s)')

    parser.add_argument('--custom-batch-state-codes', nargs='+', choices=[state.code for state in StateName], default=[StateName.NEW_YORK.code],
                        help='state code to use in custom batch (default: %(default)s)')
    parser.add_argument('--custom-batch-mean-pop', type=int, default=2083097,
                        help='mean size of student population in custom batch. note that this is represents the ENTIRE student population, including students not in grades of concern. (default: %(default)s)')
    parser.add_argument('--custom-batch-mean-district-count', type=int, default=866,
                        help='mean number of districts in custom batch (default: %(default)s)')
    parser.add_argument('--custom-batch-mean-school-count', type=int, default=4397,
                        help='mean number of districts in custom batch (default: %(default)s)')
    parser.add_argument('--custom-batch-required-districts', nargs='*', choices=[district.name for district in ExampleDistrict], default=[],
                        help='required districts in custom batch (default: %(default)s)')
    parser.add_argument('--custom-batch-distribution', choices=['uniform', 'gamma'], default='uniform',
                        help='statistical distribution when dividing populations in custom batch. '
                        'uniform -> everything the same size; gamma -> more "realistic" (default: %(default)s)')
    parser.add_argument('--custom-batch-distribution-kwargs', type=literal_eval, default={'scale_ratio': 4, 'min_size': 100},
                        help='extra args for statistical distribution for custom batch. for "uniform", these are not used; for gamma, see defaults (default: %(default)s)')
    parser.add_argument('--custom-batch-demographics', choices=[demo.name for demo in Demographics], default=Demographics.TYPICAL1.name,
                        help='demographics to use in custom batch (default: %(default)s)')
    parser.add_argument('--custom-batch-year-range', type=lambda arg: range(*map(int, arg.split(','))), default=DEFAULT_YEARS,
                        help='range of years to use in custom batch (default: %(default)s)')
    parser.add_argument('--custom-batch-grades', nargs='+', choices=[grade.name for grade in Grade], default=[grade.name for grade in DEFAULT_GRADES],
                        help='grades to use in custom batch (default: %(default)s)')

    parser.add_argument('--out-path', '-o', default=Path('out'), type=Path,
                        help='specify the root directory for writing output files to (default: %(default)s)')
    parser.add_argument('--overwrite', '-f', action='store_true', default=False,
                        help='wipe existing output data directory if it exists. (default: %(default)s)')

    parser.add_argument('--udl-json-schema', '-S', dest='json_schema', type=Path,
                        default=(main_path / '..' / 'parcc_udl' / 'schema' / 'summative_assessment_metadata_schema.json'),
                        help='the schema for validating the output payload.js file for UDL jobs')

    parser.add_argument('--udl-gpg-home-dir', '-l', dest='gpg_home_dir', type=Path,
                        default=(main_path / '..' / 'component_tests' / 'components_tests' / 'data' / 'keys' / 'dev'),
                        help='location of your gpg keys (default: %(default)s)')
    parser.add_argument('--udl-gpg-recipient', '-r', dest='gpg_recipient', default='sbac_data_provider@sbac.com',
                        help='recipient is who you are encrypting the file for (default: %(default)s)')
    parser.add_argument('--udl-gpg-passphrase', '-p', dest='gpg_passphrase', default='sbac udl2',
                        help='passphrase (default: %(default)s)')
    parser.add_argument('--udl-gpg-signature-key', '-k', dest='gpg_signature_key', default='ca_user@ca.com',
                        help='owner of your key for signing the output (default: %(default)s)')

    parser.add_argument('--db-connector', default='postgresql',
                        help='name of the sqlalchemy connector to use when writing directly to DB (default: %(default)s)')
    parser.add_argument('--db-host', default='127.0.0.1',
                        help='name of db host when writing directly to db. (default: %(default)s)')
    parser.add_argument('--db-port', type=int, default=5432,
                        help="port to use when writing directly to db. (default: %(default)s)")
    parser.add_argument('--db-username', default='postgres',
                        help="user name to use when writing directly to db. (default: %(default)s)")
    parser.add_argument('--db-password', default='',
                        help="password to use when writing directly to db. (default: %(default)s)")
    parser.add_argument('--db-database', default='analytics',
                        help="database to use when writing directly to db. (default: %(default)s)")
    parser.add_argument('--db-schema', default='edware_prod',
                        help="schema to use when writing directly to db. (default: %(default)s)")
    parser.add_argument('--db-delete', action='store_true', default=False,
                        help="if present, delete existing data in tables before writing directly to db. (default: %(default)s)")
    parser.add_argument('--db-use-transactions', '-X', action='store_true', default=False,
                        help="if present, use db transactions for batches (default: %(default)s)")

    parser.add_argument('--output-chunk-size', type=int, default=1024,
                        help='how many assessment outcome objects to collect before flushing them to disk/db')

    return parser.parse_args(args, namespace)


def init_logging(args):
    log_format = '%(levelname) 8s:%(message)s'  # TODO make configurable?
    log_level = getattr(logging, args.log_level)

    logging.root.setLevel(log_level)

    if args.log_file:
        formatter = logging.Formatter(log_format)

        file_handler = logging.FileHandler(str(args.log_file))
        file_handler.setLevel(log_level)
        file_handler.setFormatter(formatter)
        logging.root.addHandler(file_handler)

        console_handler = logging.StreamHandler()
        console_handler.setLevel(log_level if args.verbose else logging.WARN)
        console_handler.setFormatter(formatter)
        logging.root.addHandler(console_handler)

    else:
        if args.verbose:
            level = log_level

        else:
            level = logging.WARN

        logging.basicConfig(format=log_format, level=level)


def main(args):
    """
    :param args: command line argument namespace
    """
    init_logging(args)

    # Record current (start) time
    tstart = datetime.datetime.now()
    logging.info('Run began @ %s' % tstart)

    if args.multithread and args.random_seed:
        logging.warning("specifying a seed isn't guaranteed to produce deterministic output in multi-thread mode!")

    output_formats = tuple(OutputFormat[output_format] for output_format in args.output_formats)

    # check if we need to initialize the DB
    if any(output_format.record_writer_class == DBWriter for output_format in output_formats) and args.db_delete:
        url = URL(drivername=args.db_connector, username=args.db_username, password=args.db_password, host=args.db_host, port=args.db_port, database=args.db_database)
        engine = create_engine(url)

        metadata = MetaData(schema=args.db_schema, bind=engine)
        metadata.reflect()

        for table in metadata.tables.values():
            engine.execute(table.delete())

    # check if we need to initialize an output directory
    if any(output_format.record_writer_class != DBWriter for output_format in output_formats):
        args.out_path = args.out_path.absolute()  # make the path absolute, for reasons

        # Verify output directory exists and is empty
        if args.out_path.exists():
            if args.overwrite:
                shutil.rmtree(str(args.out_path))

            else:
                logging.fatal("Error: output directory %s already exists. Give the -f flag to overwrite." %
                              (args.out_path,))
                exit(1)

        args.out_path.mkdir(parents=True)

    # choose a base datagen class
    if args.multithread:
        main_class = ParccDataGenMulti

    else:
        main_class = ParccDataGen

    data_gen = main_class(args)

    data_gen.generate()

    # Record now current (end) time
    tend = datetime.datetime.now()

    # Print statistics
    logging.info('Run ended at:  %s' % tend)
    logging.info('Run run took:  %s' % (tend - tstart))


if __name__ == '__main__':
    main(parse_args())
