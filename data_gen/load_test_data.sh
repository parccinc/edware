#!/usr/bin/env bash
set -v
set -e
set -x

run_a_load() {
    outdir=$1
    population=$2
    dist_count=$3
    school_count=$4
    year_range=$5
    district=$6

    python generate_data.py -m -f \
                             -o $outdir \
                             -O UDL_LZ REPORTING_CSV \
                             -b CUSTOM \
                             --custom-batch-state-codes NY \
                             --custom-batch-mean-pop $population \
                             --custom-batch-mean-district-count $dist_count \
                             --custom-batch-required-districts $district \
                             --custom-batch-mean-school-count $school_count \
                             --custom-batch-year-range $year_range &> ${outdir}.log

    tar -cz -f ${outdir}/udl_lz.tar.gz        ${outdir}/LZ
    tar -cz -f ${outdir}/reporting_csv.tar.gz ${outdir}/CSV
    aws s3 cp ${outdir}/udl_lz.tar.gz        s3://amplify-parcc-datagen/${outdir}/udl_lz.tar.gz
    aws s3 cp ${outdir}/reporting_csv.tar.gz s3://amplify-parcc-datagen/${outdir}/reporting_csv.tar.gz
}

run_a_load 0073k_1year 105512   44  223 2015,2016 &
run_a_load 0073k_3year 105512   44  223 2013,2016 &

run_a_load 0094k_1year 135287   56  286 2015,2016 &
run_a_load 0094k_3year 135287   56  286 2013,2016 &

run_a_load 0120k_1year 173462   72  366 2015,2016 &
run_a_load 0120k_3year 173462   72  366 2013,2016 &

run_a_load 0154k_1year 222411   92  469 2015,2016 &
run_a_load 0154k_3year 222411   92  469 2013,2016 &

for job in $(jobs -p)
do
    wait $job
done

run_a_load 0197k_1year 285171  119  602 2015,2016 &
run_a_load 0197k_3year 285171  119  602 2013,2016 &

run_a_load 0253k_1year 365642  152  772 2015,2016 &
run_a_load 0253k_3year 365642  152  772 2013,2016 &

run_a_load 0325k_1year 468820  195  990 2015,2016 &
run_a_load 0325k_3year 468820  195  990 2013,2016 &

run_a_load 0416k_1year 601114  250 1269 2015,2016 &
run_a_load 0416k_3year 601114  250 1269 2013,2016 &

for job in $(jobs -p)
do
    wait $job
done

run_a_load 0534k_1year 770740  320 1627 2015,2016 &
run_a_load 0534k_3year 770740  320 1627 2013,2016 &

run_a_load 0684k_1year 988230  411 2086 2015,2016 &
run_a_load 0684k_3year 988230  411 2086 2013,2016 &

run_a_load 0877k_1year 1267093 527 2675 2015,2016 &
run_a_load 0877k_3year 1267093 527 2675 2013,2016 &

run_a_load 1000k_1year 1444444 866 4397 2015,2016 &
run_a_load 1000k_3year 1444444 866 4397 2013,2016 &

for job in $(jobs -p)
do
    wait $job
done

run_a_load 1125k_1year 1624648 675 3429 2015,2016 &
run_a_load 1125k_3year 1624648 675 3429 2013,2016 &

run_a_load 1442k_1year_chi 2083097 866 4397 2015,2016 CHICAGO &
run_a_load 1442k_3year_chi 2083097 866 4397 2013,2016 CHICAGO &

run_a_load 2000k_1year_chi 2888888 866 4397 2015,2016 CHICAGO &
run_a_load 2000k_3year_chi 2888888 866 4397 2013,2016 CHICAGO &

for job in $(jobs -p)
do
    wait $job
done

