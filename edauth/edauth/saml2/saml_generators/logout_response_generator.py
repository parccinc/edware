from xml.dom.minidom import Document
import uuid
from time import gmtime, strftime
from edauth.security.utils import deflate_base64_encode


class SamlResponseGenerator:
    """
    Represent a SAML Response
    """
    def __init__(self, issuer_name):
        self._uuid = str(uuid.uuid1())
        self._issuer_name = issuer_name

    def format_response(self, doc):
        '''
        Serialize the doc's root element so that it will strip out the xml declaration
        '''
        data = doc.documentElement.toxml('utf-8')
        return {'SAMLResponse': deflate_base64_encode(data)}

    def get_id(self):
        '''
        Return request id
        '''
        return self._uuid


class SamlLogoutResponseGenerator(SamlResponseGenerator):
    """
    Represent a SAML Logout Response
    """
    def __init__(self,
                 issuer_name,
                 destination,
                 in_response_to,
                 status):
        super().__init__(issuer_name)
        self.destination = destination
        self.in_response_to = in_response_to
        self.status = status

    def generate_saml_response(self):
        """
        Creates an XML SAML Logout Response. Returns as a byte string.
        """
        doc = Document()
        samlp_logout_request = doc.createElement('saml2p:LogoutResponse')
        samlp_logout_request.setAttribute('xmlns:saml2p', 'urn:oasis:names:tc:SAML:2.0:protocol')
        samlp_logout_request.setAttribute('ID', self._uuid)
        samlp_logout_request.setAttribute('Version', '2.0')
        samlp_logout_request.setAttribute('IssueInstant', strftime('%Y-%m-%dT%H:%M:%SZ', gmtime()))
        samlp_logout_request.setAttribute('Destination', self.destination)
        samlp_logout_request.setAttribute('InResponseTo', self.in_response_to)

        saml_issuer = doc.createElement('saml2:Issuer')
        saml_issuer.setAttribute('xmlns:saml2', 'urn:oasis:names:tc:SAML:2.0:assertion')
        saml_issuer_text = doc.createTextNode(self._issuer_name)
        saml_issuer.appendChild(saml_issuer_text)
        samlp_logout_request.appendChild(saml_issuer)

        samlp_status = doc.createElement('saml2p:Status')
        status_code = doc.createElement('saml2p:StatusCode')
        status_code.setAttribute('Value', 'urn:oasis:names:tc:SAML:2.0:status:{}'.format(self.status))
        samlp_status.appendChild(status_code)
        samlp_logout_request.appendChild(samlp_status)
        doc.appendChild(samlp_logout_request)

        return self.format_response(doc)
