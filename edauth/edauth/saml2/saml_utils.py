# Utils for saml requests and responses
import time
import calendar


def time_convert_utc(datetime):
    '''
    support only UTC datetime format
    datetime must end with Z
    '''
    # if time is in UTC
    current_time_utc = None
    if datetime.endswith('Z'):
        current_time_utc = calendar.timegm(time.strptime(datetime[:-1], '%Y-%m-%dT%H:%M:%S'))
    return current_time_utc
