from xml.dom.minidom import Node


class LogoutRequestParser():
    """
    Parses SAML XML received from OpenAM
    """
    def __init__(self, request_xml):
        self.__parse(request_xml)

    def __parse(self, request_xml):
        """
        get parent Element "Response
        :param request_xml:
        :return:
        """
        Request_node = request_xml.childNodes[0]
        self.ID = self.__get_value(Request_node, "ID")
        self.Version = self.__get_value(Request_node, "Version")
        self.IssueInstant = self.__get_value(Request_node, "IssueInstant")
        self.Destination = self.__get_value(Request_node, "Destination")
        self.NotBefore = self.__get_value(Request_node, "NotBefore")
        self.NotOnOrAfter = self.__get_value(Request_node, "NotOnOrAfter")

        for node in Request_node.childNodes:
            if node.nodeType == Node.ELEMENT_NODE:
                if node.localName == "Issuer":
                    self.__issuer = LogoutRequestParser.Issuer(node)
                elif node.localName == "SessionIndex":
                    self.__sessionindex = LogoutRequestParser.SessionIndex(node)
                elif node.localName == "NameID":
                    self.__nameid = LogoutRequestParser.NameID(node)

    def __get_value(self, node, name):
        return node.getAttribute(name)

    def get_id(self):
        return self.ID

    def get_index(self):
        return self.__sessionindex

    class SessionIndex:
        """
        Session element
        """
        def __init__(self, index_node):
            self.__index = index_node.childNodes[0].data

        def get_session_index(self):
            return self.__index

    class Issuer:
        """
        Issuer Element
        """
        def __init__(self, issuer_node):
            self.__issuer = issuer_node.childNodes[0].data

    class NameID:
        """
        Assertion Element
        """
        def __init__(self, nameid_node):
            self.__nameID = nameid_node.childNodes[0].data
