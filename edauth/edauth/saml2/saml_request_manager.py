import time
import calendar
from xml.dom.minidom import parseString
from edauth.saml2.saml_request_parser import LogoutRequestParser
from edauth.saml2.saml_utils import time_convert_utc


SAML_REQUEST_MAP = {
    'LogoutRequest': LogoutRequestParser
}


class SamlLogoutRequestManager():
    """
    Managing SAMLLogoutRequest - verify signature, check time condition
    and status.
    """
    def __init__(self, saml_request_string):
        self.__saml_request = None
        self.__saml_string = saml_request_string
        if saml_request_string is not None:
            request_dom = parseString(self.__saml_string)
            request_type = request_dom.childNodes[0].localName
            request_parser = SAML_REQUEST_MAP.get(request_type)
            if request_parser:
                self.__saml_request = request_parser(request_dom)

    def is_condition_ok(self):
        '''
        check datetime condition
        '''
        result = True
        if self.__saml_request is not None:
            current_utc_time = calendar.timegm(time.gmtime())
            not_before_utc = time_convert_utc(self.__saml_request.NotBefore)
            not_on_or_after_utc = time_convert_utc(self.__saml_request.NotOnOrAfter)
            if not_before_utc and not_on_or_after_utc:
                result = not_before_utc < current_utc_time <= not_on_or_after_utc
        else:
            result = False
        return result

    def get_idp_session_index(self):
        session = self.__saml_request.get_index()
        return session.get_session_index()

    def get_id(self):
        return self.__saml_request.get_id()
