#!/bin/bash

set -e  # exit on error
#set -v  # verbose
#set -x  # echo

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
EDWARE_BASE="$( cd -P "$( dirname "$SOURCE" )" && pwd )"/..

function check_pep8 {
    echo "********************************"
    echo "Checking code style against pep8"
    echo "********************************"
    # E501: line too long
    # E265: block comment should start with ‘# ‘
    # E402: module level import not at top of file
    ignore="E501,E265,E402"

    if [ "$MODULE" == "data_gen" ]
    then
        # temporary; new data-gen code wasn't build with re-use in mind and doesn't pass pep8
        pep8 --ignore=$ignore "$MODULE_BASE/parcc"
    else
        pep8 --ignore=$ignore "$MODULE_BASE"
    fi

    echo "Finished checking code style against pep8"
}

function run_unit_tests {
    echo "Running unit tests"

    cd "$MODULE_BASE"

    if [[ "$MODULE" == "edudl2" ]] || [[ "$MODULE" == "edschema" ]]
    then
        # chokes on parallel tests because of files written to disk?
        echo nosetests -vs --cover-erase --cov-report term
        nosetests -vs --cover-erase --cov-report term
    elif [[ "$MODULE" == "data_gen" ]]
    then
        echo nosetests for data gen
        nosetests -vs --cover-erase --cov-report term -w data_generation
    else
        echo nosetests -vs --processes=8 --process-timeout=240 --cover-erase --cov-report term
        nosetests -vs --cover-erase --cov-report term
        #nosetests -vs --processes=8 --process-timeout=240 --cover-erase --cov-report term
    fi
}

function usage() {
    echo Usage:
    echo    $0 MODULE_NAME
}

function main() {
    if [[ -z "$1" ]]
    then
         usage
         exit
    fi

    MODULE=$1
    MODULE_BASE="$EDWARE_BASE/$MODULE"

    if [[ ! -e "$MODULE_BASE" ]]
    then
        echo "Module $MODULE can't be found @ $MODULE_BASE"
        exit 1
    fi

    check_pep8
    run_unit_tests
}

main $@
