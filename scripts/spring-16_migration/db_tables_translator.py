"""
Migration script for data migration to fall-2015 format
"""
import argparse
import logging
import subprocess
import sys
import os


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
logger.addHandler(ch)

#############################################################################
#                   CONSTANTS REQUIRED FOR TRANSLATION                      #
#############################################################################


class Constants:
    FILE_SUFFIX = '_fall_2015'
    SUPPORTED_TABLES = ('rpt_ela_sum', 'rpt_math_sum')


#############################################################################
#                       MAIN TRANSFORMATION LOGIC                           #
#############################################################################


def main(args):
    """ Run translation script for each file """
    files_for_translation = set()
    if args.csv_file and os.path.isfile(args.csv_file):
        files_for_translation.add(os.path.abspath(args.csv_file))

    if args.csv_list:
        files_for_translation.update(
            {os.path.abspath(fname.strip()) for fname in args.csv_list if os.path.isfile(fname.strip())})

    if args.search_files:
        files_for_translation.update(search_files(args.search_files))

    for fname in files_for_translation:
        logger.info('\n---------------------------------------------')
        logger.info('Process file: {}'.format(fname))
        run_translation(fname, args)


def run_translation(filename: str, args: argparse.Namespace):
    """
    Main translation function. First we check file format, if file in format 'fall-2015' -
    skip transformation. Otherwise apply transformation rules. For this we create temporary file
    to store new data and after transformation replace original file by new.
    :param filename: file for transformation
    :param args: script arguments
    """
    # if filename is not in the list of files that required translation - just insert this file to the DB
    upload_data_from_csv(filename, args.username, args.password)


def upload_data_from_csv(filename, username, password):
    """
    Upload data from csv file to the database
    :param filename: str, filename with the data
    :param login: db login, 'edware' by default
    :param password: db login, 'edware2013' by default
    """
    schema = 'edware_prod'  # hardcoded, change in case we'll need to load data to some other schema
    table = get_tablename_from_filename(filename)
    db_conn = get_db_conn(username, password)
    mask = 'psql {db_conn} -c "copy {schema}.{table} from \'{filename}\' ' \
           'WITH DELIMITER \',\' HEADER CSV"'
    command = mask.format(db_conn=db_conn, schema=schema, table=table, filename=filename)
    try:
        logger.info('Starting csv upload process for table: {table}'.format(table=table))
        status = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True).decode('utf-8')
        logger.info(status)
        logger.info('[SUCESS] CSV uploaded for table: {table}'.format(table=table))

    except subprocess.CalledProcessError as e:
        logger.error(e.output.decode('utf-8'))
        logger.error("Possibly you're uploading data to the non empty table!")


def get_db_conn(username: str, password: str)-> str:
    """
    Create token for db connection
    :param username: db username, 'edware' by default
    :param password: db login, 'edware2013' by default
    :return: str, db_conn token in format: "postgres://edware:edware2013@[host_ip]:5432/edware"
    """
    port = '5432'
    database = 'edware'
    host = get_host_ip()
    db_conn_mask = "postgres://{username}:{password}@{host}:{port}/{database}"
    return db_conn_mask.format(username=username, password=password, host=host,
                               port=port, database=database)


def get_host_ip()-> str:
    """
    helper method, required to get host ip for DB connection by token
    :return: str, host ip address
    """
    command = "ip route get 1 | awk '{print $NF;exit}'"
    return subprocess.getoutput(command)


def get_tablename_from_filename(filename: str)-> str:
    """
    strip extension and suffix '_fall_2015' from filename
    :param filename: str, full path to the file
    :return: str, table title
    """
    basename = get_basename(filename)
    if basename.endswith(Constants.FILE_SUFFIX):
        table_name = basename[:len(basename) - len(Constants.FILE_SUFFIX)]  # strip suffix '_fall_2015'
    else:
        table_name = basename
    return table_name


def get_basename(filename: str)-> str:
    """
    Helper method, split filename and return basename without extension
    :param filename: str, full path to the file
    :return: str, file basename
    """
    return os.path.splitext(os.path.basename(filename))[0]


def search_files(path: str, extension: str='.csv')-> set:
    """
    Search all csv files in some folder and all subfolders
    :param path: path to search
    :param extension: extension by what we'll search files
    :return: set of csv files
    """
    csv_files = set()
    for root, dirs, files in os.walk(path):
        for name in files:
            if name.endswith(extension):
                csv_files.add(os.path.join(root, name))

    return csv_files


def args_parser() -> argparse.Namespace:
    parser = argparse.ArgumentParser('csv_translator')
    parser.description = 'Convert summative files from old format to "fall-2015"'
    parser.add_argument('-c', '--csv-file', dest='csv_file', nargs='?', required=False,
                        help='path to csv file in db dump format')
    parser.add_argument('-l', '--csv-list-in-file', dest='csv_list', required=False,
                        type=argparse.FileType('r'), help='file with a list of csv files for translation')
    parser.add_argument('-s', '--search', dest='search_files', required=False,
                        help='if specified search all files by ".csv" mask in folder and all subfolders')
    # parser.add_argument('-n', '--create-new', dest='create_new', action='store_true', required=False,
    #                     help='If specified, don\'t replace original file, '
    #                          'create new with suffix "_fall_2015" in the same directory')
    parser.add_argument('-u', '--user', dest='username', required=False, default='edware',
                        help='DB connection username')
    parser.add_argument('-p', '--password', dest='password', required=False, default='edware2013',
                        help='DB connection password')

    if len(sys.argv[1:]) == 0:
        logger.info('You should specify at least one argument!')
        parser.print_help()
        sys.exit(1)

    return parser.parse_args()


if __name__ == "__main__":
    main(args_parser())
