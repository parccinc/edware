#!/bin/bash
set -e # Exit on errors

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Assume starting in <repo>/scripts
cd "$DIR"

#nosetests edudl2/tests/unit_tests/*

bash unittest_module_local.sh edschema
bash unittest_module_local.sh edcore
bash unittest_module_local.sh edudl2
bash unittest_module_local.sh edmigrate
bash unittest_module_local.sh edworker
bash unittest_module_local.sh edapi
bash unittest_module_local.sh edsftp
bash unittest_module_local.sh edextract
bash unittest_module_local.sh services
bash unittest_module_local.sh edauth
bash unittest_module_local.sh starmigrate
