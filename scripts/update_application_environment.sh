#!/bin/bash

if [ "$#" != "10" ]; then
	echo "Usage: sh update_application_environment.sh (environment) (env_version) (edudl2version) (edsftpversion) (edmigrateversion) (edextract_version) (reporting_version) (starmigrate_version) (hpz_version) (pentaho_customization_version)"
	exit 1
fi

cd $WORKSPACE/edware_devops/ansible_playbooks

IS_REALM_CREATED=$(eval aws ec2 describe-instances --filters "Name=tag:realm,Values=$1" "Name=instance-state-name,Values=pending,running" --query "Reservations[0].Instances[0].InstanceId")

if [ -z ${IS_REALM_CREATED} ]; then
    echo 'No environment detected, provisioning a new environment'
    ./infrastructure/parallel-infrastructure-playbook.py $1 dev -f 30 -e make_elbs=true \
        -e '{"web_groups": ["{{web_group_from_within_vpc}}", "{{web_group_from_amplify}}"]}' \
        -e '{"pentaho_groups": "{{ web_groups + [pentaho_group] }}"}'
    if [ "$?" != "0" ]; then
        echo "Infrastructure playbook failed"
        exit 1
    fi
else
    echo "$1 environment exists"
fi

IS_VERSION_UPTODATE=$(eval aws ec2 describe-instances --filters "Name=tag:realm,Values=$1" "Name=tag:env_version,Values=$2" "Name=instance-state-name,Values=pending,running" --query "Reservations[0].Instances[0].InstanceId")
if [ -z ${IS_VERSION_UPTODATE} ]; then
    echo "$1 environment not up to date"
    echo "Bringing up any missing instances"
    ./infrastructure/parallel-infrastructure-playbook.py $1 dev -f 30 -e make_elbs=true \
        -e '{"web_groups": ["{{web_group_from_within_vpc}}", "{{web_group_from_amplify}}"]}' \
        -e '{"pentaho_groups": "{{ web_groups + [pentaho_group] }}"}'
    if [ "$?" != "0" ]; then
        echo "Infrastructure playbook failed"
        exit 1
    fi
    echo "Updating $1 environment"
    ./applications/parallel-application-playbook.py $1 -f 30 -e edudl2_version=$3 -e edsftp_version=$4 \
        -e edmigrate_version=$5 -e edextract_version=$6 -e reporting_version=$7 -e starmigrate_version=$8 \
        -e hpz_version=$9 -e pentaho_customization_version=${10} -e elb_present=true -e url_protocol=https \
        -e reporting_server_address="{{realm}}-reporting-elb{{hosted_zone}}" \
        -e override_auth_policy_secure="True" -e hpz_frs_port=8080 \
        -e override_reporting_auth_saml_issuer_name="https://{{reporting_server_address}}/sp.xml" \
        -e override_analytics_url="https://{{pentaho_server_address}}/pentaho" \
        -e override_hpz_file_registration_url="http://{{ hpz_app_server_pvtdns_hostname }}:{{ hpz_frs_port }}/registration" \
        -e override_hpz_auth_saml_issuer_name="https://{{hpz_server_address}}/sp.xml" \
        -e override_hpz_file_upload_base_url="http://{{ hpz_app_server_pvtdns_hostname }}:{{ hpz_frs_port }}/files" \
        -e override_hpz_frs_download_base_url="https://{{hpz_server_address}}/download" \
        -e elb_present=true -e url_protocol=https -e web_heartbeat_port=443 \
        -e pentaho_heartbeat_port=443 -e '{"e2e_monitoring_topics": ["e2e-heartbeats"]}' \
        -e reinitialize_tenant_dbs=true \
        -e vaulted_var_file=../vault_vars/dev_passwords.yml \
        --private-key=~/.ssh/amplify_parcc_ci_adminkeys.pem
else
    echo "Updating software rpms for $1 environment"
    ./applications/parallel-application-playbook.py $1 -f 30 -e edudl2_version=$3 -e edsftp_version=$4 \
        -e edmigrate_version=$5 -e edextract_version=$6 -e reporting_version=$7 -e starmigrate_version=$8 \
        -e hpz_version=$9 -e pentaho_customization_version=${10} -e elb_present=true -e url_protocol=https \
        -e reporting_server_address="{{realm}}-reporting-elb{{hosted_zone}}" \
        -e override_auth_policy_secure="True" -e hpz_frs_port=8080 \
        -e override_reporting_auth_saml_issuer_name="https://{{reporting_server_address}}/sp.xml" \
        -e override_analytics_url="https://{{pentaho_server_address}}/pentaho" \
        -e override_hpz_file_registration_url="http://{{ hpz_app_server_pvtdns_hostname }}:{{ hpz_frs_port }}/registration" \
        -e override_hpz_auth_saml_issuer_name="https://{{hpz_server_address}}/sp.xml" \
        -e override_hpz_file_upload_base_url="http://{{ hpz_app_server_pvtdns_hostname }}:{{ hpz_frs_port }}/files" \
        -e override_hpz_frs_download_base_url="https://{{hpz_server_address}}/download" \
        -e elb_present=true -e url_protocol=https -e web_heartbeat_port=443 \
        -e pentaho_heartbeat_port=443 -e '{"e2e_monitoring_topics": ["e2e-heartbeats"]}' \
        -e reinitialize_tenant_dbs=true \
        -e vaulted_var_file=../vault_vars/dev_passwords.yml \
        --private-key=~/.ssh/amplify_parcc_ci_adminkeys.pem --dont-run rds --tags "rpm_only"
fi

if [ "$?" != "0" ]; then
    echo "Applications playbook failed"
    exit 1
fi

if [ -z ${IS_VERSION_UPTODATE} ]; then
    echo 'Environment tag not up to date, updating the environment'
    echo 'Tagging environment with latest version'
    ansible-playbook -f 30 -i infrastructure/inventory/ec2.sh -e realm=$1 -e env_version=$2 infrastructure/ec2-tag-version.yml
    if [ "$?" != "0" ]; then
        echo "Tagging playbook failed"
        exit 1
    fi
else
    echo "$1 environment is up to date with version $2"
fi
