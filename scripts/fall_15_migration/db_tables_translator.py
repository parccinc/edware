"""
Migration script for data migration to fall-2015 format
"""
import argparse
import csv
import logging
import shutil
import subprocess
import sys
import os
import re
from tempfile import NamedTemporaryFile


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
logger.addHandler(ch)


#############################################################################
#                   CONSTANTS REQUIRED FOR TRANSLATION                      #
#############################################################################

SUMMATIVE_DB_SCHEMA_OLD_FORMAT = (
    'rec_id', 'record_type', 'is_multi_rec', 'is_reported_summative', 'is_roster_reported',
    'report_suppression_code', 'report_suppression_action', 'pba_category', 'eoy_category', 'state_code',
    'resp_dist_id', 'resp_dist_name', 'resp_school_id', 'resp_school_name', 'pba_test_dist_id',
    'pba_test_dist_name', 'pba_test_school_id', 'pba_test_school_name', 'eoy_test_dist_id',
    'eoy_test_dist_name', 'eoy_test_school_id', 'eoy_test_school_name', 'student_parcc_id',
    'student_state_id', 'student_local_id', 'student_first_name', 'student_middle_name', 'student_last_name',
    'student_sex', 'student_dob', 'opt_state_data1', 'student_grade', 'ethn_hisp_latino',
    'ethn_indian_alaska', 'ethn_asian', 'ethn_black', 'ethn_hawai', 'ethn_white', 'ethn_two_or_more_races',
    'ethn_filler', 'ethnicity', 'ell', 'lep_status', 'gift_talent', 'migrant_status', 'econo_disadvantage',
    'disabil_student', 'primary_disabil_type', 'staff_id', 'accomod_ell', 'accomod_504', 'accomod_ind_ed',
    'accomod_freq_breaks', 'accomod_alt_location', 'accomod_small_group', 'accomod_special_equip',
    'accomod_spec_area', 'accomod_time_day', 'accomod_answer_mask', 'accomod_color_contrast',
    'accomod_text_2_speech_math', 'accomod_read_math', 'accomod_asl_video', 'accomod_screen_reader',
    'accomod_close_capt_ela', 'accomod_read_ela', 'accomod_braille_ela', 'accomod_tactile_graph',
    'accomod_text_2_speech_ela', 'accomod_answer_rec', 'accomod_braille_resp', 'accomod_calculator',
    'accomod_construct_resp_ela', 'accomod_select_resp_ela', 'accomod_math_resp', 'accomod_monitor_test_resp',
    'accomod_word_predict', 'accomod_native_lang', 'accomod_loud_native_lang', 'accomod_math_rsp',
    'accomod_math_text_2_speech', 'accomod_math_trans_online', 'accomod_w_2_w_dict', 'accomod_extend_time',
    'accomod_alt_paper_test', 'accomod_paper_trans_math', 'accomod_human_read_sign', 'accomod_large_print',
    'accomod_braille_tactile', 'opt_state_data2', 'opt_state_data3', 'opt_state_data4', 'opt_state_data5',
    'opt_state_data6', 'opt_state_data7', 'opt_state_data8', 'poy', 'test_code', 'asmt_subject',
    'pba_form_id', 'eoy_form_id', 'pba_test_uuid', 'eoy_test_uuid', 'sum_score_rec_uuid', 'pba_total_items',
    'pba_attempt_flag', 'eoy_total_items', 'eoy_attempt_flag', 'pba_total_items_attempt',
    'pba_total_items_unit1', 'pba_unit1_items_attempt', 'pba_total_items_unit2', 'pba_unit2_items_attempt',
    'pba_total_items_unit3', 'pba_unit3_items_attempt', 'pba_total_items_unit4', 'pba_unit4_items_attempt',
    'pba_total_items_unit5', 'pba_unit5_items_attempt', 'eoy_total_items_attempt', 'eoy_total_items_unit1',
    'eoy_unit1_items_attempt', 'eoy_total_items_unit2', 'eoy_unit2_items_attempt', 'eoy_total_items_unit3',
    'eoy_unit3_items_attempt', 'eoy_total_items_unit4', 'eoy_unit4_items_attempt', 'eoy_total_items_unit5',
    'eoy_unit5_items_attempt', 'pba_not_tested_reason', 'eoy_not_tested_reason', 'pba_void_reason',
    'eoy_void_reason', 'pba_raw_score', 'eoy_raw_score', 'sum_scale_score', 'sum_csem', 'sum_perf_lvl',
    'sum_read_scale_score', 'sum_read_csem', 'sum_write_scale_score', 'sum_write_csem', 'subclaim1_category',
    'subclaim2_category', 'subclaim3_category', 'subclaim4_category', 'subclaim5_category',
    'subclaim6_category', 'state_growth_percent', 'district_growth_percent', 'parcc_growth_percent', 'year',
    'asmt_grade', 'include_in_parcc', 'include_in_state', 'include_in_district', 'include_in_school',
    'include_in_roster', 'include_in_isr', 'batch_guid', 'rec_status', 'create_date', 'status_change_date')

SUMMATIVE_DB_SCHEMA_NEW_FORMAT = (
    'rec_id', 'record_type', 'is_multi_rec', 'is_reported_summative', 'is_roster_reported',
    'report_suppression_code', 'report_suppression_action', 'pba_category', 'eoy_category', 'state_code',
    'resp_dist_id', 'resp_dist_name', 'resp_school_id', 'resp_school_name', 'pba_test_dist_id',
    'pba_test_dist_name', 'pba_test_school_id', 'pba_test_school_name', 'eoy_test_dist_id',
    'eoy_test_dist_name', 'eoy_test_school_id', 'eoy_test_school_name', 'student_parcc_id',
    'student_state_id', 'student_local_id', 'student_first_name', 'student_middle_name', 'student_last_name',
    'student_sex', 'student_dob', 'opt_state_data1', 'student_grade', 'ethn_hisp_latino',
    'ethn_indian_alaska', 'ethn_asian', 'ethn_black', 'ethn_hawai', 'ethn_white', 'ethn_two_or_more_races',
    'ethn_filler', 'ethnicity', 'ell', 'lep_status', 'gift_talent', 'migrant_status', 'econo_disadvantage',
    'disabil_student', 'primary_disabil_type', 'staff_id', 'accomod_ell', 'accomod_freq_breaks',
    'accomod_alt_location', 'accomod_small_group', 'accomod_special_equip', 'accomod_spec_area',
    'accomod_time_day', 'accomod_answer_mask', 'accomod_general_mask', 'accomod_color_contrast',
    'accomod_asl_video', 'accomod_screen_reader', 'accomod_non_screen_reader', 'accomod_close_capt_ela',
    'accomod_braille_ela', 'accomod_answer_rec', 'accomod_braille_resp', 'accomod_calculator',
    'accomod_construct_resp_ela', 'accomod_select_resp_ela', 'accomod_math_resp', 'accomod_monitor_test_resp',
    'accomod_word_predict', 'accomod_native_lang', 'accomod_loud_native_lang', 'accomod_math_rsp',
    'accomod_math_trans_online', 'accomod_w_2_w_dict', 'accomod_extend_time', 'accomod_alt_paper_test',
    'accomod_paper_trans_math', 'accomod_large_print', 'accomod_braille_tactile', 'accomod_text_2_speech',
    'accomod_human_read_sign', 'accomod_uniq', 'accomod_emergency', 'accomod_st_read_aloud',
    'accomod_human_sign_test_direct', 'opt_state_data2', 'opt_state_data3', 'opt_state_data4',
    'opt_state_data5', 'opt_state_data6', 'opt_state_data7', 'opt_state_data8', 'opt_state_data9',
    'opt_state_data10', 'opt_state_data11', 'opt_state_data12', 'poy', 'test_code', 'asmt_subject',
    'pba_form_id', 'eoy_form_id', 'pba_test_uuid', 'eoy_test_uuid', 'sum_score_rec_uuid', 'pba_total_items',
    'pba_attempt_flag', 'eoy_total_items', 'eoy_attempt_flag', 'pba_total_items_attempt',
    'pba_total_items_unit1', 'pba_unit1_items_attempt', 'pba_total_items_unit2', 'pba_unit2_items_attempt',
    'pba_total_items_unit3', 'pba_unit3_items_attempt', 'pba_total_items_unit4', 'pba_unit4_items_attempt',
    'pba_total_items_unit5', 'pba_unit5_items_attempt', 'eoy_total_items_attempt', 'eoy_total_items_unit1',
    'eoy_unit1_items_attempt', 'eoy_total_items_unit2', 'eoy_unit2_items_attempt', 'eoy_total_items_unit3',
    'eoy_unit3_items_attempt', 'eoy_total_items_unit4', 'eoy_unit4_items_attempt', 'eoy_total_items_unit5',
    'eoy_unit5_items_attempt', 'pba_not_tested_reason', 'eoy_not_tested_reason', 'pba_void_reason',
    'eoy_void_reason', 'pba_raw_score', 'eoy_raw_score', 'sum_scale_score', 'sum_csem', 'sum_perf_lvl',
    'sum_read_scale_score', 'sum_read_csem', 'sum_write_scale_score', 'sum_write_csem', 'subclaim1_category',
    'subclaim2_category', 'subclaim3_category', 'subclaim4_category', 'subclaim5_category',
    'subclaim6_category', 'state_growth_percent', 'district_growth_percent', 'parcc_growth_percent', 'year',
    'asmt_grade', 'include_in_parcc', 'include_in_state', 'include_in_district', 'include_in_school',
    'include_in_roster', 'include_in_isr', 'batch_guid', 'rec_status', 'create_date', 'status_change_date')

#############################################################################
#                   CONSTANTS REQUIRED FOR TRANSLATION                      #
#############################################################################


class Constants:
    FALL_2015 = 'fall-2015'
    OLD_FORMAT = 'old format'
    UNSUPPORTED_OR_BROKEN_FORMAT = 'unsupported_or_broken'
    FILE_SUFFIX = '_fall_2015'
    EMPTY_STRING = ''
    DEPRECATED_COLUMNS_DB = ('accomod_tactile_graph',
                             'accomod_text_2_speech_ela',
                             'accomod_text_2_speech_math',
                             'accomod_504',
                             'accomod_read_ela',
                             'accomod_read_math',
                             'accomod_math_text_2_speech',
                             'accomod_ind_ed')
    SUPPORTED_TABLES = ('rpt_ela_sum', 'rpt_math_sum')

#############################################################################
#                          TRANSFORMATION RULES                             #
#############################################################################


def transform_admin_direct_in_student_language(value: str)-> str:
    """
    Check if value match regular expression, if yes apply changes otherwise return unmodified value
    regex: "^(OralScriptReadbyTestAdministrator([A-Z]{3}))|(HumanTranslator)$"

    group1: match words that started with "OralScriptReadbyTestAdministrator" and
            have 3 more letters in capital cases
    group2: match word "HumanTranslator"

    :param value: value to be processed
    :return: updated value
    """
    group1 = 'OralScriptReadbyTestAdministrator([A-Z]{3})'
    group2 = 'HumanTranslator'
    mask = re.compile('^({gr1})|({gr2})$'.format(gr1=group1, gr2=group2))

    def rules(matchobj):
        """
        regex checker, check matches and return modified value according to the matched group
        :param matchobj: compiled regex object
        :return: str, modified value
        """
        match = matchobj.group(0)
        if match == group2:
            return 'HT'
        else:
            return match[-3:]

    value = re.sub(mask, rules, value)
    if len(value) > 3:
        value = value[:3]
    return value


def transform_grade_level_when_assessed(value: str)-> str:
    """  Check if value is in rules or keep_it, if yes - apply changes, otherwise return '' """
    rules = {
        'Other': '99',
        'OutOfSchool': 'OS',
    }
    keep_it = ('02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13')

    if value in rules:
        return rules[value]
    elif value in keep_it:
        return value
    else:
        # This field can't be empty, so for other grades use 'Other'
        return rules['Other']


def transform_color_contrast(value: str)-> str:
    """ Check if value is in rules, if yes - apply changes, otherwise return '' """
    rules = {
        'black-cream': '01',
        'black-lblue': '02',
        'black-lmagenta': '03',
        'white-black': '04',
        'lblue-dblue': '05',
        'dgray-pgreen': '06',
        'ColorOverlay': '07'
    }
    return rules[value] if value in rules else Constants.EMPTY_STRING


def transform_response(value: str)-> str:
    """ Check if value is in rules, if yes - apply changes, otherwise return '' """
    rules = {
        'SpeechToText': '01',
        'HumanScribe': '02',
        'HumanSigner': '03',
        'ExternalATDevice': '04',
    }
    return rules[value] if value in rules else Constants.EMPTY_STRING


def transform_braille_response(value: str)-> str:
    """ Check if value is in rules, if yes - apply changes, otherwise return '' """
    rules = {
        'BrailleWriter': '01',
        'BrailleNotetaker': '02'
    }
    return rules[value] if value in rules else Constants.EMPTY_STRING


def transform_human_reader_or_writer(value: str)-> str:
    """ Check if value is in rules, if yes - apply changes, otherwise return '' """
    rules = {
        'HumanSigner': '01',
        'HumanReadAloud': '02'
    }
    return rules[value] if value in rules else Constants.EMPTY_STRING


def transform_math_response_el(value: str)-> str:
    """ Value should have length <= 2 """
    return value[:2]


def transform_student_with_disabilities(value: str)-> str:
    """ Check if value is 'Y' or 'N', if yes return value, otherwise return '' """
    return value if value in ('Y', 'N') else Constants.EMPTY_STRING


def transform_braille_tactile_graphics(value: str)-> str:
    """ Check if value is 'Y', if yes return value, otherwise return '' """
    return value if value == 'Y' else Constants.EMPTY_STRING


TRANSFORMATION_MAP_DB = {
    'accomod_math_resp': transform_response,
    'accomod_construct_resp_ela': transform_response,
    'accomod_select_resp_ela': transform_response,
    'accomod_color_contrast': transform_color_contrast,
    'accomod_braille_resp': transform_braille_response,
    'accomod_human_read_sign': transform_human_reader_or_writer,
    'student_grade': transform_grade_level_when_assessed,
    'disabil_student': transform_student_with_disabilities,
    'accomod_braille_tactile': transform_braille_tactile_graphics,
    'accomod_loud_native_lang': transform_admin_direct_in_student_language,
    'accomod_math_rsp': transform_math_response_el}


#############################################################################
#                       MAIN TRANSFORMATION LOGIC                           #
#############################################################################


def main(args):
    """ Run translation script for each file """
    files_for_translation = set()
    if args.csv_file and os.path.isfile(args.csv_file):
        files_for_translation.add(os.path.abspath(args.csv_file))

    if args.csv_list:
        files_for_translation.update(
            {os.path.abspath(fname.strip()) for fname in args.csv_list if os.path.isfile(fname.strip())})

    if args.search_files:
        files_for_translation.update(search_files(args.search_files))

    for fname in files_for_translation:
        logger.info('\n---------------------------------------------')
        logger.info('Process file: {}'.format(fname))
        run_translation(fname, args)


def required_translation(fname: str)-> bool:
    """
    For now only 'rpt_ela_sum' and 'rpt_math_sum' supported, check if table is in the list of supported
    :param fname: filename of csv file
    :return: bool
    """
    basename = get_basename(fname)
    return True if basename in Constants.SUPPORTED_TABLES else False


def run_translation(filename: str, args: argparse.Namespace):
    """
    Main translation function. First we check file format, if file in format 'fall-2015' -
    skip transformation. Otherwise apply transformation rules. For this we create temporary file
    to store new data and after transformation replace original file by new.
    :param filename: file for transformation
    :param args: script arguments
    """
    create_new = args.create_new
    # if filename is not in the list of files that required translation - just insert this file to the DB
    if not required_translation(filename):
        upload_data_from_csv(filename, args.username, args.password)
        return

    csv_version = get_file_version(filename)
    if csv_version == Constants.FALL_2015:
        logger.info('File is already in format "fall-2015"')
        upload_data_from_csv(filename, args.username, args.password)
        return
    elif csv_version == Constants.UNSUPPORTED_OR_BROKEN_FORMAT:
        logger.info('Validation failed for file: {}'.format(filename))
        return
    temporary_filename = None
    try:
        with NamedTemporaryFile(mode='w', delete=False, suffix='_tmp_transform') as tempfile:
            temporary_filename = tempfile.name
            c_writer = csv.DictWriter(tempfile, SUMMATIVE_DB_SCHEMA_NEW_FORMAT)
            c_writer.writeheader()
            for row in translate_csv(filename):
                c_writer.writerow(row)

        new_filename = get_filename(filename, create_new)
        shutil.move(tempfile.name, new_filename)
        os.chmod(new_filename, 0o644)   # give read access for file

        logger.info('[SUCESS] Finished transformation for file: {filename}'.format(filename=filename))
        upload_data_from_csv(new_filename, args.username, args.password)
    except Exception as e:
        logger.info('[FAIL] Failed transformation for file: {filename}'.format(filename=filename))
        raise e
    finally:
        # In case of error delete tmp file
        if os.path.exists(temporary_filename):
            os.remove(temporary_filename)


def upload_data_from_csv(filename, username, password):
    """
    Upload data from csv file to the database
    :param filename: str, filename with the data
    :param login: db login, 'edware' by default
    :param password: db login, 'edware2013' by default
    """
    schema = 'edware_prod'  # hardcoded, change in case we'll need to load data to some other schema
    table = get_tablename_from_filename(filename)
    db_conn = get_db_conn(username, password)
    mask = 'psql {db_conn} -c "copy {schema}.{table} from \'{filename}\' ' \
           'WITH DELIMITER \',\' HEADER CSV"'
    command = mask.format(db_conn=db_conn, schema=schema, table=table, filename=filename)
    try:
        logger.info('Starting csv upload process for table: {table}'.format(table=table))
        status = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True).decode('utf-8')
        logger.info(status)
        logger.info('[SUCESS] CSV uploaded for table: {table}'.format(table=table))

    except subprocess.CalledProcessError as e:
        logger.error(e.output.decode('utf-8'))
        logger.error("Possibly you're uploading data to the non empty table!")


def get_db_conn(username: str, password: str)-> str:
    """
    Create token for db connection
    :param username: db username, 'edware' by default
    :param password: db login, 'edware2013' by default
    :return: str, db_conn token in format: "postgres://edware:edware2013@[host_ip]:5432/edware"
    """
    port = '5432'
    database = 'edware'
    host = get_host_ip()
    db_conn_mask = "postgres://{username}:{password}@{host}:{port}/{database}"
    return db_conn_mask.format(username=username, password=password, host=host,
                               port=port, database=database)


def get_host_ip()-> str:
    """
    helper method, required to get host ip for DB connection by token
    :return: str, host ip address
    """
    command = "ip route get 1 | awk '{print $NF;exit}'"
    return subprocess.getoutput(command)


def get_tablename_from_filename(filename: str)-> str:
    """
    strip extension and suffix '_fall_2015' from filename
    :param filename: str, full path to the file
    :return: str, table title
    """
    basename = get_basename(filename)
    if basename.endswith(Constants.FILE_SUFFIX):
        table_name = basename[:len(basename) - len(Constants.FILE_SUFFIX)]  # strip suffix '_fall_2015'
    else:
        table_name = basename
    return table_name


def get_filename(filename: str, create_new: bool) -> str:
    """
    Create filename for file with data in new format if 'create_new' parameter is True,
    otherwise return filename without modifications
    :param filename: string, path and filename of the source
    :param create_new: bool, if True create
    :return: new_filename
    """
    if create_new:
        path, extension = os.path.splitext(filename)
        new_filename = ''.join([path, Constants.FILE_SUFFIX, extension])
        return new_filename
    else:
        return filename


def get_basename(filename: str)-> str:
    """
    Helper method, split filename and return basename without extension
    :param filename: str, full path to the file
    :return: str, file basename
    """
    return os.path.splitext(os.path.basename(filename))[0]


def search_files(path: str, extension: str='.csv')-> set:
    """
    Search all csv files in some folder and all subfolders
    :param path: path to search
    :param extension: extension by what we'll search files
    :return: set of csv files
    """
    csv_files = set()
    for root, dirs, files in os.walk(path):
        for name in files:
            if name.endswith(extension):
                csv_files.add(os.path.join(root, name))

    return csv_files


def get_file_version(filename: str)-> str:
    """
    read line with headers from file and check version
    :param filename: file for check
    :return: 'fall-2015' in case of new format otherwise 'old format'
    """
    with open(filename) as cfile:
        c_reader = csv.reader(cfile)
        headers = next(c_reader)
        if validate_schema_format(headers, SUMMATIVE_DB_SCHEMA_NEW_FORMAT):
            version = Constants.FALL_2015
        elif validate_schema_format(headers, SUMMATIVE_DB_SCHEMA_OLD_FORMAT):
            version = Constants.OLD_FORMAT
        else:
            version = Constants.UNSUPPORTED_OR_BROKEN_FORMAT
        return version


def validate_schema_format(headers: list, validation_schema: tuple) -> bool:
    """
    Check if file for processing is valid
    :param headers: headers of the file
    :param validation_schema: schema for validation
    :return: bool
    """
    return set(headers) == set(validation_schema)


def translate_csv(csv_file: str):
    """
    Translate data from old csv format to fal-2015, steps:
    1. Delete deprecated columns
    2. Transform data in to new format
    3. Insert new columns according to new format, cells are empty
    :param csv_file: csv file in old format
    :return: yield file row by row
    """
    logger.info('Started transformation for file: {filename}'.format(filename=csv_file))
    with open(csv_file) as cfile:
        c_reader = csv.DictReader(cfile)
        for row in c_reader:
            _delete_deprecated_columns(row)
            _transform_row_values(row)
            _insert_new_columns(row)
            yield row


def _delete_deprecated_columns(row: dict):
    """
    Delete deprecated columns from row
    :param row: row for transformation
    :return: We don't need to return anything cause 'del' mutates the original dict
    """
    for column_name in Constants.DEPRECATED_COLUMNS_DB:
        del row[column_name]


def _transform_row_values(row: dict):
    """
    Iterate over transformation rules and apply each of them to the row
    :param row: data for tranformation
    :return: We don't need to return anything cause changes mutates the original dict
    """
    for column_name, rule in TRANSFORMATION_MAP_DB.items():
        row[column_name] = rule(row[column_name])


def _insert_new_columns(row: dict):
    """
    Insert new columns according to new schema
    :param row: row for transformation
    :return: We don't need to return anything cause changes mutates the original dict
    """
    row.update({col_name: Constants.EMPTY_STRING for col_name in SUMMATIVE_DB_SCHEMA_NEW_FORMAT
                if col_name not in row})


def args_parser() -> argparse.Namespace:
    parser = argparse.ArgumentParser('csv_translator')
    parser.description = 'Convert summative files from old format to "fall-2015"'
    parser.add_argument('-c', '--csv-file', dest='csv_file', nargs='?', required=False,
                        help='path to csv file in db dump format')
    parser.add_argument('-l', '--csv-list-in-file', dest='csv_list', required=False,
                        type=argparse.FileType('r'), help='file with a list of csv files for translation')
    parser.add_argument('-s', '--search', dest='search_files', required=False,
                        help='if specified search all files by ".csv" mask in folder and all subfolders')
    parser.add_argument('-n', '--create-new', dest='create_new', action='store_true', required=False,
                        help='If specified, don\'t replace original file, '
                             'create new with suffix "_fall_2015" in the same directory')
    parser.add_argument('-u', '--user', dest='username', required=False, default='edware',
                        help='DB connection username')
    parser.add_argument('-p', '--password', dest='password', required=False, default='edware2013',
                        help='DB connection password')

    if len(sys.argv[1:]) == 0:
        logger.info('You should specify at least one argument!')
        parser.print_help()
        sys.exit(1)

    return parser.parse_args()


if __name__ == "__main__":
    main(args_parser())
