Name:		starmigrate%(echo ${PARCC_ENV_NAME:=""})
Summary:	PARCC Analytics Migration Application
Version:	%(echo ${RPM_VERSION:="X.X"})
Release:	%(echo ${BUILD_NUMBER:="X"})%{?dist}

Group:		ETL pipeline
License:	Proprietary software
URL:		http://www.amplify.com
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

Requires: pentaho-kettle
AutoReqProv: no

%define _unpackaged_files_terminate_build 0

%description
EdWare StarMigrate
commit: %(echo ${GIT_COMMIT:="UNKNOWN"})


%prep
rm -rf virtualenv/starmigrate

%build
export LANG=en_US.UTF-8
virtualenv-3.3 --distribute virtualenv/starmigrate
source virtualenv/starmigrate/bin/activate

cp ${WORKSPACE}/scripts/repmgr_cleanup.sh virtualenv/starmigrate/bin/

cd ${WORKSPACE}/config
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edworker
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edschema
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edcore
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edapi
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edreplicate
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/starmigrate
python setup.py clean --all
python setup.py install
cd -

deactivate
find virtualenv/starmigrate/bin -type f -exec sed -i 's/\/var\/lib\/jenkins\/rpmbuild\/BUILD/\/opt/g' {} \;


%install
rm -rf %{buildroot}

mkdir -p %{buildroot}/opt/virtualenv
cp -r virtualenv/starmigrate %{buildroot}/opt/virtualenv

mkdir -p %{buildroot}/opt/edware
cp -r ${WORKSPACE}/scripts %{buildroot}/opt/edware
mkdir -p %{buildroot}/opt/edware/conf
mkdir -p %{buildroot}/opt/edware/log
mkdir -p %{buildroot}/etc/rc.d/init.d

cp ${WORKSPACE}/config/generate_ini.py %{buildroot}/opt/edware/conf/
cp ${WORKSPACE}/config/settings.yaml %{buildroot}/opt/edware/conf/
cp ${WORKSPACE}/starmigrate/config/linux/opt/edware/conf/celeryd-starmigrate.conf %{buildroot}/opt/edware/conf/
cp ${WORKSPACE}/starmigrate/config/linux/etc/rc.d/init.d/celeryd-starmigrate %{buildroot}/etc/rc.d/init.d/
cp ${WORKSPACE}/starmigrate/config/linux/etc/rc.d/init.d/star-migrate-conductor %{buildroot}/etc/rc.d/init.d/

# KETTLE ETL
mkdir -p %{buildroot}/opt/pentaho/edware-analytics
cp -r ${WORKSPACE}/starmigrate/ETL %{buildroot}/opt/pentaho/edware-analytics/ETL

cat <<EOF > %{buildroot}/opt/pentaho/run_load.sh
#!/bin/bash
/opt/pentaho/edware-analytics/ETL/kitchen.sh -file=/opt/pentaho/edware-analytics/ETL/jb_load.kjb -param:INI_FILE=/opt/edware/conf/smarter.ini -param:summative_csv_dir=/vol/migrate_dbdump/
EOF


%clean


%files
%defattr(644,root,root,755)
/opt/edware/conf/generate_ini.py
/opt/edware/conf/settings.yaml
/opt/edware/conf/celeryd-starmigrate.conf
/opt/virtualenv/starmigrate/include/*
/opt/virtualenv/starmigrate/lib/*
/opt/virtualenv/starmigrate/bin/activate
%attr(755,root,root) /opt/virtualenv/starmigrate/bin/activate
%attr(755,root,root) /opt/virtualenv/starmigrate/bin/camqadm
%attr(755,root,root) /opt/virtualenv/starmigrate/bin/celery
%attr(755,root,root) /opt/virtualenv/starmigrate/bin/celerybeat
%attr(755,root,root) /opt/virtualenv/starmigrate/bin/celeryctl
%attr(755,root,root) /opt/virtualenv/starmigrate/bin/celeryd
%attr(755,root,root) /opt/virtualenv/starmigrate/bin/celeryd-multi
%attr(755,root,root) /opt/virtualenv/starmigrate/bin/celeryev
%attr(755,root,root) /opt/virtualenv/starmigrate/bin/easy_install
%attr(755,root,root) /opt/virtualenv/starmigrate/bin/easy_install-3.3
%attr(755,root,root) /opt/virtualenv/starmigrate/bin/pip
%attr(755,root,root) /opt/virtualenv/starmigrate/bin/pip3
%attr(755,root,root) /opt/virtualenv/starmigrate/bin/pip3.3
%attr(755,root,root) /opt/virtualenv/starmigrate/bin/python3.3
%dir %attr(777,root,root) /opt/edware/log
/opt/virtualenv/starmigrate/bin/python
/opt/virtualenv/starmigrate/bin/python3
%attr(755,root,root) /opt/virtualenv/starmigrate/bin/repmgr_cleanup.sh
%attr(755,root,root) /etc/rc.d/init.d/celeryd-starmigrate
%attr(755,root,root) /etc/rc.d/init.d/star-migrate-conductor

%attr(755,root,root) /opt/pentaho/edware-analytics/ETL/*
%attr(755,root,root) /opt/pentaho/run_load.sh

%pre
id celery > /dev/null 2>&1
if [ $? != 0 ]; then
   useradd celery
   usermod -G fuse celery
fi
if [ ! -d /opt/edware/log ]; then
    mkdir -p /opt/edware/log
fi
if [ ! -d /var/log/celery-starmigrate ]; then
    mkdir -p /var/log/celery-starmigrate
    chown celery.celery /var/log/celery-starmigrate
fi

%post
chkconfig --add celeryd-starmigrate
chkconfig --add star-migrate-conductor
chkconfig --level 2345 celeryd-starmigrate off
chkconfig --level 2345 star-migrate-conductor off

%preun
chkconfig --del celeryd-starmigrate
chkconfig --del star-migrate-conductor

%postun


%changelog
