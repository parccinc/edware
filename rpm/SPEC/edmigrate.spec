Name:		edmigrate%(echo ${PARCC_ENV_NAME:=""})
Version:	%(echo ${RPM_VERSION:="X.X"})
Release:	%(echo ${BUILD_NUMBER:="X"})%{?dist}
Summary:	PARCC Migration Application

Group:		ETL pipeline
License:	Proprietary software
URL:		http://www.amplify.com
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

AutoReqProv: no

%define _unpackaged_files_terminate_build 0

%description
EdWare EdMigrate
commit: %(echo ${GIT_COMMIT:="UNKNOWN"})


%prep
rm -rf virtualenv/edmigrate

%build
export LANG=en_US.UTF-8
virtualenv-3.3 --distribute virtualenv/edmigrate
source virtualenv/edmigrate/bin/activate

cp ${WORKSPACE}/scripts/repmgr_cleanup.sh virtualenv/edmigrate/bin/

cd ${WORKSPACE}/config
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edcore
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edschema
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edapi
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edworker
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edmigrate
python setup.py clean --all
python setup.py install
cd -


deactivate
echo -e "/opt/edware/smarter\n." > virtualenv/edmigrate/lib/python3.3/site-packages/smarter.egg-link
find virtualenv/edmigrate/bin -type f -exec sed -i 's/\/var\/lib\/jenkins\/rpmbuild\/BUILD/\/opt/g' {} \;

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}/opt/virtualenv
cp -r virtualenv/edmigrate %{buildroot}/opt/virtualenv

mkdir -p %{buildroot}/opt/edware
cp -r ${WORKSPACE}/scripts %{buildroot}/opt/edware
mkdir -p %{buildroot}/opt/edware/conf
mkdir -p %{buildroot}/opt/edware/log
mkdir -p %{buildroot}/etc/rc.d/init.d
cp ${WORKSPACE}/config/generate_ini.py %{buildroot}/opt/edware/conf/
cp ${WORKSPACE}/config/settings.yaml %{buildroot}/opt/edware/conf/
cp ${WORKSPACE}/edmigrate/config/linux/opt/edware/conf/celeryd-edmigrate.conf %{buildroot}/opt/edware/conf/
cp ${WORKSPACE}/edmigrate/config/linux/etc/rc.d/init.d/celeryd-edmigrate %{buildroot}/etc/rc.d/init.d/
cp ${WORKSPACE}/edmigrate/config/linux/etc/rc.d/init.d/edmigrate-conductor %{buildroot}/etc/rc.d/init.d/


%clean


%files
%defattr(644,root,root,755)
/opt/edware/conf/generate_ini.py
/opt/edware/conf/settings.yaml
/opt/edware/conf/celeryd-edmigrate.conf
/opt/virtualenv/edmigrate/include/*
/opt/virtualenv/edmigrate/lib/*
/opt/virtualenv/edmigrate/bin/activate
/opt/virtualenv/edmigrate/bin/activate.csh
/opt/virtualenv/edmigrate/bin/activate.fish
/opt/virtualenv/edmigrate/bin/activate_this.py
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/bfg2pyramid
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/easy_install
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/easy_install-3.3
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/mako-render
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/pcreate
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/pip
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/pip3.3
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/prequest
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/proutes
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/pserve
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/pshell
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/ptweens
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/pviews
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/python3.3
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/celery
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/celery
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/celerybeat
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/celeryctl
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/celeryd
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/celeryd-multi
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/celeryev
%dir %attr(777,root,root) /opt/edware/log
/opt/virtualenv/edmigrate/bin/python
/opt/virtualenv/edmigrate/bin/python3
%attr(755,root,root) /opt/virtualenv/edmigrate/bin/repmgr_cleanup.sh
%attr(755,root,root) /etc/rc.d/init.d/celeryd-edmigrate
%attr(755,root,root) /etc/rc.d/init.d/edmigrate-conductor

%pre
id celery > /dev/null 2>&1
if [ $? != 0 ]; then
   useradd celery
   usermod -G fuse celery
fi
if [ ! -d /opt/edware/log ]; then
    mkdir -p /opt/edware/log
fi
if [ ! -d /var/log/celery-edmigrate ]; then
    mkdir -p /var/log/celery-edmigrate
    chown celery.celery /var/log/celery-edmigrate
fi

%post
chkconfig --add celeryd-edmigrate
chkconfig --add edmigrate-conductor
chkconfig --level 2345 celeryd-edmigrate off
chkconfig --level 2345 edmigrate-conductor off

%preun
chkconfig --del celeryd-edmigrate
chkconfig --del edmigrate-conductor

%postun


%changelog

