Name:		edtests%(echo ${TEST_ENV_NAME:=""})
Version:	%(echo ${RPM_VERSION:="X.X"})
Release:	%(echo ${BUILD_NUMBER:="X"})%{?dist}
Summary:	EdWare Tests for Application

Group:		TESTS
License:	Proprietary software
URL:		http://www.amplify.com
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

AutoReqProv: no

%define _unpackaged_files_terminate_build 0

%description
EdWare Tests
commit: %(echo ${GIT_COMMIT:="UNKNOWN"})

%prep
rm -rf virtualenv/edtests

%build
export LANG=en_US.UTF-8
virtualenv-3.3 --distribute virtualenv/edtests
source virtualenv/edtests/bin/activate

cd ${WORKSPACE}/component_tests
python setup.py clean --all
python setup.py install
cd -

deactivate
find virtualenv/edtests/bin -type f -exec sed -i 's/\/var\/lib\/jenkins\/rpmbuild\/BUILD/\/opt/g' {} \;

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/opt/edware
cp -r ${WORKSPACE}/component_tests %{buildroot}/opt/edware
mkdir -p %{buildroot}/opt/virtualenv
cp -r virtualenv/edtests %{buildroot}/opt/virtualenv

%clean

%files
%defattr(644,root,root,755)
/opt/virtualenv/edtests/include/*
/opt/virtualenv/edtests/lib/*
/opt/virtualenv/edtests/bin/activate
/opt/virtualenv/edtests/bin/activate.csh
/opt/virtualenv/edtests/bin/activate.fish
/opt/virtualenv/edtests/bin/activate_this.py
/opt/edware/component_tests/e2e/*
/opt/edware/component_tests/behave_wrapper.py
%attr(755,root,root) /opt/edware/component_tests/generate_ini_for_tests.sh
%attr(755,root,root) /opt/virtualenv/edtests/bin/easy_install
%attr(755,root,root) /opt/virtualenv/edtests/bin/easy_install-3.3
%attr(755,root,root) /opt/virtualenv/edtests/bin/behave
%attr(755,root,root) /opt/virtualenv/edtests/bin/pip
%attr(755,root,root) /opt/virtualenv/edtests/bin/pip3
%attr(755,root,root) /opt/virtualenv/edtests/bin/pip3.3
%attr(755,root,root) /opt/virtualenv/edtests/bin/python3.3
/opt/virtualenv/edtests/bin/python
/opt/virtualenv/edtests/bin/python3

%pre

%post

%preun

%postun

%changelog
