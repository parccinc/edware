Name:		reporting%(echo ${REPORTING_ENV_NAME:=""})
Version:	%(echo ${RPM_VERSION:="X.X"})
Release:	%(echo ${BUILD_NUMBER:="X"})%{?dist}
Summary:	REPORTING EdWare Reporting Web Application

Group:		WSGI Web Application
License:	Proprietary software
URL:		http://www.amplify.com
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

AutoReqProv: no

%define _unpackaged_files_terminate_build 0

%description
EdWare Reporting App
commit: %(echo ${GIT_COMMIT:="UNKNOWN"})


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/opt/edware
cp -r ${WORKSPACE}/reporting %{buildroot}/opt/edware
cp -r ${WORKSPACE}/scripts %{buildroot}/opt/edware
cp -r ${WORKSPACE}/assets %{buildroot}/opt/edware/assets
touch %{buildroot}/opt/edware/assets/__init__.py
mkdir -p %{buildroot}/opt/edware/conf
mkdir -p %{buildroot}/opt/edware/log
mkdir -p %{buildroot}/etc/rc.d/init.d
cp ${WORKSPACE}/config/generate_ini.py %{buildroot}/opt/edware/conf/
cp ${WORKSPACE}/config/settings.yaml %{buildroot}/opt/edware/conf/
cp ${WORKSPACE}/config/comparing_populations_precache_filters.json %{buildroot}/opt/edware/conf/
cp ${WORKSPACE}/services/config/linux/opt/edware/conf/celeryd-services.conf %{buildroot}/opt/edware/conf/
cp ${WORKSPACE}/services/config/linux/etc/rc.d/init.d/celeryd-services %{buildroot}/etc/rc.d/init.d/
cp ${WORKSPACE}/edextract/config/linux/opt/edware/conf/celeryd-edextract.conf %{buildroot}/opt/edware/conf/
cp ${WORKSPACE}/edextract/config/linux/etc/rc.d/init.d/celeryd-edextract %{buildroot}/etc/rc.d/init.d/

export LANG=en_US.UTF-8
virtualenv-3.3 --distribute virtualenv/reporting
source virtualenv/reporting/bin/activate

cd ${WORKSPACE}/scripts
BUILDROOT=%{buildroot}
WORKSPACE_PATH=${BUILDROOT//\//\\\/}
sed -i.bak "s/assets.directory = \/path\/assets/assets.directory = ${WORKSPACE_PATH}\/opt\/edware\/assets/g" compile_assets.ini

python compile_assets.py
cd -

cd ${WORKSPACE}/config
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edcore
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edschema
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edauth
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edapi
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edworker
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/services
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edextract
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/parcc_common
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/hpz_client
python setup.py clean --all
python setup.py install
cd -
cd %{buildroot}/opt/edware/reporting
rm -rf assets
mv ../assets .
python setup.py clean --all
python setup.py install
cd -

deactivate
echo -e "/opt/edware/reporting\n." > virtualenv/reporting/lib/python3.3/site-packages/reporting.egg-link
find virtualenv/reporting/bin -type f -exec sed -i 's/\/var\/lib\/jenkins\/rpmbuild\/BUILD/\/opt/g' {} \;

find %{buildroot}/opt/edware/reporting/assets -type f -exec sed -i 's:%{buildroot}::g' {} \;

mkdir -p %{buildroot}/opt/virtualenv
cp -r virtualenv/reporting %{buildroot}/opt/virtualenv


%clean
rm -rf virtualenv/reporting


%files
%defattr(644,root,root,755)
/opt/edware/reporting/reporting.wsgi
/opt/edware/conf/generate_ini.py
/opt/edware/conf/settings.yaml
/opt/edware/conf/comparing_populations_precache_filters.json
/opt/edware/conf/celeryd-services.conf
/opt/edware/conf/celeryd-edextract.conf
/opt/virtualenv/reporting/include/*
/opt/virtualenv/reporting/lib/*
/opt/virtualenv/reporting/bin/activate
/opt/virtualenv/reporting/bin/activate.csh
/opt/virtualenv/reporting/bin/activate.fish
/opt/virtualenv/reporting/bin/activate_this.py
%attr(755,root,root) /opt/virtualenv/reporting/bin/bfg2pyramid
%attr(755,root,root) /opt/virtualenv/reporting/bin/easy_install
%attr(755,root,root) /opt/virtualenv/reporting/bin/easy_install-3.3
%attr(755,root,root) /opt/virtualenv/reporting/bin/initialize_reporting_db
%attr(755,root,root) /opt/virtualenv/reporting/bin/mako-render
%attr(755,root,root) /opt/virtualenv/reporting/bin/pcreate
%attr(755,root,root) /opt/virtualenv/reporting/bin/pip
%attr(755,root,root) /opt/virtualenv/reporting/bin/pip3.3
%attr(755,root,root) /opt/virtualenv/reporting/bin/prequest
%attr(755,root,root) /opt/virtualenv/reporting/bin/proutes
%attr(755,root,root) /opt/virtualenv/reporting/bin/pserve
%attr(755,root,root) /opt/virtualenv/reporting/bin/pshell
%attr(755,root,root) /opt/virtualenv/reporting/bin/ptweens
%attr(755,root,root) /opt/virtualenv/reporting/bin/pviews
%attr(755,root,root) /opt/virtualenv/reporting/bin/pygmentize
%attr(755,root,root) /opt/virtualenv/reporting/bin/python3.3
%attr(755,root,root) /opt/virtualenv/reporting/bin/celery
%attr(755,root,root) /opt/virtualenv/reporting/bin/celery
%attr(755,root,root) /opt/virtualenv/reporting/bin/celerybeat
%attr(755,root,root) /opt/virtualenv/reporting/bin/celeryctl
%attr(755,root,root) /opt/virtualenv/reporting/bin/celeryd
%attr(755,root,root) /opt/virtualenv/reporting/bin/celeryd-multi
%attr(755,root,root) /opt/virtualenv/reporting/bin/celeryev
%dir %attr(777,root,root) /opt/edware/log
/opt/virtualenv/reporting/bin/python
/opt/virtualenv/reporting/bin/python3
%attr(755,root,root) /etc/rc.d/init.d/celeryd-services
%attr(755,root,root) /etc/rc.d/init.d/celeryd-edextract

%pre
id celery > /dev/null 2>&1
if [ $? != 0 ]; then
   useradd celery
   usermod -G fuse celery
fi
if [ ! -d /opt/edware/log ]; then
    mkdir -p /opt/edware/log
fi
if [ ! -d /var/log/celery-services ]; then
    mkdir -p /var/log/celery-services
    chown celery.celery /var/log/celery-services
fi
if [ ! -d /var/log/celery-edextract ]; then
    mkdir -p /var/log/celery-edextract
    chown celery.celery /var/log/celery-edextract
fi

%post
chkconfig --add celeryd-services
chkconfig --add celeryd-edextract
chkconfig --level 2345 celeryd-services off
chkconfig --level 2345 celeryd-edextract off

%preun
chkconfig --del celeryd-services
chkconfig --del celeryd-edextract

%postun


%changelog
