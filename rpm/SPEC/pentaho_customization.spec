Name:		pentaho_customization%(echo ${PARCC_ENV_NAME:=""})
Summary:	Pentaho customization package
Version:	%(echo ${RPM_VERSION:="X.X"})
Release:	%(echo ${BUILD_NUMBER:="X"})%{?dist}

Group:		ETL pipeline
License:	Proprietary software
URL:		http://www.amplify.com
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

Requires:   pentaho-biserver

AutoReqProv: no

%define _unpackaged_files_terminate_build 0

%define mvn_target_root pentaho_customization/pentaho-sso/target
%define analytics_root opt/pentaho/edware-analytics
%define fake_root %{analytics_root}/fake-root
%define pentaho_root opt/pentaho/biserver-ce
%define tomcat_root %{pentaho_root}/tomcat
%define system_root %{pentaho_root}/pentaho-solutions/system
%define web_inf_root %{tomcat_root}/webapps/pentaho/WEB-INF
%define lib_root %{web_inf_root}/lib
%define messages_root %{web_inf_root}/classes/com/pentaho/messages
%define mantle_image_root %{tomcat_root}/webapps/pentaho/mantle/themes/crystal/images

%description
Contains customizations made to Pentaho
commit: %(echo ${GIT_COMMIT:="UNKNOWN"})

%prep
rm -rf ${WORKSPACE}/%{mvn_target_root}

%build
cd ${WORKSPACE}/pentaho_customization/pentaho-sso
mvn clean dependency:copy-dependencies package

cd ${WORKSPACE}/pentaho_customization/content/content-zip
rm -f content.zip
zip -r content home

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}/%{analytics_root}
mkdir -p %{buildroot}/%{system_root}/saiku/lib
mkdir -p %{buildroot}/%{lib_root}

cp -r ${WORKSPACE}/pentaho_customization/dist/* %{buildroot}
cp -r ${WORKSPACE}/pentaho_customization/content %{buildroot}/%{analytics_root}/content
cp ${WORKSPACE}/pentaho_customization/content/content-zip/content.zip %{buildroot}/%{analytics_root}/content

# Copy the built JARs and the corresponding dependencies.
cp ${WORKSPACE}/%{mvn_target_root}/pentaho-sso*.jar %{buildroot}/%{system_root}/saiku/lib
cp ${WORKSPACE}/%{mvn_target_root}/pentaho-sso*.jar %{buildroot}/%{lib_root}
cp ${WORKSPACE}/%{mvn_target_root}/dependency/bcprov-jdk15on-*.jar %{buildroot}/%{lib_root}
cp ${WORKSPACE}/%{mvn_target_root}/dependency/not-yet-commons-ssl-*.jar %{buildroot}/%{lib_root}
cp ${WORKSPACE}/%{mvn_target_root}/dependency/opensaml-*.jar %{buildroot}/%{lib_root}
cp ${WORKSPACE}/%{mvn_target_root}/dependency/openws-*.jar %{buildroot}/%{lib_root}
cp ${WORKSPACE}/%{mvn_target_root}/dependency/servlet-api-*.jar %{buildroot}/%{lib_root}
cp ${WORKSPACE}/%{mvn_target_root}/dependency/velocity-*.jar %{buildroot}/%{lib_root}
cp ${WORKSPACE}/%{mvn_target_root}/dependency/xmlsec-*.jar %{buildroot}/%{lib_root}
cp ${WORKSPACE}/%{mvn_target_root}/dependency/xmltooling-*.jar %{buildroot}/%{lib_root}

%files
%defattr(644,pentaho,pentaho,755)
/%{analytics_root}
/%{system_root}/saiku/lib/pentaho-sso*.jar
/%{tomcat_root}/webapps/pentaho/login_failure.css
/%{tomcat_root}/webapps/pentaho/login_failure.jsp
/%{tomcat_root}/webapps/pentaho/PARCC_Ad_Hoc_Analytics.pdf
/%{tomcat_root}/webapps/pentaho/PARCC_Ad_Hoc_Reporting.mp4
/%{mantle_image_root}/16x16_right.png
/%{mantle_image_root}/arrow-dark-small.png
/%{mantle_image_root}/arrow-light-small.png
/%{mantle_image_root}/dropdown_arrow_blue.png
/%{mantle_image_root}/dropdown_arrow_gray.png
/%{mantle_image_root}/grey_triangle_right.png
/%{mantle_image_root}/logo.png
/%{mantle_image_root}/parcc/*
/%{mantle_image_root}/seperator_horz.png
/%{mantle_image_root}/seperator_vert.png
/%{messages_root}/MondrianMessages.properties
/%{lib_root}/pentaho-sso*.jar
/%{lib_root}/bcprov-jdk15on-*.jar
/%{lib_root}/not-yet-commons-ssl-*.jar
/%{lib_root}/opensaml-*.jar
/%{lib_root}/openws-*.jar
/%{lib_root}/servlet-api-*.jar
/%{lib_root}/velocity-*.jar
/%{lib_root}/xmlsec-*.jar
/%{lib_root}/xmltooling-*.jar

%clean

%pre

%post
# Retain the old path for compatibility with devops.
if [[ ! -e /%{analytics_root}/Content ]]
then
    ln -s /%{analytics_root}/content /%{analytics_root}/Content
fi

cd /%{fake_root}
for file in `find . -type f`; do
    if [[ -e /${file} ]]
    then
        mv -f /${file} /${file}.orig
    fi
    cp -f ${file} /${file}
done

%preun
cd /%{fake_root}
for file in `find . -type f`; do
    if [[ -e /${file}.orig ]]
    then
        mv -f /${file}.orig /${file}
    fi
done

%changelog
