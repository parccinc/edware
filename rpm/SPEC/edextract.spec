Name:		edextract%(echo ${EDEXTRACT_ENV_NAME:=""})
Version:	%(echo ${RPM_VERSION:="X.X"})
Release:	%(echo ${BUILD_NUMBER:="X"})%{?dist}
Summary:	EdWare Extract Application

Group:		ETL pipeline
License:	Proprietary software
URL:		http://www.amplify.com
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

AutoReqProv: no

%define _unpackaged_files_terminate_build 0

%description
EdWare Extract
commit: %(echo ${GIT_COMMIT:="UNKNOWN"})


%prep
rm -rf virtualenv/edextract

%build
export LANG=en_US.UTF-8
virtualenv-3.3 --distribute virtualenv/edextract
source virtualenv/edextract/bin/activate

cd ${WORKSPACE}/config
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edcore
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edschema
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edworker
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edauth
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/hpz_client
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/parcc_common
python setup.py clean --all
python setup.py install
cd -
cd ${WORKSPACE}/edextract
python setup.py clean --all
python setup.py install
cd -

deactivate
find virtualenv/edextract/bin -type f -exec sed -i 's/\/var\/lib\/jenkins\/rpmbuild\/BUILD/\/opt/g' {} \;

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/opt/edware
cp -r ${WORKSPACE}/scripts %{buildroot}/opt/edware
mkdir -p %{buildroot}/opt/edware/conf
mkdir -p %{buildroot}/opt/edware/log
mkdir -p %{buildroot}/etc/rc.d/init.d
cp ${WORKSPACE}/config/generate_ini.py %{buildroot}/opt/edware/conf/
cp ${WORKSPACE}/config/settings.yaml %{buildroot}/opt/edware/conf/
cp ${WORKSPACE}/edextract/config/linux/opt/edware/conf/celeryd-edextract.conf %{buildroot}/opt/edware/conf/
cp ${WORKSPACE}/edextract/config/linux/etc/rc.d/init.d/celeryd-edextract %{buildroot}/etc/rc.d/init.d/
cp -r ${WORKSPACE}/edextract/scripts %{buildroot}/opt/edware/conf
mkdir -p %{buildroot}/opt/virtualenv
cp -r virtualenv/edextract %{buildroot}/opt/virtualenv


%clean


%files
%defattr(644,root,root,755)
/opt/edware/conf/generate_ini.py
/opt/edware/conf/settings.yaml
/opt/edware/conf/celeryd-edextract.conf
/opt/edware/conf/scripts/*
/opt/virtualenv/edextract/include/*
/opt/virtualenv/edextract/lib/*
/opt/virtualenv/edextract/bin/activate
/opt/virtualenv/edextract/bin/activate.csh
/opt/virtualenv/edextract/bin/activate.fish
/opt/virtualenv/edextract/bin/activate_this.py
%attr(755,root,root) /opt/virtualenv/edextract/bin/bfg2pyramid
%attr(755,root,root) /opt/virtualenv/edextract/bin/easy_install
%attr(755,root,root) /opt/virtualenv/edextract/bin/easy_install-3.3
%attr(755,root,root) /opt/virtualenv/edextract/bin/mako-render
%attr(755,root,root) /opt/virtualenv/edextract/bin/pcreate
%attr(755,root,root) /opt/virtualenv/edextract/bin/pip
%attr(755,root,root) /opt/virtualenv/edextract/bin/pip3.3
%attr(755,root,root) /opt/virtualenv/edextract/bin/prequest
%attr(755,root,root) /opt/virtualenv/edextract/bin/proutes
%attr(755,root,root) /opt/virtualenv/edextract/bin/pserve
%attr(755,root,root) /opt/virtualenv/edextract/bin/pshell
%attr(755,root,root) /opt/virtualenv/edextract/bin/ptweens
%attr(755,root,root) /opt/virtualenv/edextract/bin/pviews
%attr(755,root,root) /opt/virtualenv/edextract/bin/python3.3
%attr(755,root,root) /opt/virtualenv/edextract/bin/celery
%attr(755,root,root) /opt/virtualenv/edextract/bin/celerybeat
%attr(755,root,root) /opt/virtualenv/edextract/bin/celeryctl
%attr(755,root,root) /opt/virtualenv/edextract/bin/celeryd
%attr(755,root,root) /opt/virtualenv/edextract/bin/celeryd-multi
%attr(755,root,root) /opt/virtualenv/edextract/bin/celeryev
%dir %attr(777,root,root) /opt/edware/log
/opt/virtualenv/edextract/bin/python
/opt/virtualenv/edextract/bin/python3
%attr(755,root,root) /etc/rc.d/init.d/celeryd-edextract

%pre
id celery > /dev/null 2>&1
if [ $? != 0 ]; then
   useradd celery
   usermod -G fuse celery
fi
if [ ! -d /opt/edware/log ]; then
    mkdir -p /opt/edware/log
fi
if [ ! -d /var/log/celery-edextract ]; then
    mkdir -p /var/log/celery-edextract
    chown celery.celery /var/log/celery-edextract
fi

%post
chkconfig --add celeryd-edextract
chkconfig --level 2345 celeryd-edextract off

%preun
chkconfig --del celeryd-edextract

%postun


%changelog

