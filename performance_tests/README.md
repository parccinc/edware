# PERFORMANCE TESTS FOR REPORTING APP

* **jmeter** contain data and testcases that work with predefined data in csv files.

    * Run steps is [here](https://docs.google.com/document/d/1pDeEYKxuX0b06ujWO3T6EdLWygpBwwze6SPLGfajJJ4/edit#heading=h.vgdtq7)


* **db_testcases** contain testcases that use data from database loadtesting-dw-master-db-server-cat.dev.ae1.parcdc.net

    * Run steps is [here](https://docs.google.com/document/d/1sAiqXW0qfXsATSH1LWvp86BgspLTcsBAfAJprTsWZlc/edit)
    * By default test work with database *loadtesting-dw-master-db-server-cat.dev.ae1.parcdc.net*. If you want use another, change edware connection configuration
    * use *localhost* in edware connection configuration against *loadtesting-dw-master-db-server-cat.dev.ae1.parcdc.net* and local.settings if you want run tests locally
    
    * run tests:
       
        cd <dir with tests>
        jmeter -p ../user.settings -t [tescase.jmx]
