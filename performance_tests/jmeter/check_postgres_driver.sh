#!/bin/bash
# Basic script to check that driver is downloaded. If not, download driver to current directory
# TODO plans:
# 1. Add features to run jmeter with required environment and file, for example:
# check_postgres_driver_exist local db [testsuit]  -> run [testsuit]  with local.properties and data from db
# check_postgres_driver_exist aws csv [testsuit]  -> run [testsuit]  with user.properties and data from csv
# 2. Rename filename if functionality above will be implemented
# 3. Investigate option to run jmeter with predefined driver location (for now we need to set path
# manually from GUI).


DRIVER_FILENAME="postgresql-9.4-1204.jdbc4.jar"
WGET="wget -S --tries=1 --timeout=1 -q -O  https://jdbc.postgresql.org/download/$DRIVER_FILENAME"


if [[ ! -f "$DRIVER_FILENAME" ]]
then
  echo "Driver not found, start downloading driver for postgres"
  eval $WGET
else
  echo "Driver found, please run jmeter"
fi
