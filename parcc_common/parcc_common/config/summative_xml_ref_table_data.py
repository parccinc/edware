__author__ = 'esnyder'

TO_SUBSTRING = 'substr({src_column}, 1, {length})'
TO_DATE = "to_date({src_column}, 'YYYY-MM-DD')"
TO_FIVE_DIGITS = "to_number({src_column}, '99999')"
TO_TWO_DIGITS = "to_number({src_column},'99')"
TO_ONE_DIGIT = "to_number({src_column},'9')"

COLUMNS = ('phase', 'source_table', 'source_column', 'target_table', 'target_column', 'transformation_rule', 'stored_proc_name')


ref_table_conf = {
    'column_definitions': COLUMNS,
    'column_mappings': [
        # Columns:
        # column_map_key, phase, source_table, source_column, target_table, target_column, transformation_rule, stored_proc_name, stored_proc_created_date, create_date
        # XML to loader
        ('1', 'lz_xml', 'asmt_attempt_guid', 'ldr_student_item_score', 'asmt_attempt_guid', 'clean', None),
        ('1', 'lz_xml', 'item_guid', 'ldr_student_item_score', 'item_guid', 'clean', None),
        ('1', 'lz_xml', 'student_guid', 'ldr_student_item_score', 'student_guid', 'clean', None),
        ('1', 'lz_xml', 'student_item_score', 'ldr_student_item_score', 'student_item_score', 'clean', None),
        # loader to staging
        ('2', 'ldr_student_item_score', 'batch_guid', 'stg_student_item_score', 'batch_guid', None, None),
        ('2', 'ldr_student_item_score', 'record_num', 'stg_student_item_score', 'record_num', None, None),
        ('2', 'ldr_student_item_score', 'asmt_attempt_guid', 'stg_student_item_score', 'asmt_attempt_guid', None, TO_SUBSTRING),
        ('2', 'ldr_student_item_score', 'item_guid', 'stg_student_item_score', 'item_guid', None, TO_SUBSTRING),
        ('2', 'ldr_student_item_score', 'student_guid', 'stg_student_item_score', 'student_guid', None, TO_SUBSTRING),
        ('2', 'ldr_student_item_score', 'student_item_score', 'stg_student_item_score', 'student_item_score', None, TO_FIVE_DIGITS),
        ('2', 'ldr_student_item_score', 'create_date', 'stg_student_item_score', 'create_date', None, None),
        # staging to integration
        ('3', 'stg_student_item_score', 'batch_guid', 'int_student_item_score', 'batch_guid', None, None),
        ('3', 'stg_student_item_score', 'record_num', 'int_student_item_score', 'record_num', None, None),
        ('3', 'stg_student_item_score', 'asmt_attempt_guid', 'int_student_item_score', 'asmt_attempt_guid', None, None),
        ('3', 'stg_student_item_score', 'item_guid', 'int_student_item_score', 'item_guid', None, None),
        ('3', 'stg_student_item_score', 'student_guid', 'int_student_item_score', 'student_guid', None, None),
        ('3', 'stg_student_item_score', 'student_item_score', 'int_student_item_score', 'student_item_score', None, None),
        ('3', 'stg_student_item_score', 'create_date', 'int_student_item_score', 'create_date', None, None)
    ]
}
