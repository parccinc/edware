TO_SMALLINT = "cast({src_column} as smallint)"
TO_THREE_DIGITS = "to_number({src_column},'999')"
TO_SUBSTRING = 'substr({src_column}, 1, {length})'

COLUMNS = ('phase', 'source_table', 'source_column', 'target_table', 'target_column', 'transformation_rule', 'stored_proc_name')


ref_table_conf = {
    'column_definitions': COLUMNS,
    'column_mappings': [
        # Columns:
        # column_map_key, phase, source_table, source_column, target_table, target_column, transformation_rule, stored_proc_name, stored_proc_created_date, create_date
        # CSV to loader
        ('1', 'lz_csv', 'AssessmentAttemptGuid', 'ldr_item_math_progress', 'asmt_attempt_guid', 'clean', None),
        ('1', 'lz_csv', 'StudentGuid', 'ldr_item_math_progress', 'student_parcc_id', 'clean', None),
        ('1', 'lz_csv', 'ItemGuid', 'ldr_item_math_progress', 'item_guid', 'clean', None),
        ('1', 'lz_csv', 'KeySelected', 'ldr_item_math_progress', 'select_key', 'clean', None),
        ('1', 'lz_csv', 'ScorePoints', 'ldr_item_math_progress', 'student_item_score', 'clean', None),
        ('1', 'lz_csv', 'ItemSequence', 'ldr_item_math_progress', 'item_seq', 'clean', None),


        # Loader to Staging
        ('2', 'ldr_item_math_progress', 'batch_guid', 'stg_item_math_progress', 'batch_guid', None, None),
        ('2', 'ldr_item_math_progress', 'record_num', 'stg_item_math_progress', 'record_num', None, None),
        ('2', 'ldr_item_math_progress', 'create_date', 'stg_item_math_progress', 'create_date', None, None),
        ('2', 'ldr_item_math_progress', 'asmt_attempt_guid', 'stg_item_math_progress', 'asmt_attempt_guid', None, TO_SUBSTRING),
        ('2', 'ldr_item_math_progress', 'student_parcc_id', 'stg_item_math_progress', 'student_parcc_id', None, TO_SUBSTRING),
        ('2', 'ldr_item_math_progress', 'item_guid', 'stg_item_math_progress', 'item_guid', None, TO_SUBSTRING),
        ('2', 'ldr_item_math_progress', 'select_key', 'stg_item_math_progress', 'select_key', None, TO_SUBSTRING),
        ('2', 'ldr_item_math_progress', 'student_item_score', 'stg_item_math_progress', 'student_item_score', None, TO_THREE_DIGITS),
        ('2', 'ldr_item_math_progress', 'item_seq', 'stg_item_math_progress', 'item_seq', None, TO_SMALLINT),

        # Loader to Staging
        ('3', 'stg_item_math_progress', 'batch_guid', 'int_item_math_progress', 'batch_guid', None, None),
        ('3', 'stg_item_math_progress', 'record_num', 'int_item_math_progress', 'record_num', None, None),
        ('3', 'stg_item_math_progress', 'create_date', 'int_item_math_progress', 'create_date', None, None),
        ('3', 'stg_item_math_progress', 'asmt_attempt_guid', 'int_item_math_progress', 'asmt_attempt_guid', None, None),
        ('3', 'stg_item_math_progress', 'student_parcc_id', 'int_item_math_progress', 'student_parcc_id', None, None),
        ('3', 'stg_item_math_progress', 'item_guid', 'int_item_math_progress', 'item_guid', None, None),
        ('3', 'stg_item_math_progress', 'select_key', 'int_item_math_progress', 'select_key', None, None),
        ('3', 'stg_item_math_progress', 'student_item_score', 'int_item_math_progress', 'student_item_score', None, None),
        ('3', 'stg_item_math_progress', 'item_seq', 'int_item_math_progress', 'item_seq', None, None),
    ]
}
