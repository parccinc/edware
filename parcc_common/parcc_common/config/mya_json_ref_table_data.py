TO_NUMERIC40 = "cast({src_column} as decimal (4, 0))"
TO_SUBSTRING = 'substr({value}, 1, {length})'
TO_FIVE_DIGITS = "to_number({value}, '99999')"

COLUMNS = ('phase', 'source_table', 'source_column', 'target_table', 'target_column', 'transformation_rule', 'stored_proc_name')


ref_table_conf = {
    'column_definitions': COLUMNS,
    'column_mappings': [
        # Columns:
        # column_map_key, phase, source_table, source_column, target_table, target_column, transformation_rule, stored_proc_name, stored_proc_created_date, create_date
        # JSON to staging
        ('1', 'lz_json', 'guid', 'stg_admin', 'admin_guid', None, TO_SUBSTRING),
        ('1', 'lz_json', 'code', 'stg_admin', 'admin_code', None, TO_SUBSTRING),
        ('1', 'lz_json', 'name', 'stg_admin', 'admin_name', None, TO_SUBSTRING),
        ('1', 'lz_json', 'year', 'stg_admin', 'date_taken_year', None, TO_SUBSTRING),
        ('1', 'lz_json', 'period', 'stg_admin', 'admin_period', None, TO_SUBSTRING),
        ('1', 'lz_json', 'type', 'stg_admin', 'admin_type', None, TO_SUBSTRING),
        ('1', 'lz_json', 'DataFileType', 'stg_admin', 'file_type', None, TO_SUBSTRING),

        ('1', 'lz_json', 'parent_guid', 'stg_asmt', 'admin_guid', None, TO_SUBSTRING),
        ('1', 'lz_json', 'guid', 'stg_asmt', 'asmt_guid', None, TO_SUBSTRING),
        ('1', 'lz_json', 'name', 'stg_asmt', 'asmt_title', None, TO_SUBSTRING),
        ('1', 'lz_json', 'testSubject', 'stg_asmt', 'asmt_subject', None, TO_SUBSTRING),

        ('1', 'lz_json', 'parent_guid', 'stg_asmt_lvl', 'asmt_guid', None, TO_SUBSTRING),
        ('1', 'lz_json', 'asmt_lvl_design', 'stg_asmt_lvl', 'asmt_lvl_design', None, TO_SUBSTRING),

        ('1', 'lz_json', 'parent_guid', 'stg_form', 'asmt_guid', None, TO_SUBSTRING),
        ('1', 'lz_json', 'form_format', 'stg_form', 'format', None, TO_SUBSTRING),
        ('1', 'lz_json', 'form_name', 'stg_form', 'name', None, TO_SUBSTRING),
        ('1', 'lz_json', 'guid', 'stg_form', 'form_guid', None, TO_SUBSTRING),
        ('1', 'lz_json', 'code', 'stg_form', 'form_code', None, TO_SUBSTRING),
        ('1', 'lz_json', 'refId', 'stg_form', 'form_ref_id', None, TO_SUBSTRING),
        ('1', 'lz_json', 'maxScore', 'stg_form', 'asmt_score_max', None, TO_FIVE_DIGITS),
        ('1', 'lz_json', 'version', 'stg_form', 'form_version', None, TO_SUBSTRING),

        ('1', 'lz_json', 'parent_guid', 'stg_form_group', 'asmt_form_guid', None, TO_SUBSTRING),
        ('1', 'lz_json', 'guid', 'stg_form_group', 'group_guid', None, TO_SUBSTRING),
        ('1', 'lz_json', 'code', 'stg_form_group', 'group_code', None, TO_SUBSTRING),
        ('1', 'lz_json', 'name', 'stg_form_group', 'group_name', None, TO_SUBSTRING),
        ('1', 'lz_json', 'description', 'stg_form_group', 'group_desc', None, TO_SUBSTRING),
        ('1', 'lz_json', 'type', 'stg_form_group', 'group_type', None, TO_SUBSTRING),
        ('1', 'lz_json', 'refId', 'stg_form_group', 'group_ref_id', None, TO_SUBSTRING),
        ('1', 'lz_json', 'ParentGroupGUID', 'stg_form_group', 'parent_group_guid', None, TO_SUBSTRING),

        ('1', 'lz_json', 'parent_guid', 'stg_item_mya_meta', 'asmt_form_group_guid', None, TO_SUBSTRING),
        ('1', 'lz_json', 'guid', 'stg_item_mya_meta', 'item_guid', None, TO_SUBSTRING),
        ('1', 'lz_json', 'name', 'stg_item_mya_meta', 'item_name', None, TO_SUBSTRING),
        ('1', 'lz_json', 'description', 'stg_item_mya_meta', 'item_descript', None, TO_SUBSTRING),
        ('1', 'lz_json', 'evidenceGUID', 'stg_item_mya_meta', 'evidence_guid', None, TO_SUBSTRING),
        ('1', 'lz_json', 'evidenceStatement', 'stg_item_mya_meta', 'evidence_statement', None, TO_SUBSTRING),
        ('1', 'lz_json', 'standardGUID', 'stg_item_mya_meta', 'standard_guid', None, TO_SUBSTRING),
        ('1', 'lz_json', 'standard', 'stg_item_mya_meta', 'standard', None, TO_SUBSTRING),
        ('1', 'lz_json', 'refId', 'stg_item_mya_meta', 'item_ref_id', None, TO_SUBSTRING),
        ('1', 'lz_json', 'maxPoints', 'stg_item_mya_meta', 'item_max_points', None, TO_SUBSTRING),
        ('1', 'lz_json', 'version', 'stg_item_mya_meta', 'item_version', None, TO_SUBSTRING),
        ('1', 'lz_json', 'omitted', 'stg_item_mya_meta', 'is_item_omitted', None, TO_SUBSTRING),
        ('1', 'lz_json', 'itemStatus', 'stg_item_mya_meta', 'item_status', None, TO_SUBSTRING),
        ('1', 'lz_json', 'testItemType', 'stg_item_mya_meta', 'test_item_type', None, TO_SUBSTRING),
        ('1', 'lz_json', 'parentItem', 'stg_item_mya_meta', 'parent_item_guid', None, TO_SUBSTRING),


        # Staging to integration
        ('3', 'stg_admin', 'admin_guid', 'int_admin', 'admin_guid', None, None),
        ('3', 'stg_admin', 'admin_name', 'int_admin', 'admin_name', None, None),
        ('3', 'stg_admin', 'admin_code', 'int_admin', 'admin_code', None, None),
        ('3', 'stg_admin', 'admin_type', 'int_admin', 'admin_type', None, None),
        ('3', 'stg_admin', 'admin_period', 'int_admin', 'admin_period', None, None),
        ('3', 'stg_admin', 'date_taken_year', 'int_admin', 'date_taken_year', None, None),
        ('3', 'stg_admin', 'file_type', 'int_admin', 'file_type', None, None),
        ('3', 'stg_admin', 'batch_guid', 'int_admin', 'batch_guid', None, None),
        ('3', 'stg_admin', 'create_date', 'int_admin', 'create_date', None, None),

        ('3', 'stg_asmt', 'asmt_guid', 'int_asmt', 'asmt_guid', None, None),
        ('3', 'stg_asmt', 'admin_guid', 'int_asmt', 'admin_guid', None, None),
        ('3', 'stg_asmt', 'asmt_title', 'int_asmt', 'asmt_title', None, None),
        ('3', 'stg_asmt', 'asmt_subject', 'int_asmt', 'asmt_subject', None, None),
        ('3', 'stg_asmt', 'batch_guid', 'int_asmt', 'batch_guid', None, None),
        ('3', 'stg_asmt', 'create_date', 'int_asmt', 'create_date', None, None),
        # asmt_lvl_design?

        ('3', 'stg_asmt_lvl', 'asmt_guid', 'int_asmt_lvl', 'asmt_guid', None, None),
        ('3', 'stg_asmt_lvl', 'asmt_lvl_design', 'int_asmt_lvl', 'asmt_lvl_design', None, None),
        ('3', 'stg_asmt_lvl', 'batch_guid', 'int_asmt_lvl', 'batch_guid', None, None),
        ('3', 'stg_asmt_lvl', 'create_date', 'int_asmt_lvl', 'create_date', None, None),

        ('3', 'stg_form', 'form_guid', 'int_form', 'form_guid', None, None),
        ('3', 'stg_form', 'asmt_guid', 'int_form', 'asmt_guid', None, None),
        ('1', 'stg_form', 'form_format', 'int_form', 'form_format', None, None),
        ('1', 'stg_form', 'form_name', 'int_form', 'form_name', None, None),
        ('3', 'stg_form', 'form_code', 'int_form', 'form_code', None, None),
        ('3', 'stg_form', 'form_ref_id', 'int_form', 'form_ref_id', None, None),
        ('3', 'stg_form', 'form_version', 'int_form', 'form_version', None, None),
        ('3', 'stg_form', 'asmt_score_max', 'int_form', 'asmt_score_max', None, None),
        ('3', 'stg_form', 'batch_guid', 'int_form', 'batch_guid', None, None),
        ('3', 'stg_form', 'record_num', 'int_form', 'record_num', None, None),
        ('3', 'stg_form', 'create_date', 'int_form', 'create_date', None, None),

        ('3', 'vw_form_group_int_src', 'asmt_guid', 'int_form_group', 'asmt_guid', None, None),
        ('3', 'vw_form_group_int_src', 'form_guid', 'int_form_group', 'form_guid', None, None),
        ('3', 'vw_form_group_int_src', 'group_guid', 'int_form_group', 'group_guid', None, None),
        ('3', 'vw_form_group_int_src', 'group_code', 'int_form_group', 'group_code', None, None),
        ('3', 'vw_form_group_int_src', 'group_name', 'int_form_group', 'group_name', None, None),
        ('3', 'vw_form_group_int_src', 'group_desc', 'int_form_group', 'group_desc', None, None),
        ('3', 'vw_form_group_int_src', 'group_type', 'int_form_group', 'group_type', None, None),
        ('3', 'vw_form_group_int_src', 'parent_group_guid', 'int_form_group', 'parent_group_guid', None, None),
        ('3', 'vw_form_group_int_src', 'group_ref_id', 'int_form_group', 'group_ref_id', None, None),
        ('3', 'vw_form_group_int_src', 'batch_guid', 'int_form_group', 'batch_guid', None, None),
        ('3', 'vw_form_group_int_src', 'record_num', 'int_form_group', 'record_num', None, None),
        ('3', 'vw_form_group_int_src', 'create_date', 'int_form_group', 'create_date', None, None),

        ('3', 'vw_form_group_item_int_mya_src', 'asmt_guid', 'int_form_group_item', 'asmt_guid', None, None),
        ('3', 'vw_form_group_item_int_mya_src', 'form_guid', 'int_form_group_item', 'form_guid', None, None),
        ('3', 'vw_form_group_item_int_mya_src', 'group_guid', 'int_form_group_item', 'group_guid', None, None),
        ('3', 'vw_form_group_item_int_mya_src', 'item_guid', 'int_form_group_item', 'item_guid', None, None),
        ('3', 'vw_form_group_item_int_mya_src', 'batch_guid', 'int_form_group_item', 'batch_guid', None, None),
        ('3', 'vw_form_group_item_int_mya_src', 'record_num', 'int_form_group_item', 'record_num', None, None),
        ('3', 'vw_form_group_item_int_mya_src', 'create_date', 'int_form_group_item', 'create_date', None, None),

        ('3', 'vw_item_mya_meta_int_src', 'item_guid', 'int_item_mya_meta', 'item_guid', None, None),
        ('3', 'vw_item_mya_meta_int_src', 'item_name', 'int_item_mya_meta', 'item_name', None, None),
        ('3', 'vw_item_mya_meta_int_src', 'item_descript', 'int_item_mya_meta', 'item_descript', None, None),
        ('3', 'vw_item_mya_meta_int_src', 'evidence_guid', 'int_item_mya_meta', 'evidence_guid', None, None),
        ('3', 'vw_item_mya_meta_int_src', 'evidence_statement', 'int_item_mya_meta', 'evidence_statement', None, None),
        ('3', 'vw_item_mya_meta_int_src', 'standard_guid', 'int_item_mya_meta', 'standard_guid', None, None),
        ('3', 'vw_item_mya_meta_int_src', 'standard', 'int_item_mya_meta', 'standard', None, None),
        ('3', 'vw_item_mya_meta_int_src', 'item_ref_id', 'int_item_mya_meta', 'item_ref_id', None, None),
        ('3', 'vw_item_mya_meta_int_src', 'item_max_points', 'int_item_mya_meta', 'item_max_points', None, None),
        ('3', 'vw_item_mya_meta_int_src', 'item_version', 'int_item_mya_meta', 'item_version', None, None),
        ('3', 'vw_item_mya_meta_int_src', 'test_item_type', 'int_item_mya_meta', 'test_item_type', None, None),
        ('3', 'vw_item_mya_meta_int_src', 'is_item_omitted', 'int_item_mya_meta', 'is_item_omitted', None, None),
        ('3', 'vw_item_mya_meta_int_src', 'item_status', 'int_item_mya_meta', 'item_status', None, None),
        ('3', 'vw_item_mya_meta_int_src', 'parent_item_guid', 'int_item_mya_meta', 'parent_item_guid', None, None),
        ('3', 'vw_item_mya_meta_int_src', 'batch_guid', 'int_item_mya_meta', 'batch_guid', None, None),
        ('3', 'vw_item_mya_meta_int_src', 'create_date', 'int_item_mya_meta', 'create_date', None, None)
    ]

}
