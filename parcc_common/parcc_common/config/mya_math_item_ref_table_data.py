TO_NUMERIC40 = "cast({src_column} as decimal (4, 0))"

COLUMNS = ('phase', 'source_table', 'source_column', 'target_table', 'target_column', 'transformation_rule', 'stored_proc_name')


ref_table_conf = {
    'column_definitions': COLUMNS,
    'column_mappings': [
        # Columns:
        # column_map_key, phase, source_table, source_column, target_table, target_column, transformation_rule, stored_proc_name, stored_proc_created_date, create_date
        # CSV to loader
        ('1', 'lz_csv', 'AssessmentRegistrationTestAttemptIdentifier', 'ldr_item_math_mya', 'asmt_attempt_guid', 'clean', None),
        ('1', 'lz_csv', 'PARCCStudentIdentifier', 'ldr_item_math_mya', 'student_parcc_id', 'clean', None),
        ('1', 'lz_csv', 'ItemGUID', 'ldr_item_math_mya', 'item_guid', 'clean', None),
        ('1', 'lz_csv', 'StudentItemScore', 'ldr_item_math_mya', 'student_item_score', 'clean', None),

        # Loader to Staging
        ('2', 'ldr_item_math_mya', 'batch_guid', 'stg_item_math_mya', 'batch_guid', None, None),
        ('2', 'ldr_item_math_mya', 'record_num', 'stg_item_math_mya', 'record_num', None, None),
        ('2', 'ldr_item_math_mya', 'create_date', 'stg_item_math_mya', 'create_date', None, None),
        ('2', 'ldr_item_math_mya', 'asmt_attempt_guid', 'stg_item_math_mya', 'asmt_attempt_guid', None, None),
        ('2', 'ldr_item_math_mya', 'student_parcc_id', 'stg_item_math_mya', 'student_parcc_id', None, None),
        ('2', 'ldr_item_math_mya', 'item_guid', 'stg_item_math_mya', 'item_guid', None, None),
        ('2', 'ldr_item_math_mya', 'student_item_score', 'stg_item_math_mya', 'student_item_score', None, TO_NUMERIC40),

        # Staging to Integration
        ('3', 'stg_item_math_mya', 'batch_guid', 'int_item_math_mya', 'batch_guid', None, None),
        ('3', 'stg_item_math_mya', 'record_num', 'int_item_math_mya', 'record_num', None, None),
        ('3', 'stg_item_math_mya', 'create_date', 'int_item_math_mya', 'create_date', None, None),
        ('3', 'stg_item_math_mya', 'asmt_attempt_guid', 'int_item_math_mya', 'asmt_attempt_guid', None, None),
        ('3', 'stg_item_math_mya', 'student_parcc_id', 'int_item_math_mya', 'student_parcc_id', None, None),
        ('3', 'stg_item_math_mya', 'item_guid', 'int_item_math_mya', 'item_guid', None, None),
        ('3', 'stg_item_math_mya', 'student_item_score', 'int_item_math_mya', 'student_item_score', None, None),
    ]
}
