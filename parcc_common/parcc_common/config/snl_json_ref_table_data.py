__author__ = 'npandey'

TO_SUBSTRING = 'substr({value}, 1, {length})'

COLUMNS = ('phase', 'source_table', 'source_column', 'target_table', 'target_column', 'transformation_rule', 'stored_proc_name')


ref_table_conf = {
    'column_definitions': COLUMNS,
    'column_mappings': [
        # Columns:
        # column_map_key, phase, source_table, source_column, target_table, target_column, transformation_rule, stored_proc_name, stored_proc_created_date, create_date
        # Json to Staging
        ('1', 'lz_json', 'guid', 'stg_admin', 'admin_guid', None, TO_SUBSTRING),
        ('1', 'lz_json', 'code', 'stg_admin', 'admin_code', None, TO_SUBSTRING),
        ('1', 'lz_json', 'name', 'stg_admin', 'admin_name', None, TO_SUBSTRING),
        ('1', 'lz_json', 'year', 'stg_admin', 'date_taken_year', None, TO_SUBSTRING),
        ('1', 'lz_json', 'period', 'stg_admin', 'admin_period', None, TO_SUBSTRING),
        ('1', 'lz_json', 'type', 'stg_admin', 'admin_type', None, TO_SUBSTRING),
        ('1', 'lz_json', 'DataFileType', 'stg_admin', 'file_type', None, TO_SUBSTRING),

        ('1', 'lz_json', 'parent_guid', 'stg_snl_asmt_m', 'admin_guid', None, TO_SUBSTRING),
        ('1', 'lz_json', 'guid', 'stg_snl_asmt_m', 'asmt_guid', None, TO_SUBSTRING),
        ('1', 'lz_json', 'name', 'stg_snl_asmt_m', 'asmt_title', None, TO_SUBSTRING),
        ('1', 'lz_json', 'testSubject', 'stg_snl_asmt_m', 'asmt_subject', None, TO_SUBSTRING),
        ('1', 'lz_json', 'documentID', 'stg_snl_asmt_m', 'document_id', None, TO_SUBSTRING),
        ('1', 'lz_json', 'setID', 'stg_snl_asmt_m', 'set_id', None, TO_SUBSTRING),
        ('1', 'lz_json', 'setSeqNum', 'stg_snl_asmt_m', 'set_seq_num', None, TO_SUBSTRING),
        ('1', 'lz_json', 'evidenceStatements.evidenceStatement1', 'stg_snl_asmt_m', 'evidence_stmt1', None, TO_SUBSTRING),
        ('1', 'lz_json', 'evidenceStatements.evidence1guid', 'stg_snl_asmt_m', 'evidence_stmt_guid1', None, TO_SUBSTRING),
        ('1', 'lz_json', 'evidenceStatements.evidenceStatement2', 'stg_snl_asmt_m', 'evidence_stmt2', None, TO_SUBSTRING),
        ('1', 'lz_json', 'evidenceStatements.evidence2guid', 'stg_snl_asmt_m', 'evidence_stmt_guid2', None, TO_SUBSTRING),
        ('1', 'lz_json', 'evidenceStatements.evidenceStatement3', 'stg_snl_asmt_m', 'evidence_stmt3', None, TO_SUBSTRING),
        ('1', 'lz_json', 'evidenceStatements.evidence3guid', 'stg_snl_asmt_m', 'evidence_stmt_guid3', None, TO_SUBSTRING),
        ('1', 'lz_json', 'evidenceStatements.evidenceStatement4', 'stg_snl_asmt_m', 'evidence_stmt4', None, TO_SUBSTRING),
        ('1', 'lz_json', 'evidenceStatements.evidence4guid', 'stg_snl_asmt_m', 'evidence_stmt_guid4', None, TO_SUBSTRING),
        ('1', 'lz_json', 'evidenceStatements.evidenceStatement5', 'stg_snl_asmt_m', 'evidence_stmt5', None, TO_SUBSTRING),
        ('1', 'lz_json', 'evidenceStatements.evidence5guid', 'stg_snl_asmt_m', 'evidence_stmt_guid5', None, TO_SUBSTRING),
        ('1', 'lz_json', 'rubric1', 'stg_snl_asmt_m', 'rubric1', None, TO_SUBSTRING),
        ('1', 'lz_json', 'rubric2', 'stg_snl_asmt_m', 'rubric2', None, TO_SUBSTRING),

        ('1', 'lz_json', 'parent_guid', 'stg_snl_asmt_mode_m', 'asmt_guid', None, TO_SUBSTRING),
        ('1', 'lz_json', 'asmt_mode', 'stg_snl_asmt_mode_m', 'asmt_mode', None, TO_SUBSTRING),

        ('1', 'lz_json', 'parent_guid', 'stg_asmt_lvl', 'asmt_guid', None, TO_SUBSTRING),
        ('1', 'lz_json', 'asmt_lvl_design', 'stg_asmt_lvl', 'asmt_lvl_design', None, TO_SUBSTRING),

        ('1', 'lz_json', 'parent_guid', 'stg_snl_task_m', 'asmt_guid', None, TO_SUBSTRING),
        ('1', 'lz_json', 'guid', 'stg_snl_task_m', 'task_guid', None, TO_SUBSTRING),
        ('1', 'lz_json', 'description', 'stg_snl_task_m', 'task_descript', None, TO_SUBSTRING),
        ('1', 'lz_json', 'shortName', 'stg_snl_task_m', 'task_short_name', None, TO_SUBSTRING),
        ('1', 'lz_json', 'ptMode', 'stg_snl_task_m', 'pt_task_mode', None, TO_SUBSTRING),
        ('1', 'lz_json', 'ptComplexity', 'stg_snl_task_m', 'pt_complexity', None, TO_SUBSTRING),
        ('1', 'lz_json', 'ptStimulusType', 'stg_snl_task_m', 'pt_stimulus_type', None, TO_SUBSTRING),
        ('1', 'lz_json', 'ptStimulusComplexity', 'stg_snl_task_m', 'pt_stimulus_complexity', None, TO_SUBSTRING),


        # Staging to Integration
        ('3', 'stg_admin', 'admin_guid', 'int_admin', 'admin_guid', None, None),
        ('3', 'stg_admin', 'admin_name', 'int_admin', 'admin_name', None, None),
        ('3', 'stg_admin', 'admin_code', 'int_admin', 'admin_code', None, None),
        ('3', 'stg_admin', 'admin_type', 'int_admin', 'admin_type', None, None),
        ('3', 'stg_admin', 'admin_period', 'int_admin', 'admin_period', None, None),
        ('3', 'stg_admin', 'date_taken_year', 'int_admin', 'date_taken_year', None, None),
        ('3', 'stg_admin', 'file_type', 'int_admin', 'file_type', None, None),
        ('3', 'stg_admin', 'batch_guid', 'int_admin', 'batch_guid', None, None),
        ('3', 'stg_admin', 'create_date', 'int_admin', 'create_date', None, None),

        ('3', 'stg_snl_asmt_m', 'asmt_guid', 'int_snl_asmt_m', 'asmt_guid', None, None),
        ('3', 'stg_snl_asmt_m', 'admin_guid', 'int_snl_asmt_m', 'admin_guid', None, None),
        ('3', 'stg_snl_asmt_m', 'asmt_title', 'int_snl_asmt_m', 'asmt_title', None, None),
        ('3', 'stg_snl_asmt_m', 'asmt_subject', 'int_snl_asmt_m', 'asmt_subject', None, None),
        ('3', 'stg_snl_asmt_m', 'document_id', 'int_snl_asmt_m', 'document_id', None, None),
        ('3', 'stg_snl_asmt_m', 'set_id', 'int_snl_asmt_m', 'set_id', None, None),
        ('3', 'stg_snl_asmt_m', 'set_seq_num', 'int_snl_asmt_m', 'set_seq_num', None, None),
        ('3', 'stg_snl_asmt_m', 'evidence_stmt1', 'int_snl_asmt_m', 'evidence_stmt1', None, None),
        ('3', 'stg_snl_asmt_m', 'evidence_stmt_guid1', 'int_snl_asmt_m', 'evidence_stmt_guid1', None, None),
        ('3', 'stg_snl_asmt_m', 'evidence_stmt2', 'int_snl_asmt_m', 'evidence_stmt2', None, None),
        ('3', 'stg_snl_asmt_m', 'evidence_stmt_guid2', 'int_snl_asmt_m', 'evidence_stmt_guid2', None, None),
        ('3', 'stg_snl_asmt_m', 'evidence_stmt3', 'int_snl_asmt_m', 'evidence_stmt3', None, None),
        ('3', 'stg_snl_asmt_m', 'evidence_stmt_guid3', 'int_snl_asmt_m', 'evidence_stmt_guid3', None, None),
        ('3', 'stg_snl_asmt_m', 'evidence_stmt4', 'int_snl_asmt_m', 'evidence_stmt4', None, None),
        ('3', 'stg_snl_asmt_m', 'evidence_stmt_guid4', 'int_snl_asmt_m', 'evidence_stmt_guid4', None, None),
        ('3', 'stg_snl_asmt_m', 'evidence_stmt5', 'int_snl_asmt_m', 'evidence_stmt5', None, None),
        ('3', 'stg_snl_asmt_m', 'evidence_stmt_guid5', 'int_snl_asmt_m', 'evidence_stmt_guid5', None, None),
        ('3', 'stg_snl_asmt_m', 'rubric1', 'int_snl_asmt_m', 'rubric1', None, None),
        ('3', 'stg_snl_asmt_m', 'rubric2', 'int_snl_asmt_m', 'rubric2', None, None),
        ('3', 'stg_snl_asmt_m', 'batch_guid', 'int_snl_asmt_m', 'batch_guid', None, None),
        ('3', 'stg_snl_asmt_m', 'create_date', 'int_snl_asmt_m', 'create_date', None, None),

        ('3', 'stg_snl_asmt_mode_m', 'asmt_guid', 'int_snl_asmt_mode_m', 'asmt_guid', None, None),
        ('3', 'stg_snl_asmt_mode_m', 'asmt_mode', 'int_snl_asmt_mode_m', 'asmt_mode', None, None),
        ('3', 'stg_snl_asmt_mode_m', 'batch_guid', 'int_snl_asmt_mode_m', 'batch_guid', None, None),
        ('3', 'stg_snl_asmt_mode_m', 'create_date', 'int_snl_asmt_mode_m', 'create_date', None, None),

        ('3', 'stg_asmt_lvl', 'asmt_guid', 'int_asmt_lvl', 'asmt_guid', None, None),
        ('3', 'stg_asmt_lvl', 'asmt_lvl_design', 'int_asmt_lvl', 'asmt_lvl_design', None, None),
        ('3', 'stg_asmt_lvl', 'batch_guid', 'int_asmt_lvl', 'batch_guid', None, None),
        ('3', 'stg_asmt_lvl', 'create_date', 'int_asmt_lvl', 'create_date', None, None),

        ('3', 'stg_snl_task_m', 'asmt_guid', 'int_snl_task_m', 'asmt_guid', None, None),
        ('3', 'stg_snl_task_m', 'task_guid', 'int_snl_task_m', 'task_guid', None, None),
        ('3', 'stg_snl_task_m', 'task_descript', 'int_snl_task_m', 'task_descript', None, None),
        ('3', 'stg_snl_task_m', 'task_short_name', 'int_snl_task_m', 'task_short_name', None, None),
        ('3', 'stg_snl_task_m', 'pt_task_mode', 'int_snl_task_m', 'pt_task_mode', None, None),
        ('3', 'stg_snl_task_m', 'pt_complexity', 'int_snl_task_m', 'pt_complexity', None, None),
        ('3', 'stg_snl_task_m', 'pt_stimulus_type', 'int_snl_task_m', 'pt_stimulus_type', None, None),
        ('3', 'stg_snl_task_m', 'pt_stimulus_complexity', 'int_snl_task_m', 'pt_stimulus_complexity', None, None),
        ('3', 'stg_snl_task_m', 'batch_guid', 'int_snl_task_m', 'batch_guid', None, None),
        ('3', 'stg_snl_task_m', 'create_date', 'int_snl_task_m', 'create_date', None, None),


    ]
}
