__author__ = 'esnyder'

TO_SUBSTRING = 'substr({value}, 1, {length})'
TO_FIVE_DIGITS = "to_number({value}, '99999')"
TO_SMALLINT = "cast({value} as smallint)"

COLUMNS = ('phase', 'source_table', 'source_column', 'target_table', 'target_column', 'transformation_rule', 'stored_proc_name')


ref_table_conf = {
    'column_definitions': COLUMNS,
    'column_mappings': [
        # Columns:
        # column_map_key, phase, source_table, source_column, target_table, target_column, transformation_rule, stored_proc_name, stored_proc_created_date, create_date
        # JSON to staging
        ('1', 'lz_json', 'test_code', 'stg_sum_test_score_m', 'test_code', None, TO_SUBSTRING),
        ('1', 'lz_json', 'summative_period', 'stg_sum_test_score_m', 'sum_period', None, TO_SUBSTRING),

        ('1', 'lz_json', 'parent_guid', 'stg_sum_test_level_m', 'test_code', None, TO_SUBSTRING),
        ('1', 'lz_json', 'test_perf_lvl_id', 'stg_sum_test_level_m', 'perf_lvl_id', None, TO_SUBSTRING),
        ('1', 'lz_json', 'test_perf_lvl_cutpoint_label', 'stg_sum_test_level_m', 'cutpoint_label', None, TO_SUBSTRING),
        ('1', 'lz_json', 'test_perf_lvl_cutpoint_low', 'stg_sum_test_level_m', 'cutpoint_low', None, TO_SMALLINT),
        ('1', 'lz_json', 'test_perf_lvl_cutpoint_upper', 'stg_sum_test_level_m', 'cutpoint_upper', None, TO_SMALLINT),

        # Staging to Integration
        ('3', 'stg_sum_test_score_m', 'test_code', 'int_sum_test_score_m', 'test_code', None, None),
        ('3', 'stg_sum_test_score_m', 'sum_period', 'int_sum_test_score_m', 'sum_period', None, None),
        ('3', 'stg_sum_test_score_m', 'batch_guid', 'int_sum_test_score_m', 'batch_guid', None, None),
        ('3', 'stg_sum_test_score_m', 'create_date', 'int_sum_test_score_m', 'create_date', None, None),

        ('3', 'stg_sum_test_level_m', 'test_code', 'int_sum_test_level_m', 'test_code', None, None),
        ('3', 'stg_sum_test_level_m', 'perf_lvl_id', 'int_sum_test_level_m', 'perf_lvl_id', None, None),
        ('3', 'stg_sum_test_level_m', 'cutpoint_label', 'int_sum_test_level_m', 'cutpoint_label', None, None),
        ('3', 'stg_sum_test_level_m', 'cutpoint_low', 'int_sum_test_level_m', 'cutpoint_low', None, None),
        ('3', 'stg_sum_test_level_m', 'cutpoint_upper', 'int_sum_test_level_m', 'cutpoint_upper', None, None),
        ('3', 'stg_sum_test_level_m', 'batch_guid', 'int_sum_test_level_m', 'batch_guid', None, None),
        ('3', 'stg_sum_test_level_m', 'create_date', 'int_sum_test_level_m', 'create_date', None, None),
    ]
}
