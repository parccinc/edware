__author__ = 'npandey'

TO_SUBSTRING = 'substr({src_column}, 1, {length})'
TO_DATE = "to_date({src_column}, 'YYYY-MM-DD')"
TO_NUMERIC50 = "cast({src_column} as numeric (5, 0))"
TO_NUMERIC30 = "cast({src_column} as numeric (3, 0))"
TO_NUMERIC20 = "cast({src_column} as numeric (2, 0))"

COLUMNS = ('phase', 'source_table', 'source_column', 'target_table', 'target_column', 'transformation_rule', 'stored_proc_name')

ref_table_conf = {
    'column_definitions': COLUMNS,
    'column_mappings': [
        # Columns:
        # column_map_key, phase, source_table, source_column, target_table, target_column, transformation_rule, stored_proc_name, stored_proc_created_date, create_date
        # CSV to loader
        ('1', 'lz_csv', 'RecordType', 'ldr_ela_read_flu', 'record_type', 'clean', None),
        ('1', 'lz_csv', 'MultipleRecordFlag', 'ldr_ela_read_flu', 'is_multi_rec', 'yn', None),
        ('1', 'lz_csv', 'ReportedDiagnosticScoreFlag', 'ldr_ela_read_flu', 'is_reported_diagnostic', 'yn', None),
        ('1', 'lz_csv', 'Year', 'ldr_ela_read_flu', 'year', 'clean', None),
        ('1', 'lz_csv', 'AssessmentDate', 'ldr_ela_read_flu', 'asmt_date', 'clean', None),
        ('1', 'lz_csv', 'StateAbbreviation', 'ldr_ela_read_flu', 'state_code', 'cleanUpper', None),
        ('1', 'lz_csv', 'ResponsibleDistrictIdentifier', 'ldr_ela_read_flu', 'resp_dist_id', 'clean', None),
        ('1', 'lz_csv', 'ResponsibleDistrictName', 'ldr_ela_read_flu', 'resp_dist_name', 'clean', None),
        ('1', 'lz_csv', 'ResponsibleSchoolInstitutionIdentifier', 'ldr_ela_read_flu', 'resp_school_id', 'clean', None),
        ('1', 'lz_csv', 'ResponsibleSchoolInstitutionName', 'ldr_ela_read_flu', 'resp_school_name', 'clean', None),
        ('1', 'lz_csv', 'PARCCStudentIdentifier', 'ldr_ela_read_flu', 'student_parcc_id', 'clean', None),
        ('1', 'lz_csv', 'StateStudentIdentifier', 'ldr_ela_read_flu', 'student_state_id', 'clean', None),
        ('1', 'lz_csv', 'LocalStudentIdentifier', 'ldr_ela_read_flu', 'student_local_id', 'clean', None),
        ('1', 'lz_csv', 'FirstName', 'ldr_ela_read_flu', 'student_first_name', 'clean', None),
        ('1', 'lz_csv', 'MiddleName', 'ldr_ela_read_flu', 'student_middle_name', 'clean', None),
        ('1', 'lz_csv', 'LastName', 'ldr_ela_read_flu', 'student_last_name', 'clean', None),
        ('1', 'lz_csv', 'Sex', 'ldr_ela_read_flu', 'student_sex', 'gender', None),
        ('1', 'lz_csv', 'Birthdate', 'ldr_ela_read_flu', 'student_dob', 'date', None),
        ('1', 'lz_csv', 'OptionalStateData1', 'ldr_ela_read_flu', 'opt_state_data1', 'clean', None),
        ('1', 'lz_csv', 'GradeLevelWhenAssessed', 'ldr_ela_read_flu', 'student_grade', 'clean', None),
        ('1', 'lz_csv', 'HispanicLatinoEthnicity', 'ldr_ela_read_flu', 'ethn_hisp_latino', 'yn', None),
        ('1', 'lz_csv', 'AmericanIndianAlaskaNative', 'ldr_ela_read_flu', 'ethn_indian_alaska', 'yn', None),
        ('1', 'lz_csv', 'Asian', 'ldr_ela_read_flu', 'ethn_asian', 'yn', None),
        ('1', 'lz_csv', 'BlackorAfricanAmerican', 'ldr_ela_read_flu', 'ethn_black', 'yn', None),
        ('1', 'lz_csv', 'NativeHawaiianorOtherPacificIslander', 'ldr_ela_read_flu', 'ethn_hawai', 'yn', None),
        ('1', 'lz_csv', 'White', 'ldr_ela_read_flu', 'ethn_white', 'yn', None),
        ('1', 'lz_csv', 'TwoOrMoreRaces', 'ldr_ela_sum', 'ethn_two_or_more_races', 'yn', None),
        ('1', 'lz_csv', 'FillerRaceField', 'ldr_ela_read_flu', 'ethn_filler', 'yn', None),
        ('1', 'lz_csv', 'FederalRace', 'ldr_ela_read_flu', 'ethnicity', 'clean', None),
        ('1', 'lz_csv', 'EnglishLearner', 'ldr_ela_read_flu', 'ell', 'yn', None),
        ('1', 'lz_csv', 'TitleIIILimitedEnglishProficientParticipationStatus', 'ldr_ela_read_flu', 'lep_status', 'yn', None),
        ('1', 'lz_csv', 'GiftedAndTalented', 'ldr_ela_read_flu', 'gift_talent', 'yn', None),
        ('1', 'lz_csv', 'MigrantStatus', 'ldr_ela_read_flu', 'migrant_status', 'yn', None),
        ('1', 'lz_csv', 'EconomicDisadvantageStatus', 'ldr_ela_read_flu', 'econo_disadvantage', 'yn', None),
        ('1', 'lz_csv', 'StudentWithDisability', 'ldr_ela_read_flu', 'disabil_student', 'yn', None),
        ('1', 'lz_csv', 'PrimaryDisabilityType', 'ldr_ela_read_flu', 'primary_disabil_type', 'clean', None),
        ('1', 'lz_csv', 'StaffMemberIdentifier', 'ldr_ela_read_flu', 'staff_id', 'clean', None),
        ('1', 'lz_csv', 'AssessmentAccommodationEnglishlearner', 'ldr_ela_read_flu', 'accomod_ell', 'yn', None),
        ('1', 'lz_csv', 'AssessmentAccommodation504', 'ldr_ela_read_flu', 'accomod_504', 'yn', None),
        ('1', 'lz_csv', 'AssessmentAccommodationIndividualizedEducationalPlan', 'ldr_ela_read_flu', 'accomod_ind_ed', 'yn', None),
        ('1', 'lz_csv', 'FrequentBreaks', 'ldr_ela_read_flu', 'accomod_freq_breaks', 'yn', None),
        ('1', 'lz_csv', 'SeparateAlternateLocation', 'ldr_ela_read_flu', 'accomod_alt_location', 'yn', None),
        ('1', 'lz_csv', 'SmallTestingGroup', 'ldr_ela_read_flu', 'accomod_small_group', 'yn', None),
        ('1', 'lz_csv', 'SpecializedEquipmentorFurniture', 'ldr_ela_read_flu', 'accomod_special_equip', 'yn', None),
        ('1', 'lz_csv', 'SpecifiedAreaorSetting', 'ldr_ela_read_flu', 'accomod_spec_area', 'yn', None),
        ('1', 'lz_csv', 'TimeOfDay', 'ldr_ela_read_flu', 'accomod_time_day', 'yn', None),
        ('1', 'lz_csv', 'AnswerMasking', 'ldr_ela_read_flu', 'accomod_answer_mask', 'yn', None),
        ('1', 'lz_csv', 'ColorContrast', 'ldr_ela_read_flu', 'accomod_color_contrast', 'clean', None),
        ('1', 'lz_csv', 'TexttoSpeechforMathematics', 'ldr_ela_read_flu', 'accomod_text_2_speech_math', 'yn', None),
        ('1', 'lz_csv', 'HumanReaderorHumanSignerforMathematics', 'ldr_ela_read_flu', 'accomod_read_math', 'clean', None),
        ('1', 'lz_csv', 'ASLVideo', 'ldr_ela_read_flu', 'accomod_asl_video', 'yn', None),
        ('1', 'lz_csv', 'ScreenReaderORotherAssistiveTechnologyAT', 'ldr_ela_read_flu', 'accomod_screen_reader', 'yn', None),
        ('1', 'lz_csv', 'ClosedCaptioningforELAL', 'ldr_ela_read_flu', 'accomod_close_capt_ela', 'yn', None),
        ('1', 'lz_csv', 'HumanReaderorHumanSignerforELAL', 'ldr_ela_read_flu', 'accomod_read_ela', 'clean', None),
        ('1', 'lz_csv', 'RefreshableBrailleDisplayforELAL', 'ldr_ela_read_flu', 'accomod_braille_ela', 'yn', None),
        ('1', 'lz_csv', 'TactileGraphics', 'ldr_ela_read_flu', 'accomod_tactile_graph', 'yn', None),
        ('1', 'lz_csv', 'TexttoSpeechforELAL', 'ldr_ela_read_flu', 'accomod_text_2_speech_ela', 'yn', None),
        ('1', 'lz_csv', 'AnswersRecordedInTestBook', 'ldr_ela_read_flu', 'accomod_answer_rec', 'yn', None),
        ('1', 'lz_csv', 'BrailleResponse', 'ldr_ela_read_flu', 'accomod_braille_resp', 'clean', None),
        ('1', 'lz_csv', 'CalculationDeviceandMathematicsTools', 'ldr_ela_read_flu', 'accomod_calculator', 'yn', None),
        ('1', 'lz_csv', 'ELALConstructedResponse', 'ldr_ela_read_flu', 'accomod_construct_resp_ela', 'clean', None),
        ('1', 'lz_csv', 'ELALSelectedResponseorTechnologyEnhancedItems', 'ldr_ela_read_flu', 'accomod_select_resp_ela', 'clean', None),
        ('1', 'lz_csv', 'MathematicsResponse', 'ldr_ela_read_flu', 'accomod_math_resp', 'clean', None),
        ('1', 'lz_csv', 'MonitorTestResponse', 'ldr_ela_read_flu', 'accomod_monitor_test_resp', 'yn', None),
        ('1', 'lz_csv', 'WordPrediction', 'ldr_ela_read_flu', 'accomod_word_predict', 'yn', None),
        ('1', 'lz_csv', 'AdministrationDirectionsClarifiedinStudentsNativeLanguage', 'ldr_ela_read_flu', 'accomod_native_lang', 'yn', None),
        ('1', 'lz_csv', 'AdministrationDirectionsReadAloudinStudentsNativeLanguage', 'ldr_ela_read_flu', 'accomod_loud_native_lang', 'clean', None),
        ('1', 'lz_csv', 'MathematicsResponseEL', 'ldr_ela_read_flu', 'accomod_math_rsp', 'clean', None),
        ('1', 'lz_csv', 'TranslationoftheMathematicsAssessmentinTexttoSpeech', 'ldr_ela_read_flu', 'accomod_math_text_2_speech', 'clean', None),
        ('1', 'lz_csv', 'TranslationoftheMathematicsAssessmentOnline', 'ldr_ela_read_flu', 'accomod_math_trans_online', 'clean', None),
        ('1', 'lz_csv', 'WordtoWordDictionaryEnglishNativeLanguage', 'ldr_ela_read_flu', 'accomod_w_2_w_dict', 'yn', None),
        ('1', 'lz_csv', 'ExtendedTime', 'ldr_ela_read_flu', 'accomod_extend_time', 'clean', None),
        ('1', 'lz_csv', 'AlternateRepresentationPaperTest', 'ldr_ela_read_flu', 'accomod_alt_paper_test', 'yn', None),
        ('1', 'lz_csv', 'TranslationoftheMathematicsAssessmentinPaper', 'ldr_ela_read_flu', 'accomod_paper_trans_math', 'clean', None),
        ('1', 'lz_csv', 'HumanReaderorHumanSigner', 'ldr_ela_read_flu', 'accomod_human_read_sign', 'clean', None),
        ('1', 'lz_csv', 'LargePrint', 'ldr_ela_read_flu', 'accomod_large_print', 'yn', None),
        ('1', 'lz_csv', 'BraillewithTactileGraphics', 'ldr_ela_read_flu', 'accomod_braille_tactile', 'yn', None),
        ('1', 'lz_csv', 'OptionalStateData2', 'ldr_ela_read_flu', 'opt_state_data2', 'clean', None),
        ('1', 'lz_csv', 'OptionalStateData3', 'ldr_ela_read_flu', 'opt_state_data3', 'clean', None),
        ('1', 'lz_csv', 'OptionalStateData4', 'ldr_ela_read_flu', 'opt_state_data4', 'clean', None),
        ('1', 'lz_csv', 'OptionalStateData5', 'ldr_ela_read_flu', 'opt_state_data5', 'clean', None),
        ('1', 'lz_csv', 'OptionalStateData6', 'ldr_ela_read_flu', 'opt_state_data6', 'clean', None),
        ('1', 'lz_csv', 'OptionalStateData7', 'ldr_ela_read_flu', 'opt_state_data7', 'clean', None),
        ('1', 'lz_csv', 'OptionalStateData8', 'ldr_ela_read_flu', 'opt_state_data8', 'clean', None),
        ('1', 'lz_csv', 'Period', 'ldr_ela_read_flu', 'poy', 'clean', None),
        ('1', 'lz_csv', 'TestCode', 'ldr_ela_read_flu', 'test_code', 'clean', None),
        ('1', 'lz_csv', 'AssessmentGrade', 'ldr_ela_read_flu', 'asmt_grade', 'clean', None),
        ('1', 'lz_csv', 'Subject', 'ldr_ela_read_flu', 'asmt_subject', 'clean', None),
        ('1', 'lz_csv', 'WPM', 'ldr_ela_read_flu', 'wpm', 'clean', None),
        ('1', 'lz_csv', 'WCPM', 'ldr_ela_read_flu', 'wcpm', 'clean', None),
        ('1', 'lz_csv', 'Accuracy', 'ldr_ela_read_flu', 'accuracy', 'clean', None),
        ('1', 'lz_csv', 'Expressiveness', 'ldr_ela_read_flu', 'express', 'clean', None),
        ('1', 'lz_csv', 'Passage1Type', 'ldr_ela_read_flu', 'pass1_type', 'clean', None),
        ('1', 'lz_csv', 'Passage2Type', 'ldr_ela_read_flu', 'pass2_type', 'clean', None),
        ('1', 'lz_csv', 'Passage3Type', 'ldr_ela_read_flu', 'pass3_type', 'clean', None),

        # Loader to Staging
        ('2', 'ldr_ela_read_flu', 'batch_guid', 'stg_ela_read_flu', 'batch_guid', None, None),
        ('2', 'ldr_ela_read_flu', 'record_num', 'stg_ela_read_flu', 'record_num', None, None),
        ('2', 'ldr_ela_read_flu', 'record_type', 'stg_ela_read_flu', 'record_type', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'is_multi_rec', 'stg_ela_read_flu', 'is_multi_rec', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'is_reported_diagnostic', 'stg_ela_read_flu', 'is_reported_diagnostic', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'year', 'stg_ela_read_flu', 'year', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'asmt_date', 'stg_ela_read_flu', 'asmt_date', None, TO_DATE),
        ('2', 'ldr_ela_read_flu', 'state_code', 'stg_ela_read_flu', 'state_code', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'resp_dist_id', 'stg_ela_read_flu', 'resp_dist_id', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'resp_dist_name', 'stg_ela_read_flu', 'resp_dist_name', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'resp_school_id', 'stg_ela_read_flu', 'resp_school_id', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'resp_school_name', 'stg_ela_read_flu', 'resp_school_name', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'student_parcc_id', 'stg_ela_read_flu', 'student_parcc_id', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'student_state_id', 'stg_ela_read_flu', 'student_state_id', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'student_local_id', 'stg_ela_read_flu', 'student_local_id', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'student_first_name', 'stg_ela_read_flu', 'student_first_name', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'student_middle_name', 'stg_ela_read_flu', 'student_middle_name', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'student_last_name', 'stg_ela_read_flu', 'student_last_name', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'student_sex', 'stg_ela_read_flu', 'student_sex', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'student_dob', 'stg_ela_read_flu', 'student_dob', None, TO_DATE),
        ('2', 'ldr_ela_read_flu', 'opt_state_data1', 'stg_ela_read_flu', 'opt_state_data1', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'student_grade', 'stg_ela_read_flu', 'student_grade', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'ethn_hisp_latino', 'stg_ela_read_flu', 'ethn_hisp_latino', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'ethn_indian_alaska', 'stg_ela_read_flu', 'ethn_indian_alaska', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'ethn_asian', 'stg_ela_read_flu', 'ethn_asian', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'ethn_black', 'stg_ela_read_flu', 'ethn_black', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'ethn_hawai', 'stg_ela_read_flu', 'ethn_hawai', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'ethn_white', 'stg_ela_read_flu', 'ethn_white', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'ethn_two_or_more_races', 'stg_ela_read_flu', 'ethn_two_or_more_races', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'ethn_filler', 'stg_ela_read_flu', 'ethn_filler', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'ethnicity', 'stg_ela_read_flu', 'ethnicity', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'ell', 'stg_ela_read_flu', 'ell', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'lep_status', 'stg_ela_read_flu', 'lep_status', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'gift_talent', 'stg_ela_read_flu', 'gift_talent', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'migrant_status', 'stg_ela_read_flu', 'migrant_status', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'econo_disadvantage', 'stg_ela_read_flu', 'econo_disadvantage', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'disabil_student', 'stg_ela_read_flu', 'disabil_student', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'primary_disabil_type', 'stg_ela_read_flu', 'primary_disabil_type', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'staff_id', 'stg_ela_read_flu', 'staff_id', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_ell', 'stg_ela_read_flu', 'accomod_ell', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_504', 'stg_ela_read_flu', 'accomod_504', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_ind_ed', 'stg_ela_read_flu', 'accomod_ind_ed', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_freq_breaks', 'stg_ela_read_flu', 'accomod_freq_breaks', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_alt_location', 'stg_ela_read_flu', 'accomod_alt_location', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_small_group', 'stg_ela_read_flu', 'accomod_small_group', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_special_equip', 'stg_ela_read_flu', 'accomod_special_equip', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_spec_area', 'stg_ela_read_flu', 'accomod_spec_area', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_time_day', 'stg_ela_read_flu', 'accomod_time_day', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_answer_mask', 'stg_ela_read_flu', 'accomod_answer_mask', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_color_contrast', 'stg_ela_read_flu', 'accomod_color_contrast', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_text_2_speech_math', 'stg_ela_read_flu', 'accomod_text_2_speech_math', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_read_math', 'stg_ela_read_flu', 'accomod_read_math', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_asl_video', 'stg_ela_read_flu', 'accomod_asl_video', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_screen_reader', 'stg_ela_read_flu', 'accomod_screen_reader', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_close_capt_ela', 'stg_ela_read_flu', 'accomod_close_capt_ela', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_read_ela', 'stg_ela_read_flu', 'accomod_read_ela', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_braille_ela', 'stg_ela_read_flu', 'accomod_braille_ela', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_tactile_graph', 'stg_ela_read_flu', 'accomod_tactile_graph', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_text_2_speech_ela', 'stg_ela_read_flu', 'accomod_text_2_speech_ela', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_answer_rec', 'stg_ela_read_flu', 'accomod_answer_rec', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_braille_resp', 'stg_ela_read_flu', 'accomod_braille_resp', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_calculator', 'stg_ela_read_flu', 'accomod_calculator', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_construct_resp_ela', 'stg_ela_read_flu', 'accomod_construct_resp_ela', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_select_resp_ela', 'stg_ela_read_flu', 'accomod_select_resp_ela', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_math_resp', 'stg_ela_read_flu', 'accomod_math_resp', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_monitor_test_resp', 'stg_ela_read_flu', 'accomod_monitor_test_resp', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_word_predict', 'stg_ela_read_flu', 'accomod_word_predict', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_native_lang', 'stg_ela_read_flu', 'accomod_native_lang', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_loud_native_lang', 'stg_ela_read_flu', 'accomod_loud_native_lang', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_math_rsp', 'stg_ela_read_flu', 'accomod_math_rsp', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_math_text_2_speech', 'stg_ela_read_flu', 'accomod_math_text_2_speech', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_math_trans_online', 'stg_ela_read_flu', 'accomod_math_trans_online', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_w_2_w_dict', 'stg_ela_read_flu', 'accomod_w_2_w_dict', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_extend_time', 'stg_ela_read_flu', 'accomod_extend_time', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_alt_paper_test', 'stg_ela_read_flu', 'accomod_alt_paper_test', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_paper_trans_math', 'stg_ela_read_flu', 'accomod_paper_trans_math', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_human_read_sign', 'stg_ela_read_flu', 'accomod_human_read_sign', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_large_print', 'stg_ela_read_flu', 'accomod_large_print', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'accomod_braille_tactile', 'stg_ela_read_flu', 'accomod_braille_tactile', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'opt_state_data2', 'stg_ela_read_flu', 'opt_state_data2', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'opt_state_data3', 'stg_ela_read_flu', 'opt_state_data3', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'opt_state_data4', 'stg_ela_read_flu', 'opt_state_data4', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'opt_state_data5', 'stg_ela_read_flu', 'opt_state_data5', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'opt_state_data6', 'stg_ela_read_flu', 'opt_state_data6', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'opt_state_data7', 'stg_ela_read_flu', 'opt_state_data7', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'opt_state_data8', 'stg_ela_read_flu', 'opt_state_data8', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'poy', 'stg_ela_read_flu', 'poy', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'test_code', 'stg_ela_read_flu', 'test_code', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'asmt_grade', 'stg_ela_read_flu', 'asmt_grade', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'asmt_subject', 'stg_ela_read_flu', 'asmt_subject', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'wpm', 'stg_ela_read_flu', 'wpm', None, TO_NUMERIC50),
        ('2', 'ldr_ela_read_flu', 'wcpm', 'stg_ela_read_flu', 'wcpm', None, TO_NUMERIC50),
        ('2', 'ldr_ela_read_flu', 'accuracy', 'stg_ela_read_flu', 'accuracy', None, TO_NUMERIC30),
        ('2', 'ldr_ela_read_flu', 'express', 'stg_ela_read_flu', 'express', None, TO_NUMERIC20),
        ('2', 'ldr_ela_read_flu', 'pass1_type', 'stg_ela_read_flu', 'pass1_type', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'pass2_type', 'stg_ela_read_flu', 'pass2_type', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'pass3_type', 'stg_ela_read_flu', 'pass3_type', None, TO_SUBSTRING),
        ('2', 'ldr_ela_read_flu', 'create_date', 'stg_ela_read_flu', 'create_date', None, None),

        # Staging to Integration
        ('3', 'stg_ela_read_flu', 'batch_guid', 'int_ela_read_flu', 'batch_guid', None, None),
        ('3', 'stg_ela_read_flu', 'record_num', 'int_ela_read_flu', 'record_num', None, None),
        ('3', 'stg_ela_read_flu', 'record_type', 'int_ela_read_flu', 'record_type', None, None),
        ('3', 'stg_ela_read_flu', 'is_multi_rec', 'int_ela_read_flu', 'is_multi_rec', None, None),
        ('3', 'stg_ela_read_flu', 'is_reported_diagnostic', 'int_ela_read_flu', 'is_reported_diagnostic', None, None),
        ('3', 'stg_ela_read_flu', 'year', 'int_ela_read_flu', 'year', None, None),
        ('3', 'stg_ela_read_flu', 'asmt_date', 'int_ela_read_flu', 'asmt_date', None, None),
        ('3', 'stg_ela_read_flu', 'state_code', 'int_ela_read_flu', 'state_code', None, None),
        ('3', 'stg_ela_read_flu', 'resp_dist_id', 'int_ela_read_flu', 'resp_dist_id', None, None),
        ('3', 'stg_ela_read_flu', 'resp_dist_name', 'int_ela_read_flu', 'resp_dist_name', None, None),
        ('3', 'stg_ela_read_flu', 'resp_school_id', 'int_ela_read_flu', 'resp_school_id', None, None),
        ('3', 'stg_ela_read_flu', 'resp_school_name', 'int_ela_read_flu', 'resp_school_name', None, None),
        ('3', 'stg_ela_read_flu', 'student_parcc_id', 'int_ela_read_flu', 'student_parcc_id', None, None),
        ('3', 'stg_ela_read_flu', 'student_state_id', 'int_ela_read_flu', 'student_state_id', None, None),
        ('3', 'stg_ela_read_flu', 'student_local_id', 'int_ela_read_flu', 'student_local_id', None, None),
        ('3', 'stg_ela_read_flu', 'student_first_name', 'int_ela_read_flu', 'student_first_name', None, None),
        ('3', 'stg_ela_read_flu', 'student_middle_name', 'int_ela_read_flu', 'student_middle_name', None, None),
        ('3', 'stg_ela_read_flu', 'student_last_name', 'int_ela_read_flu', 'student_last_name', None, None),
        ('3', 'stg_ela_read_flu', 'student_sex', 'int_ela_read_flu', 'student_sex', None, None),
        ('3', 'stg_ela_read_flu', 'student_dob', 'int_ela_read_flu', 'student_dob', None, None),
        ('3', 'stg_ela_read_flu', 'opt_state_data1', 'int_ela_read_flu', 'opt_state_data1', None, None),
        ('3', 'stg_ela_read_flu', 'student_grade', 'int_ela_read_flu', 'student_grade', None, None),
        ('3', 'stg_ela_read_flu', 'ethn_hisp_latino', 'int_ela_read_flu', 'ethn_hisp_latino', None, None),
        ('3', 'stg_ela_read_flu', 'ethn_indian_alaska', 'int_ela_read_flu', 'ethn_indian_alaska', None, None),
        ('3', 'stg_ela_read_flu', 'ethn_asian', 'int_ela_read_flu', 'ethn_asian', None, None),
        ('3', 'stg_ela_read_flu', 'ethn_black', 'int_ela_read_flu', 'ethn_black', None, None),
        ('3', 'stg_ela_read_flu', 'ethn_hawai', 'int_ela_read_flu', 'ethn_hawai', None, None),
        ('3', 'stg_ela_read_flu', 'ethn_white', 'int_ela_read_flu', 'ethn_white', None, None),
        ('3', 'stg_ela_read_flu', 'ethn_two_or_more_races', 'int_ela_read_flu', 'ethn_two_or_more_races', None, None),
        ('3', 'stg_ela_read_flu', 'ethn_filler', 'int_ela_read_flu', 'ethn_filler', None, None),
        ('3', 'stg_ela_read_flu', 'ethnicity', 'int_ela_read_flu', 'ethnicity', None, None),
        ('3', 'stg_ela_read_flu', 'ell', 'int_ela_read_flu', 'ell', None, None),
        ('3', 'stg_ela_read_flu', 'lep_status', 'int_ela_read_flu', 'lep_status', None, None),
        ('3', 'stg_ela_read_flu', 'gift_talent', 'int_ela_read_flu', 'gift_talent', None, None),
        ('3', 'stg_ela_read_flu', 'migrant_status', 'int_ela_read_flu', 'migrant_status', None, None),
        ('3', 'stg_ela_read_flu', 'econo_disadvantage', 'int_ela_read_flu', 'econo_disadvantage', None, None),
        ('3', 'stg_ela_read_flu', 'disabil_student', 'int_ela_read_flu', 'disabil_student', None, None),
        ('3', 'stg_ela_read_flu', 'primary_disabil_type', 'int_ela_read_flu', 'primary_disabil_type', None, None),
        ('3', 'stg_ela_read_flu', 'staff_id', 'int_ela_read_flu', 'staff_id', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_ell', 'int_ela_read_flu', 'accomod_ell', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_504', 'int_ela_read_flu', 'accomod_504', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_ind_ed', 'int_ela_read_flu', 'accomod_ind_ed', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_freq_breaks', 'int_ela_read_flu', 'accomod_freq_breaks', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_alt_location', 'int_ela_read_flu', 'accomod_alt_location', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_small_group', 'int_ela_read_flu', 'accomod_small_group', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_special_equip', 'int_ela_read_flu', 'accomod_special_equip', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_spec_area', 'int_ela_read_flu', 'accomod_spec_area', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_time_day', 'int_ela_read_flu', 'accomod_time_day', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_answer_mask', 'int_ela_read_flu', 'accomod_answer_mask', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_color_contrast', 'int_ela_read_flu', 'accomod_color_contrast', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_text_2_speech_math', 'int_ela_read_flu', 'accomod_text_2_speech_math', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_read_math', 'int_ela_read_flu', 'accomod_read_math', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_asl_video', 'int_ela_read_flu', 'accomod_asl_video', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_screen_reader', 'int_ela_read_flu', 'accomod_screen_reader', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_close_capt_ela', 'int_ela_read_flu', 'accomod_close_capt_ela', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_read_ela', 'int_ela_read_flu', 'accomod_read_ela', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_braille_ela', 'int_ela_read_flu', 'accomod_braille_ela', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_tactile_graph', 'int_ela_read_flu', 'accomod_tactile_graph', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_text_2_speech_ela', 'int_ela_read_flu', 'accomod_text_2_speech_ela', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_answer_rec', 'int_ela_read_flu', 'accomod_answer_rec', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_braille_resp', 'int_ela_read_flu', 'accomod_braille_resp', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_calculator', 'int_ela_read_flu', 'accomod_calculator', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_construct_resp_ela', 'int_ela_read_flu', 'accomod_construct_resp_ela', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_select_resp_ela', 'int_ela_read_flu', 'accomod_select_resp_ela', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_math_resp', 'int_ela_read_flu', 'accomod_math_resp', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_monitor_test_resp', 'int_ela_read_flu', 'accomod_monitor_test_resp', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_word_predict', 'int_ela_read_flu', 'accomod_word_predict', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_native_lang', 'int_ela_read_flu', 'accomod_native_lang', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_loud_native_lang', 'int_ela_read_flu', 'accomod_loud_native_lang', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_math_rsp', 'int_ela_read_flu', 'accomod_math_rsp', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_math_text_2_speech', 'int_ela_read_flu', 'accomod_math_text_2_speech', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_math_trans_online', 'int_ela_read_flu', 'accomod_math_trans_online', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_w_2_w_dict', 'int_ela_read_flu', 'accomod_w_2_w_dict', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_extend_time', 'int_ela_read_flu', 'accomod_extend_time', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_alt_paper_test', 'int_ela_read_flu', 'accomod_alt_paper_test', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_paper_trans_math', 'int_ela_read_flu', 'accomod_paper_trans_math', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_human_read_sign', 'int_ela_read_flu', 'accomod_human_read_sign', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_large_print', 'int_ela_read_flu', 'accomod_large_print', None, None),
        ('3', 'stg_ela_read_flu', 'accomod_braille_tactile', 'int_ela_read_flu', 'accomod_braille_tactile', None, None),
        ('3', 'stg_ela_read_flu', 'opt_state_data2', 'int_ela_read_flu', 'opt_state_data2', None, None),
        ('3', 'stg_ela_read_flu', 'opt_state_data3', 'int_ela_read_flu', 'opt_state_data3', None, None),
        ('3', 'stg_ela_read_flu', 'opt_state_data4', 'int_ela_read_flu', 'opt_state_data4', None, None),
        ('3', 'stg_ela_read_flu', 'opt_state_data5', 'int_ela_read_flu', 'opt_state_data5', None, None),
        ('3', 'stg_ela_read_flu', 'opt_state_data6', 'int_ela_read_flu', 'opt_state_data6', None, None),
        ('3', 'stg_ela_read_flu', 'opt_state_data7', 'int_ela_read_flu', 'opt_state_data7', None, None),
        ('3', 'stg_ela_read_flu', 'opt_state_data8', 'int_ela_read_flu', 'opt_state_data8', None, None),
        ('3', 'stg_ela_read_flu', 'poy', 'int_ela_read_flu', 'poy', None, None),
        ('3', 'stg_ela_read_flu', 'test_code', 'int_ela_read_flu', 'test_code', None, None),
        ('3', 'stg_ela_read_flu', 'asmt_grade', 'int_ela_read_flu', 'asmt_grade', None, None),
        ('3', 'stg_ela_read_flu', 'asmt_subject', 'int_ela_read_flu', 'asmt_subject', None, None),
        ('3', 'stg_ela_read_flu', 'wpm', 'int_ela_read_flu', 'wpm', None, None),
        ('3', 'stg_ela_read_flu', 'wcpm', 'int_ela_read_flu', 'wcpm', None, None),
        ('3', 'stg_ela_read_flu', 'accuracy', 'int_ela_read_flu', 'accuracy', None, None),
        ('3', 'stg_ela_read_flu', 'express', 'int_ela_read_flu', 'express', None, None),
        ('3', 'stg_ela_read_flu', 'pass1_type', 'int_ela_read_flu', 'passage1_type', None, None),
        ('3', 'stg_ela_read_flu', 'pass2_type', 'int_ela_read_flu', 'passage2_type', None, None),
        ('3', 'stg_ela_read_flu', 'pass3_type', 'int_ela_read_flu', 'passage3_type', None, None),
        ('3', 'stg_ela_read_flu', 'create_date', 'int_ela_read_flu', 'create_date', None, None),
        ('3', 'stg_ela_read_flu', "'C'", 'int_ela_read_flu', 'rec_status', None, None),
    ]
}
