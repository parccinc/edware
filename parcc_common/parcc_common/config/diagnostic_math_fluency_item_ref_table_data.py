TO_SMALLINT = "cast({src_column} as smallint)"
TO_THREE_DIGITS = "to_number({src_column},'999')"
TO_FOUR_DIGITS = "to_number({src_column},'9999')"
TO_SUBSTRING = 'substr({src_column}, 1, {length})'

COLUMNS = ('phase', 'source_table', 'source_column', 'target_table', 'target_column', 'transformation_rule', 'stored_proc_name')


ref_table_conf = {
    'column_definitions': COLUMNS,
    'column_mappings': [
        # Columns:
        # column_map_key, phase, source_table, source_column, target_table, target_column, transformation_rule, stored_proc_name, stored_proc_created_date, create_date
        # CSV to loader
        ('1', 'lz_csv', 'AssessmentAttemptGuid', 'ldr_item_math_flu', 'asmt_attempt_guid', 'clean', None),
        ('1', 'lz_csv', 'StudentGuid', 'ldr_item_math_flu', 'student_parcc_id', 'clean', None),
        ('1', 'lz_csv', 'ItemGuid', 'ldr_item_math_flu', 'item_guid', 'clean', None),
        ('1', 'lz_csv', 'ResponseEntered', 'ldr_item_math_flu', 'rsp_entered', 'clean', None),
        ('1', 'lz_csv', 'ScorePoints', 'ldr_item_math_flu', 'student_item_score', 'clean', None),
        ('1', 'lz_csv', 'ItemSequence', 'ldr_item_math_flu', 'item_seq', 'clean', None),
        ('1', 'lz_csv', 'ItemTime', 'ldr_item_math_flu', 'item_time', 'clean', None),

        # Loader to Staging
        ('2', 'ldr_item_math_flu', 'batch_guid', 'stg_item_math_flu', 'batch_guid', None, None),
        ('2', 'ldr_item_math_flu', 'record_num', 'stg_item_math_flu', 'record_num', None, None),
        ('2', 'ldr_item_math_flu', 'create_date', 'stg_item_math_flu', 'create_date', None, None),
        ('2', 'ldr_item_math_flu', 'asmt_attempt_guid', 'stg_item_math_flu', 'asmt_attempt_guid', None, TO_SUBSTRING),
        ('2', 'ldr_item_math_flu', 'student_parcc_id', 'stg_item_math_flu', 'student_parcc_id', None, TO_SUBSTRING),
        ('2', 'ldr_item_math_flu', 'item_guid', 'stg_item_math_flu', 'item_guid', None, TO_SUBSTRING),
        ('2', 'ldr_item_math_flu', 'rsp_entered', 'stg_item_math_flu', 'rsp_entered', None, TO_SMALLINT),
        ('2', 'ldr_item_math_flu', 'student_item_score', 'stg_item_math_flu', 'student_item_score', None, TO_THREE_DIGITS),
        ('2', 'ldr_item_math_flu', 'item_seq', 'stg_item_math_flu', 'item_seq', None, TO_SMALLINT),
        ('2', 'ldr_item_math_flu', 'item_time', 'stg_item_math_flu', 'item_time', 'clean', TO_FOUR_DIGITS),

        # Loader to Staging
        ('3', 'stg_item_math_flu', 'batch_guid', 'int_item_math_flu', 'batch_guid', None, None),
        ('3', 'stg_item_math_flu', 'record_num', 'int_item_math_flu', 'record_num', None, None),
        ('3', 'stg_item_math_flu', 'create_date', 'int_item_math_flu', 'create_date', None, None),
        ('3', 'stg_item_math_flu', 'asmt_attempt_guid', 'int_item_math_flu', 'asmt_attempt_guid', None, None),
        ('3', 'stg_item_math_flu', 'student_parcc_id', 'int_item_math_flu', 'student_parcc_id', None, None),
        ('3', 'stg_item_math_flu', 'item_guid', 'int_item_math_flu', 'item_guid', None, None),
        ('3', 'stg_item_math_flu', 'rsp_entered', 'int_item_math_flu', 'rsp_entered', None, None),
        ('3', 'stg_item_math_flu', 'student_item_score', 'int_item_math_flu', 'student_item_score', None, None),
        ('3', 'stg_item_math_flu', 'item_seq', 'int_item_math_flu', 'item_seq', None, None),
        ('3', 'stg_item_math_flu', 'item_time', 'int_item_math_flu', 'item_time', 'clean', None),
    ]
}
