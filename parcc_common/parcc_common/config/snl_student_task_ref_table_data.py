TO_NUMERIC40 = "cast({src_column} as decimal (4, 0))"
TO_SUBSTRING = 'substr({src_column}, 1, {length})'

COLUMNS = ('phase', 'source_table', 'source_column', 'target_table', 'target_column', 'transformation_rule', 'stored_proc_name')


ref_table_conf = {
    'column_definitions': COLUMNS,
    'column_mappings': [
        # Columns:
        # column_map_key, phase, source_table, source_column, target_table, target_column, transformation_rule, stored_proc_name, stored_proc_created_date, create_date
        # CSV to loader
        ('1', 'lz_csv', 'AssessmentRegistrationTestAttemptIdentifier', 'ldr_student_task_snl', 'asmt_attempt_guid', 'clean', None),
        ('1', 'lz_csv', 'PARCCStudentIdentifier', 'ldr_student_task_snl', 'student_parcc_id', 'clean', None),
        ('1', 'lz_csv', 'TaskGUID', 'ldr_student_task_snl', 'task_guid', 'clean', None),
        ('1', 'lz_csv', 'TaskName', 'ldr_student_task_snl', 'task_name', 'clean', None),
        ('1', 'lz_csv', 'OverallTaskScore', 'ldr_student_task_snl', 'total_task_score', 'clean', None),
        ('1', 'lz_csv', 'Observations', 'ldr_student_task_snl', 'observation', 'clean', None),
        ('1', 'lz_csv', 'DimScore1', 'ldr_student_task_snl', 'dim_score1', 'clean', None),
        ('1', 'lz_csv', 'DimScore2', 'ldr_student_task_snl', 'dim_score2', 'clean', None),
        ('1', 'lz_csv', 'DimScore3', 'ldr_student_task_snl', 'dim_score3', 'clean', None),
        ('1', 'lz_csv', 'DimScore4', 'ldr_student_task_snl', 'dim_score4', 'clean', None),
        ('1', 'lz_csv', 'DimScore5', 'ldr_student_task_snl', 'dim_score5', 'clean', None),
        ('1', 'lz_csv', 'DiscussionEvidenceObserved1', 'ldr_student_task_snl', 'dr_evidence_observ1', 'clean', None),
        ('1', 'lz_csv', 'DiscussionEvidenceObserved2', 'ldr_student_task_snl', 'dr_evidence_observ2', 'clean', None),
        ('1', 'lz_csv', 'DiscussionEvidenceObserved3', 'ldr_student_task_snl', 'dr_evidence_observ3', 'clean', None),
        ('1', 'lz_csv', 'DiscussionEvidenceObserved4', 'ldr_student_task_snl', 'dr_evidence_observ4', 'clean', None),
        ('1', 'lz_csv', 'DiscussionEvidenceObserved5', 'ldr_student_task_snl', 'dr_evidence_observ5', 'clean', None),

        # Loader to Staging
        ('2', 'ldr_student_task_snl', 'batch_guid', 'stg_student_task_snl', 'batch_guid', None, None),
        ('2', 'ldr_student_task_snl', 'record_num', 'stg_student_task_snl', 'record_num', None, None),
        ('2', 'ldr_student_task_snl', 'create_date', 'stg_student_task_snl', 'create_date', None, None),
        ('2', 'ldr_student_task_snl', 'asmt_attempt_guid', 'stg_student_task_snl', 'asmt_attempt_guid', None, None),
        ('2', 'ldr_student_task_snl', 'student_parcc_id', 'stg_student_task_snl', 'student_parcc_id', None, None),
        ('2', 'ldr_student_task_snl', 'task_guid', 'stg_student_task_snl', 'task_guid', None, None),
        ('2', 'ldr_student_task_snl', 'task_name', 'stg_student_task_snl', 'task_name', None, None),
        ('2', 'ldr_student_task_snl', 'total_task_score', 'stg_student_task_snl', 'total_task_score', None, TO_NUMERIC40),
        ('2', 'ldr_student_task_snl', 'observation', 'stg_student_task_snl', 'observation', None, None),
        ('2', 'ldr_student_task_snl', 'dim_score1', 'stg_student_task_snl', 'dim_score1', None, TO_NUMERIC40),
        ('2', 'ldr_student_task_snl', 'dim_score2', 'stg_student_task_snl', 'dim_score2', None, TO_NUMERIC40),
        ('2', 'ldr_student_task_snl', 'dim_score3', 'stg_student_task_snl', 'dim_score3', None, TO_NUMERIC40),
        ('2', 'ldr_student_task_snl', 'dim_score4', 'stg_student_task_snl', 'dim_score4', None, TO_NUMERIC40),
        ('2', 'ldr_student_task_snl', 'dim_score5', 'stg_student_task_snl', 'dim_score5', None, TO_NUMERIC40),
        ('2', 'ldr_student_task_snl', 'dr_evidence_observ1', 'stg_student_task_snl', 'dr_evidence_observ1', None, TO_SUBSTRING),
        ('2', 'ldr_student_task_snl', 'dr_evidence_observ2', 'stg_student_task_snl', 'dr_evidence_observ2', None, TO_SUBSTRING),
        ('2', 'ldr_student_task_snl', 'dr_evidence_observ3', 'stg_student_task_snl', 'dr_evidence_observ3', None, TO_SUBSTRING),
        ('2', 'ldr_student_task_snl', 'dr_evidence_observ4', 'stg_student_task_snl', 'dr_evidence_observ4', None, TO_SUBSTRING),
        ('2', 'ldr_student_task_snl', 'dr_evidence_observ5', 'stg_student_task_snl', 'dr_evidence_observ5', None, TO_SUBSTRING),

        # Staging to Integration
        ('3', 'stg_student_task_snl', 'batch_guid', 'int_student_task_snl', 'batch_guid', None, None),
        ('3', 'stg_student_task_snl', 'record_num', 'int_student_task_snl', 'record_num', None, None),
        ('3', 'stg_student_task_snl', 'create_date', 'int_student_task_snl', 'create_date', None, None),
        ('3', 'stg_student_task_snl', 'asmt_attempt_guid', 'int_student_task_snl', 'asmt_attempt_guid', None, None),
        ('3', 'stg_student_task_snl', 'student_parcc_id', 'int_student_task_snl', 'student_parcc_id', None, None),
        ('3', 'stg_student_task_snl', 'task_guid', 'int_student_task_snl', 'task_guid', None, None),
        ('3', 'stg_student_task_snl', 'task_name', 'int_student_task_snl', 'task_name', None, None),
        ('3', 'stg_student_task_snl', 'total_task_score', 'int_student_task_snl', 'total_task_score', None, None),
        ('3', 'stg_student_task_snl', 'observation', 'int_student_task_snl', 'observation', None, None),
        ('3', 'stg_student_task_snl', 'dim_score1', 'int_student_task_snl', 'dim_score1', None, None),
        ('3', 'stg_student_task_snl', 'dim_score2', 'int_student_task_snl', 'dim_score2', None, None),
        ('3', 'stg_student_task_snl', 'dim_score3', 'int_student_task_snl', 'dim_score3', None, None),
        ('3', 'stg_student_task_snl', 'dim_score4', 'int_student_task_snl', 'dim_score4', None, None),
        ('3', 'stg_student_task_snl', 'dim_score5', 'int_student_task_snl', 'dim_score5', None, None),
        ('3', 'stg_student_task_snl', 'dr_evidence_observ1', 'int_student_task_snl', 'dr_evidence_observ1', None, None),
        ('3', 'stg_student_task_snl', 'dr_evidence_observ2', 'int_student_task_snl', 'dr_evidence_observ2', None, None),
        ('3', 'stg_student_task_snl', 'dr_evidence_observ3', 'int_student_task_snl', 'dr_evidence_observ3', None, None),
        ('3', 'stg_student_task_snl', 'dr_evidence_observ4', 'int_student_task_snl', 'dr_evidence_observ4', None, None),
        ('3', 'stg_student_task_snl', 'dr_evidence_observ5', 'int_student_task_snl', 'dr_evidence_observ5', None, None),
    ]
}
