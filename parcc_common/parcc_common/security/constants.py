'''
Created on May 9, 2013

@author: dip
'''


class RolesConstants():
    '''
    Defined roles
    '''
    GENERAL = 'GENERAL'
    PII = 'PII'
    PII_ANALYTICS = 'PII_ANALYTICS'
    SF_EXTRACT = 'SF_EXTRACT'
    RF_EXTRACT = 'RF_EXTRACT'
    PF_EXTRACT = 'PF_EXTRACT'
    PSRD_EXTRACT = 'PSRD_EXTRACT'
    CDS_EXTRACT = 'CDS_EXTRACT'
    SUPER_USER = 'SUPER_USER'
    # For no role in memberOf in SAML response
    NONE = 'NONE'
