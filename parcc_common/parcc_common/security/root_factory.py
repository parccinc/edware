'''
Created on Feb 11, 2013

@author: dip
'''
from pyramid.security import Allow

from parcc_common.security.constants import RolesConstants


class Permission:
    VIEW = 'view'
    LOGOUT = 'logout'
    DOWNLOAD = 'download'
    DEFAULT = 'default'
    SUPER_ADMIN_RIGHTS = 'super_admin_rights'


class RootFactory(object):
    '''
    Called on every request sent to the application by pyramid.
    The root factory returns the traversal root of an application.
    Right now, we're saying that all roles have permission.
    '''
    __acl__ = [(Allow, RolesConstants.GENERAL, (Permission.VIEW, Permission.LOGOUT, Permission.DOWNLOAD, Permission.DEFAULT)),
               (Allow, RolesConstants.PII, (Permission.VIEW, Permission.LOGOUT)),
               (Allow, RolesConstants.PII_ANALYTICS, (Permission.VIEW, Permission.LOGOUT)),
               (Allow, RolesConstants.SF_EXTRACT, (Permission.VIEW, Permission.LOGOUT)),
               (Allow, RolesConstants.RF_EXTRACT, (Permission.VIEW, Permission.LOGOUT)),
               (Allow, RolesConstants.PF_EXTRACT, (Permission.VIEW, Permission.LOGOUT)),
               (Allow, RolesConstants.PSRD_EXTRACT, (Permission.VIEW, Permission.LOGOUT)),
               (Allow, RolesConstants.CDS_EXTRACT, (Permission.VIEW, Permission.LOGOUT)),
               (Allow, RolesConstants.SUPER_USER, (Permission.VIEW, Permission.LOGOUT, Permission.SUPER_ADMIN_RIGHTS)),
               # For no role in memberOf in SAML response
               # Ideally, this should be in edauth
               (Allow, RolesConstants.NONE, Permission.LOGOUT)]

    def __init__(self, request):
        pass
