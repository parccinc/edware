__author__ = 'npandey'


class ExtractConstants():
    SUMMATIVE_ASMT_RESULT = "summ_asmt_result"
    SUMMATIVE_RIF = "item_student_score"
    PSYCHOMETRIC = "p_data"
    CDS = "cds_data"

    EXTRACT_TYPE_RIF_ELA = SUMMATIVE_RIF + '.ELA'
    EXTRACT_TYPE_RIF_MATH = SUMMATIVE_RIF + '.Math'

    EXTRACT_FILE_TYPE_MAPPING = {
        EXTRACT_TYPE_RIF_ELA: 'ela_item_student_score',
        EXTRACT_TYPE_RIF_MATH: 'math_item_student_score',
        PSYCHOMETRIC: 'p_data'
    }


class JSONConstants():
    """
    Constants for json fields used by edextract
    """

    # JSON File Constants
    FILE_TYPE = 'file_type'
    DATA_FILE_TYPE = 'DataFileType'
    FILE_NAME = 'file_name'
    TEST_CODE = 'test_code'
    SUMMATIVE_PERIOD = 'summative_period'
    TEST_LEVEL_ID = 'test_perf_lvl_id'
    TEST_LEVEL_LABEL = 'test_perf_lvl_cutpoint_label'
    TEST_LEVEL_LOW = 'test_perf_lvl_cutpoint_low'
    TEST_LEVEL_UPPER = 'test_perf_lvl_cutpoint_upper'
    TEST_LEVELS = 'test_levels'
    SUMMATIVE_ASSESSMENT_TYPE = 'asmt_data'
    ASMT_GUID = 'asmt_guid'
    ADMIN_GUID = 'admin_guid'

    # JSON DB Fields
    DB_TEST_CODE = 'test_code'
    DB_SUMMATIVE_PERIOD = 'sum_period'
    DB_TEST_LEVEL_ID = 'perf_lvl_id'
    DB_TEST_LEVEL_LABEL = 'cutpoint_label'
    DB_TEST_LEVEL_LOW = 'cutpoint_low'
    DB_TEST_LEVEL_UPPER = 'cutpoint_upper'
