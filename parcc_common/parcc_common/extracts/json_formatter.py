import os
from parcc_common.extracts.constants import JSONConstants, ExtractConstants


def summative_json_formatter(results, output_file):
    test_code = results[0][JSONConstants.DB_TEST_CODE]
    sum_period = results[0][JSONConstants.DB_SUMMATIVE_PERIOD]
    json_body = {
        JSONConstants.FILE_TYPE: JSONConstants.SUMMATIVE_ASSESSMENT_TYPE,
        JSONConstants.FILE_NAME: os.path.basename(output_file),
        JSONConstants.TEST_CODE: test_code,
        JSONConstants.SUMMATIVE_PERIOD: sum_period
    }
    test_levels = []
    for result in results:
        test_levels.append({
            JSONConstants.TEST_LEVEL_ID: result[JSONConstants.DB_TEST_LEVEL_ID],
            JSONConstants.TEST_LEVEL_LABEL: result[JSONConstants.DB_TEST_LEVEL_LABEL],
            JSONConstants.TEST_LEVEL_LOW: int(result[JSONConstants.DB_TEST_LEVEL_LOW]),
            JSONConstants.TEST_LEVEL_UPPER: int(result[JSONConstants.DB_TEST_LEVEL_UPPER])
        })
    json_body[JSONConstants.TEST_LEVELS] = test_levels
    return json_body


def basic_json_formatter(extract_type, subject=None):
    if subject:
        extract_type += '.' + subject
    return {JSONConstants.DATA_FILE_TYPE: ExtractConstants.EXTRACT_FILE_TYPE_MAPPING.get(extract_type)}
