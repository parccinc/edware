import unittest
from parcc_common.extracts.constants import JSONConstants
from parcc_common.extracts.json_formatter import summative_json_formatter

__author__ = 'smuhit'


class TestJSONFormatter(unittest.TestCase):

    def test_summative_json_formatter(self):
        results = [{JSONConstants.DB_SUMMATIVE_PERIOD: 'Spring',
                    JSONConstants.DB_TEST_LEVEL_ID: 'id 1',
                    JSONConstants.DB_TEST_LEVEL_LABEL: 'label 1',
                    JSONConstants.DB_TEST_LEVEL_LOW: 0,
                    JSONConstants.DB_TEST_LEVEL_UPPER: 1,
                    JSONConstants.DB_TEST_CODE: 'test'},
                   {JSONConstants.DB_SUMMATIVE_PERIOD: 'Spring',
                    JSONConstants.DB_TEST_LEVEL_ID: 'id 2',
                    JSONConstants.DB_TEST_LEVEL_LABEL: 'label 2',
                    JSONConstants.DB_TEST_LEVEL_LOW: 2,
                    JSONConstants.DB_TEST_LEVEL_UPPER: 3,
                    JSONConstants.DB_TEST_CODE: 'test'}]

        output_file = "/some/dummy/file/path/file.file"
        json_dict = summative_json_formatter(results, output_file)

        self.assertEqual(json_dict[JSONConstants.TEST_CODE], 'test', 'JSON is incorrectly formatted')
        self.assertEqual(json_dict[JSONConstants.SUMMATIVE_PERIOD], 'Spring', 'JSON is incorrectly formatted')
        self.assertEqual(json_dict[JSONConstants.FILE_NAME], 'file.file', 'JSON is incorrectly formatted')
        self.assertEqual(json_dict[JSONConstants.FILE_TYPE], JSONConstants.SUMMATIVE_ASSESSMENT_TYPE, 'JSON is incorrectly formatted')
        self.assertEqual(len(json_dict[JSONConstants.TEST_LEVELS]), 2, 'JSON is incorrectly formatted')
        self.assertEqual(json_dict[JSONConstants.TEST_LEVELS][0][JSONConstants.TEST_LEVEL_LABEL], 'label 1', 'JSON is incorrectly formatted')
        self.assertEqual(json_dict[JSONConstants.TEST_LEVELS][0][JSONConstants.TEST_LEVEL_LOW], 0, 'JSON is incorrectly formatted')
        self.assertEqual(json_dict[JSONConstants.TEST_LEVELS][0][JSONConstants.TEST_LEVEL_UPPER], 1, 'JSON is incorrectly formatted')
        self.assertEqual(json_dict[JSONConstants.TEST_LEVELS][0][JSONConstants.TEST_LEVEL_ID], 'id 1', 'JSON is incorrectly formatted')
        self.assertEqual(json_dict[JSONConstants.TEST_LEVELS][1][JSONConstants.TEST_LEVEL_LABEL], 'label 2', 'JSON is incorrectly formatted')
        self.assertEqual(json_dict[JSONConstants.TEST_LEVELS][1][JSONConstants.TEST_LEVEL_LOW], 2, 'JSON is incorrectly formatted')
        self.assertEqual(json_dict[JSONConstants.TEST_LEVELS][1][JSONConstants.TEST_LEVEL_UPPER], 3, 'JSON is incorrectly formatted')
        self.assertEqual(json_dict[JSONConstants.TEST_LEVELS][1][JSONConstants.TEST_LEVEL_ID], 'id 2', 'JSON is incorrectly formatted')
