'''
Created on Jun 18, 2013

@author: dawu
'''
from urllib.parse import urljoin

from services.tasks.pdf import generate
from services.celery import TIMEOUT
from batch.base import BatchBase


class PDFGenerator(BatchBase):
    '''
    Batch pdf pre-generation trigger.
    '''

    def __init__(self, settings, tenant=None):
        '''
        Constructor with config file path as parameter.

        :param string configFile: path to configuration file
        '''
        super().__init__(settings, tenant)
        self.__base_url = settings.get('pdf.base.url')
        self.__queue_name = settings.get('pdf.batch.job.queue')

    def send_pdf_request(self, relative_pdf_url, file_name):
        # url for generating pdf
        pdf_url = urljoin(self.__base_url, relative_pdf_url)
        # send asynchronous request
        kwargs = {'cookie_name': self.cookie_name, 'timeout': TIMEOUT, 'grayscale': True}
        return generate.apply_async((self.cookie_value, pdf_url, file_name), kwargs=kwargs, queue=self.__queue_name)  # @UndefinedVariable
