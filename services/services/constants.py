'''
Created on Jul 4, 2014

@author: tosako
'''


class ServicesConstants():
    COVER_SHEET_NAME_PREFIX = 'cover_sheet_grade_'
    # minimum cover file size of pdf generated
    MINIMUM_COVER_FILE_SIZE = 200
    PAGECOUNT = 'pageCount'
    MAX_PDFUNITE_FILE = 50
    PDF_MERGE_MAX_RETRY = 10
    PDF_MERGE_RETRY_DELAY = 120
