import unittest
from unittest.mock import patch, DEFAULT
import re
import subprocess
import shlex
import threading
import sqlalchemy

from starmigrate import main


class TestMain(unittest.TestCase):
    def setUp(self):
        self.conf = {'starmigrate.seconds_before_logging_slow_warning': 120,
                     'starmigrate.tenant': 'dino',
                     'starmigrate.dest.db.url': 'does it even matter?',
                     'migrate.dumpfile_location': '/tmp/thisbetterbeempty',
                     'migrate.dumpfile_name_format': 'like you care',
                     'import_schema_name': 'boogie down database blues'}

    @patch.multiple(shlex, split=DEFAULT)
    @patch.multiple(subprocess, Popen=DEFAULT)
    @patch.multiple(main, _load_gz_dump_file=DEFAULT)
    @patch.multiple(main, check_analytics_schema=DEFAULT)
    def test_starmigrate_task_migrate_only(self, **patch_kwags):
        patch_kwags['Popen']().__enter__().communicate.return_value = ('', '')
        patch_kwags['Popen']().__enter__().wait.return_value = 0
        conf = unittest.mock.MagicMock()
        main.starmigrate_task(conf, True)

    @patch.multiple(shlex, split=DEFAULT)
    @patch.multiple(subprocess, Popen=DEFAULT)
    @patch.multiple(main, _load_gz_dump_file=DEFAULT)
    @patch.multiple(main, check_analytics_schema=DEFAULT)
    def test_starmigrate_task_full(self, **patch_kwags):
        patch_kwags['Popen']().__enter__().communicate.return_value = ('', '')
        patch_kwags['Popen']().__enter__().wait.return_value = 0
        conf = unittest.mock.MagicMock()
        main.starmigrate_task(conf, False)

    def test_parse_args_default(self):
        args = main.parse_args([])
        self.assertEquals(args.pidfile, '/opt/edware/run/starmigrate.pid')
        self.assertEquals(args.daemon, False)
        self.assertEquals(args.ini_file, None)

    def test_parse_args_set_all(self):
        args = main.parse_args('-p PID_FILE -d -i INI_FILE'.split())
        self.assertEquals(args.pidfile, 'PID_FILE')
        self.assertEquals(args.daemon, True)
        self.assertEquals(args.ini_file, 'INI_FILE')

    @patch.multiple(main, starmigrate_task=DEFAULT)
    def test_main_default(self, **patch_kwargs):
        args = main.parse_args([])
        main.main(args)

    @patch.multiple(threading, Thread=DEFAULT)
    @patch.multiple(main, run_cron_job=DEFAULT, create_daemon=DEFAULT, _daemon_active=DEFAULT)
    def test_main_daemon(self, **patch_kwargs):
        ini_file = main.get_ini_file()
        self.assertFalse(re.search('\s', ini_file),
                         "this test currently assumes your ini file isn't in a path with whitespace in: '%s'" % (ini_file,))
        args = main.parse_args('-i {ini_file} -d'
                               .format(ini_file=ini_file)
                               .split())
        main.main(args)
