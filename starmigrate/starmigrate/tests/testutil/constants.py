from starmigrate.database.utils.constants import AnalyticsConstants


class AnalyticsTestConstants():
    TEST_NON_STATIC_DIM_TABLE_NAMES = [
        AnalyticsConstants.DIM_RESP_INSTITUTION_TABLE_NAME,
        AnalyticsConstants.DIM_OPT_STATE_DATA_TABLE_NAME,
        AnalyticsConstants.DIM_TEST_ATTEMPT_TABLE_NAME
    ]

    TEST_STATIC_DIM_TABLE_NAMES = [
        AnalyticsConstants.DIM_POY_TABLE_NAME,
        AnalyticsConstants.DIM_REPORT_ROSTER_TABLE_NAME,
        AnalyticsConstants.DIM_RECORD_TYPE_TABLE_NAME,
        AnalyticsConstants.DIM_REPORT_SUPPRESSION_TABLE_NAME,
        AnalyticsConstants.DIM_SUPPRESSION_ACTION_TABLE_NAME,
        AnalyticsConstants.DIM_TEST_CATEGORY_TABLE_NAME
    ]

    TEST_EXCLUDE_COLUMNS = ['create_date', 'rec_id']
