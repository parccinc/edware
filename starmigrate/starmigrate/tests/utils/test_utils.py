# -*- coding: utf-8 -*-
import os
import unittest
from unittest.mock import patch

from starmigrate.utils import utils


class TestUtils(unittest.TestCase):
    def test_get_config_file_none(self):
        if 'CELERY_PROD_CONFIG' in os.environ:
            del os.environ['CELERY_PROD_CONFIG']

        self.assertIsNone(utils.get_config_file())

    def test_get_config_file_doesnt_exist(self):
        nonexistent_file = '/nonexistent_file'
        self.assertFalse(os.path.exists(nonexistent_file),
                         "test file which isn't supposed to exist does, can't test")

        os.environ['CELERY_PROD_CONFIG'] = nonexistent_file

        self.assertIsNone(utils.get_config_file())

    def test_get_config_file_exists(self):
        existing_file = '/'

        self.assertTrue(os.path.exists(existing_file),
                        "root doesn't exist, can't test????")

        os.environ['CELERY_PROD_CONFIG'] = existing_file

        self.assertEqual(utils.get_config_file(), existing_file)

    @patch.multiple(os.path, exists=lambda file: False if file == '/opt/edware/conf/smarter.ini' else os.path.exists)
    def test_get_ini_file_devel(self, **kwargs):
        ini_file = utils.get_ini_file()

        self.assertEquals(os.path.split(ini_file)[-1], 'development.ini')

    @patch.multiple(os.path, exists=lambda file: True)
    def test_get_ini_file_prod(self):
        ini_file = utils.get_ini_file()

        self.assertEquals(ini_file, '/opt/edware/conf/smarter.ini')

    @patch.multiple(os.path, exists=lambda file: False)
    def test_get_ini_file_not_exists(self):
        try:
            utils.get_ini_file()

        except FileNotFoundError:
            return

        else:
            self.fail("get_ini_file should fail if no valid ini file exists")

    def test_read_ini(self):
        self.assertIsNotNone(utils.read_ini(utils.get_ini_file()))
