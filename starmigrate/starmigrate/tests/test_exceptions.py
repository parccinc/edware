# -*- coding: utf-8 -*-
import unittest
from starmigrate import exceptions


class TestExceptions(unittest.TestCase):
    def test_star_migrate_exception(self):
        v = exceptions.StarMigrateException("hi")
        str(v)

    def test_star_migrate_fail_exception(self):
        v = exceptions.StarMigrateFailOnRowsFoundException("what")
        str(v)
