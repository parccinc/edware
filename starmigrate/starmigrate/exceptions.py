class StarMigrateException(Exception):
    """
    generic edmigrate exception
    """

    def __init__(self, msg='StarMigrate Generic Exception'):
        self.__msg = msg

    def __str__(self):
        return repr(self.__msg)


class StarMigrateFailOnRowsFoundException(StarMigrateException):
    """
    Exception that is raised when a SQL statement returns rows and fail_on: rows_found is set.
    """

    def __init__(self, msg='StarMigrate Fail On Rows Found Exception'):
        super().__init__(msg)


class StarMigratePGLoadFailedException(StarMigrateException):
    pass


class StarMigrateKettleNotInstalledException(StarMigrateException):
    pass


class StarMigrateKettleFailedException(StarMigrateException):
    pass
