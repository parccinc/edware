__author__ = 'zvanwijk'

import logging
import logging.config

import os
from copy import deepcopy
from celery import Celery

from edworker.celery import setup_celery
from edcore.database import initialize_db
from edreplicate.database.repmgr_connector import RepMgrDBConnection
from edreplicate.utils.misc import extract_settings
from edreplicate.utils.constants import Constants as EdReplConstants
from edreplicate.player.player import Player, player_task
from starmigrate.utils.utils import get_config_file, get_ini_file, read_ini


# Get INI file
ini_file = get_config_file()
if ini_file is None or not os.path.exists(ini_file):
    ini_file = get_ini_file()

ini_settings = read_ini(ini_file)

logging.config.fileConfig(ini_file)
logger = logging.getLogger('starmigrate')

tenant = ini_settings.get('starmigrate.tenant')
my_settings = deepcopy(EdReplConstants.DEFAULT_SETTINGS)
my_settings.update(extract_settings(ini_settings, 'starmigrate.edreplicate', tenant))
logger.debug('Extracted settings: %s' % my_settings)

celery = Celery()
setup_celery(celery, my_settings)

# Initialize Player
initialize_db(RepMgrDBConnection, my_settings)
player = Player(my_settings)


@celery.task(name=my_settings['player.task_name'], ignore_results=True)
def celery_task(command, nodes):
    player_task(command, nodes)
