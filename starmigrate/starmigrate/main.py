"""
Entry point for migrating data from the Reporting database to the Analytics database

Created on February 6, 2015

@author: zvanwijk
"""
import argparse
import logging
import logging.config
import subprocess
import shlex
import threading
import time
import os

from edcore.utils.utils import read_ini, run_cron_job, create_daemon
from edreplicate.utils.misc import extract_settings
from starmigrate.exceptions import StarMigrateKettleNotInstalledException, StarMigrateKettleFailedException
from starmigrate.utils.utils import get_ini_file, check_analytics_schema
from starmigrate.utils.restoredb import _load_gz_dump_file


logger = logging.getLogger('starmigrate.main')


def _clean_output(text: bytes):
    if isinstance(text, bytes):
        try:
            return str(text, 'utf-8')

        except UnicodeDecodeError:
            return '\n'.join(map(str, text.splitlines()))

    else:
        return text


def run_kettle(starmigrate_conf, logger: logging.Logger):
    kettle_command = shlex.split(starmigrate_conf['starmigrate.kettle_command'])
    if not os.path.exists(kettle_command[0]):
        raise StarMigrateKettleNotInstalledException('kettle not installed @ "%s"' % (kettle_command,))

    stdout, stderr, retval = None, None, None
    try:
        with subprocess.Popen(kettle_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE) as kettle:
            stdout, stderr = kettle.communicate()
            retval = kettle.wait()

    except Exception as e:
        if stderr:
            logger.warning("kettle stderr:")
            logger.warning(_clean_output(stderr))

        raise StarMigrateKettleFailedException("kettle failed w/ exception") from e

    else:
        if stdout:
            logger.debug("kettle stdout:")
            logger.debug(_clean_output(stdout))

        if stderr:
            if retval:
                logger.warning("kettle stderr:")
                logger.warning(_clean_output(stderr))

            else:
                logger.debug("kettle stderr:")
                logger.debug(_clean_output(stderr))

        if retval:
            raise StarMigrateKettleFailedException("kettle failed w/ return value %s" % (retval,))


def _daemon_active():
    while True:
        time.sleep(20)


def _launch_monitor_thread(timeout: float):
    # starmigrate_task should grab this when it has completed
    completion_lock = threading.Lock()

    def monitor_starmigrate():
        """
        after waiting timeout seconds, see if we can grab the completion_lock.
        if so, things are taking too long, log a delay/timeout event.
        """

        # timeout comes from defining scope
        if timeout is not None:
            time.sleep(timeout)
            # if this lock hasn't been taken, then things are taking too long
            if completion_lock.acquire(blocking=False):
                logger.error("Refresh Delay/Timeout")

    # start a new thread up here for monitoring
    monitor = threading.Thread(name="StarMigrate Monitor", target=monitor_starmigrate)
    # don't hold up exiting due to monitor
    monitor.setDaemon(True)
    monitor.start()

    return completion_lock


def starmigrate_task(starmigrate_conf, timeout=None):
    """
    run star migrate
    """
    if timeout is None:
        timeout = float(starmigrate_conf['starmigrate.seconds_before_logging_slow_warning'])

    logger = logging.getLogger('starmigrate.database')

    completion_lock = _launch_monitor_thread(timeout)
    dumpfile = _load_gz_dump_file(starmigrate_conf)  # process a dump file
    if dumpfile is None:
        logger.info("no data to process")
        completion_lock.acquire(blocking=False)
        return
    else:
        logger.info("loaded dumpfile %s, beginning StarMigration..." % dumpfile.complete_filepath)

    check_analytics_schema(starmigrate_conf)
    try:
        run_kettle(starmigrate_conf, logger)
    # below, if finished_in_time is false, we've already logged the delay,
    # potentially we could do something with it.
    except Exception as e:
        # exception raised, this failed
        # TODO: determine exception types?
        finished_in_time = completion_lock.acquire(blocking=False)
        logger.error("Refresh Failed")
        dumpfile.set_filestatus("failed")
        logger.exception(e)
    else:
        # there be success here, arr
        finished_in_time = completion_lock.acquire(blocking=False)
        logger.info("Successful Refresh")
        # remove dbdump file
        dumpfile.delete_file()
    finally:
        dumpfile.delete_gz_files()


def parse_args(argv: list=None, namespace: argparse.Namespace=None):
    """
    parse command line arguments

    :param argv:
    :param namespace:
    :return:
    """
    parser = argparse.ArgumentParser(description='StarMigrate entry point')
    parser.add_argument('-p', dest='pidfile', default='/opt/edware/run/starmigrate.pid',
                        help="pid file for daemon")
    parser.add_argument('-d', dest='daemon', action='store_true', default=False, help="daemon")
    parser.add_argument('-i', dest='ini_file', default=None, help="ini file")
    parser.add_argument('-t', dest='tenant', default=None, help="tenant override")
    parser.add_argument('-w', dest='warn_timeout', type=float, default=None, help="log delay if timeout (seconds) exceeded")

    return parser.parse_args(argv, namespace)


def main(args: argparse.Namespace):
    """
    main logic

    :param args:
    :return:
    """
    if args.ini_file:
        file = args.ini_file

    else:
        file = get_ini_file()

    # configure logging
    logging.config.fileConfig(file)

    # Read settings
    complete_conf = read_ini(file)

    tenant = complete_conf.get('starmigrate.tenant')
    if args.tenant:
        tenant = args.tenant
        complete_conf["starmigrate.tenant"] = tenant

    # The following is needed to get rid of unneeded tenant settings
    starmigrate_conf = extract_settings(complete_conf, '', tenant)

    migrate_schema_key = 'migrate_dest.db.{tenant}.schema_name'.format(tenant=tenant)
    starmigrate_db_url = 'starmigrate.dest.db.{tenant}.url'.format(tenant=tenant)

    starmigrate_conf["import_schema_name"] = complete_conf.get(migrate_schema_key)
    starmigrate_conf["starmigrate.dest.db.url"] = complete_conf.get(starmigrate_db_url)
    starmigrate_conf["migrate.dumpfile_name_format"] = complete_conf.get("migrate.dumpfile_name_format")
    starmigrate_conf["migrate.dumpfile_location"] = complete_conf.get("migrate.dumpfile_location")
    starmigrate_conf["migrate.aws_access_key"] = complete_conf.get("migrate.AWS_ACCESS_KEY", None)
    starmigrate_conf["migrate.aws_secret_access_key"] = complete_conf.get("migrate.AWS_SECRET_ACCESS_KEY", None)

    daemon_mode = args.daemon
    pid_file = args.pidfile

    if daemon_mode:
        create_daemon(pid_file)
        daemonize = threading.Thread(name="StarMigrate Daemonize", target=_daemon_active)
        # don't hold up exiting due to monitor
        daemonize.start()
        run_cron_job(settings=starmigrate_conf, prefix='starmigrate.main_task.',
                     job=starmigrate_task)

    else:
        if args.warn_timeout is None:
            warn_timeout = float(starmigrate_conf['starmigrate.seconds_before_logging_slow_warning'])

        else:
            warn_timeout = args.warn_timeout

        starmigrate_task(starmigrate_conf, warn_timeout)


if __name__ == '__main__':
    main(parse_args())
