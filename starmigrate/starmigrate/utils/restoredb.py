'''
Created on May 20, 2015

@authors: tcollins, mjacob
'''
import io
import logging
import zipfile
from edcore.utils.dbdumpfile import DBDumpFileManager, DBDumpFileException


def _get_dump_file(conf):
    location = conf["migrate.dumpfile_location"]
    filename_format = conf["migrate.dumpfile_name_format"]
    tenant = conf["starmigrate.tenant"]
    gz_archive_path = conf["starmigrate.dump_gz_archive_path"]
    dumpfiles = DBDumpFileManager(location=location, filename_format=filename_format,
                                  tenant=tenant, gz_archive_path=gz_archive_path)
    return dumpfiles.find_latest_with_status("complete", delete_older=True)


def _load_gz_dump_file(conf, logger=logging.getLogger('starmigrate')):
    """
    Load archive that contain '*.csv.gz'dumps, extract subarchives to required folder and return dumpfile
    object that contain all required information about dump type: s3 locations, extract patches and etc.
    :return: DBDumpFile object or None
    """
    dumpfile_to_migrate = _get_dump_file(conf)
    if dumpfile_to_migrate is None:
        return None
    dumpfile_to_migrate.set_filestatus("processing")
    logger.info("found dumpfile: %s" % (dumpfile_to_migrate.base_filename,))
    try:
        with io.BytesIO() as b:
            dumpfile_to_migrate.read_file_to_stream(b)
            with zipfile.ZipFile(b, mode='r') as zipf:
                for subfile in zipf.namelist():
                    zipf.extract(subfile, dumpfile_to_migrate.gz_archive_path)
                    dumpfile_to_migrate.register_archived_dump_file(subfile)
    except Exception as e:
        dumpfile_to_migrate.set_filestatus("failed")
        error_msg = 'Reading root dump archive failed with exception!'
        logger.info(': '.join([error_msg, str(e)]))
        raise DBDumpFileException(error_msg) from e

    return dumpfile_to_migrate
