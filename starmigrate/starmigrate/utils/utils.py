import configparser
import logging
import os
from sqlalchemy import create_engine
from sqlalchemy.engine.url import make_url
from sqlalchemy.sql import exists, select

from edcore.utils.utils import convert_sqlalchemy_url


logger = logging.getLogger(__name__)


def get_config_file():
    # Read environment variable that is set in prod mode that stores path of smarter.ini
    prod_config = os.environ.get("CELERY_PROD_CONFIG")
    if prod_config and os.path.exists(prod_config):
        return prod_config

    return None


def get_ini_file():
    """
    Get ini file path name
    """
    smarter_ini = '/opt/edware/conf/smarter.ini'
    if os.path.exists(smarter_ini):
        ini_file = smarter_ini

    else:
        here = os.path.abspath(os.path.dirname(__file__))
        ini_file = os.path.abspath(os.path.join(here, '../../../config/development.ini'))

        if not os.path.exists(ini_file):
            raise FileNotFoundError("edware config file not found: %s" % (ini_file,))

    return ini_file


def read_ini(ini_file, header='app:main'):
    config = configparser.ConfigParser()
    config.read(ini_file)
    return config[header]


def check_analytics_schema(starmigrate_conf):
    """
    if there is no schema for analytics, create it before run kettle
    # TODO: add UT for this functionality
    """
    sqlalchemy_url_string = starmigrate_conf['starmigrate.dest.db.url']
    sqlalchemy_url = make_url(sqlalchemy_url_string)
    db_conn_string = convert_sqlalchemy_url(sqlalchemy_url)
    engine = create_engine(db_conn_string)
    schema = starmigrate_conf['starmigrate.analytics_schema']
    query = select(
        [exists(select([("schema_name")])
                .select_from("information_schema.schemata").where("schema_name = '{}'".format(schema)))
         ])
    with engine.connect() as conn:
        if not conn.scalar(query):
            conn.execute('CREATE SCHEMA {schema};'.format(schema=schema))
