class Constants():
    """
    Constants related to starmigrate
    """
    INI_PREFIX = 'starmigrate'
    TENANT = INI_PREFIX + '.tenant'
    SQL_DIR = INI_PREFIX + '.sql_dir'
    MIGRATOR_TYPE = INI_PREFIX + '.migrate_type'
    STARMIGRATE_ADMIN_LOGGER = 'starmigrate_admin'
