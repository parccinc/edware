Mac Gotchas
===========


1) Error when trying to use psycopg2

    ImportError: dlopen(/Users/zvanwijk/PycharmEnv/PARCC/edware/lib/python3.3/site-packages/psycopg2-2.5.1-py3.3-macosx-10.10-x86_64.egg/psycopg2/_psycopg.so, 2): Library not loaded: libssl.1.0.0.dylib
      Referenced from: /Users/zvanwijk/PycharmEnv/PARCC/edware/lib/python3.3/site-packages/psycopg2-2.5.1-py3.3-macosx-10.10-x86_64.egg/psycopg2/_psycopg.so
      Reason: image not found
  
Cause: Brew OpenSSL libraries are not found

Fix: Set DYLD_LIBRARY_PATH=/usr/local/opt/openssl/lib

2) Error when attempting to install lxml

    ...
    In file included from src/lxml/lxml.etree.c:239:
    
    /Users/zvanwijk/PycharmEnv/PARCC/edware/build/lxml/src/lxml/includes/etree_defs.h:14:10: fatal error: 'libxml/xmlversion.h' file not found
    
    #include "libxml/xmlversion.h"
    ...

Fix: Manually run

    CPATH=/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.10.sdk/usr/include/libxml2/ pip install lxml
