__author__ = 'vummalaneni'

from setuptools import setup, find_packages

requires = [
    'SQLAlchemy == 0.9.9',
    'pyyaml == 3.10',
    'celery == 3.0.23',
    'boto==2.34.0',
    "apscheduler==2.1.1",
]

tests_require = requires

docs_extras = [
    'Sphinx',
    'docutils',
    'repoze.sphinx.autointerface']

setup(name='starmigrate',
      version='0.1',
      description='Scheduled Data Migration process from Edware Reporting to Analytics',
      classifiers=[
          "Programming Language :: Python",
          "Framework :: Pyramid",
          "Topic :: Internet :: WWW/HTTP",
          "Topic :: Internet :: WWW/HTTP :: WSGI :: Application", ],
      author='',
      author_email='',
      url='',
      keywords='web wsgi starmigrate edware celery',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='nose.collector',
      install_requires=requires,
      tests_require=tests_require,
      extras_require={
          'docs': docs_extras, },
      entry_points="""\
      """,
      )
