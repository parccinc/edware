ETL uses some parts of Pentaho Data Integration Solution (a.k.a. Kettle).
They are located in directories 'launcher' and 'lib', together with the shell scripts.
They are distributed under the Apache license (LICENSE.txt), check Kettle documentation for details.

Script 'run-with-test-data.cmd' shows how it should be run.
Memory is set with the -Xmx parameter in Spoon.* (just search for -Xmx)