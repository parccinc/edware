'''
Created on Jun 19, 2013

@author: tosako
'''
import sys
import argparse
from sqlalchemy.engine import create_engine
from sqlalchemy.schema import CreateSchema, DropSchema
from edschema.metadata.ed_metadata import generate_ed_metadata
from edschema.metadata.stats_metadata import generate_stats_metadata
from edschema.metadata.reporting.aggregation_metadata import create_median_function
from edschema.metadata.reporting.summ_rpt_cds_metadata import generate_summ_rpt_cds_metadata

DEBUG = 0
VERBOSE = False


def create_parser():
    parser = argparse.ArgumentParser(description='Create New Schema for EdWare')
    parser.add_argument("-s", "--schema", help="set schema name.  required")
    parser.add_argument("-m", "--metadata", choices=['edware', 'stats', 'cds'], help="edware or stats or cds")
    parser.add_argument("-d", "--database", default="edware", help="set database name default[edware]")
    parser.add_argument("--host", default="127.0.0.1:5432", help="postgre host default[127.0.0.1:5432]")
    parser.add_argument("-u", "--user", default="edware", help="postgre username default[edware]")
    parser.add_argument("-p", "--password", default="edware", help="postgre password default[edware]")
    parser.add_argument("-a", "--action", choices=['setup', 'teardown', 'truncate'],
                        default="setup", help="action, default is setup, use teardown to drop all tables")
    return parser


def process_command_lines(args):
    parser = create_parser()
    if args is None:
        args = parser.parse_args()
    schema = args.schema
    metadata = args.metadata
    database = args.database
    host = args.host
    user = args.user
    password = args.password
    action = args.action

    if metadata is None:
        parser.error('Please specify --metadata option')
        return None
    if schema is None:
        parser.error("Please specify --schema option")
        return None

    return schema, metadata, database, host, user, password, action


def run(args):
    schema, metadata, *args, action = args
    if action == 'setup':
        return setup(schema, metadata, *args)
    elif action == 'truncate':
        return truncate(schema, *args)
    elif action == 'teardown':
        return teardown(schema, *args)
    else:
        raise NotImplementedError()


def _create_connection(database, host, user, password):
    db_template = "postgresql+pypostgresql://{user}:{password}@{host}/{database}"
    database_url = db_template.format(user=user, password=password, host=host, database=database)
    engine = create_engine(database_url, echo=True)
    return engine.connect(), engine


def truncate(schema, *args):
    try:
        connection, engine = _create_connection(*args)
        db_metadata = generate_ed_metadata(schema_name=schema, bind=engine)
        trans = connection.begin()
        for table in reversed(db_metadata.sorted_tables):
            connection.execute(table.delete())
        trans.commit()
    except Exception:
        print("error deleting" + Exception)
        return -1


def teardown(schema, *args):
    try:
        connection, _ = _create_connection(*args)
        connection.execute(DropSchema(schema, cascade=True))
    except:
        return -1


def setup(schema, metadata, *args):
    connection, engine = _create_connection(*args)
    connection.execute(CreateSchema(schema))
    if metadata in ('edware', 'cds'):
        generator = generate_summ_rpt_cds_metadata if metadata == 'cds' else generate_ed_metadata
        db_metadata = generator(schema_name=schema, bind=engine)
        create_median_function(schema_name=schema, bind=engine)
        connection.execute('CREATE SEQUENCE "' + schema + '"."global_rec_seq"')
    else:
        db_metadata = generate_stats_metadata(schema_name=schema, bind=engine)
    db_metadata.create_all(engine)


def main(args=None):
    args = process_command_lines(args)
    if not args:
        return -1
    status = run(args)
    return status


if __name__ == "__main__":
    status = main()
    sys.exit(status)
