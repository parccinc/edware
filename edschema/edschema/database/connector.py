'''
Created on Jan 15, 2013

@author: tosako
'''
from functools import wraps
from sqlalchemy.engine.base import Engine
from edschema.database.interfaces import ConnectionBase
from zope import interface, component
from zope.interface.declarations import implementer
from sqlalchemy import Table
from sqlalchemy import schema
from collections import OrderedDict
import logging
from sqlalchemy.exc import DatabaseError, InterfaceError
import time


logger = logging.getLogger(__name__)


class IDbUtil(interface.Interface):
    def get_engine(self):
        pass

    def get_metadata(self):
        pass

    def set_metadata(self):
        pass


@implementer(IDbUtil)
class DbUtil:
    def __init__(self, engine, metadata=None):
        self.__engine = engine
        self.__metadata = metadata

    def get_engine(self):
        return self.__engine

    def get_metadata(self):
        return self.__metadata

    def set_metadata(self, metadata):
        self.__metadata = metadata


def retry(origin_func):

    @wraps(origin_func)
    def wrap(conn, *args, max_retries=1, **kwargs):
        num_of_execute = 0
        while num_of_execute <= max_retries:
            try:
                return origin_func(conn, *args, **kwargs)
            except (DatabaseError, InterfaceError) as err:
                logger.error(err)
                reconnected = try_to_reconnect(conn, max_retries)
                if not reconnected:
                    raise err
            num_of_execute += 1
        raise Exception("Maximum retries exhausted. SQL statement execution failed.")

    def try_to_reconnect(conn, max_retries):
        num_of_retries = 0
        while num_of_retries <= max_retries:
            logger.error("Connection was invalidated.  Will reconnect.")
            succeeded = conn.wait_and_reconnect()
            if succeeded:
                return True
            num_of_retries += 1
        logger.error("Maximum retries exhausted.")
        return False

    return wrap


class DBConnection(ConnectionBase):
    '''
    Inheritate this class if you are making a report class and need to access to database
    BaseReport is just managing session for your database connection and convert result to dictionary
    '''
    def __init__(self, name=''):
        '''
        name is an empty string by default
        '''
        self.__name = name
        if name is None:
            # we don't have a valid name to lookup for database routing
            raise Exception()
        db_util = component.queryUtility(IDbUtil, name=name)
        engine = db_util.get_engine()
        self.__connection = engine.connect()
        self.host = self.get_host(engine)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, value, tb):
        return self.close_connection()

    def __del__(self):
        # Please refer to the following link for the reasoning behind the try except logic:
        # https://docs.python.org/3.3/reference/datamodel.html#object.__del__
        try:
            self.close_connection()
        except AttributeError:
            pass

    def get_engine(self) -> Engine:
        db_util = component.queryUtility(IDbUtil, name=self.__name)
        return db_util.get_engine()

    def get_result(self, query):
        '''
        query and get result
        Convert from result_set to dictionary.
        :param query: A select query to be executed
        :return: list of results or [] if execution return empty row
        '''
        return [OrderedDict(zip(el.keys(), el)) for el in self.execute(query)]

    def get_streaming_result(self, query, fetch_size=1024):
        '''
        Query and get result
        Convert from result_set to dictionary
        Also do it in streaming way it wont use up memory
        :param query: A select query to be execute
        :param fetch_size: max number of rows to be returned
        '''
        result = self.execute(query, stream_results=True)
        # we should make this configurable in the long run
        rows = result.fetchmany(fetch_size)
        while len(rows) > 0:
            for row in rows:
                result_row = OrderedDict()
                for key in row.keys():
                    result_row[key] = row[key]
                yield result_row
            rows = result.fetchmany(fetch_size)

    # return Table Metadata
    def get_table(self, table_name):
        return Table(table_name, self.get_metadata())

    def get_metadata(self):
        db_util = component.queryUtility(IDbUtil, name=self.__name)
        return db_util.get_metadata()

    def set_metadata_by_generate(self, schema_name, metadata_func):
        '''
        Set metadata by passing in a function that generates its metadata
        '''
        metadata = metadata_func(schema_name=schema_name, bind=self.get_engine())
        self._set_metadata(metadata)

    def set_metadata_by_reflect(self, schema_name):
        '''
        Given a schema name, reflect on current database and set the metadata
        '''
        metadata = schema.MetaData(bind=self.get_engine(), schema=schema_name)
        metadata.reflect(views=True)
        self._set_metadata(metadata)

    def _set_metadata(self, metadata):
        db_util = component.queryUtility(IDbUtil, name=self.__name)
        db_util.set_metadata(metadata)

    @retry
    def execute(self, statement, *multiparams, stream_results=False, **params):
        return self.__connection.execution_options(stream_results=stream_results).execute(statement, *multiparams, **params)

    def get_transaction(self):
        """
        return open connection
        """
        return self.__connection.begin()

    def close_connection(self):
        """
        closes the connection
        """
        if self.__connection is not None:
            self.__connection.close()
        self.__connection = None

    def reconnect(self):
        self.close_connection()
        self.__connection = self.get_engine().connect()

    def wait_and_reconnect(self, wait_seconds=5):
        time.sleep(wait_seconds)
        try:
            self.reconnect()
            return True
        except:
            # ignore any kind of error during retry loop
            return False

    @staticmethod
    def get_host(engine):
        """
        Get the host name (ip address)
        :param engine: connection engine
        :return: host name
        """
        return engine.url.host

    @staticmethod
    def get_datasource_name(tenant=None):
        '''
        return data source name
        '''
        raise Exception('need to implement get_datasource_name')

    @staticmethod
    def get_db_config_prefix(tenant=None):
        '''
        return config prefix
        '''
        raise Exception('need to implement get_db_config_prefix')

    @staticmethod
    def generate_metadata(schema_name=None, bind=None):
        '''
        return metadata
        '''
        raise Exception('need to implement generate_metadata')
