from sqlalchemy import Table, Column
from sqlalchemy import SmallInteger, String, Integer
from sqlalchemy.types import CHAR, Numeric
from sqlalchemy.schema import MetaData, Index


def generate_summ_rpt_cds_metadata(schema_name=None, bind=None):
    metadata = MetaData(schema=schema_name, bind=bind)

    rpt_math_sum = Table('rpt_math_sum', metadata,
                         Column('student_sex', CHAR(1), nullable=False),
                         Column('test_code', String(20), nullable=False),
                         Column('econo_disadvantage', CHAR(1), nullable=True),
                         Column('ethnicity', CHAR(2), nullable=True),
                         Column('ell', CHAR(1), nullable=True),
                         Column('lep_status', CHAR(1), nullable=True),
                         Column('asmt_grade', String(16), nullable=True),
                         Column('student_grade', String(12), nullable=False),
                         Column('disabil_student', CHAR(3), nullable=True),
                         Column('asmt_subject', String(35), nullable=False),
                         Column('year', String(9), nullable=False),
                         Column('sum_score_rec_uuid', String(36), nullable=True, info={'natural_key': True}),
                         Column('sum_scale_score', Integer, nullable=True),
                         Column('sum_perf_lvl', SmallInteger, nullable=True),
                         Column('sum_read_scale_score', Integer, nullable=True),
                         Column('sum_write_scale_score', Integer, nullable=True),
                         Column('subclaim1_category', SmallInteger, nullable=True),
                         Column('subclaim2_category', SmallInteger, nullable=True),
                         Column('subclaim3_category', SmallInteger, nullable=True),
                         Column('subclaim4_category', SmallInteger, nullable=True),
                         Column('subclaim5_category', SmallInteger, nullable=True),
                         Column('subclaim6_category', SmallInteger, nullable=True),
                         Column('parcc_growth_percent', Numeric(5, 2), nullable=True))

    Index('rpt_math_sum_ix',
          rpt_math_sum.c.year,
          rpt_math_sum.c.asmt_grade,
          rpt_math_sum.c.asmt_subject,
          rpt_math_sum.c.econo_disadvantage,
          rpt_math_sum.c.ethnicity,
          rpt_math_sum.c.student_sex,
          rpt_math_sum.c.ell,
          rpt_math_sum.c.lep_status,
          rpt_math_sum.c.disabil_student,)

    rpt_ela_sum = Table('rpt_ela_sum', metadata,
                        Column('student_sex', CHAR(1), nullable=False),
                        Column('test_code', String(20), nullable=False),
                        Column('econo_disadvantage', CHAR(1), nullable=True),
                        Column('ethnicity', CHAR(2), nullable=True),
                        Column('ell', CHAR(1), nullable=True),
                        Column('lep_status', CHAR(1), nullable=True),
                        Column('asmt_grade', String(16), nullable=False),
                        Column('student_grade', String(12), nullable=False),
                        Column('disabil_student', CHAR(3), nullable=True),
                        Column('asmt_subject', String(35), nullable=False),
                        Column('year', String(9), nullable=False),
                        Column('sum_score_rec_uuid', String(36), nullable=True, info={'natural_key': True}),
                        Column('sum_scale_score', Integer, nullable=True),
                        Column('sum_perf_lvl', SmallInteger, nullable=True),
                        Column('sum_read_scale_score', Integer, nullable=True),
                        Column('sum_write_scale_score', Integer, nullable=True),
                        Column('subclaim1_category', SmallInteger, nullable=True),
                        Column('subclaim2_category', SmallInteger, nullable=True),
                        Column('subclaim3_category', SmallInteger, nullable=True),
                        Column('subclaim4_category', SmallInteger, nullable=True),
                        Column('subclaim5_category', SmallInteger, nullable=True),
                        Column('subclaim6_category', SmallInteger, nullable=True),
                        Column('parcc_growth_percent', Numeric(5, 2), nullable=True))

    Index('rpt_ela_sum_ix',
          rpt_ela_sum.c.year,
          rpt_ela_sum.c.asmt_grade,
          rpt_ela_sum.c.econo_disadvantage,
          rpt_ela_sum.c.ethnicity,
          rpt_ela_sum.c.student_sex,
          rpt_ela_sum.c.ell,
          rpt_ela_sum.c.lep_status,
          rpt_ela_sum.c.disabil_student,)

    rpt_ela_sum_item_score = Table('rpt_ela_sum_item_score', metadata,
                                   Column('year', String(9), nullable=False),
                                   Column('test_code', String(20), nullable=False),
                                   Column('asmt_grade', String(15), nullable=True),
                                   Column('asmt_subject', String(35), nullable=False),
                                   Column('item_uin', String(35), nullable=False, info={'natural_key': True}),
                                   Column('test_uuid', String(36), nullable=False, info={'natural_key': True}),
                                   Column('parent_item_score', SmallInteger, nullable=False))

    Index('rpt_ela_sum_item_score_ix',
          rpt_ela_sum_item_score.c.parent_item_score,
          rpt_ela_sum_item_score.c.item_uin,
          rpt_ela_sum_item_score.c.test_uuid,)

    rpt_math_sum_item_score = Table('rpt_math_sum_item_score', metadata,
                                    Column('year', String(9), nullable=False),
                                    Column('test_code', String(20), nullable=False),
                                    Column('asmt_grade', String(15), nullable=True),
                                    Column('asmt_subject', String(35), nullable=False),
                                    Column('item_uin', String(35), nullable=False, info={'natural_key': True}),
                                    Column('test_uuid', String(36), nullable=False, info={'natural_key': True}),
                                    Column('parent_item_score', SmallInteger, nullable=False))

    Index('rpt_math_sum_item_score_ix',
          rpt_math_sum_item_score.c.parent_item_score,
          rpt_math_sum_item_score.c.item_uin,
          rpt_math_sum_item_score.c.test_uuid,)

    rpt_sum_test_level_m = Table('rpt_sum_test_level_m', metadata,
                                 Column('test_code', String(20), nullable=False, info={'natural_key': True}),
                                 Column('perf_lvl_id', String(30), nullable=False, info={'natural_key': True}),
                                 Column('cutpoint_label', String(50), nullable=False),
                                 Column('cutpoint_low', Numeric(4, 0), nullable=False),
                                 Column('cutpoint_upper', Numeric(4, 0), nullable=False))

    Index('rpt_sum_test_level_m_ix', rpt_sum_test_level_m.c.test_code)

    return metadata
