from sqlalchemy.schema import MetaData


def create_median_function(schema_name=None, bind=None):
    # https://wiki.postgresql.org/wiki/Aggregate_Median
    metadata = MetaData(schema=schema_name, bind=bind)
    cmd_create_median_func = '''
        CREATE OR REPLACE FUNCTION _final_median(numeric[])
          RETURNS numeric AS
        $BODY$
           SELECT AVG(val)
           FROM (
             SELECT val
             FROM unnest($1) val
             ORDER BY 1
             LIMIT  2 - MOD(array_upper($1, 1), 2)
             OFFSET CEIL(array_upper($1, 1) / 2.0) - 1
           ) sub;
        $BODY$
          LANGUAGE sql IMMUTABLE;
    '''
    cmd_drop_aggregate = "DROP AGGREGATE IF EXISTS median(numeric);"
    cmd_create_aggregate = '''
        CREATE AGGREGATE median(numeric) (
          SFUNC=array_append,
          STYPE=numeric[],
          FINALFUNC=_final_median,
          INITCOND='{}'
        );
    '''
    metadata.bind.execute(cmd_create_median_func)
    metadata.bind.execute(cmd_drop_aggregate)
    metadata.bind.execute(cmd_create_aggregate)
