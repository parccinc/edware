'''
create_star -- create a star schema

create_star will create a database, a schema within the database, and all the required tables, indexes,
and foreign keys required to implement the star schema

Command line options are available form --help, but as a quick start:
    to locally create a schema use something like:
        --database --name <your_star_db> --schema --sname <your_schema_name> --tables --verbose
    to make a schema on QA:
        --database --name <qa_star_date> --schema --sname <edware> --tables --server monetdb1.poc.dum.edwdc.net:5432 --user edware --password edware --verbose

@author:     smacgibbon

@copyright:  2013 Wireless Generation. All rights reserved.

@contact:    edwaredevs@wgen.net
@deffield    updated: Updated
'''
from sqlalchemy.schema import MetaData
from sqlalchemy import Column
from edschema.metadata.reporting.mya_rpt_metadata import generate_mya_rpt_metadata
from edschema.metadata.reporting.summ_rpt_metadata import generate_summ_rpt_metadata
from edschema.metadata.reporting.snl_rpt_metadata import generate_snl_rpt_metadata
from edschema.metadata.reporting.diagnostic_rpt_metadata import generate_diagnostic_rpt_metadata
from edschema.metadata.reporting.summ_rpt_cds_metadata import generate_summ_rpt_cds_metadata

__all__ = []
__version__ = 0.1
__date__ = '2013-02-02'
__updated__ = '2014-04-03'


class MetaColumn(Column):
    col_type = 'MetaColumn'


def generate_ed_metadata(schema_name=None, bind=None):
    metadata = MetaData(schema=schema_name, bind=bind)
    generate_summ_rpt_metadata(metadata)
    generate_mya_rpt_metadata(metadata)
    generate_snl_rpt_metadata(metadata)
    generate_diagnostic_rpt_metadata(metadata)

    return metadata
