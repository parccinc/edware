'''
Created on Feb 28, 2013

@author: tosako
'''
import unittest
from edschema.database.sqlite_connector import create_sqlite
from zope import component
from edschema.database.connector import IDbUtil
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.dialects.postgres import BYTEA
from unittest.mock import patch
from sqlalchemy.schema import MetaData
from sqlalchemy import Table, Column
from sqlalchemy import String, BigInteger


class Test(unittest.TestCase):

    @patch('edschema.database.sqlite_connector.generate_ed_metadata')
    def test_create_engine(self, gen_patch):
        dbUtil = component.queryUtility(IDbUtil)
        self.assertIsNone(dbUtil)
        # Make sure we do not have sqlite in memory
        gen_patch.execute = self.dummy_generate_metadata

        create_sqlite(force_foreign_keys=True, use_metadata_from_db=False, echo=False)
        dbUtil = component.queryUtility(IDbUtil)
        engine = dbUtil.get_engine()
        metadata = dbUtil.get_metadata()
        self.assertIsNotNone(engine)
        self.assertIsNotNone(metadata)

    @patch('edschema.database.sqlite_connector.generate_ed_metadata')
    def test_list_of_tables_from_db(self, gen_patch):
        gen_patch.execute = self.dummy_generate_metadata

        create_sqlite(force_foreign_keys=True, use_metadata_from_db=True, echo=False)
        dbUtil = component.queryUtility(IDbUtil)
        engine = dbUtil.get_engine()
        metadata = dbUtil.get_metadata()
        self.assertIsNotNone(engine)
        self.assertIsNotNone(metadata)
        self.assertIsNotNone(metadata.tables.keys())

    def dummy_generate_metadata(self, schema_name=None, bind=None):
        metadata = MetaData(schema=schema_name, bind=bind)
        test_table = Table('test_table', metadata,
                           Column('col1', String(50), nullable=False))
        return metadata


# Fixes failing test for schema definitions with BigIntegers
@compiles(BigInteger, 'sqlite')
def compile_big_int_sqlite(type_, compiler, **kw):
    return 'INTEGER'


@compiles(BYTEA, 'sqlite')
def compile_bytea_sqlite(type_, compiler, **kw):
    return 'BLOB'

if __name__ == "__main__":
    unittest.main()
