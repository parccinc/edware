'''
Created on Feb 8, 2013

@author: tosako
'''
import unittest
from sqlalchemy import String, Column
from edschema.database.connector import DBConnection
from edschema.metadata.ed_metadata import MetaColumn
from edschema.tests.database.utils.unittest_with_sqlite import Unittest_with_sqlite


class TestEdMetadata(Unittest_with_sqlite):

    # Test rpt_reader_motiv columns
    def test_rpt_reader_motiv_type(self):
        self.assertTable('rpt_reader_motiv', 112)

    # Test rpt_ela_read_comp columns
    def test_rpt_ela_read_comp_type(self):
        self.assertTable('rpt_ela_comp', 101)

    # Test rpt_ela_writing columns
    def test_rpt_ela_writing_type(self):
        self.assertTable('rpt_ela_writing', 96)

    # Test rpt_ela_decod columns
    def test_rpt_ela_decod_type(self):
        self.assertTable('rpt_ela_decod', 105)

    # Test rpt_ela_sum columns
    def test_rpt_ela_sum_type(self):
        self.assertTable('rpt_ela_sum', 232)

    # Test rpt_math_sum columns
    def test_rpt_math_sum_type(self):
        self.assertTable('rpt_math_sum', 232)

    # Test rpt_admin columns
    def test_rpt_admin_type(self):
        self.assertTable('rpt_admin', 12)

    # Test rpt_asmt columns
    def test_rpt_asmt_type(self):
        self.assertTable('rpt_asmt', 9)

    # Test rpt_asmt_lvl_design columns
    def test_rpt_asmt_lvl_type(self):
        self.assertTable('rpt_asmt_lvl', 7)

    # Test rpt_form columns
    def test_rpt_form_type(self):
        self.assertTable('rpt_form', 33)

    # Test rpt_form_group columns
    def test_rpt_form_group_type(self):
        self.assertTable('rpt_form_group', 14)

    # Test rpt_form_group_item columns
    def test_rpt_form_group_item_type(self):
        self.assertTable('rpt_form_group_item', 9)

    # Test rpt_item columns
    def test_rpt_item_type(self):
        self.assertTable('rpt_item', 15)

    # Test item_p_data columns
    def test_rpt_item_p_data_type(self):
        self.assertTable('rpt_item_p_data', 69)

    # Test rpt_file_store columns
    def test_rpt_file_store_type(self):
        self.assertTable('rpt_file_store', 11)

    def test_meta_column_col_type_attr(self):
        meta_column = MetaColumn('test_meta_column', String(50))
        self.assertEquals(getattr(meta_column, "col_type", "Column"), "MetaColumn")
        column = Column('test_column', String(50))
        self.assertEquals(getattr(column, "col_type", "Column"), "Column")

    def assertTable(self, table_name, column_count):
        self.assertTrue(table_name in self.get_Metadata().tables, "missing " + table_name)
        with DBConnection() as connector:
            table = connector.get_table(table_name)

            # check number of field in the table
            self.assertEqual(column_count, len(table.c), "Number of fields in " + table_name)

if __name__ == "__main__":
    unittest.main()
