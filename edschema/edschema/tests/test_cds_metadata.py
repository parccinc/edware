import unittest
from edschema.database.sqlite_connector import create_sqlite
from edschema.metadata.reporting.summ_rpt_cds_metadata import generate_summ_rpt_cds_metadata
from edschema.database.connector import DBConnection


class TestCDSMetadata(unittest.TestCase):

    def setUp(self):
        create_sqlite(datasource_name='', metadata=generate_summ_rpt_cds_metadata(), use_metadata_from_db=True, force_foreign_keys=True)

    def test_columns(self):
        with DBConnection() as connector:
            table = connector.get_table('rpt_math_sum')
            self.assertEqual(23, len(table.c))
            table = connector.get_table('rpt_ela_sum')
            self.assertEqual(23, len(table.c))
            table = connector.get_table('rpt_ela_sum_item_score')
            self.assertEqual(7, len(table.c))
            table = connector.get_table('rpt_math_sum_item_score')
            self.assertEqual(7, len(table.c))
