'''
Created on Feb 8, 2013

@author: tosako
'''
import unittest
from sqlalchemy import String, Column
from edschema.database.connector import DBConnection
from edschema.metadata.ed_metadata import MetaColumn
from edschema.tests.database.utils.unittest_with_sqlite import Unittest_with_sqlite
from edschema.metadata.int_metadata import generate_int_metadata


class TestIntMetadata(Unittest_with_sqlite):

    @classmethod
    def setUpClass(cls):
        super().setUpClass(metadata=generate_int_metadata())

    # Test int_reader_motiv columns
    def test_int_reader_motiv_type(self):
        self.assertTable('int_reader_motiv', 111)

    # Test int_ela_read_comp columns
    def test_int_ela_read_comp_type(self):
        self.assertTable('int_ela_comp', 100)

    # Test int_ela_writing columns
    def test_int_ela_writing_type(self):
        self.assertTable('int_ela_writing', 95)

    # Test int_ela_decod columns
    def test_int_ela_decod_type(self):
        self.assertTable('int_ela_decod', 104)

    # Test int_ela_sum columns
    def test_int_ela_sum_type(self):
        self.assertTable('int_ela_sum', 231)

    # Test int_math_sum columns
    def test_int_math_sum_type(self):
        self.assertTable('int_math_sum', 231)

    # Test int_file_store columns
    def test_int_file_store_type(self):
        self.assertTable('int_file_store', 9)

    # Test int_admin columns
    def test_int_admin_type(self):
        self.assertTable('int_admin', 10)

    # Test int_asmt columns
    def test_int_asmt_type(self):
        self.assertTable('int_asmt', 7)

    # Test int_asmt_lvl_design columns
    def test_int_asmt_lvl_type(self):
        self.assertTable('int_asmt_lvl', 5)

    # Test int_form columns
    def test_int_form_type(self):
        self.assertTable('int_form', 32)

    # Test int_form_group columns
    def test_int_form_group_type(self):
        self.assertTable('int_form_group', 13)

    # Test int_form_group_item columns
    def test_int_form_group_item_type(self):
        self.assertTable('int_form_group_item', 8)

    # Test int_item columns
    def test_int_item_type(self):
        self.assertTable('int_item', 14)

    # Test item_p_data columns
    def test_int_item_p_data_type(self):
        self.assertTable('int_item_p_data', 68)

    def test_meta_column_col_type_attr(self):
        meta_column = MetaColumn('test_meta_column', String(50))
        self.assertEquals(getattr(meta_column, "col_type", "Column"), "MetaColumn")
        column = Column('test_column', String(50))
        self.assertEquals(getattr(column, "col_type", "Column"), "Column")

    def assertTable(self, table_name, column_count):
        self.assertTrue(table_name in self.get_Metadata().tables, "missing " + table_name)
        with DBConnection() as connector:
            table = connector.get_table(table_name)

            # check number of field in the table
            self.assertEqual(column_count, len(table.c), "Number of fields in " + table_name)

if __name__ == "__main__":
    unittest.main()
