import sys
__author__ = 'sravi'

from setuptools import setup, find_packages

requires = [
    'amqp==1.0.13',
    'billiard==2.7.3.34',
    'kombu==2.5.16',
    'celery == 3.0.25',
    'edudl2'
]

requires.append('pyinotify') if sys.platform == 'linux' else None

tests_require = requires

docs_extras = [
    'Sphinx',
    'docutils',
    'repoze.sphinx.autointerface']

setup(name='parcc-udl',
      version='0.1',
      description='Parcc Universal data loader',
      classifiers=[
          "Programming Language :: Python",
          "Framework :: Celery", ],
      author='',
      author_email='',
      url='',
      keywords='web edudl2 edware celery',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='nose.collector',
      install_requires=requires,
      tests_require=tests_require,
      extras_require={
          'docs': docs_extras, },
      entry_points="""\
      """,
      )
