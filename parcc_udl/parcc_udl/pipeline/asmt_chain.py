from celery import chain
from edudl2.udl2.celery import celery
from edudl2.udl2 import message_keys as mk
from edudl2.udl2.constants import Constants
from edudl2.udl2.udl2_base_task import Udl2BaseTask
from parcc_udl.pipeline.common_chain import get_post_etl_chain
from edudl2.udl2 import (
    W_parallel_csv_load,
    W_load_to_staging_table,
    W_load_to_integration_table,
    W_load_json_to_staging,
    W_load_file_to_integration
)

__author__ = 'ablum'


@celery.task(name='parcc.W_get_asmt_chain.task', base=Udl2BaseTask)
def get_asmt_etl_chain(msg):
    asmt_chain = chain(get_load_to_ldr_chain(msg) +
                       get_load_to_stg_chain() +
                       get_stg_to_int_chain(msg) +
                       get_post_etl_chain())
    asmt_chain.delay()


def get_load_to_ldr_chain(msg):
    return [W_parallel_csv_load.get_load_from_csv_tasks(msg)]


def get_load_to_stg_chain():
    return [W_load_to_staging_table.task.s(),
            W_load_json_to_staging.task.s()
            ]


def get_stg_to_int_chain(msg):
    tasks = [W_load_to_integration_table.task.s()]
    load_type = msg[mk.LOAD_TYPE]
    file_load_types = [Constants.LOAD_TYPE_ASMT_ELA, Constants.LOAD_TYPE_ASMT_MATH]
    return tasks
