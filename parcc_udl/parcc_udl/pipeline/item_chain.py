from celery import chain
from edudl2.udl2.celery import celery
from edudl2.udl2 import W_load_xml_to_loader, W_load_file_to_integration, W_load_to_staging_table, \
    W_load_to_integration_table
from edudl2.udl2.udl2_base_task import Udl2BaseTask
from parcc_udl.pipeline.common_chain import get_post_etl_chain

__author__ = 'ablum'


@celery.task(name='parcc.W_get_item_chain.task', base=Udl2BaseTask)
def get_item_etl_chain(msg):
    item_chain = chain([W_load_xml_to_loader.task.s(msg),
                        W_load_to_staging_table.task.s(),
                        W_load_to_integration_table.task.s(),
                        W_load_file_to_integration.task.s()] +
                       get_post_etl_chain())
    item_chain.delay()
