from celery import chain
from edudl2.udl2.celery import celery
from edudl2.udl2.udl2_base_task import Udl2BaseTask
from edudl2.udl2 import (W_parallel_csv_load, W_load_to_staging_table, W_load_to_integration_table, W_load_json_to_staging)
from parcc_udl.pipeline.common_chain import get_post_etl_chain


@celery.task(name='parcc.W_get_mya_item_chain.task', base=Udl2BaseTask)
def get_non_summ_item_etl_chain(msg):
    mya_item_chain = chain(get_load_to_ldr_chain(msg) +
                           get_load_to_stg_chain() +
                           get_stg_to_int_chain() +
                           get_post_etl_chain())
    mya_item_chain.delay()


def get_load_to_ldr_chain(msg):
    return [W_parallel_csv_load.get_load_from_csv_tasks(msg)]


def get_load_to_stg_chain():
    return [W_load_to_staging_table.task.s()]


def get_stg_to_int_chain():
    return [W_load_to_integration_table.task.s()]
