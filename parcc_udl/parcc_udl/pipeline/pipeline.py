from celery import chain
from edudl2.udl2.udl2_pipeline import Pipeline
from edudl2.udl2 import (W_file_arrived, W_file_decrypter, W_file_expander, W_get_values_from_json,
                         W_simple_file_validator, W_file_splitter, W_data_translator,
                         W_data_validator)
from edudl2.udl2.celery import celery
from edudl2.udl2 import message_keys as mk
from edudl2.udl2.udl2_base_task import Udl2BaseTask
from celery.utils.log import get_task_logger
from edudl2.udl2.constants import Constants
from parcc_udl.pipeline.asmt_chain import get_asmt_etl_chain
from parcc_udl.pipeline.item_chain import get_item_etl_chain
from parcc_udl.pipeline.basic_chain import get_basic_etl_chain
from parcc_udl.pipeline.non_summ_item_chain import get_non_summ_item_etl_chain

logger = get_task_logger(__name__)

__author__ = 'ablum'


class ParccPipeline(Pipeline):

    def get_pipeline(self, arrival_msg):
        parcc_chain = chain(get_pre_etl_chain(arrival_msg) +
                            [get_etl_chain.s()])

        return parcc_chain


def get_pre_etl_chain(arrival_msg):
    return [W_file_arrived.task.si(arrival_msg),
            W_file_decrypter.task.s(),
            W_file_expander.task.s(),
            W_simple_file_validator.task.s(),
            W_get_values_from_json.task.s(),
            W_data_translator.task.s(),
            W_data_validator.task.s()]


@celery.task(name='parcc.W_get_etl_chain.task', base=Udl2BaseTask)
def get_etl_chain(msg):
    logger.info('DETERMINE END ROUTE BY TYPE')
    logger.debug(msg)
    load_type = msg[mk.LOAD_TYPE]

    chains = {Constants.LOAD_TYPE_ASMT_ELA: chain([W_file_splitter.task.s(msg)] +
                                                  [get_asmt_etl_chain.s()]),
              Constants.LOAD_TYPE_ASMT_MATH: chain([W_file_splitter.task.s(msg)] +
                                                   [get_asmt_etl_chain.s()]),
              Constants.LOAD_TYPE_MYA_ASMT_ELA: chain([W_file_splitter.task.s(msg)] +
                                                      [get_asmt_etl_chain.s()]),
              Constants.LOAD_TYPE_MYA_ASMT_MATH: chain([W_file_splitter.task.s(msg)] +
                                                       [get_asmt_etl_chain.s()]),
              Constants.LOAD_TYPE_SNL_ASMT: chain([W_file_splitter.task.s(msg)] +
                                                  [get_asmt_etl_chain.s()]),
              Constants.LOAD_TYPE_PSYCHOMETRIC: chain([W_file_splitter.task.s(msg)] +
                                                      [get_basic_etl_chain.s()]),
              Constants.LOAD_TYPE_ITEM: chain(get_item_etl_chain.s(msg)),
              Constants.LOAD_TYPE_MYA_ELA_ITEM: chain([W_file_splitter.task.s(msg)] +
                                                      [get_non_summ_item_etl_chain.s()]),
              Constants.LOAD_TYPE_MYA_MATH_ITEM: chain([W_file_splitter.task.s(msg)] +
                                                       [get_non_summ_item_etl_chain.s()]),
              Constants.LOAD_TYPE_SNL_STUDENT_TASK: chain([W_file_splitter.task.s(msg)] +
                                                          [get_non_summ_item_etl_chain.s()]),
              Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_READING_COMP: chain([W_file_splitter.task.s(msg)] +
                                                                          [get_non_summ_item_etl_chain.s()]),
              Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_VOCAB: chain([W_file_splitter.task.s(msg)] +
                                                                   [get_non_summ_item_etl_chain.s()]),
              Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_DECODING: chain([W_file_splitter.task.s(msg)] +
                                                                      [get_non_summ_item_etl_chain.s()]),
              Constants.LOAD_TYPE_DIAGNOSTIC_ELA_VOCAB: chain([W_file_splitter.task.s(msg)] +
                                                              [get_basic_etl_chain.s()]),
              Constants.LOAD_TYPE_DIAGNOSTIC_ELA_COMP: chain([W_file_splitter.task.s(msg)] +
                                                             [get_basic_etl_chain.s()]),
              Constants.LOAD_TYPE_DIAGNOSTIC_ELA_DECODING: chain([W_file_splitter.task.s(msg)] +
                                                                 [get_basic_etl_chain.s()]),
              Constants.LOAD_TYPE_DIAGNOSTIC_ELA_WRITING: chain([W_file_splitter.task.s(msg)] +
                                                                [get_basic_etl_chain.s()]),
              Constants.LOAD_TYPE_DIAGNOSTIC_ELA_READ_FLU: chain([W_file_splitter.task.s(msg)] +
                                                                 [get_basic_etl_chain.s()]),
              Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_PROGRESSION: chain([W_file_splitter.task.s(msg)] +
                                                                          [get_non_summ_item_etl_chain.s()]),
              Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_COMPREHENSION: chain([W_file_splitter.task.s(msg)] +
                                                                            [get_non_summ_item_etl_chain.s()]),
              Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_FLUENCY: chain([W_file_splitter.task.s(msg)] +
                                                                      [get_non_summ_item_etl_chain.s()]),
              Constants.LOAD_TYPE_DIAGNOSTIC_MATH_LOCATOR: chain([W_file_splitter.task.s(msg)] +
                                                                 [get_basic_etl_chain.s()]),
              Constants.LOAD_TYPE_DIAGNOSTIC_MATH_PROGRESSION: chain([W_file_splitter.task.s(msg)] +
                                                                     [get_basic_etl_chain.s()]),
              Constants.LOAD_TYPE_DIAGNOSTIC_MATH_FLUENCY: chain([W_file_splitter.task.s(msg)] +
                                                                 [get_basic_etl_chain.s()]),
              Constants.LOAD_TYPE_DIAGNOSTIC_MATH_GRADE_LEVEL: chain([W_file_splitter.task.s(msg)] +
                                                                     [get_basic_etl_chain.s()]),
              Constants.LOAD_TYPE_DIAGNOSTIC_MATH_CLUSTER: chain([W_file_splitter.task.s(msg)] +
                                                                 [get_basic_etl_chain.s()]),
              Constants.LOAD_TYPE_READER_MOTIVATION: chain([W_file_splitter.task.s(msg)] +
                                                           [get_basic_etl_chain.s()]),
              Constants.LOAD_TYPE_ELA_ITEM: chain([W_file_splitter.task.s(msg)] +
                                                  [get_basic_etl_chain.s()]),
              Constants.LOAD_TYPE_MATH_ITEM: chain([W_file_splitter.task.s(msg)] +
                                                   [get_basic_etl_chain.s()]),
              }

    chains[load_type].delay()
