from edudl2.udl2 import W_post_etl, W_all_done
import logging


logger = logging.getLogger(__name__)


def get_post_etl_chain():
    task_list = [W_post_etl.task.s(), W_all_done.task.s()]
    return task_list
