from parcc_udl.pipeline.pipeline import ParccPipeline

__author__ = 'ablum'

import unittest
from celery import chain

MESSAGE_KEYS = ['landing_zone_work_dir', 'load_type', 'parts', 'guid_batch', 'input_file_path']


class TestParccPipeline(unittest.TestCase):

    def test_get_pipeline_chain_check_type(self):
        arch_file = 'path_to_some_file'
        load_type = 'some_load_type'
        file_part = 12
        batc_guid = '1234-s5678'
        pipeline_chain = ParccPipeline.create().get_pipeline_chain(arch_file, load_type, file_part, batc_guid)
        self.assertIsInstance(pipeline_chain, chain)


if __name__ == '__main__':
    unittest.main()
