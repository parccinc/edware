from edudl2.udl2.constants import Constants

__author__ = 'esnyder'


class ValidationConfig():

    def CSV_REQUIRED_HEADER_MAPPING(load_type, schema_version=None):
        REQUIRED_HEADERS_MAP = {
            Constants.LOAD_TYPE_ASMT_MATH: ValidationConfig.SUMMATIVE_ASSESSMENT_REQUIRED_HEADERS_MATH,
            Constants.LOAD_TYPE_ASMT_ELA: ValidationConfig.SUMMATIVE_ASSESSMENT_REQUIRED_HEADERS_ELA,
            Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_READING_COMP: ValidationConfig.ELA_ITEM_READ_COMP,
            Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_VOCAB: ValidationConfig.ELA_ITEM_VOCAB,
            Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_DECODING: ValidationConfig.ELA_ITEM_DECOD,
            Constants.LOAD_TYPE_DIAGNOSTIC_ELA_VOCAB: ValidationConfig.DIAGNOSTIC_ELA_VOCAB_REQUIRED_HEADERS,
            Constants.LOAD_TYPE_DIAGNOSTIC_ELA_WRITING: ValidationConfig.DIAGNOSTIC_ELA_WRITING_REQUIRED_HEADERS,
            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_COMPREHENSION: ValidationConfig.MATH_ITEM_COMP,
            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_PROGRESSION: ValidationConfig.MATH_ITEM_PROGRESS,
            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_FLUENCY: ValidationConfig.MATH_ITEM_FLUENCY,
            Constants.LOAD_TYPE_MYA_ASMT_ELA: ValidationConfig.MYA_ELA_REQUIRED_HEADERS,
            Constants.LOAD_TYPE_MYA_ASMT_MATH: ValidationConfig.MYA_MATH_REQUIRED_HEADERS,
            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_LOCATOR: ValidationConfig.DIAGNOSTIC_MATH_LOCATOR_REQUIRED_HEADERS,
            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_PROGRESSION: ValidationConfig.DIAGNOSTIC_MATH_PROGRESSION_REQUIRED_HEADERS,
            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_FLUENCY: ValidationConfig.DIAGNOSTIC_MATH_FLUENCY_REQUIRED_HEADERS,
            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_GRADE_LEVEL: ValidationConfig.DIAGNOSTIC_MATH_GRADE_LEVEL_REQUIRED_HEADERS,
            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_CLUSTER: ValidationConfig.DIAGNOSTIC_MATH_CLUSTER_REQUIRED_HEADERS,
            Constants.LOAD_TYPE_SNL_ASMT: ValidationConfig.SNL_REQUIRED_HEADERS,
            Constants.LOAD_TYPE_READER_MOTIVATION: ValidationConfig.DIAGNOSTIC_RMS_REQUIRED_HEADERS,
            Constants.LOAD_TYPE_ELA_ITEM: ValidationConfig.SUMMATIVE_ITEM_REQUIRED_HEADERS_ELA,
            Constants.LOAD_TYPE_MATH_ITEM: ValidationConfig.SUMMATIVE_ITEM_REQUIRED_HEADERS_MATH
        }
        headers_map = REQUIRED_HEADERS_MAP.get(load_type, None)
        # if we have inherited items
        if isinstance(headers_map, dict):
            headers_map = headers_map.get(schema_version)
        return headers_map

    def CSV_VALIDATION_LOAD_TYPES():
        return [
            Constants.LOAD_TYPE_ASMT_ELA,
            Constants.LOAD_TYPE_ASMT_MATH,
            Constants.LOAD_TYPE_MYA_ASMT_MATH,
            Constants.LOAD_TYPE_MYA_ASMT_ELA,
            Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_READING_COMP,
            Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_VOCAB,
            Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_DECODING,
            Constants.LOAD_TYPE_DIAGNOSTIC_ELA_VOCAB,
            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_COMPREHENSION,
            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_PROGRESSION,
            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_FLUENCY,
            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_LOCATOR,
            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_PROGRESSION,
            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_FLUENCY,
            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_GRADE_LEVEL,
            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_CLUSTER,
            Constants.LOAD_TYPE_SNL_ASMT,
            Constants.LOAD_TYPE_READER_MOTIVATION,
            Constants.LOAD_TYPE_ELA_ITEM,
            Constants.LOAD_TYPE_MATH_ITEM
        ]

    BASE_REQUIRED_HEADERS = [
        'RecordType',
        'Period',
        'PARCCOrganizationCodeTestingSchool',
        'PARCCOrganizationCodeResponsibleSchool',
        'StateAbbreviation',
        'TestingDistrictIdentifier',
        'TestingDistrictName',
        'TestingSchoolIdentifier',
        'TestingSchoolName',
        'ResponsibleDistrictIdentifier',
        'ResponsibleDistrictName',
        'SchoolInstitutionIdentifier',
        'OrganizationName',
        'PARCCOrganizationType',
        'AdministrativeFundingControl',
        'ProgramType',
        'CountyANSICode',
        'EconomicResearchServiceRuralUrbanContinuumCode',
        'PARCCStudentIdentifier',
        'StateStudentIdentifier',
        'FirstName',
        'LastName',
        'Sex',
        'Birthdate',
        'GradeLevelWhenAssessed',
        'AssessmentAcademicSubject',
        'AssessmentRegistrationTestAttemptIdentifier',
        'FormID',
        'AssessmentYear',
        'TestCode'
    ]
    BASE_SUMMATIVE_ITEM_REQUIRED_HEADERS = [
        'AcademicYear',
        'TestAdminCode',
        'TestCode',
        'Subject',
        'FormID',
        'FormFormat',
        'PARCCStudentIdentifier',
        'StateStudentIdentifier',
        'GradeLevelWhenAssessed',
        'StudentTestUUID',
        'ItemUIN',
        'ItemMaxPoints',
        'SingleParentItemScore'
    ]

    SUMMATIVE_REQUIRED_HEADERS_SPRING_2015 = [
        'RecordType',
        'StateAbbreviation',
        'ResponsibleDistrictIdentifier',
        'ResponsibleDistrictName',
        'ResponsibleSchoolInstitutionIdentifier',
        'ResponsibleSchoolInstitutionName',
        'PARCCStudentIdentifier',
        'StateStudentIdentifier',
        'FirstName',
        'LastName',
        'Birthdate',
        'AssessmentYear',
        'SummativeScoreRecordUUID',
        'Sex',
        'GradeLevelWhenAssessed',
        'Subject',
        'Period',
        'TestCode',
        'AssessmentYear',
    ]

    SUMMATIVE_REQUIRED_HEADERS_SPRING_2016 = [
        'TestCode',
        'TestFormat',
        'Retest',
        'GradeLevelWhenAssessed',
        'Sex',
        'FirstName',
        'LastOrSurname',
        'Birthdate',
        'StateStudentIdentifier',
        'TestingSchoolCode',
        'TestingDistrictCode',
        'StateAbbreviation',
    ]

    SUMMATIVE_REQUIRED_HEADERS_FALL_2015 = [
        'RecordType',
        'StateAbbreviation',
        'ResponsibleDistrictCode',
        'ResponsibleDistrictName',
        'ResponsibleSchoolCode',
        'ResponsibleSchoolInstitutionName',
        'PARCCStudentIdentifier',
        'StateStudentIdentifier',
        'FirstName',
        'LastName',
        'Birthdate',
        'AssessmentYear',
        'SummativeScoreRecordUUID',
        'Sex',
        'GradeLevelWhenAssessed',
        'Subject',
        'Period',
        'TestCode',
        'AssessmentYear',
    ]

    FINAL_SUMMATIVE_REQUIRED_HEADERS = {
        Constants.ASMT_MATH_REF_TABLE_SPRING_2015: SUMMATIVE_REQUIRED_HEADERS_SPRING_2015,
        Constants.ASMT_MATH_REF_TABLE_FALL_2015: SUMMATIVE_REQUIRED_HEADERS_FALL_2015,
        Constants.ASMT_MATH_REF_TABLE_SPRING_2016: SUMMATIVE_REQUIRED_HEADERS_SPRING_2016,
        Constants.ASMT_ELA_REF_TABLE_SPRING_2015: SUMMATIVE_REQUIRED_HEADERS_SPRING_2015,
        Constants.ASMT_ELA_REF_TABLE_FALL_2015: SUMMATIVE_REQUIRED_HEADERS_FALL_2015,
        Constants.ASMT_ELA_REF_TABLE_SPRING_2016: SUMMATIVE_REQUIRED_HEADERS_SPRING_2016,
    }

    FINAL_DIAGNOSTIC_REQUIRED_HEADERS = [
        'RecordType',
        'StateAbbreviation',
        'ResponsibleDistrictIdentifier',
        'ResponsibleDistrictName',
        'ResponsibleSchoolInstitutionIdentifier',
        'ResponsibleSchoolInstitutionName',
        'PARCCStudentIdentifier',
        'StateStudentIdentifier',
        'LocalStudentIdentifier',
        'FirstName',
        'LastName',
        'Birthdate',
        'GradeLevelWhenAssessed',
        'Period',
        'TestCode',
        'Year',
        'AssessmentGrade'
    ]

    # Summative
    SUMMATIVE_ASSESSMENT_REQUIRED_HEADERS_ELA = FINAL_SUMMATIVE_REQUIRED_HEADERS
    SUMMATIVE_ASSESSMENT_REQUIRED_HEADERS_MATH = FINAL_SUMMATIVE_REQUIRED_HEADERS
    SUMMATIVE_ITEM_REQUIRED_HEADERS_ELA = BASE_SUMMATIVE_ITEM_REQUIRED_HEADERS
    SUMMATIVE_ITEM_REQUIRED_HEADERS_MATH = BASE_SUMMATIVE_ITEM_REQUIRED_HEADERS

    # Diagnostic Item
    DIAGNOSTIC_ITEM_REQUIRED_HEADERS = [
        'AssessmentAttemptGuid',
        'StudentGuid',
        'ItemGuid'
    ]

    ELA_ITEM_READ_COMP = DIAGNOSTIC_ITEM_REQUIRED_HEADERS
    ELA_ITEM_VOCAB = DIAGNOSTIC_ITEM_REQUIRED_HEADERS
    ELA_ITEM_DECOD = DIAGNOSTIC_ITEM_REQUIRED_HEADERS
    MATH_ITEM_COMP = DIAGNOSTIC_ITEM_REQUIRED_HEADERS
    MATH_ITEM_PROGRESS = DIAGNOSTIC_ITEM_REQUIRED_HEADERS
    MATH_ITEM_FLUENCY = DIAGNOSTIC_ITEM_REQUIRED_HEADERS

    # Diagnostic
    DIAGNOSTIC_COMMON_REQUIRED_HEADERS = [
        'AssessmentDate',
        'StaffMemberIdentifier',
    ]

    ELA_VOCAB_HEADERS = [
        'ScaleScore'
    ]

    ELA_READ_FLUENCY_HEADERS = [
        'WPM',
        'WCPM',
        'Accuracy',
        'Expressiveness'
    ]

    ELA_DECODING_HEADERS = [
        'P1CVC',
        'P2Blend',
        'P3Cvow',
        'P4Ccons',
        'P5Msyl1',
        'P6Msyl2'
    ]

    ELA_COMP_HEADERS = [
        'ScaleScore',
        'IRL',
        'Passage1Type',
        'Passage1RMM',
    ]

    MATH_LOCATOR_HEADERS = [
        'ScaleScore',
        'SuggestedGradeLevel'
    ]

    MATH_PROGRESSION_HEADERS = [
        'Progression',
        'CodeCluster1',
        'ProbCluster1',
        'ClassCluster1'
    ]

    MATH_FLUENCY_HEADERS = [
        'Skill1Name',
        'Skill1Totalitems',
        'Skill1Time',
        'Skill1Correcttime',
        'Skill1Numbercorrect',
        'Skill1Numberincorrect',
        'SkillName',
        'SkillsTotalItems',
        'SkillsTotalTime',
        'SkillsTotalCorrecttime',
        'SkillsTotalNumbercorrect',
        'SkillsTotalNumberincorrect'
    ]

    MATH_GRADE_LEVEL_HEADERS = [
        'ClusterGradeLevel',
        'CodeCluster1',
        'ProbCluster1',
        'ClassCluster1'
    ]

    MATH_CLUSTER_HEADERS = [
        'CodeCluster',
        'ProbCluster',
        'ClassCluster'
    ]

    DIAGNOSTIC_REQUIRED_HEADERS = FINAL_DIAGNOSTIC_REQUIRED_HEADERS + DIAGNOSTIC_COMMON_REQUIRED_HEADERS

    DIAGNOSTIC_ELA_VOCAB_REQUIRED_HEADERS = DIAGNOSTIC_REQUIRED_HEADERS + ELA_VOCAB_HEADERS
    DIAGNOSTIC_ELA_WRITING_REQUIRED_HEADERS = DIAGNOSTIC_REQUIRED_HEADERS
    DIAGNOSTIC_ELA_COMP_REQUIRED_HEADERS = DIAGNOSTIC_REQUIRED_HEADERS + ELA_COMP_HEADERS
    DIAGNOSTIC_ELA_DECODING_REQUIRED_HEADERS = DIAGNOSTIC_REQUIRED_HEADERS + ELA_DECODING_HEADERS
    DIAGNOSTIC_ELA_READ_FLU_REQUIRED_HEADERS = DIAGNOSTIC_REQUIRED_HEADERS + ELA_READ_FLUENCY_HEADERS
    DIAGNOSTIC_RMS_REQUIRED_HEADERS = DIAGNOSTIC_REQUIRED_HEADERS
    DIAGNOSTIC_MATH_LOCATOR_REQUIRED_HEADERS = DIAGNOSTIC_REQUIRED_HEADERS + MATH_LOCATOR_HEADERS
    DIAGNOSTIC_MATH_PROGRESSION_REQUIRED_HEADERS = DIAGNOSTIC_REQUIRED_HEADERS + MATH_PROGRESSION_HEADERS
    DIAGNOSTIC_MATH_FLUENCY_REQUIRED_HEADERS = DIAGNOSTIC_REQUIRED_HEADERS + MATH_FLUENCY_HEADERS
    DIAGNOSTIC_MATH_GRADE_LEVEL_REQUIRED_HEADERS = DIAGNOSTIC_REQUIRED_HEADERS + MATH_GRADE_LEVEL_HEADERS
    DIAGNOSTIC_MATH_CLUSTER_REQUIRED_HEADERS = DIAGNOSTIC_REQUIRED_HEADERS + MATH_CLUSTER_HEADERS

    # MYA
    MYA_EXTRA_HEADERS = [
        'AssessmentGuid',
        'AssessmentSubtestResultScoreValue'
    ]
    MYA_REQUIRED_HEADERS = BASE_REQUIRED_HEADERS + MYA_EXTRA_HEADERS

    MYA_ELA_EXTRA_HEADERS = ['RST']
    MYA_MATH_EXTRA_HEADERS = ['ModuleName1Score']

    MYA_ELA_REQUIRED_HEADERS = MYA_REQUIRED_HEADERS + MYA_ELA_EXTRA_HEADERS
    MYA_MATH_REQUIRED_HEADERS = MYA_REQUIRED_HEADERS + MYA_MATH_EXTRA_HEADERS

    # SNL
    SNL_EXTRA_HEADERS = [
        'AssessmentGuid',
        'AssessmentTotalScore'
    ]
    SNL_REQUIRED_HEADERS = BASE_REQUIRED_HEADERS + SNL_EXTRA_HEADERS
