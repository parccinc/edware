'''
This module contains http-related exceptions used in EdApi

Created on Jan 18, 2013

@author: dip
'''
from pyramid.httpexceptions import HTTPNotFound, HTTPPreconditionFailed, HTTPRequestURITooLong,\
    HTTPForbidden, HTTPInternalServerError
import json
import logging


logger = logging.getLogger(__name__)


def generate_exception_response(msg, cls_name: str=None) -> dict:
    '''
    Generates kwargs for an exception response in json format

    :param msg: the error message
    :type msg: string
    :param cls_name: current exception class name
    :type cls_name: string
    '''

    if cls_name:
        logger.warning('{cls_name}: {msg}'.format(cls_name=cls_name, msg=msg))
    else:
        logger.warning(msg)
    return {'text': json.dumps({'error': str(msg)}), 'content_type': "application/json"}


class EdApiHTTPNotFound(HTTPNotFound):
    '''
    a custom http exception return when resource not found
    '''
    #code = 404
    #title = 'Requested report not found'
    #explanation = ('The resource could not be found.')

    def __init__(self, msg):
        '''
        :param msg: the error message
        :type msg: string
        '''
        super().__init__(**generate_exception_response(msg, self.__class__.__name__))


class EdApiHTTPPreconditionFailed(HTTPPreconditionFailed):
    '''
    a custom http exception when precondition is not met
    '''
    #code = 412
    #title = 'Parameter validation failed'
    #xplanation = ('Request precondition failed.')

    def __init__(self, msg):
        '''
        :param msg: the error message
        :type msg: string
        '''
        super().__init__(**generate_exception_response(msg, self.__class__.__name__))


class EdApiHTTPRequestURITooLong(HTTPRequestURITooLong):
    '''
    a custom http exception when uri is too long
    '''

    def __init__(self, max_length):
        '''
        :param max_length: the URI maximum length
        :type max_length: number
        '''
        msg = "Request URI too long - maximum size supported is %s characters" % max_length
        super().__init__(**generate_exception_response(msg, self.__class__.__name__))


class EdApiHTTPForbiddenAccess(HTTPForbidden):
    '''
    a custom http exception for forbidden access
    '''

    def __init__(self, msg):
        '''
        :param msg: error message
        :type msg: string
        '''
        super().__init__(**generate_exception_response(msg, self.__class__.__name__))


class EdApiHTTPInternalServerError(HTTPInternalServerError):
    '''
    a custom http internal server exception
    '''
    def __init__(self, msg):
        '''
        :param msg: error message
        :type msg: string
        '''
        super().__init__(**generate_exception_response(msg, self.__class__.__name__))
