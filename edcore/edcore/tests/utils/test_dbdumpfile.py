
'''
Created on May 25, 2015

@author: tcollins
'''
import configparser
import unittest
import builtins
import glob
import re

from unittest.mock import Mock, patch, DEFAULT, mock_open
from edcore.utils.bucket_helper import BucketHelper
from edcore.utils.dbdumpfile import DBDumpFile, DBDumpFileManager
from boto.s3.connection import Bucket


class TestUtils(unittest.TestCase):

    def setUp(self):
        filename_format = "{dbid}-filename-test"

    def test_s3_location_awareness(self):
        """
        Check DBDumpFile knows whether it is using s3 or not
        """
        local_dbfile = DBDumpFile(tenant="dino", filename_format=filename_format, location="/tmp/dumptest/", status="TESTFILE")
        self.assertFalse(local_dbfile._is_on_s3())
        for location in ["s3://parcc-analytics-source-dev/", "s3://parcc-analytics-source-dev/testrealm/", "s3://parcc-analytics-source-dev/fun%y"]:
            yield self.check_s3_location_awareness(location)

    def check_s3_location_awareness(self, location):
        """
        Check DBDumpFile knows whether it is using s3 or not
        """
        filename_format = "{dbid}-filename-test"
        s3_dbfile = DBDumpFile(tenant="dino", filename_format=filename_format, location=location, status="TESTFILE")
        self.assertTrue(s3_dbfile._is_on_s3())

    def test_filename_generation_local(self):
        dbfile = DBDumpFile(tenant="dino", filename_format="{dbid}-filename-test", location="/tmp/dumptest/", dbid="nosetest", status_separator="@#", status="TESTFILE")
        self.assertEqual(dbfile.complete_filepath, '/tmp/dumptest/dino@^nosetest-filename-test@#TESTFILE')

    def test_filename_generation_s3(self):
        for location in ["s3://parcc-analytics-source-dev/", "s3://parcc-analytics-source-dev/testrealm/", "s3://parcc-analytics-source-dev/fun%y"]:
            yield self.check_filename_generation_s3(location)

    def check_filename_generation_s3(self, location):
        dbfile = DBDumpFile(tenant="dino", filename_format="{dbid}-filename-test", location=location, dbid="nosetest", status_separator="@#", status="TESTFILE")
        self.assertEqual(dbfile.complete_filepath, '%s@^dino@^TESTFILE@#nosetest-filename-test' % location)

    def test_find_all_with_status(self):
        """
        Check that all matching DBDumpFiles are found with a given status
        """

        filename_format = "{dbid}-filename-test"
        alt_filename_format = "{dbid}-altfilename-test"
        for location in ("s3://parcc-analytics-source-dev/", "s3://parcc-analytics-source-dev/testrealm/", "s3://parcc-analytics-source-dev/fun%y", "/tmp/dumptest/"):
            testfiles = []
            altfiles = []
            for i in range(1, 6):
                dbfile = DBDumpFile(tenant="dino", dbid=i, filename_format=filename_format, location=location, status="TESTFILE")
                foo = Mock()
                foo.name = dbfile.complete_filepath
                testfiles.append(foo)
            for i in range(1, 4):
                dbfile = DBDumpFile(tenant="dino", dbid=i, filename_format=alt_filename_format, location=location, status="ALTTESTFILE")
                foo = Mock()
                foo.name = dbfile.complete_filepath
                testfiles.append(foo)

            def mock_list(*args, **kwargs):
                return testfiles

            def mock_glob(pattern):
                pattern = re.sub(re.escape("\*"), ".*", re.escape(pattern))
                return [x.name for x in testfiles if re.match(pattern, x.name)]

            m = Mock()
            m.list = mock_list
            with patch.object(BucketHelper, 'get_bucket', return_value=m):
                with patch.object(glob, 'glob', mock_glob):
                    testfile_mgr = DBDumpFileManager(location=location, filename_format=filename_format, tenant="dino")
                    returned_testfiles = testfile_mgr.find_all_with_status("TESTFILE")

            self.assertEquals(len(returned_testfiles), 5, "testfiles is %r" % testfiles)

    def test_find_latest_works(self):
        """
        Write some files out and make sure that we get the latest of each kind
        """
        filename_format = "{dbid}-filename-test"
        alt_filename_format = "{dbid}-altfilename-test"
        gz_path = "/tmp/"
        for location in ("s3://parcc-analytics-source-dev/", "s3://parcc-analytics-source-dev/testrealm/", "s3://parcc-analytics-source-dev/fun%y", "/tmp/dumptest/"):
            testfiles = []
            for i in range(1, 6):
                dbfile = DBDumpFile(tenant="dino", dbid=i, filename_format=filename_format, location=location, status="TESTFILE")
                foo = Mock()
                if dbfile._is_on_s3():
                    foo.name = dbfile._generate_filename()
                else:
                    foo.name = dbfile.complete_filepath
                testfiles.append(foo)
            for i in range(1, 4):
                dbfile = DBDumpFile(tenant="dino", dbid=i, filename_format=alt_filename_format, location=location, status="ALTTESTFILE")
                foo = Mock()
                if dbfile._is_on_s3():
                    foo.name = dbfile._generate_filename()
                else:
                    foo.name = dbfile.complete_filepath
                testfiles.append(foo)

            def mock_list(*args, **kwargs):
                return testfiles

            def mock_glob(pattern):
                pattern = re.sub(re.escape("\*"), ".*", re.escape(pattern))
                return [x.name for x in testfiles if re.match(pattern, x.name)]

            latest_testfile_standard = DBDumpFile(tenant="dino", dbid=5, filename_format=filename_format, location=location, status="TESTFILE", gz_archive_path=gz_path)
            latest_alttestfile_standard = DBDumpFile(tenant="dino", dbid=3, filename_format=alt_filename_format, location=location, status="ALTTESTFILE", gz_archive_path=gz_path)
            m = Mock()
            m.list = mock_list
            with patch.object(BucketHelper, 'get_bucket', return_value=m):
                with patch.object(glob, 'glob', mock_glob):
                    testfile_mgr = DBDumpFileManager(location=location, filename_format=filename_format, tenant="dino", gz_archive_path=gz_path)
                    alttestfile_mgr = DBDumpFileManager(location=location, filename_format=alt_filename_format, tenant="dino", gz_archive_path=gz_path)
                    latest_testfile = testfile_mgr.find_latest_with_status("TESTFILE")
                    latest_alttestfile = alttestfile_mgr.find_latest_with_status("ALTTESTFILE")
                    self.assertEquals(latest_testfile.complete_filepath, latest_testfile_standard.complete_filepath)
                    self.assertEquals(latest_alttestfile.complete_filepath, latest_alttestfile_standard.complete_filepath)

    def test_use_existing_file_works(self):
        """
        create a DBDumpFile object from a pre-existing file
        """
        filename_format = "{dbid}-filename-test"
        for location in ("s3://parcc-analytics-source-dev/", "s3://parcc-analytics-source-dev/testrealm/", "s3://parcc-analytics-source-dev/fun%y", "/tmp/dumptest/"):
            dbfile = DBDumpFile(tenant="dino", filename_format=filename_format, location=location, status="TESTFILE")
            filename = dbfile._generate_filename()
            dbfile_from_existing = DBDumpFile.use_existing(location, filename_format, filename)
            self.assertEquals(dbfile.complete_filepath, dbfile_from_existing.complete_filepath)

    @patch.multiple(DBDumpFile, _set_filestatus_s3=DEFAULT, _set_filestatus_local=DEFAULT)
    def test_set_filestatus_works(self, **patch_kwargs):
        """
        check set_filestatus works locally
        """
        for location in ["s3://parcc-analytics-source-dev/", "s3://parcc-analytics-source-dev/testrealm/", "s3://parcc-analytics-source-dev/fun%y", "/tmp/dumptest/"]:
            filename_format = "{dbid}-filename-test"
            dbfile = DBDumpFile(tenant="dino", filename_format=filename_format, location=location, status="TESTFILE")
            orig_filename = dbfile._generate_filename()
            dbfile.set_filestatus("ALTTESTFILE")
            self.assertEquals(dbfile._generate_filename(), orig_filename.replace("TESTFILE", "ALTTESTFILE"))

    def generate_mock_testfiles(self, location="/tmp/dbdumptestfiles/", filename_format="{dbid}-filename-test", alt_filename_format="{dbid}-altfilename-test", num_files=5, num_altfiles=3):
        testfiles = []
        for i in range(1, num_files + 1):
            dbfile = DBDumpFile(tenant="dino", dbid=i, filename_format=filename_format, location=location, status="TESTFILE")
            foo = Mock()
            if dbfile._is_on_s3():
                foo.name = dbfile._generate_filename()
            else:
                foo.name = dbfile.complete_filepath
            testfiles.append(foo.name)
        for i in range(1, num_altfiles + 1):
            dbfile = DBDumpFile(tenant="dino", dbid=i, filename_format=alt_filename_format, location=location, status="ALTTESTFILE")
            foo = Mock()
            if dbfile._is_on_s3():
                foo.name = dbfile._generate_filename()
            else:
                foo.name = dbfile.complete_filepath
            testfiles.append(foo.name)

        return testfiles

    @patch.multiple(DBDumpFile, delete_file=DEFAULT)
    def test_bulk_delete_works(self, **patch_kwargs):
        """
        Check bulk_delete deletes what it should not what it shouldn't
        """
        delete_mock = patch_kwargs['delete_file']
        filename_format = "{dbid}-filename-test"
        for location in ("s3://parcc-analytics-source-dev/", "s3://parcc-analytics-source-dev/testrealm/", "s3://parcc-analytics-source-dev/fun%y", "/tmp/dumptest/"):
            delete_mock.reset_mock()
            testfiles = self.generate_mock_testfiles(location=location, filename_format=filename_format, num_altfiles=0)
            testfile_mgr = DBDumpFileManager(location=location, filename_format=filename_format, tenant="dino")
            testfile_mgr.bulk_delete(testfiles)
            self.assertEquals(len(testfiles), len(delete_mock.mock_calls))

    @patch.multiple(DBDumpFile, delete_file=DEFAULT)
    def test_delete_older_works(self, **patch_kwargs):
        """
        Check that delete_older flag deletes what it is supposed to, and not what it shouldn't
        """
        delete_mock = patch_kwargs['delete_file']
        filename_format = "{dbid}-filename-test"
        gz_path = "/tmp/"
        for location in ("s3://parcc-analytics-source-dev/", "s3://parcc-analytics-source-dev/testrealm/", "s3://parcc-analytics-source-dev/fun%y", "/tmp/dumptest/"):
            delete_mock.reset_mock()
            num_testfiles = 5
            testfiles = self.generate_mock_testfiles(location=location, filename_format=filename_format, num_files=num_testfiles, num_altfiles=0)

            with patch.object(DBDumpFileManager, "find_all_with_status", return_value=testfiles):
                testfile_mgr = DBDumpFileManager(location=location, filename_format=filename_format,
                                                 tenant="dino", gz_archive_path=gz_path)
                latest_testfile = testfile_mgr.find_latest_with_status("TESTFILE", delete_older=True)
                self.assertEquals(num_testfiles - 1, len(delete_mock.mock_calls))

if __name__ == "__main__":
    unittest.main()
