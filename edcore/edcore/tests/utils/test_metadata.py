from edschema.metadata.ed_metadata import generate_ed_metadata


def generate_test_metadata(schema_name=None, bind=None):
    return generate_ed_metadata(schema_name=schema_name, bind=bind)
