'''
Created on May 18, 2015

@author: dip
'''
import unittest
from edcore.database.cds_connector import CdsDBConnection


class TestCDSConnector(unittest.TestCase):

    def test_get_namespace(self):
        self.assertEqual(CdsDBConnection.get_namespace(), 'edware.cds.db.')

    def test_get_datasource_name(self):
        self.assertEqual(CdsDBConnection.get_datasource_name('tenantName'), 'edware.cds.db.tenantName')

    def test_get_db_config_prefix(self):
        self.assertEqual(CdsDBConnection.get_db_config_prefix('tenantName'), 'edware.cds.db.tenantName.')

    def test_generate_metadata(self):
        meta = CdsDBConnection.generate_metadata()
        self.assertIsNotNone(meta)
        self.assertEquals(len(meta.tables), 5)

if __name__ == "__main__":
    unittest.main()
