'''
Created on May 18, 2015

@author: dip
'''
import unittest
from edcore.database.routing import ReportingDbConnection
from edcore.tests.utils.unittest_with_edcore_sqlite import get_unittest_tenant_name,\
    Unittest_with_edcore_sqlite
from edcore.database.edcore_connector import EdCoreDBConnection
from edcore.database.cds_connector import CdsDBConnection


class TestRouting(Unittest_with_edcore_sqlite):

    def test_instance_type(self):
        conn = ReportingDbConnection(tenant=get_unittest_tenant_name(), is_public=False)
        self.assertIsInstance(conn.db_conn, EdCoreDBConnection)

    def test_public_instance_type(self):
        # Override config for UT purposes
        CdsDBConnection.CONFIG_NAMESPACE = EdCoreDBConnection.CONFIG_NAMESPACE
        conn = ReportingDbConnection(tenant=get_unittest_tenant_name(), is_public=True)
        self.assertIsInstance(conn.db_conn, CdsDBConnection)

if __name__ == "__main__":
    unittest.main()
