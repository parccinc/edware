
'''
Created on Jan 23, 2014

@author: dip
'''
from edcore.utils.utils import reverse_map
from pyramid.threadlocal import get_current_registry

TENANT_MAP = {}
CDS_TENANT_MAP = {}


def set_tenant_map(tenant_map):
    '''
    Sets the tenant to state code mapping
    '''
    global TENANT_MAP
    TENANT_MAP = tenant_map


def set_cds_tenant_map(cds_tenants_map):
    '''
    Sets cds tenant to state code mapping
    '''
    global CDS_TENANT_MAP
    CDS_TENANT_MAP = {}
    for tenant in cds_tenants_map:
        CDS_TENANT_MAP[tenant] = TENANT_MAP.get(tenant)


def get_state_code_mapping(tenants):
    '''
    Given a list of tenants, return list of state code that it maps to
    :param list tenants:  list of tenants
    '''
    state_codes = []
    if tenants:
        for tenant in tenants:
            state_codes.append(TENANT_MAP.get(tenant))
    return state_codes


def get_tenant_map():
    return TENANT_MAP


def get_cds_tenants_map():
    return CDS_TENANT_MAP


def get_cds_states_text():
    state_codes = sorted([state_code for state_code in get_cds_tenants_map().values() if state_code])
    return ", ".join(state_codes)


def get_state_code_to_tenant_map():
    '''
    Returns tenant to state code mapping
    '''
    return reverse_map(TENANT_MAP)


def get_tenant_by_state_code(state_code):
    '''
    Returns teant given state_code
    @param param: state_code
    '''
    return get_state_code_to_tenant_map().get(state_code)


def get_all_tenants():
    return list(TENANT_MAP.keys())


def get_all_state_codes():
    return list(TENANT_MAP.values())


def get_cds_opt_out_states():
    return set(TENANT_MAP.values()) - set(CDS_TENANT_MAP.values())


def get_public_access_states():
    '''
    :return: List of states which have granted all other states access to their data
    '''
    registry = get_current_registry()
    public_access_state_list = []
    if registry.settings and 'reporting.public_access.states' in registry.settings:
        public_access_state_list = registry.settings.get('reporting.public_access.states').split(',')
    return list(map(str.strip, public_access_state_list))


def is_public_access_state(state_code):
    return state_code in get_public_access_states()
