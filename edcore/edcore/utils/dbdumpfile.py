'''
Created on May 20, 2015

@authors: tcollins, mjacob
'''
import datetime
import errno
import glob
import logging
import os
import re
from shutil import copyfile
from edcore.utils.bucket_helper import BucketHelper


class DBDumpFile:
    """
    A class to wrap the pgdump files created by edmigrate and consumed by starmigrate.
    Manages pgdumpfile writing, tracking status (via altering filename),
    and handles s3 or local operations.
    """

    status_separator = "@#"
    tenant_separator = "@^"

    def __init__(self, status="incomplete", filename_format=None, location=None, dbid=None, tenant=None,
                 status_separator=status_separator, tenant_separator=tenant_separator, gz_archive_path=None,
                 aws_access_key=None, aws_secret_access_key=None):
        self.status_separator = status_separator

        if location is None:
            raise ValueError("location can not be None")

        if filename_format is None:
            raise ValueError("filename_format can not be None")
        if re.search(self.status_separator, filename_format):
            raise ValueError("filename_format can not contain %s" % self.status_separator)
        if re.search(self.tenant_separator, filename_format):
            raise ValueError("filename_format can not contain %s" % self.tenant_separator)

        if dbid is None:
            self.dbid = datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S.%f")
        else:
            self.dbid = dbid

        self.status = status
        self.tenant = tenant
        self.filename_format = filename_format
        self.location = location

        self.gz_archive_path = gz_archive_path
        self.archived_dump_files = []

        self.aws_access_key = aws_access_key
        self.aws_secret_access_key = aws_secret_access_key

        self.logger = logging.getLogger('DBDumpFile')

    @property
    def base_filename(self):
        return self.filename_format.format(dbid=self.dbid)

    @property
    def complete_filepath(self):
        root_location = self.location
        if self._is_on_s3():
            root_location = "%s%s/" % (BucketHelper.S3_BUCKET_PROTOCOL, BucketHelper.get_bucketname_from_location(self.location))

        return "%s%s" % (root_location, self._generate_filename())

    def set_filestatus(self, new_status):
        """
        set dbdump file status.  currently handled through filename.
        Statuses: incomplete, complete, processing, ?failed, ?done
        """

        # since we rely on [separator chars] to clear status,
        # status may not contain string [separator chars]
        if re.search(self.status_separator, new_status):
            raise ValueError("Status may not contain character string \"%s\"" % self.status_separator)
        if re.search(self.tenant_separator, new_status):
            raise ValueError("Status may not contain character string \"%s\"" % self.tenant_separator)

        old_status = self.status
        self.status = new_status

        if self._is_on_s3():
            self.logger.info("setting s3 file status to %s" % new_status)
            self._set_filestatus_s3(old_status, new_status)
        else:
            self.logger.info("setting local file status to %s" % new_status)
            self._set_filestatus_local(old_status, new_status)

    def _is_on_s3(self):
        return BucketHelper.location_is_on_s3(self.location)

    def _set_filestatus_local(self, old_status, new_status):
        """
        renames the file with correct status locally
        """
        source = "%s%s" % (self.location, self._generate_filename(old_status))
        dest = "%s%s" % (self.location, self._generate_filename(new_status))
        self.logger.info("moving %s to %s" % (source, dest))
        os.rename(source, dest)

    def _set_filestatus_s3(self, old_status, new_status):
        """
        copes the file to new name with correct status, and deletes the old, on s3
        """
        source = self._generate_filename(old_status)
        dest = self._generate_filename(new_status)
        self.logger.info("moving %s to %s" % (source, dest))
        bucket = BucketHelper.get_bucket(self.location, self.aws_access_key, self.aws_secret_access_key)
        key = bucket.get_key(source)
        key.copy(bucket, dest, encrypt_key=True)
        key.delete()

    def _generate_filename(self, status=None):
        """
        appends "[separator][status]" to the filename
        """
        if status is None:
            status = self.status
        if self._is_on_s3():
            location_prefix = BucketHelper.get_prefix_from_location(self.location)
            return "%s%s%s%s%s%s%s" % (location_prefix, self.tenant_separator, self.tenant, self.tenant_separator, status, self.status_separator, self.base_filename)
        return "%s%s%s%s%s" % (self.tenant, self.tenant_separator, self.base_filename, self.status_separator, status)

    def write_file(self, contents):
        """
        Write the file to either a local dir (dev) or encrypted s3 bucket.
        Currently just creates an empty file.
        """

        if self._is_on_s3():
            self._write_file_s3(contents)
            self.logger.info("writing s3 file")
        else:
            self._write_file_local(contents)
            self.logger.info("writing local file")

    def _write_file_local(self, contents):
        """
        Write the file to either a local dir (dev)
        Currently just creates an empty file.
        """

        self.logger.info("writing complete_filepath: %s" % self.complete_filepath)

        # ensure directory exists
        try:
            os.makedirs(self.location)
        except OSError as e:
            # if dir already exists, ignore error, otherwise, raise exception
            if e.errno != errno.EEXIST:
                raise

        # write our file
        file = open(self.complete_filepath, "w")
        file.write(contents)
        file.close()

    def _write_file_s3(self, contents):
        """
        Write the file to encrypted s3 bucket.
        """
        k = BucketHelper.get_key(self.location, self.aws_access_key, self.aws_secret_access_key)
        k.key = self._generate_filename()
        self.logger.info("writing to bucket %s with key filename %s" % (k.bucket.name, k.key))
        k.set_contents_from_string(contents, encrypt_key=True)

    def write_from_file(self, source_file):
        """
        Write local source_file to either a local dir (dev) or encrypted s3 bucket.
        """

        if self._is_on_s3():
            self._write_from_file_s3(source_file)
            self.logger.info("writing s3 file")
        else:
            self._write_from_file_local(source_file)
            self.logger.info("writing local file")

    def _write_from_file_local(self, source_file):
        """
        Write the file to either a local dir (dev)
        Currently just creates an empty file.
        """

        self.logger.info("writing complete_filepath: %s" % self.complete_filepath)

        # ensure directory exists
        try:
            os.makedirs(self.location)
        except OSError as e:
            # if dir already exists, ignore error, otherwise, raise exception
            if e.errno != errno.EEXIST:
                raise

        # write our file
        copyfile(source_file, self.complete_filepath)

    def _write_from_file_s3(self, source_file):
        """
        Write the file to encrypted s3 bucket.
        """
        k = BucketHelper.get_key(self.location, self.aws_access_key, self.aws_secret_access_key)
        k.key = self._generate_filename()
        self.logger.info("writing source_file %s to bucket %s with key filename %s" % (source_file, k.bucket.name, k.key))
        k.set_contents_from_filename(source_file, encrypt_key=True)

    def read_file(self):
        """
        Read the file, return a string
        """

        if self._is_on_s3():
            return self._read_file_s3()
        else:
            return self._read_file_local()

    def _read_file_s3(self):

        k = BucketHelper.get_key(self.location, self.aws_access_key, self.aws_secret_access_key)
        k.key = self._generate_filename()
        self.logger.info("writing to bucket %s with key filename %s" % (k.bucket.name, k.key))
        # this should be get_contents_to_file with a file-like object fp
        # k.get_contents_to_file(fp, encrypt_key=True)
        # TODO: this should be binary?
        contents = k.get_contents_as_string(encoding="UTF-8")
        return contents

    def _read_file_local(self):
        """
        this will likely be a large file, this slurps the whole file
        """
        with open(self.complete_filepath, 'r') as f:
            contents = f.read()
        return contents

    def read_file_to_stream(self, stream):
        """
        Read the contents of the file into stream
        """

        if self._is_on_s3():
            return self._read_file_to_stream_s3(stream)
        else:
            return self._read_file_to_stream_local(stream)

    def _read_file_to_stream_s3(self, stream):
        """
        Read the contents of the file into stream
        """
        k = BucketHelper.get_key(self.location, self.aws_access_key, self.aws_secret_access_key)
        k.key = self._generate_filename()
        self.logger.info("writing to bucket %s with key filename %s" % (k.bucket.name, k.key))
        # this should be get_contents_to_file with a file-like object fp
        # k.get_contents_to_file(fp, encrypt_key=True)
        k.get_contents_to_file(stream)

    def _read_file_to_stream_local(self, stream):
        """
        untested, just placeholder
        """
        with open(self.complete_filepath, 'rb') as f:
            stream.write(f.read())

    def complete_file(self):
        """
        perform necessary actions marking dump as complete (and ready to go)
        """
        self.set_filestatus("complete")

    def register_archived_dump_file(self, filename):
        """
        Added path to the csv.gz file with dump,
        """
        self.logger.info("Unpacked file: '{}'".format(filename))
        self.archived_dump_files.append(os.path.join(self.gz_archive_path, filename))

    def delete_file(self):
        if self._is_on_s3():
            self._delete_file_s3()
            self.logger.info("deleting s3 file")
        else:
            self._delete_file_local()
            self.logger.info("deleting local file")

    def delete_gz_files(self):
        """
        Delete unpacked files
        """
        for fpath in self.archived_dump_files:
            self.logger.info("deleting file: '{}'".format(os.path.basename(fpath)))
            os.remove(fpath)

    def _delete_file_local(self):
        # delete file from filesystem
        os.remove(self.complete_filepath)

    def _delete_file_s3(self):
        bucket = BucketHelper.get_bucket(self.location, self.aws_access_key, self.aws_secret_access_key)
        key = bucket.get_key(self._generate_filename())
        key.delete()

    @classmethod
    def use_existing(cls, location=None, filename_format=None, filename=None,
                     status_separator=status_separator, tenant_separator=tenant_separator,
                     gz_archive_path=None):
        """
        using filename format, will pass to __init__ values matched out of filename,
        with status being a special regex pinned to the end.
        """
        # parse filename for:
        # dbid, status
        if location is None:
            raise ValueError("location can not be None")

        if filename_format is None:
            raise ValueError("filename_format can not be None")

        if filename is None:
            raise ValueError("filename can not be None")

        if re.search(status_separator, filename_format):
            raise ValueError("filename_format can not contain %s" % status_separator)

        # if local, remove leading path info
        if not BucketHelper.location_is_on_s3(location):
            filename = os.path.basename(filename)

        # convert format string into a grouped regex
        format_regex = re.sub(r"\{([^\}]+)\}", r"(?P<\1>.*)", filename_format)
        # setup matching for status
        if BucketHelper.location_is_on_s3(location):
            location_prefix = BucketHelper.get_prefix_from_location(location)
            final_regex = "^%s%s%s%s%s%s%s" % (re.escape(location_prefix), re.escape(tenant_separator), "(?P<tenant>.*)", re.escape(tenant_separator), "(?P<status>.*)", re.escape(status_separator), format_regex)
        else:
            final_regex = "%s%s%s%s%s" % ("^(?P<tenant>.*)", re.escape(tenant_separator), format_regex, re.escape(status_separator), "?(?P<status>.*)$")
        p = re.compile(final_regex)
        m = p.match(filename)

        if m is None:
            raise ValueError("Filename %s does not match filename_format %s (final regex was %s)" % (filename, filename_format, final_regex))

        initargs = m.groupdict()
        initargs["location"] = location
        initargs["filename_format"] = filename_format
        initargs["gz_archive_path"] = gz_archive_path
        return cls(**initargs)


class DBDumpFileManager:
    """
    A class to manage all the DBDumpfiles found at a location with a given filename_format.
    Provides ability to get latest DBDumpfile of a given status.  Can also manage all files
    in a given location for mass update of status, etc.
    """

    status_separator = DBDumpFile.status_separator
    tenant_separator = DBDumpFile.tenant_separator

    def __init__(self, location=None, filename_format=None, gz_archive_path=None, tenant=None,
                 aws_access_key=None, aws_secret_access_key=None,
                 status_separator=status_separator, tenant_separator=tenant_separator):
        self.logger = logging.getLogger('DBDumpFileManager')
        if location is None:
            raise ValueError("location can not be None")

        if filename_format is None:
            raise ValueError("filename_format can not be None")

        self.location = location

        if not BucketHelper.location_is_on_s3(self.location):
            # make sure local paths end with trailing /
            self.location = os.path.join(location, "")

        if gz_archive_path:
            if not os.path.exists(gz_archive_path):
                os.makedirs(gz_archive_path)
            # make sure local paths end with trailing /
            self.gz_archive_path = os.path.join(gz_archive_path, "")

        self.filename_format = filename_format
        self.tenant = tenant
        self.aws_access_key = aws_access_key
        self.aws_secret_access_key = aws_secret_access_key

    def find_all_with_status(self, status=None, newest_first=True):
        """
        Glob the files for status (NB: will it work for S3?)
        """
        if BucketHelper.location_is_on_s3(self.location):
            # turn it into a list rather than boto.s3.BucketListResultSet
            return self._find_all_with_status_s3(status, newest_first)
        else:
            return self._find_all_with_status_local(status, newest_first)

    def _find_all_with_status_local(self, status=None, newest_first=True):
        if status is None:
            status = "*"
        valid_pattern = "%s%s%s%s%s%s" % (self.location, self.tenant, self.tenant_separator, self.filename_format.format(dbid="*"), self.status_separator, status)
        self.logger.info("valid_pattern: %s" % valid_pattern)
        # sort by name (leading timestamp ensures reverse sort first item is newest)
        return sorted(glob.glob(valid_pattern), reverse=newest_first)

    def _find_all_with_status_s3(self, status=None, newest_first=True):
        bucket = BucketHelper.get_bucket(self.location, self.aws_access_key, self.aws_secret_access_key)
        format_regex = "(%s)" % (re.sub(r"\{([^\}]+)\}", r".*", self.filename_format))
        frgx = re.compile(format_regex)
        return sorted([y.group(1) for y in [frgx.match(x.name) for x in bucket.list(prefix="%s%s%s%s%s" % (BucketHelper.get_prefix_from_location(self.location), self.tenant_separator, self.tenant, self.tenant_separator, status))] if y], reverse=newest_first)

    def find_latest_with_status(self, status, delete_older=False):
        """
        Finds all DBDumpFiles at location with given status, and returns the newest/latest one.
        if delete_older is true, any other files matching the status that are old will be deleted.
        """
        matching_files = self.find_all_with_status(status, newest_first=False)
        newest_matching_file = matching_files.pop() if matching_files else None
        self.logger.info("got file: %s" % newest_matching_file)
        # if no matching files, return None
        if newest_matching_file is None:
            return None
        dbfile = DBDumpFile.use_existing(self.location, self.filename_format, newest_matching_file,
                                         gz_archive_path=self.gz_archive_path)
        if delete_older:
            self.bulk_delete(matching_files)

        return dbfile

    def _bulk_process(self, file_list, action, *action_args):
        """
        given a filename list (from find_all_with_status() or similar call)
        iterate over that list, instantiating a DBDumpFile and performing
        some action() on it, with kwargs as relevant.
        Returns # of files processed
        """
        count = 0
        for db_filename in file_list:
            dbfile = DBDumpFile.use_existing(self.location, self.filename_format, db_filename)
            try:
                action_to_perform = getattr(dbfile, action)
                action_to_perform(*action_args)
            except Exception as e:
                self.logger.error("error during action %s on file %s:  %s.  (%s items succesfully processed before error)" % (action, db_filename, e, count))
                raise
            count += 1

        return

    def bulk_delete(self, file_list):
        return self._bulk_process(file_list, "delete_file")

    def bulk_status_change(self, file_list, status):
        return self._bulk_process(file_list, "set_filestatus", status)


class DBDumpFileException(Exception):
    def __init__(self, msg='DBDumpFile processing rasied exception'):
        super().__init__(msg)
