'''
Created on May 27, 2015

@author: dip
'''
from collections import namedtuple


QueryMapKey = namedtuple('QueryMapKey', 'type subject level tenant')
QueryMapKey.__new__.__defaults__ = (None, None, None, None)
