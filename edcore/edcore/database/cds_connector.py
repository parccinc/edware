'''
Created on May 8, 2015

@author: dip
'''
from edschema.database.connector import DBConnection
from edschema.metadata.reporting.summ_rpt_cds_metadata import generate_summ_rpt_cds_metadata


class CdsDBConnection(DBConnection):
    '''
    DBConnector for Consortium Data Store - there's currently no restrictions
    on accessing this database
    '''
    CONFIG_NAMESPACE = 'edware.cds.db'

    def __init__(self, tenant=None, **kwargs):
        super().__init__(name=self.get_datasource_name(tenant))

    @staticmethod
    def get_namespace():
        '''
        Returns the namespace of cds database connection
        '''
        return CdsDBConnection.CONFIG_NAMESPACE + '.'

    @staticmethod
    def get_datasource_name(tenant=None):
        '''
        Returns datasource name for a tenant
        '''
        if tenant is None:
            # Returns None will raise an Exception in base class
            return None
        return CdsDBConnection.get_namespace() + tenant

    @staticmethod
    def get_db_config_prefix(tenant=None):
        '''
        Returns database config prefix based on tenant name
        '''
        if tenant is None:
            # Returns None will raise an Exception in base class
            return None
        return CdsDBConnection.get_namespace() + tenant + '.'

    @staticmethod
    def generate_metadata(schema_name=None, bind=None):
        '''
        Generates metadata for cds
        '''
        return generate_summ_rpt_cds_metadata(schema_name=schema_name, bind=bind)
