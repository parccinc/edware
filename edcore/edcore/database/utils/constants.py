'''
Created on Mar 1, 2014

@author: dip
'''


class UdlStatsConstants():
    '''
    Constants related to extracts
    '''
    REC_ID = 'rec_id'
    BATCH_GUID = 'batch_guid'
    SCHEMA_NAME = 'schema_name'
    TENANT = 'tenant'
    DEST_TENANT = 'dest_tenant'
    FILE_ARRIVED = 'file_arrived'
    LOAD_START = 'load_start'
    LOAD_END = 'load_end'
    LOAD_TYPE = 'load_type'
    MIGRATE_TYPE = 'migrate_type'
    LOAD_STATUS = 'load_status'
    RECORD_LOADED_COUNT = 'record_loaded_count'
    LAST_PRE_CACHED = 'last_pre_cached'
    LAST_PDF_TASK_REQUESTED = 'last_pdf_task_requested'
    UDL_STATS = 'udl_stats'
    BATCH_OPERATION = 'batch_operation'
    SNAPSHOT_CRITERIA = 'snapshot_criteria'
    NOTIFICATION = 'notification'
    NOTIFICATION_STATUS = 'notification_status'
    # Available Load Status
    UDL_STATUS_RECEIVED = 'udl.received'
    UDL_STATUS_FAILED = 'udl.failed'
    UDL_STATUS_LOADING = 'udl.loading'
    UDL_STATUS_INGESTED = 'udl.ingested'
    MIGRATE_IN_PROCESS = 'migrate.in_process'
    MIGRATE_INGESTED = 'migrate.ingested'
    MIGRATE_FAILED = 'migrate.failed'
    DUMPDB_STARTED = 'dumpdb.started'
    DUMPDB_CREATED = 'dumpdb.created'
    DUMPDB_FAILED = 'dumpdb.failed'
    # Available Batch Operations
    SNAPSHOT = 's'


class LoadType():
    ASSESSMENT = 'assessment'
    STUDENT_REGISTRATION = 'studentregistration'
    LOAD_TYPE_ASSESSMENT_ELA = 'asmt_data.ela'
    LOAD_TYPE_ASSESSMENT_MATH = 'asmt_data.math'
    LOAD_TYPE_ITEM = 'item_student_score'
    LOAD_TYPE_PSYCHOMETRIC = 'p_data'
    LOAD_TYPE_MYA_ELA_ITEM = 'mya_ela_item_data'
    LOAD_TYPE_MYA_MATH_ITEM = 'mya_math_item_data'
    LOAD_TYPE_MYA_ASMT_MATH = 'mya_math_asmt_data'
    LOAD_TYPE_MYA_ASMT_ELA = 'mya_ela_asmt_data'
    LOAD_TYPE_SNL_STUDENT_TASK = 'snl_task_data'
    LOAD_TYPE_SNL_ASMT = 's&l_total_asmt_data'
    LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_READING_COMP = 'diagnostic_ela_item_reading_comp'
    LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_VOCAB = 'diagnostic_ela_item_vocab'
    LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_DECODING = 'diagnostic_ela_item_decoding'
    LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_COMP = 'diagnostic_math_item_comp'
    LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_PROGRESS = 'diagnostic_math_item_progression'
    LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_FLU = 'diagnostic_math_item_fluency'
    LOAD_TYPE_DIAGNOSTIC_ELA_VOCAB = 'diagnostic_ela_vocab'
    LOAD_TYPE_DIAGNOSTIC_ELA_COMP = 'diagnostic_ela_comp'
    LOAD_TYPE_DIAGNOSTIC_ELA_DECODING = 'diagnostic_ela_decoding'
    LOAD_TYPE_DIAGNOSTIC_ELA_READ_FLU = 'diagnostic_ela_reading_fluency'
    LOAD_TYPE_DIAGNOSTIC_ELA_WRITING = 'diagnostic_ela_writing'
    LOAD_TYPE_DIAGNOSTIC_MATH_LOCATOR = 'diagnostic_math_locator'
    LOAD_TYPE_DIAGNOSTIC_MATH_PROGRESS = 'diagnostic_math_progression'
    LOAD_TYPE_DIAGNOSTIC_MATH_FLU = 'diagnostic_math_fluency'
    LOAD_TYPE_DIAGNOSTIC_MATH_GRADE_LEVEL = 'diagnostic_math_gradelevel'
    LOAD_TYPE_DIAGNOSTIC_MATH_CLUSTER = 'diagnostic_math_cluster'
    LOAD_TYPE_READER_MOTIVATION = 'diagnostic_reader_motivation'
    LOAD_TYPE_ELA_ITEM = 'ela_item_student_score'
    LOAD_TYPE_MATH_ITEM = 'math_item_student_score'


class AssessmentType():
    SUMMATIVE = 'SUMMATIVE'
    INTERIM_COMPREHENSIVE = 'INTERIM COMPREHENSIVE'
    INTERIM_ASSESSMENTS_BLOCKS = 'INTERIM ASSESSMENT BLOCKS'


class Constants():
    STATUS_CURRENT = 'C'
    STATUS_SHADOW = 'S'
    STATUS_DELETE = 'D'
    STATUS_WAITING = 'W'
    BATCH_GUID = 'batch_guid'
    GUID_BATCH = 'guid_batch'
    REC_STATUS = 'rec_status'
    DIM_TABLES_PREFIX = 'dim_'
    FACT_TABLES_PREFIX = 'fact_'
    META_COLUMN = 'MetaColumn'
    RECORD_SID = 'record_sid'
