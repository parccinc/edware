'''
Created on May 14, 2015

@author: dip
'''
from edcore.database.cds_connector import CdsDBConnection
from edcore.database.edcore_connector import EdCoreDBConnection


class ReportingDbConnection():

    def __init__(self, tenant=None, state_code=None, is_public=False):
        '''
        :params str tenant:  Name of tenant
        :params str state_code name of state_code
        :params bool is_public.  True if we want to access de-identified datastore
        '''
        self.tenant = tenant
        self.state_code = state_code
        if is_public:
            self.db_conn = CdsDBConnection(tenant=tenant, state_code=state_code)
        else:
            self.db_conn = EdCoreDBConnection(tenant=tenant, state_code=state_code)

    def __enter__(self):
        return self.db_conn

    def __exit__(self, exc_type, value, tb):
        self.db_conn.close_connection()
