'''
Created on Mar 5, 2013

@author: tosako
'''
from edschema.database.connector import DBConnection
from pyramid.security import authenticated_userid
from pyramid.threadlocal import get_current_request
from edschema.metadata.ed_metadata import generate_ed_metadata
from edcore.security.tenant import get_tenant_by_state_code, is_public_access_state


class EdCoreDBConnection(DBConnection):
    '''
    DBConnector for Parcc Project
    This is used for database connection for reports
    '''
    CONFIG_NAMESPACE = 'edware.db'

    def __init__(self, tenant=None, state_code=None):
        tenant = self._get_tenant(tenant, state_code)
        if not tenant:
            raise ValueError("tenant cannot be found")
        if not self._has_access(tenant) and not is_public_access_state(state_code):
            raise ValueError("user doesn't have access to tenant %s" % (tenant,))
        super().__init__(name=self.get_datasource_name(tenant))

    def _get_tenant(self, tenant, state_code):
        if tenant:
            return tenant
        if state_code:
            return get_tenant_by_state_code(state_code)
        # return first tenant a user belongs to
        user_tenants = self._get_user_tenants()
        return user_tenants[0] if user_tenants else None

    def _get_user_tenants(self):
        # Get user's tenant from session
        user = authenticated_userid(get_current_request())
        if not user:
            return None
        tenants = user.get_tenants()
        return tenants

    def _has_access(self, tenant):
        user = authenticated_userid(get_current_request())
        if not user:
            # if no user information associated, then current connection is
            # established by celery
            return True
        return tenant in self._get_user_tenants()

    @staticmethod
    def get_namespace():
        '''
        Returns the namespace of smarter database connection
        '''
        return EdCoreDBConnection.CONFIG_NAMESPACE + '.'

    @staticmethod
    def get_datasource_name(tenant=None):
        '''
        Returns datasource name for a tenant
        '''
        if tenant is None:
            # Returns None will raise an Exception in base class
            return None
        return EdCoreDBConnection.get_namespace() + tenant

    @staticmethod
    def get_db_config_prefix(tenant=None):
        '''
        Returns database config prefix based on tenant name
        '''
        if tenant is None:
            # Returns None will raise an Exception in base class
            return None
        return EdCoreDBConnection.get_namespace() + tenant + '.'

    @staticmethod
    def generate_metadata(schema_name=None, bind=None):
        '''
        Generates metadata for edware
        '''
        return generate_ed_metadata(schema_name=schema_name, bind=bind)
