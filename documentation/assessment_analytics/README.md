ARCHITECTURAL ASSESSMENT OF PARCC ANALYTICS
---------------

This file contains the PoC code for the Architecture Assessment of PARCC Analytics conducted
by SoftServe in January 2016.

The Assessment Report is here https://docs.google.com/a/amplify.com/document/d/1uJZXJAjp_ZKhhmyklyJa0Yq0Op9XUK6A9T-FnFgHpJo/edit?usp=sharing

The code consists of a PoC Kettle ETL script to import Reporting data to Pentaho.

This code is not meant to be operational, but it gives a good impression of how ETL for PARCC shoudl be done.

