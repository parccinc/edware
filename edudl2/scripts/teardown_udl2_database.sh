#!/bin/sh

# use virtualenv to run initialization script

python -m edudl2.database.database $1 $2 --action teardown

# teardown edware star schema

python3.3 -m edschema.metadata_generator --metadata stats -a teardown -s edware_stats -d edware_stats --host=localhost:5432 -u edware -p edware2013

python3.3 -m edschema.metadata_generator --metadata edware -a teardown -s edware_prod -d edware --host=localhost:5432 -u edware -p edware2013
python3.3 -m edschema.metadata_generator --metadata edware -a teardown -s edware_prod_cat -d edware --host=localhost:5432 -u edware -p edware2013
python3.3 -m edschema.metadata_generator --metadata edware -a teardown -s edware_prod_fish -d edware --host=localhost:5432 -u edware -p edware2013

python3.3 -m edschema.metadata_generator --metadata cds -a teardown -s edware_tenant_cds_cat -d edware --host=localhost:5432 -u edware -p edware2013
python3.3 -m edschema.metadata_generator --metadata cds -a teardown -s edware_tenant_cds_dog -d edware --host=localhost:5432 -u edware -p edware2013

python3.3 -m edschema.metadata_generator --metadata cds -a teardown -s edware_cds -d edware --host=localhost:5432 -u edware -p edware2013