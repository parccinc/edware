#!/bin/sh

# Create users udl2 and edware, make udl2 and edware superusers, and finally create udl2, edware, and edware_stats
# databases. Note that a valid DB name needs to be passed to psql and the default DB name is "postgres".
psql postgres < create_users_and_dbs.sql

# initialize edware star schema

python3.3 -m edschema.metadata_generator --metadata stats -s edware_stats -d edware_stats --host=localhost:5432 -u edware -p edware2013

# Create prod schema
python3.3 -m edschema.metadata_generator --metadata edware -s edware_prod -d edware --host=localhost:5432 -u edware -p edware2013
python3.3 -m edschema.metadata_generator --metadata edware -s edware_prod_cat -d edware --host=localhost:5432 -u edware -p edware2013
python3.3 -m edschema.metadata_generator --metadata edware -s edware_prod_fish -d edware --host=localhost:5432 -u edware -p edware2013

# Create cds schemas
python3.3 -m edschema.metadata_generator --metadata cds -s edware_cds -d edware --host=localhost:5432 -u edware -p edware2013
python3.3 -m edschema.metadata_generator --metadata cds -s edware_tenant_cds_cat -d edware --host=localhost:5432 -u edware -p edware2013
python3.3 -m edschema.metadata_generator --metadata cds -s edware_tenant_cds_dog -d edware --host=localhost:5432 -u edware -p edware2013

# use virtualenv to run initialization script
# otherwise, it must be python3.3
python3.3 -m edudl2.database.database $1 $2 --action setup