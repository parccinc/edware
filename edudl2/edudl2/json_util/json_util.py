import json
import os
import logging
from requests.structures import CaseInsensitiveDict
from edudl2.udl2_util import file_util

__author__ = 'tshewchuk'
logger = logging.getLogger(__name__)


def get_attribute_value_from_json_keypath(json_file_path, *attribute_key_path):
    '''
    Determine the attribute value from the json file contents
    @param json_file_path: The full directory pathname of the json file
    @type string
    @param attribute_key_path: the key path to search the json for
    @type tuple
    @return: value of the key
    @rtype: string
    '''

    attribute_value = None
    try:
        json_object = read_json(json_file_path)
        attribute_value = json_object
        for key in attribute_key_path:
            # If the value is a list, grab the first instance and continue
            if isinstance(attribute_value, list):
                attribute_value = attribute_value[0].get(key)
            else:
                attribute_value = attribute_value.get(key)
    except ValueError:
        logger.error('Malformed json file %s' % json_file_path)
    except KeyError:
        logger.error('Cannot find key %s in file %s' % (str(attribute_key_path), json_file_path))
    except AttributeError:
        logger.error('The given path %s in file %s is invalid' % (str(attribute_key_path), json_file_path))
    return attribute_value


def get_value_from_json(json_file_dir, attribute_key_path):
    '''
    Gets a nested attribute from a json file
    @param json_file_dir: The directory name which contains a json file
    @type string
    @param attribute_key_path: The key for which to retrieve the value. A nested value can be denoted using '.' between each key.
    @type string
    @return: the value of the key from the json
    '''

    json_file_name = file_util.get_file_name_in_dir(json_file_dir, 'json')
    json_file_path = os.path.join(json_file_dir, json_file_name)

    keys = attribute_key_path.split('.')
    attribute = get_attribute_value_from_json_keypath(json_file_path, *keys)

    return attribute


def read_json(json_file_path):
        with open(json_file_path) as json_file:
            json_data = json.load(json_file, object_hook=CaseInsensitiveDict)
        return json_data
