import os
import logging
import shutil
import tarfile
import zipfile
from edudl2.udl2.constants import Constants

logger = logging.getLogger(__name__)


def _is_valid_archive(file_to_expand, archive_type):
    """Check if file is valid archive.

    :param file_to_expand: archive file
    :param archive_type: type of archive file
    :return: True or False

    """
    validators = {
        Constants.ZIP_TYPE: _is_zip_file,
        Constants.TARBALL_TYPE: _is_tar_file,
    }

    valid = _is_file_exists(file_to_expand)
    if valid:
        logger.info('File exists and is readable -- {}'.format(file_to_expand))
        if archive_type in validators and validators[archive_type](file_to_expand):
            logger.info('Valid {0} archive -- {1}'.format(archive_type, file_to_expand))
        else:
            logger.error('{0} is not a valid {1} archive.'.format(file_to_expand, archive_type))
    else:
        logger.error('File missing or un-readable -- {}'.format(file_to_expand))
    return valid


def _get_archive_type_by_extension(file_to_expand):
    """Detect archive type by its extension.

    :param file_to_expand: archive file
    :return: type of archive if supported, or None

    """
    if any(file_to_expand.endswith(ext) for ext in Constants.ARCH_TYPES):
        return next(Constants.ARCH_TYPES[ext] for ext in Constants.ARCH_TYPES if file_to_expand.endswith(ext))
    else:
        logger.error("File with such extension isn`t supported -- {}".format(file_to_expand))
        return None


def _get_absolute_path(file_name, expanded_dir):
    """Return absolute path.

    :param file_name: archive file
    :param expanded_dir: directory in which archive is unpacking
    :return: absolute path to unpacked file.

    """
    file_name = os.path.basename(file_name)
    return os.path.join(expanded_dir, file_name)


def _is_file_exists(file_name):
    """Check if file exists and readable.

    :param file_name: the path to the file to be expanded
    :return: boolean true, if the file exists and is readable

    """
    return os.path.isfile(file_name) and os.access(file_name, os.R_OK)


def _is_tar_file(archive):
    """Check for valid tarball.

    :param archive: the path to the file to be expanded
    :return: boolean true, if the file is a valid tarball

    """
    return tarfile.is_tarfile(archive)


def _is_zip_file(archive):
    """Check for valid zip file.

    :param archive: the path to the file to be expanded
    :return: boolean true, if the file is a valid zip file

    """
    return zipfile.is_zipfile(archive)


def _get_tar_instance(archive):
    """Return TarFile object.

    :param archive: archive to be processed
    :return: TarFile object.

    """
    return tarfile.open(archive, 'r:gz')


def _get_zip_instance(archive):
    """Return ZipFile object.

    :param archive: archive to be processed
    :return: ZipFile object.

    """
    return zipfile.ZipFile(archive)


def _get_archive_handler(archive_type):
    """Chose handler for archive by its type.

    :param archive_type: the path to the file to be expanded
    :return: handler function

    """
    handlers = {
        Constants.ZIP_TYPE: _get_zip_instance,
        Constants.TARBALL_TYPE: _get_tar_instance,
    }
    return handlers.get(archive_type)


def _verify_archive_contents(members_names):
    """Verifies the archive contents for presence of exactly two files [one csv and one JSON file]

    :param members_names: list of contents returned by tar or zip module
    :return: false if verification fails

    """
    valid = (len(members_names) == 2)
    for name in members_names:
        if not any(name.endswith(ext) for ext in Constants.SUPPORTED_FILE_EXTENSIONS):
            return False
    return valid


def _put_files_to_root_dir(root_dir):
    """Move files from subdirectories to root directory.

    :param root_dir: root directory

    """
    for root, subdirs, files in os.walk(root_dir):
        if subdirs:
            for subdir in subdirs:
                _put_files_to_root_dir(subdir)
        for file_entry in files:
            file_path = os.path.join(root, file_entry)
            shutil.move(file_path, os.path.join(root_dir, file_entry))


def _clear_root_dir(root_dir):
    """Clear directory from subdirectories.

    :param root_dir: root directory

    """
    for entry in os.listdir(root_dir):
        entry_path = os.path.join(root_dir, entry)
        if os.path.isdir(entry_path):
            shutil.rmtree(entry_path)


def _move_files_to_expanded_dir(expanded_dir):
    """Move files from subdirectories to root directoryi and remove subdirectories.

    :param expanded_dir: root directory

    """
    _put_files_to_root_dir(expanded_dir)
    _clear_root_dir(expanded_dir)


def _extract_archive(archive, archive_type, expanded_dir):
    """Extract archive and return list of paths to extracted files.

    :param archive: archive to be unpacked
    :param archive_type: zip or tarball
    :param expanded_dir: directory to store extracted files
    :return: archive_contents: list of absolute paths to extracted files
    :raises: Exception if there is no handler for archive type

    """
    handler = _get_archive_handler(archive_type)
    if handler is None:
        raise Exception('No handler available for archive', archive)
    archive = handler(archive)
    archive.extractall(path=expanded_dir)
    archive.close()
    _move_files_to_expanded_dir(expanded_dir)
    archive_contents = [_get_absolute_path(file_entry, expanded_dir) for file_entry in os.listdir(expanded_dir)]
    if not _verify_archive_contents(archive_contents):
        raise Exception('Expected 2 files not found in the archive', archive_contents)
    return archive_contents


def expand_file(file_to_expand, expanded_dir):
    """Extract file contents to the destination directory

    :param file_to_expand: the path to the file to be expanded
    :param expanded_dir: the destination directory
    :return: archive_contents: the tar or zip file contents as list [path to csv and json files]
    :raises: Exception if source file is invalid

    """
    archive_type = _get_archive_type_by_extension(file_to_expand)
    if archive_type is None and not _is_valid_archive(file_to_expand, archive_type):
        raise Exception('Invalid source file -- {}'.format(file_to_expand))

    archive_contents = _extract_archive(file_to_expand, archive_type, expanded_dir)
    return archive_contents
