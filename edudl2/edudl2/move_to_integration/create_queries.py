import re
from edudl2.udl2 import message_keys as mk
from sqlalchemy.sql.expression import text, bindparam, select, and_
from edudl2.database.udl2_connector import get_udl_connection
from psycopg2.extensions import QuotedString
from edudl2.udl2_util.database_util import create_filtered_sql_string
import edschema.metadata.util as edschema_util


class InsertQueryBuilder:

    def __init__(self, conf, source_table, target_table, column_mapping, column_types):
        self.dblink_url = dblink_url_composer(host=conf[mk.SOURCE_DB_HOST],
                                              port=conf[mk.SOURCE_DB_PORT],
                                              db_name=conf[mk.SOURCE_DB_NAME],
                                              db_user=conf[mk.SOURCE_DB_USER],
                                              db_password=conf[mk.SOURCE_DB_PASSWORD])
        self.source_table = source_table
        self.target_table = target_table
        self.column_types = column_types
        self.target_columns = ",".join(list(column_mapping.keys()))
        lst = list(column_mapping.values())
        self.guid_field = lst[0]
        self.quoted_source_columns = ",".join(value.replace("'", "''") for value in lst[1:])
        self.group_by_columns = ",".join(value for value in lst[1:] if "'" not in value)
        self.record_mapping = ",".join(list(column_types.values()))
        self.params = [bindparam('batch_guid', conf[mk.GUID_BATCH])]
        self.target_schema_and_table = combine_schema_and_table(conf[mk.TARGET_DB_SCHEMA],
                                                                target_table)
        self.source_schema_and_table = combine_schema_and_table(conf[mk.SOURCE_DB_SCHEMA], source_table)

    @property
    def op(self):
        return self._op

    @op.setter
    def op(self, op):
        self._op = op
        if op:
            self.params += [bindparam('op', op)]

    @property
    def distinct(self):
        return self._distinct

    @distinct.setter
    def distinct(self, distinct):
        self._distinct = distinct
        if distinct:
            self.distinct_expression = 'max(%s)' % self.guid_field
        else:
            self.distinct_expression = self.guid_field

    def build(self):
        from_query = "SELECT {distinct_expression}, {quoted_source_columns} " + \
                     "FROM {source_schema_and_table} " + \
                     "WHERE batch_guid=':batch_guid' "
        if self._op:
            from_query += " AND op = ':op' "
        if self._distinct:
            from_query += " GROUP BY {group_by_columns}"
        from_query = from_query.format(distinct_expression=self.distinct_expression,
                                       quoted_source_columns=self.quoted_source_columns,
                                       source_schema_and_table=self.source_schema_and_table,
                                       group_by_columns=self.group_by_columns)

        query = "INSERT INTO {target_schema_and_table} ({target_columns}) " + \
                "SELECT * FROM " + \
                "dblink({dblink_url}, '{from_query}') AS t({record_mapping});"
        query = query.format(target_schema_and_table=self.target_schema_and_table,
                             dblink_url=self.dblink_url,
                             from_query=from_query,
                             record_mapping=self.record_mapping,
                             target_columns=self.target_columns)
        return text(query, bindparams=self.params)


def combine_schema_and_table(schema_name, table_name):
    '''
    Function to create the expression of "schema_name"."table_name"
    '''
    if schema_name:
        return create_filtered_sql_string('"{schema}"."{table}"', schema=schema_name, table=table_name)
    else:
        return create_filtered_sql_string('"{table}"', table=table_name)


def get_column_mapping(ref_table, target_table, source_table=None):
    '''
    Get column mapping to target table.

    @param schema_name: DB schema name
    @param ref_table: DB reference mapping table name
    @target_table: Table into which to insert data
    @param source_table: (optional) Only include columns from this table

    @return Mapping query
    '''
    with get_udl_connection() as conn:
        table = conn.get_table(ref_table)
        query = select([table.c.target_column,
                        table.c.source_column],
                       from_obj=table)
        query = query.where(table.c.target_table == target_table)
        if source_table:
            query = query.where(and_(table.c.source_table == source_table))
        return conn.get_result(query)


def dblink_url_composer(host, port, db_name, db_user, db_password):
    return QuotedString('host={host} port={port} dbname={db_name} user={db_user} password={db_password}'
                        .format(host=host, port=port, db_name=db_name, db_user=db_user, db_password=db_password))


def create_insert_query(conf, source_table, target_table, column_mapping, column_types, need_distinct, op=None):
    '''
    Main function to create query to insert data from source table to target table
    The query will be executed on the database where target table exists
    Since the source tables and target tables can be existing on different databases/servers,
    dblink is used here to get data from source database in the select clause
    '''
    # TODO:if guid_batch is changed to uuid, need to add quotes around it
    builder = InsertQueryBuilder(conf, source_table, target_table, column_mapping, column_types)
    builder.op = op
    builder.distinct = need_distinct
    return builder.build()
