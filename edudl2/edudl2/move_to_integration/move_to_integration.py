from collections import OrderedDict
import logging
from edschema.metadata.int_metadata import generate_int_metadata
from edudl2.move_to_integration.move_to_integration_setup import get_table_column_types, get_table_mapping
from edudl2.move_to_integration.create_queries import get_column_mapping, create_insert_query
from edudl2.udl2 import message_keys as mk
from edudl2.database.udl2_connector import get_target_connection
from edcore.database.utils.utils import create_schema

logger = logging.getLogger(__name__)


def move_data_from_staging_to_integration(conf):
    '''
    map sql data type in configuration file into what SQLAlchemy type is.
    move from staging tables on udl2 db to the appropritae schema (determined by batch_guid) on edware db
    @param task_name: name of the tas
           conf: configration dictionary, the format is defined in W_load_to_integration_table.py
           note: udl2_conf is the udl2_conf dictionary that stores all configuration settings
    '''

    tenant_name = conf[mk.TENANT_NAME]
    target_db_schema = conf[mk.TARGET_DB_SCHEMA]
    ref_table = conf[mk.REF_TABLE]

    with get_target_connection(tenant_name) as integration_conn:
        create_schema(integration_conn, generate_int_metadata, target_db_schema)

    with get_target_connection(tenant_name, target_db_schema) as conn:

        table_mappings = get_table_mapping(ref_table, conf[mk.PHASE])
        affected_rows = 0
        for target_table in table_mappings:

            source_table = table_mappings[target_table]

            column_mappings = get_column_mapping_to_int(ref_table, target_table)
            column_types = get_table_column_types(conf, target_table, column_mappings.keys())

            query = create_insert_query(conf, source_table, target_table, column_mappings, column_types, False)
            result = conn.execute(query)
            affected_rows = affected_rows + result.rowcount
    return affected_rows


def get_column_mapping_to_int(ref_table_name, target_table_name):
    results = get_column_mapping(ref_table_name, target_table_name)
    column_mapping_list = []
    for mapping in results:
        target_column = mapping['target_column']
        source_column = mapping['source_column']
        target_source_pair = (target_column, source_column)
        # this is the primary key, need to put the pair in front
        if source_column is not None and 'nextval' in source_column:
            column_mapping_list.insert(0, target_source_pair)
        else:
            column_mapping_list.append(target_source_pair)

    return OrderedDict(column_mapping_list)
