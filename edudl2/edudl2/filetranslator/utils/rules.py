import re

EMPTY_STRING = ''

RENAMING_MAP = {
    'OptionalStateData1': 'StateField1',
    'OptionalStateData2': 'StateField2',
    'OptionalStateData3': 'StateField3',
    'OptionalStateData4': 'StateField4',
    'OptionalStateData5': 'StateField5',
    'OptionalStateData6': 'StateField6',
    'OptionalStateData7': 'StateField7',
    'OptionalStateData8': 'StateField8',
    'PBATestingDistrictIdentifier': 'PBATestingDistrictCode',
    'EOYTestingDistrictIdentifier': 'EOYTestingDistrictCode',
    'ResponsibleDistrictIdentifier': 'ResponsibleDistrictCode',
    'AssessmentAccommodationEnglishLearner': 'ELAccommodation',
    'ResponsibleSchoolInstitutionIdentifier': 'ResponsibleSchoolCode',
    'PBATestingSchoolInstitutionIdentifier': 'PBATestingSchoolInstitutionCode',
    'EOYTestingSchoolInstitutionIdentifier': 'EOYTestingSchoolInstitutionCode',
    'TranslationoftheMathematicsAssessmentOnline': 'TranslationoftheMathematicsAssessment',
    'ScreenReaderORotherAssistiveTechnologyATApplication': 'AssistiveTechnologyScreenReader'}

DEPRECATED_COLUMNS = ('TactileGraphics',
                      'TexttoSpeechforELAL',
                      'TexttoSpeechforMathematics',
                      'AssessmentAccommodation504',
                      'HumanReaderorHumanSignerforELAL',
                      'HumanReaderorHumanSignerforMathematics',
                      'TranslationoftheMathematicsAssessmentinTexttoSpeech',
                      'AssessmentAccommodationIndividualizedEducationalPlanIEP')

#############################################################################
#                          TRANSFORMATION RULES                             #
#############################################################################


def transform_admin_direct_in_student_language(value: str)-> str:
    """
    Check if value match regular expression, if yes apply changes otherwise return unmodified value
    regex: "^(OralScriptReadbyTestAdministrator([A-Z]{3}))|(HumanTranslator)$"

    group1: match words that started with "OralScriptReadbyTestAdministrator" and
            have 3 more letters in capital cases
    group2: match word "HumanTranslator"

    :param value: value to be processed
    :return: updated value
    """
    group1 = 'OralScriptReadbyTestAdministrator([A-Z]{3})'
    group2 = 'HumanTranslator'
    mask = re.compile('^({gr1})|({gr2})$'.format(gr1=group1, gr2=group2))

    def rules(matchobj):
        match = matchobj.group(0)
        if match == group2:
            return 'HT'
        else:
            return match[-3:]

    value = re.sub(mask, rules, value)
    if len(value) > 3:
        value = value[:3]
    return value


def transform_grade_level_when_assessed(value: str)-> str:
    """  Check if value is in rules or keep_it, if yes - apply changes, otherwise return '' """
    rules = {
        'Other': '99',
        'OutOfSchool': 'OS',
    }
    keep_it = ('02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13')

    if value in rules:
        return rules[value]
    elif value in keep_it:
        return value
    else:
        # This field can't be empty, so for other grades use 'Other'
        return rules['Other']


def transform_color_contrast(value: str)-> str:
    """ Check if value is in rules, if yes - apply changes, otherwise return '' """
    rules = {
        'black-cream': '01',
        'black-lblue': '02',
        'black-lmagenta': '03',
        'white-black': '04',
        'lblue-dblue': '05',
        'dgray-pgreen': '06',
        'ColorOverlay': '07'
    }
    return rules[value] if value in rules else EMPTY_STRING


def transform_response(value: str)-> str:
    """ Check if value is in rules, if yes - apply changes, otherwise return '' """
    rules = {
        'SpeechToText': '01',
        'HumanScribe': '02',
        'HumanSigner': '03',
        'ExternalATDevice': '04',
    }
    return rules[value] if value in rules else EMPTY_STRING


def transform_braille_response(value: str)-> str:
    """ Check if value is in rules, if yes - apply changes, otherwise return '' """
    rules = {
        'BrailleWriter': '01',
        'BrailleNotetaker': '02'
    }
    return rules[value] if value in rules else EMPTY_STRING


def transform_human_reader_or_writer(value: str)-> str:
    """ Check if value is in rules, if yes - apply changes, otherwise return '' """
    rules = {
        'HumanSigner': '01',
        'HumanReadAloud': '02'
    }
    return rules[value] if value in rules else EMPTY_STRING


def transform_math_response_el(value: str)-> str:
    """ Value should have length <= 2 """
    return value[:2]


def transform_student_with_disabilities(value: str)-> str:
    """ Check if value is 'Y' or 'N', if yes return value, otherwise return '' """
    return value if value in ('Y', 'N') else EMPTY_STRING


def transform_braille_tactile_graphics(value: str)-> str:
    """ Check if value is 'Y', if yes return value, otherwise return '' """
    return value if value == 'Y' else EMPTY_STRING


# def transform_disability_type(value: str)-> str:
#     """ There is no transformation rules so just return value """
#     return value

TRANSFORMATION_MAP = {
    'MathematicsResponse': transform_response,
    'ELALConstructedResponse': transform_response,
    'ELALSelectedResponseorTechnologyEnhancedItems': transform_response,
    'ColorContrast': transform_color_contrast,
    'BrailleResponse': transform_braille_response,
    # 'PrimaryDisabilityType': transform_disability_type,
    'HumanReaderorHumanSigner': transform_human_reader_or_writer,
    'GradeLevelWhenAssessed': transform_grade_level_when_assessed,
    'StudentWithDisabilities': transform_student_with_disabilities,
    'BraillewithTactileGraphics': transform_braille_tactile_graphics,
    'AdministrationDirectionsReadAloudinStudentsNativeLanguage': transform_admin_direct_in_student_language,
    'MathematicsResponseEL': transform_math_response_el
}
