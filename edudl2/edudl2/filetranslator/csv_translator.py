from __future__ import absolute_import
import os
import csv
import shutil
from celery.utils.log import get_task_logger
from tempfile import NamedTemporaryFile

from edudl2.udl2.constants import Constants
from edudl2.data_validator.data_validator_util import get_source_column_values_from_ref_column_mapping
from edudl2.filetranslator.utils import rules as translation_rules
from edudl2.udl2_util.exceptions import UDL2Exception


logger = get_task_logger(__name__)


allowed_translation_load_types = Constants.LOAD_TYPES_ALLOWED_SCHEMA_TRANSLATION()


class FileTranslator():

    def __init__(self, dir_path, load_type, batch_sid):
        self.dir_path = dir_path
        self.load_type = load_type
        self.batch_sid = batch_sid
        self.expected_csv_fields = get_source_column_values_from_ref_column_mapping(Constants.UDL2_CSV_LZ_TABLE,
                                                                                    load_type)
        if not isinstance(self.expected_csv_fields, dict):
            self.expected_csv_fields = {load_type: self.expected_csv_fields}

    def get_schema_version(self, filename):
        with open(filename) as cfile:
            c_reader = csv.reader(cfile)
            headers = next(c_reader)
            headers = [header.lower() for header in headers]

        for schema_version, fields in self.expected_csv_fields.items():
            lowered_fields = [field.lower() for field in fields]
            if self._validate_schema_format(lowered_fields, headers):
                return schema_version
        else:
            logger.error('Schema with fields not exists: [batch: {} headers: {}]'.format(self.batch_sid,
                                                                                         headers))
            raise UDL2Exception('Unsupported schema format. Translation task aborted')

    def translate_file(self, filename: str, schema_version: str):
        """
        Translate csv file from old schema to new schema.
        Create temporary file in the same directory where located file in old format
         then apply all transformation rules and write it to the tmp file,
         after transformation replace old file by new one. Then delete tmp file.
        param filename: str, path to the file that will be translated.
        """
        temporary_filename = None
        headers = self.expected_csv_fields[schema_version]
        try:
            with NamedTemporaryFile(mode='w', delete=False,
                                    dir=self.dir_path, suffix='_tmp_transform') as tempfile:
                temporary_filename = tempfile.name
                c_writer = csv.DictWriter(tempfile, headers)
                c_writer.writeheader()
                for row in self._translate_csv(filename, headers):
                    c_writer.writerow(row)

            shutil.move(tempfile.name, filename)

            logger.info('[SUCESS] Finished transformation for file: {filename}'.format(filename=filename))
        except Exception as e:
            logger.info('[FAIL] Failed transformation for file: {filename}'.format(filename=filename))
            raise e
        finally:
            # In case of error delete tmp file
            if os.path.exists(temporary_filename):
                os.remove(temporary_filename)

    def _translate_csv(self, csv_file: str, headers: list):
        """
        Apply all transformation rules to the csv file line by line
        :param csv_file:
        :return:
        """
        logger.info('Started transformation for file: {filename}'.format(filename=csv_file))
        with open(csv_file) as cfile:
            c_reader = csv.DictReader(cfile)
            for row in c_reader:
                self._delete_deprecated_columns(row)
                self._transform_row_values(row)
                self._rename_column_titles(row)
                self._insert_new_columns(row, headers)
                yield row

    def _delete_deprecated_columns(self, row: dict):
        """
        Delete deprecated columns from row
        :param row: row for transformation
        :return: We don't need to return anything cause 'del' mutates the original dict
        """
        for column_name in translation_rules.DEPRECATED_COLUMNS:
            del row[column_name]

    def _transform_row_values(self, row: dict):
        """
        Iterate over transformation rules and apply each of them to the row
        :param row: data for tranformation
        :return: We don't need to return anything cause changes mutates the original dict
        """
        for column_name, rule in translation_rules.TRANSFORMATION_MAP.items():
            row[column_name] = rule(row[column_name])

    def _rename_column_titles(self, row: dict):
        """
        Rename column titles according to new schema
        :param row: row for transformation
        :return: We don't need to return anything cause changes mutates the original dict
        """
        for old_title, new_title in translation_rules.RENAMING_MAP.items():
            row[new_title] = row.pop(old_title)

    def _insert_new_columns(self, row: dict, headers: list):
        """
        Insert new columns according to new schema
        :param row: row for transformation
        :return: We don't need to return anything cause changes mutates the original dict
        """

        row.update({col_name: translation_rules.EMPTY_STRING for col_name in headers
                    if col_name not in row})

    def allowed_translation_load_type(self) -> bool:
        """
        Check if type of the file is valid and supported, only summative files supported
        :return: bool
        """
        return True if self.load_type in allowed_translation_load_types else False

    def file_required_translation(self, file_schema)-> bool:
        """
        Check file schema and decide do we need apply translation rules to this file or not
        :param file_schema: schema for check
        :return: message if schema format is valid (new) or not
        """

        if file_schema in Constants.TRANSLATION_SUMMATIVE_LOAD_TYPES_ROADMAP.keys():
            return True
        elif file_schema in Constants.VALID_SUMMATIVE_LOAD_TYPES:
            return False
        else:
            raise UDL2Exception('Unsupported schema format. Translation task aborted')

    def _validate_schema_format(self, headers: list, validation_schema: tuple) -> bool:
        """
        Check if file for processing is in the last supported format
        :param headers: headers of the file
        :param validation_schema: schema for validation
        :return: bool
        """
        # TODO: This check is weak in case we'll have duplicated csv headers, rewrite it
        return True if set(headers) == set(validation_schema) else False
