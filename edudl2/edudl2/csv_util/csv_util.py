from edudl2.udl2_util import file_util
import os
import csv
import logging

__author__ = 'npandey'

logger = logging.getLogger(__name__)


def get_value_from_csv(file_dir, header_name, row_number=1):
    csv_file_name = file_util.get_file_name_in_dir(file_dir, 'csv')
    csv_file_path = os.path.join(file_dir, csv_file_name)

    with open(csv_file_path) as csv_file:
        csv_content = csv.reader(csv_file)

        row_count = 0
        header_field_index = None
        field_value = None

        for row in csv_content:
            if row_count == 0:
                header = row
                try:
                    header_field_index = header.index(header_name)
                except ValueError:
                    logger.info('Field not found %s in the csv' % header_name)
            elif row_count == row_number:
                if header_field_index:
                    field_value = row[header_field_index]
                break
            row_count += 1

    return field_value
