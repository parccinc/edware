FALL_2015 = 'fall-2015'
OLD_FORMAT = 'old format'
UNSUPPORTED_OR_BROKEN_FORMAT = 'unsupported_or_broken'
FILE_SUFFIX = '_fall_2015'
EMPTY_STRING = ''

RENAMING_MAP = {
    'OptionalStateData1': 'StateField1',
    'OptionalStateData2': 'StateField2',
    'OptionalStateData3': 'StateField3',
    'OptionalStateData4': 'StateField4',
    'OptionalStateData5': 'StateField5',
    'OptionalStateData6': 'StateField6',
    'OptionalStateData7': 'StateField7',
    'OptionalStateData8': 'StateField8',
    'PBATestingDistrictIdentifier': 'PBATestingDistrictCode',
    'EOYTestingDistrictIdentifier': 'EOYTestingDistrictCode',
    'ResponsibleDistrictIdentifier': 'ResponsibleDistrictCode',
    'AssessmentAccommodationEnglishLearner': 'ELAccommodation',
    'ResponsibleSchoolInstitutionIdentifier': 'ResponsibleSchoolCode',
    'PBATestingSchoolInstitutionIdentifier': 'PBATestingSchoolInstitutionCode',
    'EOYTestingSchoolInstitutionIdentifier': 'EOYTestingSchoolInstitutionCode',
    'TranslationoftheMathematicsAssessmentOnline': 'TranslationoftheMathematicsAssessment',
    'ScreenReaderORotherAssistiveTechnologyATApplication': 'AssistiveTechnologyScreenReader'}

DEPRECATED_COLUMNS_CSV = ('TactileGraphics',
                          'TexttoSpeechforELAL',
                          'TexttoSpeechforMathematics',
                          'AssessmentAccommodation504',
                          'HumanReaderorHumanSignerforELAL',
                          'HumanReaderorHumanSignerforMathematics',
                          'TranslationoftheMathematicsAssessmentinTexttoSpeech',
                          'AssessmentAccommodationIndividualizedEducationalPlanIEP')

DEPRECATED_COLUMNS_DB = ('accomod_tactile_graph',
                         'accomod_text_2_speech_ela',
                         'accomod_text_2_speech_math',
                         'accomod_504',
                         'accomod_read_ela',
                         'accomod_read_math',
                         'accomod_math_text_2_speech',
                         'accomod_ind_ed')
