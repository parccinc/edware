import argparse
import csv
import logging
import shutil
import sys
import os
import re
from tempfile import NamedTemporaryFile

from edudl2.udl2_util.csv_translator.helpers import constants
from edudl2.udl2_util.csv_translator.helpers.schemas import SUMMATIVE_SCHEMA_OLD_FORMAT, \
    SUMMATIVE_SCHEMA_NEW_FORMAT


logger = logging.getLogger(__name__)


def args_parser() -> argparse.Namespace:
    parser = argparse.ArgumentParser('csv_translator')
    parser.description = 'Convert summative files from old format to "fall-2015"'
    parser.add_argument('-c', '--csv-file', dest='csv_file', nargs='?', required=False,
                        help='name of csv file')
    parser.add_argument('-l', '--csv-list-in-file', dest='csv_list', required=False,
                        type=argparse.FileType('r'),
                        help='file with a list of csv files for translation')
    parser.add_argument('-n', '--create-new', dest='create_new', action='store_true', required=False,
                        help='If present, don\'t replace original file, '
                             'create new with suffix "_fall_2015" in the same folder')

    if len(sys.argv[1:]) == 0:
        logger.info('You schould specify at least one argument!')
        parser.print_help()
        sys.exit(1)

    return parser.parse_args()


def main(args):
    """ Run translation script for each file """
    files_for_translation = set()
    if args.csv_file and os.path.isfile(args.csv_file):
        files_for_translation.add(os.path.abspath(args.csv_file))

    if args.csv_list:
        files_for_translation.update(
            {os.path.abspath(fname.strip()) for fname in args.csv_list if os.path.isfile(fname.strip())})

    for fname in sorted(files_for_translation):
        logger.info('Process file: {}'.format(fname))
        run_translation(fname, args.create_new)


def run_translation(csv_file: str, create_new: bool):
    """
    Main translation function. First we check file format,
    if file in format 'fall-2015', skip transformation.
    Otherwise apply transformation rules. For this we create temporary file to store new data
    and after transformation replace original file by new.
    :param csv_file: file for transformation
    :param create_new: if True, don't overwrite original file after transformation
                       and create new file with suffix "_fall_2015" in the folder with original file
    """
    csv_version = get_file_version(csv_file)
    if csv_version == constants.FALL_2015:
        logger.info('File is already in format "fall-2015"')
        return
    elif csv_version == constants.UNSUPPORTED_OR_BROKEN_FORMAT:
        logger.info('Validation failed for file: {}'.format(csv_file))
        return
    temporary_filename = None
    try:
        with NamedTemporaryFile(mode='w', delete=False, suffix='_tmp_transform') as tempfile:
            temporary_filename = tempfile.name
            c_writer = csv.DictWriter(tempfile, SUMMATIVE_SCHEMA_NEW_FORMAT)
            c_writer.writeheader()
            for row in _translate_csv(csv_file):
                c_writer.writerow(row)

        write_file(tempfile.name, csv_file, create_new)

        logger.info('[SUCESS] Finished transformation for file: {filename}'.format(filename=csv_file))
        print('[SUCESS] Finished transformation for file: {filename}'.format(filename=csv_file))
    except Exception as e:
        logger.info('[FAIL] Failed transformation for file: {filename}'.format(filename=csv_file))
        raise e
    finally:
        # In case of error delete tmp file
        if os.path.exists(temporary_filename):
            os.remove(temporary_filename)


def get_file_version(filename: str)-> str:
    """
    read line with headers from file and check version
    :param filename: file for check
    :return: 'fall-2015' in case of new format otherwise 'old format'
    """
    with open(filename) as cfile:
        c_reader = csv.reader(cfile)
        headers = next(c_reader)
        if _validate_schema_format(headers, SUMMATIVE_SCHEMA_NEW_FORMAT):
            version = constants.FALL_2015
        elif _validate_schema_format(headers, SUMMATIVE_SCHEMA_OLD_FORMAT):
            version = constants.OLD_FORMAT
        else:
            version = constants.UNSUPPORTED_OR_BROKEN_FORMAT
        return version


def _validate_schema_format(headers: list, validation_schema: tuple) -> bool:
    """
    Check if file for processing is valid
    :param headers: headers of the file
    :param validation_schema: schema for validation
    :return: bool
    """
    return True if tuple(headers) == validation_schema else False


def write_file(source: str, dest: str, create_new: bool):
    if create_new:
        path, extension = os.path.splitext(dest)
        dest = ''.join([path, constants.FILE_SUFFIX, extension])
    shutil.move(source, dest)


def _translate_csv(csv_file: str):
    """
    Translate data from old csv format to fal-2015, steps:
    1. Delete deprecated columns
    2. Transform data in to new format
    3. Rename column titles according to new format
    4. Insert new columns according to new format, cells are empty
    :param csv_file: csv file in old format
    :return: yield file row by row
    """
    logger.info('Started transformation for file: {filename}'.format(filename=csv_file))
    with open(csv_file) as cfile:
        c_reader = csv.DictReader(cfile)
        for row in c_reader:
            _delete_deprecated_columns(row)
            _transform_row_values(row)
            _rename_column_titles(row)
            _insert_new_columns(row)
            yield row


def _delete_deprecated_columns(row: dict):
    """
    Delete deprecated columns from row
    :param row: row for transformation
    :return: We don't need to return anything cause 'del' mutates the original dict
    """
    for column_name in constants.DEPRECATED_COLUMNS_CSV:
        del row[column_name]


def _transform_row_values(row: dict):
    """
    Iterate over transformation rules and apply each of them to the row
    :param row: data for tranformation
    :return: We don't need to return anything cause changes mutates the original dict
    """
    for column_name, rule in TRANSFORMATION_MAP.items():
        row[column_name] = rule(row[column_name])


def _rename_column_titles(row: dict):
    """
    Rename column titles according to new schema
    :param row: row for transformation
    :return: We don't need to return anything cause changes mutates the original dict
    """
    for old_title, new_title in constants.RENAMING_MAP.items():
        row[new_title] = row.pop(old_title)


def _insert_new_columns(row: dict):
    """
    Insert new columns according to new schema
    :param row: row for transformation
    :return: We don't need to return anything cause changes mutates the original dict
    """
    row.update({col_name: constants.EMPTY_STRING for col_name in SUMMATIVE_SCHEMA_NEW_FORMAT
                if col_name not in row})


#############################################################################
#                          TRANSFORMATION RULES                             #
#############################################################################

def transform_admin_direct_in_student_language(value: str)-> str:
    """
    Check if value match regular expression, if yes apply changes otherwise return unmodified value
    regex: "^(OralScriptReadbyTestAdministrator([A-Z]{3}))|(HumanTranslator)$"

    group1: match words that started with "OralScriptReadbyTestAdministrator" and
            have 3 more letters in capital cases
    group2: match word "HumanTranslator"

    :param value: value to be processed
    :return: updated value
    """
    group1 = 'OralScriptReadbyTestAdministrator([A-Z]{3})'
    group2 = 'HumanTranslator'
    mask = re.compile('^({gr1})|({gr2})$'.format(gr1=group1, gr2=group2))

    def rules(matchobj):
        match = matchobj.group(0)
        if match == group2:
            return 'HT'
        else:
            return match[-3:]

    value = re.sub(mask, rules, value)
    if len(value) > 3:
        value = value[:3]
    return value


def transform_grade_level_when_assessed(value: str)-> str:
    """  Check if value is in rules or keep_it, if yes - apply changes, otherwise return '' """
    rules = {
        'Other': '99',
        'OutOfSchool': 'OS',
    }
    keep_it = ('02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13')

    if value in rules:
        return rules[value]
    elif value in keep_it:
        return value
    else:
        # This field can't be empty, so for other grades use 'Other'
        return rules['Other']


def transform_color_contrast(value: str)-> str:
    """ Check if value is in rules, if yes - apply changes, otherwise return '' """
    rules = {
        'black-cream': '01',
        'black-lblue': '02',
        'black-lmagenta': '03',
        'white-black': '04',
        'lblue-dblue': '05',
        'dgray-pgreen': '06',
        'ColorOverlay': '07'
    }
    return rules[value] if value in rules else constants.EMPTY_STRING


def transform_response(value: str)-> str:
    """ Check if value is in rules, if yes - apply changes, otherwise return '' """
    rules = {
        'SpeechToText': '01',
        'HumanScribe': '02',
        'HumanSigner': '03',
        'ExternalATDevice': '04',
    }
    return rules[value] if value in rules else constants.EMPTY_STRING


def transform_braille_response(value: str)-> str:
    """ Check if value is in rules, if yes - apply changes, otherwise return '' """
    rules = {
        'BrailleWriter': '01',
        'BrailleNotetaker': '02'
    }
    return rules[value] if value in rules else constants.EMPTY_STRING


def transform_human_reader_or_writer(value: str)-> str:
    """ Check if value is in rules, if yes - apply changes, otherwise return '' """
    rules = {
        'HumanSigner': '01',
        'HumanReadAloud': '02'
    }
    return rules[value] if value in rules else constants.EMPTY_STRING


def transform_math_response_el(value: str)-> str:
    """ Value should have length <= 2 """
    return value[:2]


def transform_student_with_disabilities(value: str)-> str:
    """ Check if value is 'Y' or 'N', if yes return value, otherwise return '' """
    return value if value in ('Y', 'N') else constants.EMPTY_STRING


def transform_braille_tactile_graphics(value: str)-> str:
    """ Check if value is 'Y', if yes return value, otherwise return '' """
    return value if value == 'Y' else constants.EMPTY_STRING


# def transform_disability_type(value: str)-> str:
#     """ There is no transformation rules so just return value """
#     return value


TRANSFORMATION_MAP = {
    'MathematicsResponse': transform_response,
    'ELALConstructedResponse': transform_response,
    'ELALSelectedResponseorTechnologyEnhancedItems': transform_response,
    'ColorContrast': transform_color_contrast,
    'BrailleResponse': transform_braille_response,
    # 'PrimaryDisabilityType': transform_disability_type,
    'HumanReaderorHumanSigner': transform_human_reader_or_writer,
    'GradeLevelWhenAssessed': transform_grade_level_when_assessed,
    'StudentWithDisabilities': transform_student_with_disabilities,
    'BraillewithTactileGraphics': transform_braille_tactile_graphics,
    'AdministrationDirectionsReadAloudinStudentsNativeLanguage': transform_admin_direct_in_student_language,
    'MathematicsResponseEL': transform_math_response_el}


if __name__ == "__main__":
    main(args_parser())
