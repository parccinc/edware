import logging
from edudl2.json_util.json_util import get_value_from_json
from edudl2.csv_util.csv_util import get_value_from_csv
from edudl2.udl2.constants import Constants
from edudl2.udl2 import message_keys as mk

__author__ = 'tshewchuk'

logger = logging.getLogger(__name__)
load_types = Constants.LOAD_TYPES()
load_subjects = Constants.LOAD_SUBJECTS()
subject_dependant_load_type = Constants.SUBJECT_DEPENDANT_LOAD_TYPES()

periods = Constants.PERIODS()


def get_values_from_json(file_dir: str) -> dict:
    """
    Get the load type for this UDL job from the json file and subject from csv file
    Also get the value for period  of this file
    @param file_dir: A directory that houses the json file
    @return: UDL job load type
    @rtype: string
    """
    values = {}
    values[mk.LOAD_TYPE] = _get_load_type(file_dir)
    values[mk.PERIOD] = _get_period(file_dir)
    return values


def _get_load_type(file_dir: str) -> str:
    load_type = get_value_from_json(file_dir, Constants.LOAD_TYPE_KEY)
    if load_type:
        load_type = load_type.lower()
    else:
        load_type = get_value_from_json(file_dir, Constants.LOAD_TYPE_KEY_SUMM).lower()

    if load_type not in load_types:
        raise ValueError('No valid load type specified in json file --')

    if load_type in subject_dependant_load_type:
        csv_subject = get_value_from_csv(file_dir, Constants.SUBJECT_KEY)
        load_subject = get_load_subject(csv_subject)

        load_type = load_type + '.' + load_subject

    return load_type


def _get_period(file_dir: str) -> str:
    """Get value of period from json file
    :param file_dir: json file
    :return: string - value of period
    """
    # period = get_value_from_json(file_dir, Constants.PERIOD_KEY)
    #
    # if period is None:
    #     period = get_value_from_json(file_dir, Constants.PERIOD_KEY_SUMM)
    # if period:
    #     period = period.lower()
    # First try to get period by PERIOD_KEY, if this key not exist in json file then search period by
    # PERIOD_KEY_SUMM.
    period = None
    keys = [Constants.PERIOD_KEY, Constants.PERIOD_KEY_SUMM]
    for key in keys:
        period = get_value_from_json(file_dir, key)
        if period:
            period = period.lower()
            break

    if period and period not in periods:
        raise ValueError('No valid periods specified in json file --')

    return period


def get_load_subject(csv_subject):
    if csv_subject:
        if Constants.ASMT_SUBJECT_ELA in csv_subject.lower():
            return Constants.INTERNAL_SUBJECT_ELA
        elif csv_subject.lower() in Constants.MATH_SUBJECTS:
            return Constants.INTERNAL_SUBJECT_MATH
    raise ValueError("No valid load subject specified in csv file -- {subject}".format(subject=csv_subject))
