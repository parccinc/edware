'''
Several methods are provided to create/drop tables/sequences/extensions/foreign data wrapper servers
in postgres database.

Main Method: setup_udl2_schema(conf)
Main method: teardown_udl2_schema(conf)

Created on May 10, 2013

@author: ejen
'''
import argparse

from sqlalchemy.schema import CreateSequence, DropSequence, DropSchema
from sqlalchemy.sql.expression import text

from parcc_common.config.summative_ela_ref_table_data import ref_table_conf as ela_ref_table_conf
from parcc_common.config.summative_ela_ref_table_data_fall_2015 import ref_table_conf as ela_ref_table_conf_fall_2015
from parcc_common.config.summative_ela_ref_table_data_spring_2015 import ref_table_conf as ela_ref_table_conf_spring_2015
from parcc_common.config.summative_ela_ref_table_data_spring_2016 import ref_table_conf as ela_ref_table_conf_spring_2016

from parcc_common.config.summative_ela_item_ref_table_data import ref_table_conf as ela_item_ref_table_conf
from parcc_common.config.summative_math_ref_table_data import ref_table_conf as math_ref_table_conf
from parcc_common.config.summative_math_ref_table_data_fall_2015 import ref_table_conf as math_ref_table_conf_fall_2015
from parcc_common.config.summative_math_ref_table_data_spring_2015 import ref_table_conf as math_ref_table_conf_spring_2015
from parcc_common.config.summative_math_ref_table_data_spring_2016 import ref_table_conf as math_ref_table_conf_spring_2016

from parcc_common.config.summative_math_item_ref_table_data import ref_table_conf as math_item_ref_table_conf
from parcc_common.config.summative_json_ref_table_data import ref_table_conf as json_ref_table_conf
from parcc_common.config.summative_xml_ref_table_data import ref_table_conf as xml_ref_table_conf
from parcc_common.config.psychometric_ref_table_data import ref_table_conf as psychometric_ref_table_conf
from parcc_common.config.mya_ela_item_ref_table_data import ref_table_conf as mya_ela_item_ref_table_conf
from parcc_common.config.mya_math_item_ref_table_data import ref_table_conf as mya_math_item_ref_table_conf
from parcc_common.config.mya_ela_ref_table_data import ref_table_conf as mya_ela_ref_table_conf
from parcc_common.config.mya_math_ref_table_data import ref_table_conf as mya_math_ref_table_conf
from parcc_common.config.mya_json_ref_table_data import ref_table_conf as mya_json_ref_table_conf
from parcc_common.config.snl_ela_ref_table_data import ref_table_conf as snl_ela_ref_table_conf
from parcc_common.config.snl_json_ref_table_data import ref_table_conf as snl_json_ref_table_conf
from parcc_common.config.snl_student_task_ref_table_data import ref_table_conf as snl_student_task_ref_table_conf
from parcc_common.config.diagnostic_ela_reading_comp_item_ref_table_data import ref_table_conf as ela_reading_comp_item_ref_table_conf
from parcc_common.config.diagnostic_ela_vocab_item_ref_table_data import ref_table_conf as ela_vocab_item_ref_table_conf
from parcc_common.config.diagnostic_ela_decoding_item_ref_table_data import ref_table_conf as ela_decoding_item_ref_table_conf
from parcc_common.config.diagnostic_ela_vocab_ref_table_data import ref_table_conf as ela_vocab_ref_table_conf
from parcc_common.config.diagnostic_ela_comp_ref_table_data import ref_table_conf as ela_comp_ref_table_conf
from parcc_common.config.diagnostic_ela_decoding_ref_table_data import ref_table_conf as ela_decoding_ref_table_conf
from parcc_common.config.diagnostic_ela_writing_ref_table_data import ref_table_conf as ela_writing_ref_table_conf
from parcc_common.config.diagnostic_ela_read_flu_ref_table_data import ref_table_conf as ela_read_flu_ref_table_conf
from parcc_common.config.diagnostic_math_comp_item_ref_table_data import ref_table_conf as math_comp_item_ref_table_conf
from parcc_common.config.diagnostic_math_progress_item_ref_table_data import ref_table_conf as math_progress_item_ref_table_conf
from parcc_common.config.diagnostic_math_fluency_item_ref_table_data import ref_table_conf as math_fluency_item_ref_table_conf
from parcc_common.config.diagnostic_math_locator_ref_table_data import ref_table_conf as math_locator_ref_table_conf
from parcc_common.config.diagnostic_reader_motiv_ref_table_data import ref_table_conf as reader_motiv_ref_table_conf
from parcc_common.config.diagnostic_math_progress_ref_table_data import ref_table_conf as math_progress_ref_table_conf
from parcc_common.config.diagnostic_math_flu_ref_table_data import ref_table_conf as math_flu_ref_table_conf
from parcc_common.config.diagnostic_math_grade_level_ref_table_data import ref_table_conf as math_grade_level_ref_table_conf
from parcc_common.config.diagnostic_math_cluster_ref_table_data import ref_table_conf as math_cluster_ref_table_conf

from edudl2.udl2_util.config_reader import read_ini_file
from edudl2.udl2.defaults import UDL2_DEFAULT_CONFIG_PATH_FILE
from edudl2.database.metadata.udl2_metadata import generate_udl2_sequences
from edudl2.database.metadata.rsa_stored_procs import rsa_stored_procs
from edudl2.database.udl2_connector import initialize_db_udl,\
    get_udl_connection, initialize_db_prod, initialize_db_tenant
from edudl2.database.populate_ref_info import populate_ref_column_map,\
    populate_stored_proc
from edudl2.udl2.constants import Constants


def _parse_args():
    '''
    private method to parse command line options when call from shell. We use it to setup/teardown database
    automatically by configuration (this helps jenkins/Continous Integration)
    '''
    parser = argparse.ArgumentParser('database')
    parser.add_argument('--config_file', dest='config_file',
                        help="full path to configuration file for UDL2, default is /opt/wgen/edware-udl/etc/udl2_conf.py")
    parser.add_argument('--action', dest='action', required=False,
                        help="'setup' for setting up udl2 database. " +
                             "'teardown' for tear down udl2 database." +
                             "'tenant_setup' for setting up udl2 database for a new tenant")
    parser.add_argument('--tenant', dest='tenant', required=False,
                        help="If you chose 'tenant_setup' as your action, specify the tenant's name with this field.")
    args = parser.parse_args()
    return (parser, args)


def drop_schema(schema_name):
    '''
    drop schemas according to configuration file
    @param udl2_conf: The configuration dictionary for
    '''
    with get_udl_connection() as conn:
        conn.execute(DropSchema(schema_name, cascade=True))


def create_udl2_sequence(schema_name):
    '''
    create sequences according to configuration file
    @param udl2_conf: The configuration dictionary for
    '''
    print("create sequences")
    with get_udl_connection() as conn:
        metadata = conn.get_metadata()
        for sequence in generate_udl2_sequences(schema_name, metadata):
            conn.execute(CreateSequence(sequence))


def drop_udl2_sequences():
    '''
    drop sequences according to configuration file
    '''
    try:
        print("drop sequences")
        with get_udl_connection() as conn:
            for seq in generate_udl2_sequences():
                conn.execute(DropSequence(seq))
    except Exception as e:
        print("Error occurs when tearing down sequence: " + e)


def create_foreign_data_wrapper_extension(schema_name):
    '''
    create foreign data wrapper extension according to configuration file
    @param udl2_conf: The configuration dictionary for
    '''
    print('create foreign data wrapper extension')
    with get_udl_connection() as conn:
        conn.execute(text("CREATE EXTENSION IF NOT EXISTS file_fdw"))


def drop_foreign_data_wrapper_extension():
    '''
    drop foreign data wrapper extension according to configuration file
    '''
    print('drop foreign data wrapper extension')
    with get_udl_connection() as conn:
        conn.execute(text("DROP EXTENSION IF EXISTS file_fdw CASCADE"))


def create_foreign_data_wrapper_server(fdw_server):
    '''
    create server for foreign data wrapper according to configuration file
    @param udl2_conf: The configuration dictionary for
    '''
    print('create foreign data wrapper server')
    with get_udl_connection() as conn:
        conn.execute(text("CREATE SERVER %s FOREIGN DATA WRAPPER file_fdw" % (fdw_server)))


def drop_foreign_data_wrapper_server(fdw_server):
    '''
    drop server for foreign data wrapper according to configuration file
    @param udl2_conf: The configuration dictionary for
    '''
    print('drop foreign data wrapper server')
    with get_udl_connection() as conn:
        conn.execute(text("DROP SERVER IF EXISTS %s CASCADE" % (fdw_server)))


def load_reference_data():
    '''
    load the reference data into the reference tables
    @param udl2_conf: The configuration dictionary for
    '''
    #Summative Psychometric
    populate_ref_column_map(psychometric_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_PSYCHOMETRIC))
    #Summative Item
    populate_ref_column_map(xml_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_ITEM))
    populate_ref_column_map(ela_item_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_ELA_ITEM))
    populate_ref_column_map(math_item_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_MATH_ITEM))

    #Summative Assessment Results. We need to use this hack due to "special" architecture.
    # Warning, order does matter!
    ela_ref_table_configurations = (ela_ref_table_conf, ela_ref_table_conf_spring_2015,
                                    ela_ref_table_conf_fall_2015, ela_ref_table_conf_spring_2016)
    summative_ela = zip(ela_ref_table_configurations,
                        Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_ASMT_ELA))
    populate_summative_ref_column_map(summative_ela)

    math_ref_table_configurations = (math_ref_table_conf, math_ref_table_conf_spring_2015,
                                     math_ref_table_conf_fall_2015, math_ref_table_conf_spring_2016)
    summative_math = zip(math_ref_table_configurations,
                         Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_ASMT_MATH))
    populate_summative_ref_column_map(summative_math)

    #Summative Metadata. In case we'll have different metadata for different periods - use approach above!
    populate_ref_column_map(json_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_ASMT_ELA))
    populate_ref_column_map(json_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_ASMT_MATH))
    #MYA Item
    populate_ref_column_map(mya_ela_item_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_MYA_ELA_ITEM))
    populate_ref_column_map(mya_math_item_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_MYA_MATH_ITEM))
    #MYA Assessment Results
    populate_ref_column_map(mya_ela_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_MYA_ASMT_ELA))
    populate_ref_column_map(mya_math_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_MYA_ASMT_MATH))
    #MYA Metadata
    populate_ref_column_map(mya_json_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_MYA_ASMT_ELA))
    populate_ref_column_map(mya_json_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_MYA_ASMT_MATH))

    #SNL Assessment
    populate_ref_column_map(snl_ela_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_SNL_ASMT))
    #SNL Assessment Metadata
    populate_ref_column_map(snl_json_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_SNL_ASMT))
    #SNL Student Task Metadata
    populate_ref_column_map(snl_student_task_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_SNL_STUDENT_TASK))

    #Diagnostic ELA Item
    populate_ref_column_map(ela_reading_comp_item_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_READING_COMP))
    populate_ref_column_map(ela_vocab_item_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_VOCAB))
    populate_ref_column_map(ela_decoding_item_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_DECODING))

    #Diagnostic Math Item
    populate_ref_column_map(math_comp_item_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_COMPREHENSION))
    populate_ref_column_map(math_progress_item_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_PROGRESSION))
    populate_ref_column_map(math_fluency_item_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_FLUENCY))

    #Diagnostic ELA Vocab
    populate_ref_column_map(ela_vocab_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_DIAGNOSTIC_ELA_VOCAB))

    #Diagnostic ELA Comprehension
    populate_ref_column_map(ela_comp_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_DIAGNOSTIC_ELA_COMP))

    #Diagnostic ELA Decoding
    populate_ref_column_map(ela_decoding_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_DIAGNOSTIC_ELA_DECODING))

    #Diagnostic ELA Reading FLuency
    populate_ref_column_map(ela_read_flu_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_DIAGNOSTIC_ELA_READ_FLU))

    #Diagnostic ELA Writing
    populate_ref_column_map(ela_writing_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_DIAGNOSTIC_ELA_WRITING))

    #Diagnostic Math Progression
    populate_ref_column_map(math_progress_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_DIAGNOSTIC_MATH_PROGRESSION))

    #Diagnostic Math Fluency
    populate_ref_column_map(math_flu_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_DIAGNOSTIC_MATH_FLUENCY))

    #Diagnostic Math Locator
    populate_ref_column_map(math_locator_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_DIAGNOSTIC_MATH_LOCATOR))

    #Diagnostic Math Grade Level
    populate_ref_column_map(math_grade_level_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_DIAGNOSTIC_MATH_GRADE_LEVEL))

    #Diagnostic Math Cluster
    populate_ref_column_map(math_cluster_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_DIAGNOSTIC_MATH_CLUSTER))

    # Diagnostic Reader Motivation
    populate_ref_column_map(reader_motiv_ref_table_conf, Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_READER_MOTIVATION))


def populate_summative_ref_column_map(mapped):
    """
    Helper method to populate summative reference tables
    :param mapped: mapped reference data and tables configuration
    :return:
    """
    for ref_table, load_type in mapped:
        populate_ref_column_map(ref_table, load_type)


def load_stored_proc():
    '''
    Generate and load the stored procedures to be used for transformations and
    validations into the database.
    @param udl2_conf: The configuration dictionary for
    '''
    populate_stored_proc(Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_ASMT_MATH),
                         Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_ASMT_ELA),
                         Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_ITEM))


def load_rsa_procs():
    print("Loading the Report Suppression Actions Stored Procs")
    with get_udl_connection() as conn:
        for sql_text in rsa_stored_procs.values():
            conn.execute(sql_text)


def setup_udl2_schema(udl2_conf):
    '''
    create whole udl2 database schema according to configuration file
    @param udl2_conf: The configuration dictionary for
    '''
    initialize_db_prod(udl2_conf)
    # Setup udl2 schema
    initialize_db_udl(udl2_conf, allow_create_schema=True)
    udl2_schema_name = udl2_conf['udl2_db_conn']['db_schema']
    create_foreign_data_wrapper_extension(udl2_schema_name)
    create_foreign_data_wrapper_server(Constants.UDL2_FDW_SERVER)

    # load data and stored procedures into udl tables
    print('populate reference columns maps')
    load_reference_data()
    print('load stored procedures')
    load_stored_proc()
    load_rsa_procs()


def setup_udl2_tenant(udl2_conf, tenant):
    initialize_db_tenant(udl2_conf, tenant)


def teardown_udl2_schema(udl2_conf):
    '''
    drop whole udl2 database schema according to configuration file
    @param udl2_conf: The configuration dictionary for
    '''
    try:
        # Tear down udl2 schema
        initialize_db_udl(udl2_conf)
        # drop_udl2_sequences()
        drop_foreign_data_wrapper_server(Constants.UDL2_FDW_SERVER)
        drop_foreign_data_wrapper_extension()
        drop_schema(udl2_conf['udl2_db_conn']['db_schema'])
    except:
        pass


def main():
    '''
    create or drop udl2 database objects according to command line.
    The purpose for this script is to enable clean up whole database artifacts or create
    whole database artifacts without problem. Since UDL uses databases to clean data. Database object
    in UDL can be dropped or recreated at will for changes. So we can verifiy system
    '''
    (parser, args) = _parse_args()
    if args.config_file is None:
        config_path_file = UDL2_DEFAULT_CONFIG_PATH_FILE
    else:
        config_path_file = args.config_file
    conf_tup = read_ini_file(config_path_file)
    udl2_conf = conf_tup[0]

    if args.action is None:
        parser.print_help()
        return
    if args.action == 'setup':
        setup_udl2_schema(udl2_conf)
    elif args.action == 'teardown':
        teardown_udl2_schema(udl2_conf)
    elif args.action == 'tenant_setup':
        if args.tenant:
            setup_udl2_tenant(udl2_conf, args.tenant)
        else:
            print("Please supply the tenant's name if you choose the 'tenant_setup' action")

if __name__ == '__main__':
    main()
