__author__ = 'npandey'

rsa_stored_procs = {
    "aggregate": """CREATE OR REPLACE FUNCTION udl2.include_in_aggregate(record_type character varying, is_reported_summative character varying, report_suppression_code character varying, report_suppression_action character varying)
  RETURNS Boolean AS
$BODY$
DECLARE
    include_in_report Boolean;

BEGIN
    if record_type = '01' and is_reported_summative = 'Y' THEN
     if report_suppression_code in ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10') THEN
      if report_suppression_action in ('01', '02', '03', '05') THEN
       include_in_report = false;
      elseif report_suppression_action in ('04', '06') THEN
       include_in_report = true;
      else -- case for suppression actions 07-99
       include_in_report = true;
      end if; -- suppression action
     else -- report suppression code > 10
      include_in_report = true;
     end if; -- suppression code
    else -- case for report type 02 and 03; summative = N
      include_in_report = false;
    end if;

    RETURN include_in_report;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION udl2.include_in_aggregate(character varying, character varying, character varying, character varying)
  OWNER TO udl2;""",

    "school": """CREATE OR REPLACE FUNCTION udl2.include_in_school(record_type character varying, is_reported_summative character varying, report_suppression_code character varying, report_suppression_action character varying)
  RETURNS Boolean AS
$BODY$
DECLARE
    include_in_report Boolean;

BEGIN
    if record_type = '01' and is_reported_summative = 'Y' THEN
     if report_suppression_code in ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10') THEN
      if report_suppression_action in ('01', '02', '03', '05', '06') THEN
       include_in_report = false;
      elseif report_suppression_action = '04' THEN
       include_in_report = true;
      else -- case for suppression actions 07-99
       include_in_report = true;
      end if;
     else -- report suppression code > 10
      include_in_report = true;
     end if; -- suppression code
    else -- case for report type 02 and 03; summative = N
      include_in_report = false;
    end if;

    RETURN include_in_report;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION udl2.include_in_school(character varying, character varying, character varying, character varying)
  OWNER TO udl2;""",

    "roster": """CREATE OR REPLACE FUNCTION udl2.include_in_roster(record_type character varying, is_reported_summative character varying, report_suppression_code character varying, report_suppression_action character varying, is_roster_reported character varying)
  RETURNS character AS
$BODY$
DECLARE
    include_in_report character;

BEGIN
    if record_type = '01' THEN
     if is_reported_summative = 'Y' THEN
      if report_suppression_code in ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10') THEN
       if report_suppression_action in ('01', '04', '05') THEN
        include_in_report = 'N';
       elseif report_suppression_action in ('02', '06') THEN
        include_in_report = 'Y';
       elseif report_suppression_action = '03' THEN
        include_in_report = 'W';
       else -- case for suppression actions 07-99
        include_in_report = 'Y';
       end if; -- suppression action
      else -- case for report suppression code > 10
       include_in_report = 'Y';
      end if; -- suppression code
     else -- case for summative flag N
      include_in_report = 'N';
     end if; -- summative flag
    else -- case for report type 02 and 03
      if is_roster_reported = 'Y' THEN
        include_in_report = 'W';
      else
        include_in_report = 'N';
      end if;
    end if;

    RETURN include_in_report;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION udl2.include_in_roster(character varying, character varying, character varying, character varying, character varying )
  OWNER TO udl2;""",

    "isr": """CREATE OR REPLACE FUNCTION udl2.include_in_isr(record_type character varying, is_reported_summative character varying, report_suppression_code character varying, report_suppression_action character varying)
  RETURNS Boolean AS
$BODY$
DECLARE
    include_in_report Boolean;

BEGIN
    if record_type = '01' and is_reported_summative = 'Y' THEN
     if report_suppression_code in ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10') THEN
      if report_suppression_action in ('01', '03', '04') THEN
       include_in_report = false;
      elseif report_suppression_action in ('02', '05', '06') THEN
       include_in_report = true;
      else -- case for suppression actions 07-99
       include_in_report = true;
      end if;
     else -- report suppression code > 10
      include_in_report = true;
     end if; -- suppression code
    else -- case for report type 02 and 03; summative = N
      include_in_report = false;
    end if;

    RETURN include_in_report;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION udl2.include_in_isr(character varying, character varying, character varying, character varying)
  OWNER TO udl2;"""
}
