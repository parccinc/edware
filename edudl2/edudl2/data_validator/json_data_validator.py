import json
import os

import jsonschema
from celery.utils.log import get_task_logger

from edudl2.udl2.celery import udl2_conf
from edudl2.exceptions.errorcodes import ErrorCode
from edudl2.udl2.constants import Constants


logger = get_task_logger(__name__)


class JsonValidator():
    """
    Invoke a suite of validations for json files.
    """

    def __init__(self, load_type, schema_version=None):
        self.validators = [HasExpectedFormat(load_type)]

    def execute(self, dir_path, file_name, batch_sid):
        """
        Run all validation tests and return a list of error codes for all failures, or
        errorcodes.STATUS_OK if all tests pass

        @param dir_path: path of the file
        @type dir_path: string
        @param file_name: name of the file
        @type file_name: string
        @param batch_sid: batch id of the file
        @type batch_sid: integer
        @return: tuple of the form: (status_code, dir_path, file_name, batch_sid)
        """
        error_list = []
        for validator in self.validators:
            result = validator.execute(dir_path, file_name, batch_sid)
            if result[0] != ErrorCode.STATUS_OK:
                error_list.append(result)
            else:
                pass
        return error_list


class HasExpectedFormat(object):
    '''Make sure the JSON file is formatted to our standards '''

    def __init__(self, load_type):
        self.schema_name = Constants.JSON_SCHEMA_MAPPING(load_type)
        self.schema_path = udl2_conf.get('schema_dir')

    def execute(self, dir_path, file_name, batch_sid):
        '''
        Iterate through all the elements of mapping, and check that we can reach all expected fields using
        the provided paths.

        @param dir_path: path of the file
        @type dir_path: string
        @param file_name: name of the file
        @type file_name: string
        @param batch_sid: batch id of the file
        @type batch_sid: integer
        @return: tuple of the form: (status_code, dir_path, file_name, batch_sid, field) or (status_code, dir_path, file_name, batch_sid)
        '''
        complete_file_path = os.path.join(dir_path, file_name)
        complete_schema_path = os.path.join(self.schema_path, self.schema_name)
        with open(complete_file_path) as input_file:
            with open(complete_schema_path) as schema:
                try:
                    json_file = json.loads(input_file.read())
                    json_schema = json.loads(schema.read())
                    jsonschema.validate(json_file, json_schema)
                    return ErrorCode.STATUS_OK, dir_path, file_name, batch_sid
                except jsonschema.ValidationError as e:
                    logger.error('INVALID JSON. JSON SCHEMA VALIDATOR RETURNED: {error}'.format(error=e.message))
                    return ErrorCode.SRC_JSON_INVALID_FORMAT, dir_path, file_name, batch_sid
