from __future__ import absolute_import

"""
Validates the content of csv.

Some of the basic validations include (but not limited to)
a) Checking the file has the correct headers
b) Checking if the required fields in the csv are not left blank
"""
import csv

from edudl2.exceptions.errorcodes import ErrorCode
from edudl2.data_validator.data_validator_util import get_source_column_values_from_ref_column_mapping
from edudl2.udl2_util.file_util import abs_path_join
from edudl2.udl2.constants import Constants
from parcc_udl.config.validation_config import ValidationConfig
from celery.utils.log import get_task_logger
from csvvalidator import *

logger = get_task_logger(__name__)


class CsvValidator():
    """
    Invoke a suite of validations for csv files.
    """

    def __init__(self, load_type, schema_version):
        """Constructor
        @param arg_udl_db: database connections
        @type arg_udl_db: udl.lib.UdlDb
        """
        self.csv_validations = [DoesSourceFileInExpectedFormat(load_type, schema_version),
                                DoesSourceFileHaveValidData(load_type, schema_version)
                                ]

    def execute(self, dir_path, file_name, batch_sid):
        """
        Run all validation tests and return a list of error codes for all failures, or
        errorcodes.STATUS_OK if all tests pass

        @param dir_path: path of the file
        @type dir_path: string
        @param file_name: name of the file
        @type file_name: string
        @param batch_sid: batch id of the file
        @type batch_sid: integer
        @return: tuple of the form: (status_code, dir_path, file_name, batch_sid)
        """

        error_list = []
        for validation in self.csv_validations:
            result = validation.execute(dir_path, file_name, batch_sid)
            if result[0] != ErrorCode.STATUS_OK:
                error_list.append(result)
            else:
                pass
        return error_list


class DoesSourceFileInExpectedFormat(object):
    """Check if source file is in the expected format with all the columns expected"""

    def __init__(self, load_type, schema_version, csv_fields=None):
        expected_csv_fields = get_source_column_values_from_ref_column_mapping(
            Constants.UDL2_CSV_LZ_TABLE, load_type) if csv_fields is None else csv_fields

        if isinstance(expected_csv_fields, dict):
            expected_csv_fields = expected_csv_fields[schema_version]

        expected_csv_fields = [field.lower() for field in expected_csv_fields]
        self.expected_csv_fields = expected_csv_fields

    def are_eq(self, a, b):
        return len(a) == len(b) and set(a) == set(b)

    def execute(self, dir_path, file_name, batch_sid, schema_version=None):
        """Check if file has only and all the columns expected to be present
           the validator does not care about case and order of the columns

        @param dir_path: path of the file
        @type dir_path: string
        @param file_name: name of the file
        @type file_name: string
        @param batch_sid: batch id of the file
        @type batch_sid: integer
        @return: tuple of the form: (status_code, dir_path, file_name, batch_sid)
        """
        full_path = abs_path_join(dir_path, file_name)

        try:
            # open file and get the header
            file_to_validate = open(full_path, 'rU', encoding='utf-8')
            file_reader = csv.reader(file_to_validate)
            header_row = next(file_reader)
            file_to_validate.close()
            header_row = [column.lower() for column in header_row]

            if not self.are_eq(header_row, self.expected_csv_fields):
                return ErrorCode.SRC_FILE_HAS_HEADERS_MISMATCH_EXPECTED_FORMAT, dir_path, file_name, batch_sid

        except StopIteration:
            return ErrorCode.SRC_FILE_HAS_NO_DATA, dir_path, file_name, batch_sid
        except FileNotFoundError as e:
            return ErrorCode.SRC_FILE_NOT_ACCESSIBLE_SFV, dir_path, file_name, batch_sid
        except Exception as e1:
            return ErrorCode.STATUS_UNKNOWN_ERROR, dir_path, file_name, batch_sid

        return ErrorCode.STATUS_OK, dir_path, file_name, batch_sid


class DoesSourceFileHaveValidData(object):
    """Check if source file has data in the required fields"""

    def __init__(self, load_type, schema_version):
        self.load_type = load_type
        self.schema_version = schema_version

    def execute(self, dir_path, file_name, batch_sid):
        full_path = abs_path_join(dir_path, file_name)
        if self.load_type in ValidationConfig.CSV_VALIDATION_LOAD_TYPES():
            try:
                with open(full_path, 'r') as f:
                    data = csv.reader(f)
                    field_names = next(data)

                with open(full_path, 'rU') as file_to_validate:
                    validator = self.create_csv_validator(field_names)

                    data = csv.reader(file_to_validate)
                    validation_errors = validator.validate(data)
                    if len(validation_errors) > 0:
                        for validation_error in validation_errors:
                            logger.info('CSV data validation failed because of the following problems: ' + str(validation_error['message']))
                        return ErrorCode.SRC_FILE_HAS_MISSING_REQUIRED_DATA, dir_path, file_name, batch_sid

            except FileNotFoundError as e:
                return ErrorCode.SRC_FILE_NOT_ACCESSIBLE_SFV, dir_path, file_name, batch_sid
            except AssertionError as e:
                # We have a missing required field, should be caught by DoesSourceFileInExpectedFormat
                pass
            except Exception as e:
                return ErrorCode.STATUS_UNKNOWN_ERROR, dir_path, file_name, batch_sid

        return ErrorCode.STATUS_OK, dir_path, file_name, batch_sid

    def create_csv_validator(self, field_names):
        validator = CSVValidator(field_names)

        for name in ValidationConfig.CSV_REQUIRED_HEADER_MAPPING(self.load_type, self.schema_version):
            validator.add_value_check(name, self.check_not_blank, 'Missing Data',
                                      'missing value for required field {name}'.format(name=name))

        return validator

    def check_not_blank(self, field_value):
        if field_value is None or field_value.isspace() or field_value == '':
            raise ValueError()
