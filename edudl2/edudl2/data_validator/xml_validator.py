from edudl2.exceptions.errorcodes import ErrorCode
from edudl2.udl2.celery import udl2_conf
from lxml import etree
import os
import queue


class XMLValidator(object):

    """
    This class performs validation on an XML file

    """

    def __init__(self, loadtype, schema_version=None):
        """ Initialize the XMLValidator object

        """
        self.default_schema_file = os.path.realpath(
            os.path.join(udl2_conf.get('schema_dir'), 'assessment_results.xsd'))
        self.valid_session_identifiers = {'/pearson/student', '/parcc/assessmentRegistrationRefId'}
        self.source_ids = []
        self.num_scores = 0
        self.num_item_results = 0
        self.num_ass_results = 0
        self.q = queue.Queue()

    def execute(self, dir_path, file_name, batch_sid):
        """
        Run all validation tests and return a list of error codes for all failures, or
        errorcodes.STATUS_OK if all tests pass

        :param dir_path: path of the file
        :param file_name: name of the file
        :param batch_sid: batch id of the file
        :type dir_path: str
        :type file_name: str
        :type batch_sid: int
        :returns: status_code, dir_path, file_name, batch_sid
        :rtype: list

        """
        self.xmlfilepath = os.path.join(dir_path, file_name)

        validity_code = self.validate()
        if validity_code != ErrorCode.STATUS_OK:
            return [(validity_code, dir_path, file_name, batch_sid)]
        return []

    def validate(self):
        """
        Validates the XML file.

        :returns: status_code
        :rtype: int

        """
        if not os.path.isfile(self.xmlfilepath):
            return ErrorCode.SRC_XML_INVALID_PATH

        with open(self.default_schema_file, 'r') as f:
            xmlschema_doc = etree.parse(f)
        schema = etree.XMLSchema(xmlschema_doc)
        try:
            parser = etree.iterparse(open(self.xmlfilepath, 'rb'))
            self.iterate_xml(parser, self.pass_function)
        except etree.XMLSyntaxError as e:
            return ErrorCode.SRC_XML_INVALID_STRUCTURE

        try:
            parser = etree.iterparse(open(self.xmlfilepath, 'rb'), schema=schema)
            self.iterate_xml(parser, self.pass_function)
        except etree.XMLSyntaxError as e:
            return ErrorCode.SRC_XML_INVALID_FORMAT

        if not self.validate_missing_data():
            return ErrorCode.SRC_XML_MISSING_DATA

        return ErrorCode.STATUS_OK

    def validate_missing_data(self):
        """ Helper method to validate missing data

        :returns: whether there's missing data or not
        :rtype: bool

        """
        with open(self.xmlfilepath, 'rb') as f:
            parser = etree.iterparse(open(self.xmlfilepath, 'rb'))
            self.iterate_xml(parser, self.check_element_for_missing_field)

        if self.num_ass_results < 1:
            return False

        num_source_ids = len(self.source_ids)
        if self.valid_session_identifiers == set(self.source_ids) and ((self.num_scores + self.num_item_results + num_source_ids) / 4) == self.num_ass_results:
            return True
        return False

    def pass_function(self, event, elem):
        pass

    def check_element_for_missing_field(self, e, elem):
        tag = elem.tag.split("}")[1].strip()

        if tag == 'sessionIdentifier':
            self.source_ids.append(elem.attrib['sourceID'])
        elif tag == 'outcomeVariable' and elem.attrib['identifier'].lower() == 'score':
            if self.q.empty():
                return False
            if self.q.get().get('value', None) is None:
                return False
            self.num_scores += 1
        elif tag == 'itemResult':
            self.num_item_results += 1
        elif tag == 'assessmentResult':
            self.num_ass_results += 1

        if not self.q.empty():
            self.q.get()
        self.q.put({tag: elem.text})

    def iterate_xml(self, context, func):
        for event, elem in context:
            func(event, elem)
            elem.clear()
            while elem.getprevious() is not None:
                del elem.getparent()[0]
        del context
