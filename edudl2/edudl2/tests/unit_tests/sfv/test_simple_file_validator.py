from edudl2.sfv import simple_file_validator
from edudl2.exceptions.errorcodes import ErrorCode
import unittest
from edudl2.udl2.defaults import UDL2_DEFAULT_CONFIG_PATH_FILE
import os
from edudl2.udl2_util.config_reader import read_ini_file


class UnitTestSimpleFileValidator(unittest.TestCase):

    def setUp(self):
        try:
            config_path = dict(os.environ)['UDL2_CONF']
        except Exception:
            config_path = UDL2_DEFAULT_CONFIG_PATH_FILE
        conf_tup = read_ini_file(config_path)
        self.conf = conf_tup[0]
        self.data_dir = os.path.join(os.path.dirname(__file__), "..", "..", "data")
        self.test_csv_field_list = {'StateAbbreviation', 'ResponsibleDistrictIdentifier', 'ResponsibleDistrictName', 'ResponsibleSchoolIdentifier', 'OrganizationName', 'OrganizationalStructure', 'ShortNameOfInstitution', 'OrganizationType', 'AdministrativeFundingControl', 'ProgramType', 'CountyANSICode', 'EconomicResearchServiceRuralUrbanContinuumCode', 'PARCCStudentIdentifier', 'StateStudentIdentifier', 'LocalStudentIdentifier', 'FirstName', 'MiddleName', 'LastOrSurname', 'Sex', 'Birthdate', 'GradeLevelWhenAssessed', 'HispanicOrLatinoEthnicity', 'AmericanIndianOrAlaskaNative', 'Asian', 'BlackOrAfricanAmerican', 'NativeHawaiianOrOtherPacificIslander', 'White', 'DemographicRaceTwoOrMoreRaces', 'LEPStatus', 'Section504Status', 'EconomicDisadvantageStatus', 'MigrantStatus', 'EnglishLanguageLearnerELL', 'GiftedAndTalented', 'StudentWithDisability', 'PrimaryDisabilityType', 'StateFillerField1', 'StateFillerField2', 'StateFillerField3', 'StateFillerField4', 'StateFillerField5', 'StateFillerField6', 'StateFillerField7', 'StateFillerField8', 'StateFillerField9', 'StateFillerField10', 'AssessmentSessionProctorIdentifier', 'Group1Text', 'Group2Id', 'Group2Text', 'AssessmentGuid', 'AssessmentAcademicSubject', 'AssessmentSessionLocationId', 'AssessmentSessionLocation', 'AssessmentRegistrationTestAttemptIdentifier', 'AssessmentFormNumber', 'AssessmentSessionActualEndDateTime', 'AssessmentSessionActualStartDateTime', 'AssessmentAdministrationFinishDate', 'AssessmentYear', 'AssessmentPeriod', 'AssessmentSubtestResultScoreValue', 'AssessmentPerformanceLevelScoreMetric', 'AssessmentSubtestScaledScore', 'AssessmentSubtestPerformanceLevel', 'AssessmentSubtestResultScoreClaim1Value', 'AssessmentSubtestResultScoreClaim2Value', 'AssessmentSubtestResultScoreSubclaim1Value', 'AssessmentSubtestResultScoreSubClaim2Value', 'AssessmentSubtestResultScoreSubClaim3Value', 'AssessmentSubtestResultScoreSubClaim4Value', 'AssessmentSubtestResultScoreSubClaim5Value', 'AccommodationAmericanSignLanguage', 'AccommodationSignLanguageHumanIntervention', 'AccommodationBraille', 'AccommodationClosedCaptioning', 'AccommodationTextToSpeech', 'AccommodationAbacus', 'AccommodationAlternateResponseOptions', 'AccommodationCalculator', 'AccommodationMultiplicationTable', 'AccommodationPrintOnDemand', 'AccommodationReadAloud', 'AccommodationScribe', 'AccommodationSpeechToText', 'AccommodationStreamlineMode', 'AccommodationDictionaryInNativeLanguage', 'AccommodationWordPredition', 'AccommodationAnswerMashing', 'AccommodationColorContrast', 'AccommodationLargePrint', 'AccommodationTactileGraphics', 'AccommodationReadSynthetic', 'AccommodationReadQuestions', 'PnpSpokenSupportUsage', 'PnpASL', 'PnpCloseCaptioning', 'PnpBrailleUsage', 'PnpReadAloud', 'PnpAlternateLanguage', 'PnpAlternativeText', 'PnpAnswerAssignedSupport', 'PnpLineReader', 'PnpMagnification', 'PnpAuditoryCalming', 'PnpAdditionalBreaks', 'PnpSpeechToText', 'PnpTranscription', 'AssessmentAttemptFlag', 'AssessmentParticipantSessionPlatformType', 'AssessmentRegistrationRetestIndicator', 'AssessmentRegistrationTestingIndicator', 'AssessmentRegistrationReasonNotCompleting'}

    def _setup_valid_mocks(self, csv_mock):
        csv_mock.return_value = self.test_csv_field_list

    def test_simple_file_validator_passes_for_valid_asmt_csv(self):
        validator = simple_file_validator.SimpleFileValidator()
        results = validator.execute(self.data_dir,
                                    'unittest/'
                                    'valid_asmt.csv', 1)
        print(results)
        self.assertEqual(len(results), 0)

    def test_simple_file_validator_fails_for_missing_csv(self):
        validator = simple_file_validator.SimpleFileValidator()
        results = validator.execute(self.data_dir, 'nonexistent.csv', 1)
        self.assertEqual(results[0][0], ErrorCode.SRC_FILE_NOT_ACCESSIBLE_SFV, "Wrong error code")
        validator = simple_file_validator.SimpleFileValidator()
        results = validator.execute(self.data_dir, 'nonexistent.csv', 1)
        self.assertEqual(results[0][0], ErrorCode.SRC_FILE_NOT_ACCESSIBLE_SFV, "Wrong error code")

    def test_simple_file_validator_invalid_extension(self):
        validator = simple_file_validator.SimpleFileValidator()
        results = validator.execute(self.data_dir, 'invalid_ext.xls', 1)
        self.assertEqual(results[0][0], ErrorCode.SRC_FILE_TYPE_NOT_SUPPORTED)
        validator = simple_file_validator.SimpleFileValidator()
        results = validator.execute(self.data_dir, 'invalid_ext.xls', 1)
        self.assertEqual(results[0][0], ErrorCode.SRC_FILE_TYPE_NOT_SUPPORTED)

    def test_extention_errorcode(self):
        expected_error_code = simple_file_validator.SimpleFileValidator().execute(self.data_dir, "REALDATA_3010.xlsx", 123)
        self.assertEqual(expected_error_code[0][0], ErrorCode.SRC_FILE_TYPE_NOT_SUPPORTED, "Validation Code for different file formate is incorrect")
