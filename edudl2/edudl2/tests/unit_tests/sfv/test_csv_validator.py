import unittest
from edudl2.exceptions.errorcodes import ErrorCode
from edudl2.sfv import csv_validator
import os
from edudl2.udl2.defaults import UDL2_DEFAULT_CONFIG_PATH_FILE
from edudl2.udl2_util.config_reader import read_ini_file
from edudl2.udl2.constants import Constants
from unittest.mock import patch

__location__ = '../data'


class TestCsvValidator(unittest.TestCase):

    def setUp(self, ):
        try:
            config_path = dict(os.environ)['UDL2_CONF']
        except Exception:
            config_path = UDL2_DEFAULT_CONFIG_PATH_FILE

        conf_tup = read_ini_file(config_path)
        self.conf = conf_tup[0]
        self.data_dir = os.path.join(os.path.dirname(__file__), "..", "..", "data")
        self.test_csv_field_list = {'StateAbbreviation', 'ResponsibleDistrictIdentifier', 'ResponsibleDistrictName', 'ResponsibleSchoolIdentifier', 'OrganizationName', 'OrganizationalStructure', 'ShortNameOfInstitution', 'OrganizationType', 'AdministrativeFundingControl', 'ProgramType', 'CountyANSICode', 'EconomicResearchServiceRuralUrbanContinuumCode', 'PARCCStudentIdentifier', 'StateStudentIdentifier', 'LocalStudentIdentifier', 'FirstName', 'MiddleName', 'LastOrSurname', 'Sex', 'Birthdate', 'GradeLevelWhenAssessed', 'HispanicOrLatinoEthnicity', 'AmericanIndianOrAlaskaNative', 'Asian', 'BlackOrAfricanAmerican', 'NativeHawaiianOrOtherPacificIslander', 'White', 'DemographicRaceTwoOrMoreRaces', 'LEPStatus', 'Section504Status', 'EconomicDisadvantageStatus', 'MigrantStatus', 'EnglishLanguageLearnerELL', 'GiftedAndTalented', 'StudentWithDisability', 'PrimaryDisabilityType', 'StateFillerField1', 'StateFillerField2', 'StateFillerField3', 'StateFillerField4', 'StateFillerField5', 'StateFillerField6', 'StateFillerField7', 'StateFillerField8', 'StateFillerField9', 'StateFillerField10', 'AssessmentSessionProctorIdentifier', 'Group1Text', 'Group2Id', 'Group2Text', 'AssessmentGuid', 'AssessmentAcademicSubject', 'AssessmentSessionLocationId', 'AssessmentSessionLocation', 'AssessmentRegistrationTestAttemptIdentifier', 'AssessmentFormNumber', 'AssessmentSessionActualEndDateTime', 'AssessmentSessionActualStartDateTime', 'AssessmentAdministrationFinishDate', 'AssessmentYear', 'AssessmentPeriod', 'AssessmentSubtestResultScoreValue', 'AssessmentPerformanceLevelScoreMetric', 'AssessmentSubtestScaledScore', 'AssessmentSubtestPerformanceLevel', 'AssessmentSubtestResultScoreClaim1Value', 'AssessmentSubtestResultScoreClaim2Value', 'AssessmentSubtestResultScoreSubclaim1Value', 'AssessmentSubtestResultScoreSubClaim2Value', 'AssessmentSubtestResultScoreSubClaim3Value', 'AssessmentSubtestResultScoreSubClaim4Value', 'AssessmentSubtestResultScoreSubClaim5Value', 'AccommodationAmericanSignLanguage', 'AccommodationSignLanguageHumanIntervention', 'AccommodationBraille', 'AccommodationClosedCaptioning', 'AccommodationTextToSpeech', 'AccommodationAbacus', 'AccommodationAlternateResponseOptions', 'AccommodationCalculator', 'AccommodationMultiplicationTable', 'AccommodationPrintOnDemand', 'AccommodationReadAloud', 'AccommodationScribe', 'AccommodationSpeechToText', 'AccommodationStreamlineMode', 'AccommodationDictionaryInNativeLanguage', 'AccommodationWordPredition', 'AccommodationAnswerMashing', 'AccommodationColorContrast', 'AccommodationLargePrint', 'AccommodationTactileGraphics', 'AccommodationReadSynthetic', 'AccommodationReadQuestions', 'PnpSpokenSupportUsage', 'PnpASL', 'PnpCloseCaptioning', 'PnpBrailleUsage', 'PnpReadAloud', 'PnpAlternateLanguage', 'PnpAlternativeText', 'PnpAnswerAssignedSupport', 'PnpLineReader', 'PnpMagnification', 'PnpAuditoryCalming', 'PnpAdditionalBreaks', 'PnpSpeechToText', 'PnpTranscription', 'AssessmentAttemptFlag', 'AssessmentParticipantSessionPlatformType', 'AssessmentRegistrationRetestIndicator', 'AssessmentRegistrationTestingIndicator', 'AssessmentRegistrationReasonNotCompleting'}

    # For bad CSVFiles
    def test_sourcefolder_errorcode(self):
        CSV_FOLDER = "csv_file11"
        # Test # 1 -- > Check folder does exists or not (True / False), if not return Error 3001.
        csv_folder = os.path.join(self.data_dir, CSV_FOLDER)
        validate_instance = csv_validator.IsSourceFolderAccessible()
        expected_error_code = validate_instance.execute(csv_folder, CSV_FOLDER, 123)
        self.assertEqual(expected_error_code[0], ErrorCode.SRC_FOLDER_NOT_ACCESSIBLE_SFV, "Validation Code for CSV Source folder not accessible is incorrect")

    def test_sourcefile_errorcode(self):
        #  test#2 --> source_file_accessible
        validate_instance = csv_validator.IsSourceFileAccessible()
        expected_error_code = validate_instance.execute(self.data_dir, "REALDATA_3002.csv", 123)
        self.assertEqual(expected_error_code[0], ErrorCode.SRC_FILE_NOT_ACCESSIBLE_SFV, "Validation Code for CSV Source file not accessible is incorrect")

    def test_blankfile_errorcode(self):
        # test#3 --> blank_file
        validate_instance = csv_validator.IsFileBlank()
        expected_error_code = validate_instance.execute(self.data_dir, "realdata_3003.csv", 123)
        self.assertEqual(expected_error_code[0], ErrorCode.SRC_FILE_HAS_NO_DATA, "Validation Code for CSV blank file is incorrect")

    def test_wrong_delimiter_errorcode(self):
        # test#3 --> blank_file
        validate_instance = csv_validator.IsSourceFileCommaDelimited()
        expected_error_code = validate_instance.execute(self.data_dir, "REALDATA_3005.csv", 123)
        self.assertEqual(expected_error_code[0], ErrorCode.SRC_FILE_WRONG_DELIMITER, "Validation Code for CSV file with wrong delimiter is incorrect")

    def test_noHeader_errorcode(self):
        # test#4 --> no headers (3009)
        validate_instance = csv_validator.DoesSourceFileContainHeaders()
        expected_error_code = validate_instance.execute(self.data_dir, "realdata_3009.csv", 123)
        self.assertEqual(expected_error_code[0], ErrorCode.SRC_FILE_HAS_NO_HEADERS, "Validation Code for no header is incorrect")

    def test_duplicateHeaders_errorcode(self):
        # test#5 --> duplicate_values (3011)
        validate_instance = csv_validator.DoesSourceFileContainDuplicateHeaders()
        expected_error_code = validate_instance.execute(self.data_dir, "REALDATA_3011.csv", 123)
        self.assertEqual(expected_error_code[0], ErrorCode.SRC_FILE_HAS_DUPLICATE_HEADERS, "Validation Code for duplicate headers is incorrect")

    def test_noDataRow_errorcode(self):
        # test#6 --> DoesSourceFileHaveData
        validate_instance = csv_validator.DoesSourceFileHaveData()
        expected_error_code = validate_instance.execute(self.data_dir, "test_file_headers.csv", 123)
        self.assertEqual(expected_error_code[0], ErrorCode.SRC_FILE_HAS_NO_DATA, "Validation Code for atleast one data row is incorrect")

    def test_dataMismatch_errorcode(self):
        # test#7 --> dataFormate
        validate_instance = csv_validator.IsCsvWellFormed()
        expected_error_code = validate_instance.execute(self.data_dir, "REALDATA_3008.csv", 123)
        self.assertEqual(expected_error_code[0], ErrorCode.SRC_FILE_HEADERS_MISMATCH_DATA, "Validation Code for data mismatch row is incorrect")

    def test_simple_file_validator_wrong_delimiter(self):

        validator = csv_validator.IsSourceFileCommaDelimited()
        results = validator.execute(self.data_dir, 'unittest/wrong_delimiter.csv', 1)
        print(results)
        self.assertEqual(results[0], ErrorCode.SRC_FILE_WRONG_DELIMITER)
