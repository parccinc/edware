from edudl2.exceptions.errorcodes import ErrorCode
import unittest
from edudl2.sfv import json_validator
import os

__author__ = 'npandey'


class TestJsonValidator(unittest.TestCase):

    def setUp(self):
        self.data_dir = os.path.join(os.path.dirname(__file__), "..", "..", "data", "unittest")
        self.invalid_structure = 'summative_assessment_bad_structure.json'

    def test_json_structure_errorcode(self):
        #test# 9 --> file structure
        validate_instance = json_validator.JsonValidator().validators[0]
        expected_error_code = validate_instance.execute(self.data_dir, self.invalid_structure, 123)
        self.assertEqual(expected_error_code[0], ErrorCode.SRC_JSON_INVALID_STRUCTURE, "Validation Code for JSON file structure is incorrect")
