from unittest.mock import patch
from edudl2.move_to_integration.move_to_integration_setup import get_table_column_types
from edudl2.tests.unit_tests.unittest_with_udl2_sqlite import Unittest_with_udl2_sqlite
from edudl2.move_to_integration.move_to_integration import get_column_mapping_to_int


class TestMoveToIntegration(Unittest_with_udl2_sqlite):
    @classmethod
    def setUpClass(cls):
        super().setUpClass(force_foreign_keys=False)

    def setUp(self):
        self.test_target_columns = ['batch_guid', 'record_num', 'state_code', 'district_name', 'student_first_name', 'student_last_name']
        self.test_source_columns = ['batch_guid', 'record_num', 'state_code', 'district_name', 'student_first_name', 'student_last_name']
        self.test_target_column_types = {'batch_guid VARCHAR(50)', 'record_num BIGINT', 'state_code VARCHAR(30)',
                                         'district_name VARCHAR(150)', 'student_first_name VARCHAR(150)',
                                         'student_last_name VARCHAR(150)'}

    @patch('edudl2.move_to_integration.move_to_integration.get_column_mapping')
    def test_get_column_mapping_to_int(self, mock_column_mapping):
        integration_table_name = 'int_ela_sum'
        ref_table_name = 'ela_sum_ref_column_mapping'
        mock_row_data = [{'target_column': 'batch_guid', 'source_column': 'batch_guid'}, {'target_column': 'record_num', 'source_column': 'record_num'}]
        mock_column_mapping.return_value = mock_row_data

        column_mapping_dict = get_column_mapping_to_int(ref_table_name, integration_table_name)
        for column_name in column_mapping_dict.keys():
            self.assertIn(column_name, self.test_target_columns)

    def test_get_table_column_types(self):
        conf = {'tenant_name': 'edware', 'guid_batch': None}
        integration_table_name = 'int_ela_sum'

        column_types = get_table_column_types(conf, integration_table_name, self.test_target_columns)
        for column_type in column_types.values():
            self.assertIn(column_type, self.test_target_column_types)
