from collections import OrderedDict
import unittest
from edudl2.move_to_integration.create_queries import create_insert_query
from edudl2.udl2 import message_keys as mk

__author__ = 'ablum'


class TestCreateQueries(unittest.TestCase):

    def test_create_insert_query_for_dim_table(self):
        guid_batch = '8866c6d5-7e5e-4c54-bf4e-775abc4021b2'
        conf = self.generate_conf(guid_batch)
        target_table, source_table = ('int_ela_sum', 'stg_ela_sum')
        column_mapping = self.get_column_mapping(target_table)
        column_types = self.get_column_types(target_table)
        actual_value = create_insert_query(conf, source_table, target_table, column_mapping, column_types, False)
        expected_value = self.get_expected_insert_query(conf[mk.SOURCE_DB_HOST], conf[mk.SOURCE_DB_PORT], target_table, guid_batch,
                                                        conf[mk.SOURCE_DB_NAME], conf[mk.SOURCE_DB_USER], conf[mk.SOURCE_DB_PASSWORD])
        self.assertEqual(str(actual_value), expected_value)

    def generate_conf(self, guid_batch):
        '''
        Return all needed configuration information
        '''
        conf = {mk.GUID_BATCH: guid_batch,
                mk.SOURCE_DB_SCHEMA: 'udl2',
                mk.TARGET_DB_SCHEMA: guid_batch,
                mk.SOURCE_DB_DRIVER: 'postgres',
                mk.SOURCE_DB_USER: 'udl2',
                mk.SOURCE_DB_PASSWORD: 'udl22013',
                mk.SOURCE_DB_HOST: 'localhost',
                mk.SOURCE_DB_PORT: '5432',
                mk.SOURCE_DB_NAME: 'udl2',
                mk.TENANT_NAME: 'edware'}
        return conf

    def get_column_mapping(self, target_table):
        column_map_integration_to_target = {'int_ela_sum': OrderedDict([('batch_guid', 'batch_guid'),
                                                                        ('record_num', 'record_num'),
                                                                        ('state_code', 'state_code'),
                                                                        ('district_name', 'district_name'),
                                                                        ('student_first_name', 'student_first_name'),
                                                                        ('student_last_name', 'student_last_name')
                                                                        ])}

        return column_map_integration_to_target[target_table]

    def get_column_types(self, target_table):
        column_names = list(self.get_column_mapping(target_table).keys())
        column_types = ['batch_guid character varying(50)', 'record_num bigint', 'state_code character varying(30)',
                        'district_name character varying(150)', 'student_first_name character varying(150)',
                        'student_last_name character varying(160)']
        column_name_type_map = OrderedDict()
        for i in range(len(column_names)):
            column_name_type_map[column_names[i]] = column_types[i]
        return column_name_type_map

    def get_expected_insert_query(self, host_name, port, table_name, guid_batch, dbname, user, password):
        return "INSERT INTO \"{guid_batch}\".\"{table_name}\" (batch_guid,record_num,state_code,district_name,student_first_name,student_last_name) " \
               "SELECT * FROM dblink('host={host} port={port} dbname={dbname} user={user} password={password}', " \
               "'SELECT batch_guid, record_num,state_code,district_name,student_first_name,student_last_name "\
               "FROM \"udl2\".\"stg_ela_sum\" WHERE batch_guid=':batch_guid' ') "\
               "AS t(batch_guid character varying(50),record_num bigint,state_code character varying(30),district_name character varying(150),"\
               "student_first_name character varying(150),student_last_name character varying(160));".format(host=host_name, port=port, table_name=table_name, guid_batch=guid_batch, dbname=dbname, user=user, password=password)
