from edudl2.udl2.udl2_pipeline import Pipeline

__author__ = 'swimberly'

import unittest
from celery import chain, task

MESSAGE_KEYS = ['landing_zone_work_dir', 'load_type', 'parts', 'guid_batch', 'input_file_path']


class TestUDL2Pipeline(unittest.TestCase):

    def test_get_pipeline_chain_check_msg(self):

        arch_file = 'path_to_some_file'
        load_type = 'some_load_type'
        file_part = 12
        batc_guid = '1234-s5678'
        pipeline_chain = Pipeline.create('edudl2.tests.unit_tests.test_udl2_pipeline.MockPipeline').get_pipeline_chain(arch_file, load_type, file_part, batc_guid)

        msg = pipeline_chain.tasks[0].args[0]

        for mk in MESSAGE_KEYS:
            self.assertIn(mk, msg)

    def test_get_pipeline_chain_check_msg_values(self):
        arch_file = 'path_to_some_file'
        load_type = 'some_load_type'
        file_part = 12
        batc_guid = '1234-s5678'
        pipeline_chain = Pipeline.create('edudl2.tests.unit_tests.test_udl2_pipeline.MockPipeline').get_pipeline_chain(arch_file, load_type, file_part, batc_guid)

        msg = pipeline_chain.tasks[0].args[0]
        self.assertEqual(msg['guid_batch'], batc_guid)
        self.assertEqual(msg['parts'], file_part)
        self.assertEqual(msg['input_file_path'], arch_file)
        self.assertEqual(msg['load_type'], load_type)


class MockPipeline(Pipeline):
    def get_pipeline(self, arrival_msg):
        return chain(mock_task.s(arrival_msg))


@task
def mock_task(msg):
    return msg

if __name__ == '__main__':
    unittest.main()
