from edudl2.fileexpander import file_expander

import unittest
import os


class TestFileExpander(unittest.TestCase):

    def setUp(self):
        self.test_tarball_name = '/tmp/some_file.tar.gz'
        self.test_zip_name = '/tmp/some_file.zip'

    def tearDown(self):
        # cleanup
        test_files = (self.test_tarball_name, self.test_zip_name)
        for file_name in test_files:
            if os.path.isfile(file_name):
                os.remove(file_name)

    def test_is_file_exists_for_invalid_tarball_file(self):
        result = file_expander._is_file_exists(self.test_tarball_name)
        self.assertFalse(result)

    def test_is_file_exists_for_invalid_zip_file(self):
        result = file_expander._is_file_exists(self.test_zip_name)
        self.assertFalse(result)

    def test_is_file_exists_for_valid_tarball_file(self):
        open(self.test_tarball_name, 'w')
        self.assertTrue(os.path.isfile(self.test_tarball_name))
        result = file_expander._is_file_exists(self.test_tarball_name)
        self.assertTrue(result)

    def test_is_file_exists_for_valid_zip_file(self):
        open(self.test_zip_name, 'w')
        self.assertTrue(os.path.isfile(self.test_zip_name))
        result = file_expander._is_file_exists(self.test_zip_name)
        self.assertTrue(result)

    def test_verify_valid_archive_contents(self):
        self.assertTrue(file_expander._verify_archive_contents(['test.csv', 'test.json']))

    def test_verify_missing_json(self):
        self.assertFalse(file_expander._verify_archive_contents(['test.csv']))

    def test_verify_missing_csv(self):
        self.assertFalse(file_expander._verify_archive_contents(['test.json']))

    def test_verify_missing_json_and_csv(self):
        self.assertFalse(file_expander._verify_archive_contents(['test.doc']))

    def test_verify_more_than_two_files(self):
        self.assertFalse(file_expander._verify_archive_contents(['test.json', 'test.csv', 'test2.csv']))

    def test_verify_two_files_with_missing_csv(self):
        self.assertFalse(file_expander._verify_archive_contents(['test.json', 'test.doc']))
