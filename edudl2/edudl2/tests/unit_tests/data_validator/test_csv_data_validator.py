import unittest
from edudl2.exceptions.errorcodes import ErrorCode
from edudl2.data_validator import csv_data_validator
import os
from edudl2.udl2.defaults import UDL2_DEFAULT_CONFIG_PATH_FILE
from edudl2.udl2_util.config_reader import read_ini_file
from edudl2.udl2.constants import Constants
from unittest.mock import patch


class TestCsvDataValidator(unittest.TestCase):

    def setUp(self):
        try:
            config_path = dict(os.environ)['UDL2_CONF']
        except Exception:
            config_path = UDL2_DEFAULT_CONFIG_PATH_FILE

        conf_tup = read_ini_file(config_path)
        self.conf = conf_tup[0]
        self.data_dir = os.path.join(os.path.dirname(__file__), "..", "..", "data")

        self.test_csv_field_list = {'RecordType', 'Period', 'PARCCOrganizationCodeTestingSchool', 'PARCCOrganizationCodeResponsibleSchool', 'StateAbbreviation', 'TestingDistrictIdentifier', 'TestingDistrictName', 'TestingSchoolIdentifier', 'TestingSchoolName', 'ResponsibleDistrictIdentifier', 'ResponsibleDistrictName', 'SchoolInstitutionIdentifier', 'OrganizationName', 'ShortNameOfInstitution', 'PARCCOrganizationType', 'AdministrativeFundingControl', 'ProgramType', 'CountyANSICode', 'EconomicResearchServiceRuralUrbanContinuumCode', 'PARCCStudentIdentifier', 'StateStudentIdentifier', 'LocalStudentIdentifier', 'FirstName', 'MiddleName', 'LastName', 'Sex', 'Birthdate', 'GradeLevelWhenAssessed', 'StudentGrowthPercentilePARCC', 'StudentGrowthPercentileState', 'StudentGrowthPercentileDistrict', 'HispanicorLatinoEthnicity', 'AmericanIndianorAlaskaNative', 'Asian', 'BlackorAfricanAmerican', 'NativeHawaiianorOtherPacificIslander', 'White', 'DemographicRaceTwoorMoreRaces', 'TitleIIILimitedEnglishProficientParticipationStatus', 'AssessmentAccommodation504', 'AssessmentAccommodationEnglishLearner', 'AssessmentAccommodationIndividualizedEducationalPlanIEP', 'EconomicDisadvantageStatus', 'MigrantStatus', 'EnglishLanguageLearnerEL', 'GiftedandTalented', 'StudentWithDisabilities', 'PrimaryDisabilityType', 'StateFillerField1', 'StateFillerField2', 'StateFillerField3', 'StateFillerField4', 'StateFillerField5', 'StateFillerField6', 'StateFillerField7', 'StateFillerField8', 'StateFillerField9', 'StateFillerField10', 'PBAAssessmentGuid', 'EOYAssessmentGuid', 'SummativeAssessmentGuid', 'AssessmentAcademicSubject', 'AssessmentSessionTestAdministratorIdentifier', 'AssessmentSessionLocation', 'StaffMemberIdentifier', 'ClassroomIdentifier', 'AssessmentRegistrationTestAttemptIdentifier', 'FormID', 'AssessmentSessionActualEndDateTime', 'AssessmentSessionActualStartDateTime', 'AssessmentYear', 'AdminCode', 'TestCode', 'RawScore', 'CSEM', 'ScaleScore', 'PerformanceLevel', 'ReadingScore', 'WritingScore', 'Subclaim1', 'Subclaim2', 'Subclaim3', 'Subclaim4', 'Subclaim5', 'ASLVideo', 'HumanReaderorHumanSignerforELAL', 'HumanReaderorHumanSignerforMathematics', 'BraillewithTactileGraphics', 'ClosedCaptioningforELAL', 'TexttoSpeechforMathematics', 'TexttoSpeechforELAL', 'AnswersRecordedinTestBook', 'BrailleResponse', 'MonitorTestResponse', 'CalculationDeviceandMathematicsTools', 'ELALConstructedResponse', 'ELALSelectedResponseorTechnologyEnhancedItems', 'MathematicsResponse', 'WordtoWordDictionaryEnglishNativeLanguage', 'WordPrediction', 'AnswerMasking', 'ColorContrast', 'LargePrint', 'TactileGraphics', 'ScreenReaderORotherAssistiveTechnologyATApplication', 'ClosedCaptioningforELAL', 'RefreshableBrailleDisplayforELAL', 'TranslationoftheMathematicsAssessmentinPaper', 'TranslationoftheMathematicsAssessmentOnline', 'TranslationoftheMathematicsAssessmentinTexttoSpeech', 'SeparateAlternateLocation', 'SmallTestingGroup', 'SpecializedEquipmentorFurniture', 'SpecifiedAreaorSetting', 'TimeOfDay', 'AdministrationDirectionsClarifiedinStudentsNativeLanguage', 'AdministrationDirectionsReadAloudinStudentsNativeLanguage', 'MathematicsResponseEL', 'ExtendedTime', 'AlternateRepresentationPaperTest', 'FrequentBreaks', 'RetestFlag', 'TotalTestItems', 'TotalTestItemsAttempted', 'TotalNumbersofItemsinunit1', 'unit1NumberofAttemptedItems', 'TotalNumbersofItemsinunit2', 'unit2NumberofAttemptedItems', 'TotalNumbersofItemsinunit3', 'unit3NumberofAttemptedItems', 'TotalNumbersofItemsinunit4', 'unit4NumberofAttemptedItems', 'TotalNumbersofItemsinunit5', 'unit5NumberofAttemptedItems', 'InvalidationCode', 'InvalidationReason', 'SummativeFlag', 'NotTestedCode', 'NotTestedReason', 'ReportSuppressionCode', 'ReportSuppressionAction', 'UserAgent'}

    def _setup_valid_mocks(self, csv_mock):
        csv_mock.return_value = self.test_csv_field_list

    def test_for_source_file_with_missing_required_data(self):
        validator = csv_data_validator.DoesSourceFileHaveValidData(
            Constants.LOAD_TYPE_ASMT_MATH,
            Constants.ASMT_MATH_REF_TABLE_SPRING_2015,
        )
        error_code_expected = ErrorCode.SRC_FILE_HAS_MISSING_REQUIRED_DATA
        results = [validator.execute(self.data_dir, 'missing_required_field.csv', 1)]

        self.assertEqual(len(results), 1)
        self.assertEqual(results[0][0], error_code_expected)

    def test_for_source_file_with_less_number_of_columns(self):
        validator = csv_data_validator.DoesSourceFileInExpectedFormat(
            None,
            Constants.ASMT_MATH_REF_TABLE_SPRING_2016,
            csv_fields=self.test_csv_field_list,
        )
        error_code_expected = ErrorCode.SRC_FILE_HAS_HEADERS_MISMATCH_EXPECTED_FORMAT
        results = [validator.execute(self.data_dir,
                                     'invalid_csv.csv', 1)]
        self.assertEqual(len(results), 1)
        self.assertEqual(results[0][0], error_code_expected)

    def test_for_source_file_with_mismatched_format(self):
        validator = csv_data_validator.DoesSourceFileInExpectedFormat(
            None,
            Constants.ASMT_MATH_REF_TABLE_SPRING_2016,
            csv_fields=self.test_csv_field_list,
        )
        error_code_expected = ErrorCode.SRC_FILE_HAS_HEADERS_MISMATCH_EXPECTED_FORMAT
        results = [validator.execute(self.data_dir,
                                     'missing_column.csv', 1)]
        self.assertEqual(len(results), 1)
        self.assertEqual(results[0][0], error_code_expected)

    def test_for_source_file_with_matching_columns(self):
        validator = csv_data_validator.DoesSourceFileInExpectedFormat(
            None,
            Constants.ASMT_MATH_REF_TABLE_SPRING_2016,
            csv_fields=self.test_csv_field_list)
        results = [validator.execute(self.data_dir,
                                     'test_data_latest/'
                                     'current_ela_summative_asmt.csv', 1)]
        self.assertEqual(len(results), 1)
        self.assertEqual(results[0][0], '0')

    @patch('edudl2.data_validator.csv_data_validator.get_source_column_values_from_ref_column_mapping')
    def test_multiple_errorcode(self, csv_mapping_mock):
        # test#11 --> test multiple errors in one csv file (Error: 3006, 3011 & 3019)
        self._setup_valid_mocks(csv_mapping_mock)

        multierror_list = [ErrorCode.SRC_FILE_HAS_HEADERS_MISMATCH_EXPECTED_FORMAT,
                           ErrorCode.SRC_FILE_HAS_MISSING_REQUIRED_DATA]
        errorcode_list = []
        expected_error_code = csv_data_validator.CsvValidator(
            Constants.LOAD_TYPE_ASMT_ELA,
            Constants.ASMT_MATH_REF_TABLE_SPRING_2015,
        ).execute(self.data_dir, "mismatch_column_missing_data.csv", 123)
        for i in range(len(expected_error_code)):
            errorcode_list.append(expected_error_code[i][0])
        print(errorcode_list)
        self.assertEqual(len(multierror_list), len(errorcode_list))
        self.assertEqual(set(multierror_list), set(errorcode_list), "Error codes are incorrect for duplicate headers and for data mismatch")
