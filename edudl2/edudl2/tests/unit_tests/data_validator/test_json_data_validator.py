from edudl2.exceptions.errorcodes import ErrorCode
import unittest
from edudl2.data_validator import json_data_validator
import os


class TestJsonValidator(unittest.TestCase):

    def setUp(self):
        self.data_dir = os.path.join(os.path.dirname(__file__), "..", "..", "data", "unittest")
        self.valid_file = 'summative_assessment.json'
        self.invalid_file = 'invalid_summative_assessment.json'
        self.invalid_structure = 'summative_assessment_bad_structure.json'

    def test_json_validate_against_schema(self):

        validator = json_data_validator.JsonValidator('asmt_data.math').validators[0]
        validator.schema_path = self.data_dir
        results = validator.execute(self.data_dir, self.valid_file, '00000000-0000-0000-000000000000')

        self.assertEqual(results[0], ErrorCode.STATUS_OK)

    def test_invalid_json_against_schema(self):

        validator = json_data_validator.JsonValidator('asmt_data.math').validators[0]
        validator.schema_path = self.data_dir
        results = validator.execute(self.data_dir, self.invalid_file, '00000000-0000-0000-000000000000')

        self.assertEqual(results[0], ErrorCode.SRC_JSON_INVALID_FORMAT)
