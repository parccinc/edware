from edudl2.exceptions.errorcodes import ErrorCode
from edudl2.data_validator import xml_validator
import unittest
import os


class TestXMLValidator(unittest.TestCase):

    """ Test case for XML Validator

    """

    def setUp(self):
        """ Setup the class

        """
        self.data_dir = os.path.join(
            os.path.dirname(__file__), "..", "..", "data", "unittest", "test_item_results")
        self.non_existent_file = 'blah.xml'
        self.valid_file = 'item_results_valid.xml'
        self.invalid_structure = 'item_results_invalid_structure.xml'
        self.invalid_format = 'item_results_invalid_format.xml'
        self.no_student_id = 'item_results_no_student_id.xml'
        self.no_item_result = 'item_results_no_item_result.xml'
        self.no_score = 'item_results_no_score.xml'
        self.no_registration_ref_id = 'item_results_no_registration_ref_id.xml'
        self.validator = xml_validator.XMLValidator('dummy')
        self.validator.default_schema_file = os.path.join(
            self.data_dir, 'item_results.xsd')

    def test_non_existent_file(self):
        """ Test if a file doesn't exist

        """
        results = self.validator.execute(
            self.data_dir, self.non_existent_file, '00000000-0000-0000-000000000000')
        self.assertEqual(results[0][0], ErrorCode.SRC_XML_INVALID_PATH)

    def test_valid_xml(self):
        """ Test a valid xml file

        """
        results = self.validator.execute(
            self.data_dir, self.valid_file, '00000000-0000-0000-000000000000')
        self.assertEqual(len(results), 0)

    def test_invalid_structure(self):
        """ Test an invalid xml file structure

        """
        results = self.validator.execute(
            self.data_dir, self.invalid_structure, '00000000-0000-0000-000000000000')
        self.assertEqual(results[0][0], ErrorCode.SRC_XML_INVALID_STRUCTURE)

    def test_invalid_format(self):
        """ Test an invalid format. Meaning it does not match with the schema

        """
        results = self.validator.execute(
            self.data_dir, self.invalid_format, '00000000-0000-0000-000000000000')
        self.assertEqual(results[0][0], ErrorCode.SRC_XML_INVALID_FORMAT)

    def test_missing_reportable_fields(self):
        """ Test for missing reportable fields

        """
        results = self.validator.execute(
            self.data_dir, self.no_student_id, '00000000-0000-0000-000000000000')
        self.assertEqual(results[0][0], ErrorCode.SRC_XML_MISSING_DATA)

        results = self.validator.execute(
            self.data_dir, self.no_item_result, '00000000-0000-0000-000000000000')
        self.assertEqual(results[0][0], ErrorCode.SRC_XML_MISSING_DATA)

        results = self.validator.execute(
            self.data_dir, self.no_score, '00000000-0000-0000-000000000000')
        self.assertEqual(results[0][0], ErrorCode.SRC_XML_MISSING_DATA)

        results = self.validator.execute(
            self.data_dir, self.no_registration_ref_id, '00000000-0000-0000-000000000000')
        self.assertEqual(results[0][0], ErrorCode.SRC_XML_MISSING_DATA)
