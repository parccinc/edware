import unittest
from edudl2.udl2.constants import Constants

__author__ = 'sravi'


class TestConstants(unittest.TestCase):

    def test_all_udl_constants(self):

        # loader tables
        self.assertEqual(Constants.LDR_MATH_SUM, 'ldr_math_sum')
        self.assertEqual(Constants.LDR_ELA_SUM, 'ldr_ela_sum')

        # staging tables
        self.assertEqual(Constants.STG_MATH_SUM, 'stg_math_sum')
        self.assertEqual(Constants.STG_ELA_SUM, 'stg_ela_sum')

        # other tables
        self.assertEqual(Constants.UDL2_BATCH_TABLE, 'udl_batch')
        self.assertEqual(Constants.ASMT_MATH_REF_TABLE, 'math_sum_ref_column_mapping')
        self.assertEqual(Constants.ASMT_ELA_REF_TABLE, 'ela_sum_ref_column_mapping')
        self.assertEqual(Constants.UDL2_ERR_LIST_TABLE, 'err_list')
        self.assertEqual(Constants.UDL2_CSV_LZ_TABLE, 'lz_csv')
        self.assertEqual(Constants.UDL2_JSON_LZ_TABLE, 'lz_json')
        self.assertEqual(Constants.UDL2_FDW_SERVER, 'udl2_fdw_server')

        # column values
        self.assertEqual(Constants.OP_COLUMN_NAME, 'op')

        # load types
        self.assertEqual(Constants.LOAD_TYPE_KEY, 'DataFileType')
        self.assertEqual(Constants.LOAD_TYPE_ASSESSMENT, 'asmt_data')

        self.assertEqual(Constants.SEQUENCE_NAME, 'global_rec_seq')

    def test_all_lambda_constants(self):
        self.assertEqual(len(Constants.LOAD_TYPES()), 28)
        self.assertEqual(Constants.LOAD_TYPES(), [Constants.LOAD_TYPE_ASSESSMENT, Constants.LOAD_TYPE_ITEM, Constants.LOAD_TYPE_PSYCHOMETRIC,
                                                  Constants.LOAD_TYPE_MYA_ELA_ITEM, Constants.LOAD_TYPE_MYA_MATH_ITEM,
                                                  Constants.LOAD_TYPE_MYA_ASMT_ELA, Constants.LOAD_TYPE_MYA_ASMT_MATH,
                                                  Constants.LOAD_TYPE_SNL_ASMT, Constants.LOAD_TYPE_SNL_STUDENT_TASK,
                                                  Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_READING_COMP, Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_VOCAB,
                                                  Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_DECODING,
                                                  Constants.LOAD_TYPE_DIAGNOSTIC_ELA_VOCAB,
                                                  Constants.LOAD_TYPE_DIAGNOSTIC_ELA_COMP,
                                                  Constants.LOAD_TYPE_DIAGNOSTIC_ELA_DECODING,
                                                  Constants.LOAD_TYPE_DIAGNOSTIC_ELA_WRITING,
                                                  Constants.LOAD_TYPE_DIAGNOSTIC_ELA_READ_FLU,
                                                  Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_PROGRESSION,
                                                  Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_COMPREHENSION,
                                                  Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_FLUENCY,
                                                  Constants.LOAD_TYPE_DIAGNOSTIC_MATH_LOCATOR,
                                                  Constants.LOAD_TYPE_DIAGNOSTIC_MATH_PROGRESSION,
                                                  Constants.LOAD_TYPE_DIAGNOSTIC_MATH_FLUENCY,
                                                  Constants.LOAD_TYPE_DIAGNOSTIC_MATH_GRADE_LEVEL,
                                                  Constants.LOAD_TYPE_DIAGNOSTIC_MATH_CLUSTER,
                                                  Constants.LOAD_TYPE_READER_MOTIVATION,
                                                  Constants.LOAD_TYPE_ELA_ITEM, Constants.LOAD_TYPE_MATH_ITEM])
        self.assertEqual(Constants.UDL2_LOADER_TABLE(Constants.LOAD_TYPE_ASMT_MATH), Constants.LDR_MATH_SUM)
        self.assertEqual(Constants.UDL2_LOADER_TABLE(Constants.LOAD_TYPE_ASMT_ELA), Constants.LDR_ELA_SUM)
        self.assertEqual(Constants.UDL2_STAGING_TABLE(Constants.LOAD_TYPE_ASMT_MATH), Constants.STG_MATH_SUM)
        self.assertEqual(Constants.UDL2_STAGING_TABLE(Constants.LOAD_TYPE_ASMT_ELA), Constants.STG_ELA_SUM)
        self.assertEqual(
            Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_ASMT_MATH),
            (
                Constants.ASMT_MATH_REF_TABLE, Constants.ASMT_MATH_REF_TABLE_SPRING_2015,
                Constants.ASMT_MATH_REF_TABLE_FALL_2015, Constants.ASMT_MATH_REF_TABLE_SPRING_2016,
            ),
        )
        self.assertEqual(
            Constants.UDL2_REF_MAPPING_TABLE(Constants.LOAD_TYPE_ASMT_ELA),
            (
                Constants.ASMT_ELA_REF_TABLE, Constants.ASMT_ELA_REF_TABLE_SPRING_2015,
                Constants.ASMT_ELA_REF_TABLE_FALL_2015, Constants.ASMT_ELA_REF_TABLE_SPRING_2016,
            ),
        )
        self.assertEqual(Constants.TENANT_SEQUENCE_NAME('cat'), Constants.SEQUENCE_NAME + '_' + 'cat')
        self.assertEqual(Constants.TENANT_SEQUENCE_NAME(''), None)
        self.assertEqual(Constants.TENANT_SEQUENCE_NAME(None), None)
