'''
Created on Mar 28, 2014

@author: dip
'''
import os
from edschema.tests.database.utils.unittest_with_sqlite import Unittest_with_sqlite
from edudl2.database.metadata.udl2_metadata import generate_udl2_metadata
from edudl2.database.udl2_connector import TARGET_NAMESPACE,\
    PRODUCTION_NAMESPACE, UDL2DBConnection, UDL_NAMESPACE
from edschema.metadata.ed_metadata import generate_ed_metadata
from sqlalchemy.schema import MetaData, Sequence, Index
from sqlalchemy import Table, Column, text
from sqlalchemy.types import Text, Boolean, TIMESTAMP, Interval, TIME
from sqlalchemy.types import BigInteger, SmallInteger, String, DateTime
from sqlalchemy.dialects.sqlite import BLOB


class Unittest_with_udl2_sqlite(Unittest_with_sqlite):
    @classmethod
    def setUpClass(cls, force_foreign_keys=True, setup_udl=True):
        super().setUpClass(datasource_name=TARGET_NAMESPACE + '.' + get_unittest_tenant_name(), metadata=generate_edware_test_metadata(), use_metadata_from_db=False,
                           resources_dir=None, force_foreign_keys=force_foreign_keys)
        super().setUpClass(datasource_name=PRODUCTION_NAMESPACE + '.' + get_unittest_tenant_name(), metadata=generate_edware_test_metadata(), use_metadata_from_db=False,
                           resources_dir=None, force_foreign_keys=force_foreign_keys)
        if setup_udl:
            here = os.path.abspath(os.path.dirname(__file__))
            resources_dir = os.path.abspath(os.path.join(os.path.join(here, 'resources')))
            super().setUpClass(datasource_name=UDL_NAMESPACE + '.' + get_unittest_tenant_name(), metadata=generate_test_metadata(), resources_dir=resources_dir,
                               force_foreign_keys=force_foreign_keys)

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()


class UnittestIntegrationDBConnection(UDL2DBConnection):
    def __init__(self):
        super().__init__(namespace=TARGET_NAMESPACE, tenant=get_unittest_tenant_name())


class UnittestLoaderDBConnection(UDL2DBConnection):
    def __init__(self):
        super().__init__(namespace=UDL_NAMESPACE, tenant=get_unittest_tenant_name())


def get_unittest_tenant_name():
    return 'edware'


def get_unittest_schema_name():
    '''
    It's important that schema name is none for target connection as sqlite doesn't have concept of schema
    '''
    return None


def generate_edware_test_metadata():
    # TODO: not testing creation of schema?
    metadata = MetaData(schema=None, bind=None)
    int_ela_sum = Table('int_ela_sum', metadata,
                        Column('batch_guid', String(50), nullable=False),
                        Column('record_num', BigInteger, primary_key=True, autoincrement=True, nullable=False),
                        Column('state_code', String(30), nullable=True),
                        Column('district_name', String(150), nullable=True),
                        Column('student_first_name', String(150), nullable=True),
                        Column('student_last_name', String(150), nullable=True),
                        )

    int_file_store = Table('int_file_store', metadata,
                           Column('admin_guid', String(50), nullable=False, primary_key=True),
                           Column('asmt_guid', String(50), nullable=False, primary_key=True),
                           Column('page_no', BigInteger, nullable=False, primary_key=True),
                           Column('file_type', String(10), nullable=False),
                           Column('file_name', String(150), nullable=False),
                           Column('file_data', BLOB, nullable=False),
                           Column('batch_guid', String(50), nullable=False),
                           Column('create_date', TIMESTAMP(timezone=True), nullable=False)
                           )
    return metadata


def generate_test_metadata():
        metadata = MetaData(schema=None, bind=None)

        ldr_ela_sum = Table('stg_asmt', metadata,
                            Column('batch_guid', String(50), nullable=False),
                            Column('record_num', BigInteger, primary_key=True, autoincrement=True, nullable=False),
                            Column('data_file_name', String(30), nullable=True),
                            Column('data_file_type', String(150), nullable=True),
                            Column('asmt_code', String(150), nullable=True),
                            Column('asmt_name', String(150), nullable=True),
                            Column('asmt_score_max', SmallInteger, nullable=True))

        ldr_ela_sum = Table('ldr_ela_sum', metadata,
                            Column('batch_guid', String(50), nullable=False),
                            Column('record_num', BigInteger, primary_key=True, autoincrement=True, nullable=False),
                            Column('state_code', String(30), nullable=True),
                            Column('district_name', String(150), nullable=True),
                            Column('student_first_name', String(150), nullable=True),
                            Column('student_last_name', String(150), nullable=True),
                            )
        stg_ela_sum = Table('stg_ela_sum', metadata,
                            Column('batch_guid', String(50), nullable=False),
                            Column('record_num', BigInteger, primary_key=True, autoincrement=True, nullable=False),
                            Column('state_code', String(30), nullable=True),
                            Column('district_name', String(150), nullable=True),
                            Column('student_first_name', String(150), nullable=True),
                            Column('student_last_name', String(150), nullable=True),
                            )
        err_list = Table('err_list', metadata,
                         Column('record_sid', String(50), nullable=False),
                         )
        ela_sum_ref_column_mapping = Table('ela_sum_ref_column_mapping', metadata,
                                           Column('column_map_key', BigInteger, primary_key=True),
                                           Column('phase', SmallInteger, nullable=True),
                                           Column('source_table', String(50), nullable=False),
                                           Column('source_column', String(256), nullable=True),
                                           Column('target_table', String(50), nullable=True),
                                           Column('target_column', String(50), nullable=True),
                                           Column('transformation_rule', String(50), nullable=True),
                                           Column('stored_proc_name', String(256), nullable=True),
                                           Column('stored_proc_created_date', TIMESTAMP(timezone=True), nullable=True),
                                           Column('create_date', TIMESTAMP(timezone=True), nullable=True),
                                           )
        return metadata
