from unittest.mock import patch
from edudl2.tests.unit_tests.unittest_with_udl2_sqlite import Unittest_with_udl2_sqlite, \
    UnittestLoaderDBConnection
from edudl2.fileloader import csv_loader
from edudl2.udl2.constants import Constants
from edudl2.udl2 import message_keys as mk
import uuid
import os

__author__ = 'npandey'


class TestFileLoader(Unittest_with_udl2_sqlite):

    @classmethod
    def setUpClass(cls):
        super().setUpClass(force_foreign_keys=False)

    def setUp(self):
        self.data_dir = os.path.join(os.path.dirname(__file__), "..", "..", "data")
        self.batch_guid = str(uuid.uuid4())
        self.test_target_columns = ['batch_guid', 'record_num', 'state_code', 'district_name', 'student_first_name', 'student_last_name']
        self.test_source_columns = ['StateAbbreviation', 'ResponsibleDistrictName', 'FirstName', 'LastOrSurname']
        self.test_transformation_rules = ['', '', None, None, None, None]
        self.ref_table_name = 'ela_sum_ref_column_mapping'
        self.lz_csv_table = 'lz_csv'
        self.loader_table = 'ldr_ela_sum'

    def test_get_fields_map(self):
        with UnittestLoaderDBConnection() as conn:
            loader_table_columns, csv_table_columns, transformation_rules = csv_loader.get_fields_map(conn, self.ref_table_name, self.lz_csv_table, self.batch_guid)

        for loader_column in self.test_target_columns:
            self.assertTrue(loader_column in loader_table_columns, "Loader table {column} not found".format(column=loader_column))

        for csv_column in self.test_source_columns:
            self.assertTrue(csv_column in csv_table_columns, "Csv field {column} not found".format(column=csv_column))

        self.assertEqual(transformation_rules, self.test_transformation_rules, "Transformation rules don't match")

    def test_extract_csv_header(self):
        expected_header_names = ['stateabbreviation', 'responsibledistrictname', 'firstname', 'lastorsurname']
        expected_header_types = ['text', 'text', 'text', 'text']

        with UnittestLoaderDBConnection() as conn:
            header_names, header_types = csv_loader.extract_csv_header(conn, self.ref_table_name, self.lz_csv_table,
                                                                       os.path.join(self.data_dir, 'unittest', 'test_ela_sum_headers.csv'))

        self.assertEqual(expected_header_names, header_names, "Header names do not match")
        self.assertEqual(expected_header_types, header_types, "Header types do not match")

    @patch('edudl2.fileloader.prepare_queries.create_sequence_query')
    @patch('edudl2.fileloader.prepare_queries.create_inserting_into_loader_query')
    @patch('edudl2.fileloader.prepare_queries.drop_sequence_query')
    @patch('edudl2.fileloader.csv_loader.execute_udl_queries')
    def test_import_via_fdw(self, mock_exec_query, mock_drop_seq_query, mock_query_insert_loader_query, mock_create_seq_query):
        mock_exec_query.return_value = None
        mock_drop_seq_query.return_value = 'query3'
        mock_query_insert_loader_query.return_value = 'query2'
        mock_create_seq_query.return_value = 'query1'

        seq_name = (self.lz_csv_table + '_' + str(1)).lower()
        loader_schema = 'ldrSchema'
        csv_schema = 'csvSchema'
        tenant_name = 'testTenant'

        with UnittestLoaderDBConnection() as conn:
            csv_loader.import_via_fdw(conn, self.test_target_columns, self.test_source_columns,
                                      self.test_transformation_rules, True, loader_schema,
                                      self.loader_table, csv_schema, self.lz_csv_table, 1, tenant_name)

        mock_create_seq_query.assert_called_once_with(loader_schema, seq_name, 1)
        mock_query_insert_loader_query.assert_called_once_with(self.test_target_columns, True,
                                                               self.test_source_columns, loader_schema,
                                                               self.loader_table, csv_schema, self.lz_csv_table,
                                                               seq_name, Constants.TENANT_SEQUENCE_NAME(tenant_name),
                                                               self.test_transformation_rules)
        mock_drop_seq_query.assert_called_once_with(loader_schema, seq_name)
        mock_exec_query.assert_called_once_with(conn, ['query1', 'query2', 'query3'], 'Exception in loading data -- ',
                                                'file_loader', 'import_via_fdw')

    @patch('edudl2.fileloader.csv_loader.extract_csv_header')
    @patch('edudl2.fileloader.csv_loader.create_fdw_tables')
    @patch('edudl2.fileloader.csv_loader.get_fields_map')
    @patch('edudl2.fileloader.csv_loader.import_via_fdw')
    @patch('edudl2.fileloader.csv_loader.drop_fdw_tables')
    def test_load_data_process(self, mock_drop_fdw, mock_import_via_fdw, mock_get_fields_map, mock_create_fdw, mock_extract_header):
        mock_drop_fdw.return_value = None
        mock_import_via_fdw.return_value = None
        mock_get_fields_map.return_value = (None, None, None)
        mock_create_fdw.return_value = None
        mock_extract_header.return_value = (None, None)

        loader_schema = 'ldrSchema'
        csv_schema = 'csvSchema'
        tenant_name = 'testTenant'
        header_types = ['text', 'text', 'text', 'text']

        conf = {mk.FILE_TO_LOAD: 'mock_file',
                mk.ROW_START: 1,
                mk.HEADERS: self.test_source_columns,
                mk.CSV_SCHEMA: csv_schema,
                mk.CSV_TABLE: self.lz_csv_table,
                mk.FDW_SERVER: Constants.UDL2_FDW_SERVER,
                mk.TARGET_DB_SCHEMA: loader_schema,
                mk.TARGET_DB_TABLE: Constants.UDL2_LOADER_TABLE("ELA"),
                mk.APPLY_RULES: True,
                mk.REF_TABLE: Constants.UDL2_REF_MAPPING_TABLE("ELA"),
                mk.CSV_LZ_TABLE: Constants.UDL2_CSV_LZ_TABLE,
                mk.GUID_BATCH: self.batch_guid,
                mk.TENANT_NAME: tenant_name}

        with UnittestLoaderDBConnection() as conn:
            csv_loader.load_data_process(conn, conf)

        mock_extract_header.assert_called_with_once(conn, self.ref_table_name, self.lz_csv_table,
                                                    os.path.join(self.data_dir, 'unittest', 'test_ela_sum_headers.csv'))

        mock_create_fdw.assert_called_with_once(conn, self.test_source_columns, header_types, conf.get(mk.FILE_TO_LOAD),
                                                conf.get(mk.CSV_SCHEMA), conf.get(mk.CSV_TABLE), conf.get(mk.FDW_SERVER))

        mock_get_fields_map.assert_called_with_once(conn, self.ref_table_name, self.lz_csv_table, self.batch_guid)

        mock_import_via_fdw.assert_called_with_once(conn, self.test_target_columns, self.test_source_columns,
                                                    self.test_transformation_rules, True, loader_schema,
                                                    self.loader_table, csv_schema, self.lz_csv_table, 1, tenant_name)

        mock_drop_fdw.assert_called_with_once(conn, conf.get(mk.CSV_SCHEMA), conf.get(mk.CSV_TABLE))
