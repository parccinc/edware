from edudl2.fileloader.value_builder import ValueBuilder
from edudl2.tests.unit_tests.unittest_with_udl2_sqlite import Unittest_with_udl2_sqlite, UnittestLoaderDBConnection

__author__ = 'ablum'


class TestValueBuilder(Unittest_with_udl2_sqlite):

    def test_build(self):
        test_dict = {'DataFileName': 'BOY_ELA_Sum_2015_CA_State',
                     'DataFileType': 'asmt_data',
                     'score_max': 1600}

        test_column_mapping_info = [{'target_column': 'data_file_name', 'source_column': 'DataFileName', 'stored_proc_name': 'sp_clean'},
                                    {'target_column': 'data_file_type', 'source_column': 'DataFileType', 'stored_proc_name': 'substr({value}, 1, {length})'},
                                    {'target_column': 'asmt_score_max', 'source_column': 'score_max', 'stored_proc_name': "to_number({value},'9999')"}]

        value_builder = ValueBuilder('00000-00000-00', 'edware', 'udl2', test_column_mapping_info)
        with UnittestLoaderDBConnection() as conn:
            s_stg_table = conn.get_table('stg_asmt')
            field_to_value_mappings = value_builder.build(test_dict, s_stg_table)

        self.assertEqual(5, len(field_to_value_mappings.keys()))
        self.assertIn('record_num', field_to_value_mappings)
        self.assertIn('batch_guid', field_to_value_mappings)
        self.assertIn('data_file_name', field_to_value_mappings)
        self.assertIn('data_file_type', field_to_value_mappings)
        self.assertEqual("sp_clean('BOY_ELA_Sum_2015_CA_State')", str(field_to_value_mappings['data_file_name']))
        self.assertEqual("substr('asmt_data', 1, 150)", str(field_to_value_mappings['data_file_type']))
        self.assertEqual("to_number('1600','9999')", str(field_to_value_mappings['asmt_score_max']))

    def test_build_negative(self):
        test_dict = {'missing_from_table': 'missing_from_table',
                     'DataFileType': None,
                     'missing_from_mapping': 'missing_from_mapping'}

        test_column_mapping_info = [{'target_column': 'missing_from_table', 'source_column': 'missing_from_table', 'stored_proc_name': 'sp_clean'},
                                    {'target_column': 'data_file_type', 'source_column': 'DataFileType', 'stored_proc_name': 'substr({value}, 1, {length})'}]

        value_builder = ValueBuilder('00000-00000-00', 'edware', 'udl2', test_column_mapping_info)
        with UnittestLoaderDBConnection() as conn:
            s_stg_table = conn.get_table('stg_asmt')
            field_to_value_mappings = value_builder.build(test_dict, s_stg_table)

        self.assertNotIn('missing_from_table', field_to_value_mappings)
        self.assertNotIn('missing_from_mapping', field_to_value_mappings)
        self.assertIn('data_file_type', field_to_value_mappings)
        self.assertEqual(None, field_to_value_mappings['data_file_type'])
