import unittest
import os
from edudl2.fileloader.summative_item_result_parser import SummativeItemResultParser

__author__ = 'npandey'


class TestSummativeItemResultParser(unittest.TestCase):

    def setUp(self):
        self.file_name = 'summ_item_res.xml'
        self.neg_file_name = 'summ_item_neg_res.xml'
        self.data_dir = os.path.join(os.path.dirname(__file__), "..", "..", "data", "unittest")
        self.test_file = os.path.join(self.data_dir, self.file_name)
        self.neg_test_file = os.path.join(self.data_dir, self.neg_file_name)
        self.batch_size = 1000

    def test_parse_xml_to_data(self):
        gen = SummativeItemResultParser(self.batch_size).parse_xml_to_data(self.test_file)
        for result in gen:
            self.assertEqual(len(result), 2)
            self.assertEqual(len(result[0]), 4)
            self.assertEqual(result[0]["student_item_score"], "75")
            self.assertEqual(result[0]["asmt_attempt_guid"], "b47319fa-6f7d-18a3-dbff-21006df00001")
            self.assertEqual(result[0]["student_guid"], "a54926fc-6f7d-18a3-dbff-21006df00001")
            self.assertEqual(result[0]["item_guid"], "M40109")

            self.assertEqual(result[1]["student_item_score"], "1")
            self.assertEqual(result[1]["asmt_attempt_guid"], "b47319fa-6f7d-18a3-dbff-21006df00002")
            self.assertEqual(result[1]["student_guid"], "a54926fc-6f7d-18a3-dbff-21006df00002")
            self.assertEqual(result[1]["item_guid"], "M40110")

    def test_neg_parse_xml_to_data(self):
        gen = SummativeItemResultParser(self.batch_size).parse_xml_to_data(self.neg_test_file)
        for result in gen:
            self.assertEqual(len(result), 2)
            self.assertEqual(len(result[0]), 4)
            self.assertEqual(result[0]["student_item_score"], "7")
            self.assertEqual(result[0]["asmt_attempt_guid"], "123sdf-7489wedf-123sd-refd457")
            self.assertEqual(result[0]["student_guid"], "6788ea64-434c-11e4-b4d6-14109fe04331")
            self.assertEqual(result[0]["item_guid"], "1db8bc94-2915-4ff7-acbe-65376ebffb13")

            self.assertEqual(result[1]["student_item_score"], "50")
            self.assertEqual(result[0]["asmt_attempt_guid"], "123sdf-7489wedf-123sd-refd457")
            self.assertEqual(result[0]["student_guid"], "6788ea64-434c-11e4-b4d6-14109fe04331")
            self.assertEqual(result[1]["item_guid"], "05ef64b3-0d96-4cf9-ba4d-655789dae900")
