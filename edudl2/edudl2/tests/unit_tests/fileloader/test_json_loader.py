'''
Created on May 17, 2013

@author: swimberly
'''

import unittest
from requests.structures import CaseInsensitiveDict
from edudl2.fileloader.json_loader import fix_empty_strings, flatten_fields
import tempfile
import shutil
from edudl2.tests.unit_tests.unittest_with_udl2_sqlite import Unittest_with_udl2_sqlite


class TestJsonLoader(Unittest_with_udl2_sqlite):

    def setUp(self):
        self.json_dict = {'pls': {'pl1': {'cp': '1200', 'name': 'Minimal', 'level': 1}, 'pl2': {'cp': '1400', 'name': 'Partial', 'level': 2},
                                  'pl3': {'cp': '1800', 'name': 'Adequate', 'level': 3}},
                          'id': {'year': '2015', 'id': '28', 'subject': 'Math', 'period': '2015', 'type': 'SUMMATIVE', 'version': 'V1'}
                          }
        self.mappings = {'val1': ['pls', 'pl1', 'name'], 'val2': ['pls', 'pl2', 'name'], 'val3': ['pls', 'pl3', 'name'], 'val4': ['pls', 'pl2', 'level'],
                         'val5': ['id', 'year'], 'val6': ['id', 'type'], 'val7': ['pls', 'pl3', 'cp'], 'val8': ['pls', 'pl1', 'level']
                         }
        self.temp_dir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.temp_dir, ignore_errors=True)

    def test_fix_empty_strings_1(self):
        ''' check method with no empty strings in dict '''
        data_dict = {'a': '1', 'b': '2', 'c': '3'}
        res_dict = fix_empty_strings(data_dict)
        self.assertEqual(data_dict, res_dict, 'no changes should have been made to the dict')

    def test_fix_empty_string_2(self):
        ''' check with some empty strings '''
        data_dict = {'a': '', 'b': '2', 'c': "", 'd': 0, 'e': '0'}
        expected = {'a': None, 'b': '2', 'c': None, 'd': 0, 'e': '0'}
        res = fix_empty_strings(data_dict)
        self.assertEqual(res, expected, 'Check with a few empty strings')

    def test_flatten_fields(self):
        test_data = {'asmt_perf': [{'level': '1', 'code': '1000'},
                                   {'level': '2', 'code': '1001'},
                                   {'level': '3', 'code': '1002'}],
                     'claims': CaseInsensitiveDict({
                         'asmt_claims': [{'subscore': '1'},
                                         {'subscore': '2'}],
                         'asmt_subclaims': [{'subscore_subclaim': '1'},
                                            {'subscore_subclaim': '2'}]
                     })}
        flatten_fields(test_data)
        self.assertEqual('1', test_data['asmt_perf[1].level'])
        self.assertEqual('2', test_data['asmt_perf[2].level'])
        self.assertEqual('3', test_data['asmt_perf[3].level'])
        print(test_data)
        self.assertEqual('1', test_data['claims.asmt_claims[1].subscore'])
        self.assertEqual('2', test_data['claims.asmt_claims[2].subscore'])
        self.assertEqual('1', test_data['claims.asmt_subclaims[1].subscore_subclaim'])
        self.assertEqual('2', test_data['claims.asmt_subclaims[2].subscore_subclaim'])


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
