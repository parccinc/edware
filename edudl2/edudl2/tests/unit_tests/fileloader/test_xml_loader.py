import unittest
from edudl2.fileloader import xml_loader
from unittest.mock import patch
from edudl2.udl2 import message_keys as mk
from unittest.mock import ANY
from mock import MagicMock
from edudl2.fileloader.summative_item_result_parser import SummativeItemResultParser

__author__ = 'npandey'


class TestXMLLoader(unittest.TestCase):

    def setUp(self):

        self.parser_result = [{'item_guid': 'I111', 'student_guid': 'S111', 'asmt_attempt_guid': 'A111', 'student_item_score': '11'},
                              {'item_guid': 'I222', 'student_guid': 'S222', 'asmt_attempt_guid': 'A222', 'student_item_score': '22'}]

        self.test_column_mapping_info = [{'target_column': 'asmt_attempt_guid', 'source_column': 'asmt_attempt_guid', 'stored_proc_name': 'sp_clean'},
                                         {'target_column': 'item_guid', 'source_column': 'item_guid', 'stored_proc_name': 'sp_clean'},
                                         {'target_column': 'student_guid', 'source_column': 'student_guid', 'stored_proc_name': 'sp_clean'}]

        self.batch_guid = 'guid_batch_123'
        self.tenant_name = 'test_tenant'
        self.target_schema = 'udl_schema'
        self.batch_size = 1000

        self.conf = {mk.GUID_BATCH: self.batch_guid,
                     mk.TENANT_NAME: self.tenant_name,
                     mk.FILE_TO_LOAD: 'testfile',
                     mk.TARGET_DB_TABLE: 'ldr_table',
                     mk.TARGET_DB_SCHEMA: self.target_schema,
                     mk.REF_TABLE: 'ref_table'
                     }

    @patch('edudl2.fileloader.xml_loader.SummativeItemResultParser')
    @patch('edudl2.fileloader.xml_loader.load_to_table')
    def test_load_xml(self, load_patch, parser_patch):
        load_patch.return_value = 1

        parser_mock = SummativeItemResultParser(1000)
        parser_mock.parse_xml_to_data = MagicMock()
        parser_mock.parse_xml_to_data.return_value = iter(self.parser_result)
        parser_mock.parse_xml_to_data.__iter__.side_effect = lambda: iter(self.parser_result)

        parser_patch.return_value = parser_mock

        result = xml_loader.load_xml(self.conf, self.batch_size)

        load_patch.assert_called_with_twice(ANY, self.conf.get(mk.GUID_BATCH),
                                            self.conf.get(mk.TARGET_DB_TABLE), self.conf.get(mk.TENANT_NAME),
                                            self.conf.get(mk.TARGET_DB_SCHEMA), self.conf.get(mk.REF_TABLE))

        self.assertEqual(2, result)

    def test_build_table_row(self):
        ldr_table_mock = MagicMock()
        ldr_table_mock.columns = ['asmt_attempt_guid', 'item_guid', 'student_guid']

        result = xml_loader.build_table_row(self.parser_result[0], ldr_table_mock, self.test_column_mapping_info, self.target_schema, self.batch_guid, self.tenant_name)
        self.assertEqual(len(result), 5)
        self.assertEqual(result["batch_guid"], self.batch_guid)
        self.assertTrue("record_num" in result)
        self.assertTrue("item_guid" in result)
        self.assertTrue("student_guid" in result)
        self.assertTrue("asmt_attempt_guid" in result)
