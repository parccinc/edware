from edudl2.tests.unit_tests.unittest_with_udl2_sqlite import (
    Unittest_with_udl2_sqlite,
    UnittestIntegrationDBConnection
)
from unittest.mock import patch
from sqlalchemy import cast, BLOB, select, Table, func
from edudl2.fileloader import content_loader
from edudl2.udl2.constants import Constants
from edudl2.udl2 import message_keys as mk
from edudl2.tests.unit_tests.unittest_with_udl2_sqlite import generate_edware_test_metadata
import os


class TestFileContentLoader(Unittest_with_udl2_sqlite):

    @classmethod
    def setUpClass(cls):
        super().setUpClass(force_foreign_keys=False)

    def setUp(self):
        self.guid_batch = '912ab535-fd3f-4fae-86f3-aaa3bb6ed85c'
        self.tenant = 'cat'
        self.data_dir = os.path.join(
            os.path.dirname(__file__), "..", "..", "data", "unittest")
        self.test_file_1 = os.path.join(self.data_dir, 'multiple_assessment_results.xml')
        self.test_file_2 = os.path.join(self.data_dir, 'summative_assessment.json')
        self.conf = {mk.GUID_BATCH: self.guid_batch,
                     mk.TARGET_DB_TABLE: Constants.UDL2_XML_FILE_TABLE,
                     mk.TARGET_DB_SCHEMA: self.guid_batch,
                     mk.TENANT_NAME: self.tenant,
                     }
        self.identification_data = {}
        self.identification_data["admin_guid"] = "temp"
        self.identification_data["asmt_guid"] = "temp"
        self.table_name = 'int_file_store'
        self.metadata = generate_edware_test_metadata()
        self.table = Table(self.table_name, self.metadata)

    @patch('edudl2.fileloader.content_loader.cast')
    def test_content_load(self, mock_cast):

        # SQLite and Postgres don't use the same type for storing binary. Use
        # BLOB instead of BYTEA.
        mock_cast.return_value = cast('some data', BLOB)

        with UnittestIntegrationDBConnection() as conn:
            content_loader.move_content(self.test_file_1, self.conf, self.identification_data, conn)
            row_count = select([func.count()]).select_from(self.table)

            self.assertEqual(conn.execute(row_count).fetchall()[0][0], 1)

        self.identification_data["asmt_guid"] = "-1"

        with UnittestIntegrationDBConnection() as conn:
            content_loader.move_content(self.test_file_2, self.conf, self.identification_data, conn)
            row_count = select([func.count()]).select_from(self.table)

            #verify a second row has been added
            self.assertEqual(conn.execute(row_count).fetchall()[0][0], 2)
