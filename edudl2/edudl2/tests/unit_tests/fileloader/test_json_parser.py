import os
import unittest
from edudl2.fileloader import json_parser
from edudl2.fileloader.json_parser import JsonParser
from edudl2.udl2.constants import Constants

__author__ = 'npandey'


class TestJsonParser(unittest.TestCase):

    def setUp(self):
        self.file_name = 'summative_assessment.json'
        self.data_dir = os.path.join(os.path.dirname(__file__), "..", "..", "data", "unittest")
        self.test_file = os.path.join(self.data_dir, self.file_name)
        self.load_type = Constants.LOAD_TYPE_ASMT_ELA

    def test_loader(self):
        result = json_parser.JsonParser(load_type=self.load_type).parse_json(self.test_file)

        self.assertEqual(len(result), 2)
        print(result)
        test_level_content = result[Constants.STG_TEST_LEVEL]
        self.assertIsNotNone(test_level_content)
        self.assertEqual(len(test_level_content[0]), 5)

        test_score_content = result[Constants.STG_TEST_SCORE]
        self.assertIsNotNone(test_score_content)
        self.assertEqual(len(test_score_content), 1)
        self.check_value_exists(test_score_content, "test_code", "QA-Test-12345678912-")

    def test_process_singular_list(self):
        parser = JsonParser(load_type=Constants.LOAD_TYPE_ASMT_MATH)
        test_data = [3, 4, 5]

        parser.initialize_data_map(Constants.STG_ASMT_LVL)
        parser.process_singular_list(test_data, 'parent_guid', Constants.STG_ASMT_LVL)

        to_write = parser.table_data_map
        self.assertEqual(3, len(to_write[Constants.STG_ASMT_LVL]))
        self.assertEqual('parent_guid', to_write[Constants.STG_ASMT_LVL][0]['parent_guid'])
        self.assertEqual('parent_guid', to_write[Constants.STG_ASMT_LVL][1]['parent_guid'])
        self.assertEqual('parent_guid', to_write[Constants.STG_ASMT_LVL][2]['parent_guid'])

        self.assertEqual(3, to_write[Constants.STG_ASMT_LVL][0]['asmt_lvl_design'])
        self.assertEqual(4, to_write[Constants.STG_ASMT_LVL][1]['asmt_lvl_design'])
        self.assertEqual(5, to_write[Constants.STG_ASMT_LVL][2]['asmt_lvl_design'])

    def test_process_data(self):
        parser = JsonParser(load_type=Constants.LOAD_TYPE_ASMT_MATH)
        test_data = [{'guid': '0000-0000-0000-001', 'code': 'SUM1', 'testLevel': []},
                     {'guid': '0000-0000-0000-002', 'code': 'SUM2', 'testLevel': []}]

        parser.initialize_data_map('tests_table')
        parser.process_list(test_data, '.tests', '1111-1111-1111-111', 'tests_table')

        to_write = parser.table_data_map
        self.assertEqual(2, len(to_write['tests_table']))
        self.assertEqual('0000-0000-0000-001', to_write['tests_table'][0]['guid'])
        self.assertEqual('0000-0000-0000-002', to_write['tests_table'][1]['guid'])
        self.assertEqual('SUM1', to_write['tests_table'][0]['code'])
        self.assertEqual('SUM2', to_write['tests_table'][1]['code'])
        self.assertEqual('1111-1111-1111-111', to_write['tests_table'][0]['parent_guid'])
        self.assertEqual('1111-1111-1111-111', to_write['tests_table'][1]['parent_guid'])

        deque = parser.data_to_process_queue
        self.assertEqual(0, len(deque))

    def check_value_exists(self, data_list, field_name, expected_value):
        found = False
        for data in data_list:
            for field in data:
                if field == field_name:
                    if data[field] == expected_value:
                        found = True
        self.assertTrue(found)
