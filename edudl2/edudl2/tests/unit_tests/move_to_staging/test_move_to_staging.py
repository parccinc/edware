'''
Created on May 24, 2013

@author: ejen
'''
import os
import unittest
from unittest.mock import patch
from edudl2.move_to_staging.move_to_staging import move_data_from_loader_to_staging, create_migration_query, \
    get_varchar_column_name_and_length
from edudl2.tests.unit_tests.unittest_with_udl2_sqlite import Unittest_with_udl2_sqlite, \
    UnittestLoaderDBConnection
from edudl2.move_to_staging.move_to_staging import get_column_mapping_from_ldr_to_stg
from edudl2.udl2 import message_keys as mk


class TestLoadToStagingTable(Unittest_with_udl2_sqlite):

    @classmethod
    def setUpClass(cls):
        super().setUpClass(force_foreign_keys=False)

    def setUp(self):
        self.test_target_columns = ['batch_guid', 'record_num', 'state_code', 'district_name', 'student_first_name', 'student_last_name']
        self.test_source_columns = ['"A".batch_guid', '"A".record_num', 'substr("A".state_code, 1, 50)', 'substr("A".district_name, 1, 50)', 'substr("A".student_first_name, 1, 50)', 'substr("A".student_last_name, 1, 50)']

        self.expected_insert_query = 'INSERT INTO stg_ela_sum (batch_guid, record_num, state_code, district_name, student_first_name, student_last_name)'
        self.expected_select_query = 'SELECT "A".batch_guid, "A".record_num, substr("A".state_code, 1, 50), substr("A".district_name, 1, 50), substr("A".student_first_name, 1, 50), substr("A".student_last_name, 1, 50)'
        self.expected_from_query = 'FROM ldr_ela_sum AS "A" LEFT OUTER JOIN err_list AS "B" ON "B".record_sid = "A".record_num'
        self.expected_where_query = 'WHERE "A".batch_guid = :batch_guid_1 AND "B".record_sid IS NULL'

    @patch('edudl2.move_to_staging.move_to_staging.get_varchar_column_name_and_length')
    def test_get_column_mapping_from_ldr_to_stg(self, get_info_mock):
        get_info_mock.return_value = {'record_num': 50, 'state_code': 50, 'district_name': 50, 'student_first_name': 50, 'student_last_name': 50}

        with UnittestLoaderDBConnection() as conn:

            ref_table_name = 'ela_sum_ref_column_mapping'
            loader_table = 'ldr_ela_sum'
            staging_table_name = 'stg_ela_sum'
            target_columns, source_columns_with_tran_rule = get_column_mapping_from_ldr_to_stg(conn, ref_table_name, loader_table, staging_table_name)

        for field_name in self.test_target_columns:
            self.assertTrue(field_name in target_columns, "{field} not in {columns}".format(field=field_name, columns=target_columns))
        for field_with_trans_rule in self.test_source_columns:
            self.assertTrue(field_with_trans_rule in source_columns_with_tran_rule, "{field} not in {columns}".format(field=field_with_trans_rule, columns=source_columns_with_tran_rule))

    def test_create_migration_query(self):

        with UnittestLoaderDBConnection() as conn:
            ref_table_name = 'ela_sum_ref_column_mapping'
            source_table_name = 'ldr_ela_sum'
            target_table_name = 'stg_ela_sum'
            error_table_name = 'err_list'
            batch_guid = '5c6e30fb-3cb7-4390-a366-e3a7ed080641'
            query = create_migration_query(conn, source_table_name, target_table_name, error_table_name, batch_guid, self.test_target_columns, self.test_source_columns)

        self.assertTrue(self.expected_insert_query in str(query), "{part} not in {query}".format(part=self.expected_insert_query, query=query))
        self.assertTrue(self.expected_select_query in str(query), "{part} not in {query}".format(part=self.expected_select_query, query=query))
        self.assertTrue(self.expected_from_query in str(query), "{part} not in {query}".format(part=self.expected_from_query, query=query))
        self.assertTrue(self.expected_where_query in str(query), "{part} not in {query}".format(part=self.expected_where_query, query=query))

    @patch('edudl2.move_to_staging.move_to_staging.get_table_columns_info')
    def test_get_varchar_column_name_and_length(self, mock_column_info):
        mock_column_info.return_value = [('batch_guid', 'character varying', 50), ('rec_num', 'character', 50), ('num', 'integer', 50)]
        with UnittestLoaderDBConnection() as conn:
            col_to_length_mapping = get_varchar_column_name_and_length(conn, 'ldr_ela_sum')

        self.assertTrue('batch_guid' in col_to_length_mapping)
        self.assertTrue('rec_num' in col_to_length_mapping)
        self.assertFalse('num' in col_to_length_mapping)

    @patch('edudl2.move_to_staging.move_to_staging.get_varchar_column_name_and_length')
    @patch('edudl2.move_to_staging.move_to_staging.get_table_columns_info')
    def test_move_data_from_loader_to_staging(self, mock_column_info, get_info_mock):
        get_info_mock.return_value = {'batch_guid': 50, 'record_num': 50, 'state_code': 50, 'district_name': 50, 'student_first_name': 50, 'student_last_name': 50}
        mock_column_info.return_value = [('batch_guid', 'character varying', 50), ('rec_num', 'character varying', 50),
                                         ('state_code', 'character varying ', 50), ('district_name', 'character varying ', 50), ('student_first_name', 'character varying ', 50),
                                         ('student_last_name', 'character varying ', 50)]
        conf = {}
        conf[mk.REF_TABLE] = 'ela_sum_ref_column_mapping'
        conf[mk.SOURCE_DB_TABLE] = 'ldr_ela_sum'
        conf[mk.TARGET_DB_TABLE] = 'stg_ela_sum'
        conf[mk.ERR_LIST_TABLE] = 'err_list'
        conf[mk.GUID_BATCH] = '5c6e30fb-3cb7-4390-a366-e3a7ed080641'

        move_data_from_loader_to_staging(conf)

        self.assertEqual(4, self.postloading_count(conf[mk.GUID_BATCH]))

    def postloading_count(self, guid_batch):
        sql_template = """
            SELECT COUNT(*) FROM "stg_ela_sum"
            WHERE batch_guid = '{guid_batch}'
        """
        with UnittestLoaderDBConnection() as conn:
            sql = sql_template.format(guid_batch=guid_batch)
            result = conn.execute(sql)
            count = 0
            for row in result:
                count = row[0]
            return count
