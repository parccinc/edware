from unittest import TestCase
from unittest import mock
import os
import shutil
import tempfile

from edudl2.udl2.defaults import UDL2_DEFAULT_CONFIG_PATH_FILE
from edudl2.udl2_util.config_reader import read_ini_file
from edudl2.udl2_util.file_util import abs_path_join
from edudl2.udl2.constants import Constants
from edudl2.filetranslator import csv_translator
from edudl2.filetranslator.csv_translator import FileTranslator
from edudl2.udl2_util.exceptions import UDL2Exception
from edudl2.tests.unit_tests.constants_for_tests.assment_expected_csv_fields_fall_2015 import  \
    EXPECTED_CSV_FIELDS


class Test(TestCase):

    def setUp(self):
        try:
            config_path = dict(os.environ)['UDL2_CONF']
        except Exception:
            config_path = UDL2_DEFAULT_CONFIG_PATH_FILE
        conf_tup = read_ini_file(config_path)
        self.conf = conf_tup[0]
        self.output_dir = tempfile.mkdtemp()
        self.dir_path = os.path.join(
            os.path.dirname(__file__), "..", "..", "data", "unittest")
        self.tmp_filename_for_translation = 'test_translation_tmp.csv'
        self.expected_csv_fields = EXPECTED_CSV_FIELDS

    def tearDown(self):
        shutil.rmtree(self.output_dir, ignore_errors=True)

    def _setup_valid_mocks(self, csv_mock):
        csv_mock.return_value = self.expected_csv_fields

    @mock.patch.multiple(csv_translator, get_source_column_values_from_ref_column_mapping=mock.DEFAULT)
    def test_old_format_schema(self, get_source_column_values_from_ref_column_mapping):
        self._setup_valid_mocks(get_source_column_values_from_ref_column_mapping)
        translator = FileTranslator(self.dir_path, 'asmt_data.ela', '1234-abcd')
        full_filename = abs_path_join(self.dir_path,
                                      'test_translator_schema_validation_valid_schema.csv')
        self.assertTrue(translator.file_required_translation(Constants.ASMT_ELA_REF_TABLE_SPRING_2015))

    @mock.patch.multiple(csv_translator, get_source_column_values_from_ref_column_mapping=mock.DEFAULT)
    def test_new_format_schema(self, get_source_column_values_from_ref_column_mapping):
        self._setup_valid_mocks(get_source_column_values_from_ref_column_mapping)
        translator = FileTranslator(self.dir_path, 'asmt_data.ela', '1234-abcd')
        self.assertFalse(translator.file_required_translation(Constants.ASMT_MATH_REF_TABLE_SPRING_2016))

    @mock.patch.multiple(csv_translator, get_source_column_values_from_ref_column_mapping=mock.DEFAULT)
    def test_validate_unsupported_schema(self, get_source_column_values_from_ref_column_mapping):
        self._setup_valid_mocks(get_source_column_values_from_ref_column_mapping)
        translator = FileTranslator(self.dir_path, 'asmt_data.ela', '1234-abcd')
        full_filename = abs_path_join(self.dir_path,
                                      'test_translator_schema_validation_unsupported_schema.csv')
        with self.assertRaises(UDL2Exception):
            translator.file_required_translation(full_filename)

    @mock.patch.multiple(csv_translator, get_source_column_values_from_ref_column_mapping=mock.DEFAULT)
    def test_validate_load_type_success(self, get_source_column_values_from_ref_column_mapping):
        translator = FileTranslator(self.dir_path, 'asmt_data.ela', '1234-abcd')
        self.assertTrue(translator.allowed_translation_load_type())

    @mock.patch.multiple(csv_translator, get_source_column_values_from_ref_column_mapping=mock.DEFAULT)
    def test_validate_load_type_failed(self, get_source_column_values_from_ref_column_mapping):
        translator = FileTranslator(self.dir_path, 'unsupported_data.ela', '1234-abcd')
        self.assertFalse(translator.allowed_translation_load_type())

    @mock.patch.multiple(csv_translator, get_source_column_values_from_ref_column_mapping=mock.DEFAULT)
    def test_successfull_translation(self, get_source_column_values_from_ref_column_mapping):
        self._setup_valid_mocks(get_source_column_values_from_ref_column_mapping)
        full_filename = abs_path_join(self.dir_path,
                                      'test_translator_schema_validation_deprecated_schema.csv')
        path_to_copy = abs_path_join(self.dir_path, self.tmp_filename_for_translation)
        shutil.copy(full_filename, path_to_copy)
        try:
            translator = FileTranslator(self.dir_path, 'asmt_data.ela', '1234-abcd')
            translator.translate_file(path_to_copy, 'asmt_data.ela')
            # if translation was successfull than file is in new schema
            self.assertTrue(translator.file_required_translation(Constants.ASMT_MATH_REF_TABLE_SPRING_2015))
        finally:
            os.remove(path_to_copy)
