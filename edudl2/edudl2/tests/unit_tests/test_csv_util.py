import unittest
import os
import tempfile
from edudl2.udl2.constants import Constants
import shutil
from edudl2.csv_util.csv_util import get_value_from_csv

__author__ = 'npandey'


class TestCsvUtil(unittest.TestCase):

    def setUp(self):
        self.data_dir = os.path.join(os.path.dirname(__file__), "..", "data", "unittest", "test_load_type_subject")
        self.test_expanded_dir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.test_expanded_dir)

    def test_get_value_from_csv(self):
        shutil.copy(os.path.join(self.data_dir, 'valid_subject.csv'), self.test_expanded_dir)
        field_value = get_value_from_csv(self.test_expanded_dir, Constants.SUBJECT_KEY)
        self.assertEqual(Constants.ASMT_SUBJECT_ELA.lower(), field_value.lower())

    def test_no_field_in_csv(self):
        shutil.copy(os.path.join(self.data_dir, 'valid_subject.csv'), self.test_expanded_dir)
        field_value = get_value_from_csv(self.test_expanded_dir, 'Random Field Name')
        self.assertIsNone(field_value)

    def test_no_data_in_csv(self):
        shutil.copy(os.path.join(self.data_dir, 'valid_subject.csv'), self.test_expanded_dir)
        field_value = get_value_from_csv(self.test_expanded_dir, 'AssessmentAcademicSubject', 2)
        self.assertIsNone(field_value)
