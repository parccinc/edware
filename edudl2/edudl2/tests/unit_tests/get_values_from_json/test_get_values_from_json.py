from edudl2.udl2.defaults import UDL2_DEFAULT_CONFIG_PATH_FILE
from edudl2.get_values_from_json import get_values_from_json
from edudl2.udl2_util.config_reader import read_ini_file
from edudl2.udl2 import message_keys as mk
import unittest
import tempfile
import shutil
import os


class TestGetValuesFromJson(unittest.TestCase):

    def setUp(self):
        try:
            config_path = dict(os.environ)['UDL2_CONF']
        except Exception:
            config_path = UDL2_DEFAULT_CONFIG_PATH_FILE
        udl2_conf = read_ini_file(config_path)
        if isinstance(udl2_conf, tuple):
            self.conf = udl2_conf[0]
        else:
            self.conf = udl2_conf
        self.data_dir = os.path.join(os.path.dirname(__file__), "..", "..", "data",
                                     "unittest", "test_load_type_subject")
        self.test_expanded_dir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.test_expanded_dir)

    def test_get_values_from_json_from_valid_content(self):
        shutil.copy(os.path.join(self.data_dir, 'valid_content_type.json'), self.test_expanded_dir)
        shutil.copy(os.path.join(self.data_dir, 'valid_subject.csv'), self.test_expanded_dir)
        values = get_values_from_json.get_values_from_json(self.test_expanded_dir)
        self.assertEqual(('asmt_data.ela'), values[mk.LOAD_TYPE])
        self.assertEqual(('spring'), values[mk.PERIOD])

    def test_get_values_from_json_from_invalid_content_json(self):
        shutil.copy(os.path.join(self.data_dir, 'invalid_content_type.json'), self.test_expanded_dir)
        shutil.copy(os.path.join(self.data_dir, 'valid_subject.csv'), self.test_expanded_dir)
        with self.assertRaises(ValueError):
            get_values_from_json.get_values_from_json(self.test_expanded_dir)

    def test_get_values_from_json_from_invalid_content_csv(self):
        shutil.copy(os.path.join(self.data_dir, 'valid_content_type.json'), self.test_expanded_dir)
        shutil.copy(os.path.join(self.data_dir, 'invalid_subject.csv'), self.test_expanded_dir)
        with self.assertRaises(ValueError):
            get_values_from_json.get_values_from_json(self.test_expanded_dir)

    def test_get_values_from_json_from_missing_content_csv(self):
        shutil.copy(os.path.join(self.data_dir, 'valid_content_type.json'), self.test_expanded_dir)
        shutil.copy(os.path.join(self.data_dir, 'missing_subject.csv'), self.test_expanded_dir)
        with self.assertRaises(ValueError):
            get_values_from_json.get_values_from_json(self.test_expanded_dir)
