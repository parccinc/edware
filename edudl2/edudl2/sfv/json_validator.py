import json
import os

import jsonschema
from celery.utils.log import get_task_logger

from edudl2.udl2.celery import udl2_conf
from edudl2.exceptions.errorcodes import ErrorCode
from edudl2.udl2.constants import Constants


logger = get_task_logger(__name__)


class JsonValidator():
    """
    Invoke a suite of validations for json files.
    """

    def __init__(self):
        self.validators = [IsValidJsonFile()]

    def execute(self, dir_path, file_name, batch_sid):
        """
        Run all validation tests and return a list of error codes for all failures, or
        errorcodes.STATUS_OK if all tests pass

        @param dir_path: path of the file
        @type dir_path: string
        @param file_name: name of the file
        @type file_name: string
        @param batch_sid: batch id of the file
        @type batch_sid: integer
        @return: tuple of the form: (status_code, dir_path, file_name, batch_sid)
        """
        error_list = []
        for validator in self.validators:
            result = validator.execute(dir_path, file_name, batch_sid)
            if result[0] != ErrorCode.STATUS_OK:
                error_list.append(result)
            else:
                pass
        return error_list


class IsValidJsonFile(object):
    '''Make sure the file contains a parsable json string'''

    def execute(self, dir_path, file_name, batch_sid):
        '''
        Run json.load() on the given file, if it is invalid json, the exception will be caught and the proper code returned

        @param dir_path: path of the file
        @type dir_path: string
        @param file_name: name of the file
        @type file_name: string
        @param batch_sid: batch id of the file
        @type batch_sid: integer
        @return: tuple of the form: (status_code, dir_path, file_name, batch_sid)
        '''
        complete_path = os.path.join(dir_path, file_name)
        with open(complete_path) as f:
            try:
                json.load(f)
                return ErrorCode.STATUS_OK, dir_path, file_name, batch_sid
            except ValueError:
                return ErrorCode.SRC_JSON_INVALID_STRUCTURE, dir_path, file_name, batch_sid
