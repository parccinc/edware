from __future__ import absolute_import
import os
from celery.utils.log import get_task_logger
import datetime

from edudl2.udl2.celery import celery
from edudl2.udl2.constants import Constants
from edudl2.filetranslator.csv_translator import FileTranslator
from edudl2.udl2_util.file_util import abs_path_join
from edudl2.udl2 import message_keys as mk
from edudl2.udl2.udl2_base_task import Udl2BaseTask
from edudl2.udl2_util.measurement import BatchTableBenchmark

'''
Schema translator, required for backward compatibility between old and new schemas.
If file in new format, continue without changes, otherwise apply transformation rules.
For this create temporary file in the same directory as file for changes,
than apply changes to line by line and save to the tmp file.
After all updates replace file with old schema by the new one and delete tmp file
'''

logger = get_task_logger(__name__)


@celery.task(name="udl2.W_data_translator.task", base=Udl2BaseTask)
def task(incoming_msg):
    start_time = datetime.datetime.now()
    try:
        tenant_directory_paths = incoming_msg[mk.TENANT_DIRECTORY_PATHS]
        expanded_dir = tenant_directory_paths[mk.EXPANDED]
        guid_batch = incoming_msg[mk.GUID_BATCH]
        load_type = incoming_msg[mk.LOAD_TYPE]
        # We don't need period check anymore (cause app logic)
        # period = incoming_msg[mk.PERIOD]
    except KeyError:
        raise("Some of required parameter(s) are missing")
    # Outgoing message to be piped to the data validator
    outgoing_msg = {}
    schema_translator = FileTranslator(expanded_dir, load_type, guid_batch)

    csv_files = (filename for filename in os.listdir(expanded_dir) if filename.endswith('.csv'))
    for file_name in csv_files:
        full_filename = abs_path_join(expanded_dir, file_name)
        schema_version = schema_translator.get_schema_version(full_filename)
        if schema_translator.allowed_translation_load_type():
            # outgoing_msg[schema_version = ] = schema_version
            if schema_translator.file_required_translation(schema_version):
                logger.info('Starting translation for file:')
                # Override schema version
                schema_version = Constants.TRANSLATION_SUMMATIVE_LOAD_TYPES_ROADMAP[schema_version]
                schema_translator.translate_file(full_filename, schema_version)
            else:
                logger.info('File is already in supported schema format, continue.')
                continue
        else:
            logger.info('Translation not supported for load type "{load_type}", '
                        'translation step skipped.'.format(load_type=load_type))
    else:
        # variable schema version will be defined in any case, so this is save
        outgoing_msg['schema_version'] = schema_version

    finish_time = datetime.datetime.now()

    benchmark = BatchTableBenchmark(guid_batch, load_type, task.name, start_time, finish_time,
                                    task_id=str(task.request.id), tenant=incoming_msg[mk.TENANT_NAME])
    benchmark.record_benchmark()

    outgoing_msg.update(incoming_msg)
    return outgoing_msg
