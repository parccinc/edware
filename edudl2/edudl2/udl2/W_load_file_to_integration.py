import datetime
import celery
from edudl2.udl2.udl2_base_task import Udl2BaseTask
from edudl2.udl2 import message_keys as mk
from edudl2.udl2_util.measurement import BatchTableBenchmark
from edudl2.udl2_util.file_util import get_file_type_from_dir
from edudl2.fileloader.content_loader import load_content_to_integration
from edudl2.udl2.constants import Constants
from edudl2.udl2.celery import udl2_conf, celery
from edudl2.json_util import json_util

__author__ = 'ablum'


@celery.task(name="udl2.W_load_file_to_integration.task", base=Udl2BaseTask)
def task(msg):
    start_time = datetime.datetime.now()
    guid_batch = msg.get(mk.GUID_BATCH)
    load_type = msg.get(mk.LOAD_TYPE)
    tenant_name = msg.get(mk.TENANT_NAME)
    tenant_directory_paths = msg.get(mk.TENANT_DIRECTORY_PATHS)
    expanded_dir = tenant_directory_paths.get(mk.EXPANDED)
    json_file = get_file_type_from_dir('.json', expanded_dir)
    identification_data = {}

    if load_type == 'item_student_score':
        file_to_load = get_file_type_from_dir('.xml', expanded_dir)
        identification_data["admin_guid"] = json_util.get_attribute_value_from_json_keypath(json_file, "admin_guid")
        identification_data["asmt_guid"] = json_util.get_attribute_value_from_json_keypath(json_file, "asmt_guid")
    elif load_type == 'asmt_data.math' or load_type == 'asmt_data.ela':
        file_to_load = json_file
        identification_data["admin_guid"] = json_util.get_attribute_value_from_json_keypath(json_file, "guid")
        identification_data["asmt_guid"] = '-1'

    conf = generate_conf(guid_batch, tenant_name)
    load_content_to_integration(file_to_load, conf, identification_data)

    end_time = datetime.datetime.now()

    # record benchmark
    benchmark = BatchTableBenchmark(guid_batch, load_type, task.name, start_time, end_time,
                                    task_id=str(task.request.id),
                                    size_records=10, tenant=msg[mk.TENANT_NAME])
    benchmark.record_benchmark()
    return msg


def generate_conf(guid_batch, tenant):
    conf = {mk.GUID_BATCH: guid_batch,
            mk.TARGET_DB_TABLE: Constants.UDL2_XML_FILE_TABLE,
            mk.TARGET_DB_SCHEMA: guid_batch,
            mk.TENANT_NAME: tenant,
            }
    return conf
