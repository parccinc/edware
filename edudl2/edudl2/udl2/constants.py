__author__ = 'sravi'


class Constants():

    """
    constants related to udl db schema
    """
    UDL2_DB_CONN = 'udl2_db_conn'

    DB_SCHEMA = 'db_schema'
    URL = 'url'

    # guid fields
    GUID = 'guid'
    PARENT_GUID = 'parent_guid'
    TEST_CODE = 'test_code'

    #Loader Tables
    #Summative
    LDR_MATH_SUM = 'ldr_math_sum'
    LDR_ELA_SUM = 'ldr_ela_sum'
    LDR_ITEM_RESULT = 'ldr_student_item_score'
    LDR_PSYCHOMETRIC = 'ldr_item_p_data'
    LDR_MATH_SUM_ITEM = 'ldr_math_sum_item_score'
    LDR_ELA_SUM_ITEM = 'ldr_ela_sum_item_score'
    #MYA
    LDR_MYA_ELA_ITEM = 'ldr_item_ela_mya'
    LDR_MYA_MATH_ITEM = 'ldr_item_math_mya'
    LDR_ELA_MYA = 'ldr_ela_mya'
    LDR_MATH_MYA = 'ldr_math_mya'
    #SNL
    LDR_ELA_SNL = 'ldr_ela_snl'
    LDR_SNL_STUDENT_TASK = 'ldr_student_task_snl'
    #Diagnostic ELA
    LDR_ELA_ITEM_READING_COMP = 'ldr_item_ela_read_comp'
    LDR_ELA_ITEM_VOCAB = 'ldr_item_ela_vocab'
    LDR_ELA_ITEM_DECODING = 'ldr_item_ela_decod'
    LDR_ELA_VOCAB = 'ldr_ela_vocab'
    LDR_ELA_COMP = 'ldr_ela_comp'
    LDR_ELA_DECODING = 'ldr_ela_decod'
    LDR_ELA_WRITING = 'ldr_ela_writing'
    LDR_ELA_READ_FLU = 'ldr_ela_read_flu'
    LDR_READER_MOTIV = 'ldr_reader_motiv'
    #Diagnostic Math
    LDR_MATH_ITEM_FLUENCY = 'ldr_item_math_flu'
    LDR_MATH_ITEM_COMPREHENSION = 'ldr_item_math_comp'
    LDR_MATH_ITEM_PROGRESSION = 'ldr_item_math_progress'
    LDR_MATH_LOCATOR = 'ldr_math_locator'
    LDR_MATH_PROGRESSION = 'ldr_math_progress'
    LDR_MATH_FLUENCY = 'ldr_math_flu'
    LDR_MATH_GRADE_LEVEL = 'ldr_math_grade_level'
    LDR_MATH_CLUSTER = 'ldr_math_cluster'

    #Staging Tables
    #Assessment Metadata
    STG_ADMIN = 'stg_admin'
    STG_ASMT = 'stg_asmt'
    STG_ASMT_LVL = 'stg_asmt_lvl'
    STG_FORM = 'stg_form'
    STG_FORM_GROUP = 'stg_form_group'
    STG_ITEM = 'stg_item'
    STG_TEST_SCORE = 'stg_sum_test_score_m'
    STG_TEST_LEVEL = 'stg_sum_test_level_m'
    #Summative Assessment Result
    STG_MATH_SUM = 'stg_math_sum'
    STG_ELA_SUM = 'stg_ela_sum'
    #Summative Item
    STG_ITEM_RESULT = 'stg_student_item_score'
    STG_PSYCHOMETRIC = 'stg_item_p_data'
    STG_MATH_SUM_ITEM = 'stg_math_sum_item_score'
    STG_ELA_SUM_ITEM = 'stg_ela_sum_item_score'
    #MYA Item
    STG_MYA_ELA_ITEM = 'stg_item_ela_mya'
    STG_MYA_MATH_ITEM = 'stg_item_math_mya'
    #MYA Assessment
    STG_ELA_MYA = 'stg_ela_mya'
    STG_MATH_MYA = 'stg_math_mya'
    #MYA Metadata
    STG_ITEM_MYA_META = 'stg_item_mya_meta'
    #SNL
    STG_ELA_SNL = 'stg_ela_snl'
    STG_SNL_STUDENT_TASK = 'stg_student_task_snl'
    STG_SNL_ASMT = 'stg_snl_asmt_m'
    STG_SNL_TASK = 'stg_snl_task_m'
    STG_SNL_ASMT_MODE = 'stg_snl_asmt_mode_m'
    #Diagnostic ELA Item
    STG_ELA_ITEM_READING_COMP = 'stg_item_ela_read_comp'
    STG_ELA_ITEM_VOCAB = 'stg_item_ela_vocab'
    STG_ELA_ITEM_DECODING = 'stg_item_ela_decod'
    #Diagnostic Math Item
    STG_MATH_ITEM_FLUENCY = 'stg_item_math_flu'
    STG_MATH_ITEM_COMPREHENSION = 'stg_item_math_comp'
    STG_MATH_ITEM_PROGRESSION = 'stg_item_math_progress'
    #Diagnostic ELA Assessment
    STG_ELA_VOCAB = 'stg_ela_vocab'
    STG_ELA_COMP = 'stg_ela_comp'
    STG_ELA_DECODING = 'stg_ela_decod'
    STG_ELA_WRITING = 'stg_ela_writing'
    STG_ELA_READ_FLU = 'stg_ela_read_flu'
    STG_READER_MOTIV = 'stg_reader_motiv'
    #Diagnostic Math Assessment
    STG_MATH_LOCATOR = 'stg_math_locator'
    STG_MATH_PROGRESSION = 'stg_math_progress'
    STG_MATH_FLUENCY = 'stg_math_flu'
    STG_MATH_GRADE_LEVEL = 'stg_math_grade_level'
    STG_MATH_CLUSTER = 'stg_math_cluster'
    #Diagnostic Metadata
    STG_DGNST_ASMT = 'stg_dgnst_asmt_m'
    STG_DGNST_ELA_ITEM = 'stg_dgnst_ela_item_m'
    STG_DGNST_MATH_ITEM = 'stg_dgnst_math_item_m'
    STG_DGNST_ASMT_LVL = 'stg_dgnst_asmt_lvl_m'
    STG_ASMT_SKILL = 'stg_asmt_skill_m'

    #Integration Tables
    #Summative
    INT_MATH_SUM = 'int_math_sum'
    INT_ELA_SUM = 'int_ela_sum'
    INT_PSYCHOMETRIC = 'int_item_p_data'
    INT_MATH_SUM_ITEM = 'int_math_sum_item_score'
    INT_ELA_SUM_ITEM = 'int_ela_sum_item_score'
    #MYA
    INT_MYA_ELA_ITEM = 'int_item_ela_mya'
    INT_MYA_MATH_ITEM = 'int_item_math_mya'
    INT_ELA_MYA = 'int_ela_mya'
    INT_MATH_MYA = 'int_math_mya'
    #SNL
    INT_ELA_SNL = 'int_ela_snl'
    INT_SNL_STUDENT_TASK = 'int_student_task_snl'
    #Diagnostic
    INT_ELA_ITEM_READING_COMP = 'int_item_ela_read_comp'
    INT_ELA_ITEM_VOCAB = 'int_item_ela_vocab'
    INT_ELA_ITEM_DECODING = 'int_item_ela_decod'
    INT_ELA_VOCAB = 'int_ela_vocab'
    INT_ELA_COMP = 'int_ela_comp'
    INT_ELA_DECODING = 'int_ela_decod'
    INT_ELA_WRITING = 'int_ela_writing'
    INT_ELA_READ_FLU = 'int_ela_read_flu'
    INT_READER_MOTIV = 'int_reader_motiv'
    #Diagnostic Math
    INT_MATH_ITEM_FLUENCY = 'int_item_math_flu'
    INT_MATH_ITEM_COMPREHENSION = 'int_item_math_comp'
    INT_MATH_ITEM_PROGRESSION = 'int_item_math_progress'
    INT_MATH_LOCATOR = 'int_math_locator'
    INT_MATH_PROGRESSION = 'int_math_progress'
    INT_MATH_FLUENCY = 'int_math_flu'
    INT_MATH_GRADE_LEVEL = 'int_math_grade_level'
    INT_MATH_CLUSTER = 'int_math_cluster'

    # ref mapping tables
    ASMT_MATH_REF_TABLE = 'math_sum_ref_column_mapping'
    ASMT_MATH_REF_TABLE_SPRING_2015 = 'math_sum_ref_column_mapping_spring_2015'
    ASMT_MATH_REF_TABLE_FALL_2015 = 'math_sum_ref_column_mapping_fall_2015'
    ASMT_MATH_REF_TABLE_SPRING_2016 = 'math_sum_ref_column_mapping_spring_2016'

    ASMT_ELA_REF_TABLE = 'ela_sum_ref_column_mapping'
    ASMT_ELA_REF_TABLE_SPRING_2015 = 'ela_sum_ref_column_mapping_spring_2015'
    ASMT_ELA_REF_TABLE_FALL_2015 = 'ela_sum_ref_column_mapping_fall_2015'
    ASMT_ELA_REF_TABLE_SPRING_2016 = 'ela_sum_ref_column_mapping_spring_2016'

    ASMT_PSYCHOMETRIC_REF_TABLE = 'psychometric_sum_ref_column_mapping'
    MYA_ELA_ITEM_REF_TABLE = 'mya_item_ela_ref_column_mapping'
    MYA_MATH_ITEM_REF_TABLE = 'mya_item_math_ref_column_mapping'
    MYA_MATH_REF_TABLE = 'mya_math_ref_column_mapping'
    MYA_ELA_REF_TABLE = 'mya_ela_ref_column_mapping'
    SUMMATIVE_ITEM_REF_TABLE = 'summative_item_ref_column_mapping'
    SNL_REF_TABLE = 'snl_asmt_ref_column_mapping'
    SNL_STUDENT_TASK_REF_TABLE = 'snl_student_task_ref_column_mapping'
    ELA_ITEM_READING_COMP_REF_TABLE = 'item_ela_reading_comp_ref_column_mapping'
    ELA_ITEM_VOCAB_REF_TABLE = 'item_ela_vocab_ref_column_mapping'
    ELA_ITEM_DECODING_REF_TABLE = 'item_ela_decoding_ref_column_mapping'
    ELA_VOCAB_REF_TABLE = 'ela_vocab_ref_column_mapping'
    ELA_READ_COMP_REF_TABLE = 'ela_read_comp_ref_column_mapping'
    ELA_DECODING_REF_TABLE = 'ela_decod_ref_column_mapping'
    ELA_WRITING_REF_TABLE = 'ela_writing_ref_column_mapping'
    ELA_READ_FLU_REF_TABLE = 'ela_read_flu_ref_column_mapping'
    MATH_ITEM_FLUENCY_REF_TABLE = 'item_math_fluency_ref_column_mapping'
    MATH_ITEM_COMPREHENSION_REF_TABLE = 'item_math_comprehension_ref_column_mapping'
    MATH_ITEM_PROGRESSION_REF_TABLE = 'item_math_progression_ref_column_mapping'
    MATH_LOCATOR_REF_TABLE = 'math_locator_ref_column_mapping'
    MATH_PROGRESS_REF_TABLE = 'math_progress_ref_column_mapping'
    MATH_FLU_REF_TABLE = 'math_flu_ref_column_mapping'
    MATH_GRADE_LEVEL_REF_TABLE = 'math_grade_level_ref_column_mapping'
    MATH_CLUSTER_REF_TABLE = 'math_cluster_ref_column_mapping'
    READER_MOTIV_REF_TABLE = 'reader_motiv_ref_column_mapping'
    SUMMATIVE_ELA_ITEM_REF_TABLE = 'summative_ela_item_ref_column_mapping'
    SUMMATIVE_MATH_ITEM_REF_TABLE = 'summative_math_item_ref_column_mapping'

    # other tables
    UDL2_BATCH_TABLE = 'udl_batch'
    UDL2_ERR_LIST_TABLE = 'err_list'
    UDL2_CSV_LZ_TABLE = 'lz_csv'
    UDL2_JSON_LZ_TABLE = 'lz_json'
    UDL2_XML_LZ_TABLE = 'lz_xml'
    UDL2_FDW_SERVER = 'udl2_fdw_server'
    UDL2_XML_FILE_TABLE = 'int_file_store'

    # column values
    OP_COLUMN_NAME = 'op'
    GUID_ASMT = 'guid_asmt'
    STG_GUID_ASMT = 'assessmentguid'

    # load types
    LOAD_TYPE_KEY = 'DataFileType'
    LOAD_TYPE_KEY_SUMM = 'file_type'
    LOAD_TYPE_ASSESSMENT = 'asmt_data'
    LOAD_TYPE_ITEM = 'item_student_score'
    LOAD_TYPE_MYA_ELA_ITEM = 'mya_ela_item_data'
    LOAD_TYPE_MYA_MATH_ITEM = 'mya_math_item_data'
    LOAD_TYPE_PSYCHOMETRIC = 'p_data'
    LOAD_TYPE_SNL_STUDENT_TASK = 'snl_task_data'
    LOAD_TYPE_SNL_ASMT = 's&l_total_asmt_data'
    LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_READING_COMP = 'diagnostic_ela_item_reading_comp'
    LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_VOCAB = 'diagnostic_ela_item_vocab'
    LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_DECODING = 'diagnostic_ela_item_decoding'
    LOAD_TYPE_DIAGNOSTIC_ELA_VOCAB = 'diagnostic_ela_vocab'
    LOAD_TYPE_DIAGNOSTIC_ELA_COMP = 'diagnostic_ela_comp'
    LOAD_TYPE_DIAGNOSTIC_ELA_DECODING = 'diagnostic_ela_decoding'
    LOAD_TYPE_DIAGNOSTIC_ELA_WRITING = 'diagnostic_ela_writing'
    LOAD_TYPE_DIAGNOSTIC_ELA_READ_FLU = 'diagnostic_ela_reading_fluency'
    LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_PROGRESSION = 'diagnostic_math_item_progression'
    LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_COMPREHENSION = 'diagnostic_math_item_comp'
    LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_FLUENCY = 'diagnostic_math_item_fluency'
    LOAD_TYPE_DIAGNOSTIC_MATH_LOCATOR = 'diagnostic_math_locator'
    LOAD_TYPE_DIAGNOSTIC_MATH_PROGRESSION = 'diagnostic_math_progression'
    LOAD_TYPE_DIAGNOSTIC_MATH_FLUENCY = 'diagnostic_math_fluency'
    LOAD_TYPE_DIAGNOSTIC_MATH_GRADE_LEVEL = 'diagnostic_math_gradelevel'
    LOAD_TYPE_DIAGNOSTIC_MATH_CLUSTER = 'diagnostic_math_cluster'
    LOAD_TYPE_READER_MOTIVATION = 'diagnostic_reader_motivation'
    LOAD_TYPE_ELA_ITEM = 'ela_item_student_score'
    LOAD_TYPE_MATH_ITEM = 'math_item_student_score'

    # periods
    PERIOD_KEY = 'period'
    PERIOD_KEY_SUMM = 'summative_period'
    PERIOD_SPRING = 'spring'
    PERIOD_FALLBLOCK = 'fallblock'

    # subjects
    SUBJECT_KEY = 'Subject'
    INTERNAL_SUBJECT_MATH = 'math'
    INTERNAL_SUBJECT_ELA = 'ela'
    ASMT_SUBJECT_ELA = "english language arts/literacy"
    ASMT_SUBJECT_MATH = 'mathematics'
    ASMT_SUBJECT_ALG_I = 'algebra i'
    ASMT_SUBJECT_ALG_II = 'algebra ii'
    ASMT_SUBJECT_GEOMETRY = 'geometry'
    ASMT_SUBJECT_MATH_I = 'integrated mathematics i'
    ASMT_SUBJECT_MATH_II = 'integrated mathematics ii'
    ASMT_SUBJECT_MATH_III = 'integrated mathematics iii'
    MATH_SUBJECTS = [ASMT_SUBJECT_MATH, ASMT_SUBJECT_ALG_I, ASMT_SUBJECT_ALG_II, ASMT_SUBJECT_GEOMETRY,
                     ASMT_SUBJECT_MATH_I, ASMT_SUBJECT_MATH_II, ASMT_SUBJECT_MATH_III]

    LOAD_TYPE_ASMT_MATH = LOAD_TYPE_ASSESSMENT + '.' + INTERNAL_SUBJECT_MATH
    LOAD_TYPE_ASMT_ELA = LOAD_TYPE_ASSESSMENT + '.' + INTERNAL_SUBJECT_ELA
    LOAD_TYPE_MYA_ASMT_MATH = 'mya_math_asmt_data'
    LOAD_TYPE_MYA_ASMT_ELA = 'mya_ela_asmt_data'
    # global sequence name
    SEQUENCE_NAME = 'global_rec_seq'

    ZONES = 'zones'
    ARRIVALS = 'arrivals'

    # Phase number
    INT_TO_STAR_PHASE = 4

    # File extensions
    PROCESSING_FILE_EXT = '.processing'
    SUPPORTED_FILE_EXTENSIONS = ['xml', 'json', 'csv']
    TARBALL_TYPE = 'tarball'
    ZIP_TYPE = 'zip'
    TARBALL_EXTENSION = 'tar.gz'
    ZIP_EXTENSION = '.zip'
    ARCH_TYPES = {
        TARBALL_EXTENSION: TARBALL_TYPE,
        ZIP_EXTENSION: ZIP_TYPE,
    }

    # lambdas for returning list of constants or constants based on some condition
    # TODO: in future this will be replaced with dynamic udl schema based on load being processed
    LOAD_TYPES = lambda: [Constants.LOAD_TYPE_ASSESSMENT,
                          Constants.LOAD_TYPE_ITEM,
                          Constants.LOAD_TYPE_PSYCHOMETRIC,
                          Constants.LOAD_TYPE_MYA_ELA_ITEM,
                          Constants.LOAD_TYPE_MYA_MATH_ITEM,
                          Constants.LOAD_TYPE_MYA_ASMT_ELA,
                          Constants.LOAD_TYPE_MYA_ASMT_MATH,
                          Constants.LOAD_TYPE_SNL_ASMT,
                          Constants.LOAD_TYPE_SNL_STUDENT_TASK,
                          Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_READING_COMP,
                          Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_VOCAB,
                          Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_DECODING,
                          Constants.LOAD_TYPE_DIAGNOSTIC_ELA_VOCAB,
                          Constants.LOAD_TYPE_DIAGNOSTIC_ELA_COMP,
                          Constants.LOAD_TYPE_DIAGNOSTIC_ELA_DECODING,
                          Constants.LOAD_TYPE_DIAGNOSTIC_ELA_WRITING,
                          Constants.LOAD_TYPE_DIAGNOSTIC_ELA_READ_FLU,
                          Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_PROGRESSION,
                          Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_COMPREHENSION,
                          Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_FLUENCY,
                          Constants.LOAD_TYPE_DIAGNOSTIC_MATH_LOCATOR,
                          Constants.LOAD_TYPE_DIAGNOSTIC_MATH_PROGRESSION,
                          Constants.LOAD_TYPE_DIAGNOSTIC_MATH_FLUENCY,
                          Constants.LOAD_TYPE_DIAGNOSTIC_MATH_GRADE_LEVEL,
                          Constants.LOAD_TYPE_DIAGNOSTIC_MATH_CLUSTER,
                          Constants.LOAD_TYPE_READER_MOTIVATION,
                          Constants.LOAD_TYPE_ELA_ITEM,
                          Constants.LOAD_TYPE_MATH_ITEM]

    PERIODS = lambda: [Constants.PERIOD_SPRING, Constants.PERIOD_FALLBLOCK]

    LOAD_TYPES_ALLOWED_SCHEMA_TRANSLATION = lambda: [Constants.LOAD_TYPE_ASMT_ELA,
                                                     Constants.LOAD_TYPE_ASMT_MATH]

    PERIODS_ALLOWED_SCHEMA_TRANSLATION = lambda: [Constants.PERIOD_FALLBLOCK]

    LOAD_SUBJECTS = lambda: [Constants.INTERNAL_SUBJECT_MATH, Constants.INTERNAL_SUBJECT_ELA]

    SUBJECT_DEPENDANT_LOAD_TYPES = lambda: [Constants.LOAD_TYPE_ASSESSMENT]

    UDL2_LOADER_TABLE = lambda load_type: {Constants.LOAD_TYPE_ASMT_MATH: Constants.LDR_MATH_SUM,
                                           Constants.LOAD_TYPE_ASMT_ELA: Constants.LDR_ELA_SUM,
                                           Constants.LOAD_TYPE_PSYCHOMETRIC: Constants.LDR_PSYCHOMETRIC,
                                           Constants.LOAD_TYPE_ITEM: Constants.LDR_ITEM_RESULT,
                                           Constants.LOAD_TYPE_MYA_ELA_ITEM: Constants.LDR_MYA_ELA_ITEM,
                                           Constants.LOAD_TYPE_MYA_MATH_ITEM: Constants.LDR_MYA_MATH_ITEM,
                                           Constants.LOAD_TYPE_MYA_ASMT_ELA: Constants.LDR_ELA_MYA,
                                           Constants.LOAD_TYPE_MYA_ASMT_MATH: Constants.LDR_MATH_MYA,
                                           Constants.LOAD_TYPE_SNL_ASMT: Constants.LDR_ELA_SNL,
                                           Constants.LOAD_TYPE_SNL_STUDENT_TASK: Constants.LDR_SNL_STUDENT_TASK,
                                           Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_READING_COMP: Constants.LDR_ELA_ITEM_READING_COMP,
                                           Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_VOCAB: Constants.LDR_ELA_ITEM_VOCAB,
                                           Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_DECODING: Constants.LDR_ELA_ITEM_DECODING,
                                           Constants.LOAD_TYPE_DIAGNOSTIC_ELA_VOCAB: Constants.LDR_ELA_VOCAB,
                                           Constants.LOAD_TYPE_DIAGNOSTIC_ELA_COMP: Constants.LDR_ELA_COMP,
                                           Constants.LOAD_TYPE_DIAGNOSTIC_ELA_DECODING: Constants.LDR_ELA_DECODING,
                                           Constants.LOAD_TYPE_DIAGNOSTIC_ELA_WRITING: Constants.LDR_ELA_WRITING,
                                           Constants.LOAD_TYPE_DIAGNOSTIC_ELA_READ_FLU: Constants.LDR_ELA_READ_FLU,
                                           Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_PROGRESSION: Constants.LDR_MATH_ITEM_PROGRESSION,
                                           Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_COMPREHENSION: Constants.LDR_MATH_ITEM_COMPREHENSION,
                                           Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_FLUENCY: Constants.LDR_MATH_ITEM_FLUENCY,
                                           Constants.LOAD_TYPE_DIAGNOSTIC_MATH_LOCATOR: Constants.LDR_MATH_LOCATOR,
                                           Constants.LOAD_TYPE_DIAGNOSTIC_MATH_PROGRESSION: Constants.LDR_MATH_PROGRESSION,
                                           Constants.LOAD_TYPE_DIAGNOSTIC_MATH_FLUENCY: Constants.LDR_MATH_FLUENCY,
                                           Constants.LOAD_TYPE_DIAGNOSTIC_MATH_GRADE_LEVEL: Constants.LDR_MATH_GRADE_LEVEL,
                                           Constants.LOAD_TYPE_DIAGNOSTIC_MATH_CLUSTER: Constants.LDR_MATH_CLUSTER,
                                           Constants.LOAD_TYPE_READER_MOTIVATION: Constants.LDR_READER_MOTIV,
                                           Constants.LOAD_TYPE_ELA_ITEM: Constants.LDR_ELA_SUM_ITEM,
                                           Constants.LOAD_TYPE_MATH_ITEM: Constants.LDR_MATH_SUM_ITEM}.get(load_type, None)

    UDL2_STAGING_TABLE = lambda load_type: {Constants.LOAD_TYPE_ASMT_MATH: Constants.STG_MATH_SUM,
                                            Constants.LOAD_TYPE_ASMT_ELA: Constants.STG_ELA_SUM,
                                            Constants.LOAD_TYPE_PSYCHOMETRIC: Constants.STG_PSYCHOMETRIC,
                                            Constants.LOAD_TYPE_ITEM: Constants.STG_ITEM_RESULT,
                                            Constants.LOAD_TYPE_MYA_ELA_ITEM: Constants.STG_MYA_ELA_ITEM,
                                            Constants.LOAD_TYPE_MYA_MATH_ITEM: Constants.STG_MYA_MATH_ITEM,
                                            Constants.LOAD_TYPE_MYA_ASMT_ELA: Constants.STG_ELA_MYA,
                                            Constants.LOAD_TYPE_MYA_ASMT_MATH: Constants.STG_MATH_MYA,
                                            Constants.LOAD_TYPE_SNL_ASMT: Constants.STG_ELA_SNL,
                                            Constants.LOAD_TYPE_SNL_STUDENT_TASK: Constants.STG_SNL_STUDENT_TASK,
                                            Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_READING_COMP: Constants.STG_ELA_ITEM_READING_COMP,
                                            Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_VOCAB: Constants.STG_ELA_ITEM_VOCAB,
                                            Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_DECODING: Constants.STG_ELA_ITEM_DECODING,
                                            Constants.LOAD_TYPE_DIAGNOSTIC_ELA_VOCAB: Constants.STG_ELA_VOCAB,
                                            Constants.LOAD_TYPE_DIAGNOSTIC_ELA_COMP: Constants.STG_ELA_COMP,
                                            Constants.LOAD_TYPE_DIAGNOSTIC_ELA_DECODING: Constants.STG_ELA_DECODING,
                                            Constants.LOAD_TYPE_DIAGNOSTIC_ELA_WRITING: Constants.STG_ELA_WRITING,
                                            Constants.LOAD_TYPE_DIAGNOSTIC_ELA_READ_FLU: Constants.STG_ELA_READ_FLU,
                                            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_PROGRESSION: Constants.STG_MATH_ITEM_PROGRESSION,
                                            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_COMPREHENSION: Constants.STG_MATH_ITEM_COMPREHENSION,
                                            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_FLUENCY: Constants.STG_MATH_ITEM_FLUENCY,
                                            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_LOCATOR: Constants.STG_MATH_LOCATOR,
                                            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_PROGRESSION: Constants.STG_MATH_PROGRESSION,
                                            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_FLUENCY: Constants.STG_MATH_FLUENCY,
                                            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_GRADE_LEVEL: Constants.STG_MATH_GRADE_LEVEL,
                                            Constants.LOAD_TYPE_DIAGNOSTIC_MATH_CLUSTER: Constants.STG_MATH_CLUSTER,
                                            Constants.LOAD_TYPE_READER_MOTIVATION: Constants.STG_READER_MOTIV,
                                            Constants.LOAD_TYPE_ELA_ITEM: Constants.STG_ELA_SUM_ITEM,
                                            Constants.LOAD_TYPE_MATH_ITEM: Constants.STG_MATH_SUM_ITEM}.get(load_type, None)

    UDL2_INTEGRATION_TABLE = lambda load_type: {Constants.LOAD_TYPE_ASMT_MATH: Constants.INT_MATH_SUM,
                                                Constants.LOAD_TYPE_PSYCHOMETRIC: Constants.INT_PSYCHOMETRIC,
                                                Constants.LOAD_TYPE_ASMT_ELA: Constants.INT_ELA_SUM,
                                                Constants.LOAD_TYPE_MYA_ELA_ITEM: Constants.INT_MYA_ELA_ITEM,
                                                Constants.LOAD_TYPE_MYA_MATH_ITEM: Constants.INT_MYA_MATH_ITEM,
                                                Constants.LOAD_TYPE_MYA_ASMT_ELA: Constants.INT_ELA_MYA,
                                                Constants.LOAD_TYPE_MYA_ASMT_MATH: Constants.INT_MATH_MYA,
                                                Constants.LOAD_TYPE_SNL_ASMT: Constants.INT_ELA_SNL,
                                                Constants.LOAD_TYPE_SNL_STUDENT_TASK: Constants.INT_SNL_STUDENT_TASK,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_READING_COMP: Constants.INT_ELA_ITEM_READING_COMP,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_VOCAB: Constants.INT_ELA_ITEM_VOCAB,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_DECODING: Constants.INT_ELA_ITEM_DECODING,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_ELA_VOCAB: Constants.INT_ELA_VOCAB,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_ELA_COMP: Constants.INT_ELA_COMP,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_ELA_DECODING: Constants.INT_ELA_DECODING,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_ELA_WRITING: Constants.INT_ELA_WRITING,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_ELA_READ_FLU: Constants.INT_ELA_READ_FLU,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_PROGRESSION: Constants.INT_MATH_ITEM_PROGRESSION,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_COMPREHENSION: Constants.INT_MATH_ITEM_COMPREHENSION,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_FLUENCY: Constants.INT_MATH_ITEM_FLUENCY,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_MATH_LOCATOR: Constants.INT_MATH_LOCATOR,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_MATH_PROGRESSION: Constants.INT_MATH_PROGRESSION,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_MATH_FLUENCY: Constants.INT_MATH_FLUENCY,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_MATH_GRADE_LEVEL: Constants.INT_MATH_GRADE_LEVEL,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_MATH_CLUSTER: Constants.INT_MATH_CLUSTER,
                                                Constants.LOAD_TYPE_READER_MOTIVATION: Constants.INT_READER_MOTIV,
                                                Constants.LOAD_TYPE_ELA_ITEM: Constants.INT_ELA_SUM_ITEM,
                                                Constants.LOAD_TYPE_MATH_ITEM: Constants.INT_MATH_SUM_ITEM}.get(load_type, None)

    UDL2_REF_MAPPING_TABLE = lambda load_type: {Constants.LOAD_TYPE_ASMT_MATH: (Constants.ASMT_MATH_REF_TABLE,
                                                                                Constants.ASMT_MATH_REF_TABLE_SPRING_2015,
                                                                                Constants.ASMT_MATH_REF_TABLE_FALL_2015,
                                                                                Constants.ASMT_MATH_REF_TABLE_SPRING_2016,
                                                                                ),
                                                Constants.LOAD_TYPE_ASMT_ELA: (Constants.ASMT_ELA_REF_TABLE,
                                                                               Constants.ASMT_ELA_REF_TABLE_SPRING_2015,
                                                                               Constants.ASMT_ELA_REF_TABLE_FALL_2015,
                                                                               Constants.ASMT_ELA_REF_TABLE_SPRING_2016,
                                                                               ),
                                                Constants.LOAD_TYPE_PSYCHOMETRIC: Constants.ASMT_PSYCHOMETRIC_REF_TABLE,
                                                Constants.LOAD_TYPE_ITEM: Constants.SUMMATIVE_ITEM_REF_TABLE,
                                                Constants.LOAD_TYPE_MYA_ELA_ITEM: Constants.MYA_ELA_ITEM_REF_TABLE,
                                                Constants.LOAD_TYPE_MYA_MATH_ITEM: Constants.MYA_MATH_ITEM_REF_TABLE,
                                                Constants.LOAD_TYPE_MYA_ASMT_ELA: Constants.MYA_ELA_REF_TABLE,
                                                Constants.LOAD_TYPE_MYA_ASMT_MATH: Constants.MYA_MATH_REF_TABLE,
                                                Constants.LOAD_TYPE_SNL_ASMT: Constants.SNL_REF_TABLE,
                                                Constants.LOAD_TYPE_SNL_STUDENT_TASK: Constants.SNL_STUDENT_TASK_REF_TABLE,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_READING_COMP: Constants.ELA_ITEM_READING_COMP_REF_TABLE,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_VOCAB: Constants.ELA_ITEM_VOCAB_REF_TABLE,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_DECODING: Constants.ELA_ITEM_DECODING_REF_TABLE,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_ELA_VOCAB: Constants.ELA_VOCAB_REF_TABLE,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_ELA_COMP: Constants.ELA_READ_COMP_REF_TABLE,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_ELA_DECODING: Constants.ELA_DECODING_REF_TABLE,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_ELA_WRITING: Constants.ELA_WRITING_REF_TABLE,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_ELA_READ_FLU: Constants.ELA_READ_FLU_REF_TABLE,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_PROGRESSION: Constants.MATH_ITEM_PROGRESSION_REF_TABLE,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_COMPREHENSION: Constants.MATH_ITEM_COMPREHENSION_REF_TABLE,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_FLUENCY: Constants.MATH_ITEM_FLUENCY_REF_TABLE,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_MATH_LOCATOR: Constants.MATH_LOCATOR_REF_TABLE,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_MATH_PROGRESSION: Constants.MATH_PROGRESS_REF_TABLE,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_MATH_FLUENCY: Constants.MATH_FLU_REF_TABLE,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_MATH_GRADE_LEVEL: Constants.MATH_GRADE_LEVEL_REF_TABLE,
                                                Constants.LOAD_TYPE_DIAGNOSTIC_MATH_CLUSTER: Constants.MATH_CLUSTER_REF_TABLE,
                                                Constants.LOAD_TYPE_READER_MOTIVATION: Constants.READER_MOTIV_REF_TABLE,
                                                Constants.LOAD_TYPE_ELA_ITEM: Constants.SUMMATIVE_ELA_ITEM_REF_TABLE,
                                                Constants.LOAD_TYPE_MATH_ITEM: Constants.SUMMATIVE_MATH_ITEM_REF_TABLE}.get(load_type, None)

    UDL2_SUMMATIVE_REF_TABLE_ROADMAP = lambda load_type: {
        Constants.ASMT_MATH_REF_TABLE_SPRING_2015: Constants.ASMT_MATH_REF_TABLE,
        Constants.ASMT_MATH_REF_TABLE_FALL_2015: Constants.ASMT_MATH_REF_TABLE,
        Constants.ASMT_MATH_REF_TABLE_SPRING_2016: Constants.ASMT_MATH_REF_TABLE,
        Constants.ASMT_ELA_REF_TABLE_SPRING_2015: Constants.ASMT_ELA_REF_TABLE,
        Constants.ASMT_ELA_REF_TABLE_FALL_2015: Constants.ASMT_ELA_REF_TABLE,
        Constants.ASMT_ELA_REF_TABLE_SPRING_2016: Constants.ASMT_ELA_REF_TABLE}.get(load_type, None)

    # Summative schemas can be changed in future, so we need to check which one is valid and which one is deprecated.
    VALID_SUMMATIVE_LOAD_TYPES = [ASMT_MATH_REF_TABLE_FALL_2015, ASMT_MATH_REF_TABLE_SPRING_2016,
                                  ASMT_ELA_REF_TABLE_FALL_2015, ASMT_ELA_REF_TABLE_SPRING_2016]

    TRANSLATION_SUMMATIVE_LOAD_TYPES_ROADMAP = {ASMT_MATH_REF_TABLE_SPRING_2015: ASMT_MATH_REF_TABLE_FALL_2015,
                                                ASMT_ELA_REF_TABLE_SPRING_2015: ASMT_ELA_REF_TABLE_FALL_2015}

    TENANT_SEQUENCE_NAME = lambda tenant: Constants.SEQUENCE_NAME + '_' + tenant if tenant is not None and len(tenant) > 0 else None

    # Json schema names
    SUMM_ASMT_SCHEMA = 'summative_assessment_metadata_schema.json'
    SUMM_ITEM_SCHEMA = 'summative_item_schema.json'
    SUMM_PSYCHO_SCHEMA = 'summative_psychometric_schema.json'
    MYA_ITEM_SCHEMA = 'mya_item_schema.json'
    MYA_SCHEMA = 'mya_schema.json'
    SNL_ASMT_SCHEMA = 'snl_assessment_schema.json'
    SNL_STUDENT_TASK_SCHEMA = 'snl_student_task_schema.json'
    ITEM_ELA_READING_COMP_SCHEMA = 'item_ela_reading_comp_schema.json'
    ITEM_ELA_VOCAB_SCHEMA = 'item_ela_vocab_schema.json'
    ITEM_ELA_DECODING_SCHEMA = 'item_ela_decoding_schema.json'
    MATH_ITEM_PROGRESSION_SCHEMA = 'item_math_progress_schema.json'
    MATH_ITEM_COMPREHENSION_SCHEMA = 'item_math_comp_schema.json'
    MATH_ITEM_FLUENCY_SCHEMA = 'item_math_flu_schema.json'
    READER_MOTIV_SCHEMA = 'reader_motivation_schema.json'
    BASIC_SCHEMA = 'file_type_schema.json'

    JSON_SCHEMA_MAPPING = lambda load_type: {Constants.LOAD_TYPE_ASMT_MATH: Constants.SUMM_ASMT_SCHEMA,
                                             Constants.LOAD_TYPE_ASMT_ELA: Constants.SUMM_ASMT_SCHEMA,
                                             Constants.LOAD_TYPE_PSYCHOMETRIC: Constants.SUMM_PSYCHO_SCHEMA,
                                             Constants.LOAD_TYPE_ITEM: Constants.SUMM_ITEM_SCHEMA,
                                             Constants.LOAD_TYPE_MYA_ELA_ITEM: Constants.MYA_ITEM_SCHEMA,
                                             Constants.LOAD_TYPE_MYA_ASMT_ELA: Constants.MYA_SCHEMA,
                                             Constants.LOAD_TYPE_MYA_ASMT_MATH: Constants.MYA_SCHEMA,
                                             Constants.LOAD_TYPE_MYA_MATH_ITEM: Constants.MYA_ITEM_SCHEMA,
                                             Constants.LOAD_TYPE_SNL_ASMT: Constants.SNL_ASMT_SCHEMA,
                                             Constants.LOAD_TYPE_SNL_STUDENT_TASK: Constants.SNL_STUDENT_TASK_SCHEMA,
                                             Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_READING_COMP: Constants.ITEM_ELA_READING_COMP_SCHEMA,
                                             Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_VOCAB: Constants.ITEM_ELA_VOCAB_SCHEMA,
                                             Constants.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_DECODING: Constants.ITEM_ELA_DECODING_SCHEMA,
                                             Constants.LOAD_TYPE_DIAGNOSTIC_ELA_VOCAB: Constants.BASIC_SCHEMA,
                                             Constants.LOAD_TYPE_DIAGNOSTIC_ELA_COMP: Constants.BASIC_SCHEMA,
                                             Constants.LOAD_TYPE_DIAGNOSTIC_ELA_DECODING: Constants.BASIC_SCHEMA,
                                             Constants.LOAD_TYPE_DIAGNOSTIC_ELA_WRITING: Constants.BASIC_SCHEMA,
                                             Constants.LOAD_TYPE_DIAGNOSTIC_ELA_READ_FLU: Constants.BASIC_SCHEMA,
                                             Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_PROGRESSION: Constants.MATH_ITEM_PROGRESSION_SCHEMA,
                                             Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_COMPREHENSION: Constants.MATH_ITEM_COMPREHENSION_SCHEMA,
                                             Constants.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_FLUENCY: Constants.MATH_ITEM_FLUENCY_SCHEMA,
                                             Constants.LOAD_TYPE_DIAGNOSTIC_MATH_LOCATOR: Constants.BASIC_SCHEMA,
                                             Constants.LOAD_TYPE_DIAGNOSTIC_MATH_PROGRESSION: Constants.BASIC_SCHEMA,
                                             Constants.LOAD_TYPE_DIAGNOSTIC_MATH_FLUENCY: Constants.BASIC_SCHEMA,
                                             Constants.LOAD_TYPE_DIAGNOSTIC_MATH_GRADE_LEVEL: Constants.BASIC_SCHEMA,
                                             Constants.LOAD_TYPE_DIAGNOSTIC_MATH_CLUSTER: Constants.BASIC_SCHEMA,
                                             Constants.LOAD_TYPE_READER_MOTIVATION: Constants.READER_MOTIV_SCHEMA,
                                             Constants.LOAD_TYPE_ELA_ITEM: Constants.SUMM_ITEM_SCHEMA,
                                             Constants.LOAD_TYPE_MATH_ITEM: Constants.SUMM_ITEM_SCHEMA}.get(load_type, None)
