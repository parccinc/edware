'''
Worker to load assessment json data from a json file
to the staging table.

Main Celery Task:
method: task(msg)
name: "udl2.W_load_json_to_staging.task"
msg parameter requires the following:
'file_to_load', 'guid_batch'

Error Handler:
method: error_handler()
name: "udl2.W_load_json_to_staging.error_handler"
'''

from __future__ import absolute_import
import datetime

from celery.utils.log import get_task_logger
from edudl2.udl2.celery import udl2_conf, celery
from edudl2.udl2 import message_keys as mk
from edudl2.udl2.udl2_base_task import Udl2BaseTask
from edudl2.udl2_util import file_util
from edudl2.fileloader.json_loader import load_json
from edudl2.udl2_util.measurement import BatchTableBenchmark
from edudl2.data_validator import data_validator_util
from edudl2.udl2.constants import Constants

logger = get_task_logger(__name__)


@celery.task(name="udl2.W_load_json_to_staging.task", base=Udl2BaseTask)
def task(msg):
    start_time = datetime.datetime.now()
    guid_batch = msg.get(mk.GUID_BATCH)
    tenant_directory_paths = msg.get(mk.TENANT_DIRECTORY_PATHS)
    expanded_dir = tenant_directory_paths.get(mk.EXPANDED)
    load_type = msg[mk.LOAD_TYPE]
    tenant_name = msg[mk.TENANT_NAME]
    schema_version = msg.get(mk.SCHEMA_VERSION)

    json_file = file_util.get_file_type_from_dir('.json', expanded_dir)
    logger.info('LOAD_JSON_TO_STAGING: Loading json file <%s>' % json_file)
    conf = generate_conf_for_loading(json_file, guid_batch, load_type, tenant_name, schema_version)

    affected_rows = load_json(conf)

    end_time = datetime.datetime.now()
    # record benchmark
    benchmark = BatchTableBenchmark(guid_batch, msg.get(mk.LOAD_TYPE), task.name, start_time, end_time,
                                    task_id=str(task.request.id),
                                    working_schema=conf[mk.TARGET_DB_SCHEMA],
                                    size_records=affected_rows, tenant=msg[mk.TENANT_NAME])
    benchmark.record_benchmark()
    return msg


def generate_conf_for_loading(json_file, guid_batch, load_type, tenant_name, schema_version):
    '''
    takes the msg and pulls out the relevant parameters to pass
    the method that loads the json
    '''
    results = data_validator_util.get_source_target_column_values_from_ref_column_mapping(Constants.UDL2_JSON_LZ_TABLE, load_type)
    if isinstance(results, dict):
        results = results[schema_version]
    conf = {
        mk.FILE_TO_LOAD: json_file,
        mk.REF_TABLE: Constants.UDL2_REF_MAPPING_TABLE(load_type),
        mk.LOAD_TYPE: load_type,
        mk.MAPPINGS: dict([(row[0], row[1].split('.')) for row in results]),
        mk.TARGET_DB_SCHEMA: udl2_conf['udl2_db_conn']['db_schema'],
        mk.GUID_BATCH: guid_batch,
        mk.TENANT_NAME: tenant_name
    }
    if load_type in (Constants.LOAD_TYPE_ASMT_MATH, Constants.LOAD_TYPE_ASMT_ELA):
        ref_table = Constants.UDL2_SUMMATIVE_REF_TABLE_ROADMAP(schema_version)
        conf.update({mk.REF_TABLE: ref_table,
                     mk.SCHEMA_VERSION: schema_version})
    return conf
