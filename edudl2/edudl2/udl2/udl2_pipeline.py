from abc import ABCMeta, abstractmethod
import datetime
from edcore.database.utils.constants import UdlStatsConstants
from edudl2.udl2.celery import udl2_conf
from edudl2.udl2 import message_keys as mk
from edcore.utils.utils import merge_dict
from uuid import uuid4
__author__ = 'swimberly'


class Pipeline(metaclass=ABCMeta):

    @staticmethod
    def create(module_name=None):
        if module_name is None:
            module_name = udl2_conf['project']['pipeline']['module_name']
        module_path = module_name.split('.')
        class_name = module_path.pop()

        module = __import__('.'.join(module_path), fromlist=[class_name])
        class_ = getattr(module, class_name)

        return class_()

    def get_pipeline_chain(self, archive_file, load_type='Unknown', file_parts=4, guid_batch=None, initial_msg=None):
        """
        Get the celery chain object that is the udl pipeline

        :param archive_file:
        :param load_type:
        :param file_parts:
        :param batch_guid_forced:
        :return: a celery chain that contains the pipeline tasks
        """
        # Prepare parameters for task msgs
        guid_batch = str(uuid4()) if guid_batch is None else guid_batch
        lzw = udl2_conf['zones']['work']

        # generate common message for each stage
        arrival_msg = self._generate_common_message(guid_batch, load_type, file_parts, archive_file, lzw, initial_msg)

        return self.get_pipeline(arrival_msg)

    @abstractmethod
    def get_pipeline(self, arrival_msg):
        return

    def _generate_common_message(self, guid_batch, load_type, file_parts, archive_file, lzw, initial_msg):
        initial_msg = {} if initial_msg is None else initial_msg
        msg = {
            mk.GUID_BATCH: guid_batch,
            mk.LOAD_TYPE: load_type,
            mk.PARTS: file_parts,
            mk.START_TIMESTAMP: datetime.datetime.now(),
            mk.INPUT_FILE_PATH: archive_file,
            mk.LANDING_ZONE_WORK_DIR: lzw,
        }
        return merge_dict(initial_msg, msg)
