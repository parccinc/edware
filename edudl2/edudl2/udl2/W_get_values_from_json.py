from __future__ import absolute_import
from celery.utils.log import get_task_logger
import datetime
from edudl2.udl2.celery import celery
from edudl2.udl2 import message_keys as mk
from edudl2.udl2.udl2_base_task import Udl2BaseTask
from edudl2.get_values_from_json.get_values_from_json import get_values_from_json
from edudl2.udl2_util.measurement import BatchTableBenchmark
from edcore.database.utils.constants import UdlStatsConstants
from edcore.database.utils.query import update_udl_stats_by_batch_guid
from edudl2.udl2.constants import Constants

__author__ = 'tshewchuk'

logger = get_task_logger(__name__)
subject_dependant_load_type = Constants.SUBJECT_DEPENDANT_LOAD_TYPES()


@celery.task(name="udl2.W_get_values_from_json.task", base=Udl2BaseTask)
def task(incoming_msg):
    start_time = datetime.datetime.now()
    guid_batch = incoming_msg.get(mk.GUID_BATCH)
    tenant_directory_paths = incoming_msg.get(mk.TENANT_DIRECTORY_PATHS)
    expanded_dir = tenant_directory_paths.get(mk.EXPANDED)

    values = get_values_from_json(expanded_dir)

    load_type = values[mk.LOAD_TYPE]
    period = values[mk.PERIOD]

    logger.info('W_GET_VALUES_FROM_JSON: Load type is <{load_type}>'.format(load_type=load_type))
    if period:
        logger.info('W_GET_VALUES_FROM_JSON: Period is <{period}>'.format(period=period))
    end_time = datetime.datetime.now()

    # benchmark
    benchmark = BatchTableBenchmark(guid_batch, load_type, task.name, start_time, end_time, task_id=str(task.request.id),
                                    tenant=incoming_msg.get(mk.TENANT_NAME))
    benchmark.record_benchmark()

    # Outgoing message to be piped to the file translator
    outgoing_msg = {}
    outgoing_msg.update(incoming_msg)
    outgoing_msg.update({mk.LOAD_TYPE: load_type})
    if period:
        outgoing_msg.update({mk.PERIOD: period})
    # Update UDL stats
    update_udl_stats_by_batch_guid(guid_batch, {UdlStatsConstants.LOAD_TYPE: load_type})
    return outgoing_msg
