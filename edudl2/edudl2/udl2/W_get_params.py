from __future__ import absolute_import
from celery.utils.log import get_task_logger
import datetime
from edudl2.get_params.get_params import get_academic_year_param
from edudl2.udl2.celery import celery
from edudl2.udl2 import message_keys as mk
from edudl2.udl2.udl2_base_task import Udl2BaseTask
from edudl2.udl2_util.measurement import BatchTableBenchmark

__author__ = 'ablum'
logger = get_task_logger(__name__)


@celery.task(name="udl2.W_get_params.task", base=Udl2BaseTask)
def task(msg):
    start_time = datetime.datetime.now()
    guid_batch = msg[mk.GUID_BATCH]

    tenant_directory_paths = msg[mk.TENANT_DIRECTORY_PATHS]
    expanded_dir = tenant_directory_paths[mk.EXPANDED]

    academic_year = get_academic_year_param(expanded_dir, msg[mk.LOAD_TYPE])

    end_time = datetime.datetime.now()

    benchmark = BatchTableBenchmark(guid_batch, msg[mk.LOAD_TYPE], task.name, start_time, end_time, task_id=str(task.request.id), tenant=msg[mk.TENANT_NAME])
    benchmark.record_benchmark()

    outgoing_msg = {}
    outgoing_msg.update(msg)
    outgoing_msg.update({mk.ACADEMIC_YEAR: academic_year})

    return outgoing_msg
