from __future__ import absolute_import
import datetime

from celery.utils.log import get_task_logger
from edudl2.udl2.celery import udl2_conf, celery
from edudl2.udl2 import message_keys as mk
from edudl2.udl2.udl2_base_task import Udl2BaseTask
from edudl2.udl2_util import file_util
from edudl2.fileloader.xml_loader import load_xml
from edudl2.udl2_util.measurement import BatchTableBenchmark
from edudl2.data_validator import data_validator_util
from edudl2.udl2.constants import Constants

__author__ = "npandey"

logger = get_task_logger(__name__)


@celery.task(name="udl2.W_load_xml_to_loader.task", base=Udl2BaseTask)
def task(msg):
    start_time = datetime.datetime.now()
    guid_batch = msg.get(mk.GUID_BATCH)
    load_type = msg.get(mk.LOAD_TYPE)
    tenant_name = msg.get(mk.TENANT_NAME)
    tenant_directory_paths = msg.get(mk.TENANT_DIRECTORY_PATHS)
    expanded_dir = tenant_directory_paths.get(mk.EXPANDED)

    xml_file = file_util.get_file_type_from_dir('.xml', expanded_dir)

    logger.info('LOAD_XML_LOADER: Loading xml file <%s>' % xml_file)
    conf = generate_conf_for_loading(xml_file, guid_batch, load_type, tenant_name)
    batch_size = udl2_conf['xml_parser']['batch_size']
    affected_rows = load_xml(conf, batch_size)
    end_time = datetime.datetime.now()

    # record benchmark
    benchmark = BatchTableBenchmark(guid_batch, load_type, task.name, start_time, end_time,
                                    task_id=str(task.request.id),
                                    working_schema=conf[mk.TARGET_DB_SCHEMA],
                                    size_records=affected_rows, tenant=msg[mk.TENANT_NAME])
    benchmark.record_benchmark()
    return msg


def generate_conf_for_loading(xml_file, guid_batch, load_type, tenant_name):
    '''
    takes the msg and pulls out the relevant parameters to pass
    the method that loads the xml
    '''
    results = data_validator_util.get_source_target_column_values_from_ref_column_mapping(Constants.UDL2_XML_LZ_TABLE, load_type)
    conf = {
        mk.FILE_TO_LOAD: xml_file,
        mk.MAPPINGS: results,
        mk.REF_TABLE: Constants.UDL2_REF_MAPPING_TABLE(load_type),
        mk.TARGET_DB_SCHEMA: udl2_conf['udl2_db_conn']['db_schema'],
        mk.TARGET_DB_TABLE: Constants.LDR_ITEM_RESULT,
        mk.GUID_BATCH: guid_batch,
        mk.TENANT_NAME: tenant_name
    }
    return conf
