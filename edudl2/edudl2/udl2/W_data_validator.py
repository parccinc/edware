from __future__ import absolute_import
from celery.utils.log import get_task_logger
import datetime
import os
from edudl2.udl2.celery import udl2_conf, celery
from edudl2.udl2 import message_keys as mk
from edudl2.udl2.udl2_base_task import Udl2BaseTask
from edudl2.data_validator.data_validator import DataValidator
from edudl2.udl2_util.measurement import BatchTableBenchmark

logger = get_task_logger(__name__)


@celery.task(name="udl2.W_data_validator.task", base=Udl2BaseTask)
def task(incoming_msg):
    start_time = datetime.datetime.now()
    guid_batch = incoming_msg[mk.GUID_BATCH]
    load_type = incoming_msg[mk.LOAD_TYPE]
    schema_version = incoming_msg['schema_version']

    tenant_directory_paths = incoming_msg[mk.TENANT_DIRECTORY_PATHS]
    expanded_dir = tenant_directory_paths[mk.EXPANDED]

    dv = DataValidator()
    error_map = {}
    for file_name in os.listdir(expanded_dir):
        error_map[file_name] = dv.execute(expanded_dir, file_name, guid_batch, load_type, schema_version)

    for input_file in error_map.keys():
        errors = error_map[input_file]
        if len(errors) == 0:
            logger.info('DATA VALIDATOR: Validated file <%s> and found no errors.' % (os.path.join(expanded_dir, input_file)))
        else:
            for error in errors:
                logger.error('ERROR: ' + str(error))
            raise Exception('simple file validator error: %s' % errors)

    end_time = datetime.datetime.now()

    # benchmark
    benchmark = BatchTableBenchmark(guid_batch, incoming_msg[mk.LOAD_TYPE], task.name, start_time, end_time, task_id=str(task.request.id),
                                    tenant=incoming_msg[mk.TENANT_NAME])
    benchmark.record_benchmark()

    # Outgoing message to be piped to the file splitter
    outgoing_msg = {}
    outgoing_msg.update(incoming_msg)
    return outgoing_msg


# TODO: Actually implement get_number_of_parts()
def get_number_of_parts():
    return 4
