'''
Created on May 22, 2013

@author: ejen
'''
from __future__ import absolute_import
import datetime

from celery.utils.log import get_task_logger
from edudl2.udl2.celery import udl2_conf, celery
from edudl2.udl2 import message_keys as mk
from edudl2.udl2.udl2_base_task import Udl2BaseTask
from edudl2.move_to_integration.move_to_integration import move_data_from_staging_to_integration
from edudl2.udl2_util.database_util import get_db_connection_params
from edudl2.udl2_util.measurement import BatchTableBenchmark
from edudl2.udl2.constants import Constants

logger = get_task_logger(__name__)


@celery.task(name="udl2.W_load_to_integration_table.task", base=Udl2BaseTask)
def task(msg):
    start_time = datetime.datetime.now()
    logger.info("LOAD_FROM_STAGING_TO_INT: Migrating data from staging to integration.")
    guid_batch = msg[mk.GUID_BATCH]
    load_type = msg[mk.LOAD_TYPE]
    tenant_name = msg[mk.TENANT_NAME]
    schema_version = msg.get(mk.SCHEMA_VERSION)
    conf = generate_conf(guid_batch, load_type, tenant_name, schema_version)
    affected_rows = move_data_from_staging_to_integration(conf)
    end_time = datetime.datetime.now()

    # benchmark
    benchmark = BatchTableBenchmark(guid_batch, load_type, task.name, start_time, end_time, size_records=affected_rows,
                                    task_id=str(task.request.id), working_schema=conf[mk.TARGET_DB_SCHEMA], tenant=tenant_name)
    benchmark.record_benchmark()

    # Outgoing message to be piped to the file expander
    outgoing_msg = {}
    outgoing_msg.update(msg)
    outgoing_msg.update({mk.PHASE: 4, mk.TOTAL_ROWS_LOADED: affected_rows})
    return outgoing_msg


def generate_conf(guid_batch, load_type, tenant, schema_version):
    db_params_tuple = get_db_connection_params(udl2_conf['udl2_db_conn']['url'])

    conf = {
        # add guid_batch from msg
        mk.GUID_BATCH: guid_batch,
        # source schema
        mk.SOURCE_DB_SCHEMA: udl2_conf['udl2_db_conn']['db_schema'],
        # source database setting
        mk.SOURCE_DB_DRIVER: db_params_tuple[0],
        mk.SOURCE_DB_USER: db_params_tuple[1],
        mk.SOURCE_DB_PASSWORD: db_params_tuple[2],
        mk.SOURCE_DB_HOST: db_params_tuple[3],
        mk.SOURCE_DB_PORT: db_params_tuple[4],
        mk.SOURCE_DB_NAME: db_params_tuple[5],
        mk.TARGET_DB_SCHEMA: guid_batch,
        mk.REF_TABLE: Constants.UDL2_REF_MAPPING_TABLE(load_type),
        mk.PHASE: 3,
        mk.TENANT_NAME: tenant,
        mk.PROD_DB_SCHEMA: udl2_conf['prod_db_conn'][tenant]['db_schema']
    }

    if load_type in (Constants.LOAD_TYPE_ASMT_MATH, Constants.LOAD_TYPE_ASMT_ELA):
        ref_table = Constants.UDL2_SUMMATIVE_REF_TABLE_ROADMAP(schema_version)
        conf.update({mk.REF_TABLE: ref_table,
                     mk.SCHEMA_VERSION: schema_version})

    return conf
