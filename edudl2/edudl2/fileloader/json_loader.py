'''
Implementation for loading data from a json file to a table.

Typically usage would be to load data from an assessment json
file to the integration table.

Main Method: load_json(conf)

Created on May 16, 2013

@author: swimberly
'''

from requests.structures import CaseInsensitiveDict
from edudl2.fileloader.value_builder import ValueBuilder
from edudl2.fileloader.json_parser import JsonParser
import edudl2.udl2_util.database_util as db_util
from edudl2.udl2 import message_keys as mk
from edudl2.database.udl2_connector import get_udl_connection
from sqlalchemy.sql.expression import select, and_
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)


def load_json(conf):
    '''
    Main method for loading json into the integration table
    @param conf: The configuration dictionary
    '''

    table_data_map = JsonParser(load_type=conf[mk.LOAD_TYPE]).parse_json(conf.get(mk.FILE_TO_LOAD))
    total_affected_rows = 0
    for table in table_data_map:
        affected_rows = load_to_table(table_data_map[table], conf.get(mk.GUID_BATCH),
                                      table, conf.get(mk.TENANT_NAME),
                                      conf.get(mk.TARGET_DB_SCHEMA), conf.get(mk.REF_TABLE))
        total_affected_rows += affected_rows
    return total_affected_rows


def flatten_fields(json_dict):
    '''
    flattens a list of maps into the parent object
    @param json_dict: the dictionary containing the json data
    @param mappings: A dictionary with values indicate the location of the value
    @return: A dictionary of columns mapped to values
    @rtype: dict
    '''

    flat_data = {}
    for key in json_dict:
        if isinstance(json_dict[key], CaseInsensitiveDict):
            flat_map = flatten_fields(json_dict[key])
            for k, v in flat_map.items():
                new_key = '{key}.{subkey}'.format(key=key, subkey=k)
                flat_data[new_key] = v
        if isinstance(json_dict[key], list):
            count = 1
            for map in json_dict[key]:
                for k, v in map.items():
                    new_key = '{key}[{count}].{subkey}'.format(key=key, subkey=k, count=count)
                    flat_data[new_key] = v
                count += 1

    json_dict.update(flat_data)
    return json_dict


def load_to_table(data_list, guid_batch, stg_table, tenant_name, udl_schema, ref_table):
    '''
    Load the table into the proper table
    @param data_dict: the dictionary containing the data to be loaded
    @param guid_batch: the id for the batch
    @param stg_table: the name of the staging table
    @param tenant_name: name of the tenant
    @param udl_schema: udl schema name
    '''
    # Create sqlalchemy connection and get table information from sqlalchemy
    insert_values = []
    with get_udl_connection() as conn:
        s_stg_table = conn.get_table(stg_table)
        column_mapping_query = get_mapping_info(conn, ref_table, stg_table)
        column_mapping_info = conn.get_result(column_mapping_query)
        value_builder = ValueBuilder(guid_batch, tenant_name, udl_schema, column_mapping_info)
        for data_dict in data_list:
            data_dict = fix_empty_strings(data_dict)
            data_dict = flatten_fields(data_dict)

            insert_value = value_builder.build(data_dict, s_stg_table)

            insert_values.append(insert_value)

        insert_into_stg_table = s_stg_table.insert().values(insert_values)

        affected_row = db_util.execute_udl_queries(conn, [insert_into_stg_table],
                                                   'Exception in loading json data -- ',
                                                   'json_loader', 'load_to_table')
    return affected_row[0]


def get_mapping_info(conn, ref_table, stg_table):
    ref_table = conn.get_table(ref_table)
    column_mapping_query = select([ref_table.c.target_column,
                                   ref_table.c.source_column,
                                   ref_table.c.stored_proc_name],
                                  from_obj=ref_table).where(and_(ref_table.c.source_table == 'lz_json',
                                                                 ref_table.c.target_table == stg_table))
    return column_mapping_query


def fix_empty_strings(data_dict):
    ''' Replace values which are empty string with a reference to None '''
    for k, v in data_dict.items():
        if v == '':
            data_dict[k] = None
    return data_dict
