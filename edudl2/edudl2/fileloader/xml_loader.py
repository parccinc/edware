from edudl2.fileloader.summative_item_result_parser import SummativeItemResultParser
import edudl2.udl2_util.database_util as db_util
from edudl2.udl2 import message_keys as mk
from edudl2.database.udl2_connector import get_udl_connection
from sqlalchemy.sql.expression import select, and_
from celery.utils.log import get_task_logger
from psycopg2._psycopg import AsIs, QuotedString
from edudl2.udl2.constants import Constants

logger = get_task_logger(__name__)


def load_xml(conf, batch_size):
    '''
    Main method for loading xml into the loader table
    @param conf: The configuration dictionary
    '''

    affected_rows = 0
    for table_data in SummativeItemResultParser(batch_size).parse_xml_to_data(conf.get(mk.FILE_TO_LOAD)):
        if table_data:
            affected_rows += load_to_table(table_data, conf.get(mk.GUID_BATCH),
                                           conf.get(mk.TARGET_DB_TABLE), conf.get(mk.TENANT_NAME),
                                           conf.get(mk.TARGET_DB_SCHEMA), conf.get(mk.REF_TABLE))
    return affected_rows


def load_to_table(data_list, guid_batch, ldr_table, tenant_name, udl_schema, ref_table):
    '''
    Load the table into the proper table
    @param data_dict: the dictionary containing the data to be loaded
    @param guid_batch: the id for the batch
    @param ldr_table: the name of the loader table
    @param tenant_name: name of the tenant
    @param udl_schema: udl schema name
    '''
    # Create sqlalchemy connection and get table information from sqlalchemy
    insert_values = []
    with get_udl_connection() as conn:
        s_ldr_table = conn.get_table(ldr_table)
        column_mapping_query = get_mapping_info(conn, ref_table, ldr_table)
        column_mapping_info = conn.get_result(column_mapping_query)

        for data_dict in data_list:
            insert_value = build_table_row(data_dict, s_ldr_table, column_mapping_info, udl_schema, guid_batch, tenant_name)
            insert_values.append(insert_value)

        insert_into_ldr_table = s_ldr_table.insert().values(insert_values)

        affected_row = db_util.execute_udl_queries(conn, [insert_into_ldr_table],
                                                   'Exception in loading xml data -- ',
                                                   'xml_loader', 'load_to_table')
    return affected_row[0]


def get_mapping_info(conn, ref_table, ldr_table):
    ref_table = conn.get_table(ref_table)
    column_mapping_query = select([ref_table.c.target_column,
                                   ref_table.c.source_column,
                                   ref_table.c.stored_proc_name],
                                  from_obj=ref_table).where(and_(ref_table.c.source_table == 'lz_xml',
                                                                 ref_table.c.target_table == ldr_table))
    return column_mapping_query


def build_table_row(data_dict, ldr_table, column_mapping_info, udl_schema, guid_batch, tenant_name):
    field_to_value_mappings = {}

    for column_map in column_mapping_info:
        target_column = column_map['target_column']
        source_column = column_map['source_column']
        stored_proc_name = column_map['stored_proc_name']
        value = data_dict.get(source_column)
        if target_column in ldr_table.columns:
            field_to_value_mappings[target_column] = add_trans_rule_to_value(stored_proc_name, value)
        add_metadata_columns(field_to_value_mappings, udl_schema, guid_batch, tenant_name)
    return field_to_value_mappings


def add_metadata_columns(field_to_value_mappings, udl_schema, guid_batch, tenant_name):
    record_sid = AsIs('nextval(\'{schema_name}.{tenant_sequence_name}\')'.format(schema_name=udl_schema, tenant_sequence_name=Constants.TENANT_SEQUENCE_NAME(tenant_name)))
    field_to_value_mappings['record_num'] = record_sid
    field_to_value_mappings['batch_guid'] = guid_batch


def add_trans_rule_to_value(trans_rule_name, value):
    formatted_value = AsIs(trans_rule_name + '(' + QuotedString(value if type(value) is str else str(value)).getquoted().decode('utf-8') + ')')
    return formatted_value
