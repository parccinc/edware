from edudl2.udl2.constants import Constants

__author__ = 'npandey'

PARSER_TABLE_MAPPING = lambda load_type: {Constants.LOAD_TYPE_ASMT_MATH: {"": (Constants.STG_TEST_SCORE, 'process_list'),
                                                                          ".test_levels": (Constants.STG_TEST_LEVEL, 'process_list')},
                                          Constants.LOAD_TYPE_ASMT_ELA: {"": (Constants.STG_TEST_SCORE, 'process_list'),
                                                                         ".test_levels": (Constants.STG_TEST_LEVEL, 'process_list')},
                                          Constants.LOAD_TYPE_MYA_ASMT_ELA: {"": (Constants.STG_ADMIN, 'process_list'),
                                                                             ".tests": (Constants.STG_ASMT, 'process_list'),
                                                                             ".tests.testLevel": (Constants.STG_ASMT_LVL, 'process_singular_list'),
                                                                             ".tests.forms": (Constants.STG_FORM, 'process_list'),
                                                                             ".tests.forms.itemGroups": (Constants.STG_FORM_GROUP, 'process_list'),
                                                                             ".tests.forms.itemGroups.items": (Constants.STG_ITEM_MYA_META, 'process_list')},
                                          Constants.LOAD_TYPE_MYA_ASMT_MATH: {"": (Constants.STG_ADMIN, 'process_list'),
                                                                              ".tests": (Constants.STG_ASMT, 'process_list'),
                                                                              ".tests.testLevel": (Constants.STG_ASMT_LVL, 'process_singular_list'),
                                                                              ".tests.forms": (Constants.STG_FORM, 'process_list'),
                                                                              ".tests.forms.itemGroups": (Constants.STG_FORM_GROUP, 'process_list'),
                                                                              ".tests.forms.itemGroups.items": (Constants.STG_ITEM_MYA_META, 'process_list')},
                                          Constants.LOAD_TYPE_SNL_ASMT: {"": (Constants.STG_ADMIN, 'process_list'),
                                                                         ".tests": (Constants.STG_SNL_ASMT, 'process_list'),
                                                                         ".tests.modes": (Constants.STG_SNL_ASMT_MODE, 'process_singular_list'),
                                                                         ".tests.testLevel": (Constants.STG_ASMT_LVL, 'process_singular_list'),
                                                                         ".tests.tasks": (Constants.STG_SNL_TASK, 'process_list')}
                                          }.get(load_type, None)

SINGULAR_LIST_TABLE_FIELD_MAPPING = lambda table_name: {Constants.STG_ASMT_LVL: "asmt_lvl_design",
                                                        Constants.STG_SNL_ASMT_MODE: "asmt_mode",
                                                        Constants.STG_DGNST_ASMT_LVL: "asmt_lvl_design"
                                                        }.get(table_name, None)
