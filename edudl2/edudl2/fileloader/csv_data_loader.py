from collections import namedtuple, defaultdict
import datetime
import csv
import logging
from sqlalchemy.sql.expression import select
import edudl2.udl2.message_keys as mk
from edudl2.udl2.constants import Constants
import edudl2.fileloader.prepare_queries as queries
from edudl2.udl2_util.database_util import execute_udl_queries
from edudl2.database.udl2_connector import get_udl_connection


logger = logging.getLogger(__name__)


class CsvLoader():

    DATA_TYPE_IN_FDW_TABLE = 'text'
    SubstitutionItems = namedtuple('SubstitutionItems', ['schema_version', 'final_version'])

    def __init__(self, conn, conf):
        self.conn = conn
        self.conf = conf

        self.csv_table = conf.get(mk.CSV_TABLE)
        self.csv_schema = conf.get(mk.CSV_SCHEMA)
        self.csv_lz_table = conf.get(mk.CSV_LZ_TABLE)
        self.csv_headers = conf.get(mk.HEADERS)
        self.ref_table = conf.get(mk.REF_TABLE)
        self.guid_batch = str(conf.get(mk.GUID_BATCH))
        self.schema_version = conf.get(mk.SCHEMA_VERSION)

        # to avoid multiple connections to the ref table
        self.ref_table_parameters = None
        self.schema_ref_table_parameters = None
        self.subst_map = None

        self.final_csv_table_columns = ['\'{}\''.format(self.guid_batch),
                                        'nextval(\'{global_tenant_seq_name}\')']
        self.final_loader_columns = ['batch_guid', 'record_num']
        self.final_transformation_rules = ['', '']

    def extract_csv_header_parameters(self):
        with open(self.csv_headers) as csv_obj:
            reader = csv.reader(csv_obj)
            file_csv_header_names = next(reader)
            header_types = [self.DATA_TYPE_IN_FDW_TABLE] * len(file_csv_header_names)

        #Case insensitive
        lowered_file_csv_header_names = [header.lower() for header in file_csv_header_names]

        table_csv_columns, table_ldr_columns, transformation_rules = self.get_csv_parameters_from_ref_table(self.ref_table)

        lowered_table_csv_columns = [header.lower() for header in table_csv_columns]
        # ref_table_csv_to_ldr_table_map = dict(zip(lowered_table_csv_columns, table_ldr_columns))
        ref_table_csv_to_ldr_table_map = tuple(zip(lowered_table_csv_columns, table_ldr_columns))

        lowered_substitution_map = None
        if self.schema_version:
            schema_table_csv_columns, schema_table_ldr_columns, schema_transformation_rules = self.get_csv_parameters_from_ref_table(self.schema_version)

            lowered_schema_table_csv_columns = [header.lower() for header in schema_table_csv_columns]

            self.check_diff(lowered_file_csv_header_names, lowered_schema_table_csv_columns)
            # particular_schema_csv_to_ldr_table_map = dict(zip(lowered_schema_table_csv_columns, schema_table_ldr_columns))
            particular_schema_csv_to_ldr_table_map = tuple(zip(lowered_schema_table_csv_columns, schema_table_ldr_columns))

            # self.subst_map = self.get_substitution_map(dict(zip(table_csv_columns, table_ldr_columns)),
            #                                            dict(zip(schema_table_csv_columns, schema_table_ldr_columns)))

            self.subst_map = self.get_substitution_map(tuple(zip(table_csv_columns, table_ldr_columns)),
                                                       tuple(zip(schema_table_csv_columns, schema_table_ldr_columns)))

            lowered_substitution_map = self.get_substitution_map(ref_table_csv_to_ldr_table_map, particular_schema_csv_to_ldr_table_map)

            self.schema_ref_table_parameters = {
                'csv_columns': schema_table_csv_columns,
                'ldr_columns': schema_table_ldr_columns,
                'transformation_rules': schema_transformation_rules
            }
        else:
            self.check_diff(lowered_file_csv_header_names, lowered_table_csv_columns)

        formatted_header_names = [self.canonicalize_header_field(name) for name in lowered_file_csv_header_names]

        self.ref_table_parameters = {
            'csv_columns': table_csv_columns,
            'ldr_columns': table_ldr_columns,
            'transformation_rules': transformation_rules
        }
        return formatted_header_names, header_types, lowered_substitution_map

    def prepare_fdw_tables(self, header_names, header_types, csv_file, csv_schema, csv_table, fdw_server):
        '''
        Create one foreign table which maps to the given csv_file on the given fdw_server
        '''
        create_csv_ddl = queries.create_ddl_csv_query(header_names, header_types, csv_file,
                                                      csv_schema, csv_table, fdw_server)
        drop_csv_ddl = queries.drop_ddl_csv_query(csv_schema, csv_table)
        # First drop the fdw table if exists, then create a new one
        return [drop_csv_ddl, create_csv_ddl]

    def get_substitution_map(self, summarized_fields, schema_fields):

        diff_items = set(schema_fields) - set(summarized_fields)
        # reversed_summarized_fields = {v: k for k, v in summarized_fields.items()}
        # substitution_map = {}
        # for element in diff_items:
        #     db_field = schema_fields[element]
        #     substitute_element = reversed_summarized_fields[db_field]
        #     substitution_map[element] = self.SubstitutionItems(element, substitute_element)
        # return substitution_map

        substitution_map = {}
        for schema_key, db_key in diff_items:
            substitute_element = list(filter(lambda x: x[1].startswith(db_key), summarized_fields))[0][0]
            substitution_map[db_key] = self.SubstitutionItems(schema_key, substitute_element)
        return substitution_map

    def get_csv_parameters_from_ref_table(self, ref_table):
        ref_table = self.conn.get_table(ref_table)
        ref_table_columns_query = select([ref_table.c.source_column,
                                          ref_table.c.target_column,
                                          ref_table.c.stored_proc_name]).where(ref_table.c.source_table == self.csv_lz_table)
        column_mapping_result = self.conn.execute(ref_table_columns_query)
        csv_columns = []
        table_columns = []
        transformation_rules = []
        if column_mapping_result:
            for mapping in column_mapping_result:
                csv_columns.append(mapping[0])
                table_columns.append(mapping[1])
                transformation_rules.append(mapping[2])
        return csv_columns, table_columns, transformation_rules

    @staticmethod
    def substitude_headers(header_names, substitution_map):
        if substitution_map and len(substitution_map) != 0:
            for i, header in enumerate(header_names):
                rule = substitution_map.get(header)
                if rule and rule.schema_version == header:
                    header_names[i] = rule.final_version
            return header_names
        else:
            return header_names

    @staticmethod
    def canonicalize_header_field(field_name):
        """
        Canonicalize input field_name
        """
        # TODO: rules TBD
        return field_name.replace('-', '_').replace(' ', '_').replace('#', '')

    def check_diff(self, csv_headers, table_headers):
        # TODO: Weak comparison, we have cases when csv have duplicate fields in headers, rewrite it
        diff_item = set(csv_headers) - set(table_headers)
        if len(diff_item) > 0:
            raise ValueError('Column {col} does not match between header '
                             'file and mapping defined in ref table {table}'.format(col=str(diff_item), table=self.ref_table))

    def create_fields_map(self, substitution_map):
        '''
        Getting field mapping, which maps the columns in loader table, and columns in csv table
        The mapping is defined in the given ref_table except for guid_batch and src_file_rec_num
        @return: loader_table_columns - list of columns in loader table
                 csv_table_columns - list of corresponding columns in csv table
                 transformation_rules - list of transformation rules for corresponding columns
        '''
        # get column mapping from ref table
        parameters = self.schema_ref_table_parameters if self.schema_ref_table_parameters \
            else self.ref_table_parameters
        # run substitution script here!
        csv_headers = parameters['csv_columns']
        csv_headers = self.substitude_headers(csv_headers, self.subst_map)

        self.final_csv_table_columns.extend(csv_headers)
        self.final_csv_table_columns = self.add_index_for_duplicate_fieldnames(self.final_csv_table_columns)
        self.final_loader_columns.extend(parameters['ldr_columns'])
        self.final_transformation_rules.extend(parameters['transformation_rules'])

    def prepare_import_via_fdw_queries(self):
        start_seq = self.conf.get(mk.ROW_START)
        apply_rules = self.conf.get(mk.APPLY_RULES)
        loader_schema = self.conf.get(mk.TARGET_DB_SCHEMA)
        loader_table = self.conf.get(mk.TARGET_DB_TABLE)

        seq_name = (self.csv_table + '_' + str(start_seq)).lower()

        global_tenant_seq_name = Constants.TENANT_SEQUENCE_NAME(self.conf.get(mk.TENANT_NAME))

        # query 1 -- create query to create sequence
        create_sequence = queries.create_sequence_query(loader_schema, seq_name, start_seq)
        # query 2 -- create query to load data from fdw to loader table
        insert_into_loader_table = queries.create_inserting_into_loader_query(self.final_loader_columns,
                                                                              apply_rules,
                                                                              self.final_csv_table_columns,
                                                                              loader_schema,
                                                                              loader_table, self.csv_schema,
                                                                              self.csv_table,
                                                                              seq_name,
                                                                              global_tenant_seq_name,
                                                                              self.final_transformation_rules)
        # query 3 -- create query to drop sequence
        drop_sequence = queries.drop_sequence_query(loader_schema, seq_name)

        return [create_sequence, insert_into_loader_table, drop_sequence]

    def drop_fdw_tables(self):
        '''
        Drop foreign table
        '''
        drop_csv_ddl = queries.drop_ddl_csv_query(self.csv_schema, self.csv_table)
        execute_udl_queries(self.conn, [drop_csv_ddl],
                            'Exception in drop fdw table -- ', 'file_loader', 'drop_fdw_tables')

    def load_data_process(self):
        """
        Load data from csv to loader table. The database connection and configuration information are provided
        """
        header_names, header_types, substitution_map = self.extract_csv_header_parameters()
        header_names = self.substitude_headers(header_names, substitution_map)
        header_names = self.add_index_for_duplicate_fieldnames(header_names)

        # prepare FDW table queries
        fdw_queries = self.prepare_fdw_tables(header_names, header_types, self.conf.get(mk.FILE_TO_LOAD),
                                              self.csv_schema, self.csv_table, self.conf.get(mk.FDW_SERVER))

        execute_udl_queries(self.conn, fdw_queries,
                            'Exception in creating fdw tables --', 'file_loader', 'create_fdw_tables')
        # create field map
        self.create_fields_map(substitution_map)
        # load the data from FDW table to loader table
        start_time = datetime.datetime.now()
        try:
            import_queries = self.prepare_import_via_fdw_queries()
            execute_udl_queries(self.conn, import_queries,
                                'Exception in loading data -- ', 'file_loader', 'import_via_fdw')
        except:
            pass  # TODO: need to create some exception to avoid endless "celery.chord_unlock" loop
        finally:
            # drop FDW table
            self.drop_fdw_tables()

        finish_time = datetime.datetime.now()
        spend_time = finish_time - start_time
        time_as_seconds = float(spend_time.seconds + spend_time.microseconds / 1000000.0)
        return time_as_seconds

    @staticmethod
    def add_index_for_duplicate_fieldnames(header_names):
        headers_check = defaultdict(list)
        for i, header in enumerate(header_names):
            headers_check[header].append(i)

        duplicated = list(filter(lambda x: len(x[1]) != 1, headers_check.items()))
        for field, field_indexes in duplicated:
            str_index = 1
            for index in field_indexes:
                header_names[index] = '{}{}'.format(field, str_index)
                str_index += 1
        return header_names


def load_file(conf):
    '''
    Main function to initiate file loader
    '''
    logger.info("Starting data load from csv to loader")
    with get_udl_connection() as conn:
        # start loading file process
        csv_loader = CsvLoader(conn, conf)
        time_for_load_as_seconds = csv_loader.load_data_process()
    logger.info("Data Loaded from csv to Loader table in %s seconds" % time_for_load_as_seconds)
