from edudl2.database.udl2_connector import get_target_connection
from edudl2.udl2.celery import udl2_conf
from edudl2.udl2 import message_keys as mk
from sqlalchemy.dialects.postgres import BYTEA
from sqlalchemy import cast
import datetime
import ntpath


def load_content_to_integration(file_name, conf, identification_data):
    with get_target_connection(conf[mk.TENANT_NAME], conf[mk.TARGET_DB_SCHEMA]) as conn:
        move_content(file_name, conf, identification_data, conn)


def move_content(file_name, conf, identification_data, conn):

    integration_table = conn.get_table(conf[mk.TARGET_DB_TABLE])
    file_name_parts = ntpath.basename(file_name).split('.')
    filename = file_name_parts[0]
    filetype = file_name_parts[1]

    with open(file_name, 'r') as f:
        page_no = 0
        for piece in read_in_chunks(f):
            table_values = dict(file_data=piece,  # cast(piece, BYTEA),
                                admin_guid=identification_data["admin_guid"],
                                asmt_guid=identification_data["asmt_guid"],
                                create_date=datetime.datetime.now(),
                                file_name=filename,
                                file_type=filetype,
                                batch_guid=conf[mk.GUID_BATCH],
                                page_no=page_no)
            insert_query = integration_table.insert().values(**table_values)
            conn.execute(insert_query)
            page_no += 1


def read_in_chunks(file_object, chunk_size=udl2_conf.get('udl_chunk_size')):
    """Lazy function (generator) to read a file piece by piece."""
    while True:
        data = file_object.read(chunk_size).encode('utf-8')
        if not data:
            break
        yield data
