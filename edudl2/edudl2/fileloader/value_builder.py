from psycopg2._psycopg import AsIs, QuotedString
from edudl2.udl2.constants import Constants

__author__ = 'ablum'


class ValueBuilder():

    def __init__(self, guid_batch, tenant_name, udl_schema, column_mapping_info):
        self.guid_batch = guid_batch
        self.tenant_name = tenant_name
        self.udl_schema = udl_schema
        self.column_mapping_info = column_mapping_info

    def build(self, data_dict, s_stg_table):
        field_to_value_mappings = {}
        for column_map in self.column_mapping_info:
            target_column = column_map['target_column']
            source_column = column_map['source_column']
            stored_proc_name = column_map['stored_proc_name']
            value = data_dict.get(source_column)
            if target_column in s_stg_table.columns:
                converted_value = value
                if value and stored_proc_name:
                        converted_value = self._add_stored_proc_to_value(stored_proc_name, value, s_stg_table, target_column)

                field_to_value_mappings[target_column] = converted_value

        self._add_metadata_columns(field_to_value_mappings)
        return field_to_value_mappings

    def _add_stored_proc_to_value(self, stored_proc_name, value, s_stg_table, target_column):
        if stored_proc_name.startswith('sp_'):
            formatted_value = AsIs(stored_proc_name + '(' + QuotedString(value if type(value) is str else str(value)).getquoted().decode('utf-8') + ')')
        else:
            format_value = dict()
            format_value['value'] = AsIs(QuotedString(value if type(value) is str
                                                      else str(value)).getquoted().decode('utf-8'))
            if s_stg_table.c[target_column].type.python_type is str:
                format_value['length'] = s_stg_table.c[target_column].type.length
            formatted_value = AsIs(stored_proc_name.format(**format_value))

        return formatted_value

    def _add_metadata_columns(self, field_to_value_mappings):
        record_sid = AsIs('nextval(\'{schema_name}.{tenant_sequence_name}\')'.format(schema_name=self.udl_schema, tenant_sequence_name=Constants.TENANT_SEQUENCE_NAME(self.tenant_name)))
        field_to_value_mappings['record_num'] = record_sid
        field_to_value_mappings['batch_guid'] = self.guid_batch
