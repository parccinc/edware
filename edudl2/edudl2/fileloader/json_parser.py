'''
Parse the assessment json file into a table to row data map for staging.
'''
from collections import deque
from edudl2.udl2.constants import Constants
from edudl2.json_util.json_util import read_json
from celery.utils.log import get_task_logger
from edudl2.fileloader import json_parser_config

logger = get_task_logger(__name__)

__author__ = 'npandey'


class JsonParser():

    GUID_DELIMITER = "_"

    def __init__(self, load_type):
        self.data_to_process_queue = deque()
        self.table_data_map = {}

        self.table_mapping = json_parser_config.PARSER_TABLE_MAPPING(load_type)

    def parse_json(self, json_file_path):
        json_data = read_json(json_file_path)
        self.data_to_process_queue.append(("", '', [json_data]))
        self.build_table_data_map()

        return self.table_data_map

    def build_table_data_map(self):
        while self.data_to_process_queue:
            entity_long_name, parent_guid, data_to_process_list = self.data_to_process_queue.popleft()
            table_name, process_function = self.table_mapping[entity_long_name]
            self.initialize_data_map(table_name)
            func = getattr(self, process_function)
            args = self.get_args(process_function, data_to_process_list, entity_long_name, parent_guid, table_name)
            func(*args)

    def process_list(self, data_list, entity_long_name, parent_guid, table_name):
        for data_item in data_list:
            data_item_map = {}
            guid_to_append = self.generate_parent_guid(data_item, parent_guid, table_name)

            for key in data_item:
                long_name = entity_long_name + "." + key
                if isinstance(data_item[key], list) and long_name in self.table_mapping:
                    self.data_to_process_queue.append((long_name, guid_to_append, data_item[key]))
                else:
                    data_item_map[key] = data_item[key]
            data_item_map[Constants.PARENT_GUID] = parent_guid
            self.table_data_map[table_name].append(data_item_map)

    def process_singular_list(self, singular_list, parent_guid, table_name):
        field_name = json_parser_config.SINGULAR_LIST_TABLE_FIELD_MAPPING(table_name)
        for item in singular_list:
            list_item_map = {}
            list_item_map[Constants.PARENT_GUID] = parent_guid
            list_item_map[field_name] = item
            self.table_data_map[table_name].append(list_item_map)

    def initialize_data_map(self, table_name):
        if not table_name in self.table_data_map:
            self.table_data_map[table_name] = []

    def generate_parent_guid(self, data_item, parent_guid, table_name):
        if Constants.TEST_CODE in data_item:
            guid_to_append = data_item[Constants.TEST_CODE]
        else:
            data_item_guid = data_item[Constants.GUID] if Constants.GUID in data_item else ''
            guid_to_append = parent_guid + self.GUID_DELIMITER + data_item_guid if parent_guid and table_name not in ['stg_asmt', 'stg_dgnst_asmt_m'] else data_item_guid
        return guid_to_append

    def get_args(self, process_function, data_to_process_list, entity_long_name, parent_guid, table_name):
        mappings = {
            'process_list': [data_to_process_list, entity_long_name, parent_guid, table_name],
            'process_singular_list': [data_to_process_list, parent_guid, table_name]
        }
        return mappings[process_function]
