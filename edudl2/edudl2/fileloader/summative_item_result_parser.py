from lxml import etree

__author__ = "npandey"

"""
Parse the Summative Item Result xml file into a data structure
"""


class SummativeItemResultParser():

    def __init__(self, batch_size):
        self.table_data = []
        self.data_map = {}
        self.attribute_processor_map = {"assessmentResult": self.process_assmt_result,
                                        "sessionIdentifier": self.process_session_idef,
                                        "itemResult": self.process_item_result,
                                        "outcomeVariable": self.process_outcome_variable}
        self.value_processor_map = {"value": self.process_value}
        self.next_value_is_score = False
        self.processing_item_result = False
        self.batch_size = batch_size

    def parse_xml_to_data(self, source_file):
        events = ("start", "end")
        namespace = ""

        for event, elem in etree.iterparse(source_file, events):
            if event == "start":
                if "assessmentResults" in elem.tag:
                    namespace = self.get_namespace(elem)
                elif self.strip_ns(namespace, elem.tag) in self.attribute_processor_map:
                    self.attribute_processor_map[self.strip_ns(namespace, elem.tag)](elem)
            elif event == "end":
                if self.ns_tag(namespace, "assessmentResult") == elem.tag:
                    self.table_data.append(self.data_map)
                    if len(self.table_data) > self.batch_size:
                        yield self.table_data
                        self.table_data.clear()
                    elem.clear()
                    while elem.getprevious() is not None:
                        del elem.getparent()[0]
                elif self.ns_tag(namespace, "outcomeVariable") == elem.tag:
                    self.next_value_is_score = False
                elif self.ns_tag(namespace, "itemResult") == elem.tag:
                    self.processing_item_result = False
                elif self.strip_ns(namespace, elem.tag) in self.value_processor_map:
                    self.value_processor_map[self.strip_ns(namespace, elem.tag)](elem)
        yield self.table_data

    def get_namespace(self, root_element):
        return root_element.tag.split("}")[0][1:]

    def strip_ns(self, namespace, element):
        return element.split(namespace)[1].split("}")[1]

    def ns_tag(self, namespace, tag):
        return str(etree.QName(namespace, tag))

    def process_assmt_result(self, elem):
        self.data_map = {}

    def process_session_idef(self, elem):
        if elem.attrib["sourceID"] == "/pearson/student":
            self.data_map["student_guid"] = elem.attrib["identifier"]
        elif elem.attrib["sourceID"] == "/parcc/assessmentRegistrationRefId":
            self.data_map["asmt_attempt_guid"] = elem.attrib["identifier"]

    def process_item_result(self, elem):
        self.data_map["item_guid"] = elem.attrib["identifier"]
        self.processing_item_result = True

    def process_outcome_variable(self, elem):
        if elem.attrib["identifier"] == "Score":
            self.next_value_is_score = True

    def process_value(self, elem):
        if self.processing_item_result and self.next_value_is_score:
            self.data_map["student_item_score"] = elem.text
