# Trigger manually dbdump process
from argparse import ArgumentParser
import logging
import os
from edmigrate.utils.utils import read_ini
from edmigrate.utils.dumpdb import dump_tables_for_export


def process_command_lines():
    parser = ArgumentParser(description='Consortium Data Store Snapshot Migration')
    default_ini_path = '/opt/edware/conf/parcc.ini'
    parser.add_argument('-i', dest='ini_file', default=default_ini_path,
                        help="ini ini_file, default to {}".format(default_ini_path))
    args = parser.parse_args()

    ini_file = args.ini_file

    if not os.path.exists(ini_file):
        parser.error("ini configuration file doesn't exist")

    settings = read_ini(ini_file)
    logging.config.fileConfig(ini_file)

    return settings


def trigger_dump(settings):
    """
    Wrapper for dbdump process
    """
    logger = logging.getLogger('edmigrate')
    logger.info('Started manually triggered dbdump process')
    try:
        dump_tables_for_export(settings)
        logger.info('Finished manually triggered dbdump process')
    except Exception as e:
        logger.info('Manually triggered dbdump process failed')
        raise e


def main():
    """
    Entry point for creation dbdump
    """
    settings = process_command_lines()
    trigger_dump(settings)


if __name__ == '__main__':
    main()
