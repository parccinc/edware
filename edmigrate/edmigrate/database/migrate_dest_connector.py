'''
Created on Feb 14, 2014

@author: sravi
'''
from edschema.database.connector import DBConnection
from edschema.metadata.ed_metadata import generate_ed_metadata
from contextlib import contextmanager


class EdMigrateDestConnection(DBConnection):
    '''
    DBConnector for EdMigrate Project
    This is used for dest database connection for migration
    '''
    CONFIG_NAMESPACE = 'migrate_dest.db'

    def __init__(self, tenant=None, migrate_type=None, state_code=None):
        super().__init__(name=self.get_datasource_name(tenant))

    @staticmethod
    def get_namespace():
        '''
        Returns the namespace of smarter database connection
        '''
        return EdMigrateDestConnection.CONFIG_NAMESPACE + '.'

    @staticmethod
    def get_datasource_name(tenant=None):
        '''
        Returns datasource name for a tenant
        '''
        if tenant is None:
            # Returns None will raise an Exception in base class
            return None
        return EdMigrateDestConnection.get_namespace() + tenant

    @staticmethod
    def get_db_config_prefix(tenant=None):
        '''
        Returns database config prefix based on tenant name
        '''
        if tenant is None:
            # Returns None will raise an Exception in base class
            return None
        return EdMigrateDestConnection.get_namespace() + tenant + '.'

    @staticmethod
    def generate_metadata(schema_name=None, bind=None):
        '''
        Generates metadata for edware
        '''
        return generate_ed_metadata(schema_name=schema_name, bind=bind)


@contextmanager
def reflect_metadata(conn):
    schema_name = conn.get_metadata().schema
    conn.set_metadata_by_reflect(schema_name)
    yield conn
    conn.set_metadata_by_generate(schema_name, conn.generate_metadata)
