'''
Created on Feb 14, 2014

@author: sravi
'''
from edschema.database.connector import DBConnection
from edschema.metadata.int_metadata import generate_int_metadata


class EdMigrateSourceConnection(DBConnection):
    '''
    DBConnector for EdMigrate Project
    This is used for source database connection for migration
    '''
    CONFIG_NAMESPACE = 'migrate_source.db'

    def __init__(self, tenant=None, state_code=None):
        super().__init__(name=self.get_datasource_name(tenant))

    @staticmethod
    def get_namespace():
        '''
        Returns the namespace of database connection
        '''
        return EdMigrateSourceConnection.CONFIG_NAMESPACE + '.'

    @staticmethod
    def get_datasource_name(tenant=None):
        '''
        Returns datasource name for a tenant
        '''
        return EdMigrateSourceConnection.get_namespace() + tenant if tenant else EdMigrateSourceConnection.get_namespace()

    @staticmethod
    def get_db_config_prefix(tenant=None):
        '''
        Returns database config prefix based on tenant name
        '''
        return EdMigrateSourceConnection.get_namespace() + tenant + '.' if tenant else EdMigrateSourceConnection.get_namespace()

    @staticmethod
    def generate_metadata(schema_name=None, bind=None):
        '''
        Generates metadata for edware
        '''
        return generate_int_metadata(schema_name=schema_name, bind=bind)
