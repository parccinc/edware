'''
Created on Apr 29, 2015

@author: dip
'''
from edcore.database import initialize_db
from edmigrate.database.migrate_source_connector import EdMigrateSourceConnection
from edmigrate.database.migrate_dest_connector import EdMigrateDestConnection,\
    reflect_metadata
from edmigrate.migrate.migrate_by_batch import MigrateBySnapshot
from argparse import ArgumentParser
from edmigrate.utils.utils import read_ini
from edcore.utils.utils import format_asmt_year
import logging
from edmigrate.conductor_controller import start_conductor
from edmigrate.utils.consumer import ConsumerThread
import os
from edmigrate.database.repmgr_connector import RepMgrDBConnection
from edmigrate.utils.common import get_broker_connection
from sqlalchemy.sql.expression import and_, select
from edmigrate.migrate.migrate_helper import yield_rows
from edmigrate.utils.constants import Constants


logger = logging.getLogger('edmigrate')


class CDSMigration():
    '''
    CDS Migration via snapshot with or without repmgr
    '''
    def __init__(self, settings, mode, year, with_replication=False):
        self.settings = settings
        self.with_replication = with_replication
        self.migrator = CDSSnapshot(settings, mode, year)

    def start(self):
        '''
        Without repmgr in an environment, we can perform a snapshot.  With repmgr, we want to use the conductor to bring half
        the slaves down at a time
        '''
        self.start_snapshot_with_conductor() if self.with_replication else self.start_snapshot()

    def start_snapshot(self):
        '''
        starts CDS snapshot migration
        '''
        completed, num_tenants = self.migrator.start()
        if completed is not num_tenants:
            logging.error("Snapshot was unsuccessful for all the tenants")
        else:
            logging.info("Snapshot was successful for all the tenants")

    def start_snapshot_with_conductor(self):
        '''
        Initializes conductor to orchestrate half the slaves to replicate during snapshot process
        '''
        # We need repmgr connection for replication
        initialize_db(RepMgrDBConnection, self.settings)
        connect = get_broker_connection(self.settings)
        consumerThread = ConsumerThread(connect)
        try:
            consumerThread.start()
            start_conductor(self.migrator, self.settings, tenant=Constants.PARCC)
            consumerThread.stop()
        except KeyboardInterrupt:
            os._exit(0)
        except Exception as e:
            logging.error(e)
            os._exit(1)


class CDSSnapshot(MigrateBySnapshot):
    '''
    For Tenant CDS, we use the following config format in the ini file for tenant/state level snapshot

    cds.tenant.migrate_dest.db.[tenantOne].schema_name = edware_tenant_cds
    cds.tenant.migrate_dest.db.[tenantOne].url = [url]
    cds.tenant.migrate_source.db.[tenantOne].schema_name = edware_prod
    cds.tenant.migrate_source.db.[tenantOne].url = [url]

    For Consortium CDS, we use the following format in the ini file for consortium snapshot.

    cds.consortium.migrate_dest.db.parcc.schema_name = edware_cds
    cds.consortium.migrate_dest.db.parcc.url = [url]
    cds.consortium.migrate_source.db.[tenantOne].schema_name = edware_tenant_cds
    cds.consortium.migrate_source.db.[tenantOne].url = [url]
    cds.consortium.migrate_source.db.[tenantTwo].schema_name = edware_tenant_cds
    cds.consortium.migrate_source.db.[tenantTwo].url = [url]
    '''
    TENANT = 'tenant'
    CONSORTIUM = 'consortium'
    TENANT_SOURCE_NAMESPACE = 'cds.tenant.migrate_source.db'
    TENANT_TARGET_NAMESPACE = 'cds.tenant.migrate_dest.db'
    CONSORTIUM_SOURCE_NAMESPACE = 'cds.consortium.migrate_source.db'
    CONSORTIUM_TARGET_NAMESPACE = 'cds.consortium.migrate_dest.db'
    ITEMS_TABLE_MAP = {'rpt_ela_sum_item_score': 'rpt_ela_sum',
                       'rpt_math_sum_item_score': 'rpt_math_sum'}
    TEST_METDATA_TABLE = 'rpt_sum_test_level_m'

    def __init__(self, settings, mode, year):
        self.year = format_asmt_year(year)
        self.mode = mode
        self.settings = settings
        self.initialize_database(settings)
        self.validate()

    def get_source_namespace(self):
        return CDSSnapshot.TENANT_SOURCE_NAMESPACE if self.mode == CDSSnapshot.TENANT else CDSSnapshot.CONSORTIUM_SOURCE_NAMESPACE

    def get_target_namespace(self):
        return CDSSnapshot.TENANT_TARGET_NAMESPACE if self.mode == CDSSnapshot.TENANT else CDSSnapshot.CONSORTIUM_TARGET_NAMESPACE

    def get_target_schema(self, name):
        return self.settings["{namespace}.{tenant}.schema_name".format(namespace=self.get_target_namespace(), tenant=name)]

    def get_source_schema(self, name):
        return self.settings["{namespace}.{tenant}.schema_name".format(namespace=self.get_source_namespace(), tenant=name)]

    def initialize_database(self, settings):
        '''
        Initializes all database connections.  In tenant level snapsot, there will be two connections
        In a consortium level snapshot, there will be # of tenants + 1 connections
        '''
        EdMigrateSourceConnection.CONFIG_NAMESPACE = self.get_source_namespace()
        EdMigrateDestConnection.CONFIG_NAMESPACE = self.get_target_namespace()
        sources = initialize_db(EdMigrateSourceConnection, settings)
        targets = initialize_db(EdMigrateDestConnection, settings)
        self.sources = list(sources.keys())
        self.targets = list(targets.keys())

    def validate(self):
        '''
        Simple validation for Configuration
        '''
        error = False
        if len(self.targets) != 1:
            logging.error("There should be exactly one target.  Please check migrate_dest configuration")
            error = True
        elif self.mode == CDSSnapshot.TENANT and len(self.targets) != len(self.sources):
            logging.error("In Tenant mode, there should be exactly one source.  Please check migrate_source configuration")
            error = True

        if error:
            raise Exception("Configuration validation fails.  Please check your INI file.")
        else:
            # We set the target destination name as our tenant
            self.tenant = self.targets[0]

    def get_table_names(self, connector):
        '''
        Returns a list of tables that have at least one column with pii set to False
        '''
        return [table.name for table in connector.get_metadata().sorted_tables]

    def get_select_query(self, conn, table):
        '''
        Treat items table differently, we need to query for the main tables with certain critierias
        '''
        if self.mode == CDSSnapshot.TENANT and table.name in CDSSnapshot.ITEMS_TABLE_MAP.keys():
            asmt_table = conn.get_table(CDSSnapshot.ITEMS_TABLE_MAP[table.name])
            # TODO: test_uuid? schould be replaced on sum_score_rec_uuid for summative tables?
            select_query = select([table],
                                  from_obj=[table.join(asmt_table,
                                                       and_(asmt_table.c.sum_score_rec_uuid == table.c.test_uuid,
                                                            asmt_table.c.student_parcc_id == table.c.student_parcc_id))])
            return self.append_criteria(select_query, asmt_table)
        else:
            return super().get_select_query(conn, table)

    def append_criteria(self, query, table):
        columns = {'year': self.year, 'include_in_parcc': True, 'rec_status': 'C'}
        for column, value in columns.items():
            if column in table.columns:
                query = query.where(and_(table.c[column] == value))
        return query

    def migrate_metadata_table(self, target, source, table_name):
        '''
        Special case for rpt_sum_test_level_m table
        There's no year associated, we only delete if it exists in both source and target
        '''
        delete_count = 0
        insert_count = 0
        with reflect_metadata(target) as conn:
            batched_rows = yield_rows(source,
                                      self.get_select_query(source, source.get_table(table_name)),
                                      100)
            target_table = conn.get_table(table_name)
            insert_query = target_table.insert()
            for rows in batched_rows:
                test_code = []
                perf_lvl_id = []
                for row in rows:
                    test_code.append(row['test_code'])
                    perf_lvl_id.append(row['perf_lvl_id'])
                # Delete as a batch with criteria
                if test_code and perf_lvl_id:
                    delete_query = target_table.delete().where(and_(target_table.c.test_code.in_(test_code),
                                                                    target_table.c.perf_lvl_id.in_(perf_lvl_id)))
                    delete_count += conn.execute(delete_query).rowcount
                # Insert as a batch
                insert_count += conn.execute(insert_query, rows).rowcount
        return delete_count, insert_count

    def start(self):
        '''
        Truncates target db based on the criteria (by year) and migrates from 1 or more sources
        '''
        completed = 0
        for tenant_name in self.sources:
            success = True
            with EdMigrateDestConnection(self.tenant) as target, \
                    EdMigrateSourceConnection(tenant_name) as source:
                try:
                    logging.info('start migrating snapshot of year %s from %s to %s', self.year, tenant_name, self.tenant)
                    trans = target.get_transaction()
                    target.set_metadata_by_reflect(self.get_target_schema(self.tenant))
                    source.set_metadata_by_reflect(self.get_source_schema(tenant_name))
                    tables = self.get_table_names(target)
                    logging.info('Number of tables to migrate: %s', len(tables))
                    for table_name in tables:
                        # We only truncate if it's the first time we're migrating from first source
                        # This is because in consortium mode, we only truncate before processing first tenant
                        if table_name != CDSSnapshot.TEST_METDATA_TABLE:
                            delete_count, insert_count = self.migrate(target, source, table_name, table_name, truncate=completed == 0)
                        else:
                            delete_count, insert_count = self.migrate_metadata_table(target, source, table_name)
                        logging.info('%s finished, %d deleted, %d inserted.', table_name, delete_count, insert_count)

                    logging.info('Done migrating current tenant')
                    trans.commit()
                except Exception as e:
                    logging.info(e)
                    logging.exception('snapshot migration rollback for %s', tenant_name)
                    trans.rollback()
                    success = False
            if success:
                completed += 1
        return completed, len(self.sources)


def process_command_lines():
    parser = ArgumentParser(description='Consortium Data Store Snapshot Migration')
    default_ini_path = '/opt/edware/conf/parcc.ini'
    parser.add_argument('-i', dest='ini_file', default=default_ini_path, help="ini ini_file, default to %s" % default_ini_path)
    parser.add_argument('-y', dest='year', type=int, required=True, help="assessment year. ex. 2015")
    parser.add_argument('-m', dest='mode', choices=['tenant', 'consortium'], required=True, help='tenant or consortium')
    parser.add_argument('--withReplication', dest='with_replication', action='store_true', default=False, help='perform snapshot in env with repmgr')
    args = parser.parse_args()

    ini_file = args.ini_file
    year = args.year
    mode = args.mode

    if not os.path.exists(ini_file):
        parser.error("ini configuration file doesn't exist")

    settings = read_ini(ini_file)
    logging.config.fileConfig(ini_file)

    if year < 1900 or year > 2999:
        parser.error('assessment year is not within valid range')

    return mode, year, settings, args.with_replication


def main():
    '''
    Entry point to CDS snapshot migration
    '''
    mode, year, settings, with_replication = process_command_lines()
    CDSMigration(settings, mode, year, with_replication).start()


if __name__ == '__main__':
    main()
