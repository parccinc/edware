'''
Created on May 3, 2015

@author: dip
'''
import unittest
from edmigrate.utils.common import get_broker_connection
import configparser
from kombu.connection import Connection


class TestCommon(unittest.TestCase):

    def test_get_broker_connection(self):
        settings = configparser.ConfigParser()
        settings['default'] = {}
        conn = get_broker_connection(settings['default'])
        self.assertIsNotNone(conn)
        self.assertIsInstance(conn, Connection)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
