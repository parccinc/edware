'''
Created on May 2, 2015

@author: dip
'''
import unittest
from edmigrate.cds import CDSSnapshot, CDSMigration
from unittest.mock import patch, ANY
from edmigrate.tests.utils.unittest_with_source_sqlite import Unittest_with_source_sqlite
from edmigrate.tests.utils.unittest_with_dest_sqlite import Unittest_with_dest_sqlite
from edmigrate.database.migrate_source_connector import EdMigrateSourceConnection


class TestCDS(Unittest_with_source_sqlite, Unittest_with_dest_sqlite):

    def setUp(self):
        pass

    @classmethod
    def setUpClass(cls):
        Unittest_with_source_sqlite.setUpClass()
        Unittest_with_dest_sqlite.setUpClass()

    def tearDown(self):
        pass

    def test_namespace_for_tenant_mode(self,):
        settings = {'cds.tenant.migrate_dest.db.tenant.schema_name': 'edware_tenant_cds',
                    'cds.tenant.migrate_dest.db.tenant.url': 'sqlite://',
                    'cds.tenant.migrate_source.db.tenant.schema_name': 'edware_prod',
                    'cds.tenant.migrate_source.db.tenant.url': 'sqlite://'}
        cds = CDSSnapshot(settings, CDSSnapshot.TENANT, 2016)
        self.assertEquals(cds.get_source_namespace(), CDSSnapshot.TENANT_SOURCE_NAMESPACE)
        self.assertEquals(cds.get_target_namespace(), CDSSnapshot.TENANT_TARGET_NAMESPACE)

    def test_namespace_for_consortium_mode(self):
        settings = {'cds.consortium.migrate_dest.db.consortium.schema_name': 'edware_consortium_cds',
                    'cds.consortium.migrate_dest.db.consortium.url': 'sqlite://',
                    'cds.consortium.migrate_source.db.tenantOne.schema_name': 'edware_prod_tenant_one',
                    'cds.consortium.migrate_source.db.tenantOne.url': 'sqlite://'}
        cds = CDSSnapshot(settings, CDSSnapshot.CONSORTIUM, 2016)
        self.assertEquals(cds.get_source_namespace(), CDSSnapshot.CONSORTIUM_SOURCE_NAMESPACE)
        self.assertEquals(cds.get_target_namespace(), CDSSnapshot.CONSORTIUM_TARGET_NAMESPACE)

    def test_get_schema_for_tenant(self):
        settings = {'cds.tenant.migrate_dest.db.tenant.schema_name': 'edware_tenant_cds',
                    'cds.tenant.migrate_dest.db.tenant.url': 'sqlite://',
                    'cds.tenant.migrate_source.db.tenant.schema_name': 'edware_prod',
                    'cds.tenant.migrate_source.db.tenant.url': 'sqlite://'}
        cds = CDSSnapshot(settings, CDSSnapshot.TENANT, 2016)
        self.assertEquals(cds.get_target_schema('tenant'), 'edware_tenant_cds')
        self.assertEquals(cds.get_source_schema('tenant'), 'edware_prod')

    def test_get_target_schema_for_consoritum(self):
        settings = {'cds.consortium.migrate_dest.db.consortium.schema_name': 'edware_consortium_cds',
                    'cds.consortium.migrate_dest.db.consortium.url': 'sqlite://',
                    'cds.consortium.migrate_source.db.tenantOne.schema_name': 'edware_prod_tenant_one',
                    'cds.consortium.migrate_source.db.tenantOne.url': 'sqlite://',
                    'cds.consortium.migrate_source.db.tenantTwo.schema_name': 'edware_prod_tenant_two',
                    'cds.consortium.migrate_source.db.tenantTwo.url': 'sqlite://'}
        cds = CDSSnapshot(settings, CDSSnapshot.CONSORTIUM, 2016)
        self.assertEquals(cds.get_target_schema('consortium'), 'edware_consortium_cds')
        self.assertEquals(cds.get_source_schema('tenantOne'), 'edware_prod_tenant_one')
        self.assertEquals(cds.get_source_schema('tenantTwo'), 'edware_prod_tenant_two')

    @patch('edmigrate.cds.CDSSnapshot.get_table_names')
    @patch('edschema.database.connector.DBConnection.set_metadata_by_reflect')
    @patch('edmigrate.cds.CDSSnapshot.migrate')
    def test_start_with_tenant_mode(self, migrate_func, target_metadata, table_names):
        table_names.return_value = ['int_ela_sum']
        migrate_func.return_value = (3, 3)
        settings = {'cds.tenant.migrate_dest.db.tenant.schema_name': 'edware_tenant_cds',
                    'cds.tenant.migrate_dest.db.tenant.url': 'sqlite://',
                    'cds.tenant.migrate_source.db.tenant.schema_name': 'edware_prod',
                    'cds.tenant.migrate_source.db.tenant.url': 'sqlite://'}
        cds = CDSSnapshot(settings, CDSSnapshot.TENANT, 2016)
        val = cds.start()
        migrate_func.assert_called_once_with(ANY, ANY, 'int_ela_sum', 'int_ela_sum', truncate=True)
        self.assertEquals(val[0], 1)
        self.assertEquals(val[0], val[1])

    @patch('edmigrate.cds.CDSSnapshot.get_table_names')
    @patch('edschema.database.connector.DBConnection.set_metadata_by_reflect')
    @patch('edmigrate.cds.CDSSnapshot.migrate')
    def test_start_with_tenant_mode_multi_tables(self, migrate_func, target_metadata, table_names):
        table_names.return_value = ['int_ela_sum', 'a', 'b']
        migrate_func.return_value = (3, 3)
        settings = {'cds.tenant.migrate_dest.db.tenant.schema_name': 'edware_tenant_cds',
                    'cds.tenant.migrate_dest.db.tenant.url': 'sqlite://',
                    'cds.tenant.migrate_source.db.tenant.schema_name': 'edware_prod',
                    'cds.tenant.migrate_source.db.tenant.url': 'sqlite://'}
        cds = CDSSnapshot(settings, CDSSnapshot.TENANT, 2016)
        val = cds.start()
        expected = [((ANY, ANY, 'int_ela_sum', 'int_ela_sum'), {'truncate': True},),
                    ((ANY, ANY, 'a', 'a'), {'truncate': True},),
                    ((ANY, ANY, 'b', 'b'), {'truncate': True},)]
        self.assertEquals(expected, migrate_func.call_args_list)
        self.assertEquals(val[0], 1)
        self.assertEquals(val[0], val[1])

    @patch('edmigrate.cds.CDSSnapshot.get_table_names')
    @patch('edschema.database.connector.DBConnection.set_metadata_by_reflect')
    @patch('edmigrate.cds.CDSSnapshot.migrate')
    def test_start_with_consortium_mode(self, migrate_func, target_metadata, table_names):
        table_names.return_value = ['int_ela_sum', 'a']
        migrate_func.return_value = (3, 3)
        settings = {'cds.consortium.migrate_dest.db.consortium.schema_name': 'edware_consortium_cds',
                    'cds.consortium.migrate_dest.db.consortium.url': 'sqlite://',
                    'cds.consortium.migrate_source.db.tenantOne.schema_name': 'edware_prod_tenant_one',
                    'cds.consortium.migrate_source.db.tenantOne.url': 'sqlite://',
                    'cds.consortium.migrate_source.db.tenantTwo.schema_name': 'edware_prod_tenant_two',
                    'cds.consortium.migrate_source.db.tenantTwo.url': 'sqlite://'}
        cds = CDSSnapshot(settings, CDSSnapshot.CONSORTIUM, 2016)
        val = cds.start()
        expected = [((ANY, ANY, 'int_ela_sum', 'int_ela_sum'), {'truncate': True},),
                    ((ANY, ANY, 'a', 'a'), {'truncate': True},),
                    ((ANY, ANY, 'int_ela_sum', 'int_ela_sum'), {'truncate': False},),
                    ((ANY, ANY, 'a', 'a'), {'truncate': False},)]
        self.assertEquals(expected, migrate_func.call_args_list)
        self.assertEquals(val[0], 2)
        self.assertEquals(val[0], val[1])

    @patch('edmigrate.cds.CDSSnapshot.get_table_names')
    @patch('edschema.database.connector.DBConnection.set_metadata_by_reflect')
    @patch('edmigrate.cds.CDSSnapshot.migrate')
    def test_migration_exception(self, migrate_func, target_metadata, table_names):
        table_names.return_value = ['int_ela_sum']
        migrate_func.side_effect = Exception()
        settings = {'cds.tenant.migrate_dest.db.tenant.schema_name': 'edware_tenant_cds',
                    'cds.tenant.migrate_dest.db.tenant.url': 'sqlite://',
                    'cds.tenant.migrate_source.db.tenant.schema_name': 'edware_prod',
                    'cds.tenant.migrate_source.db.tenant.url': 'sqlite://'}
        cds = CDSSnapshot(settings, CDSSnapshot.TENANT, 2016)
        val = cds.start()
        migrate_func.assert_called_once_with(ANY, ANY, 'int_ela_sum', 'int_ela_sum', truncate=True)
        self.assertEquals(val[0], 0)
        self.assertEquals(val[1], 1)

    @patch('edmigrate.cds.CDSSnapshot.get_table_names')
    @patch('edschema.database.connector.DBConnection.set_metadata_by_reflect')
    @patch('edmigrate.cds.CDSSnapshot.migrate')
    def test_CDSMigration(self, migrate_func, target_metadata, table_names):
        table_names.return_value = ['int_ela_sum']
        migrate_func.return_value = (3, 3)
        settings = {'cds.tenant.migrate_dest.db.tenant.schema_name': 'edware_tenant_cds',
                    'cds.tenant.migrate_dest.db.tenant.url': 'sqlite://',
                    'cds.tenant.migrate_source.db.tenant.schema_name': 'edware_prod',
                    'cds.tenant.migrate_source.db.tenant.url': 'sqlite://'}
        cds = CDSMigration(settings, CDSSnapshot.TENANT, 2016, False)
        cds.start()
        migrate_func.assert_called_once_with(ANY, ANY, 'int_ela_sum', 'int_ela_sum', truncate=True)

    def test_validate_more_than_one_target(self):
        settings = {'cds.tenant.migrate_dest.db.tenantOne.schema_name': 'edware_tenant_cds',
                    'cds.tenant.migrate_dest.db.tenantOne.url': 'sqlite://',
                    'cds.tenant.migrate_dest.db.tenantTwo.schema_name': 'edware_tenant_cds',
                    'cds.tenant.migrate_dest.db.tenantTwo.url': 'sqlite://',
                    'cds.tenant.migrate_source.db.tenantOne.schema_name': 'edware_prod',
                    'cds.tenant.migrate_source.db.tenantOne.url': 'sqlite://'}
        self.assertRaises(Exception, CDSSnapshot, settings, CDSSnapshot.TENANT, 2016)

    def test_validate_more_than_one_sources(self):
        settings = {'cds.tenant.migrate_dest.db.tenantOne.schema_name': 'edware_tenant_cds',
                    'cds.tenant.migrate_dest.db.tenantOne.url': 'sqlite://',
                    'cds.tenant.migrate_source.db.tenantTwo.schema_name': 'edware_tenant_cds',
                    'cds.tenant.migrate_source.db.tenantTwo.url': 'sqlite://',
                    'cds.tenant.migrate_source.db.tenantOne.schema_name': 'edware_prod',
                    'cds.tenant.migrate_source.db.tenantOne.url': 'sqlite://'}
        self.assertRaises(Exception, CDSSnapshot, settings, CDSSnapshot.TENANT, 2016)

    def test_validate_more_than_one_sources_for_consortium(self):
        settings = {'cds.consortium.migrate_dest.db.parcc.schema_name': 'edware_tenant_cds',
                    'cds.consortium.migrate_dest.db.parcc.url': 'sqlite://',
                    'cds.consortium.migrate_source.db.tenantTwo.schema_name': 'edware_tenant_cds',
                    'cds.consortium.migrate_source.db.tenantTwo.url': 'sqlite://',
                    'cds.consortium.migrate_source.db.tenantOne.schema_name': 'edware_prod',
                    'cds.consortium.migrate_source.db.tenantOne.url': 'sqlite://'}
        cds = CDSSnapshot(settings, CDSSnapshot.CONSORTIUM, 2016)
        self.assertEquals(len(cds.sources), 2)

    def test_get_select_query(self):
        settings = {'cds.consortium.migrate_dest.db.parcc.schema_name': 'edware_tenant_cds',
                    'cds.consortium.migrate_dest.db.parcc.url': 'sqlite://',
                    'cds.consortium.migrate_source.db.tenantTwo.schema_name': 'edware_tenant_cds',
                    'cds.consortium.migrate_source.db.tenantTwo.url': 'sqlite://',
                    'cds.consortium.migrate_source.db.tenantOne.schema_name': 'edware_prod',
                    'cds.consortium.migrate_source.db.tenantOne.url': 'sqlite://'}
        cds = CDSSnapshot(settings, CDSSnapshot.CONSORTIUM, 2016)
        with EdMigrateSourceConnection('tenantOne') as source:
            val = cds.get_select_query(source, source.get_table('rpt_ela_summ'))
            self.assertEquals(len(val.froms), 1)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
