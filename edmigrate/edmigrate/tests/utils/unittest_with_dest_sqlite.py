from edmigrate.tests.utils.test_metadata import generate_dest_test_metadata
from edschema.tests.database.utils.unittest_with_sqlite import Unittest_with_sqlite, \
    Unittest_with_sqlite_no_data_load
from sqlalchemy.types import BigInteger
from sqlalchemy.ext.compiler import compiles
import os
from edmigrate.database.migrate_dest_connector import EdMigrateDestConnection

__author__ = 'npandey'


class Unittest_with_dest_sqlite(Unittest_with_sqlite):
    @classmethod
    def setUpClass(cls):
        here = os.path.abspath(os.path.dirname(__file__))
        resources_dir = os.path.abspath(os.path.join(os.path.join(here, '..', 'resources')))
        super().setUpClass(EdMigrateDestConnection.get_datasource_name(get_unittest_tenant_name()), metadata=generate_dest_test_metadata(), resources_dir=resources_dir)

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()


class Unittest_with_dest_sqlite_no_data_load(Unittest_with_sqlite_no_data_load):
    @classmethod
    def setUpClass(cls):
        super().setUpClass(EdMigrateDestConnection.get_datasource_name(get_unittest_tenant_name()), metadata=generate_dest_test_metadata())


class UnittestDestDBConnection(EdMigrateDestConnection):
    def __init__(self):
        super().__init__(tenant=get_unittest_tenant_name())


# Fixes failing test for schema definitions with BigIntegers
@compiles(BigInteger, 'sqlite')
def compile_big_int_sqlite(type_, compiler, **kw):
    return 'INTEGER'


def get_unittest_tenant_name():
    return 'tomcat'
