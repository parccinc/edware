import unittest
from unittest.mock import patch
from zipfile import BadZipfile
from sqlalchemy.engine.url import make_url

from edcore.utils.dbdumpfile import DBDumpFileException
from edcore.utils.utils import convert_sqlalchemy_url
from edmigrate.utils.dumpdb import dump_tables_for_export, _build_csv_gz_commands
from edmigrate.tests.utils.unittest_with_source_sqlite import get_unittest_tenant_name


class TestDumpDB(unittest.TestCase):

    def setUp(self):
        self.tenant = get_unittest_tenant_name()
        self.settings = {
            'migrate.dump_tables': 'rpt_ela_sum,rpt_math_sum',
            'migrate.dump_temp_file_format': '/tmp/{dumpid}-temp-edmigrate-dump',
            'migrate.dumpfile_location': 's3://bad-name/',
            'migrate.dumpfile_name_format': '{dbid}-edmigrate-dump',
            'migrate.dump_temp_file_path': '/tmp/{}.csv.gz',
            'migrate.tenant': self.tenant,
            'migrate_dest.db.tomcat.url': 'postgresql+psycopg2://baduser:badpass@localhost:5432/baddb',
            'migrate_dest.db.tomcat.schema_name': 'badschema',
        }
        self.command_mask = 'psql postgres://baduser:badpass@localhost:5432/baddb ' \
                            '-c "COPY badschema.{table_name} TO STDOUT DELIMITERS \',\' CSV HEADER" | ' \
                            'gzip -8 > /tmp/{table_name}.csv.gz'
        self.dump_file_patches = ['/tmp/rpt_ela_sum.csv.gz', '/tmp/rpt_math_sum.csv.gz']

    def test_dump_bad_config(self):
        del self.settings['migrate_dest.db.tomcat.schema_name']
        self.assertRaises(KeyError, dump_tables_for_export, self.settings)

    @patch("zipfile.ZipFile.__enter__")
    @patch("zipfile.ZipFile.__exit__")
    @patch("subprocess.Popen.wait", return_value=0)
    def test_failed_write_generated_gz_dump_on_s3(self, mock_subprocess_wait, zipf_enter, zipf_exit):
        with self.assertRaises(DBDumpFileException) as exc:
            dump_tables_for_export(self.settings)
        self.assertEqual("dbdump failed to write dump into bucket!", str(exc.exception))

    @patch("zipfile.ZipFile.__exit__")
    @patch("subprocess.Popen.wait", side_effect=[0, 1])
    def test_dump_subprocess_task_return_non_zero_code(self, mock_subprocess_wait, zipf_exit):
        with self.assertRaises(DBDumpFileException) as exc:
            dump_tables_for_export(self.settings)
        self.assertEqual("dbdump failed w/ error code 1!", str(exc.exception))

    @patch("zipfile.ZipFile.__enter__", side_effect=BadZipfile)
    def test_create_zipfile_error(self, mock_zip):
        with self.assertRaises(DBDumpFileException) as exc:
            dump_tables_for_export(self.settings)
        self.assertEqual("dbdump failed to create archive with dump!", str(exc.exception))

    @patch("zipfile.ZipFile.__exit__")
    @patch("subprocess.Popen.wait", return_value=0)
    @patch("edcore.utils.dbdumpfile.DBDumpFile.write_from_file")
    @patch("edcore.utils.dbdumpfile.DBDumpFile.complete_file")
    def test_success_dbdump(self, mock_dbdump_file_write, mock_dbdump_complete_file,
                            mock_subprocess_wait, zipf_exit):
        dump_tables_for_export(self.settings)
        self.assertTrue(mock_dbdump_file_write.called)
        self.assertTrue(mock_dbdump_complete_file.called)

    def test_build_csv_gz_commands(self):
        db_url = make_url(self.settings['migrate_dest.db.tomcat.url'])
        db_conn_string = convert_sqlalchemy_url(db_url)
        schema = self.settings['migrate_dest.db.tomcat.schema_name']
        tables = self.settings['migrate.dump_tables'].split(',')
        tmp_file_path = self.settings['migrate.dump_temp_file_path']
        commands, file_patch = _build_csv_gz_commands(db_conn_string, schema, tables, tmp_file_path)
        self.assertListEqual(commands, self.local_command_formatter())
        self.assertListEqual(file_patch, self.dump_file_patches)

    def local_command_formatter(self):
        tables = self.settings['migrate.dump_tables'].split(',')
        return [self.command_mask.format(table_name=table) for table in tables]
