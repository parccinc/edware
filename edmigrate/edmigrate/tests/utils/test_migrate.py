from edcore.utils.dbdumpfile import DBDumpFileException
from edmigrate.utils.dumpdb import dump_tables_for_export
from edmigrate.utils.migrate import get_batches_by_status, \
    report_udl_stats_batch_status, cleanup_batch, MigrateProcess
from edmigrate.tests.utils.unittest_with_dest_sqlite import Unittest_with_dest_sqlite, \
    get_unittest_tenant_name as get_unittest_dest_tenant_name
from edmigrate.exceptions import EdMigrateUdl_statException
from sqlalchemy.sql.expression import select, func
from edmigrate.utils.constants import Constants
from edcore.tests.utils.unittest_with_stats_sqlite import Unittest_with_stats_sqlite
from edmigrate.database.migrate_dest_connector import EdMigrateDestConnection
from edmigrate.database.migrate_source_connector import EdMigrateSourceConnection
from edcore.database.utils.constants import UdlStatsConstants
from edmigrate.tests.utils.unittest_with_source_sqlite import Unittest_with_source_sqlite, \
    get_unittest_tenant_name as get_unittest_source_tenant_name
import datetime
from edcore.database.utils.query import insert_udl_stats

__author__ = 'sravi'


class TestMigrate(Unittest_with_source_sqlite, Unittest_with_dest_sqlite, Unittest_with_stats_sqlite):

    test_tenant = 'tomcat'

    def setUp(self):
        self.__tenant = TestMigrate.test_tenant

    @classmethod
    def setUpClass(cls):
        Unittest_with_source_sqlite.setUpClass()
        Unittest_with_dest_sqlite.setUpClass()
        Unittest_with_stats_sqlite.setUpClass()

    def tearDown(self):
        pass

    def insert_into_udl_stats(self, rec_id, guid_batch, load_type, tenant_name):
        udl_stats = {UdlStatsConstants.REC_ID: rec_id,
                     UdlStatsConstants.BATCH_GUID: guid_batch,
                     UdlStatsConstants.SCHEMA_NAME: guid_batch,
                     UdlStatsConstants.LOAD_TYPE: load_type,
                     UdlStatsConstants.FILE_ARRIVED: datetime.datetime.now(),
                     UdlStatsConstants.TENANT: tenant_name,
                     UdlStatsConstants.DEST_TENANT: tenant_name,
                     UdlStatsConstants.LOAD_STATUS: UdlStatsConstants.UDL_STATUS_RECEIVED}
        insert_udl_stats(udl_stats)

    def test_report_udl_stats_batch_status(self):
        batches_to_migrate = get_batches_by_status('hotdog', UdlStatsConstants.UDL_STATUS_INGESTED)
        self.assertEqual(1, len(batches_to_migrate))
        self.assertEqual(UdlStatsConstants.UDL_STATUS_INGESTED, batches_to_migrate[0][UdlStatsConstants.LOAD_STATUS])
        updated_count = report_udl_stats_batch_status(batches_to_migrate[0][UdlStatsConstants.REC_ID], UdlStatsConstants.MIGRATE_INGESTED)
        self.assertEqual(1, updated_count)
        batches_to_migrate = get_batches_by_status('hotdog', UdlStatsConstants.UDL_STATUS_INGESTED)
        self.assertEqual(0, len(batches_to_migrate))

    def test_report_udl_stats_batch_status_non_exist_batch_id(self):
        self.assertRaises(EdMigrateUdl_statException, report_udl_stats_batch_status, 'non-exist-uuid', UdlStatsConstants.MIGRATE_IN_PROCESS)

    def cleanup_batch(self):
        batch_guid = '3384654F-9076-45A6-BB13-64E8EE252A49'
        batch = {UdlStatsConstants.BATCH_GUID: batch_guid, UdlStatsConstants.TENANT: self.__tenant,
                 UdlStatsConstants.SCHEMA_NAME: batch_guid, Constants.DEACTIVATE: False}
        rtn = cleanup_batch(batch)
        self.assertFalse(rtn)

    def test_get_batches_to_migrate_with_specified_tenant(self):
        batches_to_migrate = get_batches_by_status('test', UdlStatsConstants.UDL_STATUS_INGESTED)
        self.assertEqual(4, len(batches_to_migrate))

    def test_migrate_empty_table(self):
        tenant = get_unittest_source_tenant_name()
        dest_tenant = get_unittest_dest_tenant_name()
        src_conn = EdMigrateSourceConnection(tenant=tenant)
        dest_conn = EdMigrateDestConnection(tenant=dest_tenant)
        batch_guid = "2f95ade9-7aa2-43c4-949c-3002dd7c7f42"
        source_table = src_conn.get_table(Constants.SRC_TEST_TABLE1)
        dest_table = dest_conn.get_table(Constants.DEST_TEST_TABLE1)
        get_query = select([source_table.c.record_num]).order_by(source_table.c.record_num)

        rset = src_conn.execute(get_query)
        row = rset.fetchall()
        self.assertEqual(4, len(row))

        rset.close()

        dest_count_query = select([func.count()], from_obj=dest_table)

        result = dest_conn.execute(dest_count_query)
        self.assertEqual(0, result.fetchall()[0][0])

        result.close()

        migrate_process = MigrateProcess(tenant)
        delete_count, insert_count = migrate_process.migrate_table(
            batch_guid, None, src_conn, dest_conn, 'int_ela_sum', 'rpt_ela_sum', False)
        self.assertEqual(0, delete_count)
        self.assertEqual(4, insert_count)

        result = dest_conn.execute(dest_count_query)
        self.assertEqual(4, result.fetchall()[0][0])
        result.close()

    def test_migrate_with_data(self):
        #Unit tests do not support deactivation

        tenant = get_unittest_source_tenant_name()
        dest_tenant = get_unittest_dest_tenant_name()
        src_conn = EdMigrateSourceConnection(tenant=tenant)
        dest_conn = EdMigrateDestConnection(tenant=dest_tenant)
        batch_guid = "0ee7402b-dd44-45a9-a7c8-1be3b0b89931"

        source_table = src_conn.get_table(Constants.SRC_TEST_TABLE2)
        dest_table = dest_conn.get_table(Constants.DEST_TEST_TABLE2)

        get_query = select([source_table.c.record_num]).order_by(source_table.c.record_num)

        rset = src_conn.execute(get_query)
        row = rset.fetchall()
        self.assertEqual(4, len(row))
        rset.close()

        dest_count_query = select([func.count()], from_obj=dest_table)

        result = dest_conn.execute(dest_count_query)
        self.assertEqual(8, result.fetchall()[0][0])
        result.close()

        migrate_process = MigrateProcess(tenant)
        delete_count, insert_count = migrate_process.migrate_table(
            batch_guid, None, src_conn, dest_conn, 'int_math_sum', 'rpt_math_sum', False)
        self.assertEqual(0, delete_count)
        self.assertEqual(4, insert_count)

        result = dest_conn.execute(dest_count_query)
        self.assertEqual(12, result.fetchall()[0][0])
        result.close()
