'''
Created on May 20, 2015

@authors: tcollins, mjacob
'''
import datetime
import logging
import os
import subprocess
from zipfile import ZipFile, ZIP_STORED
from sqlalchemy.dialects.postgresql.base import PGDialect
from edcore.utils.dbdumpfile import DBDumpFile, DBDumpFileException
from edcore.utils.utils import convert_sqlalchemy_url
from sqlalchemy.engine.url import make_url


logger = logging.getLogger('edmigrate')
FAILED_STATUS = "dump_process: failed"


def dump_tables_for_export(settings):
    """
    pg_dump the tables in settings' migrate dump_tables to a DBDumpFile
    Currently for StarMigrate's use.
    TODO: determine where to handle tenancy: settings?  or here?
    """
    logger.info("dump_process: started")
    filename_format = settings.get("migrate.dumpfile_name_format")
    logger.info("got filename_format: %s" % (filename_format))
    file_path = settings.get("migrate.dumpfile_location")
    logger.info("got file_path: %s" % (file_path))
    tenant = settings['migrate.tenant']

    dbfile = DBDumpFile(filename_format=filename_format, location=file_path, tenant=tenant)

    schema_key = 'migrate_dest.db.{tenant}.schema_name'.format(tenant=tenant)
    schema = settings[schema_key]
    tables = settings['migrate.dump_tables'].split(',')

    rpt_db_url_key = 'migrate_dest.db.{tenant}.url'.format(tenant=tenant)
    sqlalchemy_url_string = settings[rpt_db_url_key]
    sqlalchemy_url = make_url(sqlalchemy_url_string)
    db_dialect = sqlalchemy_url.get_dialect()

    if issubclass(db_dialect, PGDialect):
        db_conn_string = convert_sqlalchemy_url(sqlalchemy_url)
        tmp_filename_format = settings['migrate.dump_temp_file_format']
        tmp_filename = tmp_filename_format.format(dumpid=datetime.datetime.now())

        tmp_csv_gz_file_path = settings['migrate.dump_temp_file_path']

        commands, file_patches = _build_csv_gz_commands(db_conn_string, schema, tables, tmp_csv_gz_file_path)

        # success or failures, these files should be deleted anyway
        unlink_files_list = []
        unlink_files_list.append(tmp_filename)
        unlink_files_list.extend(file_patches)
        try:
            # TODO: encryption will be done via piping pgdump.out through e.g. gpg's stdin
            # (encrypt_for_tenant(data, tenant)
            with ZipFile(tmp_filename, mode='w', compression=ZIP_STORED, allowZip64=True) as zf:
                processes = [subprocess.Popen(command, shell=True) for command in commands]
                returncodes = [p.wait() for p in processes]
                for fpath in file_patches:
                    zf.write(fpath, arcname=os.path.basename(fpath))

        except Exception as e:
            logger.error("dump_process: failed")
            raise DBDumpFileException("dbdump failed to create archive with dump!") from e

        else:
            if any(returncodes) != 0:
                error_code_msgs = set("error code {}!".format(code) for code in returncodes if code != 0)
                logger.error(FAILED_STATUS)
                for msg in error_code_msgs:
                    logger.error("dump_process: {}".format(msg))
                    raise DBDumpFileException("dbdump failed w/ {}".format(msg))
            else:
                try:
                    dbfile.write_from_file(tmp_filename)
                    dbfile.complete_file()
                except Exception as e:
                    logger.error(FAILED_STATUS)
                    logger.error(e)
                    raise DBDumpFileException("dbdump failed to write dump into bucket!") from e
                else:
                    logger.info("dump_process: success")

        finally:
            # We need to be sure that tmp_files deleted even if something was wrong
            for fpath in unlink_files_list:
                if os.path.isfile(fpath):
                    os.unlink(fpath)
    else:
        logger.error(FAILED_STATUS)
        raise NotImplementedError("unimplemented db dialect for load: {} ({})".format(db_dialect,
                                                                                      type(db_dialect)))


def encrypt_for_tenant(data, tenant=None):
    """
    placeholder to gpg_encrypt the data for a given tenant
    """
    return data


def _build_csv_gz_commands(db_conn_string: str, schema: str, tables: list, tmp_file_path: str)-> list:
    """
    returns commands that create csv.gz files for required tables and patch where to store those files
    """
    file_patch = []
    commands = []

    for table in tables:
        tmp_filename = tmp_file_path.format(table)
        mask = 'psql {db} -c "COPY {schema}.{table} TO STDOUT DELIMITERS \',\' CSV HEADER" | gzip -8 > {f}'
        command = mask.format(db=db_conn_string, schema=schema, table=table, f=tmp_filename)

        commands.append(command)
        logger.info('Table to csv.gz archive: {}'.format(os.path.basename(tmp_filename)))
        file_patch.append(tmp_filename)

    return commands, file_patch
