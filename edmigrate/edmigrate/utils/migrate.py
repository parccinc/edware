from datetime import datetime, timedelta
import logging
import json

from edmigrate.exceptions import EdMigrateUdl_statException
from sqlalchemy.sql.expression import select, and_
from edmigrate.migrate.migrate_by_batch import MigrateBySnapshot
from edmigrate.migrate.migrate_by_row import migrate_by_row
from edmigrate.utils.constants import Constants
from edcore.database.stats_connector import StatsDBConnection
from edmigrate.database.migrate_source_connector import EdMigrateSourceConnection
from edmigrate.database.migrate_dest_connector import EdMigrateDestConnection
from edcore.database.utils.constants import UdlStatsConstants, LoadType
from edcore.database.utils.utils import drop_schema
from edschema.metadata.int_metadata import generate_int_metadata

__author__ = 'sravi'
TABLES_NOT_CONNECTED_WITH_BATCH = []
logger = logging.getLogger('edmigrate')
admin_logger = logging.getLogger(Constants.EDMIGRATE_ADMIN_LOGGER)


def get_dest_table_name_from_src(src_table_name):
    return src_table_name.replace('int_', 'rpt_', 1)


def create_config():
    return {'load_status': UdlStatsConstants.UDL_STATUS_INGESTED,
            'success_status': UdlStatsConstants.MIGRATE_INGESTED}


def get_batches_by_status(tenant, load_status):
    """ This function returns daily batches to be migrated or dumped
    for a given tenant

    :param tenant: The tenant to be migrated or db dump
    :type tenant: str
    :returns: A list mapping batch_guid to the row in udl_stats table
            An empty list if no batches found
    """
    logger.info('Master: Getting daily delta batches with status: %s ' % load_status +
                ('with tenant: ' + tenant) if tenant else '')
    with StatsDBConnection() as connector:
        udl_status_table = connector.get_table(UdlStatsConstants.UDL_STATS)
        query = \
            select([udl_status_table.c.rec_id,
                    udl_status_table.c.batch_guid,
                    udl_status_table.c.schema_name,
                    udl_status_table.c.tenant,
                    udl_status_table.c.load_type,
                    udl_status_table.c.dest_tenant,
                    udl_status_table.c.load_status,
                    udl_status_table.c.batch_operation,
                    udl_status_table.c.snapshot_criteria],
                   from_obj=[udl_status_table]).\
            where(udl_status_table.c.load_status == load_status).\
            order_by(udl_status_table.c.file_arrived)
        if tenant:
            query = query.where(and_(udl_status_table.c.tenant == tenant))
        batches = connector.get_result(query)
    return batches


def report_udl_stats_batch_status(rec_id, load_status):
    """
    This method populates udl_stats for batches that were migrated
    or dumped.

    :param rec_id: The batch id that was migrated
    :param load_status: migration or dump status.
    :returns rowcount: insertion count
    """
    logger.info('Master: Reporting batch status to udl_stats')
    with StatsDBConnection() as connector:
        udl_stats_table = connector.get_table(UdlStatsConstants.UDL_STATS)
        update_query = udl_stats_table.update().values(load_status=load_status).\
            where(udl_stats_table.c.rec_id == rec_id)
        rtn = connector.execute(update_query)
        rowcount = rtn.rowcount
        if rowcount == 0:
            raise EdMigrateUdl_statException('Failed to update record for rec_id=%s' % rec_id)
    return rowcount


def cleanup_batch(batch):
    """Cleanup the pre-prod schema for the given batch

    :param batch_guid: Batch Guid of the batch under migration

    :returns true: sucess, false: fail (for UT purpose)
    """
    rtn = False
    batch_guid = batch[UdlStatsConstants.BATCH_GUID]
    tenant = batch[UdlStatsConstants.TENANT]
    schema_name = batch[UdlStatsConstants.SCHEMA_NAME]
    logger.info('Cleaning up batch: ' + batch_guid + ',for tenant: ' + tenant)
    with EdMigrateSourceConnection(tenant) as source_connector:
        try:
            drop_schema(source_connector, schema_name)
            logger.info('Master: Cleanup successful for batch: ' + batch_guid)
            rtn = True
        except Exception as e:
            logger.info('Exception happened while cleaning up batch: ' + batch_guid)
            logger.info(e)
    return rtn


def complete_batch(batch_guid):
    """Check if migrate completes given batch.

    :param batch guid: Batch guid current being processed
    :returns: True if status of all rows with given batch guid
              become `UdlStatsConstants.MIGRATE_INGESTED`, return False otherwise.
    """
    with StatsDBConnection() as connector:
        udl_status_table = connector.get_table(UdlStatsConstants.UDL_STATS)
        query = select([udl_status_table.c.rec_id]).\
            where(and_(udl_status_table.c.load_status != UdlStatsConstants.MIGRATE_INGESTED,
                       udl_status_table.c.batch_guid == batch_guid))
        result = connector.get_result(query)
        return not result


class MigrateProcess(object):

    def __init__(self, tenant):
        # only query specific tenant when migrating from pre-prod to
        # product database
        self._tenant = tenant
        config = create_config()
        self._load_status = config['load_status']
        self._success_status = config['success_status']

    def start(self):
        """migration starting point for a tenant

        :param tenant: Tenant name of the tenant to perform the migration

        :returns Nothing
        """
        migrate_ok_count = 0
        batches_to_migrate = get_batches_by_status(self._tenant, self._load_status)
        if batches_to_migrate:
            for batch in batches_to_migrate:
                batch_guid = batch[UdlStatsConstants.BATCH_GUID]
                logger.debug('processing batch_guid: ' + batch_guid)
                if self.migrate_batch(batch):
                    migrate_ok_count += 1
                    admin_logger.info('Migrating batch[' + batch_guid + '] is processed')
                else:
                    admin_logger.info('Migrating batch[' + batch_guid + '] failed')
                if complete_batch(batch_guid):
                    cleanup_batch(batch=batch)
        else:
            logger.debug('no batch found for migration')
            admin_logger.info('no batch found for migration')
        return migrate_ok_count, len(batches_to_migrate)

    def migrate_batch(self, batch, source_metadata_generator=generate_int_metadata):
        """Migrates data for the given batch and given tenant

        :param batch_guid: Batch Guid of the batch under migration

        :returns true: sucess, false: fail (for UT purpose)
        """
        rtn = False
        rec_id = batch[UdlStatsConstants.REC_ID]
        batch_guid = batch[UdlStatsConstants.BATCH_GUID]
        tenant = batch[UdlStatsConstants.TENANT]
        dest_tenant = batch[UdlStatsConstants.DEST_TENANT]
        schema_name = batch[UdlStatsConstants.SCHEMA_NAME]
        load_type = batch[UdlStatsConstants.LOAD_TYPE]
        batch_op = batch[UdlStatsConstants.BATCH_OPERATION]
        batch_criteria = batch[UdlStatsConstants.SNAPSHOT_CRITERIA]
        # this flag will be set to false from unit test, if this is not set its always True
        deactivate = batch.get(Constants.DEACTIVATE, True)
        logger.info('Migrating batch: %s,for tenant: %s', batch_guid, tenant)

        with EdMigrateDestConnection(dest_tenant) as dest_connector, \
                EdMigrateSourceConnection(tenant) as source_connector:
            try:
                # start transaction for this batch
                trans = dest_connector.get_transaction()
                source_connector.set_metadata_by_generate(schema_name=schema_name,
                                                          metadata_func=source_metadata_generator)
                report_udl_stats_batch_status(rec_id, UdlStatsConstants.MIGRATE_IN_PROCESS)
                src_tables_to_migrate = self.get_ordered_tables_to_migrate(source_connector, load_type)
                # migrate all tables
                self.migrate_all_tables(batch_guid,
                                        schema_name,
                                        source_connector,
                                        dest_connector,
                                        src_tables_to_migrate,
                                        deactivate=deactivate,
                                        batch_op=batch_op,
                                        batch_criteria=batch_criteria)
                # report udl stats with the new batch migrated
                report_udl_stats_batch_status(rec_id, self._success_status)
                # commit transaction
                trans.commit()
                logger.info('Master: Migration successful for batch: ' + batch_guid)
                rtn = True
            except Exception as e:
                logger.info('Exception happened while migrating batch: ' + batch_guid + ' - Rollback initiated')
                logger.info(e)
                print(e)
                logger.exception('migrate rollback')
                trans.rollback()
                try:
                    report_udl_stats_batch_status(rec_id, UdlStatsConstants.MIGRATE_FAILED)
                except Exception as e:
                    pass
        return rtn

    def get_ordered_tables_to_migrate(self, connector, load_type):
        """This function returns an ordered list of tables to be migrated based on schema metadata.

        :param connector: The connection to the database
        @param load_type: The load type for the current table

        @return: A list of table names ordered by dependencies
        """
        return [table.name for table in connector.get_metadata().sorted_tables if self._include_table(table.name, load_type)]

    def migrate_all_tables(self, batch_guid, schema_name, source_connector, dest_connector, src_table_names, deactivate, batch_op, batch_criteria):
        """Migrate all tables for the given batch from source to destination

        :param batch_guid: Batch Guid of the batch under migration
        :param schema_name: Schema name for this batch
        :param source_connector: Source connection
        :param dest_connector: Destination connection
        :param src_table_names: list of Tables to be migrated

        :returns Nothing
        """
        logger.info('Migrating all tables for batch: ' + batch_guid)
        for src_table_name in src_table_names:
            dest_table_name = get_dest_table_name_from_src(src_table_name)
            self.migrate_table(batch_guid, schema_name,
                               source_connector, dest_connector,
                               src_table_name, dest_table_name, deactivate,
                               batch_op=batch_op, batch_criteria=batch_criteria)

    def migrate_table(self, batch_guid, schema_name, source_connector, dest_connector, src_table_name, dest_table_name,
                      deactivate, batch_op=None, batch_criteria=None, batch_size=1000):
        """Load prod fact table with delta from pre-prod

        :param batch_guid: Batch Guid of the batch under migration
        :param schema_name: Schema name for this batch
        :param source_connector: Source connection
        :param dest_connector: Destination connection
        :param src_table_name: name of the table to be migrated
        :param batch_size: batch size for migration

        :returns number of record updated
        """
        if schema_name:
            logger.debug('migrating schema[' + schema_name + ']')
        logger.debug('migrating table[' + src_table_name + ']')

        if batch_op and batch_op == UdlStatsConstants.SNAPSHOT:
            migrator = MigrateBySnapshot(batch_criteria)
            delete_count, insert_count = migrator.migrate(dest_connector, source_connector, src_table_name, dest_table_name,
                                                          batch_size=batch_size)
        else:
            delete_count, insert_count = migrate_by_row(batch_guid, batch_size, deactivate, dest_connector,
                                                        source_connector, src_table_name, dest_table_name)

        return delete_count, insert_count

    def _include_table(self, table_name, load_type):
        """
        Determine if table name should be included in tables to migrate.

        @param table_name: Name of table candidate
        @param load_type: Load type of table candidate

        @return: Whether or not table name should be included in migration.
        """
        table_select_criteria = {
            LoadType.LOAD_TYPE_ASSESSMENT_ELA: table_name in ['int_ela_sum',
                                                              'int_sum_test_level_m',
                                                              'int_sum_test_score_m'
                                                              ],
            LoadType.LOAD_TYPE_ASSESSMENT_MATH: table_name in ['int_math_sum',
                                                               'int_sum_test_level_m',
                                                               'int_sum_test_score_m'
                                                               ],
            LoadType.LOAD_TYPE_ITEM: table_name in ['int_student_item_score', 'int_file_store'],
            LoadType.LOAD_TYPE_PSYCHOMETRIC: table_name in ['int_item_p_data'],
            LoadType.LOAD_TYPE_MYA_ELA_ITEM: table_name in ['int_item_ela_mya'],
            LoadType.LOAD_TYPE_MYA_MATH_ITEM: table_name in ['int_item_math_mya'],
            LoadType.LOAD_TYPE_MYA_ASMT_ELA: table_name in ['int_ela_mya',
                                                            'int_admin',
                                                            'int_asmt',
                                                            'int_asmt_lvl',
                                                            'int_form',
                                                            'int_form_group',
                                                            'int_form_group_item',
                                                            'int_item_mya_meta'
                                                            ],
            LoadType.LOAD_TYPE_MYA_ASMT_MATH: table_name in ['int_math_mya',
                                                             'int_admin',
                                                             'int_asmt',
                                                             'int_asmt_lvl',
                                                             'int_form',
                                                             'int_form_group',
                                                             'int_form_group_item',
                                                             'int_item_mya_meta'
                                                             ],
            LoadType.LOAD_TYPE_SNL_STUDENT_TASK: table_name in ['int_student_task_snl'],

            LoadType.LOAD_TYPE_SNL_ASMT: table_name in ['int_ela_snl',
                                                        'int_admin',
                                                        'int_snl_asmt_m',
                                                        'int_snl_asmt_mode_m',
                                                        'int_asmt_lvl',
                                                        'int_form',
                                                        'int_form_group',
                                                        'int_form_group_item',
                                                        'int_item_mya_meta'
                                                        ],
            LoadType.LOAD_TYPE_MYA_ASMT_MATH: table_name in ['int_math_mya',
                                                             'int_admin',
                                                             'int_asmt',
                                                             'int_asmt_lvl',
                                                             'int_form',
                                                             'int_form_group',
                                                             'int_form_group_item',
                                                             'int_item_mya_meta'
                                                             ],
            LoadType.LOAD_TYPE_SNL_STUDENT_TASK: table_name in ['int_student_task_snl'],

            LoadType.LOAD_TYPE_SNL_ASMT: table_name in ['int_ela_snl',
                                                        'int_admin',
                                                        'int_snl_asmt_m',
                                                        'int_snl_asmt_mode_m',
                                                        'int_asmt_lvl',
                                                        'int_snl_task_m'],

            LoadType.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_READING_COMP: table_name in ['int_item_ela_read_comp'],

            LoadType.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_VOCAB: table_name in ['int_item_ela_vocab'],

            LoadType.LOAD_TYPE_DIAGNOSTIC_ELA_ITEM_DECODING: table_name in ['int_item_ela_decod'],

            LoadType.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_COMP: table_name in ['int_item_math_comp'],

            LoadType.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_PROGRESS: table_name in ['int_item_math_progress'],

            LoadType.LOAD_TYPE_DIAGNOSTIC_MATH_ITEM_FLU: table_name in ['int_item_math_flu'],

            LoadType.LOAD_TYPE_DIAGNOSTIC_ELA_VOCAB: table_name in ['int_ela_vocab'],

            LoadType.LOAD_TYPE_DIAGNOSTIC_ELA_COMP: table_name in ['int_ela_comp'],

            LoadType.LOAD_TYPE_DIAGNOSTIC_ELA_DECODING: table_name in ['int_ela_decod'],

            LoadType.LOAD_TYPE_DIAGNOSTIC_ELA_READ_FLU: table_name in ['int_ela_read_flu'],

            LoadType.LOAD_TYPE_DIAGNOSTIC_ELA_WRITING: table_name in ['int_ela_writing'],

            LoadType.LOAD_TYPE_DIAGNOSTIC_MATH_LOCATOR: table_name in ['int_math_locator'],

            LoadType.LOAD_TYPE_DIAGNOSTIC_MATH_PROGRESS: table_name in ['int_math_progress'],

            LoadType.LOAD_TYPE_DIAGNOSTIC_MATH_FLU: table_name in ['int_math_flu'],

            LoadType.LOAD_TYPE_DIAGNOSTIC_MATH_GRADE_LEVEL: table_name in ['int_math_grade_level'],

            LoadType.LOAD_TYPE_DIAGNOSTIC_MATH_CLUSTER: table_name in ['int_math_cluster'],

            LoadType.LOAD_TYPE_READER_MOTIVATION: table_name in ['int_reader_motiv'],

            LoadType.LOAD_TYPE_ELA_ITEM: table_name in ['int_ela_sum_item_score'],

            LoadType.LOAD_TYPE_MATH_ITEM: table_name in ['int_math_sum_item_score'],
        }

        return table_name not in TABLES_NOT_CONNECTED_WITH_BATCH and table_select_criteria.get(load_type, False)
