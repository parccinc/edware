'''
Created on May 1, 2015

@author: dip
'''
from edmigrate.utils.utils import get_broker_url
from edmigrate.edmigrate_celery import celery
from kombu import Connection


def get_broker_connection(settings):
    '''
    Returns connection of broker for conductor
    '''
    url = get_broker_url(settings)
    celery.conf.update(BROKER_URL=url)
    connect = Connection(url)
    return connect
