'''
Entry point for migrating data from pre-prod to prod

Created on Mar 13, 2014

@author: dip
'''
from edcore.database.utils.constants import UdlStatsConstants
from edmigrate.utils.migrate import MigrateProcess, report_udl_stats_batch_status, get_batches_by_status
import os
from edcore.database import initialize_db
from edcore.database.stats_connector import StatsDBConnection
from edmigrate.database.migrate_source_connector import EdMigrateSourceConnection
from edmigrate.database.migrate_dest_connector import EdMigrateDestConnection
from edmigrate.database.repmgr_connector import RepMgrDBConnection
from argparse import ArgumentParser
from edmigrate.utils.utils import read_ini
import logging
import logging.config
from edmigrate.utils.consumer import ConsumerThread
import sys
import signal
from edmigrate.utils.replication_admin_monitor import ReplicationAdminMonitor
from edcore.utils.utils import run_cron_job
from edmigrate.utils.constants import Constants
from edmigrate.conductor_controller import process_conductor
from edmigrate.utils.common import get_broker_connection
from edmigrate.utils.dumpdb import dump_tables_for_export


logger = logging.getLogger('edmigrate')
pidfile = None


def signal_handler(signal, frame):
    logger.info('Received kill[' + str(signal) + ']')
    os.unlink(pidfile)
    os._exit(0)


def get_ini_file():
    '''
    Get ini file path name
    '''
    jenkins_ini = '/opt/edware/conf/parcc.ini'
    if os.path.exists(jenkins_ini):
        ini_file = jenkins_ini
    else:
        here = os.path.abspath(os.path.dirname(__file__))
        ini_file = os.path.join(here, '../../config/development.ini')
    return ini_file


def run_cron_migrate(settings):
    run_cron_job(settings, 'migrate.conductor.', migrate_task)


def post_dump_status(batch_rec_ids, dump_status):
    """
    Writes dump status into database.
    Only successfully migrated batches are allowed.

    :param batch_rec_ids: The batch id list that for dumping
    :param dump_status: dump status.
    """
    for rec_id in batch_rec_ids:
        report_udl_stats_batch_status(rec_id, dump_status)


def dump_task(settings):
    """
    Run the dump DB task and record dump status

    :param settings: Configuration dict
    :return: None:
    """
    dump_status = UdlStatsConstants.DUMPDB_STARTED
    migration_status = UdlStatsConstants.MIGRATE_INGESTED
    batches = get_batches_by_status(settings.get(Constants.MIGRATOR_TENANT), migration_status)
    batch_rec_ids = [batch[UdlStatsConstants.REC_ID] for batch in batches]
    post_dump_status(batch_rec_ids, dump_status)
    try:
        dump_tables_for_export(settings)
        dump_status = UdlStatsConstants.DUMPDB_CREATED
    except Exception as e:
        dump_status = UdlStatsConstants.DUMPDB_FAILED
        raise e
    finally:
        post_dump_status(batch_rec_ids, dump_status)


def migrate_task(settings):
    tenant = settings.get(Constants.MIGRATOR_TENANT)
    migrate_ok_status = process_conductor(tenant, settings)
    if migrate_ok_status:
        # write a dump file to trigger starmigrate
        dump_task(settings)


def run_with_conductor(daemon_mode, settings):
    logger.debug('edmigrate main program has started')
    connect = get_broker_connection(settings)
    consumerThread = ConsumerThread(connect)
    try:
        consumerThread.start()
        if daemon_mode:
            replication_lag_tolerance = settings.getint(Constants.REPMGR_ADMIN_REPLICATION_LAG_TOLERANCE, 100)
            apply_lag_tolerance = settings.getint(Constants.REPMGR_ADMIN_APPLY_LAG_TOLERANCE, 100)
            time_lag_tolerance = settings.getint(Constants.REPMGR_ADMIN_TIME_LAG_TOLERANCE, 100)
            interval_check = settings.getint(Constants.REPMGR_ADMIN_CHECK_INTERVAL, 1800)
            tenant = settings.get(Constants.MIGRATOR_TENANT)
            replicationAdminMonitor = ReplicationAdminMonitor(MigrateProcess(tenant),
                                                              replication_lag_tolerance=replication_lag_tolerance,
                                                              apply_lag_tolerance=apply_lag_tolerance,
                                                              time_lag_tolerance=time_lag_tolerance,
                                                              interval_check=interval_check)
            run_cron_migrate(settings)
            replicationAdminMonitor.start()
            consumerThread.join()
        else:
            migrate_task(settings)
        consumerThread.stop()
    except KeyboardInterrupt:
        logger.debug('terminated by a user')
        os._exit(0)
    except Exception as e:
        logger.error(e)
        os._exit(1)
    logger.debug('exiting edmigrate main program')


def create_daemon(_pidfile):
    global pidfile
    pidfile = _pidfile
    if os.path.isfile(pidfile):
        print('pid file[' + pidfile + '] still exist.  please check your system.')
        os._exit(1)
    if not os.path.isdir(os.path.dirname(pidfile)):
        os.mkdir(os.path.dirname(pidfile))
    pid = os.fork()
    if pid == 0:
        os.setsid()
        with open(pidfile, 'w') as f:
            f.write(str(os.getpid()))
        os.chdir('/')
        os.umask(0)
    else:  # parent goes bye bye
        os._exit(0)

    si = os.open('/dev/null', os.O_RDONLY)
    so = os.open('/dev/null', os.O_RDWR)
    se = os.open('/dev/null', os.O_RDWR)
    os.dup2(si, sys.stdin.fileno())
    os.dup2(so, sys.stdout.fileno())
    os.dup2(se, sys.stderr.fileno())
    os.close(si)
    os.close(so)
    os.close(se)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGHUP, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)


def initialize_dbs(run_migrate_only, settings):
    initialize_db(StatsDBConnection, settings)
    initialize_db(EdMigrateSourceConnection, settings)
    initialize_db(EdMigrateDestConnection, settings)
    if not run_migrate_only:
        initialize_db(RepMgrDBConnection, settings)


def migrate_only(settings, force_dump: bool=False):
    initialize_dbs(True, settings)
    tenant = settings.get(Constants.MIGRATOR_TENANT)
    migrate_processor = MigrateProcess(tenant)
    migrate_ok_count, total_process = migrate_processor.start()
    if migrate_ok_count or force_dump:
        dump_task(settings)


def process(settings, daemon_mode, pid_file):
    initialize_dbs(False, settings)
    if daemon_mode:
        create_daemon(pid_file)
    run_with_conductor(daemon_mode, settings)


def main():
    parser = ArgumentParser(description='EdMigrate entry point')
    parser.add_argument('--migrateOnly', action='store_true', dest='migrate_only', default=False, help="migrate only mode")
    parser.add_argument('--force-dump', action='store_true', default=False, help='Force dumping db even if no migration, for testing')
    parser.add_argument('-p', dest='pidfile', default='/opt/edware/run/edmigrate.pid', help="pid file for daemon")
    parser.add_argument('-d', dest='daemon', action='store_true', default=False, help="daemon")
    parser.add_argument('-i', dest='ini_file', default='/opt/edware/conf/parcc.ini', help="ini file")
    parser.add_argument('-t', dest='type', choices=['tenant', 'consortium'],
                        required=False, default='tenant', help="migration type")
    parser.add_argument('-n', dest='name', choices=['cat', 'dog'],
                        required=False, help="Tenant name when type = tenant")
    args = parser.parse_args()
    # CR do not daemon when migrateOnly
    file = args.ini_file
    if file is None or not os.path.exists(file):
        file = get_ini_file()
    logging.config.fileConfig(file)
    settings = read_ini(file)

    run_migrate_only = args.migrate_only

    initialize_dbs(run_migrate_only, settings)

    daemon_mode = args.daemon
    pid_file = args.pidfile

    if args.name:
        settings[Constants.MIGRATOR_TENANT] = args.name

    if run_migrate_only:
        migrate_only(settings, force_dump=args.force_dump)
    else:
        process(settings, daemon_mode, pid_file)


if __name__ == '__main__':
    main()
