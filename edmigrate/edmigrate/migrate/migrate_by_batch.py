import json
from edmigrate.migrate.migrate_helper import yield_rows
from edmigrate.database.migrate_dest_connector import reflect_metadata
from sqlalchemy.sql.expression import select, and_
from edmigrate.utils.constants import Constants

__author__ = 'ablum'


class MigrateBySnapshot():

    def __init__(self, batch_criteria):
        self.snapshot_criteria = json.loads(batch_criteria)

    def migrate(self, dest_connector, source_connector, src_table_name, dest_table_name, truncate=True, batch_size=100):
        """

        Migrate a table snapshot as part of a migration by batch.

        @param dest_connector: Destination DB connector
        @param source_connector: Source DB connector
        @param src_table_name: Name of table to migrate
        @param dest_table_name: Name of the destination table
        @param batch_criteria: Deletion criteria for destination table.
        @param truncate: delete in target first
        @return: Deletion and insertion row counts

        """
        # use reflection to run insertion to avoid columns mismatch
        with reflect_metadata(dest_connector) as dest_conn:
            # Delete old rows.
            dest_table = dest_conn.get_table(dest_table_name)
            delete_count = 0
            if truncate:
                delete_query = self.get_delete_query(dest_table)
                delete_count = dest_conn.execute(delete_query).rowcount

            # Insert new rows.
            insert_count = 0
            source_table = source_connector.get_table(src_table_name)
            batched_rows = yield_rows(source_connector,
                                      self.get_select_query(source_connector, source_table),
                                      batch_size)
            insert_query = dest_table.insert()
            for rows in batched_rows:
                insert_count += dest_conn.execute(insert_query, rows).rowcount

        return delete_count, insert_count

    def get_select_query(self, conn, table):
        select_query = select([table])
        return self.append_criteria(select_query, table)

    def get_delete_query(self, table):
        delete_query = table.delete()
        return self.append_criteria(delete_query, table)

    def append_criteria(self, query, table):
        for criteria in self.snapshot_criteria:
            column_name = criteria[Constants.COLUMN_NAME]
            op = criteria[Constants.OP]
            value = criteria[Constants.VALUE]
            query = query.where(and_(table.c[column_name].op(op)(value)))
        return query
