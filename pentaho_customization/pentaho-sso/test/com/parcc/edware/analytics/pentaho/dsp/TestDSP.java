package com.parcc.edware.analytics.pentaho.dsp;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.security.Authentication;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.GrantedAuthorityImpl;
import org.springframework.security.context.SecurityContext;
import org.springframework.security.context.SecurityContextHolder;

import com.parcc.edware.analytics.constants.PARCCDistrict;
import com.parcc.edware.analytics.constants.PARCCStates;
import com.parcc.edware.analytics.pentaho.dsp.EdwareSchemaProcessor;

import mondrian.olap.Util;

public class TestDSP {
    private static final String GOOD_CUBE_SCHEMA_PATH = "../content/Sum_schema.xml";

    @Mock
    private final SecurityContext mockSecurityContext = Mockito.mock(SecurityContext.class);
    @Mock
    private final Authentication mockAuth = Mockito.mock(Authentication.class);

    private final PARCCStates parccStates = new PARCCStates(new ArrayList<>(Arrays.asList("AR", "CO", "NY", "NJ")));
    private final PARCCDistrict parccDistrict = new PARCCDistrict(this.parccStates);
    private final EdwareSchemaProcessor esp = new EdwareSchemaProcessor(this.parccStates, this.parccDistrict);

    @Before
    public void setup() {
        final GrantedAuthority[] gaArray = new GrantedAuthority[] { new GrantedAuthorityImpl("NY"), new GrantedAuthorityImpl("NY_12345") };
        this.initializeMocks(gaArray);
    }

    @Test
    public void testBadDistrict() throws Exception {
        final GrantedAuthority[] gaArray = new GrantedAuthority[] { new GrantedAuthorityImpl("NY"), new GrantedAuthorityImpl("XXXXX") };
        final String schemaAfter = this.updateSchemaGivenId(gaArray);
        Assert.assertFalse(schemaAfter.contains(EdwareSchemaProcessor.STATE_AND_DISTRICT_TOKEN));
        Assert.assertFalse(schemaAfter.contains("'XXXXX'"));
        Assert.assertFalse(schemaAfter.contains("district_id IN"));
    }

    @Test
    public void testGoodDistrict() throws Exception {
        final GrantedAuthority[] gaArray = new GrantedAuthority[] { new GrantedAuthorityImpl("NY"), new GrantedAuthorityImpl("NY_XXXXX") };
        final String schemaAfter = this.updateSchemaGivenId(gaArray);
        Assert.assertTrue(schemaAfter.contains("district_id IN ('XXXXX')"));
    }

    @Test
    public void testStateOnlyNoDistrict() throws Exception {
        final GrantedAuthority[] gaArray = new GrantedAuthority[] { new GrantedAuthorityImpl("NY"), new GrantedAuthorityImpl("blah"), new GrantedAuthorityImpl("safdafasdh") };
        final String schemaAfter = this.updateSchemaGivenId(gaArray);
        Assert.assertFalse(schemaAfter.contains(EdwareSchemaProcessor.STATE_AND_DISTRICT_TOKEN));
        Assert.assertFalse(schemaAfter.contains("district_id IN"));
    }

    @Test
    public void testTwoDistrict() throws Exception {
        final GrantedAuthority[] gaArray = new GrantedAuthority[] { new GrantedAuthorityImpl("NY"), new GrantedAuthorityImpl("NY_XXXXX"), new GrantedAuthorityImpl("NY_YYYYY") };
        final String schemaAfter = this.updateSchemaGivenId(gaArray);
        Assert.assertTrue(schemaAfter.contains("district_id IN ('XXXXX','YYYYY')"));
    }

    @Test
    public void testActual() throws Exception {
        final String schemaBefore = this.getUnfilteredSchemaString();
        final String schemaAfter = this.getFilteredSchemaString();
        Assert.assertFalse(schemaBefore.equalsIgnoreCase(schemaAfter));
        Assert.assertTrue(schemaAfter.contains("'12345'"));
    }

    @Test
    public void testOddDistrict() throws Exception {
        final GrantedAuthority[] gaArray = new GrantedAuthority[] { new GrantedAuthorityImpl("NY"), new GrantedAuthorityImpl("NY_") };
        final String schemaAfter = this.updateSchemaGivenId(gaArray);
        Assert.assertFalse(schemaAfter.contains(EdwareSchemaProcessor.STATE_AND_DISTRICT_TOKEN));
        Assert.assertFalse(schemaAfter.contains("district_id IN "));
    }

    private String updateSchemaGivenId(final GrantedAuthority[] gaArray) throws Exception {
        this.initializeMocks(gaArray);
        return this.getFilteredSchemaString();
    }

    private void initializeMocks(final GrantedAuthority[] gaArray) {
        Mockito.when(this.mockAuth.getAuthorities()).thenReturn(gaArray);
        Mockito.when(this.mockSecurityContext.getAuthentication()).thenReturn(this.mockAuth);
        SecurityContextHolder.setContext(this.mockSecurityContext);
    }

    private String getUnfilteredSchemaString() throws IOException {
        final byte[] data = Files.readAllBytes(Paths.get(TestDSP.GOOD_CUBE_SCHEMA_PATH));
        return new String(data, Charset.defaultCharset()).replaceAll("\\r\\n|\\r|\\n", " ").trim();
    }

    private String getFilteredSchemaString() throws Exception {
        try (InputStream schemaStream = new FileInputStream(TestDSP.GOOD_CUBE_SCHEMA_PATH)) {
            return this.esp.filter(null, new Util.PropertyList(), schemaStream).replaceAll("\\r\\n|\\r|\\n", " ").trim();
        }
    }

}
