package com.parcc.edware.analytics.pentaho.dsp;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.parcc.edware.analytics.constants.PARCCDistrict;
import com.parcc.edware.analytics.constants.PARCCStates;

public class TestPARCC_States_Dist {
    private static final List<String> STATE_CODES = new ArrayList<>(Arrays.asList("AR", "CO", "NY", "NJ", "MS"));
    private static final List<String> badStates = new ArrayList<String>(Arrays.asList("Ak", "co", "ar", "sdfasere", "fud", "cat"));
    private static final List<String> validDist = new ArrayList<String>(Arrays.asList("AR_12345", "AR_AR_12312", "CO_#$#$V SF", "NY_QWREVWE", "NJ_3423",
            "MS_2312"));
    private static final List<String> validDistId = new ArrayList<String>(Arrays.asList("12345", "AR_12312", "#$#$V SF", "QWREVWE", "3423",
            "2312"));
    private static final List<String> badDist = new ArrayList<String>(Arrays.asList("_____", "co_3423", "NX23424", "FSFDSDFS", "FUD", "cat"));

    protected PARCCStates parccStates;
    protected PARCCDistrict parccDistrict;

    @Before
    public void setup(){
        this.parccStates = new PARCCStates(TestPARCC_States_Dist.STATE_CODES);
        this.parccDistrict = new PARCCDistrict(this.parccStates);
    }

    @Test
    public void testStateString() {
        for (String state : TestPARCC_States_Dist.STATE_CODES) {
            Assert.assertTrue(this.parccStates.isValid(state));
        }
        for (String state : TestPARCC_States_Dist.badStates) {
            Assert.assertFalse(this.parccStates.isValid(state));
        }
    }

    @Test
    public void testGrantedAuthority() throws Exception {
        for (String state : TestPARCC_States_Dist.STATE_CODES) {
            Assert.assertTrue(parccStates.isValid(state));
        }
        for (String state : TestPARCC_States_Dist.badStates) {
            Assert.assertFalse(parccStates.isValid(state));
        }
    }

    @Test
    public void testDistrictString() throws Exception {
        for (String district : TestPARCC_States_Dist.validDist) {
            Assert.assertTrue(district + " not valid?", parccDistrict.isValid(district));
        }
        for (String district : TestPARCC_States_Dist.badDist) {
            Assert.assertFalse(district + " is valid?", parccDistrict.isValid(district));
        }
    }

    @Test
    public void testDynamicStates() {
        final PARCCStates dynamicStates = new PARCCStates(new ArrayList<String>(Arrays.asList("WW", "XX", "YY")));
        final PARCCDistrict parccDistrict = new PARCCDistrict(dynamicStates);
        Assert.assertTrue(parccDistrict.isValid("WW_asfasdf"));
        Assert.assertFalse(parccDistrict.isValid("NY_asfasdf"));
    }

}