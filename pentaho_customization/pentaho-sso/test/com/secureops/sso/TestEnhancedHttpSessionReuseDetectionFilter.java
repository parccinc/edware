package com.secureops.sso;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.security.context.SecurityContext;
import org.springframework.security.context.SecurityContextHolder;

public class TestEnhancedHttpSessionReuseDetectionFilter {
    private static EnhancedHttpSessionReuseDetectionFilter ehsrdt;

    @BeforeClass
    public static void setup() {
        TestEnhancedHttpSessionReuseDetectionFilter.ehsrdt = new EnhancedHttpSessionReuseDetectionFilter();
        TestEnhancedHttpSessionReuseDetectionFilter.ehsrdt.setFilterProcessesUrl("/j_spring_security_check");
        TestEnhancedHttpSessionReuseDetectionFilter.ehsrdt.setSessionReuseDetectedUrl("/Logout");
        TestEnhancedHttpSessionReuseDetectionFilter.ehsrdt.setSsoFilterProcessesUrls(new String[] { "/j_spring_saml_security_check", "/saml/sso/parcc" });
    }

    @Test
    public void testDoFilter1() throws Exception {
        final HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        final FilterChain chain = Mockito.mock(FilterChain.class);
        final HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getRequestURI()).thenReturn("http://foo.bar/saml/sso/parcc");
        TestEnhancedHttpSessionReuseDetectionFilter.ehsrdt.doFilter(request, response, chain);
    }

    @Test
    public void testDoFilter2() throws Exception {
        final HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        final FilterChain chain = Mockito.mock(FilterChain.class);
        final HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getRequestURI()).thenReturn("http://foo.bar/j_spring_saml_security_check");
        TestEnhancedHttpSessionReuseDetectionFilter.ehsrdt.doFilter(request, response, chain);
    }

    @Test
    public void testDoFilter2a() throws Exception {
        final HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        final FilterChain chain = Mockito.mock(FilterChain.class);
        final HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getRequestURI()).thenReturn("http://foo.bar/notauthenticated");

        TestEnhancedHttpSessionReuseDetectionFilter.ehsrdt.doFilter(request, response, chain);
    }

    @Test
    public void testDoFilter3() throws Exception {
        final HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        final FilterChain chain = Mockito.mock(FilterChain.class);
        final HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getRequestURI()).thenReturn("http://foo.bar/j_spring_saml_security_check");
        Mockito.when(request.getRemoteUser()).thenReturn("SamL");
        TestEnhancedHttpSessionReuseDetectionFilter.ehsrdt.doFilter(request, response, chain);
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        Assert.assertNotNull(securityContext);
    }

    @Test
    public void testDoFilter4() throws Exception {
        final HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        final FilterChain chain = Mockito.mock(FilterChain.class);
        final HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getRequestURI()).thenReturn("http://foo.bar/j_spring_saml_security_check");
        Mockito.when(request.getRemoteUser()).thenReturn("SamL");
        TestEnhancedHttpSessionReuseDetectionFilter.ehsrdt.doFilter(request, response, chain);
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        Assert.assertNotNull(securityContext);
    }

    @Test
    public void testDoFilter4a() throws Exception {
        final HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        final FilterChain chain = Mockito.mock(FilterChain.class);
        final HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getRequestURI()).thenReturn("http://foo.bar/notauthenticated");
        Mockito.when(request.getRemoteUser()).thenReturn("SamL");
        TestEnhancedHttpSessionReuseDetectionFilter.ehsrdt.doFilter(request, response, chain);
    }
}
