package com.secureops.sso.saml;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.opensaml.common.SAMLException;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.opensaml.xml.ConfigurationException;
import org.springframework.security.Authentication;
import org.springframework.security.providers.UsernamePasswordAuthenticationToken;
import org.springframework.web.context.WebApplicationContext;

import com.secureops.sso.saml.consumer.SamlConsumer;
import com.secureops.sso.saml.consumer.TestSamlConsumer;

public class TestSAMLLogoutHandler {
	private SAMLLogoutHandler samlLogoutHandler = null;
	private SamlConsumer samlConsumer;
	private HttpServletRequest request;
	private HttpServletResponse response;

	@Before
	public void setup() throws IOException, MetadataProviderException, ConfigurationException, SAMLException {
		request = mock(HttpServletRequest.class);
		response = mock(HttpServletResponse.class);
		ServletOutputStream mockOutput = mock(ServletOutputStream.class);
		when(response.getOutputStream()).thenReturn(mockOutput);
		
		TestSamlConsumer tsc = new TestSamlConsumer();
		tsc.setup();
		samlConsumer = tsc.getSamlConsumer();
		
		HttpSession mockSession = mock(HttpSession.class);
		ServletContext mockServletContext = mock(ServletContext.class);
		
		WebApplicationContext mockWAC = mock(WebApplicationContext.class);
		when(mockWAC.getBean("samlConsumer")).thenReturn(this.samlConsumer);
		when(mockServletContext.getAttribute("org.springframework.web.context.WebApplicationContext.ROOT")).thenReturn(mockWAC);
		when(mockSession.getServletContext()).thenReturn(mockServletContext);
		when(request.getSession()).thenReturn(mockSession);

		
		samlLogoutHandler = new SAMLLogoutHandler();
	}

	@Test
	public void test_logout1() {
		final Authentication authentication = null;
		samlLogoutHandler.logout(request, response, authentication);
	}

	@Test
	public void test_logout2() {
		
		final Authentication authentication = new UsernamePasswordAuthenticationToken(
				"steve", "foobar");
		samlLogoutHandler.logout(request, response, authentication);
	}

	@Test
	public void test_logout3() {
		final Authentication auth = mock(Authentication.class);
		when(auth.getCredentials()).thenReturn(TestSAMLCredential.getSAMLCredential());
		samlLogoutHandler.logout(request, response, auth);
	}

	@Test
	public void test_logout4() {
		samlLogoutHandler.logout(request, response, null);
	}

}
