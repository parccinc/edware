package com.secureops.sso.saml;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import org.opensaml.common.SAMLObject;
import org.opensaml.common.binding.BasicSAMLMessageContext;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.GrantedAuthorityImpl;

public class TestSAMLAuthenticationToken {
    private static SAMLAuthenticationToken token;
    /*
     * Base implemention of {@link SAMLMessageContext}.
     *
     * @param <InboundMessageType> type of inbound SAML message
     * @param <OutboundMessageType> type of outbound SAML message
     * @param <NameIdentifierType> type of name identifier used for subjects
     */
    private static BasicSAMLMessageContext<SAMLObject, SAMLObject, SAMLObject> bsm;

    @SuppressWarnings("unchecked")
    @BeforeClass
    public static void setup() {
        TestSAMLAuthenticationToken.token = Mockito.mock(SAMLAuthenticationToken.class);
        final GrantedAuthority[] ga = new GrantedAuthority[] {new GrantedAuthorityImpl("NY"), new GrantedAuthorityImpl("1234")};

        TestSAMLAuthenticationToken.bsm = Mockito.mock(BasicSAMLMessageContext.class);
        Mockito.when(TestSAMLAuthenticationToken.token.getName()).thenReturn("ElmerFud");
        Mockito.when(TestSAMLAuthenticationToken.token.getCredentials()).thenReturn(TestSAMLAuthenticationToken.bsm);
        Mockito.when(TestSAMLAuthenticationToken.token.getAuthorities()).thenReturn(ga);
    }

    @Test
    public void testAuthorities() {
        Assert.assertEquals(2, TestSAMLAuthenticationToken.token.getAuthorities().length);
        Assert.assertEquals("NY", TestSAMLAuthenticationToken.token.getAuthorities()[0].toString());
        Assert.assertEquals("1234", TestSAMLAuthenticationToken.token.getAuthorities()[1].toString());
        Assert.assertEquals("ElmerFud", TestSAMLAuthenticationToken.token.getName());
    }

    @Test
    public void testCreds() {
        final SAMLAuthenticationToken sat = new SAMLAuthenticationToken(TestSAMLAuthenticationToken.bsm);
        Assert.assertEquals(TestSAMLAuthenticationToken.bsm, sat.getCredentials());
        Assert.assertNull(sat.getPrincipal());
    }

    public static SAMLAuthenticationToken getToken() {
        return TestSAMLAuthenticationToken.token;
    }

    @SuppressWarnings("rawtypes")
    public static BasicSAMLMessageContext getAuthenticationResponse() {
        return TestSAMLAuthenticationToken.bsm;
    }
}
