package com.secureops.sso.saml.consumer;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.opensaml.DefaultBootstrap;
import org.opensaml.common.SAMLException;
import org.opensaml.common.binding.BasicSAMLMessageContext;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.core.Status;
import org.opensaml.saml2.core.StatusCode;
import org.opensaml.saml2.metadata.provider.FilesystemMetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.encryption.DecryptionException;
import org.opensaml.xml.parse.BasicParserPool;
import org.opensaml.xml.security.SecurityException;
import org.opensaml.xml.signature.Signature;
import org.opensaml.xml.validation.ValidationException;

import com.secureops.sso.saml.SAMLCredential;
import com.secureops.sso.saml.TestSAMLCredential;
import com.secureops.sso.saml.metadata.SAMLMetadataProvider;

public class TestSamlConsumer {
	private static SamlConsumerImpl consumer = null;

	@Before
	public void setup() throws IOException, MetadataProviderException, ConfigurationException, SAMLException {
		final String keyAlias = "pentahoSamlKey";
		final String storePass = "storePass123";
		final String keyPass = "keyPass123";
		
		ClassLoader classLoader = getClass().getClassLoader();
		final File storeFile = new File(classLoader.getResource("resources/samlKeystore.jks").getFile());

		final BasicParserPool parser = new BasicParserPool();
		final SAMLMetadataProvider metadata = getMetadata(parser);
		consumer = new SamlConsumerImpl(metadata, storeFile, keyAlias, storePass, keyPass);
		consumer.setParser(parser);
	}

	private SAMLMetadataProvider getMetadata(final BasicParserPool parser) throws MetadataProviderException, IOException,
			ConfigurationException {

		DefaultBootstrap.bootstrap();
		ClassLoader classLoader = getClass().getClassLoader();

		final List<MetadataProvider> providers = new ArrayList<>();
		{
			final File metadataFile = new File(classLoader.getResource("resources/analytics-meta.xml").getFile());
			final FilesystemMetadataProvider provider = new FilesystemMetadataProvider(metadataFile);
			provider.setParserPool(parser);
			provider.initialize();
			providers.add(provider);
		}
		{
			final File metadataFile = new File(classLoader.getResource("resources/idp_metadata.xml").getFile());
			final FilesystemMetadataProvider provider = new FilesystemMetadataProvider(metadataFile);
			provider.setParserPool(parser);
			provider.initialize();
			providers.add(provider);
		}
		final Map<String, String> idpMap = new HashMap<>();
		idpMap.put("parcc", "http://openam.dev.parccresults.org:8080/OpenAM-11.0.0");
		final String sp = "http://10.117.0.48:8080/pentaho";
		return new SAMLMetadataProvider(providers, idpMap, sp);
	}

	public SamlConsumerImpl getSamlConsumer() throws IOException, MetadataProviderException, ConfigurationException, SAMLException {
		return consumer;
	}

	@Test
	public void testHandleLogin() throws SAMLException, IOException {

		final HttpServletRequest request = mock(HttpServletRequest.class);
		when(request.getParameter("saml_issuer_key")).thenReturn("parcc");
		final HttpServletResponse response = mock(HttpServletResponse.class);
		ServletOutputStream mockOutput = mock(ServletOutputStream.class);
		when(response.getOutputStream()).thenReturn(mockOutput);
		consumer.handleLogin(request, response);
	}

	/*
	 * This is incomplete need to encode a valid response and load it into
	 * the request object. Seems like a lot of work to prove working code works
	 * 
	 * @Test
	 */
	public void handleAuthenticationResponseFromIdP() throws SAMLException {
		final HttpServletRequest request = mock(HttpServletRequest.class);
		when(request.getMethod()).thenReturn("Post");
		String retStr = new String();
		when(request.getParameter("SAMLRequest")).thenReturn(retStr);
		consumer.handleAuthenticationResponseFromIdP(request);
	}

	@Test
	public void testLogoutInitiatedByPentahoUser() throws SAMLException, SecurityException, IOException {
		final HttpServletResponse response = mock(HttpServletResponse.class);
		ServletOutputStream mockOutput = mock(ServletOutputStream.class);
		when(response.getOutputStream()).thenReturn(mockOutput);
		final SAMLCredential samlCredential = TestSAMLCredential.getSAMLCredential();
		consumer.logoutInitatedByPentahoUser(response, samlCredential);
	}

	/*
	 * Code is not active in implementation. 
	 * PARCC IdP is not sending logout message
	 * 
	 * @Test
	 */
	public void testLogoutInitiatedFromIdP() throws SAMLException {
		final HttpServletRequest request = mock(HttpServletRequest.class);
		final HttpServletResponse response = mock(HttpServletResponse.class);
		final SAMLCredential samlCredential = TestSAMLCredential.getSAMLCredential();
		when(samlCredential.getSessionIndex()).thenReturn("SomeNonRandomIndexGeneratedAtLogin");
		consumer.logoutInitiatedFromIdP(request, response, samlCredential);
	}

	/*
	 * Got hung up on providing signatures, tested code does signature validation
	 * Would need to figure out how to mock the signatures
	 * 
	 * 	@Test
	 */
	@SuppressWarnings("rawtypes")
	public void testVerifyResponse() throws SAMLException, SecurityException, ValidationException, DecryptionException {

		Signature sig = mock(Signature.class);

		final BasicSAMLMessageContext context = mock(BasicSAMLMessageContext.class);
		Response msg = mock(Response.class);

		StatusCode statCode = mock(StatusCode.class);
		when(statCode.getValue()).thenReturn(StatusCode.SUCCESS_URI);
		Status stat = mock(Status.class);
		when(stat.getStatusCode()).thenReturn(statCode);

		when(msg.getStatus()).thenReturn(stat);
		when(msg.getSignature()).thenReturn(sig);

		when(context.getInboundSAMLMessage()).thenReturn(msg);
		when(context.getPeerEntityId()).thenReturn("PeerEntityId");
		consumer.verifyResponse(context);
	}

}
