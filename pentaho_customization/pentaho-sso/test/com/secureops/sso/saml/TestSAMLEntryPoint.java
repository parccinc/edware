package com.secureops.sso.saml;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.opensaml.common.SAMLException;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.opensaml.xml.ConfigurationException;
import org.springframework.security.AuthenticationException;
import org.springframework.security.ui.FilterChainOrder;

import com.secureops.sso.saml.consumer.SamlConsumer;
import com.secureops.sso.saml.consumer.TestSamlConsumer;

public class TestSAMLEntryPoint {
	private static String TEST_FILTER_PROCESS_URL = "parcc";
	private static SAMLEntryPoint samlEntryPoint;
	private static HttpServletRequest request;
	private static HttpServletResponse response;

	@BeforeClass
	public static void setup() throws IOException, MetadataProviderException,
			ConfigurationException, SAMLException {
		samlEntryPoint = new SAMLEntryPoint();
		samlEntryPoint.setFilterProcessesUrl(TEST_FILTER_PROCESS_URL);
		final TestSamlConsumer tsc = new TestSamlConsumer();
		tsc.setup();
		final SamlConsumer consumer = tsc.getSamlConsumer(); 
		samlEntryPoint.setConsumer(consumer);
		
		request = mock(HttpServletRequest.class);
		when(request.getParameter("saml_issuer_key")).thenReturn("parcc");
		response = mock(HttpServletResponse.class);
		ServletOutputStream mockOutput = mock(ServletOutputStream.class);
		when(response.getOutputStream()).thenReturn(mockOutput);
		when(request.getRequestURI()).thenReturn("parcc");

	}

	@Test
	public void test_doFilterHttp() throws IOException, ServletException {
		
		final FilterChain chain = mock(FilterChain.class);
		samlEntryPoint.doFilterHttp(request, response, chain);
	}

	@Test
	public void test_doFilter() throws IOException, ServletException {
		
		final FilterChain chain = mock(FilterChain.class);
		samlEntryPoint.doFilter(request, response, chain);
	}

	@Test
	public void test_commence() throws ServletException {
		final AuthenticationException e = null;
		samlEntryPoint.commence(request, response, e);
	}

	@Test
	public void test_getFilterProcessesUrl() {
		final String url = samlEntryPoint.getFilterProcessesUrl();
		Assert.assertNotNull(url);
		Assert.assertEquals("parcc", url);
	}

	@Test
	public void test_getOrder() {
		final int order = samlEntryPoint.getOrder();
		Assert.assertEquals(FilterChainOrder.PRE_AUTH_FILTER + 10, order);
	}
}
