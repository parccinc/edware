package com.secureops.sso.saml;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.NameID;

public class TestSAMLCredential {
	private static SAMLCredential samlCredential = null;

	@BeforeClass
	public static void setup() {
		samlCredential = getSAMLCredential();
	}

	public static SAMLCredential getSAMLCredential() {
		final NameID nameID = null;
		final Assertion authenticationAssertion = null;
		final String IDPEntityID = null;
		final String sessionIndex = null;
		final String userName = "Steve";
		final String fullName = "Steve Schafer";
		final List<String> roles = new ArrayList<>();
		final SAMLCredential samlCredential = new SAMLCredential(nameID,
				authenticationAssertion, IDPEntityID, sessionIndex, userName, fullName,
				roles);
		return samlCredential;
	}

	@Test
	public void test_getAuthenticationAssertion() {
		final Assertion authenticationAssertion = samlCredential
				.getAuthenticationAssertion();
		Assert.assertNull(authenticationAssertion);
	}

	@Test
	public void test_getIDPEntityID() {
		final String IDPEntityID = samlCredential.getIDPEntityID();
		Assert.assertNull(IDPEntityID);
	}

	@Test
	public void test_getNameID() {
		final NameID nameID = samlCredential.getNameID();
		Assert.assertNull(nameID);
	}

	@Test
	public void test_getRoles() {
		final List<String> roles = samlCredential.getRoles();
		Assert.assertNotNull(roles);
		Assert.assertTrue(roles.isEmpty());
	}

	@Test
	public void test_getSessionIndex() {
		final String sessionIndex = samlCredential.getSessionIndex();
		Assert.assertNull(sessionIndex);
	}

	@Test
	public void test() {
		final String userName = samlCredential.getUserName();
		Assert.assertEquals("Steve", userName);
	}
}
