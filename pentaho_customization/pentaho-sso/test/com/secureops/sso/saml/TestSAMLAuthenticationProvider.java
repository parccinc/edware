package com.secureops.sso.saml;

import static org.mockito.Mockito.mock;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.security.Authentication;

import com.secureops.sso.SsoUserDetailsService;
import com.secureops.sso.TestSsoUserDetailsService;
import com.secureops.sso.saml.consumer.SamlConsumer;
import com.secureops.sso.saml.consumer.TestSamlConsumer;

public class TestSAMLAuthenticationProvider {
	private static SAMLAuthenticationProvider provider = null;

	@BeforeClass
	public static void setup() throws Exception {
		provider = mock(SAMLAuthenticationProvider.class);
		TestSamlConsumer tsc = new TestSamlConsumer();
		tsc.setup();
		final SamlConsumer consumer = tsc.getSamlConsumer();
		provider.setConsumer(consumer);
		final SsoUserDetailsService userDetailsService = TestSsoUserDetailsService
				.getSsoUserDetailsService();
		provider.setUserDetailsService(userDetailsService);
		provider.afterPropertiesSet();
	}

	/*
	 * Same issue with SAMLConsumerImpl verifyResponse
	 * Need to verify the response of this object, hard to do
	 * 	@Test(expected = ClassCastException.class)
	 */
	public void testUsernamePasswordAuthentication() {
		final Authentication auth = TestSAMLAuthenticationToken.getToken();
		provider.authenticate(auth);
	}

	@Test
	public void testSAMLAuthentication() {
		final Authentication authentication = TestSAMLAuthenticationToken.getToken();
		provider.authenticate(authentication);
	}
}
