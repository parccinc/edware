package com.secureops.sso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.NameID;
import org.pentaho.platform.api.engine.security.userroledao.AlreadyExistsException;
import org.pentaho.platform.api.engine.security.userroledao.IPentahoRole;
import org.pentaho.platform.api.engine.security.userroledao.IPentahoUser;
import org.pentaho.platform.api.engine.security.userroledao.IUserRoleDao;
import org.pentaho.platform.api.engine.security.userroledao.NotFoundException;
import org.pentaho.platform.api.engine.security.userroledao.UncategorizedUserRoleDaoException;
import org.pentaho.platform.api.mt.ITenant;
import org.springframework.dao.DataAccessException;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.userdetails.UserDetails;
import org.springframework.security.userdetails.UserDetailsService;
import org.springframework.security.userdetails.UsernameNotFoundException;

import com.secureops.sso.saml.SAMLCredential;

public class TestSsoUserDetailsService {
	private static SsoUserDetailsService userDetailsService;

	@BeforeClass
	public static void setup() {
		userDetailsService = getSsoUserDetailsService();
	}

	public static SsoUserDetailsService getSsoUserDetailsService() {
		return new SsoUserDetailsService(new TestUserDetailsService());
	}

	@Test
	public void testLoadUserByUsername() throws Exception {
		// test loadUserByUsername
		userDetailsService.setUserRoleDao(null);
		final UserDetails userDetails = userDetailsService.loadUserByUsername("mock_username");
		Assert.assertEquals("mock_username", userDetails.getUsername());
		Assert.assertEquals("mock_password", userDetails.getPassword());
		final GrantedAuthority[] authorities = userDetails.getAuthorities();
		Assert.assertEquals(1, authorities.length);
		Assert.assertEquals("mock_authority", authorities[0].getAuthority());
	}

	@Test
	public void testCheckUser() {
		// test checkUser
		final UserRoleDao userRoleDao = new UserRoleDao();
		userRoleDao.createUser(null, "mock_username", "mock_password", "mock_description", new String[] { "mock_role" });
		userDetailsService.setUserRoleDao(userRoleDao);
		final NameID nameID = null;
		final Assertion authenticationAssertion = null;
		final String IDPEntityID = null;
		final String sessionIndex = null;
		final String userName = "mock_username";
		final String fullName = "A Full User Name";
		final List<String> roles = new ArrayList<String>();
		roles.add("mock_role");
		final SAMLCredential samlCredential = new SAMLCredential(nameID, authenticationAssertion, IDPEntityID, sessionIndex, userName, fullName,
				roles);
		userDetailsService.checkUser(samlCredential);
		final IPentahoRole pentahoRole = userRoleDao.getRole(null, "mock_role");
		Assert.assertNotNull(pentahoRole);
	}

	static class UserRoleDao implements IUserRoleDao {
		private final Map<String, PentahoRole> roles = new HashMap<>();
		private final Map<String, PentahoUser> users = new HashMap<>();

		public UserRoleDao() {
		}

		@Override
		public IPentahoRole createRole(final ITenant tenant, final String name, final String description, final String[] roles)
				throws AlreadyExistsException, UncategorizedUserRoleDaoException {
			final PentahoRole pentahoRole = new PentahoRole(name, description, null, roles);
			this.roles.put(name, pentahoRole);
			return pentahoRole;
		}

		@Override
		public IPentahoUser createUser(final ITenant tenant, final String username, final String password, final String description,
				final String[] roles) throws AlreadyExistsException, UncategorizedUserRoleDaoException {
			final PentahoUser pentahoUser = new PentahoUser(username, password, description, tenant, roles);
			this.users.put(username, pentahoUser);
			return pentahoUser;
		}

		@Override
		public void deleteRole(final IPentahoRole pentahoRole) throws NotFoundException, UncategorizedUserRoleDaoException {
			if (pentahoRole != null)
				this.roles.remove(pentahoRole.getName());
		}

		@Override
		public void deleteUser(final IPentahoUser pentahoUser) throws NotFoundException, UncategorizedUserRoleDaoException {
			if (pentahoUser != null)
				this.users.remove(pentahoUser.getUsername());
		}

		@Override
		public IPentahoRole getRole(final ITenant arg0, final String arg1) throws UncategorizedUserRoleDaoException {
			return roles.get(arg1);
		}

		@Override
		public List<IPentahoUser> getRoleMembers(final ITenant tenant, final String name) throws UncategorizedUserRoleDaoException {
			final Set<String> users = new HashSet<>();
			for (final PentahoUser pentahoUser : this.users.values()) {
				if (pentahoUser.roles.contains(name))
					users.add(pentahoUser.getUsername());
			}
			for (final PentahoRole pentahoRole : this.roles.values()) {
				if (pentahoRole.roles.contains(name)) {
					final List<IPentahoUser> pentahoUsers = getRoleMembers(tenant, pentahoRole.getName());
					for (final IPentahoUser pentahoUser : pentahoUsers) {
						users.add(pentahoUser.getUsername());
					}
				}
			}
			final List<IPentahoUser> pentahoUsers = new ArrayList<>();
			for (final String username : users) {
				pentahoUsers.add(getUser(tenant, username));
			}
			return pentahoUsers;
		}

		@Override
		public List<IPentahoRole> getRoles() throws UncategorizedUserRoleDaoException {
			final List<IPentahoRole> roles = new ArrayList<>();
			for (final PentahoRole pentahoRole : this.roles.values()) {
				roles.add(pentahoRole);
			}
			return roles;
		}

		@Override
		public List<IPentahoRole> getRoles(final ITenant arg0) throws UncategorizedUserRoleDaoException {
			return getRoles();
		}

		@Override
		public List<IPentahoRole> getRoles(final ITenant arg0, final boolean arg1) throws UncategorizedUserRoleDaoException {
			return getRoles();
		}

		@Override
		public IPentahoUser getUser(final ITenant tenant, final String username) throws UncategorizedUserRoleDaoException {
			return this.users.get(username);
		}

		@Override
		public List<IPentahoRole> getUserRoles(final ITenant tenant, final String username) throws UncategorizedUserRoleDaoException {
			final PentahoUser pentahoUser = this.users.get(username);
			if (pentahoUser == null)
				return null;
			final List<IPentahoRole> roles = new ArrayList<>();
			for (final String name : pentahoUser.roles) {
				roles.add(this.getRole(tenant, name));
			}
			return roles;
		}

		@Override
		public List<IPentahoUser> getUsers() throws UncategorizedUserRoleDaoException {
			final List<IPentahoUser> users = new ArrayList<>();
			for (final PentahoUser pentahoUser : this.users.values()) {
				users.add(pentahoUser);
			}
			return users;
		}

		@Override
		public List<IPentahoUser> getUsers(final ITenant arg0) throws UncategorizedUserRoleDaoException {
			return getUsers();
		}

		@Override
		public List<IPentahoUser> getUsers(final ITenant arg0, final boolean arg1) throws UncategorizedUserRoleDaoException {
			return getUsers();
		}

		@Override
		public void setPassword(final ITenant tenant, final String username, final String password) throws NotFoundException,
				UncategorizedUserRoleDaoException {
			final PentahoUser pentahoUser = this.users.get(username);
			// could get NPE
			pentahoUser.setPassword(password);
		}

		@Override
		public void setRoleDescription(final ITenant tenant, final String name, final String description) throws NotFoundException,
				UncategorizedUserRoleDaoException {
			final PentahoRole pentahoRole = this.roles.get(name);
			// could get NPE
			pentahoRole.setDescription(description);
		}

		@Override
		public void setRoleMembers(final ITenant tenant, final String name, final String[] memberUserNames) throws NotFoundException,
				UncategorizedUserRoleDaoException {
			for (final String memberUserName : memberUserNames) {
				final PentahoUser pentahoUser = this.users.get(memberUserName);
				// could get NPE
				pentahoUser.roles.add(name);
			}
		}

		@Override
		public void setUserDescription(final ITenant tenant, final String username, final String description) throws NotFoundException,
				UncategorizedUserRoleDaoException {
			final PentahoUser pentahoUser = this.users.get(username);
			// could get NPE
			pentahoUser.setDescription(description);
		}

		@Override
		public void setUserRoles(final ITenant tenant, final String username, final String[] roles) throws NotFoundException,
				UncategorizedUserRoleDaoException {
			final PentahoUser pentahoUser = this.users.get(username);
			if (pentahoUser == null)
				throw new NotFoundException("User '" + username + "' not found");
			pentahoUser.roles.clear();
			for (final String role : roles) {
				pentahoUser.roles.add(role);
			}
		}
	}

	static class PentahoRole implements IPentahoRole {
		private static final long serialVersionUID = 1L;
		private String description;
		private final String name;
		private final ITenant tenant;
		final Set<String> roles = new HashSet<>();

		public PentahoRole(final String name, final String description, final ITenant tenant, final String[] roles) {
			this.description = description;
			this.name = name;
			this.tenant = tenant;
			for (final String role : roles) {
				this.roles.add(role);
			}
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public ITenant getTenant() {
			return tenant;
		}

		@Override
		public void setDescription(final String description) {
			this.description = description;
		}
	}

	static class PentahoUser implements IPentahoUser {
		private static final long serialVersionUID = 1L;
		private String description;
		private final String name;
		private String password;
		private final ITenant tenant;
		private boolean enabled;
		final Set<String> roles = new HashSet<>();

		public PentahoUser(final String name, final String password, final String description, final ITenant tenant, final String[] roles) {
			this.name = name;
			this.password = password;
			this.description = description;
			this.tenant = tenant;
			for (final String role : roles) {
				this.roles.add(role);
			}
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public String getPassword() {
			return password;
		}

		@Override
		public ITenant getTenant() {
			return tenant;
		}

		@Override
		public String getUsername() {
			return name;
		}

		@Override
		public boolean isEnabled() {
			return enabled;
		}

		@Override
		public void setDescription(final String description) {
			this.description = description;
		}

		@Override
		public void setEnabled(final boolean enabled) {
			this.enabled = enabled;
		}

		@Override
		public void setPassword(final String password) {
			this.password = password;
		}
	}

	static class TestUserDetails implements UserDetails {
		private static final long serialVersionUID = 1L;

		@Override
		public GrantedAuthority[] getAuthorities() {
			return new GrantedAuthority[] { new TestGrantedAuthority("mock_authority") };
		}

		@Override
		public String getPassword() {
			return "mock_password";
		}

		@Override
		public String getUsername() {
			return "mock_username";
		}

		@Override
		public boolean isAccountNonExpired() {
			return false;
		}

		@Override
		public boolean isAccountNonLocked() {
			return false;
		}

		@Override
		public boolean isCredentialsNonExpired() {
			return false;
		}

		@Override
		public boolean isEnabled() {
			return false;
		}
	}

	static class TestGrantedAuthority implements GrantedAuthority {
		private static final long serialVersionUID = 1L;
		private final String authority;

		public TestGrantedAuthority(final String authority) {
			this.authority = authority;
		}

		@Override
		public int compareTo(final Object o) {
			if (!(o instanceof GrantedAuthority)) {
				return -1;
			}
			final GrantedAuthority ga = (GrantedAuthority) o;
			return this.authority.compareTo(ga.getAuthority());
		}

		@Override
		public String getAuthority() {
			return authority;
		}
	}

	static class TestUserDetailsService implements UserDetailsService {
		@Override
		public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException, DataAccessException {
			if ("mock_username" != username) {
				throw new UsernameNotFoundException("User not found");
			}
			return new TestUserDetails();
		}
	}
}
