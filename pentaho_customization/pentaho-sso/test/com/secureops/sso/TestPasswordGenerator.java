package com.secureops.sso;

import org.junit.Assert;
import org.junit.Test;

public class TestPasswordGenerator {
	@Test
	public void testGenerate() {
		final String password = PasswordGenerator.generate();
		Assert.assertNotNull(password);
		Assert.assertTrue(password.length() > 0);
	}

	@Test
	public void testGenerateLength() {
		final String password = PasswordGenerator.generate(10);
		Assert.assertNotNull(password);
		Assert.assertTrue(password.length() == 10);
	}

	@Test
	public void testGenerateInvalidLength() {
		boolean caughtException = false;
		try {
			PasswordGenerator.generate(0);
		}
		catch (final Exception e) {
			caughtException = true;
		}
		Assert.assertTrue(caughtException);
	}
}
