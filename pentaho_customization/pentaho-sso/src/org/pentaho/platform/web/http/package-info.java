/**
These two files are copied over from pentaho-extensions.
Including pentaho-extensions as a dependency drags in incompatible transitive dependencies.
Hence, it was easier to copy over than to resolve them.
Try it again in a year or something.
**/
package org.pentaho.platform.web.http;