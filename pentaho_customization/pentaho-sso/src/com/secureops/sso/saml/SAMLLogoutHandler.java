package com.secureops.sso.saml;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.Authentication;
import org.springframework.security.context.SecurityContextHolder;
import org.springframework.security.ui.logout.LogoutHandler;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.secureops.sso.saml.consumer.SamlConsumer;

public class SAMLLogoutHandler implements LogoutHandler {

   private static final Log logger = LogFactory.getLog(SAMLLogoutHandler.class);

	/**
	 * Wrapper class to get to SamlConsumer where actual SAML logout is performed.
	 */
	@Override
	public void logout(final HttpServletRequest request, final HttpServletResponse response, final Authentication authentication) {

		if (authentication == null){
			// On Pentaho Initiated Logout request, the first time in we have an authentication.
			// Further on we will create a LogoutRequest to send to OpenAM (Post binding)
			// OpenAM responds to that Logout request through a post back to Logout
			// No need to handle the request, since we are already logged out.
			return;
		}
		
		try {

			WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
			SamlConsumer samlConsumer = (SamlConsumer) wac.getBean("samlConsumer");
		
			Object credObj = authentication.getCredentials();
			if (credObj instanceof SAMLCredential) {
				SAMLCredential samlCredential = (SAMLCredential) credObj;
				logger.info("Initiated Logout event from Pentaho for user: " + samlCredential.getUserName());
				if (SecurityContextHolder.getContext() == null) {
					// this is a logout called by the IdP, need to respond to IdP and complete logout
					samlConsumer.logoutInitiatedFromIdP(request, response, samlCredential);
				} else {
					// this is a Pentaho initiated logout, need to notify IdP
					samlConsumer.logoutInitatedByPentahoUser(response, samlCredential);
				}
			} else {
				logger.info("Logout: Couldn't get the SAMLCredential: " + authentication.getName());
				return;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.debug("Oops" + ex.getMessage());
			ex.printStackTrace();
		}
		// LogoutHandler will return flow to standard Pentaho Logout.
			
	}

}
