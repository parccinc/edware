package com.secureops.sso.saml.consumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.opensaml.common.SAMLException;
import org.opensaml.common.binding.BasicSAMLMessageContext;
import org.opensaml.xml.encryption.DecryptionException;
import org.opensaml.xml.security.SecurityException;
import org.opensaml.xml.validation.ValidationException;

import com.secureops.sso.saml.SAMLCredential;


public interface SamlConsumer {

    /**
     * Handle login request by creating AuthRequest assertion and sending it to the IDP using the HTTP-POST binding.
     *
     * @param request  request
     * @param response response
     * @throws SAMLException             error
     */
    void handleLogin(HttpServletRequest request, HttpServletResponse response) throws SAMLException;

    /**
     * Handles the response and creates SAMLMessageContext object.
     *
     * @param request request
     * @return SAML message context with filled information about the message
     * @throws SAMLException             error retrieving the message from the request
     *
     */
    @SuppressWarnings("rawtypes")
	BasicSAMLMessageContext handleAuthenticationResponseFromIdP(HttpServletRequest request) throws SAMLException;

    /**
     * The input context object must have set the properties related to the returned Response, which is validated
     * and in case no errors are found the SAMLCredential is returned.
     *
     * @param context context including response object
     * @return SAMLCredential with information about user
     * @throws SAMLException in case the response is invalid
     * @throws SecurityException in the signature on response can't be verified
     * @throws ValidationException in case the response structure is not conforming to the standard
     * @throws DecryptionException in case the problem occurs trying to decrypt IdP response
     */
    @SuppressWarnings("rawtypes")
	SAMLCredential verifyResponse(BasicSAMLMessageContext context) throws SAMLException, SecurityException, ValidationException, DecryptionException;

	/**
	 * 
	 * @param response
	 * @param samlCredential
	 */
    void logoutInitatedByPentahoUser(HttpServletResponse response, SAMLCredential samlCredential) throws SAMLException, SecurityException;

	/**
	 * When a user logs out of the Circle Of Trust, the IdP will send a logout request to each active SP session
	 * This will initiate a complete logout by Pentaho as well as send a response to to the IdP
	 * 
	 * @param response
	 * @param samlCredential
	 * @throws SAMLException
	 * @throws SecurityException
	 */
    void logoutInitiatedFromIdP(HttpServletRequest request, HttpServletResponse response, SAMLCredential samlCredential)throws SAMLException;
        
}
