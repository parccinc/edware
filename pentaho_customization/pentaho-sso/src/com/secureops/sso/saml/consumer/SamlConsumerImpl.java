package com.secureops.sso.saml.consumer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.joda.time.DateTime;
import org.opensaml.Configuration;
import org.opensaml.common.SAMLException;
import org.opensaml.common.SAMLObject;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.common.SAMLRuntimeException;
import org.opensaml.common.SAMLVersion;
import org.opensaml.common.binding.BasicSAMLMessageContext;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml2.binding.decoding.BaseSAML2MessageDecoder;
import org.opensaml.saml2.binding.decoding.HTTPPostDecoder;
import org.opensaml.saml2.binding.encoding.HTTPPostEncoder;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.Audience;
import org.opensaml.saml2.core.AudienceRestriction;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.saml2.core.AuthnStatement;
import org.opensaml.saml2.core.Conditions;
import org.opensaml.saml2.core.EncryptedAssertion;
import org.opensaml.saml2.core.EncryptedAttribute;
import org.opensaml.saml2.core.IDPEntry;
import org.opensaml.saml2.core.IDPList;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.LogoutRequest;
import org.opensaml.saml2.core.LogoutResponse;
import org.opensaml.saml2.core.NameID;
import org.opensaml.saml2.core.NameIDType;
import org.opensaml.saml2.core.RequestAbstractType;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.core.Scoping;
import org.opensaml.saml2.core.SessionIndex;
import org.opensaml.saml2.core.StatusCode;
import org.opensaml.saml2.core.Subject;
import org.opensaml.saml2.core.SubjectConfirmation;
import org.opensaml.saml2.core.SubjectConfirmationData;
import org.opensaml.saml2.core.impl.IssuerBuilder;
import org.opensaml.saml2.core.impl.NameIDBuilder;
import org.opensaml.saml2.core.impl.SessionIndexBuilder;
import org.opensaml.saml2.encryption.Decrypter;
import org.opensaml.saml2.encryption.EncryptedElementTypeEncryptedKeyResolver;
import org.opensaml.saml2.metadata.AssertionConsumerService;
import org.opensaml.saml2.metadata.EntityDescriptor;
import org.opensaml.saml2.metadata.IDPSSODescriptor;
import org.opensaml.saml2.metadata.SPSSODescriptor;
import org.opensaml.saml2.metadata.SingleLogoutService;
import org.opensaml.saml2.metadata.SingleSignOnService;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.opensaml.security.MetadataCredentialResolver;
import org.opensaml.security.MetadataCriteria;
import org.opensaml.security.SAMLSignatureProfileValidator;
import org.opensaml.ws.transport.http.HTTPInTransport;
import org.opensaml.ws.transport.http.HttpServletRequestAdapter;
import org.opensaml.ws.transport.http.HttpServletResponseAdapter;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.encryption.ChainingEncryptedKeyResolver;
import org.opensaml.xml.encryption.DecryptionException;
import org.opensaml.xml.encryption.InlineEncryptedKeyResolver;
import org.opensaml.xml.encryption.SimpleRetrievalMethodEncryptedKeyResolver;
import org.opensaml.xml.parse.ParserPool;
import org.opensaml.xml.security.CriteriaSet;
import org.opensaml.xml.security.SecurityException;
import org.opensaml.xml.security.credential.Credential;
import org.opensaml.xml.security.credential.KeyStoreCredentialResolver;
import org.opensaml.xml.security.credential.UsageType;
import org.opensaml.xml.security.criteria.EntityIDCriteria;
import org.opensaml.xml.security.criteria.UsageCriteria;
import org.opensaml.xml.security.keyinfo.KeyInfoCredentialResolver;
import org.opensaml.xml.security.keyinfo.StaticKeyInfoCredentialResolver;
import org.opensaml.xml.signature.Signature;
import org.opensaml.xml.signature.impl.ExplicitKeySignatureTrustEngine;
import org.opensaml.xml.validation.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.AuthenticationException;
import org.springframework.security.BadCredentialsException;
import org.springframework.security.CredentialsExpiredException;

import com.secureops.sso.saml.SAMLCredential;
import com.secureops.sso.saml.metadata.SAMLMetadataProvider;

/**
 * Class implements SamlConsumer and offers capabilities for SP handle login request and
 * handle response coming from IDP. HTTP-POST binding is used.
 */
public class SamlConsumerImpl implements SamlConsumer {

	private static final String PENTAHO_LOGOUT_ACTION = "Pentaho Logout Action";
	//~ Static fields ==================================================================================================
	private final static Logger log = LoggerFactory.getLogger(SamlConsumerImpl.class);
	private static final int DEFAULT_PROXY_COUNT = 2;
	protected static final String BEARER_CONFIRMATION = "urn:oasis:names:tc:SAML:2.0:cm:bearer";
	// All times in milliseconds, these should reflect any time variation between machines as well as any network connectivity times.
	private static final int DEFAULT_RESPONSE_SKEW = 300000; // Maximum time from response creation when the message is deemed valid
	private static Hashtable<String, RequestAbstractType> messages = new Hashtable<String, RequestAbstractType>();
	private static ChainingEncryptedKeyResolver encryptedKeyResolver = new ChainingEncryptedKeyResolver();

	static {
		encryptedKeyResolver.getResolverChain().add(new InlineEncryptedKeyResolver());
		encryptedKeyResolver.getResolverChain().add(new EncryptedElementTypeEncryptedKeyResolver());
		encryptedKeyResolver.getResolverChain().add(new SimpleRetrievalMethodEncryptedKeyResolver());
	}

	//~ Instance fields ================================================================================================
	private SAMLMetadataProvider metadata;
	private KeyStoreCredentialResolver keyManager;
	private XMLObjectBuilderFactory builderFactory;
	private String keyAlias;
	private VelocityEngine velocityEngine;
	private ParserPool parser;
	private Decrypter decrypter;
	private ExplicitKeySignatureTrustEngine trustEngine; // Trust engine used to verify SAML signatures
	private final Random random = new SecureRandom();

	/**
	 * Initializes the saml consumer.
	 *
	 * @param metadata      metadata provider to be used
	 * @param storeFile     file pointing to the JKS keystore
	 * @param keyAlias      alias of key pair store in the keystore file
	 * @param storePass     password to access the keystore
	 * @param keyPass       password to access the keypair
	 * @throws SAMLException error initializing the consumer
	 */
	public SamlConsumerImpl(SAMLMetadataProvider metadata, File storeFile, String keyAlias, String storePass, String keyPass)
			throws SAMLException {
		this.metadata = metadata;
		this.builderFactory = Configuration.getBuilderFactory();

		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(storeFile);
			KeyStore keyStore = KeyStore.getInstance("JKS");
			keyStore.load(inputStream, storePass.toCharArray());

			Map<String, String> password = new HashMap<String, String>();
			password.put(keyAlias, keyPass);
			this.keyManager = new KeyStoreCredentialResolver(keyStore, password);
			this.keyAlias = keyAlias;

		} catch (FileNotFoundException e) {
			log.error("Key file not found", e);
			throw new SAMLException("Key file not found", e);
		} catch (IOException e) {
			log.error("Error initializing keystore", e);
			throw new SAMLException("Error initializing keystore", e);
		} catch (NoSuchAlgorithmException e) {
			log.error("Error initializing keystore", e);
			throw new SAMLException("Error initializing keystore", e);
		} catch (CertificateException e) {
			log.error("Error initializing keystore", e);
			throw new SAMLException("Error initializing keystore", e);
		} catch (KeyStoreException e) {
			log.error("Error initializing keystore", e);
			throw new SAMLException("Error initializing keystore", e);
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				log.error("Error closing keystore file", e);
			}
		}

		try {
			this.decrypter = getLocalDecrypter();
		} catch (SecurityException e) {
			log.error("Error initializing decrypter", e);
			throw new SAMLException("Error initializing decrypter", e);
		}

		MetadataCredentialResolver metadataCredentialResolver = new MetadataCredentialResolver(metadata);
		KeyInfoCredentialResolver keyInfoCredentialResolver = org.opensaml.xml.Configuration.getGlobalSecurityConfiguration()
				.getDefaultKeyInfoCredentialResolver();
		trustEngine = new ExplicitKeySignatureTrustEngine(metadataCredentialResolver, keyInfoCredentialResolver);

		try {
			velocityEngine = new VelocityEngine();
			velocityEngine.setProperty(RuntimeConstants.ENCODING_DEFAULT, "UTF-8");
			velocityEngine.setProperty(RuntimeConstants.OUTPUT_ENCODING, "UTF-8");
			velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
			velocityEngine.setProperty("classpath.resource.loader.class",
					"org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
			velocityEngine.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS, "org.apache.velocity.runtime.log.NullLogChute");
			velocityEngine.init();
		} catch (Exception e) {
			log.error("Error initializing velocity engine", e);
			throw new SAMLException("Error configuring velocity", e);
		}
	}

	/**
	 * Setter for the parser pool object
	 * @param parser parser pool
	 */
	public void setParser(ParserPool parser) {
		this.parser = parser;
	}

	/**
		 * IdP Initiated Logout requires Pentaho to logout and to send a response to IdP acknowledging logout
		 * TODO Build appropriate response to OpenAM Logout Request.
		 * At this time OpenAM is not sending a Logout Request to Pentaho, so probably not required.
		 *
		 * @param response
		 * @param samlCredential
	 * @throws SAMLException
		 */
	@Override
	public void logoutInitiatedFromIdP(HttpServletRequest request, HttpServletResponse response, SAMLCredential samlCredential) throws SAMLException {
		@SuppressWarnings("unchecked")
		BasicSAMLMessageContext<SAMLObject, SAMLObject, SAMLObject> bsmc = handleAuthenticationResponseFromIdP(request);
		log.debug(bsmc.toString());
	}

	/**
	 * Build a LogoutRequest which will be sent to the IdP Server
	 *
	 * @param response
	 * @param samlCredential
	 * @throws SAMLException
	 * @throws SecurityException
	 */
	@Override
	public void logoutInitatedByPentahoUser(HttpServletResponse response, SAMLCredential samlCredential) throws SAMLException, SecurityException {
		String sp = metadata.getSp();
		if (sp == null) {
			throw new SAMLException("Unable to logout, no SP meta");
		}

		for (Map.Entry<String, String> idpEntry : metadata.getIdpMap().entrySet()) {
			// Need to logout of each IDP, in practice for PARCC this is one IDP
			String idp = idpEntry.getValue();
			String idpKey = idpEntry.getKey();
			log.debug("Send logout request to: " + idpKey + " " + idp);

			try {
				EntityDescriptor idpEntityDescriptor = metadata.getEntityDescriptor(idp);
				IDPSSODescriptor idpssoDescriptor = (IDPSSODescriptor) metadata.getRole(idp, IDPSSODescriptor.DEFAULT_ELEMENT_NAME,
						SAMLConstants.SAML20P_NS);
				SPSSODescriptor spDescriptor = (SPSSODescriptor) metadata.getRole(sp, SPSSODescriptor.DEFAULT_ELEMENT_NAME,
						SAMLConstants.SAML20P_NS);
				String sloServiceURL = null;
				for (SingleLogoutService sls : idpEntityDescriptor.getIDPSSODescriptor(SAMLConstants.SAML20P_NS).getSingleLogoutServices()) {
					if (sls.getBinding().equals(SAMLConstants.SAML2_POST_BINDING_URI)) {
						sloServiceURL = sls.getLocation();
						break;
					}
				}
				AssertionConsumerService assertionConsumerService = getAssertionConsumerServiceForIdp(spDescriptor, "parcc");

				SingleLogoutService singleLogoutService = getSLOServiceForBinding(idpssoDescriptor, assertionConsumerService.getBinding());

				LogoutRequest logoutRequest = buildLogoutRequest(samlCredential.getSessionIndex());
				logoutRequest.setDestination(sloServiceURL);

				BasicSAMLMessageContext<SAMLObject, LogoutRequest, SAMLObject> samlMessageContext = new BasicSAMLMessageContext<SAMLObject, LogoutRequest, SAMLObject>();
				samlMessageContext.setOutboundMessageTransport(new HttpServletResponseAdapter(response, false));
				samlMessageContext.setOutboundSAMLMessage(logoutRequest);
				samlMessageContext.setPeerEntityEndpoint(singleLogoutService);
				samlMessageContext.setLocalEntityMetadata(metadata.getEntityDescriptor(metadata.getSp()));

				EntityDescriptor sp_desc = metadata.getEntityDescriptor(sp);
				String redirect_to = null;
				if (sp_desc != null) {
					redirect_to = sp_desc.getEntityID();
				}
				if (redirect_to != null) {
					log.warn("Setting relay state to '" + redirect_to + "' in the hope we redirect to it");
					samlMessageContext.setRelayState(redirect_to);
				} else {
					log.warn("Could not find a valid place to redirect logged out user!");
				}

				if (spDescriptor.isAuthnRequestsSigned() || idpssoDescriptor.getWantAuthnRequestsSigned()) {
					samlMessageContext.setOutboundSAMLMessageSigningCredential(getSPSigningCredential());
				}

				// Send the request using HTTP-POST binding
				HTTPPostEncoder encoder = new HTTPPostEncoder(velocityEngine, "/templates/saml2-post-binding.vm");
				encoder.encode(samlMessageContext);
				messages.put(logoutRequest.getID(), logoutRequest);

			} catch (Exception e) {
				log.error("An exception occurred during handling of the logout request: " + e.getMessage());
				throw new SAMLException("The exception occurred during handling logout request" + e.getMessage(), e);
			}
		}
	}

	/**
	 * Build a LogoutRequest
	 *
	 * @param sessionIdx
	 * @return
	 */
	private LogoutRequest buildLogoutRequest(final String sessionIdx) {

		LogoutRequest logoutReq = new org.opensaml.saml2.core.impl.LogoutRequestBuilder().buildObject();
		logoutReq.setID(generateRandomID());
		DateTime issueInstant = new DateTime();
		logoutReq.setIssueInstant(issueInstant);
		logoutReq.setNotOnOrAfter(new DateTime(issueInstant.getMillis() + 5 * 60 * 1000));

		Issuer newIssuer = new IssuerBuilder().buildObject();
		newIssuer.setValue(metadata.getSp());
		logoutReq.setIssuer(newIssuer);

		NameID nameId = new NameIDBuilder().buildObject();
		nameId.setFormat(NameIDType.TRANSIENT);
		nameId.setValue(PENTAHO_LOGOUT_ACTION);
		logoutReq.setNameID(nameId);
		SessionIndex sessionIndex = new SessionIndexBuilder().buildObject();
		sessionIndex.setSessionIndex(sessionIdx);
		logoutReq.getSessionIndexes().add(sessionIndex);
		logoutReq.setReason(LogoutResponse.USER_LOGOUT_URI);
		return logoutReq;
	}

	//************************ Login Methods ******************************
	/**
	 *
	 * {@inheritDoc}
	 *
	 * Creates initial entry and send SSO message to IdP
	 * <br>
	 * The implementation supports multi-IdP configuration. The required IdP is recognized based on Saml Issuer Key,
	 * which should be passed by request parameter "saml_issuer_key".
	 * This key should correspond to the keys of idpMap in SAMLMetadataProvider.
	 * <br>
	 * For PARCC, we will typically use a single IdP keyed as 'parcc'
	 *
	 */
	@Override
	public void handleLogin(HttpServletRequest request, HttpServletResponse response) throws SAMLException {
		String samlIssuerKey = request.getParameter("saml_issuer_key");
		if (samlIssuerKey == null) {
			samlIssuerKey = "parcc";
		}

		String idp = metadata.getIdpMap().get(samlIssuerKey);
		if (idp == null) {
			throw new SAMLException("Unable to handleLogin, no IdP Meta");
		}
		String sp = metadata.getSp();
		if (sp == null) {
			throw new SAMLException("Unable to handleLogin, no SP meta");
		}

		log.debug("handleLogin IdP: " + idp + " SP:" + sp);

		try {
			EntityDescriptor idpEntityDescriptor = metadata.getEntityDescriptor(idp);
			if (idpEntityDescriptor == null){
				throw new SAMLException("Failure to lookup IdP Entity Descriptor for: " + idp);
			}
			IDPSSODescriptor idpssoDescriptor = (IDPSSODescriptor) metadata.getRole(idp, IDPSSODescriptor.DEFAULT_ELEMENT_NAME,
					SAMLConstants.SAML20P_NS);
			if (idpssoDescriptor == null){
				throw new SAMLException("Failure to lookup IdP Descriptor for: " + idp);
			}

			SPSSODescriptor spDescriptor = (SPSSODescriptor) metadata.getRole(sp, SPSSODescriptor.DEFAULT_ELEMENT_NAME,
					SAMLConstants.SAML20P_NS);
			if (spDescriptor == null){
				throw new SAMLException("Failure to lookup SP Descriptor for: " + sp);
			}

			AssertionConsumerService assertionConsumerService = getAssertionConsumerServiceForIdp(spDescriptor, samlIssuerKey);
			if (assertionConsumerService == null){
				throw new SAMLException("Failure to lookup assertionConsumerService for: " + sp + " " + samlIssuerKey);
			}

			String binding = assertionConsumerService.getBinding();
			SingleSignOnService singleSignOnService = getSSOServiceForBinding(idpssoDescriptor, binding);
			if (singleSignOnService == null){
				throw new SAMLException("Failure to get singleSignOnService for " + binding + " on " + idp);
			}


			AuthnRequest authRequest = buildAuthnRequest(idpEntityDescriptor, assertionConsumerService, singleSignOnService);

			BasicSAMLMessageContext<SAMLObject, AuthnRequest, SAMLObject> samlMessageContext = new BasicSAMLMessageContext<SAMLObject, AuthnRequest, SAMLObject>();
			samlMessageContext.setOutboundMessageTransport(new HttpServletResponseAdapter(response, false));
			samlMessageContext.setOutboundSAMLMessage(authRequest);
			samlMessageContext.setPeerEntityEndpoint(singleSignOnService);
			samlMessageContext.setLocalEntityMetadata(metadata.getEntityDescriptor(metadata.getSp()));

			if (spDescriptor.isAuthnRequestsSigned() || idpssoDescriptor.getWantAuthnRequestsSigned()) {
				samlMessageContext.setOutboundSAMLMessageSigningCredential(getSPSigningCredential());
			}

			// Send the request using HTTP-POST binding
			HTTPPostEncoder encoder = new HTTPPostEncoder(velocityEngine, "/templates/saml2-post-binding.vm");
			encoder.encode(samlMessageContext);
			messages.put(authRequest.getID(), authRequest);

		} catch (Exception e) {
			throw new SAMLException("The exception occurred during handling login request: " + e.getMessage(), e);
		}
	}

	/**
	 * Returns SingleLogoutService for given binding of the SP.
	 *
	 * @param descriptor IDP to search for service in
	 * @param binding    binding supported by the service (HTTP_POST)
	 * @return SSO service capable of handling the HTTP_POST binding
	 * @throws MetadataProviderException if the service can't be determined
	 */
	private SingleLogoutService getSLOServiceForBinding(IDPSSODescriptor descriptor, String binding) throws MetadataProviderException {
		List<SingleLogoutService> services = descriptor.getSingleLogoutServices();
		for (SingleLogoutService service : services) {
			if (binding.equals(service.getBinding())) {
				return service;
			}
		}
		log.debug("No SLO binding found for IDP with binding " + binding);
		throw new MetadataProviderException("SLO Binding " + binding + " is not supported for this IDP");
	}

	/**
	 * Returns SingleSignOnService for given binding of the SP.
	 *
	 * @param descriptor IDP to search for service in
	 * @param binding    binding supported by the service (HTTP_POST)
	 * @return SSO service capable of handling the HTTP_POST binding
	 * @throws MetadataProviderException if the service can't be determined
	 */
	private SingleSignOnService getSSOServiceForBinding(IDPSSODescriptor descriptor, String binding) throws MetadataProviderException {
		List<SingleSignOnService> services = descriptor.getSingleSignOnServices();
		for (SingleSignOnService service : services) {
			if (binding.equals(service.getBinding())) {
				return service;
			}
		}
		log.debug("No SSO binding found for IDP with binding " + binding);
		throw new MetadataProviderException("SSO Binding " + binding + " is not supported for this IDP");
	}

	private AssertionConsumerService getAssertionConsumerServiceForIdp(SPSSODescriptor descriptor, String suffix)
			throws MetadataProviderException {
		List<AssertionConsumerService> services = descriptor.getAssertionConsumerServices();
		StringBuffer sb = new StringBuffer();
		sb.append("getAssertionConsumerService, looking for suffix: ").append(suffix).append("\n");
		for (AssertionConsumerService service : services) {
			sb.append(service.getLocation()).append("\n");
			if (service.getLocation().endsWith(suffix)) {
				sb.append("Found Assertion Consumer Service: ").append(service.getLocation());
				log.debug(sb.toString());
				return service;
			}
		}

		log.error("No service found for SP with specified idp");
		throw new MetadataProviderException("Assertion consumer service not supported for this SP\n" + sb.toString());
	}

	/**
	 * Returns Credential object used to sign the message issued by this entity.
	 * Public, X509 and Private keys are set in the credential.
	 *
	 * @return credential
	 */
	private Credential getSPSigningCredential() {
		try {
			CriteriaSet criteriaSet = new CriteriaSet();
			EntityIDCriteria criteria = new EntityIDCriteria(keyAlias);
			criteriaSet.add(criteria);
			return keyManager.resolveSingle(criteriaSet);
		} catch (SecurityException e) {
			throw new SAMLRuntimeException("Can't obtain SP signing key", e);
		}
	}

	private Decrypter getLocalDecrypter() throws SecurityException {
		CriteriaSet criteriaSet = new CriteriaSet();
		EntityIDCriteria criteria = new EntityIDCriteria(keyAlias);
		criteriaSet.add(criteria);

		// Locate encryption key for this entity
		Credential encryptionCredential = keyManager.resolveSingle(criteriaSet);

		// Entity used for decrypting of encrypted XML parts
		// Extracts EncryptedKey from the encrypted XML using the encryptedKeyResolver and attempts to decrypt it
		// using private keys supplied by the resolver.
		KeyInfoCredentialResolver resolver = new StaticKeyInfoCredentialResolver(encryptionCredential);

		Decrypter decrypter = new Decrypter(null, resolver, encryptedKeyResolver);
		decrypter.setRootInNewDocument(true);

		return decrypter;
	}

	/**
	 * Returns AuthnRequest SAML message to be used to demand authentication from an IDP described using
	 * idpEntityDescriptor, with an expected response to the assertionConsumer address.
	 *
	 * @param idpEntityDescriptor entity descriptor of IDP this request should be sent to
	 * @param assertionConsumerService   assertion consumer where the IDP should respond
	 * @param singleSignOnService        service used to deliver the request
	 * @return authnRequest              ready to be sent to IDP
	 * @throws SAMLException             error creating the message
	 * @throws MetadataProviderException error retrieving metadata
	 */
	@SuppressWarnings("unchecked")
	private AuthnRequest buildAuthnRequest(EntityDescriptor idpEntityDescriptor, AssertionConsumerService assertionConsumerService,
			SingleSignOnService singleSignOnService) throws SAMLException, MetadataProviderException {

		SAMLObjectBuilder<AuthnRequest> authnBuilder = (SAMLObjectBuilder<AuthnRequest>) builderFactory
				.getBuilder(AuthnRequest.DEFAULT_ELEMENT_NAME);
		AuthnRequest authnRequest = authnBuilder.buildObject();

		authnRequest.setID(generateRandomID());
		authnRequest.setVersion(SAMLVersion.VERSION_20);
		authnRequest.setIssueInstant(new DateTime());
		authnRequest.setDestination(singleSignOnService.getLocation());
		authnRequest.setAssertionConsumerServiceURL(assertionConsumerService.getLocation());
		authnRequest.setProtocolBinding(assertionConsumerService.getBinding());

		//build issuer
		SAMLObjectBuilder<Issuer> issuerBuilder = (SAMLObjectBuilder<Issuer>) builderFactory.getBuilder(Issuer.DEFAULT_ELEMENT_NAME);
		Issuer issuer = issuerBuilder.buildObject();
		issuer.setValue(metadata.getSp());
		authnRequest.setIssuer(issuer);

		//build scoping
		SAMLObjectBuilder<IDPEntry> idpEntryBuilder = (SAMLObjectBuilder<IDPEntry>) builderFactory
				.getBuilder(IDPEntry.DEFAULT_ELEMENT_NAME);
		IDPEntry idpEntry = idpEntryBuilder.buildObject();
		idpEntry.setProviderID(idpEntityDescriptor.getEntityID());
		idpEntry.setLoc(singleSignOnService.getLocation());

		SAMLObjectBuilder<IDPList> idpListBuilder = (SAMLObjectBuilder<IDPList>) builderFactory.getBuilder(IDPList.DEFAULT_ELEMENT_NAME);
		IDPList idpList = idpListBuilder.buildObject();
		idpList.getIDPEntrys().add(idpEntry);

		SAMLObjectBuilder<Scoping> scopingBuilder = (SAMLObjectBuilder<Scoping>) builderFactory.getBuilder(Scoping.DEFAULT_ELEMENT_NAME);
		Scoping scoping = scopingBuilder.buildObject();
		scoping.setIDPList(idpList);
		scoping.setProxyCount(new Integer(DEFAULT_PROXY_COUNT));

		authnRequest.setScoping(scoping);

		return authnRequest;
	}

	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
	/**
	 * Convert a byte array to a hex string
	 * Code from https://stackoverflow.com/questions/9655181/how-to-convert-a-byte-array-to-a-hex-string-in-java/9855338#9855338
	 *
	 * @param bytes
	 * @return
	 */
	protected static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

	/**
	 * Generates random ID to be used as Request/Response ID.
	 * @return random ID
	 */
	private String generateRandomID() {
		byte data[] = new byte[23]; // I'm not certain how long this field can be; at least 46 characters though.
		random.nextBytes(data);
		return "_" + bytesToHex(data); // Supposedly cannot start w/ a digit, so ensure that.
	}

	//************************ Handle Authentication ******************************
	@SuppressWarnings("rawtypes")
	public BasicSAMLMessageContext handleAuthenticationResponseFromIdP(HttpServletRequest request) throws SAMLException {

		log.debug("Basic Authentication - HTTP Post");
		BasicSAMLMessageContext<SAMLObject, AuthnRequest, SAMLObject> samlMessageContext = new BasicSAMLMessageContext<SAMLObject, AuthnRequest, SAMLObject>();

		try {
			samlMessageContext.setInboundMessageTransport(new HttpServletRequestAdapter(request));
			samlMessageContext.setLocalEntityRole(SPSSODescriptor.DEFAULT_ELEMENT_NAME);
			samlMessageContext.setMetadataProvider(metadata);
			samlMessageContext.setLocalEntityId(metadata.getSp());
			samlMessageContext.setLocalEntityRoleMetadata(metadata.getRole(metadata.getSp(), SPSSODescriptor.DEFAULT_ELEMENT_NAME,
					SAMLConstants.SAML20P_NS));
			samlMessageContext.setLocalEntityMetadata(metadata.getEntityDescriptor(metadata.getSp()));
			samlMessageContext.setPeerEntityRole(IDPSSODescriptor.DEFAULT_ELEMENT_NAME);
			samlMessageContext.setInboundSAMLProtocol(SAMLConstants.SAML2_POST_BINDING_URI);

			BaseSAML2MessageDecoder decoder = new HTTPPostDecoder(parser);

			decoder.decode(samlMessageContext);

			EntityDescriptor peerEntityDescriptor = samlMessageContext.getPeerEntityMetadata();
			if (peerEntityDescriptor == null){
				throw new SAMLException("Peer Entity Metadata not set in response, unable to validate response sender.");
			}
			samlMessageContext.setPeerEntityId(peerEntityDescriptor.getEntityID());

		} catch (Exception e) {
			log.error("The exception occurred during handling SAML authentication" + e.getMessage());
			throw new SAMLException("The exception occurred during handling SAML authentication" + e.getMessage(), e);
		}

		return samlMessageContext;
	}

	//************************ Verify Response ******************************

	@SuppressWarnings("rawtypes")
	@Override
	public SAMLCredential verifyResponse(BasicSAMLMessageContext context) throws SAMLException, SecurityException, ValidationException,
			DecryptionException {

		AuthnRequest request = null;
		SAMLObject message = context.getInboundSAMLMessage();

		// Verify type
		if (!(message instanceof Response)) {
			log.warn("Received response is not of a Response object type");
			throw new SAMLException("Error validating SAML response");
		}

		Response response = (Response) message;

		// Verify status
		if (!StatusCode.SUCCESS_URI.equals(response.getStatus().getStatusCode().getValue())) {
			log.warn("Received response has invalid status code");
			throw new SAMLException("SAML status is not success code");
		}

		// Verify signature of the response if present
		if (response.getSignature() != null) {
			verifySignature(response.getSignature(), context.getPeerEntityId());
		}

		// Verify issue time
		DateTime time = response.getIssueInstant();
		if (!isDateTimeSkewValid(DEFAULT_RESPONSE_SKEW, time)) {
			log.warn("Response issue time is either too old or with date in the future");
			throw new SAMLException("Error validating SAML response");
		}

		// Verify response to field if present, set request if correct
		if (response.getInResponseTo() != null) {
			RequestAbstractType requestType = messages.get(response.getInResponseTo());
			if (requestType == null) {
				log.warn("InResponseToField doesn't correspond to sent message", response.getInResponseTo());
				throw new SAMLException("Error validating SAML response");
			} else if (requestType instanceof AuthnRequest) {
				request = (AuthnRequest) requestType;
			} else {
				log.warn("Sent request was of different type then received response", response.getInResponseTo());
				throw new SAMLException("Error validating SAML response");
			}
		}

		// Verify destination
		if (response.getDestination() != null) {
			SPSSODescriptor localDescriptor = (SPSSODescriptor) context.getLocalEntityRoleMetadata();

			// Check if destination is correct on this SP
			List<AssertionConsumerService> services = localDescriptor.getAssertionConsumerServices();
			boolean found = false;
			for (AssertionConsumerService service : services) {
				if (response.getDestination().equals(service.getLocation())
						&& context.getInboundSAMLProtocol().equals(service.getBinding())) {
					found = true;
					break;
				}
			}
			if (!found) {
				log.warn("Destination of the response was not the expected value", response.getDestination());
				throw new SAMLException("Error validating SAML response");
			}
		}

		// Verify issuer
		if (response.getIssuer() != null) {
			Issuer issuer = response.getIssuer();
			verifyIssuer(issuer, context);
		}

		Assertion subjectAssertion = null;
		List<Attribute> attributes = new LinkedList<Attribute>();

		// Verify assertions
		List<Assertion> assertionList = response.getAssertions();

		List<EncryptedAssertion> encryptedAssertionList = response.getEncryptedAssertions();
		for (EncryptedAssertion ea : encryptedAssertionList) {
			if (decrypter == null) {
				log.warn("Can't decrypt Assertion, no decrypter is set in the context");
				throw new SAMLException("SAML Assertion is invalid");
			}
			assertionList.add(decrypter.decrypt(ea));
		}

		for (Assertion assertion : assertionList) {

			verifyAssertion(assertion, request, context);
			if (assertion.getAuthnStatements().size() > 0) {
				if (assertion.getSubject() != null && assertion.getSubject().getSubjectConfirmations() != null) {
					for (SubjectConfirmation conf : assertion.getSubject().getSubjectConfirmations()) {
						if (BEARER_CONFIRMATION.equals(conf.getMethod())) {
							subjectAssertion = assertion;
						}
					}
				}
			}

			// Process all attributes
			for (AttributeStatement attStatement : assertion.getAttributeStatements()) {
				log.debug(attStatement.toString());
				for (Attribute att : attStatement.getAttributes()) {
					attributes.add(att);
				}
				for (EncryptedAttribute att : attStatement.getEncryptedAttributes()) {
					if (decrypter == null) {
						log.warn("Can't decrypt Attribute, no decrypter is set in the context");
						throw new SAMLException("SAML Assertion is invalid");
					}
					attributes.add(decrypter.decrypt(att));
				}
			}
		}

		// Make sure that at least one assertion contains authentication statement and subject with bearer cofirmation
		if (subjectAssertion == null) {
			log.warn("Response doesn't contain authentication statement");
			throw new SAMLException("Error validating SAML response");
		}

		AuthnStatement authStmt = subjectAssertion.getAuthnStatements().get(0);

		String userName = null;
		String fullName = null;
		Set<String> roles = new HashSet<String>();
		for (Attribute attr : attributes) {
			log.debug(attr.getName());
			if ("uid".equalsIgnoreCase(attr.getName())) {
				userName = attr.getAttributeValues().get(0).getDOM().getTextContent();
			}
			if ("fullName".equalsIgnoreCase(attr.getName())) {
				fullName = attr.getAttributeValues().get(0).getDOM().getTextContent();
			}

			if ("memberOf".equalsIgnoreCase(attr.getName())) {
				List<XMLObject> vals = attr.getAttributeValues();
				for (XMLObject xmlObject : vals) {
					roles.addAll(parseRoleString(xmlObject.getDOM().getTextContent()));
				}
			}

		}
		if (userName == null){
			log.warn("Failed to find valid UserName from SAML Response");
			throw new SAMLException("Error failure to find valid UserName from SAML Response");
		}

		return new SAMLCredential(subjectAssertion.getSubject().getNameID(), subjectAssertion, context.getPeerEntityMetadata()
				.getEntityID(), authStmt.getSessionIndex(), userName, fullName, new ArrayList<String>(roles));
	}

	/**
	 * memberOf is in this format
	 * 	 ||roleName|||||||stateId|||||||||  	  stateUser
	 * 	 ||roleName|||||||stateId|||distId||||||  districtUser
	 *
	 */
	private List<String> parseRoleString(String memberOf) {
		List<String> subRoles = new ArrayList<String>();
		String[] roleArray = memberOf.split("\\|");
		subRoles.add(roleArray[2]);
  	    if (roleArray.length == 9 || roleArray[9].isEmpty()) {
  	        subRoles.add(roleArray[8]);
		} else {
  	      	subRoles.add(roleArray[9]);
	    }
		if (roleArray.length >= 12) {
			subRoles.add(roleArray[9] + "_" + roleArray[12]);
		}

		return subRoles;
	}

	@SuppressWarnings("rawtypes")
	private void verifyAssertion(Assertion assertion, AuthnRequest request, BasicSAMLMessageContext context)
			throws AuthenticationException, SAMLException, SecurityException, ValidationException, DecryptionException {
		// Verify assertion time skew
		if (!isDateTimeSkewValid(DEFAULT_RESPONSE_SKEW, assertion.getIssueInstant())) {
			log.warn("Authentication statement is too old to be used", assertion.getIssueInstant());
			throw new CredentialsExpiredException("Users authentication credential is too old to be used");
		}

		// Verify validity of assertion
		verifyIssuer(assertion.getIssuer(), context);
		verifyAssertionSignature(assertion.getSignature(), context);
		verifySubject(assertion.getSubject(), request, context);

		// Assertion with authentication statement must contain audience restriction
		if (assertion.getAuthnStatements().size() > 0) {
			verifyAssertionConditions(assertion.getConditions(), context, true);
			for (AuthnStatement statement : assertion.getAuthnStatements()) {
				verifyAuthenticationStatement(statement, context);
			}
		} else {
			verifyAssertionConditions(assertion.getConditions(), context, false);
		}
	}

	/**
	 * Verifies validity of Subject element, only bearer confirmation is validated.
	 * @param subject subject to validate
	 * @param request request
	 * @param context context
	 * @throws SAMLException error validating the object
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void verifySubject(Subject subject, AuthnRequest request, BasicSAMLMessageContext context) throws SAMLException,
			DecryptionException {
		boolean confirmed = false;

		for (SubjectConfirmation confirmation : subject.getSubjectConfirmations()) {
			if (BEARER_CONFIRMATION.equals(confirmation.getMethod())) {

				SubjectConfirmationData data = confirmation.getSubjectConfirmationData();

				if (data == null) {
					log.warn("Subject Confirmation Data of a Bearer Subject Confirmation is null");
					throw new SAMLException("SAML Assertion is invalid");
				}

				// It must not contain a NotBefore timestamp
				if (data.getNotBefore() != null) {
					log.warn("Assertion contains not before in bearer confirmation, which is forbidden");
					throw new SAMLException("SAML Assertion is invalid");
				}

				// Validate not on or after
				if (data.getNotOnOrAfter() == null || data.getNotOnOrAfter().isBeforeNow()) {
					log.warn("Subject Conf Data does not contain NotOnOrAfter or it has expired");
					throw new SAMLException("SAML Assertion is invalid");
				}

				// InResponseTo must match the AuthnRequest request id
				if (request != null) {
					if (data.getInResponseTo() == null) {
						log.warn("Assertion invalidated by subject confirmation - missing inResponseTo field");
						throw new SAMLException("SAML Assertion is invalid");
					} else {
						if (!data.getInResponseTo().equals(request.getID())) {
							log.warn("Assertion invalidated by subject confirmation - invalid in response to");
							throw new SAMLException("SAML Assertion is invalid");
						}
					}
				}

				// Validate recipient
				if (data.getRecipient() == null) {
					log.warn("Assertion invalidated by subject confirmation - recipient is missing in bearer confirmation");
					throw new SAMLException("SAML Assertion is invalid");
				} else {

					SPSSODescriptor spssoDescriptor = (SPSSODescriptor) context.getLocalEntityRoleMetadata();
					for (AssertionConsumerService service : spssoDescriptor.getAssertionConsumerServices()) {
						if (context.getInboundSAMLProtocol().equals(service.getBinding())
								&& service.getLocation().equals(data.getRecipient())) {
							confirmed = true;
						}
					}
				}
			}

			// The subject is confirmed by this confirmation data. Let's store the subject in context.
			if (confirmed) {
				NameID nameID;
				if (subject.getEncryptedID() != null) {
					if (decrypter == null) {
						log.warn("Can't decrypt NameID, no decrypter is set in the context");
						throw new SAMLException("SAML Assertion is invalid");
					}
					nameID = (NameID) decrypter.decrypt(subject.getEncryptedID());
				} else {
					nameID = subject.getNameID();
				}
				context.setSubjectNameIdentifier(nameID);
				return;
			}
		}
		log.warn("Assertion invalidated by subject confirmation - can't be confirmed by bearer method");
		throw new SAMLException("SAML Assertion is invalid");
	}

	/**
	 * Verify signature of response.
	 * @param signature     signature to verify
	 * @param IDPEntityID   Entity ID of the IDP which issued the assertion.
	 * @throws SecurityException
	 * @throws ValidationException
	 */
	private void verifySignature(Signature signature, String IDPEntityID) throws SecurityException, ValidationException {
		SAMLSignatureProfileValidator validator = new SAMLSignatureProfileValidator();
		validator.validate(signature);
		CriteriaSet criteriaSet = new CriteriaSet();
		criteriaSet.add(new EntityIDCriteria(IDPEntityID));
		criteriaSet.add(new MetadataCriteria(IDPSSODescriptor.DEFAULT_ELEMENT_NAME, SAMLConstants.SAML20P_NS));
		criteriaSet.add(new UsageCriteria(UsageType.SIGNING));
		log.debug("Verifying signature", signature);
		trustEngine.validate(signature, criteriaSet);
	}

	/**
	 * Verifies signature of the assertion. In case signature is not present and SP required signatures in metadata
	 * the exception is thrown.
	 * @param signature signature to verify
	 * @param context context
	 * @throws SAMLException signature missing although required
	 * @throws SecurityException signature can't be validated
	 * @throws ValidationException signature is malformed
	 */
	@SuppressWarnings("rawtypes")
	private void verifyAssertionSignature(Signature signature, BasicSAMLMessageContext context) throws SAMLException, SecurityException,
			ValidationException {
		SPSSODescriptor roleMetadata = (SPSSODescriptor) context.getLocalEntityRoleMetadata();
		boolean wantSigned = roleMetadata.getWantAssertionsSigned();
		if (signature != null && wantSigned) {
			verifySignature(signature, context.getPeerEntityMetadata().getEntityID());
		} else if (wantSigned) {
			log.warn("Assertion must be signed, but is not");
			throw new SAMLException("SAML Assertion is invalid");
		}
	}

	/**
	 * Verifies issuer of the assertion.
	 * @param issuer           issuer to verify.
	 * @param context          context.
	 * @throws SAMLException   error SAML Assertion is invalid.
	 */
	@SuppressWarnings("rawtypes")
	private void verifyIssuer(Issuer issuer, BasicSAMLMessageContext context) throws SAMLException {
		// Validate format of issuer
		if (issuer.getFormat() != null && !issuer.getFormat().equals(NameIDType.ENTITY)) {
			log.warn("Issuer format is not null and does not equal: " + NameIDType.ENTITY);
			throw new SAMLException("SAML Assertion is invalid");
		}

		// Issuer value must match (be contained in) Issuer IDP
		String issuerIDP = context.getPeerEntityMetadata().getEntityID();
		if (!issuerIDP.equals(issuer.getValue())) {
			log.warn("Issuer value: " + issuer.getValue() + " does not match issuer IDP: " + issuerIDP
					+ "Assertion invalidated by unexpected issuer value", issuer.getValue());
			throw new SAMLException("SAML Assertion is invalid");
		}
	}

	/**
	 * Verify assertion conditions if their exist.
	 * @param conditions        assertion conditions.
	 * @param context           context.
	 * @param audienceRequired  is audience required.
	 * @throws SAMLException    error SAML Assertion is invalid.
	 */
	@SuppressWarnings("rawtypes")
	private void verifyAssertionConditions(Conditions conditions, BasicSAMLMessageContext context, boolean audienceRequired)
			throws SAMLException {
		// If no conditions are implied, assertion is deemed valid
		if (conditions == null) {
			return;
		}

		if (conditions.getNotBefore() != null) {
			if (conditions.getNotBefore().isAfterNow()) {
				log.warn("Assertion is not yet valid, invalidated by condition notBefore", conditions.getNotBefore());
				throw new SAMLException("SAML response is not valid");
			}
		}
		if (conditions.getNotOnOrAfter() != null) {
			if (conditions.getNotOnOrAfter().isBeforeNow()) {
				log.warn("Assertion is no longer valid, invalidated by condition notOnOrAfter", conditions.getNotOnOrAfter());
				throw new SAMLException("SAML response is not valid");
			}
		}

		if (audienceRequired && conditions.getAudienceRestrictions().size() == 0) {
			log.warn("Assertion invalidated by missing audience restriction");
			throw new SAMLException("SAML response is not valid");
		}

		for (AudienceRestriction audienceRestriction : conditions.getAudienceRestrictions()) {
			if (audienceRestriction.getAudiences().size() == 0) {
				log.warn("No audit audience specified for the assertion");
				throw new SAMLException("SAML response is invalid");
			}
			boolean validAudienceUrl = false;
			for (Audience audience : audienceRestriction.getAudiences()) {
				if (context.getLocalEntityId().equals(audience.getAudienceURI())) {
					validAudienceUrl = true;
				}
			}
			if (!validAudienceUrl) {
				log.warn("Our entity is not the intended audience of the assertion");
				throw new SAMLException("SAML response is not intended for this entity");
			}
		}
	}

	/**
	 * Validate authentication statement.
	 * @param authnStatement    statement to validate.
	 * @param context           context.
	 * @throws AuthenticationException
	 */
	@SuppressWarnings("rawtypes")
	private void verifyAuthenticationStatement(AuthnStatement authnStatement, BasicSAMLMessageContext context)
			throws AuthenticationException {

		if (authnStatement.getSubjectLocality() != null) {
			HTTPInTransport httpInTransport = (HTTPInTransport) context.getInboundMessageTransport();
			if (authnStatement.getSubjectLocality().getAddress() != null) {
				if (!httpInTransport.getPeerAddress().equals(authnStatement.getSubjectLocality().getAddress())) {
					throw new BadCredentialsException("User is accessing the service from invalid address");
				}
			}
		}
	}

	/**
	 * Validate date time.
	 * @param skewInMilliSec   skew of time in seconds.
	 * @param time        time to validate.
	 * @return            boolean, true if time is valid.
	 */
	private boolean isDateTimeSkewValid(int skewInMilliSec, DateTime time) {
		long current = new Date().getTime();
		log.debug("Time Check: \nskew;current;time\t" + skewInMilliSec + "\t" + current + "\t" + time.getMillis() );

		return time.isAfter(current - skewInMilliSec) && time.isBefore(current + skewInMilliSec);
	}

}
