package com.secureops.sso.saml;

import com.secureops.sso.saml.consumer.SamlConsumer;
import org.opensaml.common.SAMLException;
import org.springframework.security.AuthenticationException;
import org.springframework.security.ui.AuthenticationEntryPoint;
import org.springframework.security.ui.FilterChainOrder;
import org.springframework.security.ui.SpringSecurityFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Handles SAML Entry Point on Service Provider side.
 * <p>
 * There are two ways the entry point can get invoked. Either user accesses a URL configured to require
 * some degree of authentication and throws AuthenticationException which is handled and invokes the entry point.
 * <p>
 * The other way is direct invocation of the entry point by accessing the DEFAULT_FILTER_URL. In this way user
 * can be forwarded to IDP after clicking for example login button.
 *
 */
public class SAMLEntryPoint extends SpringSecurityFilter implements AuthenticationEntryPoint {


    private SamlConsumer consumer;

    /**
     * Default name of path suffix which will invoke this filter.
     */
    private static final String DEFAUL_FILTER_URL = "/j_spring_saml_security_check";

    /**
     * User configured path which overrides the default value.
     */
    private String filterProcessesUrl;

    /**
     * The filter will be used in case the URL of the request ends with DEFAULT_FILTER_URL.
     *
     * @param request request used to determine whether to enable this filter
     * @return true if this filter should be used
     */
    protected boolean processFilter(HttpServletRequest request) {
        if (filterProcessesUrl != null) {
            return (request.getRequestURI().endsWith(filterProcessesUrl));
        } else {
            return (request.getRequestURI().endsWith(DEFAUL_FILTER_URL));
        }
    }

    /**
     * In case the DEFAULT_FILTER_URL is invoked directly, the filter will get called and initialize the
     * login sequence.
     */
    protected void doFilterHttp(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (processFilter(request)) {
            commence(request, response, null);
        } else {
            chain.doFilter(request, response);
        }
    }

    /**
     * Send request to IDP using any binding supported by both SP and IDP.
     *
     * @param servletRequest  request
     * @param servletResponse response
     * @param e               exception causing this entry point to be invoked
     * @throws javax.servlet.ServletException error initializing SAML protocol
     */
    public void commence(ServletRequest servletRequest, ServletResponse servletResponse, AuthenticationException e) throws ServletException {
        try {
            consumer.handleLogin((HttpServletRequest) servletRequest, (HttpServletResponse) servletResponse);

        } catch (SAMLException e1) {
            throw new ServletException("Error sending assertion", e1);
        }
    }

    public String getFilterProcessesUrl() {
        return filterProcessesUrl;
    }

    public void setFilterProcessesUrl(String filterProcessesUrl) {
        this.filterProcessesUrl = filterProcessesUrl;
    }

    public int getOrder() {
        return FilterChainOrder.PRE_AUTH_FILTER + 10;
    }

    public void setConsumer(SamlConsumer consumer) {
        this.consumer = consumer;
    }
}
