package com.secureops.sso.saml;

import com.secureops.sso.saml.consumer.SamlConsumer;

import org.opensaml.common.SAMLException;
import org.opensaml.common.binding.BasicSAMLMessageContext;
import org.springframework.security.Authentication;
import org.springframework.security.AuthenticationException;
import org.springframework.security.AuthenticationServiceException;
import org.springframework.security.ui.AbstractProcessingFilter;
import org.springframework.security.ui.FilterChainOrder;

import javax.servlet.http.HttpServletRequest;

/**
 * Filter processes arriving SAML messages by delegating to the SamlConsumer. After the SAMLAuthenticationToken
 * is obtained, authentication providers are asked to authenticate it.
 *
 */
public class SAMLProcessingFilter extends AbstractProcessingFilter {

    /**
     * Profile to delegate SAML parsing to
     */
    private SamlConsumer consumer;

    private static final String DEFAUL_URL = "/saml/sso";

    /**
     * In case the login attribute is not present it is presumed that the call is made from the remote IDP
     * and contains a SAML assertion which is processed and authenticated.
     *
     * @param request request
     * @return authentication object in case SAML data was found and valid
     * @throws org.springframework.security.AuthenticationException authentication failture
     */
    @SuppressWarnings("rawtypes")
	public Authentication attemptAuthentication(HttpServletRequest request) throws AuthenticationException {
        try {
            BasicSAMLMessageContext samlMessageContext = consumer.handleAuthenticationResponseFromIdP(request);
            SAMLAuthenticationToken token = new SAMLAuthenticationToken(samlMessageContext);
            return getAuthenticationManager().authenticate(token);
        } catch (SAMLException e) {
            throw new AuthenticationServiceException("Incoming SAML message is invalid", e);
        }
    }

    @Override
    public String getDefaultFilterProcessesUrl() {
        return DEFAUL_URL;
    }


    public int getOrder() {
        return FilterChainOrder.AUTHENTICATION_PROCESSING_FILTER + 10;
    }

    public void setConsumer(SamlConsumer consumer) {
        this.consumer = consumer;
    }
}
