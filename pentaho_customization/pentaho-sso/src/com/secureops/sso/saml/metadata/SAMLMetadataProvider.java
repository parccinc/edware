package com.secureops.sso.saml.metadata;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opensaml.saml2.metadata.provider.ChainingMetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;

/**
 * Class offering extra services on top of underlying chaining MetadataProviders. Manager keeps track of all available
 * identity and service providers configured inside the chained metadata providers. Exactly one service provider can
 * be determined as hosted.
 *
 */
public class SAMLMetadataProvider extends ChainingMetadataProvider {

    private String sp;
    private Map<String,String> idpMap;
    private static final Log logger = LogFactory.getLog(SAMLMetadataProvider.class);


    public SAMLMetadataProvider(List<MetadataProvider> providers, Map<String, String> idpMap, String sp) throws MetadataProviderException {
        super();
        logger.debug("Initialize SAMLMetadataProvider");
        setProviders(providers);
        this.sp = sp;
        this.idpMap = idpMap;
    }

    public String getSp() {
        return sp;
    }

    public Map<String, String> getIdpMap() {
        return idpMap;
    }
}
