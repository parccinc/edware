package com.secureops.sso.saml;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opensaml.common.SAMLException;
import org.opensaml.common.binding.BasicSAMLMessageContext;
import org.opensaml.xml.encryption.DecryptionException;
import org.opensaml.xml.validation.ValidationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.Authentication;
import org.springframework.security.AuthenticationException;
import org.springframework.security.AuthenticationServiceException;
import org.springframework.security.providers.AuthenticationProvider;
import org.springframework.security.providers.UsernamePasswordAuthenticationToken;
import org.springframework.security.userdetails.UserDetails;
import org.springframework.security.userdetails.UserDetailsService;
import org.springframework.util.Assert;

import com.parcc.edware.analytics.constants.PARCCStates;
import com.secureops.sso.SsoUserDetailsService;
import com.secureops.sso.saml.consumer.SamlConsumer;

/**
 * Authentication provider is capable of verifying validity of a SAMLAuthenticationToken and in case
 * the token is valid to create an authenticated UsernamePasswordAuthenticationToken.
 *
 */
public class SAMLAuthenticationProvider implements AuthenticationProvider, InitializingBean {
    private static final Log logger = LogFactory.getLog(SAMLAuthenticationProvider.class);

    private static final String ANALYTICS_ROLE = "PII_ANALYTICS";

    private SamlConsumer consumer;
    private UserDetailsService userDetailsService;

    @Autowired
    protected PARCCStates parccStates;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(this.userDetailsService, "The userDetailsService must be set");
    }

    /**
     * Performs authentication of an Authentication object created from OpenAM SAML Response
     * <br>
     * The authentication must be of type SAMLAuthenticationToken and must contain filled BasicSAMLMessageContext.
     * <br>
     * If the SAML in-bound message in the context is valid, UsernamePasswordAuthenticationToken with name given in the SAML
     * message NameID and assertion used to verify the user as credential are created and set as authenticated.
     * <br>
     * @param authentication SAMLAuthenticationToken to verify
     * @return UsernamePasswordAuthenticationToken with name as NameID value and SAMLCredential as credential object
     * @throws org.springframework.security.AuthenticationException user can't be authenticated due to an error
     */
    @Override
    @SuppressWarnings("rawtypes")
    public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
        final SAMLAuthenticationToken token = (SAMLAuthenticationToken) authentication;
        final BasicSAMLMessageContext context = token.getCredentials();
        SAMLCredential samlCredential;

        try {
            samlCredential = this.consumer.verifyResponse(context);
        } catch (final SAMLException e) {
            throw new AuthenticationServiceException("Error validating SAML message", e);
        } catch (final ValidationException e) {
            logger.error("Error validating signature", e);
            throw new AuthenticationServiceException("Error validating SAML message signature", e);
        } catch (final org.opensaml.xml.security.SecurityException e) {
            logger.error("Error validating signature", e);
            throw new AuthenticationServiceException("Error validating SAML message signature", e);
        } catch (final DecryptionException e) {
            logger.error("Error decrypting SAML message", e);
            throw new AuthenticationServiceException("Error decrypt SAML message", e);
        }

        if (this.getUserDetailsService() == null) {
            throw new AuthenticationServiceException("Failure to find userDetailsService");
        }

        if (!this.checkRoles(samlCredential.getRoles())){
            final String msg = samlCredential.getUserName() + " Does not have required roles " + samlCredential.getRoles();
            logger.error(msg);
            throw new AuthenticationServiceException(msg);
        }

        // this is the point where we build a user from SSO, including new roles
        try {
            final SsoUserDetailsService ssoUser = (SsoUserDetailsService) this.getUserDetailsService();
            ssoUser.checkUser(samlCredential);
        } catch (final Exception ex) {
            ex.printStackTrace();
        }
        // this is the basic login mechanism
        final UserDetails user = this.getUserDetailsService().loadUserByUsername(samlCredential.getUserName());
        final UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(user, samlCredential, user.getAuthorities());

        final String msg = "Successful Login to Pentaho for user: " + samlCredential.getUserName() + " , with roles: " + samlCredential.getRoles();
        logger.info(msg);

        return result;
    }

    private Boolean checkRoles (final List<String> samlRoles){
        Boolean hasValidState = false;
        Boolean hasRequiredRole = false;

        for (final String role : samlRoles) {
            if (this.parccStates.isValid(role)) {
                hasValidState = true;
            }
            if (SAMLAuthenticationProvider.ANALYTICS_ROLE.equals(role)){
                hasRequiredRole = true;
            }
        }

        return hasValidState && hasRequiredRole;
    }

    /**
     * Returns saml user details service used to load information about logged user from SAML data.
     * @return service or null if not set
     */
    public UserDetailsService getUserDetailsService() {
        return this.userDetailsService;
    }

    /**
     * The user details can be optionally set and is automatically called while user SAML assertion
     * is validated.
     * @param userDetailsService user details
     */
    public void setUserDetailsService(final UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    /**
     * SAMLAuthenticationToken is the only supported token.
     * @param aClass class to check for support
     * @return true if class is of type SAMLAuthenticationToken
     */
    @Override
    @SuppressWarnings("rawtypes")
    public boolean supports(final Class aClass) {
        return SAMLAuthenticationToken.class.isAssignableFrom(aClass);
    }

    public void setConsumer(final SamlConsumer consumer) {
        this.consumer = consumer;
    }
}
