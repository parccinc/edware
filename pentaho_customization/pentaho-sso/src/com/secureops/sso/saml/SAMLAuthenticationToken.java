package com.secureops.sso.saml;

import org.opensaml.common.binding.BasicSAMLMessageContext;
import org.springframework.security.providers.AbstractAuthenticationToken;

/**
 * SAML Token is used to pass SAMLContext object through to the SAML Authentication provider.
 *
 */
public class SAMLAuthenticationToken extends AbstractAuthenticationToken {

    private static final long serialVersionUID = 1L;
    /**
     * SAML context with content to verify
     */
    @SuppressWarnings("rawtypes")
	private BasicSAMLMessageContext credentials;

    /**
     * Default constructor initializing the context
     * @param credentials SAML context object created after decoding
     */
    @SuppressWarnings("rawtypes")
    public SAMLAuthenticationToken(BasicSAMLMessageContext credentials) {
        super(null);
        this.credentials = credentials;
        setAuthenticated(false);
    }

    /**
     * Returns the stored SAML context
     * @return context
     */
    @SuppressWarnings("rawtypes")
    public BasicSAMLMessageContext getCredentials() {
        return this.credentials;
    }

    /**
     * Always null
     * @return null
     */
    public Object getPrincipal() {
        return null;
    }

    /**
     * This object can never be authenticated, call with true result in exception.
     * @param isAuthenticated only false value allowed
     * @throws IllegalArgumentException if isAuthenticated is true
     */
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        if (isAuthenticated) {
            throw new IllegalArgumentException(
                "Cannot set this token to trusted - use constructor containing GrantedAuthority[]s instead");
        }
        super.setAuthenticated(false);
    }

}
