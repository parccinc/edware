package com.secureops.sso.saml;

import java.util.List;

import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.NameID;

/**
 * Object is a storage for entities parsed from SAML2 response during it's authentication. The object is stored
 * as credential object inside the Authentication returned after authentication success.
 *
 */
public class SAMLCredential {

    private final NameID nameID;
    private final Assertion authenticationAssertion;
    private final String IDPEntityID;
    private final String sessionIndex;
	private final String userName;
	private final String fullName; 
    private final List<String> roles;

    /**
     * Created unmodifiable SAML credential object.
     * @param nameID name ID of the authenticated entity
     * @param authenticationAssertion assertion used to validate the entity
     * @param IDPEntityID identifier of IDP where the assertion came from
     */
    public SAMLCredential(NameID nameID, Assertion authenticationAssertion, String IDPEntityID, String sessionIndex, String userName, String fullName, List<String> roles) {
        this.nameID = nameID;
        this.authenticationAssertion = authenticationAssertion;
        this.IDPEntityID = IDPEntityID;
        this.sessionIndex = sessionIndex;
        this.userName = userName;
        this.fullName = fullName;
        this.roles = roles;
    }

    /**
     * NameID returned from IDP as part of the authentication process.
     * @return name id
     */
    public NameID getNameID() {
        return nameID;
    }

    /**
     * Assertion issued by IDP as part of the authentication process.
     * @return assertion
     */
    public Assertion getAuthenticationAssertion() {
        return authenticationAssertion;
    }

    /**
     * Entity ID of the IDP which issued the assertion.
     * @return IDP entity ID
     */
    public String getIDPEntityID() {
        return IDPEntityID;
    }

    public String getSessionIndex() {
		return sessionIndex;
	}

	public String getUserName() {
		return userName;
	}

	public List<String> getRoles() {
		return roles;
	}

	public String getFullName() {
		return fullName;
	}

}
