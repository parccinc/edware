package com.secureops.sso;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.pentaho.platform.api.engine.security.userroledao.IPentahoRole;
import org.pentaho.platform.api.engine.security.userroledao.IPentahoUser;
import org.pentaho.platform.api.engine.security.userroledao.IUserRoleDao;
import org.pentaho.platform.api.engine.security.userroledao.UncategorizedUserRoleDaoException;
import org.pentaho.platform.engine.core.system.PentahoSessionHolder;
import org.pentaho.platform.engine.core.system.PentahoSystem;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.dao.DataAccessException;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.userdetails.UserDetails;
import org.springframework.security.userdetails.UserDetailsService;
import org.springframework.security.userdetails.UsernameNotFoundException;

import com.secureops.sso.saml.SAMLCredential;

/**
 * Implements UserDetailsService for SSO authentication mechanism.
 * The approach is to wrap intended PentahoUserDetailsService adding user creation functionality for users signing-up
 * through integrated Identity Provider Service.
 * <p>
 * Normally, Pentaho Security denys user creation if authorized session does not belong to Admin user. To enable
 * "auto registration" it is required to reduce security here. So, UserRoleDao security interception should allow
 * <code>createUser, setPassword, setUserDescription</code> methods invocation.
 * See "SSO authentication" integration guide for details.
 */
public class SsoUserDetailsService implements UserDetailsService, InitializingBean {
    private static final Log logger = LogFactory.getLog(SsoUserDetailsService.class);

    private final UserDetailsService pentahoUserDetailsService;
    private IUserRoleDao userRoleDao;

    public SsoUserDetailsService(final UserDetailsService pentahoUserDetailsService) {
        this.pentahoUserDetailsService = pentahoUserDetailsService;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        // do nothing
    }

    /**
     * {@inheritDoc}
     *
     * Creates new user if there is no one available in the repository by given user name.
     * This should be done by checkUser method now.
     *
     * Should be used only for SSO authentication.
     */
    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException, DataAccessException {
        return this.pentahoUserDetailsService.loadUserByUsername(username);
    }

    /**
     * SAMLCredential contains the info we need about a user as returned in the
     * SAML Authentication. This step makes sure the user is in Pentaho with appropriate
     * privileges.
     *
     * @param samlCredential
     */
    public void checkUser(final SAMLCredential samlCredential) {
        if (this.userRoleDao == null) {
            this.userRoleDao = PentahoSystem.get(IUserRoleDao.class, "userRoleDaoTxn", PentahoSessionHolder.getSession());
        }

        samlCredential.getRoles().add("Power User");

        try {
            this.testRolesBuildAsNeeded(samlCredential.getRoles().toArray(new String[0]));
        } catch (final Exception ex) {
            logger.error("Failure to create roles for user: " + samlCredential.getUserName() + " " + samlCredential.getRoles().toString());
        }

        // Test user exists, if he doesn't create with appropriate roles
        try {
            this.checkUpdateUserRoles(samlCredential);
        } catch (final UsernameNotFoundException e) {
            logger.info("Failure to create user: " + samlCredential.getUserName() + " \n" + e.getMessage());
        }
    }

    private void checkUpdateUserRoles(final SAMLCredential samlCredential) throws UsernameNotFoundException, DataAccessException {
        UserDetails userDetails = null;
        try {
            // if user doesn't exist we bail here and just create user with roles
            userDetails = this.pentahoUserDetailsService.loadUserByUsername(samlCredential.getUserName());
        } catch (final UsernameNotFoundException ex) {
            logger.debug("Create User: " + samlCredential.getUserName() + " with roles: " + samlCredential.getRoles().toString());
            final IPentahoUser user = this.userRoleDao.createUser(null, samlCredential.getUserName(), PasswordGenerator.generate(), "",
                    samlCredential.getRoles().toArray(new String[0]));
            user.setDescription(samlCredential.getFullName());
            return;
        }

        // All of the rest of this covers making sure a users roles match what comes from SAML.
        final GrantedAuthority[] grantedAuthArray = userDetails.getAuthorities();
        final List<String> curRoles = new ArrayList<String>();
        for (final GrantedAuthority ga : grantedAuthArray) {
            curRoles.add(ga.getAuthority());
        }

        // add roles if needed
        this.addSamlRolesToExistingUser(samlCredential, curRoles);

        // delete roles if user has too many roles
        this.deleteCurRoleFromExistingUser(samlCredential, curRoles);
    }

    private void deleteCurRoleFromExistingUser(final SAMLCredential samlCredential, final List<String> curRoles) {
        // figure out what needs to be removed from user
        final List<String> rolesToRemove = new ArrayList<String>();
        for (final String curRole : curRoles) {
            if (!samlCredential.getRoles().contains(curRole)) {
                rolesToRemove.add(curRole);
            }
        }

        // if there are rolesToRemove, just reset users roles
        if (rolesToRemove.size() > 0) {
            this.userRoleDao.setUserRoles(null, samlCredential.getUserName(), samlCredential.getRoles().toArray(new String[0]));
            logger.debug("reset user roles: " + samlCredential.getUserName() + " with roles: " + samlCredential.getRoles().toString());
        }
    }

    private void addSamlRolesToExistingUser(final SAMLCredential samlCredential, final List<String> curRole) {
        // figure out what needs to be added to the user
        final List<String> rolesToAdd = new ArrayList<String>();
        for (final String samlRole : samlCredential.getRoles()) {
            if (!curRole.contains(samlRole)) {
                rolesToAdd.add(samlRole);
            }
        }

        // Need to add all of the
        // All of the roles have been added in previous call
        this.userRoleDao.setUserRoles(null, samlCredential.getUserName(), rolesToAdd.toArray(new String[0]));
        logger.debug("add roles to user: " + samlCredential.getUserName() + " with roles: " + rolesToAdd.toString());
    }

    private void testRolesBuildAsNeeded(final String[] userRoles) {
        // Test that the roles exist
        logger.debug("iterate saml assigned roles, make sure they exist on BISERVER");
        for (final String roleName : userRoles) {
            try {
                final IPentahoRole ipr = this.userRoleDao.getRole(null, roleName);
                if (ipr == null) {
                    throw new UncategorizedUserRoleDaoException("build it");
                }

            } catch (final UncategorizedUserRoleDaoException ex) {
                final String[] members = new String[] {};
                this.userRoleDao.createRole(null, roleName, roleName, members);
            }
        }
    }

    public void setUserRoleDao(final IUserRoleDao userRoleDao) {
        this.userRoleDao = userRoleDao;
    }
}
