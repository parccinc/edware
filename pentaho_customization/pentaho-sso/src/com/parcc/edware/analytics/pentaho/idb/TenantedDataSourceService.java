package com.parcc.edware.analytics.pentaho.idb;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.pentaho.platform.api.data.DBDatasourceServiceException;
import org.pentaho.platform.engine.services.connection.datasource.dbcp.PooledOrJndiDatasourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.context.SecurityContext;
import org.springframework.security.context.SecurityContextHolder;

import com.parcc.edware.analytics.constants.PARCCStates;

/**
 * Based on users state code, as configured in roles, the user connects to the appropriate tenant DB
 *
 * @author Bart Maertens, Innovent Solutions, Inc.
 *
 */
public class TenantedDataSourceService extends PooledOrJndiDatasourceService {

	private static final Log logger = LogFactory.getLog(TenantedDataSourceService.class);

    @Autowired
    protected PARCCStates parccStates;

    @Override
    public DataSource getDataSource(final String dsName) throws DBDatasourceServiceException {
        return super.getDataSource(this.modifyDsNameForTenancy(dsName));
    }

    @Override
    public void clearDataSource(final String dsName) {
        super.clearDataSource(this.modifyDsNameForTenancy(dsName));
    }

    /*
     * Standard dsName should attach to a non-existent DB
     *
     */
    private String modifyDsNameForTenancy(final String baseDsName){
        logger.debug("Original DSName is " + baseDsName);

        final SecurityContext sc = SecurityContextHolder.getContext();
        final GrantedAuthority[] roles = sc.getAuthentication().getAuthorities();

        // State IDs are 2 digit codes
        // Each user will have a StateId even if they are a district admin

        String state_id  = "";
        for (final GrantedAuthority role : roles) {
            String authority = role.getAuthority();
			if (this.parccStates.isValid(authority)) {
                state_id = authority;
            }
        }

        final String dsname = state_id.concat("_").concat(baseDsName);
        logger.debug("New DSName is " + dsname);
        return dsname;
    }
}