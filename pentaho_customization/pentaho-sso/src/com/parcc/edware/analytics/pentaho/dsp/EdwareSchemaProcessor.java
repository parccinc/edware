package com.parcc.edware.analytics.pentaho.dsp;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.pentaho.database.service.DatabaseDialectService;
import org.pentaho.platform.engine.core.system.PentahoSystem;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.context.SecurityContext;
import org.springframework.security.context.SecurityContextHolder;

import com.parcc.edware.analytics.constants.PARCCDistrict;
import com.parcc.edware.analytics.constants.PARCCStates;

import mondrian.i18n.LocalizingDynamicSchemaProcessor;
import mondrian.olap.Util;

public class EdwareSchemaProcessor extends LocalizingDynamicSchemaProcessor {

	static final String STATE_AND_DISTRICT_TOKEN = "/*RESTRICT_ACCESS_TO_STATE_AND_DISTRICT*/";

	private static final Log logger = LogFactory.getLog(DatabaseDialectService.class);

	/*
	 * Because parccStates and parccDistrict are created w/ a class loaded by
	 * Spring's class loader, and this class is loaded w/ Pentaho's, we can't
	 * refer to these objects by their type directly (PARCCStates here will also
	 * refer to PARCCStates loaded by Pentaho's loader). We must use reflection.
	 */
	protected Object parccStates;
	protected Object parccDistrict;

	public EdwareSchemaProcessor() {
		this.parccStates = PentahoSystem.get(PARCCStates.class);
		this.parccDistrict = PentahoSystem.get(PARCCDistrict.class);

		if (this.parccStates == null) {
			throw new RuntimeException("Couldn't get PARCCStates bean from PentahoSystem");
		} else if (this.parccDistrict == null) {
			throw new RuntimeException("Couldn't get PARCCDistrict bean from PentahoSystem");
		}
	}

	/**
	 * Used when initializing for unit testing, when the PentahoSystem isn't
	 * initialized.
	 *
	 * @param parccStates
	 * @param parccDistrict
	 */
	public EdwareSchemaProcessor(final PARCCStates parccStates, final PARCCDistrict parccDistrict) {
		if (parccStates == null) {
			throw new IllegalArgumentException("Didn't provide a PARCCStates bean");
		} else if (parccDistrict == null) {
			throw new IllegalArgumentException("Didn't provide a PARCCDistrict bean");
		}
		this.parccStates = parccStates;
		this.parccDistrict = parccDistrict;
	}

	protected boolean isValidState(String role) {
		try {
			final Method isValid = this.parccStates.getClass().getMethod("isValid", String.class);
			return (boolean) isValid.invoke(this.parccStates, role);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new RuntimeException("couldn't get method isValid from parccStates", e);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException("error calling method isValid from parccStates", e);
		}
	}

	protected boolean isValidDistrict(String role) {
		try {
			final Method isValid = this.parccDistrict.getClass().getMethod("isValid", String.class);
			return (boolean) isValid.invoke(this.parccDistrict, role);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new RuntimeException("couldn't get method isValid from parccDistrict", e);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException("error calling method isValid from parccDistrict", e);
		}
	}

	@Override
	public String filter(final String schemaUrl, final Util.PropertyList connectInfo, final InputStream stream) throws Exception {

		String schema = super.filter(schemaUrl, connectInfo, stream);
		if (schema == null || schema.length() < 0){
			throw new DSPInvalidSchemaException("Unable to get handle to schema in Dynamic Schema Processor");
		}

		final SecurityContext sc = SecurityContextHolder.getContext();
		final GrantedAuthority[] roles = sc.getAuthentication().getAuthorities();

		List<String> quotedStates = new ArrayList<>();
		List<String> quotedDistricts = new ArrayList<>();

		for (GrantedAuthority role : roles) {
			String authority = role.getAuthority();
			if (this.isValidState(authority)) {
				quotedStates.add("'" + authority + "'");
			} else { 
				if (this.isValidDistrict(authority)) {
					quotedDistricts.add("'" + StringUtils.substring(authority, 3) + "'");
				}
			}
		}

		if (logger.isDebugEnabled()) {
			logger.debug("DSP for states: " + StringUtils.join(quotedStates, ","));
			logger.debug("DSP for districts: " + StringUtils.join(quotedDistricts, ","));
		}

		if (quotedStates.isEmpty()) {
			throw new DSPUnauthorizedUserException("Dynamic Schema Processor: User has no valid State");
		} 

		if (quotedDistricts.isEmpty()) {
			schema = StringUtils.replace(schema, 
					STATE_AND_DISTRICT_TOKEN,
					String.format(" where state_id IN (%s)", StringUtils.join(quotedStates, ",")));
		} else {
			schema = StringUtils.replace(schema, 
					STATE_AND_DISTRICT_TOKEN,
					String.format(" where state_id IN (%s) and district_id IN (%s)", StringUtils.join(quotedStates, ","), StringUtils.join(quotedDistricts, ",")));
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Cube schema after DSP:\n" + schema);
		}
		return schema;
	}

}
