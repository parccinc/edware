package com.parcc.edware.analytics.pentaho.dsp;

public class DSPUnauthorizedUserException extends Exception {
	private static final long serialVersionUID = -7927816350628031788L;

	public DSPUnauthorizedUserException(final String message) {
        super(message);
    }

    public DSPUnauthorizedUserException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public DSPUnauthorizedUserException() {
        super();
    }

    public DSPUnauthorizedUserException(final Throwable cause) {
        super(cause);
    }

    public DSPUnauthorizedUserException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
