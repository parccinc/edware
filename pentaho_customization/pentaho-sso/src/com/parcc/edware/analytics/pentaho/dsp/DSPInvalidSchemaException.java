package com.parcc.edware.analytics.pentaho.dsp;

public class DSPInvalidSchemaException extends Exception {
	private static final long serialVersionUID = 6565273202516355766L;

	public DSPInvalidSchemaException() {
        super();
    }

    public DSPInvalidSchemaException(final String message) {
        super(message);
    }

    public DSPInvalidSchemaException(final Throwable cause) {
        super(cause);
    }

    public DSPInvalidSchemaException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public DSPInvalidSchemaException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
