package com.parcc.edware.analytics.constants;

import java.util.Collections;
import java.util.List;

public class PARCCStates {
	
    private final List<String> validStates;

    /**
     * Uses dependency injection to provide list of valid states, Configured in
     * pentahoObjects-spring.xml
     *
     * Uses fall back list in the event it is never initialized correctly.
     *
     * @param validStates
     */
    public PARCCStates(final List<String> validStates) {
        if (validStates == null || validStates.size() < 1) {
            throw new IllegalArgumentException("PARCCStates: must specify at least one valid state.");
        }
        this.validStates = Collections.unmodifiableList(validStates);
    }

    /**
     * WARNING: do not rename this method from "isValid" w/out dealing w/ the
     * reflection code in EdwareSchemaProcessor.
     *
     * @param stateCodeGA
     * @return
     */
    public Boolean isValid(String authority) {
        return authority.length() == 2 && validStates.contains(authority);
    }
}
