package com.parcc.edware.analytics.constants;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

public class PARCCDistrict {
	
    @Autowired
    private PARCCStates parccStates;

    /**
     * Bean initializer
     */
    public PARCCDistrict() {
    }

    /**
     * unit test / other initializer
     *
     * @param parccStates
     */
    public PARCCDistrict(final PARCCStates parccStates) {
        this.parccStates = parccStates;
    }

    /**
     * WARNING: do not rename this method from "isValid" w/out dealing w/ the
     * reflection code in EdwareSchemaProcessor.
     *
     * @param stateCodeGA
     * @return
     */
    public Boolean isValid(String authority) {
        return StringUtils.length(authority) > 3 && parccStates.isValid(StringUtils.substring(authority, 0, 2));
    }

}
