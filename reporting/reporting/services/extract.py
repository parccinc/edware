'''
Created on Nov 1, 2013

@author: ejen
'''
from pyramid.view import view_config
from edapi.logging import audit_event
from edapi.decorators import validate_params
from pyramid.response import Response
from reporting.reports.helpers.constants import Constants
from edextract.tasks.constants import Constants as TaskConstants
import logging
import json
from reporting.extracts.summative import SummativeExtract
from reporting.extracts.psychometric import PsychometricExtract
from reporting.extracts.rif import RifExtract
from reporting.extracts.cds import CDSExtract
from reporting.extracts.base_extract import start_extract
from edapi.exceptions import ForbiddenError
from edapi.httpexceptions import EdApiHTTPForbiddenAccess,\
    EdApiHTTPInternalServerError
from parcc_common.extracts.constants import ExtractConstants


logger = logging.getLogger(__name__)

TENANT_EXTRACT_PARAMS = {
    "type": "object",
    "properties": {
        Constants.STATECODE: {
            "type": "string",
            "required": True,
            "pattern": "^[a-zA-Z]{2}$",
        },
        Constants.EXTRACTTYPE: {
            "type": "string",
            "pattern": "^(%s|%s|%s|%s)$" % (ExtractConstants.CDS,
                                            ExtractConstants.PSYCHOMETRIC,
                                            ExtractConstants.SUMMATIVE_ASMT_RESULT,
                                            ExtractConstants.SUMMATIVE_RIF),
            "required": True
        },
        Constants.YEAR: {
            "type": "integer",
            "required": True,
            "pattern": "^[1-9][0-9]{3}$"
        },
        Constants.GRADECOURSE: {
            "type": "array",
            "items": {
                "type": "string",
                "pattern": "^[a-z0-9A-Z ]{2,30}$"
            },
            "minItems": 1,
            "uniqueItems": True,
            "required": False
        },
        Constants.SUBJECT: {
            "type": "string",
            "pattern": "^subject[1-2]$",
            "required": False
        },
    }
}


@view_config(route_name='extract', request_method='POST')
@validate_params(schema=TENANT_EXTRACT_PARAMS)
@audit_event()
def post_extract_service(context, request):
    '''
    Handles POST request to /services/extract

    :param request:  Pyramid request object
    '''
    params = request.json_body
    extract_type = params[Constants.EXTRACTTYPE]
    extract_cls = {
        ExtractConstants.SUMMATIVE_ASMT_RESULT: SummativeExtract,
        ExtractConstants.PSYCHOMETRIC: PsychometricExtract,
        ExtractConstants.SUMMATIVE_RIF: RifExtract,
        ExtractConstants.CDS: CDSExtract,
    }
    extract = extract_cls[extract_type](**params)
    try:
        reg_id, download_url, web_download_url, request_id = start_extract(extract)
        # Right now, we only have one File, but can support multi
        results = {
            Constants.FILES: [{Constants.DOWNLOAD_URL: web_download_url}],
            TaskConstants.REQUEST_ID: request_id
        }
        return Response(body=json.dumps(results), content_type='application/json')
    except ForbiddenError as e:
        logger.warning("Permission denied extraction error. User doesn't have correct permission.")
        raise EdApiHTTPForbiddenAccess(e.msg)
    except Exception as e:
        logger.warning("Internal server extraction error.")
        raise EdApiHTTPInternalServerError(e.msg)
