'''
Created on May 17, 2013

@author: dip
'''
from pyramid.view import view_config
from edapi.decorators import validate_params
from reporting.pdf.constants import PDF_PARAMS
from reporting.pdf.sync_generator import SyncPDFGenerator
from reporting.pdf.async_generator import AsyncPDFGenerator


@view_config(route_name='pdf', request_method='POST')
@validate_params(schema=PDF_PARAMS.ASYNC)
def async_pdf_service(context, request):
    '''
    This is for backward compitibility and batch PDFs.
    '''
    params = request.validated_params
    return AsyncPDFGenerator(**params).run()


@view_config(route_name='pdf', request_method='GET')
@validate_params(schema=PDF_PARAMS.SYNC)
def sync_pdf_service(context, request):
    '''
    Handles POST request to /services/pdf

    :param request:  Pyramid request object
    '''
    params = request.validated_params
    return SyncPDFGenerator(**params).run()
