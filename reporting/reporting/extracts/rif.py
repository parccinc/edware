from reporting.reports.helpers.constants import Constants as ReportingConstants,\
    RptTable
from reporting.extracts.base_extract import BaseExtract
from parcc_common.security.constants import RolesConstants
from parcc_common.extracts.json_formatter import basic_json_formatter
from parcc_common.extracts.constants import ExtractConstants
from edextract.tasks.constants import ExtractionDataType
from reporting.reports.helpers.metadata import get_subject_by_alias
from datetime import datetime


class RifExtract(BaseExtract):

    def __init__(self, *args, **kargs):
        super().__init__(*args, extract_type=ExtractConstants.SUMMATIVE_RIF, **kargs)

    def get_table_name(self, subject):
        return RptTable.RPT_RIF_MATH if subject == ReportingConstants.SUBJECT1 else RptTable.RPT_RIF_ELA

    def get_formatter(self):
        return basic_json_formatter

    @property
    def permission(self):
        return RolesConstants.RF_EXTRACT

    @property
    def extraction_data_types(self):
        return [ExtractionDataType.QUERY_CSV, ExtractionDataType.QUERY_JSON]

    def get_file_name(self, subject, extension, table_name=None):
        return "ReleasedItemFile_Summative_{subject_name}_{grade_course}_{year}_{timestamp}.{extension}"\
            .format(
                subject_name=get_subject_by_alias(subject),
                grade_course=self.grade_course,
                year=self.year,
                extension=extension,
                timestamp=str(datetime.now().strftime(BaseExtract.TIMESTAMP_FORMAT)))
