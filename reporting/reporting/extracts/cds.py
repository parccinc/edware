from edextract.tasks.constants import ExtractionDataType
from reporting.extracts.base_extract import BaseExtract
from parcc_common.security.constants import RolesConstants
from parcc_common.extracts.json_formatter import basic_json_formatter
from parcc_common.extracts.constants import ExtractConstants
from edcore.database.cds_connector import CdsDBConnection
from reporting.extracts.format import get_column_mapping
from reporting.security.context import select_with_context
from sqlalchemy.sql.expression import and_
from edcore.utils.utils import compile_query_to_sql_text
from reporting.reports.helpers.report_table_map import get_table_name, SUBJECT_TO_RPT_TABLE
from reporting.reports.helpers.constants import Constants
from reporting.reports.helpers.metadata import get_subject_by_alias
from datetime import datetime


class CDSExtract(BaseExtract):
    '''
    Psychometric Extracts has a permission, but we don't actually append where clauses to query
    as the table is not state/district/school info
    '''
    def __init__(self, *args, **kargs):
        super().__init__(*args, extract_type=ExtractConstants.CDS, **kargs)

    @property
    def extraction_data_types(self):
        return [ExtractionDataType.QUERY_CDS]

    def get_table_name(self, subject):
        return get_table_name(subject, with_item_score=True)

    @property
    def subjects(self):
        return [Constants.SUBJECT1, Constants.SUBJECT2]

    @property
    def permission(self):
        return RolesConstants.CDS_EXTRACT

    def get_formatter(self):
        return basic_json_formatter

    def get_csv_extract_query(self, subject):
        with CdsDBConnection(tenant=self.tenant) as conn:
            for table_name in self.get_table_name(subject):
                table = conn.get_table(table_name)
                select_columns = self._get_select_columns(table)
                query = select_with_context(select_columns,
                                            from_obj=[table],
                                            permission=self.permission,
                                            state_code=self.state_code)
                query = query.where(and_(table.c.year == self.year))
                yield compile_query_to_sql_text(query), table_name

    def _get_select_columns(self, table):
        table_name = table.name
        schemas_column_mapping = get_column_mapping(table_name)
        column_mapping = self._get_column_mapping_by_year(schemas_column_mapping)
        select_columns = []
        for col in table.columns:
            if col.key in column_mapping:
                select_columns.append(col.label(column_mapping[col.key]))
        return select_columns

    def get_file_name(self, subject, extension, table_name=None):
        if table_name is None:
            extract_type = None
        elif table_name in SUBJECT_TO_RPT_TABLE.values():
            extract_type = 'SUMMATIVE'
        else:
            extract_type = 'ITEM'
        file_name = 'CDSFile'
        if extract_type:
            file_name += '_%s' % extract_type
        if subject:
            file_name += '_%s' % get_subject_by_alias(subject)
        return file_name + "_{year}_{timestamp}.{extension}".format(
            extension=extension,
            year=self.year,
            timestamp=str(datetime.now().strftime(BaseExtract.TIMESTAMP_FORMAT)))
