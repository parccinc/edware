from reporting.extracts.format import setup_input_file_format


# Parse input file format to populate dict for column mapping to star schema
setup_input_file_format()
