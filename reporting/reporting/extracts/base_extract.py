'''
Created on Jun 7, 2015

@author: dip
'''
import tempfile

from abc import abstractmethod
from datetime import datetime
from os import path
from pyramid.threadlocal import get_current_registry, get_current_request
from pyramid.security import authenticated_userid
from sqlalchemy.sql.expression import and_, or_
from uuid import uuid4

from edapi.exceptions import ForbiddenError
from edcore.database.edcore_connector import EdCoreDBConnection
from edcore.security.tenant import get_tenant_by_state_code
from edcore.utils.utils import format_asmt_year, compile_query_to_sql_text

from edextract.tasks.constants import Constants as TaskConstants, ExtractionDataType, QueryType
from edextract.exceptions import ExtractionError

from hpz_client.frs.file_registration import register_file
from reporting.extracts.utils import start_extract as start_remote_extract
from reporting.extracts.format import get_column_mapping
from reporting.reports.helpers.grade_course import get_grade_course_expr
from reporting.reports.helpers.constants import Constants as ReportingConstants
from reporting.reports.helpers.constants import get_required_schema_by_year
from reporting.reports.helpers.metadata import get_subject_by_alias
from reporting.security.context import select_with_context, is_permitted


def start_extract(extract):
    if not is_permitted(extract.permission, {ReportingConstants.STATECODE: extract.state_code}):
        raise ForbiddenError("Not authorized")
    working_dir = extract.working_directory
    tasks = get_tasks(extract)
    # registration id
    reg_id, download_url, web_download_url = extract.register_file()
    # Currently, we have one archive file only per request
    start_remote_extract(
        extract.tenant,
        extract.request_id,
        working_dir,
        [extract.archive_file_name],
        [working_dir],
        [reg_id],
        tasks,
        queue=extract.get_queue()
    )
    return reg_id, download_url, web_download_url, extract.request_id


def get_tasks(extract):
    tasks = []
    for extraction_type in extract.extraction_data_types:
        for subject in extract.subjects:
            queries, ext = extract.get_queries(extraction_type, subject)
            for query, table_name in queries:
                task = create_new_task(table_name, extract, extraction_type, subject, query, ext)
                tasks.append(task)
    return tasks


def create_new_task(table_name, extract, extraction_type, subject, query, ext):
    file_name = path.join(extract.working_directory,
                          extract.get_file_name(subject, ext, table_name))
    return {
        TaskConstants.TASK_QUERIES: {QueryType.QUERY: query},
        TaskConstants.TASK_FILE_NAME: file_name,
        TaskConstants.EXTRACTION_DATA_TYPE: extraction_type,
        TaskConstants.EXTRACT_TYPE: extract.extract_type,
        TaskConstants.TASK_TASK_ID: str(uuid4()),
        TaskConstants.SUBJECT: get_subject_by_alias(subject),
        TaskConstants.TASK_JSON_FORMATTER: extract.get_formatter(),
    }


class BaseExtract():
    '''
    Base class for data extract
    '''

    MULTI_GRADE = 'multigrade'

    TIMESTAMP_FORMAT = "%m-%d-%Y_%H-%M-%S"

    def __init__(self,
                 stateCode=None,
                 year=None,
                 subject=None,
                 gradeCourse=None,
                 extract_type=None,
                 **kwargs):
        self.state_code = stateCode
        self._grade_course = gradeCourse
        self._subject = subject
        self.extract_type = extract_type
        self._year = year
        self.set_user_info()

    @property
    def year(self):
        return format_asmt_year(self._year)

    def set_user_info(self):
        self.request_id = str(uuid4())
        self.user = authenticated_userid(get_current_request())
        self.tenant = get_tenant_by_state_code(self.state_code)

    @property
    def extraction_data_types(self):
        raise NotImplementedError()

    @property
    def permission(self):
        raise NotImplementedError()

    @property
    def grade_course(self):
        if not self._grade_course:
            raise ExtractionError("No grade/course given")
        if len(self._grade_course) > 1:
            return BaseExtract.MULTI_GRADE
        else:
            return self._grade_course[0].replace(' ', '_')

    @property
    def subjects(self):
        if self._subject and self._subject not in ReportingConstants.SUBJECT_MAP:
            raise ExtractionError("Invalid Subject")
        # extract one subject at a time by default
        return [self._subject]

    def get_queue(self):
        return get_current_registry().settings.get('extract.job.queue.async',
                                                   TaskConstants.DEFAULT_QUEUE_NAME)

    @property
    def working_directory(self):
        '''
        Directory for working directory to be archived
        '''
        return path.join(self.base_request_directory, 'data')

    @property
    def base_request_directory(self):
        base_dir = get_current_registry().settings.get('extract.work_zone_base_dir',
                                                       tempfile.gettempdir())
        return path.join(base_dir, self.tenant, self.request_id)

    @property
    def archive_file_name(self):
        '''
        File path of archive file
        '''
        return path.join(self.base_request_directory,
                         'archive',
                         self.get_file_name(self._subject, 'zip'))

    @abstractmethod
    def get_table_name(self, subject):
        raise NotImplementedError()

    @abstractmethod
    def get_formatter(self):
        raise NotImplementedError()

    def get_file_name(self, subject, extension, nth=None):
        return "extract_Summative_{subject_name}_{grade_course}_{year}_{timestamp}.{extension}"\
            .format(
                subject_name=get_subject_by_alias(subject),
                grade_course=self.grade_course,
                year=self.year,
                extension=extension,
                timestamp=str(datetime.now().strftime(BaseExtract.TIMESTAMP_FORMAT)))

    def register_file(self):
        '''
        Register file with HPZ for user download
        '''
        registration_id, download_url, web_download_url = register_file(self.user.get_uid())
        return registration_id, download_url, web_download_url

    def get_queries(self, extraction_type, subject):
        if extraction_type in (ExtractionDataType.QUERY_CSV, ExtractionDataType.QUERY_CDS):
            return self.get_csv_extract_query(subject), 'csv'
        elif extraction_type == ExtractionDataType.QUERY_JSON:
            # metadata query doesn't need table name
            return [(self.get_metadata_query(), '')], 'json'
        else:
            raise ExtractionError("Unsupported query type")

    def get_csv_extract_query(self, subject=None):
        '''
        Query for CSV file (assessments)
        '''
        table_name = self.get_table_name(subject)
        with EdCoreDBConnection(tenant=self.tenant) as connection:
            table = connection.get_table(table_name)
            schemas_column_mapping = get_column_mapping(table_name)
            column_mapping = self._get_column_mapping_by_year(schemas_column_mapping)
            select_columns = [table.c[col_name].label(header) for col_name, header in column_mapping.items()]
            query = select_with_context(select_columns,
                                        from_obj=[table],
                                        permission=self.permission,
                                        state_code=self.state_code)

            query = query.where(and_(table.c.rec_status == ReportingConstants.CURRENT))
            if self._year_column_exist(table):
                query = query.where(and_(table.c.year == self.year))

            grade_courses_expr = self._get_grade_course_where_condition(table)
            query = query.where(and_(grade_courses_expr))
            yield compile_query_to_sql_text(query), table_name

    def get_metadata_query(self):
        return None

    def _year_column_exist(self, table):
        """
        Check the presence of 'year' column in the table.
        Required for psychometric table where we don't have year column.
        The usual boolean context raise error:
            TypeError: Boolean value of this clause is not defined
        :param table: table where from extract data
        :return: bool
        """
        return table.c.get(ReportingConstants.YEAR) is not None

    def _get_grade_course_where_condition(self, table):
        where_condition = or_()
        if self._grade_course:
            for grade_course in self._grade_course:
                where_condition = or_(where_condition, get_grade_course_expr(grade_course, table))
        return where_condition

    def _get_column_mapping_by_year(self, column_mapping):
        #TODO: maybe we'll have some data translation here...
        return get_required_schema_by_year(self._year, column_mapping)
