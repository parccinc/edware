'''
Created on Nov 14, 2013

@author: dip
'''
import collections
from reporting.reports.helpers.constants import ExtractConstants
from copy import deepcopy


# A map of map that contains star schema table name, column to udl input name
# ex. {'dim_student': {'student_id': 'guid_student'}}
json_column_mapping = {}
table_to_csv_column_mapping = collections.OrderedDict()


def setup_input_file_format():
    '''
    Read from udl input file format from ref_column_mapping tables to get column mapping

    The assumption is that the integration and reporting tables are exactly the same
    '''
    global table_to_csv_column_mapping

    for table_name, ref_table_conf in ExtractConstants.REF_CONF_TABLE_MAPPING.items():

        # We need to organize reference tables to some general format
        if not isinstance(ref_table_conf, collections.OrderedDict):
            tmp_ref_table = collections.OrderedDict()
            tmp_ref_table[table_name] = ref_table_conf
            ref_table_conf = tmp_ref_table
        # Create a dict of list that separates each phase of udl pipeline
        table_to_csv_column_mapping[table_name] = {}
        integration_table_name = get_integration_table_name(table_name)

        for schema_name, ref_conf in ref_table_conf.items():
            phases = collections.defaultdict(list)
            mapping, definitions = ref_conf['column_mappings'], ref_conf['column_definitions']
            for row in mapping:
                current_phase = row[0]
                phases[current_phase].append(dict(zip(definitions, row)))
            csv_mapping = process_input_file_format(phases, 'lz_csv')
            table_to_csv_column_mapping[table_name][schema_name] = format_mappings(csv_mapping, integration_table_name)


def process_input_file_format(phases, input_source):
    phases = deepcopy(phases)
    mapping = collections.OrderedDict()
    keys = sorted(list(phases.keys()), key=int)
    initial_load = True
    for key in keys:
        for row in phases[key]:
            src_table = row['source_table']
            src_col = row['source_column']
            # Sometimes, source table or source column are placeholders and have no value
            if src_table is None or src_col is None:
                continue
            src_key = src_table + "|" + src_col
            tar_key = row['target_table'] + "|" + row['target_column']
            if initial_load and src_table == input_source:
                # Save everything if it's the first load
                mapping[src_key] = tar_key
            else:
                # We only care about table/columns that already exist from the previous phases
                # Add an entry to mapping
                # We don't need to delete existing mapping as it may be map to more than one column
                cur_value = mapping.get(src_key)
                if cur_value is not None:
                    mapping[tar_key] = cur_value
        if initial_load:
            initial_load = False
            # inverse it so the key is the source table of the next phase,
            # and value is the input file format column
            mapping = collections.OrderedDict([(v, k) for k, v in mapping.items()])
    return mapping


def format_mappings(mapping, table_name):
    # Format based on column_mapping schema mapping. Currently, only format for these tables
    column_mapping = {table_name: collections.OrderedDict()}
    for k, v in mapping.items():
        table_column_index = k.index('|')
        table = k[:table_column_index]
        star_column = k[table_column_index + 1:]
        tar_table_column_index = v.index('|') + 1
        if table in column_mapping.keys():
            column_mapping[table][star_column] = v[tar_table_column_index:]
    return column_mapping.get(table_name)


def get_column_mapping(table_name):
    '''
    Given a table name, return the column mapping for that table
    returns csv mapping

    '''
    return table_to_csv_column_mapping.get(table_name, {})


def get_integration_table_name(rpt_table_name):
    # Get the integration table name to get data from ref mapping
    return rpt_table_name.replace('rpt', 'int')
