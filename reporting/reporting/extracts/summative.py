from reporting.reports.helpers.constants import Constants as ReportingConstants,\
    RptTable, ExtractConstants as SubjectConstants
from parcc_common.security.constants import RolesConstants
from reporting.extracts.base_extract import BaseExtract
from parcc_common.extracts.json_formatter import summative_json_formatter
from edcore.database.edcore_connector import EdCoreDBConnection
from sqlalchemy.sql.expression import select, and_
from parcc_common.extracts.constants import JSONConstants, ExtractConstants
from edcore.utils.utils import compile_query_to_sql_text
from edextract.tasks.constants import ExtractionDataType
from reporting.reports.helpers.metadata import get_subject_by_alias
from datetime import datetime


class SummativeExtract(BaseExtract):

    def __init__(self, *args, **kargs):
        super().__init__(*args, extract_type=ExtractConstants.SUMMATIVE_ASMT_RESULT, **kargs)

    def get_table_name(self, subject):
        return RptTable.MATH_SUM if subject == ReportingConstants.SUBJECT1 else RptTable.ELA_SUM

    def get_formatter(self):
        return summative_json_formatter

    def get_metadata_query(self):
        # only generate JSON file for one grade course case
        with EdCoreDBConnection(tenant=self.tenant) as connection:
            test_score_table = connection.get_table(RptTable.RPT_SUM_TEST_SCORE_M)
            test_level_table = connection.get_table(RptTable.RPT_SUM_TEST_LEVEL_M)
            query = select([test_score_table.c.sum_period.label(JSONConstants.DB_SUMMATIVE_PERIOD),
                            test_level_table.c.perf_lvl_id.label(JSONConstants.DB_TEST_LEVEL_ID),
                            test_level_table.c.cutpoint_label.label(JSONConstants.DB_TEST_LEVEL_LABEL),
                            test_level_table.c.cutpoint_low.label(JSONConstants.DB_TEST_LEVEL_LOW),
                            test_level_table.c.cutpoint_upper.label(JSONConstants.DB_TEST_LEVEL_UPPER),
                            test_level_table.c.test_code.label(JSONConstants.DB_TEST_CODE)]).\
                select_from(test_score_table.join(test_level_table, test_score_table.c.test_code == test_level_table.c.test_code)).\
                where(and_(test_score_table.c.rec_status == ReportingConstants.CURRENT,
                           test_level_table.c.rec_status == ReportingConstants.CURRENT,
                           test_level_table.c.test_code == self.test_code))
        return compile_query_to_sql_text(query)

    @property
    def test_code(self):
        return SubjectConstants.GRADE_SUBJECT_TEST_CODE_MAP[self.subjects[0]][self._grade_course[0]]

    @property
    def extraction_data_types(self):
        if self.grade_course is BaseExtract.MULTI_GRADE:
            return [ExtractionDataType.QUERY_CSV]
        else:
            return [ExtractionDataType.QUERY_CSV, ExtractionDataType.QUERY_JSON]

    @property
    def permission(self):
        return RolesConstants.SF_EXTRACT

    def get_file_name(self, subject, extension, table_name=None):
        return "AssmtResults_Summative_{subject_name}_{grade_course}_{year}_{timestamp}.{extension}"\
            .format(
                subject_name=get_subject_by_alias(subject),
                grade_course=self.grade_course,
                year=self.year,
                extension=extension,
                timestamp=str(datetime.now().strftime(BaseExtract.TIMESTAMP_FORMAT)))
