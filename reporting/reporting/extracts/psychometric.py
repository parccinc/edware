from datetime import datetime
from edextract.tasks.constants import ExtractionDataType
from reporting.reports.helpers.constants import RptTable
from reporting.extracts.base_extract import BaseExtract
from parcc_common.security.constants import RolesConstants
from parcc_common.extracts.json_formatter import basic_json_formatter
from parcc_common.extracts.constants import ExtractConstants


class PsychometricExtract(BaseExtract):
    '''
    Psychometric Extracts has a permission, but we don't actually append where clauses to query
    as the table is not state/district/school info
    '''
    def __init__(self, *args, **kargs):
        super().__init__(*args, extract_type=ExtractConstants.PSYCHOMETRIC, **kargs)

    @property
    def year(self):
        return str(self._year)

    def get_file_name(self, subject, extension, table_name=None):
        return "PsychometricItemFile_{year}_{timestamp}.{extension}".format(
            year=self.year,
            extension=extension,
            timestamp=str(datetime.now().strftime(BaseExtract.TIMESTAMP_FORMAT)))

    @property
    def extraction_data_types(self):
        return [ExtractionDataType.QUERY_CSV, ExtractionDataType.QUERY_JSON]

    @property
    def permission(self):
        return RolesConstants.PF_EXTRACT

    def get_table_name(self, subject):
        return RptTable.RPT_ITEM_P_DATA

    def get_formatter(self):
        return basic_json_formatter
