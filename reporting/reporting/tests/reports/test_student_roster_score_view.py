from unittest.mock import patch
from edcore.tests.utils.unittest_with_edcore_sqlite import UnittestEdcoreDBConnection
from reporting.reports.summative.student_roster_report import StudentRosterScoresReport
from reporting.reports.routes.student_roster_route import get_student_roster_report
from reporting.reports.helpers.constants import AssessmentType, RptTable
from edapi.httpexceptions import EdApiHTTPPreconditionFailed
from reporting.tests.reports.base import BaseReportTestCase
from edcore.utils.query import QueryMapKey
from reporting.security.roles.pii import PII  # @UnusedImport


class TestStudentRosterReportScoreView(BaseReportTestCase):
    def setUp(self):
        BaseReportTestCase.setUp(self)

        self.valid_params = {
            'stateCode': 'RI',
            'districtGuid': 'R0001',
            'schoolGuid': 'RN001',
            'gradeCourse': '09',
            'subject': 'subject2',
            'year': 2015,
            'asmtType': AssessmentType.SUMMATIVE,
            'view': 'scores',
        }
        self.test_report = StudentRosterScoresReport(**self.valid_params)
        self.dummy_results = {
            'subject2': [
                {
                    'sum_scale_score': 100,
                    'sum_read_scale_score': 21,
                    'sum_write_scale_score': 24,
                    'subclaim1_category': 1,
                    'subclaim2_category': 1,
                    'subclaim3_category': 3,
                    'subclaim4_category': 2,
                    'subclaim5_category': 1,
                    'subclaim6_category': None,
                    'state_growth_percent': 44,
                    'parcc_growth_percent': 14,
                    'student_parcc_id': 'EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q',
                    'asmt_subject': 'English Language Arts/Literacy',
                    'student_middle_name': 'Betty',
                    'student_first_name': 'Darci',
                    'student_last_name': 'Forst',
                    'student_grade': '08',
                    'state_code': 'RI',
                    'sum_perf_lvl': 3,
                    'include_in_roster': 'Y',
                },
                {
                    'sum_scale_score': 123,
                    'sum_read_scale_score': 24,
                    'sum_write_scale_score': 27,
                    'subclaim1_category': 3,
                    'subclaim2_category': 3,
                    'subclaim3_category': 2,
                    'subclaim4_category': 3,
                    'subclaim5_category': 3,
                    'subclaim6_category': None,
                    'state_growth_percent': 96,
                    'parcc_growth_percent': 95,
                    'student_parcc_id': 'MyXTFn1S8qz5lQJAjw11CeaMjoaIiQbo8KihC5ix',
                    'asmt_subject': 'English Language Arts/Literacy',
                    'student_middle_name': 'Barbar',
                    'student_first_name': 'Christin',
                    'student_last_name': 'Gatti',
                    'student_grade': '08',
                    'state_code': 'RI',
                    'sum_perf_lvl': 3,
                    'include_in_roster': 'Y',
                }
            ],
            'subject1': [
                {
                    'sum_scale_score': 712,
                    'sum_read_scale_score': None,
                    'sum_write_scale_score': None,
                    'subclaim1_category': 2,
                    'subclaim2_category': 2,
                    'subclaim3_category': 3,
                    'subclaim4_category': 1,
                    'subclaim5_category': 3,
                    'subclaim6_category': None,
                    'state_growth_percent': 86,
                    'parcc_growth_percent': 77,
                    'student_parcc_id': 'EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q',
                    'asmt_subject': 'Mathematics',
                    'student_middle_name': 'Betty',
                    'student_first_name': 'Darci',
                    'student_last_name': 'Forst',
                    'student_grade': '08',
                    'state_code': 'RI',
                    'sum_perf_lvl': 4,
                    'include_in_roster': 'Y',
                },
                {
                    'sum_scale_score': 692,
                    'sum_read_scale_score': None,
                    'sum_write_scale_score': None,
                    'subclaim1_category': 1,
                    'subclaim2_category': 3,
                    'subclaim3_category': 1,
                    'subclaim4_category': 2,
                    'subclaim5_category': 3,
                    'subclaim6_category': None,
                    'state_growth_percent': 42,
                    'parcc_growth_percent': 8,
                    'student_parcc_id': 'MyXTFn1S8qz5lQJAjw11CeaMjoaIiQbo8KihC5ix',
                    'asmt_subject': 'Mathematics',
                    'student_middle_name': 'Barbar',
                    'student_first_name': 'Christin',
                    'student_last_name': 'Gatti',
                    'student_grade': '08',
                    'state_code': 'RI',
                    'sum_perf_lvl': 4,
                    'include_in_roster': 'Y',
                }
            ]
        }
        self.dummy_assessments = {
            QueryMapKey(type='assessments', subject='rpt_ela_sum'): [{'sum_scale_score': 692,
                                                                      'sum_read_scale_score': None,
                                                                      'sum_write_scale_score': None,
                                                                      'subclaim1_category': 1,
                                                                      'subclaim2_category': 3,
                                                                      'subclaim3_category': 1,
                                                                      'subclaim4_category': 2,
                                                                      'subclaim5_category': 3,
                                                                      'subclaim6_category': None,
                                                                      'state_growth_percent': 42,
                                                                      'parcc_growth_percent': 8,
                                                                      'student_parcc_id': 'MyXTFn1S8qz5lQJAjw11CeaMjoaIiQbo8KihC5ix',
                                                                      'asmt_subject': 'Mathematics',
                                                                      'student_middle_name': 'Barbar',
                                                                      'student_first_name': 'Christin',
                                                                      'student_last_name': 'Gatti',
                                                                      'student_grade': '08',
                                                                      'state_code': 'RI',
                                                                      'sum_perf_lvl': 4,
                                                                      'include_in_roster': 'Y'}]
        }

    def test_student_roster_report_missing_params(self):
        self.request.method = 'POST'
        self.request.json_body = {
            'stateCode': 'RI'
        }
        self.assertRaises(EdApiHTTPPreconditionFailed, None, get_student_roster_report, self.request)

    @patch('reporting.reports.base_report.execute_mapped_queries_remotely')
    def test_student_report_valid_params(self, exec_query_patch):
        self.request.method = 'POST'
        self.request.json_body = self.valid_params
        exec_query_patch.return_value = self.dummy_assessments
        actual = get_student_roster_report(self.valid_params)
        self.assertIsNotNone(actual)
        self.assertIn('user_info', actual, "response should contain user information")
        self.assertIn('context', actual, "response should contain context information")
        self.assertIn('subjects', actual, "response should contain subject information")

    def test_get_breadcrumbs(self):
        actual = StudentRosterScoresReport(**self.valid_params).get_breadcrumbs_context()
        items = actual['items']
        self.assertIsNotNone(items)
        self.assertEqual(len(items), 5, "breadcrumb must contain 5 levels")
        # test student's name and id because that's the information ISR care about
        self.assertEqual(items[4]['type'], 'grade', "last level of breadcrumb must be grade")
        self.assertEqual(items[4]['name'], '09', "breadcrumb must contain grade")

    def test_get_query(self):
        with UnittestEdcoreDBConnection() as conn:
            actual = self.test_report.get_query(conn.get_table(RptTable.ELA_SUM))
            self.assertIsNotNone(actual, "should return some query string")
            self.assertEqual(type(actual), str, "query must be string")

    def test_run_query(self):
        actual = self.test_report.run_query()
        self.assertTrue(actual, "query should return some student data")

    def test_format_results(self):
        actual = self.test_report.format_results(self.dummy_results)
        self.assertIsNotNone(actual, "should return something")
        self.assertIn('SUMMATIVE', actual, "should contain SUMMATIVE record")
        expected = {
            'SUMMATIVE': {
                'subject1': [
                    {
                        'INVALID_SCORE': False,
                        'Overall': {'level': 4, 'score': 712},
                        'asmt_subject': 'Mathematics',
                        'parcc_growth_percent': 77,
                        'state_code': 'RI',
                        'state_growth_percent': 86,
                        'student_display_name': 'Forst, Darci B.',
                        'student_first_name': 'Darci',
                        'student_grade': '08',
                        'student_guid': 'EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q',
                        'student_last_name': 'Forst',
                        'student_middle_name': 'Betty',
                        'student_parcc_id': 'EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q',
                        'subclaim1_category': 2,
                        'subclaim2_category': 2,
                        'subclaim3_category': 3,
                        'subclaim4_category': 1,
                        'subclaim5_category': 3,
                        'subclaim6_category': None,
                        'sum_perf_lvl': 4,
                        'sum_read_scale_score': None,
                        'sum_scale_score': 712,
                        'sum_write_scale_score': None,
                        'include_in_roster': 'Y',
                    },
                    {
                        'INVALID_SCORE': False,
                        'Overall': {'level': 4, 'score': 692},
                        'asmt_subject': 'Mathematics',
                        'parcc_growth_percent': 8,
                        'state_code': 'RI',
                        'state_growth_percent': 42,
                        'student_display_name': 'Gatti, Christin B.',
                        'student_first_name': 'Christin',
                        'student_grade': '08',
                        'student_guid': 'MyXTFn1S8qz5lQJAjw11CeaMjoaIiQbo8KihC5ix',
                        'student_last_name': 'Gatti',
                        'student_middle_name': 'Barbar',
                        'student_parcc_id': 'MyXTFn1S8qz5lQJAjw11CeaMjoaIiQbo8KihC5ix',
                        'subclaim1_category': 1,
                        'subclaim2_category': 3,
                        'subclaim3_category': 1,
                        'subclaim4_category': 2,
                        'subclaim5_category': 3,
                        'subclaim6_category': None,
                        'sum_perf_lvl': 4,
                        'sum_read_scale_score': None,
                        'sum_scale_score': 692,
                        'sum_write_scale_score': None,
                        'include_in_roster': 'Y',
                    }
                ],
                'subject2': [
                    {
                        'INVALID_SCORE': False,
                        'Overall': {'level': 3, 'score': 100},
                        'asmt_subject': 'English Language Arts/Literacy',
                        'parcc_growth_percent': 14,
                        'state_code': 'RI',
                        'state_growth_percent': 44,
                        'student_display_name': 'Forst, Darci B.',
                        'student_first_name': 'Darci',
                        'student_grade': '08',
                        'student_guid': 'EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q',
                        'student_last_name': 'Forst',
                        'student_middle_name': 'Betty',
                        'student_parcc_id': 'EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q',
                        'subclaim1_category': 1,
                        'subclaim2_category': 1,
                        'subclaim3_category': 3,
                        'subclaim4_category': 2,
                        'subclaim5_category': 1,
                        'subclaim6_category': None,
                        'sum_perf_lvl': 3,
                        'sum_read_scale_score': 21,
                        'sum_scale_score': 100,
                        'sum_write_scale_score': 24,
                        'include_in_roster': 'Y',
                    },
                    {
                        'INVALID_SCORE': False,
                        'Overall': {'level': 3, 'score': 123},
                        'asmt_subject': 'English Language Arts/Literacy',
                        'parcc_growth_percent': 95,
                        'state_code': 'RI',
                        'state_growth_percent': 96,
                        'student_display_name': 'Gatti, Christin B.',
                        'student_first_name': 'Christin',
                        'student_grade': '08',
                        'student_guid': 'MyXTFn1S8qz5lQJAjw11CeaMjoaIiQbo8KihC5ix',
                        'student_last_name': 'Gatti',
                        'student_middle_name': 'Barbar',
                        'student_parcc_id': 'MyXTFn1S8qz5lQJAjw11CeaMjoaIiQbo8KihC5ix',
                        'subclaim1_category': 3,
                        'subclaim2_category': 3,
                        'subclaim3_category': 2,
                        'subclaim4_category': 3,
                        'subclaim5_category': 3,
                        'subclaim6_category': None,
                        'sum_perf_lvl': 3,
                        'sum_read_scale_score': 24,
                        'sum_scale_score': 123,
                        'sum_write_scale_score': 27,
                        'include_in_roster': 'Y',
                    }
                ]
            }
        }
        self.assertEqual(sorted(actual), sorted(expected))
        self.assertEqual(sorted(actual['SUMMATIVE']), sorted(expected['SUMMATIVE']))
        self.assertEqual(sorted(actual['SUMMATIVE']['subject1'][0]), sorted(expected['SUMMATIVE']['subject1'][0]))
