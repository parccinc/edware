'''
Created on Apr 24, 2015

@author: dip
'''
import unittest
from edcore.tests.utils.unittest_with_edcore_sqlite import UnittestEdcoreDBConnection,\
    Unittest_with_edcore_sqlite_no_data_load
from reporting.reports.helpers.grade_course import get_grade_course_expr
from reporting.reports.helpers.constants import RptTable


class TestGradeCourse(Unittest_with_edcore_sqlite_no_data_load):

    def test_grade(self):
        with UnittestEdcoreDBConnection() as conn:
            table = conn.get_table(RptTable.MATH_SUM)
            expr = get_grade_course_expr('Grade 3', table)
            self.assertIn('asmt_grade', str(expr))

    def test_course(self):
        with UnittestEdcoreDBConnection() as conn:
            table = conn.get_table(RptTable.MATH_SUM)
            expr = get_grade_course_expr('Algebra', table)
            self.assertIn('asmt_subject', str(expr))


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
