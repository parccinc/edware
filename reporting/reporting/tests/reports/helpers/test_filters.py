'''
Created on Jul 16, 2013

@author: tosako
'''
import unittest
from sqlalchemy.sql.expression import select
from edcore.tests.utils.unittest_with_edcore_sqlite import Unittest_with_edcore_sqlite_no_data_load,\
    UnittestEdcoreDBConnection
from reporting.reports.helpers.constants import RptTable
from reporting.reports.helpers.filters import has_filters, FilterGender,\
    FilterEthnicity, FilterEconoDisadv, FilterDisabilities, FilterEnglishLearner,\
    apply_filter_to_query


class TestFilters(Unittest_with_edcore_sqlite_no_data_load):

    def test_has_filters_with_empty_params(self):
        self.assertFalse(has_filters({}))

    def test_has_filters_with_no_filters(self):
        self.assertFalse(has_filters({'notDemographicFilter': 'a'}))

    def test_has_filters_with_filters(self):
        self.assertTrue(has_filters({FilterGender.NAME: 'a'}))
        self.assertTrue(has_filters({FilterEthnicity.NAME: 'a'}))
        self.assertTrue(has_filters({FilterEconoDisadv.NAME: 'a'}))
        self.assertTrue(has_filters({FilterDisabilities.NAME: 'a'}))
        self.assertTrue(has_filters({FilterEnglishLearner.NAME: 'a'}))

    def test_apply_filter_to_query_with_no_filters(self):
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.ELA_SUM)
            query = select([asmt_table.c.resp_school_id],
                           from_obj=([asmt_table]))
            query = apply_filter_to_query(query, asmt_table, {})
            self.assertIsNone(query._whereclause)

    def test_apply_filter_to_query_with_ethnicity_filters(self):
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.ELA_SUM)
            query = select([asmt_table.c.resp_school_id],
                           from_obj=([asmt_table]))
            query = apply_filter_to_query(query, asmt_table, {FilterEthnicity.NAME: [FilterEthnicity.ASIAN]})
            self.assertIsNotNone(query._whereclause)
            self.assertIn(RptTable.ELA_SUM + '.ethnicity', str(query._whereclause))

    def test_apply_filter_to_query_with_disabilities_filters(self):
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.ELA_SUM)
            query = select([asmt_table.c.resp_school_id],
                           from_obj=([asmt_table]))
            query = apply_filter_to_query(query, asmt_table, {FilterDisabilities.NAME: [FilterDisabilities.NO]})
            self.assertIsNotNone(query._whereclause)
            self.assertIn(RptTable.ELA_SUM + '.disabil_student', str(query._whereclause))

    def test_apply_filter_to_query_with_econ_disadv_filters(self):
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.ELA_SUM)
            query = select([asmt_table.c.resp_school_id],
                           from_obj=([asmt_table]))
            query = apply_filter_to_query(query, asmt_table, {FilterEconoDisadv.NAME: [FilterEconoDisadv.YES]})
            self.assertIsNotNone(query._whereclause)
            self.assertIn(RptTable.ELA_SUM + '.econo_disadvantage', str(query._whereclause))

    def test_apply_filter_to_query_with_gender_filters(self):
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.ELA_SUM)
            query = select([asmt_table.c.resp_school_id],
                           from_obj=([asmt_table]))
            query = apply_filter_to_query(query, asmt_table, {FilterGender.NAME: [FilterGender.MALE]})
            self.assertIsNotNone(query._whereclause)
            self.assertIn(RptTable.ELA_SUM + '.student_sex', str(query._whereclause))

    def test_apply_filter_to_query_with_english_learner_filters(self):
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.ELA_SUM)
            query = select([asmt_table.c.resp_school_id],
                           from_obj=([asmt_table]))
            query = apply_filter_to_query(query, asmt_table, {FilterEnglishLearner.NAME: [FilterEnglishLearner.ELL]})
            self.assertIsNotNone(query._whereclause)
            self.assertIn(RptTable.ELA_SUM + '.ell', str(query._whereclause))

    def test_apply_filter_to_query_with_english_learner_filters_both(self):
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.ELA_SUM)
            query = select([asmt_table.c.resp_school_id],
                           from_obj=([asmt_table]))
            query = apply_filter_to_query(query, asmt_table, {FilterEnglishLearner.NAME: [FilterEnglishLearner.BOTH]})
            self.assertIsNotNone(query._whereclause)
            self.assertTrue(len(query._whereclause), 2)

    def test_apply_filter_to_query_with_english_learner_filters_multi(self):
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.ELA_SUM)
            query = select([asmt_table.c.resp_school_id],
                           from_obj=([asmt_table]))
            query = apply_filter_to_query(query, asmt_table, {FilterEnglishLearner.NAME:
                                                              [FilterEnglishLearner.BOTH, FilterEnglishLearner.ELL, FilterEnglishLearner.LEP]})
            self.assertIsNotNone(query._whereclause)
            self.assertTrue(len(query._whereclause), 4)

    def test_apply_filter_to_query_with_multi_filters(self):
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.MATH_SUM)
            query = select([asmt_table.c.resp_school_id],
                           from_obj=([asmt_table]))
            filters = {FilterGender.NAME: [FilterGender.FEMALE],
                       FilterEconoDisadv.NAME: [FilterEconoDisadv.YES],
                       FilterEthnicity.NAME: [FilterEthnicity.AMERICAN]}
            query = apply_filter_to_query(query, asmt_table, filters)
            self.assertIsNotNone(query._whereclause)
            self.assertIn("rpt_math_sum.student_sex", str(query._whereclause))
            self.assertIn("rpt_math_sum.econo_disadvantage", str(query._whereclause))
            self.assertIn("rpt_math_sum.ethnicity", str(query._whereclause))

    def test_FilterGender(self):
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.ELA_SUM)
            query = select([asmt_table.c.resp_school_id],
                           from_obj=([asmt_table]))
            f = FilterGender({FilterGender.NAME: ['M', 'F']})
            query = f.append_condition(query, asmt_table)
            self.assertIsNotNone(query._whereclause)
            self.assertIn("rpt_ela_sum.student_sex", str(query._whereclause))

    def test_FilterEthnicity(self):
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.ELA_SUM)
            query = select([asmt_table.c.resp_school_id],
                           from_obj=([asmt_table]))
            f = FilterEthnicity({FilterEthnicity.NAME: ['01', '02']})
            query = f.append_condition(query, asmt_table)
            self.assertIsNotNone(query._whereclause)
            self.assertIn("rpt_ela_sum.ethnicity", str(query._whereclause))

    def test_FilterEconoDisadv(self):
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.ELA_SUM)
            query = select([asmt_table.c.resp_school_id],
                           from_obj=([asmt_table]))
            f = FilterEconoDisadv({FilterEconoDisadv.NAME: ['Y']})
            query = f.append_condition(query, asmt_table)
            self.assertIsNotNone(query._whereclause)
            self.assertIn("rpt_ela_sum.econo_disadvantage", str(query._whereclause))

    def test_FilterDisabilities(self):
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.ELA_SUM)
            query = select([asmt_table.c.resp_school_id],
                           from_obj=([asmt_table]))
            f = FilterDisabilities({FilterDisabilities.NAME: ['Y']})
            query = f.append_condition(query, asmt_table)
            self.assertIsNotNone(query._whereclause)
            self.assertIn("rpt_ela_sum.disabil_student", str(query._whereclause))

    def test_FilterEnglishLearner_ell(self):
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.ELA_SUM)
            query = select([asmt_table.c.resp_school_id],
                           from_obj=([asmt_table]))
            f = FilterEnglishLearner({FilterEnglishLearner.NAME: ['ELL']})
            query = f.append_condition(query, asmt_table)
            self.assertIsNotNone(query._whereclause)
            self.assertIn("rpt_ela_sum.ell", str(query._whereclause))

    def test_FilterEnglishLearner_lep(self):
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.ELA_SUM)
            query = select([asmt_table.c.resp_school_id],
                           from_obj=([asmt_table]))
            f = FilterEnglishLearner({FilterEnglishLearner.NAME: ['LEP']})
            query = f.append_condition(query, asmt_table)
            self.assertIsNotNone(query._whereclause)
            self.assertIn("rpt_ela_sum.lep_status", str(query._whereclause))

    def test_FilterEnglishLearner_both(self):
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.ELA_SUM)
            query = select([asmt_table.c.resp_school_id],
                           from_obj=([asmt_table]))
            f = FilterEnglishLearner({FilterEnglishLearner.NAME: ['BOTH']})
            query = f.append_condition(query, asmt_table)
            self.assertIsNotNone(query._whereclause)
            self.assertIn("rpt_ela_sum.ell", str(query._whereclause))
            self.assertIn("rpt_ela_sum.lep_status", str(query._whereclause))


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.test_value_NONE']
    unittest.main()
