__author__ = 'esnyder'
import unittest
from reporting.reports.helpers.legend import get_formatted_legend_info, get_formatted_cut_points


class TestLegend(unittest.TestCase):

    def setUp(self):
        self.test_score_db_results = [
            {
                'perf_lvl_id': '1',
                'cutpoint_label': 'minimal',
                'cutpoint_low': '1',
                'cutpoint_upper': '75'
            },
            {
                'perf_lvl_id': '2',
                'cutpoint_label': 'partial',
                'cutpoint_low': '76',
                'cutpoint_upper': '150'
            },
            {
                'perf_lvl_id': '3',
                'cutpoint_label': 'moderate',
                'cutpoint_low': '151',
                'cutpoint_upper': '200'
            }
        ]

    def test_get_formatted_legend_info(self):
        results = get_formatted_legend_info(self.test_score_db_results)
        self.assertEqual(len(results), 3)
        self.assertEqual(results[0]['level'], 1)
        self.assertEqual(results[1]['title1'], 'Partially met')
        self.assertEqual(results[2]['title2'], 'expectations')

    def test_get_formatted_cut_points(self):
        results = get_formatted_cut_points(self.test_score_db_results)
        expected = [
            {'color': 'rgba(56, 152, 69, 0.15)', 'end': 75, 'start': 1},
            {'color': 'rgba(141, 199, 83, 0.15)', 'end': 150, 'start': 76},
            {'color': 'rgba(234, 204, 76, 0.15)', 'end': 200, 'start': 151}
        ]
        self.assertEqual(results, expected)


if __name__ == '__main__':
    unittest.main()
