'''
Created on Feb 20, 2015

@author: dip
'''
import unittest
from reporting.reports.helpers.report_table_map import ReportTableMap
from reporting.reports.helpers.constants import Constants, RptTable


class TestReportTableMap(unittest.TestCase):

    def test_with_no_subjects(self):
        r = ReportTableMap()
        self.assertRaises(AssertionError, r.get_table_name)

    def test_with_subject(self):
        r = ReportTableMap(Constants.SUBJECT1)
        self.assertEqual(r.get_table_name(), RptTable.MATH_SUM)

    def test_get_all_names(self):
        r = ReportTableMap()
        tables = r.get_table_names()
        self.assertEqual(len(tables), 2)
        self.assertIn((RptTable.ELA_SUM,), tables)

if __name__ == "__main__":
    unittest.main()
