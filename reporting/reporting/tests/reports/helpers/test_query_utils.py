'''
Created on Mar 31, 2015

@author: dip
'''
import unittest
from reporting.reports.helpers.query_utils import execute_query_remotely, execute_mapped_queries_remotely, \
    construct_query_info
from beaker.cache import CacheManager
from beaker.util import parse_cache_config_options
from pyramid.testing import DummyRequest
from pyramid import testing
from edauth.tests.test_helper.create_session import create_test_session
import edauth
from parcc_common.security.constants import RolesConstants
from edcore.security.tenant import set_tenant_map
from reporting.security.roles.pii import PII  # @UnusedImport
from edextract.celery import setup_celery
from edcore.tests.utils.unittest_with_edcore_sqlite import get_unittest_tenant_name,\
    Unittest_with_edcore_sqlite
from pyramid.security import Allow
from sqlalchemy.sql.expression import select


class TestQueryUtils(Unittest_with_edcore_sqlite):

    def setUp(self):
        cache_opts = {
            'cache.type': 'memory',
            'cache.regions': 'public.data'
        }
        CacheManager(**parse_cache_config_options(cache_opts))

        self.__request = DummyRequest()
        # Must set hook_zca to false to work with unittest_with_sqlite
        self.__config = testing.setUp(request=self.__request, hook_zca=False)
        defined_roles = [(Allow, RolesConstants.PII, ('view', 'logout'))]
        edauth.set_roles(defined_roles)
        set_tenant_map({get_unittest_tenant_name(): 'RI'})
        # Set up context security
        dummy_session = create_test_session([RolesConstants.PII])
        self.__config.testing_securitypolicy(dummy_session.get_user())
        # celery settings for UT
        settings = {'extract.celery.CELERY_ALWAYS_EAGER': True}
        self.__request.cookies = {'edware': '123'}
        setup_celery(settings)

    def tearDown(self):
        testing.tearDown()

    def test_execute_query_remotely(self):
        query = select([1])
        self.assertEqual(len(execute_query_remotely('RI', query)), 1)

    def test_execute_mapped_queries_remotely(self):
        query_map = {
            'key1': construct_query_info(select([1]), 'RI'),
            'key2': construct_query_info(select([1]), 'RI')
        }
        results = execute_mapped_queries_remotely(query_map)
        self.assertEqual(len(results), 2)
        self.assertEqual(results['key1'][0]['1'], 1)
        self.assertEqual(results['key2'][0]['1'], 1)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
