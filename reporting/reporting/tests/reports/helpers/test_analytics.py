from pyramid.registry import Registry
from reporting.tests.reports.base import BaseReportTestCase
from reporting.reports.helpers.analytics import get_analytics_info


class TestAnalytics((BaseReportTestCase)):

    def setUp(self):
        reg = Registry()
        reg.settings = {}
        self.dummy_link = 'http://dummy.link'
        reg.settings['analytics.url'] = self.dummy_link
        BaseReportTestCase.setUp(self, registry=reg)

    def test_get_analytics_info(self):
        actual = get_analytics_info()
        expected = {
            'url': self.dummy_link
        }
        self.assertEqual(actual, expected)
