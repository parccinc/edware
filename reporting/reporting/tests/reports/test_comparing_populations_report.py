from pyramid import testing
from edcore.tests.utils.unittest_with_edcore_sqlite import UnittestEdcoreDBConnection,\
    get_unittest_tenant_name
from reporting.reports.base_aggregator import ReportLevel
from reporting.reports.summative.compare_pop_report import ComparingPopReport,\
    CACHE_REGION_PUBLIC_DATA, get_comparing_populations_cache_route,\
    CACHE_REGION_PUBLIC_FILTERING_DATA, ComparingPopAggregator,\
    get_comparing_populations_cache_key
from reporting.reports.helpers.constants import Constants, AssessmentType,\
    RptTable, Views
from reporting.reports.summative.helpers.constants import Constants as SummativeConstants
from unittest.mock import patch, ANY
from reporting.tests.reports.base import BaseReportTestCase
from reporting.reports.helpers.filters import FilterGender
from reporting.reports.helpers.report_table_map import ReportTableMap
import unittest
from pyramid.registry import Registry
from edcore.utils.query import QueryMapKey


class TestComparingPopReport(BaseReportTestCase):

    def setUp(self):
        tenant = get_unittest_tenant_name()
        reg = Registry()
        reg.settings = {}
        reg.settings['min_cell_size.performance.{0}'.format(tenant)] = 2
        reg.settings['min_cell_size.growth.{0}'.format(tenant)] = 3
        reg.settings['alert_cell_size'] = 5
        BaseReportTestCase.setUp(self, registry=reg)
        self.db_results = [
            {
                'name': 'School1',
                'guid': '1234',
                'overall_perf_1': 1,
                'overall_perf_2': 0,
                'overall_perf_3': 0,
                'overall_perf_4': 0,
                'overall_perf_5': 1,
                'score': 555,
                'reading': 233,
                'writing': 23,
                'state': 23,
                'students': 2
            },
            {
                'name': 'School2',
                'guid': '2234',
                'overall_perf_1': 1,
                'overall_perf_2': 0,
                'overall_perf_3': 0,
                'overall_perf_4': 0,
                'overall_perf_5': 1,
                'score': 555,
                'reading': 233,
                'writing': 23,
                'state': 23,
                'students': 2
            },
            {
                'name': 'School3',
                'guid': '3234',
                'overall_perf_1': 1,
                'overall_perf_2': 0,
                'overall_perf_3': 0,
                'overall_perf_4': 0,
                'overall_perf_5': 1,
                'score': 555,
                'reading': 233,
                'writing': 23,
                'state': 23,
                'students': 2
            },
            {
                'name': 'School4',
                'guid': '4234',
                'overall_perf_1': 1,
                'overall_perf_2': 0,
                'overall_perf_3': 0,
                'overall_perf_4': 0,
                'overall_perf_5': 1,
                'score': 555,
                'reading': 233,
                'writing': 23,
                'state': 23,
                'students': 2
            }
        ]

    def tearDown(self):
        # reset the registry
        testing.tearDown()

    def test_get_comparing_populations_cache_route(self):
        params = {Constants.STATECODE: 'RI', Constants.YEAR: 2014}
        state = ComparingPopReport(**params)
        self.assertEqual(CACHE_REGION_PUBLIC_DATA, get_comparing_populations_cache_route(state))
        params = {Constants.STATECODE: 'RI', Constants.DISTRICTGUID: '1', Constants.YEAR: 2014}
        district = ComparingPopReport(**params)
        self.assertEqual(CACHE_REGION_PUBLIC_DATA, get_comparing_populations_cache_route(district))
        params = {Constants.STATECODE: 'RI', Constants.DISTRICTGUID: '1', Constants.SCHOOLGUID: '2', Constants.YEAR: 2014}
        school = ComparingPopReport(**params)
        self.assertIsNone(get_comparing_populations_cache_route(school))
        params = {Constants.STATECODE: 'RI', Constants.YEAR: 2014, FilterGender.NAME: [FilterGender.MALE]}
        state_with_filters = ComparingPopReport(**params)
        self.assertEqual(CACHE_REGION_PUBLIC_FILTERING_DATA, get_comparing_populations_cache_route(state_with_filters))

    def test_get_comparing_populations_cache_key(self):
        params = {Constants.STATECODE: 'RI', Constants.YEAR: 2014}
        state = ComparingPopReport(**params)
        key = get_comparing_populations_cache_key(state)
        self.assertEquals(len(key), 8)
        params = {Constants.STATECODE: 'RI', Constants.DISTRICTGUID: '1', Constants.YEAR: 2014}
        district = ComparingPopReport(**params)
        key = get_comparing_populations_cache_key(district)
        self.assertEquals(len(key), 9)

    def test_cpop_state_level(self):
        params = {Constants.STATECODE: 'RI', Constants.YEAR: 2014,
                  Constants.SUBJECT: Constants.SUBJECT1, Constants.GRADECOURSE: '03'}
        state = ComparingPopReport(**params)
        with UnittestEdcoreDBConnection() as conn:
            asmt_table = conn.get_table(RptTable.MATH_SUM)
            columns = state.get_select(asmt_table, [])
            self.assertEquals(len(columns), 4)
            self.assertEquals(str(columns[0]), RptTable.MATH_SUM + ".resp_dist_name")
            where = state.get_where(asmt_table, None)
            print(str(where))
            self.assertEquals(str(where).count('AND'), 1)
            group_by = state.get_guid_and_name(asmt_table)
            self.assertEquals(len(group_by), 2)
            self.assertEquals(str(group_by[0]), RptTable.MATH_SUM + ".resp_dist_id")

    def test_cpop_dist_level(self):
        params = {Constants.STATECODE: 'RI', Constants.DISTRICTGUID: '1', Constants.YEAR: 2014,
                  Constants.SUBJECT: Constants.SUBJECT1, Constants.GRADECOURSE: '03'}
        district = ComparingPopReport(**params)
        with UnittestEdcoreDBConnection() as conn:
            asmt_table = conn.get_table(RptTable.MATH_SUM)
            columns = district.get_select(asmt_table, [])
            self.assertEquals(len(columns), 4)
            self.assertEquals(str(columns[0]), RptTable.MATH_SUM + ".resp_school_name")
            where = district.get_where(asmt_table, None)
            self.assertEquals(len(where), 3)
            group_by = district.get_guid_and_name(asmt_table)
            self.assertEquals(len(group_by), 2)
            self.assertEquals(str(group_by[0]), RptTable.MATH_SUM + ".resp_school_id")

    def test_ComparingPopAggregator(self):
        with UnittestEdcoreDBConnection() as conn:
            asmt_table = conn.get_table(RptTable.MATH_SUM)
            tables = ReportTableMap(Constants.SUBJECT1).get_table_names()
            aggregator = ComparingPopAggregator(tables, 'RI', asmtYear='2014-2015', gradeCourse='Grade 3',
                                                filterParams={}, result='overall')
            level = ReportLevel('state', '', '')
            query = aggregator.get_summary_query(level, (), asmt_table)
            self.assertIsInstance(query, str)
            self.assertIn("rpt_math_sum.year = '2014-2015'", query)
            self.assertIn("rpt_math_sum.asmt_grade = 'Grade 3'", query)
            self.assertIn("'Rhode Island'", query)

    def test_build_query_with_entity_info(self):
        params = {Constants.STATECODE: 'RI', Constants.YEAR: 2015,
                  Constants.SUBJECT: Constants.SUBJECT1, Constants.GRADECOURSE: 'Grade 3'}
        state = ComparingPopReport(**params)
        with UnittestEdcoreDBConnection() as conn:
            asmt_table = conn.get_table(RptTable.MATH_SUM)
            query = state.build_query_with_entity_info(asmt_table, [], None)
            self.assertIsInstance(query, str)
            self.assertIn("rpt_math_sum.asmt_grade = 'Grade 3'", query)

    @patch('reporting.reports.summative.compare_pop_report.ComparingPopReport.get_query')
    @patch('reporting.reports.summative.compare_pop_report.get_legend_info_query')
    @patch('reporting.reports.summative.compare_pop_report.ComparingPopAggregator.get_summary_query')
    @patch('reporting.reports.base_report.execute_mapped_queries_remotely')
    def test_run_query(self, exec_query_patch, exec_summary_query_patch, exec_legend_query_patch, query_patch):
        state_expected_result = [{'state_data': 1234}]
        dist_expected_result = [{'district_data': 1234}]
        exec_query_patch.return_value = state_expected_result
        query_patch.return_value = 'state query'
        exec_legend_query_patch.return_value = 'legend query'
        exec_summary_query_patch.return_value = 'summary query'
        cpop = ComparingPopReport(stateCode='RI', year=2015, subject='subject2', gradeCourse='Grade 3', result='literarytext')
        result = cpop.run_query()
        self.assertEqual(result, state_expected_result)
        exec_query_patch.assert_called_once_with(
            {
                QueryMapKey(type='assessments', subject='rpt_ela_sum', level=None, tenant=None): {'state_code': 'RI', 'query': 'state query'},
                QueryMapKey(type='summary', subject='rpt_ela_sum', level='state', tenant=None): {'state_code': 'RI', 'query': 'summary query'},
                QueryMapKey(type='summary', subject='rpt_ela_sum', level='parcc', tenant='parcc'): {'tenant': 'parcc', 'is_public': True, 'query': 'summary query'},
            }
        )

        exec_query_patch.reset_mock()
        exec_query_patch.return_value = dist_expected_result
        query_patch.reset_mock()
        query_patch.return_value = 'district query'

        cpop = ComparingPopReport(stateCode='RI', districtGuid='R0003', year=2015, subject='subject1', gradeCourse='Grade 3', result='literarytext')
        result = cpop.run_query()
        self.assertEqual(result, dist_expected_result)
        exec_query_patch.assert_called_once_with(
            {
                QueryMapKey(type='summary', subject='rpt_math_sum', level='state', tenant=None): {'query': 'summary query', 'state_code': 'RI'},
                QueryMapKey(type='assessments', subject='rpt_math_sum', level=None, tenant=None): {'query': 'district query', 'state_code': 'RI'},
                QueryMapKey(type='summary', subject='rpt_math_sum', level='district', tenant=None): {'query': 'summary query', 'state_code': 'RI'},
                QueryMapKey(type='summary', subject='rpt_math_sum', level='parcc', tenant='parcc'): {'query': 'summary query', 'is_public': True, 'tenant': 'parcc'}
            }
        )

    def test_subscore_query_result(self):
        with UnittestEdcoreDBConnection() as conn:
            cpop = ComparingPopReport(stateCode='RI', year=2015, subject='subject2', gradeCourse='Grade 3', result='literarytext')
            query = cpop.get_query(conn.get_table(RptTable.ELA_SUM))
            self.assertTrue('resp_dist_id' in str(query))
            self.assertFalse('resp_school_id' in str(query))

            cpop = ComparingPopReport(stateCode='RI', districtGuid='R0003', year=2015, subject='subject2', result='vocab', gradeCourse='Grade 3')
            query = cpop.get_query(conn.get_table(RptTable.ELA_SUM))
            self.assertTrue('resp_school_id' in str(query))

    def test_get_query(self):
        with UnittestEdcoreDBConnection() as conn:
            cpop = ComparingPopReport(stateCode='RI', districtGuid='R0003', year=2015, subject='subject1', gradeCourse='Grade 3', result='overall')
            query = cpop.get_query(conn.get_table(RptTable.ELA_SUM))
            self.assertIn('resp_dist_id =', str(query))
            self.assertNotIn('resp_school_id =', str(query))

            cpop = ComparingPopReport(stateCode='RI', year=2015, subject='subject2', gradeCourse='Grade 3', result='overall')
            query = cpop.get_query(conn.get_table(RptTable.MATH_SUM))
            # Check for school guid in the where clause of school level query
            self.assertNotIn('resp_dist_id =', str(query))
            self.assertNotIn('resp_school_id =', str(query))

    def test_append_grade_course_where_condition(self):
        with UnittestEdcoreDBConnection() as connection:
            cpop = ComparingPopReport(stateCode='RI', year=2015, subject='subject2', gradeCourse='Grade 3')
            asmt_table = connection.get_table(RptTable.ELA_SUM)
            where = cpop.append_grade_course_where_condition(asmt_table, None)
            self.assertIsNotNone(where)
            self.assertEqual(str(where).count('AND'), 0)

    def test_format_results(self):
        cpop = ComparingPopReport(stateCode='RI', districtGuid='dae1acf4-afb0-4013-90ba-9dcde4b25621', asmtType=AssessmentType.SUMMATIVE, year=2015, gradeCourse="Grade 11", subject=Constants.SUBJECT2, result='overall')
        results = cpop.format_results({Constants.SUBJECT2: self.db_results})
        subject = results[AssessmentType.SUMMATIVE][Constants.SUBJECT2]
        self.assertEqual(len(subject), 4, "Four schools, four reports")
        self.assertEqual(subject[0][SummativeConstants.STUDENTS], 2, "Incorrect student count")
        self.assertEqual(subject[0][SummativeConstants.PERFORMANCELEVELS][0][Constants.PERCENTAGE], 50, "Percentage not correctly calculated")
        self.assertEqual(subject[0][Constants.LEVEL], 50, "Incorrect # of students > lvl 4 or storage of each subject for school1")
        self.assertEqual(subject[0][SummativeConstants.PERFORMANCE_SORT], 50, "Incorrect performance sort number")
        self.assertEqual(subject[0][SummativeConstants.AVG][SummativeConstants.READING], 233, "ELA fields should have reading/writing scores")

    def test_format_subscore_results(self):
        self.subscore_db_results = [
            {
                'name': 'Entity 1',
                'guid': '1234',
                'students': 6,
                'subclaim1_category_perf_1': 33.33,
                'subclaim1_category_perf_2': 0,
                'subclaim1_category_perf_3': 66.66,
                'score': 123,
                'reading': 233,
                'writing': 23,
                'state': 23,
            },
            {
                'name': 'Entity 2',
                'guid': '2345',
                'students': 4,
                'subclaim1_category_perf_1': 20,
                'subclaim1_category_perf_2': 20,
                'subclaim1_category_perf_3': 60,
                'score': 123,
                'reading': 233,
                'writing': 23,
                'state': 23,
            },
            {
                'name': 'Entity 3',
                'guid': '3456',
                'students': 9,
                'subclaim1_category_perf_1': 33.33,
                'subclaim1_category_perf_2': 55.55,
                'subclaim1_category_perf_3': 11.11,
                'score': 123,
                'reading': 233,
                'writing': 23,
                'state': 23,
            },
            {
                'name': 'Entity 4',
                'guid': '4567',
                'students': 1,
                'subclaim1_category_perf_1': 0,
                'subclaim1_category_perf_2': 0,
                'subclaim1_category_perf_3': 100,
                'score': 123,
                'reading': 233,
                'writing': 23,
                'state': 23,
            }
        ]
        cpop = ComparingPopReport(stateCode='RI', districtGuid='dae1acf4-afb0-4013-90ba-9dcde4b25621', asmtType=AssessmentType.SUMMATIVE,
                                  year=2015, gradeCourse="Grade 5", result='majorcontent', subject=Constants.SUBJECT1)
        results = cpop.format_results({Constants.SUBJECT1: self.subscore_db_results})
        subject = results[AssessmentType.SUMMATIVE][Constants.SUBJECT1]
        self.assertEqual(len(subject), 4, "Four entities, four reports")
        self.assertEqual(subject[0][SummativeConstants.STUDENTS], 6, "Incorrect student count")
        self.assertEqual(subject[0][SummativeConstants.PERFORMANCE_SORT], 67, "Incorrect performance sort number")
        self.assertEqual(subject[0][SummativeConstants.PERFORMANCELEVELS][0][Constants.PERCENTAGE], 33, "Percentage not correctly calculated")

        cpop = ComparingPopReport(stateCode='RI', asmtType=AssessmentType.SUMMATIVE, year=2015, gradeCourse="Grade 11", result='literarytext', subject=Constants.SUBJECT2)
        results = cpop.format_results({Constants.SUBJECT2: self.subscore_db_results})
        subject = results[AssessmentType.SUMMATIVE][Constants.SUBJECT2]
        self.assertEqual(len(subject), 4, "Four entities, four reports")
        self.assertEqual(subject[2][SummativeConstants.STUDENTS], 9, "Incorrect student count")
        self.assertEqual(subject[2][SummativeConstants.PERFORMANCE_SORT], 11, "Incorrect performance sort number")
        self.assertEqual(subject[2][SummativeConstants.PERFORMANCELEVELS][1][Constants.PERCENTAGE], 56, "Percentage not correctly calculated")
        self.assertFalse(subject[0][SummativeConstants.HAS_FILTERS])

        cpop = ComparingPopReport(stateCode='RI', asmtType=AssessmentType.SUMMATIVE, year=2015, gradeCourse="Grade 11", result='literarytext',
                                  view=Views.PERFORMANCE, subject=Constants.SUBJECT2, englishLearner=['LEP'])
        results = cpop.format_results({Constants.SUBJECT2: self.subscore_db_results})
        subject = results[AssessmentType.SUMMATIVE][Constants.SUBJECT2]
        self.assertEqual(len(subject), 4, "Four schools, four reports")
        self.assertNotIn(SummativeConstants.STUDENTS, subject[3], "Filtered results should not have student counts")
        self.assertTrue(subject[0][SummativeConstants.HAS_FILTERS])
        self.assertTrue(subject[3][SummativeConstants.IS_LESS_THAN_MIN_CELL_SIZE])
        self.assertIn(Constants.MIN_CELL_PERF_MSG, subject[3][SummativeConstants.MIN_CELL_SIZE_MSG])
        self.assertTrue(subject[1][SummativeConstants.IS_LESS_THAN_THRESHOLD])

    def test_format_cpop_summary_results(self):
        records = [
            {
                'students': 15,
                'score': 252,
                'reading': 31,
                'writing': 29,
                'guid': 'RI',
                'name': 'Rhode Island',
                'overall_perf_1': 2,
                'overall_perf_2': 2,
                'overall_perf_3': 6,
                'overall_perf_4': 4,
                'overall_perf_5': 1,
                'state': 56
            }
        ]
        cpop = ComparingPopReport(stateCode='RI', districtGuid='d', asmtType=AssessmentType.SUMMATIVE, year=2015,
                                  gradeCourse='Grade 6', view=Views.PERFORMANCE, result='overall', subject=Constants.SUBJECT1)
        aggregator = cpop.get_aggregator()
        results = aggregator.format_summary(cpop, level='state', results={Constants.SUBJECT1: records})
        subject = results[AssessmentType.SUMMATIVE][Constants.SUBJECT1]
        self.assertEqual(len(subject), 1)
        self.assertFalse(subject[0][SummativeConstants.HAS_FILTERS])
        self.assertEquals(subject[0][SummativeConstants.STUDENTS], 15)
        self.assertEquals(len(subject[0][SummativeConstants.PERFORMANCELEVELS]), 5)

    def test_format_results_with_filters(self):
        cpop = ComparingPopReport(stateCode='RI', districtGuid='d', asmtType=AssessmentType.SUMMATIVE, result='overall', year=2015,
                                  gradeCourse="Grade 12", subject=Constants.SUBJECT2, englishLearner=['LEP'], view=Views.PERFORMANCE)
        results = cpop.format_results({Constants.SUBJECT2: self.db_results})
        subject2 = results[AssessmentType.SUMMATIVE][Constants.SUBJECT2]
        self.assertEqual(len(subject2), 4, "Four schools, four reports")
        self.assertNotIn(SummativeConstants.STUDENTS, subject2[0], "Filtered results should not have student counts")
        self.assertNotIn(SummativeConstants.STUDENTS, subject2[1], "Filtered results should not have student counts")
        self.assertNotIn(SummativeConstants.STUDENTS, subject2[2], "Filtered results should not have student counts")
        self.assertNotIn(SummativeConstants.STUDENTS, subject2[3], "Filtered results should not have student counts")

    def test_format_results_for_growth(self):
        cpop = ComparingPopReport(stateCode='RI', districtGuid='d', asmtType=AssessmentType.SUMMATIVE, result='overall', year=2015,
                                  gradeCourse="Grade 12", subject=Constants.SUBJECT2, view=Views.GROWTH)
        results = cpop.format_results({Constants.SUBJECT2: self.db_results})
        subject2 = results[AssessmentType.SUMMATIVE][Constants.SUBJECT2]
        self.assertTrue(subject2[0][SummativeConstants.IS_LESS_THAN_MIN_CELL_SIZE])
        self.assertIn(Constants.MIN_CELL_GROWTH_MSG, subject2[0][SummativeConstants.MIN_CELL_SIZE_MSG])

if __name__ == '__main__':
    unittest.main()
