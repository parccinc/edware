from edcore.security.tenant import set_cds_tenant_map
from edcore.tests.utils.unittest_with_edcore_sqlite import UnittestEdcoreDBConnection
from reporting.reports.summative.student_roster_report import StudentRosterItemAnalysisReport
from reporting.reports.routes.student_roster_route import get_student_roster_report
from reporting.reports.helpers.constants import AssessmentType, RptTable
from reporting.tests.reports.base import BaseReportTestCase


EXPECTED_ASSESSMENTS = {
    'assessments': {
        'SUMMATIVE': {
            'subject1': [
                {
                    'student_display_name': 'Forst, Darci B.',
                    'student_first_name': 'Darci',
                    'student_last_name': 'Forst',
                    'student_grade': '09',
                    'student_parcc_id': 'EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q',
                    'INVALID_SCORE': False,
                    'Math_Item1': {
                        'score': 4
                    },
                    'Math_Item2': {
                        'score': 3
                    },

                    'Overall': {
                        'level': 4,
                        'score': 712,
                    },
                },
            ],
            'subject2': []
        },
    },
    'columns': {
        'SUMMATIVE': {
            'subject1': [
                {
                    'item_max_points': 10,
                    'name': 'Math_Item1',
                    'standards': ['CCC10: Add 2 numbers'],
                    'item_type': 'MC',
                    'item_num_responses': 1
                },
                {
                    'item_max_points': 10,
                    'name': 'Math_Item2',
                    'standards': ['CCC11: Multiply 2 numbers', 'ABC11: Divide 2 numbers'],
                    'item_type': 'TEI',
                    'item_num_responses': 1
                },
            ],
            'subject2': []
        }
    }
}


class TestStudentRosterReportItemAnalysisView(BaseReportTestCase):

    def setUp(self):
        BaseReportTestCase.setUp(self)
        self.valid_params = {
            'stateCode': 'RI',
            'districtGuid': 'R0001',
            'schoolGuid': 'RN001',
            'gradeCourse': 'Algebra I',
            'subject': 'subject1',
            'year': 2015,
            'asmtType': AssessmentType.SUMMATIVE,
            'view': 'itemAnalysis',
        }
        set_cds_tenant_map({'parcc': None})
        self.test_report = StudentRosterItemAnalysisReport(with_item_score=True, **self.valid_params)
        self.dummy_results = {
            'subject2': [],
            'subject1': [
                {
                    'student_last_name': 'Forst',
                    'student_first_name': 'Darci',
                    'parent_item_score': 4,
                    'student_middle_name': 'Betty',
                    'student_grade': '09',
                    'asmt_grade': '09',
                    'sum_perf_lvl': 4,
                    'sum_scale_score': 712,
                    'state_code': 'RI',
                    'asmt_subject': 'Mathematics',
                    'student_parcc_id': 'EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q',
                    'test_uuid': 'math08e7-bc8e-45f9-bf43-3f6c43d1f6ea',
                    'item_uin': 'Math_Item1',
                    'item_max_points': 10,
                    'standard1': 'CCC10: Add 2 numbers',
                    'test_item_type': 'MC',
                    'include_in_roster': 'Y',
                },
                {
                    'student_last_name': 'Forst',
                    'student_first_name': 'Darci',
                    'parent_item_score': 3,
                    'student_middle_name': 'Betty',
                    'student_grade': '09',
                    'asmt_grade': '09',
                    'sum_perf_lvl': 4,
                    'sum_scale_score': 712,
                    'state_code': 'RI',
                    'asmt_subject': 'Mathematics',
                    'student_parcc_id': 'EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q',
                    'test_uuid': 'math08e7-bc8e-45f9-bf43-3f6c43d1f6ea',
                    'item_uin': 'Math_Item2',
                    'item_max_points': 10,
                    'standard1': 'CCC11: Multiply 2 numbers',
                    'standard2': 'ABC11: Divide 2 numbers',
                    'test_item_type': 'TEI',
                    'include_in_roster': 'Y',
                },
            ]
        }

    def test_student_report_valid_params(self):
        self.request.method = 'POST'
        self.request.json_body = self.valid_params
        actual = get_student_roster_report(self.valid_params)
        self.assertIsNotNone(actual)
        self.assertIn('user_info', actual, "response should contain user information")
        self.assertIn('context', actual, "response should contain context information")
        self.assertIn('subjects', actual, "response should contain subject information")

    def test_get_query(self):
        with UnittestEdcoreDBConnection() as conn:
            actual = self.test_report.get_query(conn.get_table(RptTable.ELA_SUM), conn.get_table(RptTable.ELA_STUDENT_ITEM_SCORE), conn.get_table(RptTable.RPT_ITEM_P_DATA))
            self.assertIsNotNone(actual, "should return some query string")
            self.assertEqual(type(actual), str, "query must be string")

    def test_run_query(self):
        actual = self.test_report.run_query()
        self.assertTrue(actual, "query should return some student data")

    def test_format_assessments(self):
        result_assessments = {}
        result_assessments = self.test_report.format_assessments(result_assessments, self.dummy_results)

        for key, value in result_assessments.items():
            self.assertIsNotNone(value, "should return something")
            self.assertIn('SUMMATIVE', set(value.keys()), "should contain subjects record")

            actual = result_assessments[key]
            expected = EXPECTED_ASSESSMENTS[key]

            self.assertEqual(sorted(actual), sorted(expected))
            self.assertEqual(sorted(actual['SUMMATIVE']), sorted(expected['SUMMATIVE']))
            self.assertEqual(sorted(actual['SUMMATIVE']['subject1'][0]),
                             sorted(expected['SUMMATIVE']['subject1'][0]))

    # @patch('reporting.reports.student_roster_report.get_results')
    # def test_student_report(self, db_results):
    #     params = {Constants.DISTRICTGUID: 'dae1acf4-afb0-4013-90ba-9dcde4b25621',
    #               Constants.SCHOOLGUID: '3b10d26b-b013-4cdd-a916-5d577e895ed4', Constants.STATECODE: 'RI', Constants.ASMTTYPE: AssessmentType.SUMMATIVE,
    #               Constants.YEAR: '2015'}
    #     db_results.return_value = self.db_results

    #     result = get_student_roster_report(params)
    #     scores = result[Constants.ASSESSMENTS][AssessmentType.SUMMATIVE]
    #     self.assertGreater(
    #         len(scores), 0, "we should have at least 1 student report")
    #     self.assertGreater(len(scores['42e1acf4-afb0-4013-90ba-9dcde4b25621'][
    #                        Constants.SUBJECT1]), 0, "we should have at least 1 value the student's score")

    #     self.assertEqual(scores['42e1acf4-afb0-4013-90ba-9dcde4b25621'][
    #                      Constants.SUBJECT1]['Overall']['score'], '555', "We should store the overall score")
    #     self.assertGreater(len(scores['22e1acf4-afb0-4013-90ba-9dcde4b25621'][
    #                        Constants.SUBJECT2]), 1, "we should be able to hold 2 scores for a student")

    #     for row in result[Constants.COLUMNS][AssessmentType.SUMMATIVE][Constants.SUBJECT1]:
    #         self.assertIn(
    #             row, self.subj1_items, 'We should store information about item names')
    #     for row in result[Constants.COLUMNS][AssessmentType.SUMMATIVE][Constants.SUBJECT2]:
    #         self.assertIn(
    #             row, self.subj2_items, 'We should store information about item names')

    #     scores_list = list(scores.values())
    #     self.assertGreater(scores_list[1][Constants.STUDENT_LAST_NAME], scores_list[0][
    #                        Constants.STUDENT_LAST_NAME], "results should be in order of last name")

    # def test_get_query(self):
    #     with UnittestEdcoreDBConnection() as conn:
    #         query = get_query(
    #             RptTable.ELA_SUM, RptTable.ELA_STUDENT_ITEM_SCORE, conn, 'RI', '1234', '1235', '3', '2015', {})
    #         self.assertIn('asmt_grade', str(query))

    #         query = get_query(
    #             RptTable.MATH_SUM, RptTable.ELA_STUDENT_ITEM_SCORE, conn, 'RI', '1234', '1235', None, '2015', {})
    #         # Check for student grade in the where clause
    #         self.assertNotIn('asmt_grade =', str(query))
