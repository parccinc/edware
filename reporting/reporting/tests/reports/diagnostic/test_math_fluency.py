import unittest
from datetime import date
from pyramid import testing
from pyramid.registry import Registry
from reporting.reports.helpers.constants import Constants, AssessmentType
from reporting.reports.diagnostic.helpers.constants import DiagnosticReportType, FluencyConstants
from reporting.tests.reports.base import BaseReportTestCase
from reporting.reports.routes.school_report_route import get_school_report
from reporting.reports.diagnostic.math_fluency import MathFluencyReport
from edcore.utils.query import QueryMapKey
from reporting.security.roles.pii import PII  # @UnusedImport


class TestMathFluency(BaseReportTestCase):

    def setUp(self):
        reg = Registry()
        reg.settings = {}
        BaseReportTestCase.setUp(self, registry=reg)

    def tearDown(self):
        # reset the registry
        testing.tearDown()

    def test_get_diagnostic_math_fluency_report(self):
        results = get_school_report({
            Constants.STATECODE: 'RI',
            Constants.DISTRICTGUID: 'a',
            Constants.SCHOOLGUID: 'b',
            Constants.YEAR: 2015,
            Constants.SUBJECT: Constants.SUBJECT1,
            Constants.VIEW: DiagnosticReportType.FLUENCY
        })
        self.assertIsInstance(results, dict)
        self.assertIn(Constants.ASSESSMENTS, results)
        self.assertIn(Constants.SUBJECTS, results)

    def test_diagnostic_math_fluency_report(self):
        params = {
            Constants.STATECODE: 'RI',
            Constants.DISTRICTGUID: 'a',
            Constants.SCHOOLGUID: 'b',
            Constants.YEAR: 2015,
            Constants.SUBJECT: Constants.SUBJECT1,
            Constants.VIEW: DiagnosticReportType.FLUENCY
        }
        fluency = MathFluencyReport(**params)
        # Mr. Brule will only have two skills for this test; the rest are blank
        results = {
            QueryMapKey(type='assessments', subject='rpt_math_flu'): [
                {
                    Constants.STUDENT_PARCC_ID: '123',
                    Constants.STUDENT_FIRST_NAME: "Steve",
                    Constants.STUDENT_MIDDLE_NAME: "",
                    Constants.STUDENT_LAST_NAME: "Brule",
                    Constants.ASMT_GRADE: "03",
                    Constants.STAFF_ID: "a0b1c2",
                    Constants.ASMT_DATE: date(2015, 2, 2),
                    FluencyConstants.SKILL1_NAME: "Adding transcendental numbers",
                    FluencyConstants.SKILL1_TOTAL_ITEMS: "2",
                    FluencyConstants.SKILL1_NUMBER_CORRECT: "1",
                    FluencyConstants.SKILL1_TIME: "00:12:34",
                    FluencyConstants.SKILL2_NAME: "Math",
                    FluencyConstants.SKILL2_TOTAL_ITEMS: "100",
                    FluencyConstants.SKILL2_NUMBER_CORRECT: "1",
                    FluencyConstants.SKILL2_TIME: "00:02:01",
                    FluencyConstants.SKILL3_NAME: "",
                    FluencyConstants.SKILL3_TOTAL_ITEMS: "",
                    FluencyConstants.SKILL3_NUMBER_CORRECT: "",
                    FluencyConstants.SKILL3_TIME: "",
                    FluencyConstants.SKILL4_NAME: "",
                    FluencyConstants.SKILL4_TOTAL_ITEMS: "",
                    FluencyConstants.SKILL4_NUMBER_CORRECT: "",
                    FluencyConstants.SKILL4_TIME: "",
                    FluencyConstants.SKILL5_NAME: "",
                    FluencyConstants.SKILL5_TOTAL_ITEMS: "",
                    FluencyConstants.SKILL5_NUMBER_CORRECT: "",
                    FluencyConstants.SKILL5_TIME: "",
                    FluencyConstants.SKILL6_NAME: "",
                    FluencyConstants.SKILL6_TOTAL_ITEMS: "",
                    FluencyConstants.SKILL6_NUMBER_CORRECT: "",
                    FluencyConstants.SKILL6_TIME: "",
                }
            ]
        }
        expected = dict()
        expected[AssessmentType.DIAGNOSTIC] = {}
        expected[AssessmentType.DIAGNOSTIC][Constants.SUBJECT1] = [{
            Constants.STUDENT_DISPLAY_NAME: "Brule, Steve ",
            Constants.STUDENT_GUID: '123',
            Constants.ASMT_GRADE: "03",
            Constants.STAFF_ID: "a0b1c2",
            Constants.ASMT_DATE: "02/02/2015",
            FluencyConstants.SKILL: "Adding transcendental numbers",
            FluencyConstants.ITEMS_CORRECT: "1",
            FluencyConstants.TOTAL_TIME_PER_SKILL: "00:12:34",
            FluencyConstants.SKILL_PERCENT_CORRECT: "50.0",
            FluencyConstants.TOTAL_NUM_SKILL_ITEMS: "2",
        }, {
            Constants.STUDENT_DISPLAY_NAME: "Brule, Steve ",
            Constants.STUDENT_GUID: '123',
            Constants.ASMT_GRADE: "03",
            Constants.STAFF_ID: "a0b1c2",
            Constants.ASMT_DATE: "02/02/2015",
            FluencyConstants.SKILL: "Math",
            FluencyConstants.ITEMS_CORRECT: "1",
            FluencyConstants.TOTAL_TIME_PER_SKILL: "00:02:01",
            FluencyConstants.SKILL_PERCENT_CORRECT: "1.0",
            FluencyConstants.TOTAL_NUM_SKILL_ITEMS: "100",
        }]
        formatted = fluency.format_results(results)
        self.assertDictEqual(
            expected, formatted, "formatted results do not match")

if __name__ == '__main__':
    unittest.main()
