import unittest
from datetime import date
from pyramid import testing
from pyramid.registry import Registry
from reporting.reports.helpers.constants import Constants, AssessmentType
from reporting.reports.diagnostic.helpers.constants import DiagnosticReportType, DiagnosticsTable, ClusterConstants, \
    Constants as DiagnosticConstants
from reporting.tests.reports.base import BaseReportTestCase
from reporting.reports.routes.school_report_route import get_school_report
from reporting.reports.diagnostic.grade_level import GradeLevelReport
from edcore.utils.query import QueryMapKey
from reporting.security.roles.pii import PII  # @UnusedImport


class TestDiagnosticsGradeLevel(BaseReportTestCase):

    def setUp(self):
        reg = Registry()
        reg.settings = {}
        BaseReportTestCase.setUp(self, registry=reg)

    def tearDown(self):
        # reset the registry
        testing.tearDown()

    def test_get_diagnostic_locator_report(self):
        results = get_school_report({
            Constants.STATECODE: 'RI',
            Constants.DISTRICTGUID: 'a',
            Constants.SCHOOLGUID: 'b',
            Constants.YEAR: 2015,
            Constants.SUBJECT: Constants.SUBJECT1,
            Constants.VIEW: DiagnosticReportType.GRADE_LEVEL
        })
        self.assertIsInstance(results, dict)
        self.assertIn(Constants.ASSESSMENTS, results)
        self.assertIn(Constants.SUBJECTS, results)

    def test_diagnostic_locator_report(self):
        params = {
            Constants.STATECODE: 'RI',
            Constants.DISTRICTGUID: 'a',
            Constants.SCHOOLGUID: 'b',
            Constants.YEAR: 2015,
            Constants.SUBJECT: Constants.SUBJECT1,
            Constants.VIEW: DiagnosticReportType.GRADE_LEVEL
        }
        gradelevel = GradeLevelReport(**params)
        results = {
            QueryMapKey(type=Constants.ASSESSMENTS, subject=DiagnosticsTable.MATH_GRADE_LEVEL): [
                {
                    Constants.STUDENT_PARCC_ID: '123',
                    Constants.STUDENT_FIRST_NAME: "Steve",
                    Constants.STUDENT_MIDDLE_NAME: "Dinky",
                    Constants.STUDENT_LAST_NAME: "Brule",
                    Constants.ASMT_GRADE: "03",
                    Constants.STAFF_ID: "a0b1c2",
                    Constants.ASMT_DATE: date(2015, 5, 5),
                    DiagnosticConstants.CLUSTER_GRADE_LEVEL: 5,
                    ClusterConstants.CODE_CLUSTER1: "7.NDP.A",
                    ClusterConstants.PROB_CLUSTER1: 0.45,
                    ClusterConstants.CODE_CLUSTER2: "7.NDP.B",
                    ClusterConstants.PROB_CLUSTER2: 0.55,
                    ClusterConstants.CODE_CLUSTER3: "7.NDP.C",
                    ClusterConstants.PROB_CLUSTER3: 0.65,
                    ClusterConstants.CODE_CLUSTER4: "",
                    ClusterConstants.PROB_CLUSTER4: None,
                    ClusterConstants.CODE_CLUSTER5: "",
                    ClusterConstants.PROB_CLUSTER5: None,
                    ClusterConstants.CODE_CLUSTER6: "",
                    ClusterConstants.PROB_CLUSTER6: None,
                }
            ]
        }
        expected = dict()
        expected[AssessmentType.DIAGNOSTIC] = {}
        expected[AssessmentType.DIAGNOSTIC][Constants.SUBJECT1] = [{
            Constants.STUDENT_GUID: '123',
            Constants.STUDENT_DISPLAY_NAME: "Brule, Steve D.",
            Constants.ASMT_GRADE: "03",
            Constants.STAFF_ID: "a0b1c2",
            Constants.ASMT_DATE: "05/05/2015",
            DiagnosticConstants.CLUSTER_GRADE_LEVEL: 5,
            ClusterConstants.CLUSTERS: [
                {ClusterConstants.PROB_CLUSTER: 0.45, ClusterConstants.CODE_CLUSTER: "7.NDP.A"},
                {ClusterConstants.PROB_CLUSTER: 0.55, ClusterConstants.CODE_CLUSTER: "7.NDP.B"},
                {ClusterConstants.PROB_CLUSTER: 0.65, ClusterConstants.CODE_CLUSTER: "7.NDP.C"},
            ]
        }]
        formatted = gradelevel.format_results(results)
        self.assertDictEqual(expected, formatted, "formatted gradeLevel results do not match")

if __name__ == '__main__':
    unittest.main()
