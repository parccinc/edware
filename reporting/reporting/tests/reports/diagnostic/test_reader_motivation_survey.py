import unittest
from datetime import date
from unittest.mock import patch

from pyramid import testing
from pyramid.registry import Registry
from edcore.tests.utils.unittest_with_edcore_sqlite import UnittestEdcoreDBConnection
from reporting.reports.diagnostic.reader_motivation_survey import ReaderMotivationSurveyReport
from reporting.reports.diagnostic.helpers.constants import DiagnosticReportType, DiagnosticsTable, \
    ReaderMotivationSurveyConstants
from reporting.reports.helpers.constants import Constants, AssessmentType
from reporting.reports.routes.school_report_route import get_school_report
from reporting.tests.reports.base import BaseReportTestCase
from edcore.utils.query import QueryMapKey
from reporting.security.roles.pii import PII  # @UnusedImport


class TestReaderMotivationReport(BaseReportTestCase):

    def setUp(self):
        reg = Registry()
        reg.settings = {}
        self.params = {
            Constants.STATECODE: 'RI',
            Constants.DISTRICTGUID: 'a',
            Constants.SCHOOLGUID: 'b',
            Constants.YEAR: 2015,
            Constants.SUBJECT: Constants.SUBJECT2,
            Constants.VIEW: DiagnosticReportType.READER_MOTIVATION_SURVEY
        }
        BaseReportTestCase.setUp(self, registry=reg)

    def tearDown(self):
        # reset the registry
        testing.tearDown()

    def test_get_reader_motivation_survey_report(self):
        results = get_school_report(self.params)
        self.assertIsInstance(results, dict)
        self.assertIn(Constants.ASSESSMENTS, results)
        self.assertIn(Constants.SUBJECTS, results)

    def test_reader_motivation_survey_format_result(self):
        report = ReaderMotivationSurveyReport(**self.params)
        self.maxDiff = None
        results = {
            QueryMapKey(type=Constants.ASSESSMENTS, subject=DiagnosticsTable.ELA_READER_MOTIVATION): [
                {
                    Constants.STUDENT_FIRST_NAME: "Justine",
                    Constants.STUDENT_LAST_NAME: "Henin",
                    Constants.STUDENT_MIDDLE_NAME: "",
                    Constants.ASMT_GRADE: "03",
                    Constants.STAFF_ID: "a0b1c2",
                    Constants.ASMT_DATE: date(2015, 2, 2),
                    Constants.STUDENT_PARCC_ID: '456',
                    "response1": 'A',
                    "response2": 'A.B',
                    "response3": 'A.B.C',
                    "response4": 'B',
                    "response5": 'A.B.C.D',
                    "response6": 'Z',
                    "response7": 'A.B.C.D.E',
                    "response8": 'P',
                    "response9": 'F',
                    "response10": '',
                    "response11": 'D',
                    "response12": 'Y',
                    "response13": 'T',
                    "response14": 'E',
                    "response15": 'A.B.E',
                    "response16": 'A.B.F',
                    "response17": 'A.B.D',
                    "response18": 'A.B.Z',
                    "response19": 'A.B.G',
                }
            ]
        }
        expected = dict()
        expected[AssessmentType.DIAGNOSTIC] = {}
        expected[AssessmentType.DIAGNOSTIC][Constants.SUBJECT2] = [{
            Constants.STUDENT_DISPLAY_NAME: "Henin, Justine ",
            Constants.ASMT_GRADE: "03",
            Constants.STAFF_ID: "a0b1c2",
            Constants.ASMT_DATE: "02/02/2015",
            Constants.STUDENT_GUID: '456',
            ReaderMotivationSurveyConstants.RESPONSES: [['A'],
                                                        ['A', 'B'],
                                                        ['A', 'B', 'C'],
                                                        ['B'],
                                                        ['A', 'B', 'C', 'D'],
                                                        ['Z'],
                                                        ['A', 'B', 'C', 'D', 'E'],
                                                        ['P'],
                                                        ['F'],
                                                        [''],
                                                        ['D'],
                                                        ['Y'],
                                                        ['T'],
                                                        ['E'],
                                                        ['A', 'B', 'E'],
                                                        ['A', 'B', 'F'],
                                                        ['A', 'B', 'D'],
                                                        ['A', 'B', 'Z'],
                                                        ['A', 'B', 'G']],
        }]
        formatted = report.format_results(results)
        self.assertDictEqual(expected, formatted, "formatted results do not match")

    def test_get_query(self):
        with UnittestEdcoreDBConnection() as conn:
            report = ReaderMotivationSurveyReport(**self.params)
            query = report.get_query(conn.get_table(DiagnosticsTable.ELA_READER_MOTIVATION))
            self.assertIn('resp_dist_id =', str(query))
            self.assertIn('resp_school_id =', str(query))
            self.assertIn('state_code =', str(query))
            self.assertIn('rec_status =', str(query))

    @patch('reporting.reports.diagnostic.reader_motivation_survey.ReaderMotivationSurveyReport.get_query')
    @patch('reporting.reports.base_report.execute_mapped_queries_remotely')
    def test_run_query(self, exec_query_patch, query_patch):
        expected_query = 'reader motivation survey mock query'
        expected_result = [{'survey': 1234}]
        query_patch.return_value = expected_query
        exec_query_patch.return_value = expected_result
        report = ReaderMotivationSurveyReport(**self.params)
        result = report.run_query()
        self.assertEqual(result, expected_result)
        exec_query_patch.assert_called_once_with(
            {
                QueryMapKey(type=Constants.ASSESSMENTS, subject=DiagnosticsTable.ELA_READER_MOTIVATION): {
                    'state_code': 'RI',
                    'query': expected_query}
            }
        )
        exec_query_patch.reset_mock()


if __name__ == '__main__':
    unittest.main()
