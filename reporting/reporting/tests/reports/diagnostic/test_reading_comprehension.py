import unittest
from datetime import date
from unittest.mock import patch
from pyramid import testing
from pyramid.registry import Registry
from edcore.tests.utils.unittest_with_edcore_sqlite import UnittestEdcoreDBConnection
from reporting.reports.diagnostic.reading_comprehension import ReadingComprehensionReport
from reporting.reports.helpers.constants import Constants, AssessmentType
from reporting.reports.diagnostic.helpers.constants import DiagnosticReportType, DiagnosticsTable, \
    ReadingComprehensionConstants, Constants as DiagnosticConstants
from reporting.reports.routes.school_report_route import get_school_report
from reporting.tests.reports.base import BaseReportTestCase
from edcore.utils.query import QueryMapKey
from reporting.security.roles.pii import PII  # @UnusedImport


class TestDiagnosticsReadingComprehensionReport(BaseReportTestCase):

    def setUp(self):
        reg = Registry()
        reg.settings = {}
        self.params = {
            Constants.STATECODE: 'RI',
            Constants.DISTRICTGUID: 'R0001',
            Constants.SCHOOLGUID: 'RN001',
            Constants.YEAR: 2015,
            Constants.SUBJECT: Constants.SUBJECT2,
            Constants.VIEW: DiagnosticReportType.READING_COMPREHENSION
        }
        BaseReportTestCase.setUp(self, registry=reg)

    def tearDown(self):
        # reset the registry
        testing.tearDown()

    def test_get_diagnostic_reading_comprehension_report(self):
        results = get_school_report({
            Constants.STATECODE: 'RI',
            Constants.DISTRICTGUID: 'a',
            Constants.SCHOOLGUID: 'b',
            Constants.YEAR: 2015,
            Constants.SUBJECT: Constants.SUBJECT2,
            Constants.VIEW: DiagnosticReportType.READING_COMPREHENSION
        })
        self.assertIsInstance(results, dict)
        self.assertIn(Constants.ASSESSMENTS, results)
        self.assertIn(Constants.SUBJECTS, results)

    def test_reading_comprehension_format_result(self):
        report = ReadingComprehensionReport(**self.params)
        results = {
            QueryMapKey(type=Constants.ASSESSMENTS, subject=DiagnosticsTable.ELA_READING_COMPREHENSION): [
                {
                    Constants.STUDENT_FIRST_NAME: "Justine",
                    Constants.STUDENT_LAST_NAME: "Henin",
                    Constants.STUDENT_MIDDLE_NAME: "",
                    Constants.STUDENT_PARCC_ID: '455',
                    Constants.ASMT_GRADE: "03",
                    Constants.STAFF_ID: "a0b1c2",
                    DiagnosticConstants.ASMT_SCORE: 50,
                    Constants.ASMT_DATE: date(2015, 2, 2),
                    ReadingComprehensionConstants.IRL: 550,
                    ReadingComprehensionConstants.PASSAGE1_RMM: 2.0,
                    ReadingComprehensionConstants.PASSAGE2_RMM: 0.28,
                    ReadingComprehensionConstants.PASSAGE3_RMM: 1.20,
                    ReadingComprehensionConstants.PASSAGE1_TYPE: "Informational",
                    ReadingComprehensionConstants.PASSAGE2_TYPE: "Literary",
                    ReadingComprehensionConstants.PASSAGE3_TYPE: "Tennis",
                }
            ]
        }
        expected = dict()
        expected[AssessmentType.DIAGNOSTIC] = {}
        expected[AssessmentType.DIAGNOSTIC][Constants.SUBJECT2] = [{
            Constants.STUDENT_DISPLAY_NAME: "Henin, Justine ",
            Constants.ASMT_GRADE: "03",
            Constants.STAFF_ID: "a0b1c2",
            Constants.ASMT_DATE: "02/02/2015",
            Constants.STUDENT_GUID: '455',
            Constants.SCORE: 50,
            ReadingComprehensionConstants.IRL: 550,
            ReadingComprehensionConstants.PASSAGE_INFO: [
                {
                    ReadingComprehensionConstants.PASSAGE_TYPE: "Informational",
                    ReadingComprehensionConstants.PASSAGE_RMM: 2.0,
                },
                {
                    ReadingComprehensionConstants.PASSAGE_TYPE: "Literary",
                    ReadingComprehensionConstants.PASSAGE_RMM: 0.3,
                },
                {
                    ReadingComprehensionConstants.PASSAGE_TYPE: "Tennis",
                    ReadingComprehensionConstants.PASSAGE_RMM: 1.2,
                },
            ],
        }]
        formatted = report.format_results(results)
        self.assertDictEqual(expected, formatted, "formatted results do not match")

    def test_get_query(self):
        with UnittestEdcoreDBConnection() as conn:
            report = ReadingComprehensionReport(**self.params)
            query = report.get_query(conn.get_table(DiagnosticsTable.ELA_READING_COMPREHENSION))
            self.assertIn('resp_dist_id =', str(query))
            self.assertIn('resp_school_id =', str(query))
            self.assertIn('state_code =', str(query))
            self.assertIn('rec_status =', str(query))

    @patch('reporting.reports.diagnostic.reading_comprehension.ReadingComprehensionReport.get_query')
    @patch('reporting.reports.base_report.execute_mapped_queries_remotely')
    def test_run_query(self, exec_query_patch, query_patch):
        comprehension_expected_result = [{'comprehension': 1234}]
        query_patch.return_value = 'comprehension query'
        exec_query_patch.return_value = comprehension_expected_result
        report = ReadingComprehensionReport(**self.params)
        result = report.run_query()
        self.assertEqual(result, comprehension_expected_result)
        exec_query_patch.assert_called_once_with(
            {
                QueryMapKey(type='assessments', subject='rpt_ela_comp'): {'state_code': 'RI',
                                                                          'query': 'comprehension query'}
            }
        )
        exec_query_patch.reset_mock()

    def test_get_report_with_correct_params(self):
        results = get_school_report({
            Constants.STATECODE: 'RI',
            Constants.DISTRICTGUID: 'R0003',
            Constants.SCHOOLGUID: 'RP001',
            Constants.YEAR: 2015,
            Constants.SUBJECT: Constants.SUBJECT2,
            Constants.VIEW: DiagnosticReportType.READING_COMPREHENSION,
        })
        self.assertIsInstance(results, dict)
        self.assertIn(Constants.ASSESSMENTS, results)
        self.assertIn(Constants.SUBJECTS, results)
        expected = [
            {'asmt_grade': '04',
             'asmt_date': '03/08/2015',
             'student_display_name': 'Forst, Jacquline Z.',
             'student_guid': 'hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ',
             'irl': 2.6,
             'passage_info': [
                 {
                     'passage_type': 'Literary',
                     'passage_rmm': 8.0,
                 }
             ],
             'staff_id': '3c6b9327b0484f5f84741b204a6c9f02208bc1',
             'score': 509},
            {'asmt_grade': '06',
             'asmt_date': '11/18/2014',
             'student_display_name': 'Gatti, Shane K.',
             'student_guid': "6xduCZW2oSUGSLzpiFpJQQK8YSAzmBrMjAh4WEkn",
             'irl': 2.8,
             'passage_info': [
                 {
                     'passage_type': 'Informational',
                     'passage_rmm': 3.9,
                 },
                 {
                     'passage_type': 'Literary',
                     'passage_rmm': 3.5,
                 }
             ],
             'staff_id': '3c6b9327b0484f5f84741b204a6c9f02208bc1',
             'score': 454},
            {'asmt_grade': '06',
             'asmt_date': '11/18/2014',
             'student_display_name': 'Mccreery, Sandy D.',
             'student_guid': "wUfuK1C1vpevOq8cV6oqQxNNwJV1TbzeraL8fC4v",
             'irl': 9.3,
             'passage_info': [
                 {
                     'passage_type': 'Literary',
                     'passage_rmm': 1.5,
                 },
                 {
                     'passage_type': 'Literary',
                     'passage_rmm': 2.5,
                 }
             ],
             'staff_id': '3c6b9327b0484f5f84741b204a6c9f02208bc1',
             'score': 319},
            {'asmt_date': '04/01/2015',
             'score': 443,
             'student_display_name': 'Mccreery, Zofia K.',
             'student_guid': "zxITiM66QUVOcTyhRaooB0UIOWemGFP8eQE7xPSx",
             'asmt_grade': '03',
             'staff_id': '5f3bcce2cac34dd38578b9d423801dc3b5caf6',
             'irl': 7.4,
             'passage_info': [
                 {
                     'passage_type': 'Literary',
                     'passage_rmm': 3.8,
                 }
             ]}
        ]
        self.maxDiff = None
        self.assertEqual(expected, results[Constants.ASSESSMENTS][AssessmentType.DIAGNOSTIC][Constants.SUBJECT2])


if __name__ == '__main__':
    unittest.main()
