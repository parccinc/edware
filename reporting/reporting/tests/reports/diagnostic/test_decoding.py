import unittest
from datetime import date
from pyramid import testing
from pyramid.registry import Registry
from reporting.reports.helpers.constants import Constants, AssessmentType
from reporting.reports.diagnostic.helpers.constants import DiagnosticReportType, DecodingConstants
from reporting.tests.reports.base import BaseReportTestCase
from reporting.reports.routes.school_report_route import get_school_report
from reporting.reports.diagnostic.decoding import DecodingReport
from edcore.utils.query import QueryMapKey
from decimal import Decimal
from reporting.security.roles.pii import PII  # @UnusedImport


class TestDiagnosticsDecoding(BaseReportTestCase):

    def setUp(self):
        reg = Registry()
        reg.settings = {}

        BaseReportTestCase.setUp(self, registry=reg)

    def tearDown(self):
        # reset the registry
        testing.tearDown()

    def test_get_diagnostic_decoding_report(self):
        results = get_school_report({
            Constants.STATECODE: 'RI',
            Constants.DISTRICTGUID: 'a',
            Constants.SCHOOLGUID: 'b',
            Constants.YEAR: 2015,
            Constants.SUBJECT: Constants.SUBJECT2,
            Constants.VIEW: DiagnosticReportType.DECODING
        })
        self.assertIsInstance(results, dict)
        self.assertIn(Constants.ASSESSMENTS, results)
        self.assertIn(Constants.SUBJECTS, results)

    def test_diagnostic_decoding_report(self):
        params = {
            Constants.STATECODE: 'RI',
            Constants.DISTRICTGUID: 'a',
            Constants.SCHOOLGUID: 'b',
            Constants.YEAR: 2015,
            Constants.SUBJECT: Constants.SUBJECT2,
            Constants.VIEW: DiagnosticReportType.DECODING
        }
        decoding = DecodingReport(**params)
        results = {
            QueryMapKey(type='assessments', subject='rpt_ela_decod'): [
                {
                    Constants.STUDENT_PARCC_ID: '123',
                    Constants.STUDENT_FIRST_NAME: "Steve",
                    Constants.STUDENT_MIDDLE_NAME: "",
                    Constants.STUDENT_LAST_NAME: "Brule",
                    Constants.ASMT_GRADE: "03",
                    Constants.STAFF_ID: "a0b1c2",
                    Constants.ASMT_DATE: date(2015, 2, 2),
                    DecodingConstants.CVC: Decimal(0.5),
                    DecodingConstants.BLEND: Decimal(0.5),
                    DecodingConstants.COMPLEX_VOWELS: Decimal(0.6),
                    DecodingConstants.COMPLEX_CONSONANT: Decimal(0.7),
                    DecodingConstants.MULTI_SY1: Decimal(0.7),
                    DecodingConstants.MULTI_SY2: Decimal(0.7),
                }
            ]
        }
        expected = dict()
        expected[AssessmentType.DIAGNOSTIC] = {}
        expected = {
            Constants.STUDENT_GUID: '123',
            Constants.STUDENT_DISPLAY_NAME: "Brule, Steve ",
            Constants.ASMT_GRADE: "03",
            Constants.STAFF_ID: "a0b1c2",
            Constants.ASMT_DATE: "02/02/2015",
            DecodingConstants.CVC: 0.5,
            DecodingConstants.BLEND: 0.5,
            DecodingConstants.COMPLEX_VOWELS: 0.6,
            DecodingConstants.COMPLEX_CONSONANT: 0.7,
            DecodingConstants.MULTI_SY1: 0.7,
            DecodingConstants.MULTI_SY2: 0.7,
        }
        formatted = decoding.format_results(results)
        self.assertDictEqual(expected, formatted[AssessmentType.DIAGNOSTIC][Constants.SUBJECT2][0], "formatted decoding results do not match")

    def test_get_diagnostic_decoding_report_with_correct_params(self):
        results = get_school_report({
            Constants.STATECODE: 'RI',
            Constants.DISTRICTGUID: 'R0003',
            Constants.SCHOOLGUID: 'RP001',
            Constants.YEAR: 2015,
            Constants.SUBJECT: Constants.SUBJECT2,
            Constants.VIEW: DiagnosticReportType.DECODING,
        })
        self.assertIsInstance(results, dict)
        self.assertIn(Constants.ASSESSMENTS, results)
        self.assertIn(Constants.SUBJECTS, results)
        actual = [
            {'asmt_grade': '04',
             'asmt_date': '03/08/2015',
             'p1_cvc': 0.87,
             'p2_blend': 0.45,
             'p3_cvow': 0.22,
             'p4_ccons': 0.05,
             'p5_msyl1': 0.54,
             'p6_msyl2': 0.82,
             'staff_id': '3c6b9327b0484f5f84741b204a6c9f02208bc1',
             'student_display_name': 'Forst, Jacquline Z.',
             'student_guid': 'hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ'},
            {'asmt_grade': '06',
             'asmt_date': '11/18/2014',
             'p1_cvc': 0.51,
             'p2_blend': 0.92,
             'p3_cvow': 0.77,
             'p4_ccons': 0.46,
             'p5_msyl1': 0.82,
             'p6_msyl2': 0.63,
             'staff_id': '3c6b9327b0484f5f84741b204a6c9f02208bc1',
             'student_display_name': 'Gatti, Shane K.',
             'student_guid': '6xduCZW2oSUGSLzpiFpJQQK8YSAzmBrMjAh4WEkn'},
            {'asmt_grade': '06',
             'asmt_date': '11/18/2014',
             'p1_cvc': 0.27,
             'p2_blend': 0.52,
             'p3_cvow': 0.92,
             'p4_ccons': 0.81,
             'p5_msyl1': 0.65,
             'p6_msyl2': 0.49,
             'staff_id': '3c6b9327b0484f5f84741b204a6c9f02208bc1',
             'student_display_name': 'Mccreery, Sandy D.',
             'student_guid': 'wUfuK1C1vpevOq8cV6oqQxNNwJV1TbzeraL8fC4v'},
            {'asmt_grade': '03',
             'asmt_date': '04/01/2015',
             'p1_cvc': 0.48,
             'p2_blend': 0.63,
             'p3_cvow': 0.98,
             'p4_ccons': 0.76,
             'p5_msyl1': 0.25,
             'p6_msyl2': 0.04,
             'staff_id': '5f3bcce2cac34dd38578b9d423801dc3b5caf6',
             'student_display_name': 'Mccreery, Zofia K.',
             'student_guid': 'zxITiM66QUVOcTyhRaooB0UIOWemGFP8eQE7xPSx'}
        ]
        for i in range(0, 4):
            self.assertDictEqual(actual[i], results[Constants.ASSESSMENTS][AssessmentType.DIAGNOSTIC][Constants.SUBJECT2][i])


if __name__ == '__main__':
    unittest.main()
