import datetime
from datetime import date
from decimal import Decimal
import unittest
from unittest.mock import patch

from pyramid.testing import DummyRequest
from pyramid import testing
from pyramid.security import Allow
from beaker.cache import CacheManager
from beaker.util import parse_cache_config_options
from parcc_common.security.constants import RolesConstants
from edauth.tests.test_helper.create_session import create_test_session
from edcore.security.tenant import set_tenant_map
from edcore.tests.utils.unittest_with_edcore_sqlite import get_unittest_tenant_name, \
    UnittestEdcoreDBConnection
from reporting.reports.diagnostic.student import StudentReport
from reporting.reports.helpers.constants import Constants
from reporting.reports.routes.student_report_route import get_diagnostic_student_report
from reporting.reports.helpers.constants import AssessmentType
from reporting.reports.diagnostic.helpers.constants import DiagnosticsTable, ReadingComprehensionConstants, \
    DecodingConstants, ReadingFluencyConstants, WritingConstants, Constants as DiagnosticConstants, ClusterConstants, \
    FluencyConstants
import edauth
from edapi.httpexceptions import EdApiHTTPPreconditionFailed
from edextract.celery import setup_celery as setup_extract_celery
from reporting.tests.reports.base import BaseReportTestCase
from edcore.utils.query import QueryMapKey
from reporting.security.roles.pii import PII  # @UnusedImport


class TestDiagnosticStudentReport(BaseReportTestCase):

    def setUp(self):
        cache_opts = {
            'cache.type': 'memory',
            'cache.regions': 'public.data,public.filtered_data,public.shortlived'
        }

        CacheManager(**parse_cache_config_options(cache_opts))

        self.__request = DummyRequest()
        # Must set hook_zca to false to work with uniittest_with_sqlite
        self.__config = testing.setUp(request=self.__request, hook_zca=False)
        defined_roles = [(Allow, RolesConstants.PII, ('view', 'logout'))]
        edauth.set_roles(defined_roles)
        set_tenant_map({get_unittest_tenant_name(): 'RI'})
        # Set up context security
        dummy_session = create_test_session([RolesConstants.PII])
        self.__config.testing_securitypolicy(dummy_session.get_user())

        settings = {'extract.celery.CELERY_ALWAYS_EAGER': True}
        setup_extract_celery(settings)

        self.subj1_items = [
            {'name': 'Math_1', 'item_max_points': '5'},
            {'name': 'Math_2', 'item_max_points': '5'}
        ]
        self.subj2_items = [
            {'name': 'ELA_1', 'item_max_points': '5'},
            {'name': 'ELA_2', 'item_max_points': '5'}
        ]

        self.__valid_ela_params = {
            'stateCode': 'RI',
            'districtGuid': 'R0001',
            'schoolGuid': 'RN001',
            'gradeCourse': '09',
            'studentGuid': 'EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q',
            'year': 2015,
            'subject': 'subject2',
            'asmtType': AssessmentType.DIAGNOSTIC,
        }
        self.__valid_math_params = {
            'stateCode': 'RI',
            'districtGuid': 'R0001',
            'schoolGuid': 'RN001',
            'gradeCourse': '03',
            'studentGuid': 'EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q',
            'year': 2015,
            'subject': 'subject1',
            'asmtType': AssessmentType.DIAGNOSTIC,
        }
        self.dummy_results_math = {
            QueryMapKey(type='assessments', subject=DiagnosticsTable.MATH_LOCATOR): [
                {
                    Constants.STUDENT_PARCC_ID: '123',
                    Constants.STUDENT_FIRST_NAME: "Steve",
                    Constants.STUDENT_MIDDLE_NAME: "",
                    Constants.STUDENT_LAST_NAME: "Brule",
                    Constants.STUDENT_DOB: date(2010, 1, 1),
                    Constants.ASMT_GRADE: "03",
                    Constants.STAFF_ID: "a0b1c2",
                    Constants.ASMT_DATE: date(2015, 2, 2),
                    DiagnosticConstants.ASMT_SCORE: 412,
                    DiagnosticConstants.SUGGESTED_GRADE_LEVEL: 5,
                }
            ],
            QueryMapKey(type='assessments', subject='rpt_math_progress'): [
                {
                    Constants.STUDENT_PARCC_ID: '123',
                    Constants.STUDENT_FIRST_NAME: "Steve",
                    Constants.STUDENT_MIDDLE_NAME: "",
                    Constants.STUDENT_LAST_NAME: "Brule",
                    Constants.STUDENT_DOB: date(2010, 1, 1),
                    Constants.ASMT_GRADE: "03",
                    Constants.STAFF_ID: "a0b1c2",
                    Constants.ASMT_DATE: date(2015, 2, 2),
                    DiagnosticConstants.PROGRESSION: "NF",
                    ClusterConstants.CODE_CLUSTER1: "7.NDP.A",
                    ClusterConstants.PROB_CLUSTER1: 0.45,
                    ClusterConstants.CODE_CLUSTER2: "7.NDP.B",
                    ClusterConstants.PROB_CLUSTER2: 0.55,
                    ClusterConstants.CODE_CLUSTER3: "7.NDP.C",
                    ClusterConstants.PROB_CLUSTER3: 0.65,
                    ClusterConstants.CODE_CLUSTER4: "",
                    ClusterConstants.PROB_CLUSTER4: None,
                    ClusterConstants.CODE_CLUSTER5: "",
                    ClusterConstants.PROB_CLUSTER5: None,
                    ClusterConstants.CODE_CLUSTER6: "",
                    ClusterConstants.PROB_CLUSTER6: None,
                }
            ],
            QueryMapKey(type=Constants.ASSESSMENTS, subject=DiagnosticsTable.MATH_GRADE_LEVEL): [
                {
                    Constants.STUDENT_PARCC_ID: '123',
                    Constants.STUDENT_FIRST_NAME: "Steve",
                    Constants.STUDENT_MIDDLE_NAME: "",
                    Constants.STUDENT_LAST_NAME: "Brule",
                    Constants.STUDENT_DOB: date(2010, 1, 1),
                    Constants.ASMT_GRADE: "03",
                    Constants.STAFF_ID: "a0b1c2",
                    Constants.ASMT_DATE: date(2015, 5, 5),
                    DiagnosticConstants.CLUSTER_GRADE_LEVEL: 5,
                    ClusterConstants.CODE_CLUSTER1: "7.NDP.A",
                    ClusterConstants.PROB_CLUSTER1: 0.45,
                    ClusterConstants.CODE_CLUSTER2: "7.NDP.B",
                    ClusterConstants.PROB_CLUSTER2: 0.55,
                    ClusterConstants.CODE_CLUSTER3: "7.NDP.C",
                    ClusterConstants.PROB_CLUSTER3: 0.65,
                    ClusterConstants.CODE_CLUSTER4: "",
                    ClusterConstants.PROB_CLUSTER4: None,
                    ClusterConstants.CODE_CLUSTER5: "",
                    ClusterConstants.PROB_CLUSTER5: None,
                    ClusterConstants.CODE_CLUSTER6: "",
                    ClusterConstants.PROB_CLUSTER6: None,
                }
            ],
            QueryMapKey(type='assessments', subject='rpt_math_cluster'): [
                {
                    Constants.STUDENT_PARCC_ID: '123',
                    Constants.STUDENT_FIRST_NAME: "Steve",
                    Constants.STUDENT_MIDDLE_NAME: "",
                    Constants.STUDENT_LAST_NAME: "Brule",
                    Constants.STUDENT_DOB: date(2010, 1, 1),
                    Constants.ASMT_GRADE: "03",
                    Constants.STAFF_ID: "a0b1c2",
                    Constants.ASMT_DATE: date(2015, 2, 2),
                    ClusterConstants.CODE_CLUSTER: "7.NDP.A",
                    ClusterConstants.PROB_CLUSTER: 0.45,
                }
            ],
            QueryMapKey(type='assessments', subject='rpt_math_flu'): [
                {
                    Constants.STUDENT_PARCC_ID: '123',
                    Constants.STUDENT_FIRST_NAME: "Steve",
                    Constants.STUDENT_MIDDLE_NAME: "",
                    Constants.STUDENT_LAST_NAME: "Brule",
                    Constants.STUDENT_DOB: date(2010, 1, 1),
                    Constants.ASMT_GRADE: "03",
                    Constants.STAFF_ID: "a0b1c2",
                    Constants.ASMT_DATE: date(2015, 2, 2),
                    FluencyConstants.SKILL1_NAME: "Adding transcendental numbers",
                    FluencyConstants.SKILL1_TOTAL_ITEMS: "2",
                    FluencyConstants.SKILL1_NUMBER_CORRECT: "1",
                    FluencyConstants.SKILL1_TIME: "00:12:34",
                    FluencyConstants.SKILL2_NAME: "Math",
                    FluencyConstants.SKILL2_TOTAL_ITEMS: "100",
                    FluencyConstants.SKILL2_NUMBER_CORRECT: "1",
                    FluencyConstants.SKILL2_TIME: "00:02:01",
                    FluencyConstants.SKILL3_NAME: "",
                    FluencyConstants.SKILL3_TOTAL_ITEMS: "",
                    FluencyConstants.SKILL3_NUMBER_CORRECT: "",
                    FluencyConstants.SKILL3_TIME: "",
                    FluencyConstants.SKILL4_NAME: "",
                    FluencyConstants.SKILL4_TOTAL_ITEMS: "",
                    FluencyConstants.SKILL4_NUMBER_CORRECT: "",
                    FluencyConstants.SKILL4_TIME: "",
                    FluencyConstants.SKILL5_NAME: "",
                    FluencyConstants.SKILL5_TOTAL_ITEMS: "",
                    FluencyConstants.SKILL5_NUMBER_CORRECT: "",
                    FluencyConstants.SKILL5_TIME: "",
                    FluencyConstants.SKILL6_NAME: "",
                    FluencyConstants.SKILL6_TOTAL_ITEMS: "",
                    FluencyConstants.SKILL6_NUMBER_CORRECT: "",
                    FluencyConstants.SKILL6_TIME: "",
                }
            ],
        }

        self.dummy_results_ela = {
            QueryMapKey(type=Constants.ASSESSMENTS, subject=DiagnosticsTable.ELA_VOCABULARY): [
                {Constants.STUDENT_PARCC_ID: 'hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ',
                 Constants.STUDENT_MIDDLE_NAME: 'Zofia',
                 Constants.STUDENT_FIRST_NAME: 'Jacquline',
                 Constants.STUDENT_LAST_NAME: 'Forst',
                 Constants.STUDENT_DOB: date(2010, 1, 1),
                 Constants.ASMT_GRADE: '3',
                 Constants.STAFF_ID: 'a0b1c2d3e4f5g6h7i8j9k0l1m2n3opqrstuvwx',
                 Constants.ASMT_DATE: datetime.date(2015, 5, 3),
                 DiagnosticConstants.ASMT_SCORE: Decimal('509')}],
            QueryMapKey(type=Constants.ASSESSMENTS, subject=DiagnosticsTable.ELA_WRITING): [
                {Constants.STUDENT_PARCC_ID: 'hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ',
                 Constants.STUDENT_MIDDLE_NAME: 'Zofia',
                 Constants.STUDENT_FIRST_NAME: 'Jacquline',
                 Constants.STUDENT_LAST_NAME: 'Forst',
                 Constants.STUDENT_DOB: date(2010, 1, 1),
                 Constants.ASMT_GRADE: '3',
                 Constants.STAFF_ID: 'a0b1c2d3e4f5g6h7i8j9k0l1m2n3opqrstuvwx',
                 Constants.ASMT_DATE: datetime.date(2015, 9, 5),
                 WritingConstants.SCORE_RES: Decimal('2'),
                 WritingConstants.SCORE_LIT: Decimal('3'),
                 WritingConstants.SCORE_NAR: Decimal('2')}],
            QueryMapKey(type=Constants.ASSESSMENTS,
                        subject=DiagnosticsTable.ELA_READING_FLUENCY): [
                {Constants.STUDENT_PARCC_ID: 'hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ',
                 Constants.STUDENT_MIDDLE_NAME: 'Zofia',
                 Constants.STUDENT_FIRST_NAME: 'Jacquline',
                 Constants.STUDENT_LAST_NAME: 'Forst',
                 Constants.STUDENT_DOB: date(2010, 1, 1),
                 Constants.ASMT_GRADE: '3',
                 Constants.STAFF_ID: 'a0b1c2d3e4f5g6h7i8j9k0l1m2n3opqrstuvwx',
                 Constants.ASMT_DATE: datetime.date(2015, 9, 6),
                 ReadingFluencyConstants.WPM: 45,
                 ReadingFluencyConstants.WCPM: 35,
                 ReadingFluencyConstants.ACCURACY: 80,
                 ReadingFluencyConstants.EXPRESS: 1,
                 ReadingFluencyConstants.PASSAGE1_TYPE: 'Literary',
                 ReadingFluencyConstants.PASSAGE2_TYPE: 'Informational',
                 ReadingFluencyConstants.PASSAGE3_TYPE: 'Informational'}],
            QueryMapKey(type=Constants.ASSESSMENTS,
                        subject=DiagnosticsTable.ELA_READING_COMPREHENSION): [
                {Constants.STUDENT_PARCC_ID: 'hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ',
                 Constants.STUDENT_MIDDLE_NAME: 'Zofia',
                 Constants.STUDENT_FIRST_NAME: 'Jacquline',
                 Constants.STUDENT_LAST_NAME: 'Forst',
                 Constants.STUDENT_DOB: date(2010, 1, 1),
                 Constants.ASMT_GRADE: '3',
                 Constants.STAFF_ID: 'a0b1c2d3e4f5g6h7i8j9k0l1m2n3opqrstuvwx',
                 Constants.ASMT_DATE: datetime.date(2015, 9, 7),
                 DiagnosticConstants.ASMT_SCORE: Decimal('509'),
                 ReadingComprehensionConstants.IRL: 8.00,
                 ReadingComprehensionConstants.PASSAGE1_TYPE: 'Literary',
                 ReadingComprehensionConstants.PASSAGE2_TYPE: 'Informational',
                 ReadingComprehensionConstants.PASSAGE3_TYPE: 'Informational',
                 ReadingComprehensionConstants.PASSAGE1_RMM: 4.20,
                 ReadingComprehensionConstants.PASSAGE2_RMM: 5.60,
                 ReadingComprehensionConstants.PASSAGE3_RMM: 4.30}],
            QueryMapKey(type=Constants.ASSESSMENTS, subject=DiagnosticsTable.ELA_DECODE): [
                {Constants.STUDENT_PARCC_ID: 'hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ',
                 Constants.STUDENT_MIDDLE_NAME: 'Zofia',
                 Constants.STUDENT_FIRST_NAME: 'Jacquline',
                 Constants.STUDENT_LAST_NAME: 'Forst',
                 Constants.STUDENT_DOB: date(2010, 1, 1),
                 Constants.ASMT_GRADE: '3',
                 Constants.STAFF_ID: 'a0b1c2d3e4f5g6h7i8j9k0l1m2n3opqrstuvwx',
                 Constants.ASMT_DATE: datetime.date(2015, 9, 8),
                 DecodingConstants.CVC: Decimal(0.5),
                 DecodingConstants.BLEND: Decimal(0.5),
                 DecodingConstants.COMPLEX_VOWELS: Decimal(0.6),
                 DecodingConstants.COMPLEX_CONSONANT: Decimal(0.7),
                 DecodingConstants.MULTI_SY1: Decimal(0.7),
                 DecodingConstants.MULTI_SY2: Decimal(0.7)}]
        }

    def test_student_report_missing_params(self):
        self.__request.method = 'POST'
        self.__request.json_body = {
            'stateCode': 'RI',
            'asmtType': 'DIAGNOSTIC',
        }
        self.assertRaises(EdApiHTTPPreconditionFailed, None, get_diagnostic_student_report, self.__request)

    def test_student_report_valid_params(self):
        self.__request.method = 'POST'
        self.__request.json_body = self.__valid_ela_params
        actual = get_diagnostic_student_report(self.__valid_ela_params)
        self.assertIsNotNone(actual)
        self.assertIn('user_info', actual, "response should contain user information")
        self.assertIn('context', actual, "response should contain context information")
        self.assertIn('subjects', actual, "response should contain subject information")

    def test_get_results_for_ela(self):
        actual = StudentReport(**self.__valid_ela_params).get_results()
        self.assertIsNotNone(actual)

    def test_get_results_for_math(self):
        actual = StudentReport(**self.__valid_math_params).get_results()
        self.assertIsNotNone(actual)

    @patch('reporting.reports.helpers.breadcrumbs.execute_query_remotely')
    def test_get_breadcrumbs(self, exec_query_patch):
        exec_query_patch.return_value = [{'state_code': 'RI', 'district_name': 'Providence', 'resp_school_name': 'Alvarez High School', 'student_first_name': 'Darci', 'student_last_name': 'Forst', 'student_dob': date(2010, 1, 1)}]
        actual = StudentReport(**self.__valid_ela_params).get_breadcrumbs_context()
        items = actual['items']
        self.assertIsNotNone(items)
        self.assertEqual(len(items), 6, "breadcrumb must contain 6 levels")
        # test student's name and id because that's the information ISR care about
        self.assertEqual(items[5]['type'], 'student', "last level of breadcrumb must be student")
        self.assertEqual(items[5]['name'], 'Darci Forst', "breadcrumb must contain student name")
        exec_query_patch.reset_mock()

    def test_get_query_for_ela(self):
        test_report = StudentReport(**self.__valid_ela_params)
        with UnittestEdcoreDBConnection() as conn:
            for table in (
                    conn.get_table(DiagnosticsTable.ELA_VOCABULARY),
                    conn.get_table(DiagnosticsTable.ELA_WRITING),
                    conn.get_table(DiagnosticsTable.ELA_READING_COMPREHENSION),
                    conn.get_table(DiagnosticsTable.ELA_READING_FLUENCY),
                    conn.get_table(DiagnosticsTable.ELA_DECODE),
            ):
                actual = test_report.get_query(table)
                self.assertIsNotNone(actual, "should return some query string")
                self.assertEqual(type(actual), str, "query must be string")

    def test_get_query_for_math(self):
        test_report = StudentReport(**self.__valid_math_params)
        with UnittestEdcoreDBConnection() as conn:
            for table in (
                    conn.get_table(DiagnosticsTable.MATH_FLUENCY),
                    conn.get_table(DiagnosticsTable.MATH_PROGRESSION),
                    conn.get_table(DiagnosticsTable.MATH_LOCATOR),
                    conn.get_table(DiagnosticsTable.MATH_GRADE_LEVEL),
                    conn.get_table(DiagnosticsTable.MATH_CLUSTER),
            ):
                actual = test_report.get_query(table)
                self.assertIsNotNone(actual, "should return some query string")
                self.assertEqual(type(actual), str, "query must be string")

    def test_run_query_for_ela(self):
        test_report = StudentReport(**self.__valid_ela_params)
        actual = test_report.run_query()
        self.assertTrue(actual, "query should return some student data")

    def test_run_query_for_math(self):
        test_report = StudentReport(**self.__valid_math_params)
        actual = test_report.run_query()
        self.assertTrue(actual, "query should return some student data")

    def test_format_results_for_ela(self):
        test_report = StudentReport(**self.__valid_ela_params)
        dummy_results = dict(self.dummy_results_ela)
        actual = test_report.format_results(dummy_results)
        self.assertIsNotNone(actual, "should return something")
        self.assertIn(AssessmentType.DIAGNOSTIC, actual)
        self.assertIn('subject2', actual[AssessmentType.DIAGNOSTIC])
        expected = {
            'comp': [{'asmt_date': '09/07/2015', 'asmt_grade': '3', 'irl': 8.0,
                      'student_display_name': 'Forst, Jacquline Z.',
                      'student_guid': 'hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ',
                      'staff_id': 'a0b1c2d3e4f5g6h7i8j9k0l1m2n3opqrstuvwx', 'score': 509,
                      'passage_info': [{'passage_rmm': 4.2, 'passage_type': 'Literary'},
                                       {'passage_rmm': 5.6, 'passage_type': 'Informational'},
                                       {'passage_rmm': 4.3, 'passage_type': 'Informational'}]}],
            'decod': [
                {'student_display_name': 'Forst, Jacquline Z.', 'asmt_grade': '3', 'p2_blend': 0.5, 'p6_msyl2': 0.7,
                 'p5_msyl1': 0.7, 'student_guid': 'hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ',
                 'staff_id': 'a0b1c2d3e4f5g6h7i8j9k0l1m2n3opqrstuvwx', 'p3_cvow': 0.6, 'p4_ccons': 0.7,
                 'asmt_date': '09/08/2015', 'p1_cvc': 0.5}],
            'read_flu': [{'asmt_date': '09/06/2015', 'accuracy': 80, 'wcpm': 35, 'asmt_grade': '3', 'wpm': 45,
                          'student_display_name': 'Forst, Jacquline Z.',
                          'staff_id': 'a0b1c2d3e4f5g6h7i8j9k0l1m2n3opqrstuvwx',
                          'student_guid': 'hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ',
                          'passage_info': [{'passage_type': 'Literary'}, {'passage_type': 'Informational'},
                                           {'passage_type': 'Informational'}], 'express': 1}],
            'vocab': [{'score': 509, 'student_display_name': 'Forst, Jacquline Z.',
                       'staff_id': 'a0b1c2d3e4f5g6h7i8j9k0l1m2n3opqrstuvwx',
                       'student_guid': 'hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ', 'asmt_grade': '3',
                       'asmt_date': '05/03/2015'}],
            'writing': [
                {'student_display_name': 'Forst, Jacquline Z.', 'staff_id': 'a0b1c2d3e4f5g6h7i8j9k0l1m2n3opqrstuvwx',
                 'student_guid': 'hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ', 'asmt_grade': '3',
                 'asmt_date': '09/05/2015', 'score_nar': 2, 'score_res': 2, 'score_lit': 3}],
        }
        self.maxDiff = None
        self.assertDictEqual(expected, actual[AssessmentType.DIAGNOSTIC]['subject2'])

    def test_format_results_for_math(self):
        test_report = StudentReport(**self.__valid_math_params)
        dummy_results = dict(self.dummy_results_math)
        actual = test_report.format_results(dummy_results)
        self.assertIsNotNone(actual, "should return something")
        self.assertIn(AssessmentType.DIAGNOSTIC, actual)
        self.assertIn('subject1', actual[AssessmentType.DIAGNOSTIC])
        expected = {
            'cluster': [{'asmt_date': '02/02/2015',
                         'asmt_grade': '03',
                         'clusters': [{'code_cluster': '7.NDP.A',
                                       'prob_cluster': 0.45}],
                         'staff_id': 'a0b1c2',
                         'student_display_name': 'Brule, Steve ',
                         'student_guid': '123'}],
            'flu': [{'asmt_date': '02/02/2015',
                     'asmt_grade': '03',
                     'items_correct': '1',
                     'percent_correct': '50.0',
                     'skill': 'Adding transcendental numbers',
                     'staff_id': 'a0b1c2',
                     'student_display_name': 'Brule, Steve ',
                     'student_guid': '123',
                     'total_num_skill_items': '2',
                     'total_time_per_skill': '00:12:34'},
                    {'asmt_date': '02/02/2015',
                     'asmt_grade': '03',
                     'items_correct': '1',
                     'percent_correct': '1.0',
                     'skill': 'Math',
                     'staff_id': 'a0b1c2',
                     'student_display_name': 'Brule, Steve ',
                     'student_guid': '123',
                     'total_num_skill_items': '100',
                     'total_time_per_skill': '00:02:01'}],
            'grade_level': [{'asmt_date': '05/05/2015',
                             'asmt_grade': '03',
                             'cluster_grade_level': 5,
                             'clusters': [{'code_cluster': '7.NDP.A',
                                           'prob_cluster': 0.45},
                                          {'code_cluster': '7.NDP.B',
                                           'prob_cluster': 0.55},
                                          {'code_cluster': '7.NDP.C',
                                           'prob_cluster': 0.65}],
                             'staff_id': 'a0b1c2',
                             'student_display_name': 'Brule, Steve ',
                             'student_guid': '123'}],
            'locator': [{'asmt_date': '02/02/2015',
                         'asmt_grade': '03',
                         'score': 412,
                         'staff_id': 'a0b1c2',
                         'student_display_name': 'Brule, Steve ',
                         'student_guid': '123',
                         'suggested_grade_level': 5}],
            'progress': [{'asmt_date': '02/02/2015',
                          'asmt_grade': '03',
                          'clusters': [{'code_cluster': '7.NDP.A',
                                        'prob_cluster': 0.45},
                                       {'code_cluster': '7.NDP.B',
                                        'prob_cluster': 0.55},
                                       {'code_cluster': '7.NDP.C',
                                        'prob_cluster': 0.65}],
                          'progression': 'NF',
                          'staff_id': 'a0b1c2',
                          'student_display_name': 'Brule, Steve ',
                          'student_guid': '123'}]
        }
        self.maxDiff = None
        self.assertDictEqual(expected, actual[AssessmentType.DIAGNOSTIC]['subject1'])

if __name__ == '__main__':
    unittest.main()
