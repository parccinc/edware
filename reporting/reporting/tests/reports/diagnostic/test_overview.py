import unittest
from datetime import date
from pyramid import testing
from pyramid.registry import Registry
from reporting.reports.helpers.constants import Constants, AssessmentType
from reporting.reports.diagnostic.helpers.constants import DiagnosticReportType
from reporting.tests.reports.base import BaseReportTestCase
from reporting.reports.routes.school_report_route import get_school_report
from reporting.reports.diagnostic.overview import OverviewReport
from edcore.utils.query import QueryMapKey
from reporting.security.roles.pii import PII  # @UnusedImport


class TestDiagnosticsOverview(BaseReportTestCase):

    def setUp(self):
        reg = Registry()
        reg.settings = {}

        BaseReportTestCase.setUp(self, registry=reg)

    def tearDown(self):
        # reset the registry
        testing.tearDown()

    def test_get_diagnostic_overview_report(self):
        results = get_school_report({
            Constants.STATECODE: 'RI',
            Constants.DISTRICTGUID: 'a',
            Constants.SCHOOLGUID: 'b',
            Constants.YEAR: 2015,
            Constants.SUBJECT: Constants.SUBJECT2,
            Constants.VIEW: DiagnosticReportType.OVERVIEW
        })
        self.assertIsInstance(results, dict)
        self.assertIn(Constants.ASSESSMENTS, results)
        self.assertIn(Constants.SUBJECTS, results)

    def test_diagnostic_overview_report(self):
        params = {
            Constants.STATECODE: 'RI',
            Constants.DISTRICTGUID: 'a',
            Constants.SCHOOLGUID: 'b',
            Constants.YEAR: 2015,
            Constants.SUBJECT: Constants.SUBJECT2,
            Constants.VIEW: DiagnosticReportType.OVERVIEW
        }
        overview = OverviewReport(**params)
        results = {
            QueryMapKey(type='assessments', subject='rpt_ela_decod'): [
                {
                    Constants.STUDENT_PARCC_ID: '123',
                    Constants.STUDENT_FIRST_NAME: "Steve",
                    Constants.STUDENT_MIDDLE_NAME: "",
                    Constants.STUDENT_LAST_NAME: "Brule",
                    Constants.ASMT_GRADE: "03",
                    Constants.STAFF_ID: "a0b1c2",
                    Constants.ASMT_DATE: date(2015, 2, 2),
                }
            ]
        }
        expected = {
            'asmt_date': '02/02/2015',
            'asmt_grade': '03',
            'staff_id': 'a0b1c2',
            'student_display_name': 'Brule, Steve ',
            'student_guid': '123',
            'view': 'Decoding'
        }
        formatted = overview.format_results(results)
        self.assertDictEqual(expected, formatted[AssessmentType.DIAGNOSTIC][Constants.SUBJECT2][0], "formatted overview results do not match")

    def test_get_diagnostic_overview_report_with_correct_params(self):
        results = get_school_report({
            Constants.STATECODE: 'RI',
            Constants.DISTRICTGUID: 'R0003',
            Constants.SCHOOLGUID: 'RP001',
            Constants.YEAR: 2015,
            Constants.SUBJECT: Constants.SUBJECT2,
            Constants.VIEW: DiagnosticReportType.OVERVIEW,
        })
        self.assertIsInstance(results, dict)
        self.assertIn(Constants.ASSESSMENTS, results)
        self.assertIn(Constants.SUBJECTS, results)
        actual = [
            {
                'asmt_grade': '04',
                'asmt_date': '03/08/2015',
                'view': 'Decoding',
                'staff_id': '3c6b9327b0484f5f84741b204a6c9f02208bc1',
                'student_display_name': 'Forst, Jacquline Z.',
                'student_guid': 'hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ'
            },
            {
                'asmt_grade': '04',
                'asmt_date': '03/08/2015',
                'view': 'Reading Comprehension',
                'staff_id': '3c6b9327b0484f5f84741b204a6c9f02208bc1',
                'student_display_name': 'Forst, Jacquline Z.',
                'student_guid': 'hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ'
            },
            {
                'asmt_grade': '04',
                'asmt_date': '03/08/2015',
                'view': 'Reading Fluency',
                'staff_id': '3c6b9327b0484f5f84741b204a6c9f02208bc1',
                'student_display_name': 'Forst, Jacquline Z.',
                'student_guid': 'hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ'
            },
            {
                'asmt_grade': '04',
                'asmt_date': '03/08/2015',
                'view': 'Vocabulary',
                'staff_id': '3c6b9327b0484f5f84741b204a6c9f02208bc1',
                'student_display_name': 'Forst, Jacquline Z.',
                'student_guid': 'hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ'
            },
            {
                'asmt_grade': '04',
                'asmt_date': '03/08/2015',
                'view': 'Writing',
                'staff_id': '3c6b9327b0484f5f84741b204a6c9f02208bc1',
                'student_display_name': 'Forst, Jacquline Z.',
                'student_guid': 'hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ'
            },
        ]
        for i in range(0, 4):
            self.assertDictEqual(actual[i], results[Constants.ASSESSMENTS][AssessmentType.DIAGNOSTIC][Constants.SUBJECT2][i])


if __name__ == '__main__':
    unittest.main()
