import unittest
from datetime import date
from pyramid import testing
from pyramid.registry import Registry
from reporting.reports.helpers.constants import Constants, AssessmentType
from reporting.reports.diagnostic.helpers.constants import DiagnosticReportType, ClusterConstants
from reporting.tests.reports.base import BaseReportTestCase
from reporting.reports.routes.school_report_route import get_school_report
from reporting.reports.diagnostic.cluster import ClusterReport
from edcore.utils.query import QueryMapKey
from reporting.security.roles.pii import PII  # @UnusedImport


class TestProgression(BaseReportTestCase):

    def setUp(self):
        reg = Registry()
        reg.settings = {}

        BaseReportTestCase.setUp(self, registry=reg)

    def tearDown(self):
        # reset the registry
        testing.tearDown()

    def test_get_diagnostic_cluster_report(self):
        results = get_school_report({
            Constants.STATECODE: 'RI',
            Constants.DISTRICTGUID: 'a',
            Constants.SCHOOLGUID: 'b',
            Constants.YEAR: 2015,
            Constants.SUBJECT: Constants.SUBJECT1,
            Constants.VIEW: DiagnosticReportType.CLUSTER
        })
        self.assertIsInstance(results, dict)
        self.assertIn(Constants.ASSESSMENTS, results)
        self.assertIn(Constants.SUBJECTS, results)

    def test_diagnostic_cluster_report(self):
        params = {
            Constants.STATECODE: 'RI',
            Constants.DISTRICTGUID: 'a',
            Constants.SCHOOLGUID: 'b',
            Constants.YEAR: 2015,
            Constants.SUBJECT: Constants.SUBJECT1,
            Constants.VIEW: DiagnosticReportType.CLUSTER
        }
        cluster = ClusterReport(**params)
        results = {
            QueryMapKey(type='assessments', subject='rpt_math_cluster'): [
                {
                    Constants.STUDENT_PARCC_ID: '123',
                    Constants.STUDENT_FIRST_NAME: "Steve",
                    Constants.STUDENT_MIDDLE_NAME: "",
                    Constants.STUDENT_LAST_NAME: "Brule",
                    Constants.ASMT_GRADE: "03",
                    Constants.STAFF_ID: "a0b1c2",
                    Constants.ASMT_DATE: date(2015, 2, 2),
                    ClusterConstants.CODE_CLUSTER: "7.NDP.A",
                    ClusterConstants.PROB_CLUSTER: 0.45,
                }
            ]
        }
        expected = dict()
        expected[AssessmentType.DIAGNOSTIC] = {}
        expected[AssessmentType.DIAGNOSTIC][Constants.SUBJECT1] = [{
            Constants.STUDENT_DISPLAY_NAME: "Brule, Steve ",
            Constants.STUDENT_GUID: '123',
            Constants.ASMT_GRADE: "03",
            Constants.STAFF_ID: "a0b1c2",
            Constants.ASMT_DATE: "02/02/2015",
            ClusterConstants.CLUSTERS: [
                {ClusterConstants.PROB_CLUSTER: 0.45,
                    ClusterConstants.CODE_CLUSTER: "7.NDP.A"},
            ]
        }]
        formatted = cluster.format_results(results)
        self.assertDictEqual(
            expected, formatted, "formatted decoding results do not match")

if __name__ == '__main__':
    unittest.main()
