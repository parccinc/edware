import unittest
from datetime import date
from unittest.mock import patch

from pyramid import testing
from pyramid.registry import Registry
from edcore.tests.utils.unittest_with_edcore_sqlite import UnittestEdcoreDBConnection
from reporting.reports.diagnostic.reading_fluency import ReadingFluencyReport
from reporting.reports.diagnostic.helpers.constants import DiagnosticReportType, DiagnosticsTable, \
    ReadingFluencyConstants
from reporting.reports.helpers.constants import Constants, AssessmentType
from reporting.reports.routes.school_report_route import get_school_report
from reporting.tests.reports.base import BaseReportTestCase
from edcore.utils.query import QueryMapKey
from reporting.security.roles.pii import PII  # @UnusedImport


class TestDiagnosticsReadingFluencyReport(BaseReportTestCase):

    def setUp(self):
        reg = Registry()
        reg.settings = {}
        self.params = {
            Constants.STATECODE: 'RI',
            Constants.DISTRICTGUID: 'a',
            Constants.SCHOOLGUID: 'b',
            Constants.YEAR: 2015,
            Constants.SUBJECT: Constants.SUBJECT2,
            Constants.VIEW: DiagnosticReportType.READING_FLUENCY
        }
        BaseReportTestCase.setUp(self, registry=reg)

    def tearDown(self):
        # reset the registry
        testing.tearDown()

    def test_get_diagnostic_reading_fluency_report(self):
        results = get_school_report({
            Constants.STATECODE: 'RI',
            Constants.DISTRICTGUID: 'a',
            Constants.SCHOOLGUID: 'b',
            Constants.YEAR: 2015,
            Constants.SUBJECT: Constants.SUBJECT2,
            Constants.VIEW: DiagnosticReportType.READING_FLUENCY
        })
        self.assertIsInstance(results, dict)
        self.assertIn(Constants.ASSESSMENTS, results)
        self.assertIn(Constants.SUBJECTS, results)

    def test_reading_fluency_format_result(self):
        report = ReadingFluencyReport(**self.params)
        results = {
            QueryMapKey(type=Constants.ASSESSMENTS, subject=DiagnosticsTable.ELA_READING_FLUENCY): [
                {
                    Constants.STUDENT_FIRST_NAME: "Justine",
                    Constants.STUDENT_LAST_NAME: "Henin",
                    Constants.STUDENT_MIDDLE_NAME: "",
                    Constants.ASMT_GRADE: "03",
                    Constants.STAFF_ID: "a0b1c2",
                    Constants.ASMT_DATE: date(2015, 2, 2),
                    Constants.STUDENT_PARCC_ID: '456',
                    ReadingFluencyConstants.WPM: 550,
                    ReadingFluencyConstants.WCPM: 505,
                    ReadingFluencyConstants.ACCURACY: 90,
                    ReadingFluencyConstants.EXPRESS: 1,
                    ReadingFluencyConstants.PASSAGE1_TYPE: "Informational",
                    ReadingFluencyConstants.PASSAGE2_TYPE: "Literary",
                    ReadingFluencyConstants.PASSAGE3_TYPE: "Tennis",
                }
            ]
        }
        expected = dict()
        expected[AssessmentType.DIAGNOSTIC] = {}
        expected[AssessmentType.DIAGNOSTIC][Constants.SUBJECT2] = [{
            Constants.STUDENT_DISPLAY_NAME: "Henin, Justine ",
            Constants.ASMT_GRADE: "03",
            Constants.STAFF_ID: "a0b1c2",
            Constants.ASMT_DATE: "02/02/2015",
            Constants.STUDENT_GUID: '456',
            ReadingFluencyConstants.WPM: 550,
            ReadingFluencyConstants.WCPM: 505,
            ReadingFluencyConstants.ACCURACY: 90,
            ReadingFluencyConstants.EXPRESS: 1,
            ReadingFluencyConstants.PASSAGE_INFO: [
                {
                    ReadingFluencyConstants.PASSAGE_TYPE: "Informational",
                },
                {
                    ReadingFluencyConstants.PASSAGE_TYPE: "Literary",
                },
                {
                    ReadingFluencyConstants.PASSAGE_TYPE: "Tennis",
                },
            ],
        }]
        formatted = report.format_results(results)
        self.assertDictEqual(expected, formatted, "formatted results do not match")

    def test_get_query(self):
        with UnittestEdcoreDBConnection() as conn:
            report = ReadingFluencyReport(**self.params)
            query = report.get_query(conn.get_table(DiagnosticsTable.ELA_READING_FLUENCY))
            self.assertIn('resp_dist_id =', str(query))
            self.assertIn('resp_school_id =', str(query))
            self.assertIn('state_code =', str(query))
            self.assertIn('rec_status =', str(query))

    @patch('reporting.reports.diagnostic.reading_fluency.ReadingFluencyReport.get_query')
    @patch('reporting.reports.base_report.execute_mapped_queries_remotely')
    def test_run_query(self, exec_query_patch, query_patch):
        fluency_expected_result = [{'fluency': 1234}]
        query_patch.return_value = 'fluency query'
        exec_query_patch.return_value = fluency_expected_result
        report = ReadingFluencyReport(**self.params)
        result = report.run_query()
        self.assertEqual(result, fluency_expected_result)
        exec_query_patch.assert_called_once_with(
            {
                QueryMapKey(type='assessments', subject='rpt_ela_read_flu'): {'state_code': 'RI',
                                                                              'query': 'fluency query'}
            }
        )
        exec_query_patch.reset_mock()


if __name__ == '__main__':
    unittest.main()
