from pyramid.testing import DummyRequest
from pyramid import testing
from pyramid.security import Allow
from beaker.cache import CacheManager
from beaker.util import parse_cache_config_options
from parcc_common.security.constants import RolesConstants
from edauth.tests.test_helper.create_session import create_test_session
from edcore.tests.utils.unittest_with_edcore_sqlite import UnittestEdcoreDBConnection
from reporting.reports.summative.student_report import StudentReport
from reporting.reports.routes.student_report_route import get_student_report
from reporting.reports.helpers.constants import AssessmentType, RptTable, Constants
import unittest
import edauth
from edapi.httpexceptions import EdApiHTTPPreconditionFailed
from edextract.celery import setup_celery as setup_extract_celery
from reporting.tests.reports.base import BaseReportTestCase
from reporting.security.roles.pii import PII  # @UnusedImport


class TestStudentReport(BaseReportTestCase):
    def setUp(self):
        BaseReportTestCase.setUp(self)
        cache_opts = {
            'cache.type': 'memory',
            'cache.regions': 'public.data,public.filtered_data,public.shortlived'
        }

        CacheManager(**parse_cache_config_options(cache_opts))

        self.__request = DummyRequest()
        # Must set hook_zca to false to work with uniittest_with_sqlite
        self.__config = testing.setUp(request=self.__request, hook_zca=False)
        defined_roles = [(Allow, RolesConstants.PII, ('view', 'logout'))]
        edauth.set_roles(defined_roles)
        # Set up context security
        dummy_session = create_test_session([RolesConstants.PII])
        self.__config.testing_securitypolicy(dummy_session.get_user())

        settings = {'extract.celery.CELERY_ALWAYS_EAGER': True}
        setup_extract_celery(settings)

        self.subj1_items = [
            {'name': 'Math_1', 'item_max_points': '5'},
            {'name': 'Math_2', 'item_max_points': '5'}
        ]
        self.subj2_items = [
            {'name': 'ELA_1', 'item_max_points': '5'},
            {'name': 'ELA_2', 'item_max_points': '5'}
        ]

        self.__valid_math_params = {
            'stateCode': 'RI',
            'districtGuid': 'R0001',
            'schoolGuid': 'RN001',
            'gradeCourse': 'Grade 9',
            'studentGuid': 'EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q',
            'year': 2015,
            'subject': 'subject1',
            'asmtType': AssessmentType.SUMMATIVE,
        }

        self.__valid_ela_params = {
            'stateCode': 'RI',
            'districtGuid': 'R0001',
            'schoolGuid': 'RN001',
            'gradeCourse': 'Grade 9',
            'studentGuid': 'EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q',
            'year': 2015,
            'subject': 'subject2',
            'asmtType': AssessmentType.SUMMATIVE,
        }

        self.dummy_math_results = dict()
        self.dummy_math_results["subject1"] = [
            {
                'sum_perf_lvl': 4,
                'sum_scale_score': 712,
                'sum_csem': 738,
                'sum_read_scale_score': None,
                'sum_read_csem': None,
                'sum_write_scale_score': None,
                'sum_write_csem': None,
                'asmt_subject': 'Mathematics',
                'asmt_grade': None,
                'subclaim1_category': 2,
                'subclaim2_category': 2,
                'subclaim3_category': 1,
                'subclaim4_category': 3,
                'subclaim5_category': 3,
                'subclaim6_category': None,
                'perf_lvl_id': 1,
                'cutpoint_label': 'minimal',
                'cutpoint_low': '1',
                'cutpoint_upper': '69'
            },
            {
                'sum_perf_lvl': 4,
                'sum_scale_score': 712,
                'sum_csem': 738,
                'sum_read_scale_score': None,
                'sum_read_csem': None,
                'sum_write_scale_score': None,
                'sum_write_csem': None,
                'asmt_subject': 'Mathematics',
                'asmt_grade': None,
                'subclaim1_category': 2,
                'subclaim2_category': 2,
                'subclaim3_category': 1,
                'subclaim4_category': 3,
                'subclaim5_category': 3,
                'subclaim6_category': None,
                'perf_lvl_id': 2,
                'cutpoint_label': 'partial',
                'cutpoint_low': '70',
                'cutpoint_upper': '169'
            },
            {
                'sum_perf_lvl': 4,
                'sum_scale_score': 712,
                'sum_csem': 738,
                'sum_read_scale_score': None,
                'sum_read_csem': None,
                'sum_write_scale_score': None,
                'sum_write_csem': None,
                'asmt_subject': 'Mathematics',
                'asmt_grade': None,
                'subclaim1_category': 2,
                'subclaim2_category': 2,
                'subclaim3_category': 1,
                'subclaim4_category': 3,
                'subclaim5_category': 3,
                'subclaim6_category': None,
                'perf_lvl_id': 3,
                'cutpoint_label': 'moderate',
                'cutpoint_low': '170',
                'cutpoint_upper': '230'
            },
            {
                'sum_perf_lvl': 4,
                'sum_scale_score': 712,
                'sum_csem': 738,
                'sum_read_scale_score': None,
                'sum_read_csem': None,
                'sum_write_scale_score': None,
                'sum_write_csem': None,
                'asmt_subject': 'Mathematics',
                'subclaim1_category': 2,
                'subclaim2_category': 2,
                'subclaim3_category': 1,
                'subclaim4_category': 3,
                'subclaim5_category': 3,
                'subclaim6_category': None,
                'perf_lvl_id': 4,
                'cutpoint_label': 'strong',
                'cutpoint_low': '231',
                'cutpoint_upper': '250'
            }]
        self.dummy_ela_results = dict()
        self.dummy_ela_results["subject2"] = [{
            'sum_perf_lvl': 3,
            'sum_scale_score': 100,
            'sum_csem': 3,
            'sum_read_scale_score': 21,
            'sum_read_csem': None,
            'sum_write_scale_score': 24,
            'sum_write_csem': None,
            'asmt_subject': 'English Language Arts/Literacy',
            'asmt_grade': 'Grade 9',
            'subclaim1_category': 1,
            'subclaim2_category': 1,
            'subclaim3_category': 3,
            'subclaim4_category': 2,
            'subclaim5_category': 1,
            'subclaim6_category': None,
            'perf_lvl_id': 1,
            'cutpoint_label': 'minimal',
            'cutpoint_low': '1',
            'cutpoint_upper': '100'}]

    def test_student_report_missing_params(self):
        self.__request.method = 'POST'
        self.__request.json_body = {
            'stateCode': 'RI'
        }
        self.assertRaises(EdApiHTTPPreconditionFailed, None, get_student_report, self.__request)

    def test_student_report_valid_params(self):
        self.__request.method = 'POST'
        self.__request.json_body = self.__valid_math_params
        actual = get_student_report(self.__valid_math_params)
        self.assertIsNotNone(actual)
        self.assertIn('user_info', actual, "response should contain user information")
        self.assertIn('context', actual, "response should contain context information")
        self.assertIn('subjects', actual, "response should contain subject information")

    def test_get_results(self):
        actual = StudentReport(**self.__valid_math_params).get_results()
        self.assertIsNotNone(actual)

    def test_get_breadcrumbs(self):
        actual = StudentReport(**self.__valid_math_params).get_breadcrumbs_context()
        items = actual['items']
        self.assertIsNotNone(items)
        self.assertEqual(len(items), 6, "breadcrumb must contain 6 levels")
        # test student's name and id because that's the information ISR care about
        self.assertEqual(items[5]['type'], 'student', "last level of breadcrumb must be student")
        self.assertEqual(items[5]['name'], 'Darci Forst', "breadcrumb must contain student name")

    def test_get_query(self):
        test_report = StudentReport(**self.__valid_math_params)
        with UnittestEdcoreDBConnection() as conn:
            actual = test_report.get_query(conn.get_table(RptTable.ELA_SUM), conn.get_table(RptTable.RPT_SUM_TEST_LEVEL_M))
            self.assertIsNotNone(actual, "should return some query string")
            self.assertEqual(type(actual), str, "query must be string")

    def test_run_query(self):
        test_report = StudentReport(**self.__valid_math_params)
        actual = test_report.run_query()
        self.assertTrue(actual, "query should return some student data")

    def test_format_results(self):
        valid_params = {
            'stateCode': 'RI',
            'districtGuid': 'R0001',
            'schoolGuid': 'RN001',
            'gradeCourse': "Algebra I",
            'studentGuid': 'EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q',
            'year': 2015,
            'subject': 'subject2',
            'asmtType': AssessmentType.SUMMATIVE,
        }
        test_report = StudentReport(**valid_params)
        dummy_results = dict(self.dummy_math_results)
        actual = test_report.format_results(dummy_results)
        self.assertIsNotNone(actual, "should return something")
        self.assertIn('subject1', set(actual.keys()), "should contain subjects record")
        expected_subject1_data = {
            'subscores': [
                {
                    'claims': [
                        {
                            'level': 2
                        },
                        {
                            'level': 2
                        },
                        {
                            'level': 3
                        },
                        {
                            'level': 1
                        }
                    ]
                }
            ],
            'textBodyGradeCourse': 'Mathematics',
            'course': 'Mathematics',
            'overall': {
                'score': 712,
                'error': 738,
                'name': 'strong understanding',
                'level': 4,
                'performance': [
                    {
                        'cutpoint_start': 1.0,
                        'cutpoint_end': 69.0,
                        'name': 'minimal',
                        'level': 1
                    },
                    {
                        'cutpoint_start': 70.0,
                        'cutpoint_end': 169.0,
                        'name': 'partial',
                        'level': 2
                    },
                    {
                        'cutpoint_start': 170.0,
                        'cutpoint_end': 230.0,
                        'name': 'moderate',
                        'level': 3
                    },
                    {
                        'cutpoint_start': 231.0,
                        'cutpoint_end': 250.0,
                        'name': 'strong',
                        'level': 4
                    }
                ]
            },
            'titlePrefix': 'Mathematics'
        }

        self.assertEqual(actual['subject1'], expected_subject1_data)

        # make sure titlePrefix changes based on subject and grade level
        self.assertEqual(actual['subject1']['titlePrefix'], "Mathematics")
        self.assertEqual(actual['subject1']['textBodyGradeCourse'], "Mathematics")

    def test_ela_formatting(self):
        test_report = StudentReport(**self.__valid_ela_params)
        dummy_results = dict(self.dummy_ela_results)
        actual = test_report.format_results(dummy_results)
        self.assertIsNotNone(actual, "should return something")
        self.assertEqual(actual['subject2']['titlePrefix'], "Grade 9")
        self.assertEqual(actual['subject2']['textBodyGradeCourse'], "Grade 9")

    def test_grade_course(self):
        # US34665
        # change grade to < 9 and make sure Grade # appears
        # since there is no relevant course title to display
        test_report = StudentReport(**self.__valid_ela_params)
        dummy_results = dict(self.dummy_ela_results)
        for subj in dummy_results:
            for r in range(len(dummy_results[subj])):
                dummy_results[subj][r]['asmt_grade'] = 'Grade 5'
        actual = test_report.format_results(dummy_results)
        self.assertEqual(actual['subject2']['titlePrefix'], "Grade 5")
        self.assertEqual(actual['subject2']['textBodyGradeCourse'], "Grade 5")

    def test_format_summary(self):
        records = [
            {
                'name': 'RI',
                'average_score': 252,
                'sum_read_scale_score': 300,
                'sum_write_scale_score': 232,
                'level4_avg': {
                    'reading': 10,
                    'writing': 10,
                },
            }
        ]
        expected_state_name = "RI"
        expected_state_reading_score = 300
        expected_state_writing_score = 232
        student_report = StudentReport(**self.__valid_ela_params)
        aggregator = student_report.get_aggregator()
        results = aggregator.format_summary(student_report, report_level='state', results={Constants.SUBJECT2: records})
        ela_results = results[AssessmentType.SUMMATIVE][Constants.SUBJECT2][0]

        state_name = ela_results['name']
        state_reading_score = ela_results['average_claim_score']['reading']
        state_writing_score = ela_results['average_claim_score']['writing']

        self.assertEquals(expected_state_name, state_name)
        self.assertEquals(expected_state_reading_score, state_reading_score)
        self.assertEquals(expected_state_writing_score, state_writing_score)

if __name__ == '__main__':
    unittest.main()
