import unittest
from datetime import date
from reporting.reports.helpers.constants import Constants
from reporting.reports.diagnostic.helpers.constants import ReadingFluencyConstants
from reporting.reports.diagnostic.formatter.reading_fluency_formatter import format_results


class TestDiagnosticsReadingFluencyFormat(unittest.TestCase):

    def test_formatter(self):
        results = [{
            Constants.STUDENT_FIRST_NAME: "Roger",
            Constants.STUDENT_LAST_NAME: "Federer",
            Constants.STUDENT_MIDDLE_NAME: "",
            Constants.ASMT_GRADE: "07",
            Constants.STAFF_ID: "a0b1c2",
            Constants.ASMT_DATE: date(2015, 2, 2),
            Constants.STUDENT_PARCC_ID: "1",
            ReadingFluencyConstants.WPM: 550,
            ReadingFluencyConstants.WCPM: 505,
            ReadingFluencyConstants.ACCURACY: 90,
            ReadingFluencyConstants.EXPRESS: 1,
            ReadingFluencyConstants.PASSAGE1_TYPE: "Informational",
            ReadingFluencyConstants.PASSAGE2_TYPE: "Literary",
            ReadingFluencyConstants.PASSAGE3_TYPE: "Tennis",
        }]
        expected = [{
            Constants.STUDENT_DISPLAY_NAME: "Federer, Roger ",
            Constants.ASMT_GRADE: "07",
            Constants.STAFF_ID: "a0b1c2",
            Constants.ASMT_DATE: "02/02/2015",
            Constants.STUDENT_GUID: "1",
            ReadingFluencyConstants.WPM: 550,
            ReadingFluencyConstants.WCPM: 505,
            ReadingFluencyConstants.ACCURACY: 90,
            ReadingFluencyConstants.EXPRESS: 1,
            ReadingFluencyConstants.PASSAGE_INFO: [
                {ReadingFluencyConstants.PASSAGE_TYPE: "Informational"},
                {ReadingFluencyConstants.PASSAGE_TYPE: "Literary"},
                {ReadingFluencyConstants.PASSAGE_TYPE: "Tennis"},
            ],
        }]
        formatted = format_results(results)
        self.assertEqual(expected, formatted, "formatted results do not match")
