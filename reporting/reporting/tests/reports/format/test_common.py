import unittest

from reporting.reports.formatter.common import (build_overall_perf_level_attr,
                                                build_subclaim_perf_level_attr,
                                                ensure_sum_to_100,
                                                get_overall_perf_level_distribution,
                                                get_subscore_perf_level_distribution,
                                                GroupByAssessmentTypes,
                                                GroupBySubjects)


class TestCommonFormatter(unittest.TestCase):

    def test_ensure_sum_to_100_with_empty_input(self):
        empty = []
        actual = ensure_sum_to_100(empty)
        expected = []
        self.assertEqual(actual, expected)

    def test_ensure_sum_to_100_with_total_100(self):
        sum_to_100 = [10.1, 20.0, 33.4, 36.5]
        actual = ensure_sum_to_100(sum_to_100)
        expected = [10, 20, 33, 37]
        self.assertEqual(actual, expected)

    def test_ensure_sum_to_100_with_less_value(self):
        less_than_100 = [10.1, 20.0, 33.4]
        actual = ensure_sum_to_100(less_than_100)
        expected = [11, 21, 34]
        self.assertEqual(actual, expected)

    def test_get_overall_perf_level_distribution(self):
        test_data = {
            'students': 3,
            'score': 146,
            'reading': None,
            'writing': None,
            'overall_perf_1': 1,
            'overall_perf_2': 3,
            'overall_perf_3': 3,
            'overall_perf_4': 2,
            'overall_perf_5': 1,
            'name': 'Providence',
            'guid': 'R0003'
        }
        actual_perf, actual_levels = get_overall_perf_level_distribution(test_data)
        self.assertEqual(actual_perf, 30)
        expected_levels = [
            {'level': 1, 'percentage': 10},
            {'level': 2, 'percentage': 30},
            {'level': 3, 'percentage': 30},
            {'level': 4, 'percentage': 20},
            {'level': 5, 'percentage': 10}
        ]
        self.assertEqual(actual_levels, expected_levels)

    def test_get_overall_perf_level_distribution_rounded(self):
        test_data = {
            'students': 3,
            'score': 146,
            'reading': None,
            'writing': None,
            'overall_perf_1': 4,
            'overall_perf_2': 3,
            'overall_perf_3': 7,
            'overall_perf_4': 2,
            'overall_perf_5': 3,
            'name': 'Providence',
            'guid': 'R0003'
        }
        actual_perf, actual_levels = get_overall_perf_level_distribution(test_data)
        self.assertEqual(actual_perf, 26)
        expected_levels = [
            {'level': 1, 'percentage': 21},
            {'level': 2, 'percentage': 16},
            {'level': 3, 'percentage': 37},
            {'level': 4, 'percentage': 10},
            {'level': 5, 'percentage': 16}
        ]
        self.assertEqual(actual_levels, expected_levels)

    def test_get_subscore_perf_level_distribution(self):
        claim_column = 'subclaim1_category'
        test_data = {
            'students': 10,
            'score': 217,
            'reading': 28,
            'writing': 26,
            'subclaim1_category_perf_1': 1,
            'subclaim1_category_perf_2': 5,
            'subclaim1_category_perf_3': 4,
            'name': 'Providence',
            'guid': 'R0003'
        }
        actual_perf, actual_levels = get_subscore_perf_level_distribution(claim_column, test_data)
        self.assertEqual(actual_perf, 40)
        expected = [
            {'level': 1, 'percentage': 10},
            {'level': 2, 'percentage': 50},
            {'level': 3, 'percentage': 40}
        ]
        self.assertEqual(actual_levels, expected)

    def test_build_subclaim_perf_level_attr(self):
        subclaim = 'category'
        performance_level = 1
        actual = build_subclaim_perf_level_attr(subclaim, performance_level)
        expected = 'category_perf_1'
        self.assertEqual(actual, expected)

    def test_build_overall_perf_level_attr(self):
        actual = build_overall_perf_level_attr(1)
        expect = 'overall_perf_1'
        self.assertEqual(actual, expect)

    def test_group_by_type_empty(self):
        actual = GroupByAssessmentTypes({}).create_by(lambda x: x)
        expected = {'SUMMATIVE': {'subject1': [], 'subject2': []}}
        self.assertEqual(actual, expected)

    def test_group_by_type_with_ela_data(self):
        ela_data = {
            'subject2': [
                {
                    'students': 10,
                    'score': 217,
                    'reading': 28,
                    'writing': 26,
                    'subclaim1_category_perf_1': 1,
                    'subclaim1_category_perf_2': 5,
                    'subclaim1_category_perf_3': 4,
                    'name': 'Providence',
                    'guid': 'R0003'
                },
            ]}
        actual = GroupByAssessmentTypes(ela_data).create_by(lambda x: x)
        expected = {
            'SUMMATIVE': {
                'subject1': [],
                'subject2': [
                    {
                        'guid': 'R0003',
                        'name': 'Providence',
                        'reading': 28,
                        'score': 217,
                        'students': 10,
                        'subclaim1_category_perf_1': 1,
                        'subclaim1_category_perf_2': 5,
                        'subclaim1_category_perf_3': 4,
                        'writing': 26
                    }
                ]
            }
        }
        self.assertEqual(actual, expected)

    def test_group_by_subject_empty(self):
        actual = GroupBySubjects({}).create_by(lambda x: x)
        expected = {'subject1': [], 'subject2': []}
        self.assertEqual(actual, expected)

    def test_group_by_subject_with_ela_data(self):
        ela_data = {
            'subject2': [
                {
                    'students': 10,
                    'score': 217,
                    'reading': 28,
                    'writing': 26,
                    'subclaim1_category_perf_1': 1,
                    'subclaim1_category_perf_2': 5,
                    'subclaim1_category_perf_3': 4,
                    'name': 'Providence',
                    'guid': 'R0003'
                },
            ]}
        actual = GroupByAssessmentTypes(ela_data).create_by(lambda x: x)
        expected = {
            'SUMMATIVE': {
                'subject1': [],
                'subject2': [
                    {
                        'guid': 'R0003',
                        'name': 'Providence',
                        'reading': 28,
                        'score': 217,
                        'students': 10,
                        'subclaim1_category_perf_1': 1,
                        'subclaim1_category_perf_2': 5,
                        'subclaim1_category_perf_3': 4,
                        'writing': 26
                    }
                ]
            }
        }
        self.assertEqual(actual, expected)
