import unittest
from datetime import date
from reporting.reports.helpers.constants import Constants
from reporting.reports.diagnostic.helpers.constants import Constants as DiagnosticConstants
from reporting.reports.diagnostic .formatter.vocabulary_formatter import format_results


class TestDiagnosticsVocabularyFormat(unittest.TestCase):

    def test_formatter(self):
        results = [{
            Constants.STUDENT_FIRST_NAME: "Steve",
            Constants.STUDENT_LAST_NAME: "Brule",
            Constants.STUDENT_MIDDLE_NAME: "",
            Constants.ASMT_GRADE: "03",
            Constants.STAFF_ID: "a0b1c2",
            Constants.STUDENT_PARCC_ID: "a0b1c2",
            Constants.ASMT_DATE: date(2015, 2, 2),
            DiagnosticConstants.ASMT_SCORE: 412,
        }]
        expected = [{
            Constants.STUDENT_DISPLAY_NAME: "Brule, Steve ",
            Constants.ASMT_GRADE: "03",
            Constants.STAFF_ID: "a0b1c2",
            Constants.STUDENT_GUID: "a0b1c2",
            Constants.ASMT_DATE: "02/02/2015",
            Constants.SCORE: 412,
        }]
        formatted = format_results(results)
        self.assertEqual(expected, formatted, "formatted vocabulary results do not match")
