import unittest
from datetime import date
from reporting.reports.helpers.constants import Constants
from reporting.reports.diagnostic.helpers.constants import WritingConstants
from reporting.reports.diagnostic.formatter.writing_formatter import format_results


class TestDiagnosticsWritingFormat(unittest.TestCase):

    def test_formatter(self):
        results = [{
            Constants.STUDENT_FIRST_NAME: "Steve",
            Constants.STUDENT_LAST_NAME: "Brule",
            Constants.STUDENT_MIDDLE_NAME: "",
            Constants.ASMT_GRADE: "03",
            Constants.STAFF_ID: "a0b1c2",
            Constants.STUDENT_PARCC_ID: "a0b1c2",
            Constants.ASMT_DATE: date(2015, 2, 2),
            WritingConstants.SCORE_RES: 412,
            WritingConstants.SCORE_LIT: 413,
            WritingConstants.SCORE_NAR: 414,
        }]
        expected = [{
            Constants.STUDENT_DISPLAY_NAME: "Brule, Steve ",
            Constants.ASMT_GRADE: "03",
            Constants.STUDENT_GUID: "a0b1c2",
            Constants.STAFF_ID: "a0b1c2",
            Constants.ASMT_DATE: "02/02/2015",
            WritingConstants.SCORE_RES: 412,
            WritingConstants.SCORE_LIT: 413,
            WritingConstants.SCORE_NAR: 414,
        }]
        formatted = format_results(results)
        self.assertEqual(expected, formatted, "formatted writing results do not match")
