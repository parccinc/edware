'''
Created on May 19, 2015

@author: dip
'''
import unittest
from reporting.reports.summative.consortium import ConsortiumReport
from reporting.reports.helpers.constants import Constants, RptTable
from pyramid.registry import Registry
from reporting.tests.reports.base import BaseReportTestCase
from edcore.security.tenant import set_cds_tenant_map, get_cds_states_text
from edcore.tests.utils.unittest_with_edcore_sqlite import UnittestEdcoreDBConnection
from reporting.reports.summative.helpers.constants import Constants as SummativeConstants


class TestEmptyConsortium(BaseReportTestCase):

    def setUp(self):
        reg = Registry()
        reg.settings = {}
        reg.settings['min_cell_size.performance.{0}'.format('parcc')] = 2
        reg.settings['alert_cell_size'] = 5
        BaseReportTestCase.setUp(self, registry=reg)
        set_cds_tenant_map({'parcc': None})

    def tearDown(self):
        pass

    def test_no_cds_tenants(self):
        params = {Constants.YEAR: 2014}
        report = ConsortiumReport(**params)
        self.assertEquals(get_cds_states_text(), "")

    def test_get_query(self):
        params = {Constants.YEAR: 2014, Constants.GRADECOURSE: '6'}
        report = ConsortiumReport(**params)
        with UnittestEdcoreDBConnection() as conn:
            asmt_table = conn.get_table(RptTable.MATH_SUM)
            query = report.get_query('myName', 'myGuid', 'parcc', asmt_table)
            self.assertIn("'myGuid' AS guid", query)
            self.assertIn("'Example State myName' AS name", query)

    def test_get_summary_query(self):
        params = {Constants.YEAR: 2014, Constants.GRADECOURSE: '6'}
        report = ConsortiumReport(**params)
        with UnittestEdcoreDBConnection() as conn:
            asmt_table = conn.get_table(RptTable.MATH_SUM)
            query = report.get_summary_query(asmt_table)
            self.assertIn("'parcc' AS guid", query)
            self.assertIn("SELECT '' AS name", query)

    def test_setup_min_cell_size(self):
        params = {Constants.YEAR: 2014, Constants.GRADECOURSE: '6'}
        report = ConsortiumReport(**params)
        report.setup_min_cell_size()
        self.assertEqual(report.tenant_min_cell_size['parcc'], 2)

    def test_format_data_with_unique_min_cell_size_per_row(self):
        results = [
            {
                'name': 'State1',
                'guid': 'BC',
                'overall_perf_1': 1,
                'overall_perf_2': 0,
                'overall_perf_3': 0,
                'overall_perf_4': 0,
                'overall_perf_5': 1,
                'score': 555,
                'reading': 233,
                'writing': 23,
                'students': 2,
                'min_cell_size': 5
            },
            {
                'name': 'State2',
                'guid': 'AB',
                'overall_perf_1': 1,
                'overall_perf_2': 0,
                'overall_perf_3': 0,
                'overall_perf_4': 0,
                'overall_perf_5': 1,
                'score': 555,
                'reading': 233,
                'writing': 23,
                'students': 2,
                'min_cell_size': 1
            }]
        params = {Constants.YEAR: 2014, Constants.GRADECOURSE: '6', Constants.SUBJECT: Constants.SUBJECT2}
        report = ConsortiumReport(**params)
        formatted = report.format_results({Constants.SUBJECT2: results})
        self.assertTrue(formatted['SUMMATIVE'][Constants.SUBJECT2][0][SummativeConstants.IS_LESS_THAN_MIN_CELL_SIZE])
        self.assertEquals(formatted['SUMMATIVE'][Constants.SUBJECT2][1][SummativeConstants.STUDENTS], 2)

    def test_format_summary(self):
        results = [
            {
                'name': 'parcc',
                'guid': 'BC',
                'overall_perf_1': 1,
                'overall_perf_2': 0,
                'overall_perf_3': 0,
                'overall_perf_4': 0,
                'overall_perf_5': 1,
                'score': 555,
                'reading': 233,
                'writing': 23,
                'students': 2,
                'min_cell_size': 5
            }]
        params = {Constants.YEAR: 2014, Constants.GRADECOURSE: '6', Constants.SUBJECT: Constants.SUBJECT2}
        report = ConsortiumReport(**params)
        formatted = report.format_results({Constants.SUBJECT2: results})
        self.assertTrue(formatted['SUMMATIVE'][Constants.SUBJECT2][0][SummativeConstants.IS_LESS_THAN_MIN_CELL_SIZE])

if __name__ == "__main__":
    unittest.main()
