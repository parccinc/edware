'''
Created on Jul 6, 2015

@author: dip
'''
import unittest
from reporting.reports.helpers.constants import Constants
from pyramid.testing import DummyRequest
from pyramid.registry import Registry
from pyramid import testing
from reporting.reports.configuration.analytics import add_analytics_info


class TestConfig(unittest.TestCase):

    @add_analytics_info
    def dummy(self):
        return {}

    def setUp(self):
        self.__request = DummyRequest()
        # Must set hook_zca to false to work with uniittest_with_sqlite
        reg = Registry()
        reg.settings = {}
        reg.settings['analytics.url'] = 'http://www.analytics.url'
        self.__config = testing.setUp(registry=reg, request=self.__request, hook_zca=False)

    def tearDown(self):
        pass

    def testConfiguration(self):
        results = self.dummy()
        self.assertEquals(results[Constants.ANALYTICS]['url'], 'http://www.analytics.url')
