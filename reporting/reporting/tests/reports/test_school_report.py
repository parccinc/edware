import unittest

from edcore.security.tenant import set_cds_tenant_map
from pyramid import testing
from edcore.tests.utils.unittest_with_edcore_sqlite import get_unittest_tenant_name
from reporting.reports.helpers.constants import Constants, AssessmentType
from reporting.reports.summative.helpers.constants import Constants as SummativeConstants
from reporting.tests.reports.base import BaseReportTestCase
from pyramid.registry import Registry
from reporting.reports.summative.school_report import SchoolLevelOverallReport
from reporting.reports.routes.school_report_route import get_school_report
from reporting.reports.helpers.filters import FilterGender
from reporting.security.roles.pii import PII  # @UnusedImport


class TestSchoolReport(BaseReportTestCase):

    def setUp(self):
        tenant = get_unittest_tenant_name()
        reg = Registry()
        reg.settings = {}
        reg.settings['min_cell_size.performance.{0}'.format(tenant)] = 2
        reg.settings['min_cell_size.growth.{0}'.format(tenant)] = 1
        reg.settings['alert_cell_size'] = 5
        set_cds_tenant_map({'parcc': None})
        BaseReportTestCase.setUp(self, registry=reg)

    def tearDown(self):
        # reset the registry
        testing.tearDown()

    def test_get_school_report(self):
        results = get_school_report({Constants.STATECODE: 'RI', Constants.DISTRICTGUID: 'a', Constants.SCHOOLGUID: 'b',
                                     Constants.YEAR: 2015, Constants.SUBJECT: Constants.SUBJECT1, Constants.VIEW: 'vocab'})
        self.assertIsInstance(results, dict)
        self.assertIn(Constants.ASSESSMENTS, results)
        self.assertIn(Constants.SUBJECTS, results)

    def test_school_report(self):
        params = {Constants.STATECODE: 'RI', Constants.YEAR: 2014, Constants.DISTRICTGUID: 'a', Constants.SCHOOLGUID: 'b',
                  Constants.SUBJECT: Constants.SUBJECT1}
        school = SchoolLevelOverallReport(**params)
        records = [
            {
                'students': 15,
                'score': 252,
                'reading': 31,
                'writing': 29,
                'guid': 'Grade 3',
                'name': 'Grade 3',
                'overall_perf_1': 2,
                'overall_perf_2': 2,
                'overall_perf_3': 6,
                'overall_perf_4': 4,
                'overall_perf_5': 1,
                'parcc_growth_percent': 2,
                'state_growth_percent': 3
            }
        ]
        results = {}
        results[Constants.SUBJECT1] = {'school': records}
        formatted = school.format_results(results)
        subject = formatted[AssessmentType.SUMMATIVE][Constants.SUBJECT1]
        self.assertEquals(len(subject), 1, formatted)
        self.assertEquals(subject[0][Constants.NAME], 'Grade 3')
        self.assertEquals(subject[0][SummativeConstants.GUID], 'Grade 3')
        school_data = subject[0]['school']
        self.assertFalse(school_data[SummativeConstants.HAS_FILTERS])
        self.assertEquals(school_data[SummativeConstants.STUDENTS], 15)

    def test_school_report_with_filters_and_less_than_25(self):
        params = {Constants.STATECODE: 'RI', Constants.YEAR: 2014, Constants.DISTRICTGUID: 'a', Constants.SCHOOLGUID: 'b',
                  Constants.SUBJECT: Constants.SUBJECT2, FilterGender.NAME: [FilterGender.MALE]}
        school = SchoolLevelOverallReport(**params)
        records = [
            {
                'students': 4,
                'score': 252,
                'reading': 31,
                'writing': 29,
                'guid': 'Grade 11',
                'name': 'Grade 11',
                'overall_perf_1': 2,
                'overall_perf_2': 2,
                'overall_perf_3': 6,
                'overall_perf_4': 4,
                'overall_perf_5': 1,
                'parcc_growth_percent': 2,
                'state_growth_percent': 3
            }
        ]
        results = {}
        results[Constants.SUBJECT2] = {'school': records}
        formatted = school.format_results(results)
        subject = formatted[AssessmentType.SUMMATIVE][Constants.SUBJECT2]
        self.assertEquals(len(subject), 1)
        self.assertEquals(subject[0][Constants.NAME], 'Grade 11')
        self.assertEquals(subject[0][SummativeConstants.GUID], 'Grade 11')
        school_data = subject[0]['school']
        self.assertTrue(school_data[SummativeConstants.HAS_FILTERS])
        self.assertTrue(school_data[SummativeConstants.IS_LESS_THAN_THRESHOLD])
        self.assertNotIn(SummativeConstants.STUDENTS, school_data)

    def test_with_filters_with_grt_threshold(self):
        params = {Constants.STATECODE: 'RI', Constants.YEAR: 2014, Constants.DISTRICTGUID: 'a', Constants.SCHOOLGUID: 'b',
                  Constants.SUBJECT: Constants.SUBJECT1, FilterGender.NAME: [FilterGender.MALE]}
        school = SchoolLevelOverallReport(**params)
        records = [
            {
                'students': 55,
                'score': 252,
                'reading': 31,
                'writing': 29,
                'guid': 'Grade 8',
                'name': 'Grade 8',
                'overall_perf_1': 2,
                'overall_perf_2': 2,
                'overall_perf_3': 6,
                'overall_perf_4': 4,
                'overall_perf_5': 1,
                'parcc_growth_percent': 2,
                'state_growth_percent': 3
            }
        ]
        results = {}
        results[Constants.SUBJECT1] = {'school': records}
        formatted = school.format_results(results)
        subject = formatted[AssessmentType.SUMMATIVE][Constants.SUBJECT1]
        self.assertEquals(len(subject), 1)
        self.assertEquals(subject[0][Constants.NAME], 'Grade 8')
        self.assertEquals(subject[0][SummativeConstants.GUID], 'Grade 8')
        school_data = subject[0]['school']
        self.assertTrue(school_data[SummativeConstants.HAS_FILTERS])
        self.assertFalse(school_data[SummativeConstants.IS_LESS_THAN_THRESHOLD])
        self.assertNotIn(SummativeConstants.STUDENTS, school_data)

    def test_school_report_with_courses_with_min_cell_size(self):
        params = {Constants.STATECODE: 'RI', Constants.YEAR: 2014, Constants.DISTRICTGUID: 'a', Constants.SCHOOLGUID: 'b',
                  Constants.SUBJECT: Constants.SUBJECT1, FilterGender.NAME: [FilterGender.MALE]}
        school = SchoolLevelOverallReport(**params)
        records = [
            {
                'students': 1,
                'score': 252,
                'reading': 31,
                'writing': 29,
                'guid': 'Algebra II',
                'name': 'Algebra II',
                'overall_perf_1': 2,
                'overall_perf_2': 2,
                'overall_perf_3': 6,
                'overall_perf_4': 4,
                'overall_perf_5': 1,
                'parcc_growth_percent': 2,
                'state_growth_percent': 3
            }
        ]
        results = {}
        results[Constants.SUBJECT1] = {'school': records}
        results['courses'] = {'school': records}
        formatted = school.format_results(results)
        subject = formatted[AssessmentType.SUMMATIVE][Constants.SUBJECT1]
        self.assertEquals(len(subject), 1)
        self.assertEquals(subject[0][Constants.NAME], 'Algebra II')
        self.assertEquals(subject[0][SummativeConstants.GUID], 'Algebra II')
        school_data = subject[0]['school']
        self.assertTrue(school_data[SummativeConstants.HAS_FILTERS])
        self.assertTrue(school_data[SummativeConstants.IS_LESS_THAN_MIN_CELL_SIZE])
        self.assertIn(Constants.MIN_CELL_PERF_MSG, school_data[SummativeConstants.MIN_CELL_SIZE_MSG])
        self.assertNotIn(SummativeConstants.PERFORMANCELEVELS, school_data)

if __name__ == '__main__':
    unittest.main()
