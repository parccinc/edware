from unittest.mock import Mock

from edcore.database.cds_connector import CdsDBConnection
from edcore.database.edcore_connector import EdCoreDBConnection
from edcore.security.tenant import set_cds_tenant_map, set_tenant_map
from pyramid.testing import DummyRequest
from pyramid import testing
from pyramid.security import Allow
from beaker.cache import CacheManager
from beaker.util import parse_cache_config_options
from parcc_common.security.constants import RolesConstants
from edauth.tests.test_helper.create_session import create_test_session
from edcore.tests.utils.unittest_with_edcore_sqlite import Unittest_with_edcore_sqlite, get_unittest_tenant_name
import edauth
from edextract.celery import setup_celery as setup_extract_celery


class BaseReportTestCase(Unittest_with_edcore_sqlite):

    @classmethod
    def setUpClass(cls, *args, **kwargs):
        super().setUpClass(native_datetime=False, **kwargs)

    def setUp(self, registry=None):
        cache_opts = {
            'cache.type': 'memory',
            'cache.regions': 'public.data,public.filtered_data,public.shortlived'
        }

        CacheManager(**parse_cache_config_options(cache_opts))

        self.request = DummyRequest()
        # Must set hook_zca to false to work with uniittest_with_sqlite
        self.config = testing.setUp(registry=registry, request=self.request, hook_zca=False)
        defined_roles = [(Allow, RolesConstants.PII, ('view', 'logout'))]
        edauth.set_roles(defined_roles)
        EdCoreDBConnection._get_tenant = Mock(return_value=get_unittest_tenant_name())
        CdsDBConnection.get_datasource_name = Mock(return_value='edware.db.tomcat')
        set_tenant_map({get_unittest_tenant_name(): 'RI'})
        set_cds_tenant_map({get_unittest_tenant_name(): None})
        # Set up context security
        dummy_session = create_test_session([RolesConstants.PII])
        self.config.testing_securitypolicy(dummy_session.get_user())

        settings = {'extract.celery.CELERY_ALWAYS_EAGER': True}
        setup_extract_celery(settings)
