'''
Created on Mar 8, 2013

@author: dip
'''
import unittest

from beaker.cache import CacheManager
from pyramid.testing import DummyRequest
from pyramid import testing
from beaker.util import parse_cache_config_options
from pyramid.security import Allow

from edcore.tests.utils.unittest_with_edcore_sqlite import Unittest_with_edcore_sqlite,\
    get_unittest_tenant_name
from reporting.reports.helpers.breadcrumbs import get_breadcrumbs_context
from edauth.tests.test_helper.create_session import create_test_session
import edauth
from parcc_common.security.constants import RolesConstants
from edcore.security.tenant import set_tenant_map
from reporting.reports.helpers.constants import RptTable
from reporting.security.roles.pii import PII  # @UnusedImport
from reporting.reports.helpers.report_table_map import ReportTableMap
from edextract.celery import setup_celery


class TestContext(Unittest_with_edcore_sqlite):
    def setUp(self):
        cache_opts = {
            'cache.type': 'memory',
            'cache.regions': 'public.data'
        }
        CacheManager(**parse_cache_config_options(cache_opts))

        self.__request = DummyRequest()
        # Must set hook_zca to false to work with unittest_with_sqlite
        self.__config = testing.setUp(request=self.__request, hook_zca=False)
        defined_roles = [(Allow, RolesConstants.PII, ('view', 'logout'))]
        edauth.set_roles(defined_roles)
        set_tenant_map({get_unittest_tenant_name(): 'RI'})
        # Set up context security
        dummy_session = create_test_session([RolesConstants.PII])
        self.__config.testing_securitypolicy(dummy_session.get_user())
        # celery settings for UT
        settings = {'extract.celery.CELERY_ALWAYS_EAGER': True}
        self.__request.cookies = {'edware': '123'}
        setup_celery(settings)

    def tearDown(self):
        # reset the registry
        testing.tearDown()

    def testStateContext(self):
        results = get_breadcrumbs_context(RptTable.MATH_SUM, state_code='RI')
        self.assertEqual(len(results['items']), 2)
        self.assertEqual(results['items'][0]['name'], 'Home')
        self.assertEqual(results['items'][0]['type'], 'home')
        self.assertEqual(results['items'][1]['name'], 'Rhode Island')
        self.assertEqual(results['items'][1]['id'], 'RI')
        self.assertEqual(results['items'][1]['type'], 'state')

    def testDistrictContext(self):
        results = get_breadcrumbs_context(RptTable.MATH_SUM, state_code='RI', district_guid='R0003')
        self.assertEqual(len(results['items']), 3)
        self.assertEqual(results['items'][1]['name'], 'Rhode Island')
        self.assertEqual(results['items'][1]['type'], 'state')
        self.assertEqual(results['items'][2]['name'], 'Providence')
        self.assertEqual(results['items'][2]['type'], 'district')
        self.assertEqual(results['items'][2]['id'], 'R0003')

    def testSchoolContext(self):
        results = get_breadcrumbs_context(RptTable.MATH_SUM, state_code='RI', district_guid='R0003', school_guid='RP001')
        self.assertEqual(len(results['items']), 4)
        self.assertEqual(results['items'][1]['name'], 'Rhode Island')
        self.assertEqual(results['items'][2]['name'], 'Providence')
        self.assertEqual(results['items'][3]['name'], 'Alvarez High School')
        self.assertEqual(results['items'][3]['id'], 'RP001')
        self.assertEqual(results['items'][3]['type'], 'school')

    def testGradeContext(self):
        results = get_breadcrumbs_context(RptTable.MATH_SUM, state_code='RI', district_guid='R0003', school_guid='RP001', asmt_grade='09')
        self.assertEqual(len(results['items']), 5)
        self.assertEqual(results['items'][4]['name'], '09')
        self.assertEqual(results['items'][4]['id'], '09')
        self.assertEqual(results['items'][4]['type'], 'grade')

    def testStudentContext(self):
        results = get_breadcrumbs_context(RptTable.MATH_SUM, state_code='RI', district_guid='R0003', school_guid='RP001', asmt_grade='9', student_guid='hg02kpP44BJM9WGFlA0mRLg0cjzMSdEZR6vPrOdQ')
        self.assertEqual(len(results['items']), 6)
        self.assertEqual(results['items'][5]['name'], 'Jacquline Forst')
        self.assertEqual(results['items'][5]['type'], 'student')

    def test_No_state_code(self):
        results = get_breadcrumbs_context(RptTable.MATH_SUM)
        self.assertEqual(len(results['items']), 1)
        self.assertEqual(results['items'][0]['name'], 'Home')
        self.assertEqual(results['items'][0]['type'], 'home')

if __name__ == "__main__":
    unittest.main()
