'''
Created on May 9, 2013

@author: dip
'''
import unittest
from reporting.security.roles.base import BaseRole
from pyramid.security import Allow
from parcc_common.security.constants import RolesConstants
import edauth
from edcore.tests.utils.unittest_with_edcore_sqlite import get_unittest_tenant_name,\
    UnittestEdcoreDBConnection, Unittest_with_edcore_sqlite
from edcore.security.tenant import set_tenant_map
from edauth.tests.test_helper.create_session import create_test_session
from edauth.security.user import RoleRelation
from pyramid.testing import DummyRequest
from pyramid import testing
from edextract.celery import setup_celery


class TestBase(Unittest_with_edcore_sqlite):

    def setUp(self):
        defined_roles = [(Allow, RolesConstants.PII, ('view', 'logout')),
                         (Allow, RolesConstants.SF_EXTRACT, ('view', 'logout'))]
        edauth.set_roles(defined_roles)
        self.tenant = get_unittest_tenant_name()
        set_tenant_map({self.tenant: "RI"})
        dummy_session = create_test_session([RolesConstants.PII])
        dummy_session.set_user_context([RoleRelation(RolesConstants.PII, get_unittest_tenant_name(), "RI", "R0003", "RP002"),
                                        RoleRelation(RolesConstants.SF_EXTRACT, get_unittest_tenant_name(), "RI", "R0003", "RP002")])
        self.user = dummy_session.get_user()
        self.__request = DummyRequest()
        self.__config = testing.setUp(request=self.__request, hook_zca=False)
        self.__config.testing_securitypolicy(self.user)
        settings = {'extract.celery.CELERY_ALWAYS_EAGER': True}
        setup_celery(settings)

    def test_check_context(self):
        with UnittestEdcoreDBConnection() as connection:
            base = BaseRole(connection, 'base')
            context = base.check_context(self.tenant, 'RI', self.user, ['nostudent'])
            self.assertFalse(context)

    def test_check_context_with_context(self):
        with UnittestEdcoreDBConnection() as connection:
            base = BaseRole(connection, RolesConstants.PII)
            student_ids = ['c0pT5dPt1QuTicyE2IWo13jRf8kh2ep7ZI8flg9g']
            context = base.check_context(self.tenant, 'RI', self.user, student_ids)
            self.assertTrue(context)

if __name__ == "__main__":
    unittest.main()
