'''
Created on May 7, 2013

@author: dip
'''
import unittest
from pyramid import testing
from pyramid.testing import DummyRequest
from edcore.tests.utils.unittest_with_edcore_sqlite import Unittest_with_edcore_sqlite, \
    UnittestEdcoreDBConnection, get_unittest_tenant_name
from reporting.security.context import check_context, select_with_context,\
    get_current_context, is_permitted
# Import the roles below so test can run as a standalone
from edauth.tests.test_helper.create_session import create_test_session
from pyramid.security import Allow
import edauth
from edcore.security.tenant import set_tenant_map
from reporting.reports.helpers.constants import RptTable, Constants
from parcc_common.security.constants import RolesConstants
from edauth.security.user import RoleRelation
from reporting.security.roles.pii import PII  # @UnusedImport
from reporting.security.roles.state_level import StateLevel  # @UnusedImport
from edextract.celery import setup_celery
from edcore.utils.utils import compile_query_to_sql_text


class TestContext(Unittest_with_edcore_sqlite):

    def setUp(self):
        self.__request = DummyRequest()
        # Must set hook_zca to false to work with unittest_with_sqlite
        self.__config = testing.setUp(request=self.__request, hook_zca=False)
        self.__tenant_name = get_unittest_tenant_name()
        set_tenant_map({self.__tenant_name: "RI"})
        defined_roles = [(Allow, RolesConstants.PII, ('view', 'logout')),
                         (Allow, RolesConstants.SF_EXTRACT, ('view', 'logout')),
                         (Allow, RolesConstants.PF_EXTRACT, ('view', 'logout')),
                         (Allow, RolesConstants.RF_EXTRACT, ('view', 'logout'))]
        edauth.set_roles(defined_roles)
        dummy_session = create_test_session([RolesConstants.PII])
        dummy_session.set_user_context([RoleRelation(RolesConstants.PII, get_unittest_tenant_name(), "RI", "R0001", "RN001"),
                                        RoleRelation(RolesConstants.PII, get_unittest_tenant_name(), "RI", "R0001", "RN002"),
                                        RoleRelation(RolesConstants.SF_EXTRACT, get_unittest_tenant_name(), 'RI', None, None),
                                        RoleRelation(RolesConstants.PF_EXTRACT, get_unittest_tenant_name(), 'RI', None, None),
                                        RoleRelation(RolesConstants.RF_EXTRACT, get_unittest_tenant_name(), 'RI', None, None)])
        # For Context Security, we need to save the user object
        self.__config.testing_securitypolicy(dummy_session.get_user())
        settings = {'extract.celery.CELERY_ALWAYS_EAGER': True}
        setup_celery(settings)

    def tearDown(self):
        # reset the registry
        testing.tearDown()

    def test_select_with_context_as_pii(self):
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.ELA_SUM)
            query = select_with_context([asmt_table.c.resp_school_id],
                                        from_obj=([asmt_table]), limit=1,
                                        permission=RolesConstants.PII,
                                        state_code='RI')
            results = connection.get_result(query)
            self.assertEqual(len(results), 1)
            self.assertIn(results[0]['resp_school_id'], ['RN001', 'RN002'])

    def test_select_with_context_as_rf_extract(self):
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.MATH_SUM)
            query = select_with_context([asmt_table.c.state_code],
                                        from_obj=([asmt_table]),
                                        limit=1,
                                        permission=RolesConstants.RF_EXTRACT,
                                        state_code='RI')
            results = connection.get_result(query.where(asmt_table.c.resp_dist_id == 'R0003'))
            self.assertEqual(len(results), 1)
            self.assertEqual(results[0][Constants.STATE_CODE], 'RI')

    def test_select_with_context_as_rf_extract_with_two_districts(self):
        dummy_session = create_test_session([RolesConstants.PII])
        dummy_session.set_user_context([RoleRelation(RolesConstants.RF_EXTRACT, get_unittest_tenant_name(), 'RI', 'R0003', None),
                                        RoleRelation(RolesConstants.RF_EXTRACT, get_unittest_tenant_name(), 'RI', 'R0001', None),
                                        RoleRelation(RolesConstants.SF_EXTRACT, get_unittest_tenant_name(), 'RI', None, None)])
        # For Context Security, we need to save the user object
        self.__config.testing_securitypolicy(dummy_session.get_user())
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.MATH_SUM)
            query = select_with_context([asmt_table.c.resp_dist_id],
                                        from_obj=([asmt_table]),
                                        limit=1,
                                        permission=RolesConstants.RF_EXTRACT,
                                        state_code='RI')
            results = connection.get_result(query)
            self.assertEqual(len(results), 1)
            self.assertIn(results[0]['resp_dist_id'], ['R0003', 'R0001'])
            self.assertIn('R0003', compile_query_to_sql_text(query))
            self.assertIn('R0001', compile_query_to_sql_text(query))

    def test_select_with_context_as_sf_extract(self):
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.ELA_SUM)
            query = select_with_context([asmt_table.c.state_code],
                                        from_obj=([asmt_table]),
                                        limit=1,
                                        permission=RolesConstants.SF_EXTRACT,
                                        state_code='RI')
            results = connection.get_result(query.where(asmt_table.c.resp_dist_id == 'R0003'))
            self.assertEqual(len(results), 1)
            self.assertEqual(results[0][Constants.STATE_CODE], 'RI')

    def test_select_with_context_as_sf_extract_with_two_districts(self):
        dummy_session = create_test_session([RolesConstants.PII])
        dummy_session.set_user_context([RoleRelation(RolesConstants.SF_EXTRACT, get_unittest_tenant_name(), 'RI', 'R0003', None),
                                        RoleRelation(RolesConstants.SF_EXTRACT, get_unittest_tenant_name(), 'RI', 'R0001', None),
                                        RoleRelation(RolesConstants.RF_EXTRACT, get_unittest_tenant_name(), 'RI', None, None)])
        # For Context Security, we need to save the user object
        self.__config.testing_securitypolicy(dummy_session.get_user())
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.MATH_SUM)
            query = select_with_context([asmt_table.c.resp_dist_id],
                                        from_obj=([asmt_table]),
                                        limit=1,
                                        permission=RolesConstants.SF_EXTRACT,
                                        state_code='RI')
            results = connection.get_result(query)
            self.assertEqual(len(results), 1)
            self.assertIn(results[0]['resp_dist_id'], ['R0003', 'R0001'])
            self.assertIn('R0003', compile_query_to_sql_text(query))
            self.assertIn('R0001', compile_query_to_sql_text(query))

    def test_select_with_context_as_pf_extract(self):
        with UnittestEdcoreDBConnection() as connection:
            asmt_table = connection.get_table(RptTable.MATH_SUM)
            query = select_with_context([asmt_table.c.state_code],
                                        from_obj=([asmt_table]),
                                        limit=1,
                                        permission=RolesConstants.PF_EXTRACT,
                                        state_code='RI')
            results = connection.get_result(query.where(asmt_table.c.resp_dist_id == 'R0003'))
            self.assertEqual(len(results), 1)
            self.assertEqual(results[0][Constants.STATE_CODE], 'RI')

    def test_check_context_with_empty_guids(self):
        context = check_context('base', 'RI', [])
        self.assertFalse(context)

    def test_check_context_with_context_as_pii(self):
        context = check_context(RolesConstants.PII, 'RI', ['EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q'])
        self.assertTrue(context)

    def test_check_context_with_context_as_default(self):
        context = check_context('base', 'RI', ['EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q'])
        self.assertTrue(context)

    def test_get_current_context_at_state_level(self):
        context = get_current_context({'stateCode': 'RI'})
        self.assertTrue(context['pii']['all'])
        self.assertTrue(context['sf_extract'])
        self.assertTrue(context['pf_extract'])
        self.assertTrue(context['rf_extract'])
        self.assertTrue(context['display_extract'])

    def test_get_current_context_at_state_level_with_invalid_state(self):
        context = get_current_context({'stateCode': 'AA'})
        self.assertTrue(context['pii']['all'])
        self.assertFalse(context['sf_extract'])
        self.assertFalse(context['pf_extract'])
        self.assertFalse(context['rf_extract'])
        self.assertFalse(context['display_extract'])

    def test_get_current_context_at_district_level(self):
        context = get_current_context({'stateCode': 'RI', 'districtGuid': 'someDistrict'})
        self.assertTrue(context['pii']['all'])
        self.assertTrue(context['sf_extract'])
        self.assertTrue(context['pf_extract'])
        self.assertTrue(context['rf_extract'])
        self.assertTrue(context['display_extract'])

    def test_get_current_context_at_district_level_with_invalid_district(self):
        context = get_current_context({'stateCode': 'AB', 'districtGuid': 'ABC'})
        self.assertTrue(context['pii']['all'])
        self.assertFalse(context['sf_extract'])
        self.assertFalse(context['pf_extract'])
        self.assertFalse(context['rf_extract'])
        self.assertFalse(context['display_extract'])

    def test_get_current_context_at_school_level(self):
        context = get_current_context({'stateCode': 'RI', 'districtGuid': 'R0001', 'schoolGuid': 'RN001'})
        self.assertTrue(context['pii']['all'])
        self.assertTrue(context['sf_extract'])
        self.assertTrue(context['pf_extract'])
        self.assertTrue(context['rf_extract'])
        self.assertTrue(context['display_extract'])

    def test_get_current_context_at_school_level_with_invalid_school(self):
        context = get_current_context({'stateCode': 'RI', 'districtGuid': 'R0001', 'schoolGuid': 'BAD'})
        self.assertFalse(context['pii']['all'])
        self.assertTrue(context['sf_extract'])
        self.assertTrue(context['pf_extract'])
        self.assertTrue(context['rf_extract'])
        self.assertTrue(context['display_extract'])

    def test_consortium_level(self):
        dummy_session = create_test_session([RolesConstants.PII])
        dummy_session.set_user_context([RoleRelation(RolesConstants.PII, None, None, None, None),
                                        RoleRelation(RolesConstants.SF_EXTRACT, None, None, None, None),
                                        RoleRelation(RolesConstants.PF_EXTRACT, None, None, None, None),
                                        RoleRelation(RolesConstants.RF_EXTRACT, None, None, None, None)])
        # For Context Security, we need to save the user object
        self.__config.testing_securitypolicy(dummy_session.get_user())
        context = get_current_context({'stateCode': 'RI', 'districtGuid': 'R0001', 'schoolGuid': 'RN001'})
        self.assertTrue(context['pii']['all'])
        self.assertTrue(context['sf_extract'])
        self.assertTrue(context['pf_extract'])
        self.assertTrue(context['rf_extract'])
        self.assertTrue(context['display_extract'])

    def test_state_level(self):
        dummy_session = create_test_session([RolesConstants.PII])
        dummy_session.set_user_context([RoleRelation(RolesConstants.PII, get_unittest_tenant_name(), 'RI', None, None),
                                        RoleRelation(RolesConstants.SF_EXTRACT, get_unittest_tenant_name(), 'RI', None, None),
                                        RoleRelation(RolesConstants.RF_EXTRACT, get_unittest_tenant_name(), 'RI', None, None),
                                        RoleRelation(RolesConstants.PF_EXTRACT, None, None, None, None)])
        # For Context Security, we need to save the user object
        self.__config.testing_securitypolicy(dummy_session.get_user())
        context = get_current_context({'stateCode': 'RI', 'districtId': '229', 'schoolId': '242'})
        self.assertTrue(context['pii']['all'])
        self.assertTrue(context['sf_extract'])
        self.assertTrue(context['pf_extract'])
        self.assertTrue(context['rf_extract'])
        self.assertTrue(context['display_extract'])

    def test_is_permission_allowed(self):
        self.assertTrue(is_permitted(RolesConstants.PII, {'stateCode': 'RI'}))

    def test_is_permission_allowed_on_state_level(self):
        self.assertFalse(is_permitted(RolesConstants.PII, {'stateCode': 'BA'}))

    def test_is_permission_allowed_on_bad_school(self):
        self.assertFalse(is_permitted(RolesConstants.PII, {'stateCode': 'RI', 'districtGuid': '123', 'schoolGuid': '456'}))

    def test_is_permission_allowed_on_valid_school(self):
        self.assertTrue(is_permitted(RolesConstants.PII, {'stateCode': 'RI', 'districtGuid': 'R0001', 'schoolGuid': 'RN001'}))

if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
