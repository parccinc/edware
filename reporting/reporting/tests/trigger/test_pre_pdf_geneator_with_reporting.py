'''
Created on Jun 24, 2013

@author: dip
'''
import unittest

from edcore.tests.utils.unittest_with_edcore_sqlite import get_unittest_tenant_name

from pyramid.registry import Registry
from reporting.reports.helpers.constants import Constants
from reporting.tests.reports.base import BaseReportTestCase
from reporting.trigger.pre_pdf_generator import prepare_pre_pdf, trigger_pre_pdf
import services
from zope import component
from edauth.security.session_backend import ISessionBackend, SessionBackend
from services.celery import setup_celery


class TestPrePdfGenerator(BaseReportTestCase):

    def setUp(self):
        self.tenant = get_unittest_tenant_name()
        reg = Registry()
        reg.settings = {}
        BaseReportTestCase.setUp(self, registry=reg)

    def tearDown(self):
        pass

    def test_prepare_pre_pdf(self):
        results = prepare_pre_pdf(self.tenant, 'RI', '208b01b1-ba86-4b59-ab19-a4f1b20ae9f9', 'subject2')
        self.assertEqual(23, len(results))

    def test_prepare_pre_pdf_with_future_date(self):
        results = prepare_pre_pdf(self.tenant, 'NC', '2d3c017c-6a26-47f2-b4d1-78a6a5260a55', 'subject2')
        self.assertEqual(0, len(results))

    def test_trigger_pre_pdf_with_empty_results(self):
        triggered = trigger_pre_pdf({}, self.tenant, 'NC', [])
        self.assertFalse(triggered)

    def test_trigger_pre_pdf(self):
        settings = {'pdf.base.url': 'http://dummy:1223',
                    'pdf.batch.job.queue': 'dummy',
                    'pdf.health_check.job.queue': 'dummy',
                    'batch.user.session.timeout': 10000,
                    'auth.policy.secret': 'dummySecret',
                    'auth.policy.cookie_name': 'dummy',
                    'auth.policy.hashalg': 'sha1',
                    'celery.CELERY_ALWAYS_EAGER': True,
                    'pdf.minimum_file_size': 0,
                    'cache.regions': 'public.data, session',
                    'cache.type': 'memory'
                    }
        component.provideUtility(SessionBackend(settings), ISessionBackend)
        services.tasks.pdf.pdf_procs = ['echo', 'dummy']
        setup_celery(settings, "celery")

        results = [{Constants.SCHOOL_ID: 'RP001',
                    Constants.DISTRICT_GUID: 'R0003',
                    Constants.ASMT_YEAR: '2014-2015',
                    Constants.ASMT_GRADE: 'Grade 3',
                    Constants.STUDENT_ID: '34140997-8949-497e-bbbb-5d72aa7dc9cb',
                    Constants.SUBJECT: 'subject1',
                    Constants.ASMT_TYPE: 'SUMMATIVE'}]
        triggered = trigger_pre_pdf(settings, 'NC', self.tenant, results)
        self.assertTrue(triggered)


if __name__ == "__main__":
    unittest.main()
