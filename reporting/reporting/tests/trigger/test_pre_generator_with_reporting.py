'''
Created on Jun 22, 2013

@author: tosako
'''
import unittest
from unittest.mock import patch

from edcore.tests.utils.unittest_with_edcore_sqlite import get_unittest_tenant_name
from beaker.cache import cache_managers, CacheManager, cache_regions
from beaker.util import parse_cache_config_options
from reporting.trigger.pre_cache_generator import get_params_for_pre_caching, \
    trigger_precache
from reporting.trigger.cache.recache import read_config_from_json_file
import os
from reporting.tests.reports.base import BaseReportTestCase
from pyramid.registry import Registry


class TestPreCacheGenerator(BaseReportTestCase):

    def setUp(self):
        self.tenant = get_unittest_tenant_name()
        reg = Registry()
        reg.settings = {}
        reg.settings['min_cell_size.performance.{0}'.format(self.tenant)] = 2
        reg.settings['min_cell_size.growth.{0}'.format(self.tenant)] = 3
        reg.settings['alert_cell_size'] = 5
        BaseReportTestCase.setUp(self, registry=reg)
        cache_managers.clear()
        cache_opts = {
            'cache.type': 'memory',
            'cache.regions': 'public.data'
        }
        CacheManager(**parse_cache_config_options(cache_opts))

    def test_get_params_for_pre_caching(self):
        results = get_params_for_pre_caching(self.tenant, 'e4518e63-2f2c-47c2-85d6-d906cf9cc3ef')
        self.assertEqual(6, len(results))

    def test_get_params_for_pre_caching_nodata(self):
        results = get_params_for_pre_caching(self.tenant, '2cf08036-ddb0-11e2-a15e-68a86d3c2f82')
        self.assertEqual(0, len(results))

    @patch('reporting.reports.base_report.BaseReport.get_results')
    def testTrigger_precache(self, get_results):
        get_results.return_value = {'blabla': 'blabla'}
        results = get_params_for_pre_caching(self.tenant, 'e4518e63-2f2c-47c2-85d6-d906cf9cc3ef')
        triggered = trigger_precache(self.tenant, results, {})
        self.assertTrue(triggered)

    @patch('reporting.reports.summative.compare_pop_report.ComparingPopReport.get_unfiltered_results')
    def testTrigger_precache_with_bad_district(self, get_results):
        get_results.return_value = {'blabla': 'blabla'}
        results = [{
            'state_code': 'RI',
            'resp_dist_id': 'I_dont_exist',
            'year': 2014,
            'subject': 'subject1',
            'grade_course': 'dont_exist',
        }]
        triggered = trigger_precache(self.tenant, results, {})
        self.assertFalse(triggered)

    @patch('reporting.reports.base_report.BaseReport.get_results')
    def testTrigger_precache_with_invalid_state(self, get_results):
        get_results.return_value = {'blabla': 'blabla'}
        results = [{
            'state_code': 'I_dont_exist',
            'resp_dist_id': None,
            'year': 2014,
            'subject': 'subject1',
            'grade_course': 'dont_exist',
        }]
        triggered = trigger_precache(self.tenant, results, {})
        self.assertFalse(triggered)

    def testTrigger_precache_with_empty_results(self):
        results = get_params_for_pre_caching(self.tenant, '820568d0-ddaa-11e2-a63d-68a86d3c2f82')
        triggered = trigger_precache(self.tenant, results, {})
        self.assertFalse(triggered)

    def testTrigger_precache_with_unconfigured_region(self):
        # Clears all cache regions
        cache_regions.clear()
        results = get_params_for_pre_caching(self.tenant, '820568d0-ddaa-11e2-a63d-68a86d3c2f82')
        triggered = trigger_precache(self.tenant, results, {})
        self.assertFalse(triggered)

    def test_read_config_file_with_invalid_file(self):
        self.assertRaises(Exception, read_config_from_json_file, '../I_dont_exist.json')

    def test_read_config_file_with_valid_file(self):
        cwd = os.path.abspath(os.path.dirname(__file__))
        data = read_config_from_json_file(os.path.join(cwd, 'resource/filter.json'))
        self.assertEqual(len(data.keys()), 3)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
