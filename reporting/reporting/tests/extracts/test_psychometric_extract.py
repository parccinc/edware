'''
Created on Jun 7, 2015

@author: dip
'''
from pyramid.registry import Registry
import tempfile
from beaker.cache import CacheManager, cache_managers
from beaker.util import parse_cache_config_options
from pyramid.testing import DummyRequest
from pyramid import testing
from pyramid.security import Allow
from parcc_common.security.constants import RolesConstants
import edauth
from edcore.tests.utils.unittest_with_edcore_sqlite import get_unittest_tenant_name,\
    Unittest_with_edcore_sqlite
from edauth.tests.test_helper.create_session import create_test_session
from edcore.security.tenant import set_tenant_map
from reporting.reports.helpers.constants import Constants, RptTable
import shutil
from edextract.tasks.constants import ExtractionDataType
from reporting.security.roles.pii import PII  # @UnusedImport
from reporting.extracts.psychometric import PsychometricExtract
from edapi.exceptions import ForbiddenError
from edauth.security.user import RoleRelation
from unittest.mock import patch
from reporting.extracts.base_extract import start_extract, create_new_task
from parcc_common.extracts.constants import ExtractConstants


class TestPsychometricExtract(Unittest_with_edcore_sqlite):

    def setUp(self):
        self.reg = Registry()
        self.work_zone_dir = tempfile.mkdtemp()
        self.reg.settings = {'extract.work_zone_base_dir': self.work_zone_dir,
                             'extract.job.queue.async': 'myqueue'}
        cache_opts = {
            'cache.type': 'memory',
            'cache.regions': 'public.data,public.filtered_data,public.shortlived'
        }
        CacheManager(**parse_cache_config_options(cache_opts))
        # Set up user context
        self.__request = DummyRequest()
        # Must set hook_zca to false to work with unittest_with_sqlite
        self.__config = testing.setUp(registry=self.reg, request=self.__request, hook_zca=False)
        defined_roles = [(Allow, RolesConstants.PF_EXTRACT, ('view', 'logout'))]
        edauth.set_roles(defined_roles)
        set_tenant_map({get_unittest_tenant_name(): 'RI'})
        dummy_session = create_test_session([RolesConstants.PF_EXTRACT])
        self.__config.testing_securitypolicy(dummy_session.get_user())

    def tearDown(self):
        shutil.rmtree(self.work_zone_dir, ignore_errors=True)
        testing.tearDown()
        cache_managers.clear()

    def test_bad_permission_state_level(self):
        dummy_session = create_test_session([RolesConstants.PII])
        self.__config.testing_securitypolicy(dummy_session.get_user())
        extract = PsychometricExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        self.assertRaises(ForbiddenError, start_extract, extract)

    def test_bad_permission_district_level(self):
        dummy_session = create_test_session([RolesConstants.PF_EXTRACT])
        dummy_session.set_user_context([RoleRelation(RolesConstants.PF_EXTRACT, get_unittest_tenant_name(), "AB", "R0001", None)])
        self.__config.testing_securitypolicy(dummy_session.get_user())
        extract = PsychometricExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        self.assertRaises(ForbiddenError, start_extract, extract)

    @patch('reporting.extracts.base_extract.start_remote_extract')
    @patch('reporting.extracts.base_extract.BaseExtract.register_file')
    def test_check_permission_with_user_at_state_level(self, mock_register, mock_extract):
        mock_register.return_value = ("id", "http://url", "http://realurl")
        mock_extract.return_value = True
        dummy_session = create_test_session([RolesConstants.PF_EXTRACT])
        dummy_session.set_user_context([RoleRelation(RolesConstants.PF_EXTRACT, get_unittest_tenant_name(), "RI", None, None)])
        self.__config.testing_securitypolicy(dummy_session.get_user())
        extract = PsychometricExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        self.assertTrue(start_extract, extract)

    @patch('reporting.extracts.base_extract.start_remote_extract')
    @patch('reporting.extracts.base_extract.BaseExtract.register_file')
    def test_check_permission_with_user_at_district_level(self, mock_register, mock_extract):
        mock_register.return_value = ("id", "http://url", "http://realurl")
        mock_extract.return_value = True
        dummy_session = create_test_session([RolesConstants.PF_EXTRACT])
        dummy_session.set_user_context([RoleRelation(RolesConstants.PF_EXTRACT, get_unittest_tenant_name(), "RI", "R0001", None)])
        self.__config.testing_securitypolicy(dummy_session.get_user())
        extract = PsychometricExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        self.assertTrue(start_extract, extract)

    def test_check_base_file_name(self):
        extract = PsychometricExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        self.assertListEqual(extract.extraction_data_types, [ExtractionDataType.QUERY_CSV, ExtractionDataType.QUERY_JSON])
        file_name = extract.get_file_name('blah', 'zip')
        self.assertIn('PsychometricItemFile', file_name)
        self.assertIn('2015', file_name)
        self.assertIn('zip', file_name)

    def test_get_table_name(self):
        extract = PsychometricExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        self.assertEqual(extract.get_table_name(None), RptTable.RPT_ITEM_P_DATA)

    def test_get_formatter(self):
        extract = PsychometricExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        formatter = extract.get_formatter()
        self.assertIsNotNone(formatter)

    def test_get_metadata_query(self):
        extract = PsychometricExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        self.assertIsNone(extract.get_metadata_query())

    def test_get_csv_extract_query(self):
        extract = PsychometricExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        query, table_name = next(extract.get_csv_extract_query())
        self.assertIsNotNone(query)
        self.assertEqual(table_name, RptTable.RPT_ITEM_P_DATA)
        self.assertIn(RptTable.RPT_ITEM_P_DATA, str(query))

    def test_create_new_task(self):
        extract = PsychometricExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        task = create_new_task(0, extract, ExtractionDataType.QUERY_CSV, None, 'blah', 'csv')
        self.assertIsNotNone(task)
        self.assertIsNone(task['subject'])
        self.assertEqual(task['extract_type'], ExtractConstants.PSYCHOMETRIC)
        self.assertIsNotNone(task['task_queries'])
        self.assertEqual(task['extraction_data_type'], ExtractionDataType.QUERY_CSV)
        self.assertIn('.csv', task['file_name'])

    def test_create_new_task_json_task(self):
        extract = PsychometricExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        task = create_new_task(1, extract, ExtractionDataType.QUERY_JSON, None, 'blah', 'json')
        self.assertIsNotNone(task)
        self.assertIsNone(task['subject'])
        self.assertEqual(task['extract_type'], ExtractConstants.PSYCHOMETRIC)
        self.assertEqual(task['extraction_data_type'], ExtractionDataType.QUERY_JSON)
        self.assertIsNotNone(task['task_queries'])
        self.assertIn('.json', task['file_name'])

    @patch('hpz_client.frs.http_file_upload.api.post')
    @patch('reporting.extracts.base_extract.BaseExtract.register_file')
    @patch('reporting.extracts.base_extract.start_remote_extract')
    def test_start_extract(self, mock_post, mock_register, mock_extract):
        mock_register.return_value = ("id", "http://url", "http://realurl")
        extract = PsychometricExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        rtn = start_extract(extract)
        self.assertIsNotNone(rtn[0])
        self.assertEqual('http://url', rtn[1])
        self.assertEqual('http://realurl', rtn[2])
        self.assertEqual(rtn[3], extract.request_id)
