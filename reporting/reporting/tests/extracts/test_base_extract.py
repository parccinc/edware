'''
Created on Jun 7, 2015

@author: dip
'''
import unittest
from pyramid.registry import Registry
import tempfile
from beaker.cache import CacheManager, cache_managers
from beaker.util import parse_cache_config_options
from pyramid.testing import DummyRequest
from pyramid import testing
from pyramid.security import Allow
from parcc_common.security.constants import RolesConstants
import edauth
from edcore.tests.utils.unittest_with_edcore_sqlite import get_unittest_tenant_name
from edauth.tests.test_helper.create_session import create_test_session
from edcore.security.tenant import set_tenant_map
from reporting.extracts.base_extract import BaseExtract, create_new_task
from reporting.reports.helpers.constants import Constants, Subject
import shutil
from edextract.exceptions import ExtractionError
import os
from unittest.mock import patch


class MockResponse():
    def __init__(self, json):
        self.__json = json

    def json(self):
        return self.__json


class TestBaseExtract(unittest.TestCase):

    def setUp(self):
        self.reg = Registry()
        self.work_zone_dir = tempfile.mkdtemp()
        self.reg.settings = {'extract.work_zone_base_dir': self.work_zone_dir,
                             'extract.job.queue.async': 'myqueue'}
        cache_opts = {
            'cache.type': 'memory',
            'cache.regions': 'public.data,public.filtered_data,public.shortlived'
        }
        CacheManager(**parse_cache_config_options(cache_opts))
        # Set up user context
        self.__request = DummyRequest()
        # Must set hook_zca to false to work with unittest_with_sqlite
        self.__config = testing.setUp(registry=self.reg, request=self.__request, hook_zca=False)
        defined_roles = [(Allow, RolesConstants.SF_EXTRACT, ('view', 'logout'))]
        edauth.set_roles(defined_roles)
        set_tenant_map({get_unittest_tenant_name(): 'RI'})
        dummy_session = create_test_session([RolesConstants.SF_EXTRACT])
        self.__config.testing_securitypolicy(dummy_session.get_user())

    def tearDown(self):
        shutil.rmtree(self.work_zone_dir, ignore_errors=True)
        testing.tearDown()
        cache_managers.clear()

    def test_user_info(self):
        params = {Constants.STATECODE: 'RI', Constants.YEAR: 2015}
        extract = BaseExtract(**params)
        self.assertEqual(extract.tenant, get_unittest_tenant_name())
        self.assertIsNotNone(extract.request_id)
        self.assertIsNotNone(extract.user)

    def test_set_subject_math(self):
        extract = BaseExtract(**{
            Constants.STATECODE: 'RI',
            Constants.YEAR: 2015,
            Constants.SUBJECT: Constants.SUBJECT1
        })
        self.assertEqual(extract._subject, Constants.SUBJECT1)
        self.assertEqual(extract.subjects, [Constants.SUBJECT1])

    def test_set_subject_ela(self):
        extract = BaseExtract(**{
            Constants.STATECODE: 'RI',
            Constants.YEAR: 2015,
            Constants.SUBJECT: Constants.SUBJECT2
        })
        self.assertEqual(extract._subject, Constants.SUBJECT2)
        self.assertEqual(extract.subjects, [Constants.SUBJECT2])

    def test_set_subject_invalid_subject(self):
        extract = BaseExtract(**{
            Constants.STATECODE: 'RI',
            Constants.YEAR: 2015,
            Constants.SUBJECT: 'bla'
        })
        self.assertRaises(ExtractionError, getattr, extract, 'subjects')

    def test_get_base_file_name(self):
        extract = BaseExtract(**{
            Constants.STATECODE: 'RI',
            Constants.YEAR: 1998,
            Constants.GRADECOURSE: ['03']
        })
        file_name = extract.get_file_name('blah', 'zip')
        self.assertIn('extract', file_name)
        self.assertIn('1998', file_name)
        self.assertIn('.zip', file_name)

    def test_get_queue(self):
        extract = BaseExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        self.assertEqual('myqueue', extract.get_queue())

    def test_get_working_directory(self):
        extract = BaseExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        work_dir = extract.working_directory
        self.assertEqual(os.path.join(self.work_zone_dir, extract.tenant, extract.request_id, 'data'), work_dir)

    def test_get_base_request_directory(self):
        extract = BaseExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        base = extract.base_request_directory
        self.assertEqual(base, os.path.join(self.work_zone_dir, extract.tenant, extract.request_id))

    def test_archive_path(self):
        extract = BaseExtract(**{
            Constants.STATECODE: 'RI',
            Constants.YEAR: 2015,
            Constants.GRADECOURSE: '03'
        })
        path = extract.archive_file_name
        self.assertIn(os.path.join(self.work_zone_dir, extract.tenant, extract.request_id, 'archive'), path)
        self.assertIn('zip', path)

    def test_abstract_methods(self):
        params = {Constants.STATECODE: 'RI', Constants.YEAR: 2015}
        extract = BaseExtract(**params)
        self.assertRaises(NotImplementedError, extract.get_table_name, None)
        self.assertRaises(NotImplementedError, extract.get_formatter)

    def test_get_metadata_query(self):
        params = {Constants.STATECODE: 'RI', Constants.YEAR: 2015}
        extract = BaseExtract(**params)
        self.assertIsNone(extract.get_metadata_query())

    @patch('hpz_client.frs.file_registration.put')
    def test_register_file(self, put_patch):
        put_patch.return_value = MockResponse({'registration_id': 'a1-b2-c3-d4-e1e10', 'url': 'http://somehost:82/download/a1-b2-c3-d4-e1e10', 'web_url': 'http://someotherhost:82/download/a1-b2-c3-d4-e1e10'})
        params = {Constants.STATECODE: 'RI', Constants.YEAR: 2015}
        extract = BaseExtract(**params)
        registration_id, download_url, web_download_url = extract.register_file()
        self.assertIn('a1-b2-c3-d4-e1e10', registration_id)
        self.assertIn('http', download_url)
        self.assertIn('http', web_download_url)

    def test_create_new_task(self):
        extract = BaseExtract(**{
            Constants.STATECODE: 'RI',
            Constants.YEAR: 2015,
            Constants.GRADECOURSE: None,
        })
        self.assertRaises(ExtractionError, create_new_task, 0, extract, 'blah', 'blah', 'blah', 'blah')
