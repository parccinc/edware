'''
Created on Nov 16, 2013

@author: dip
'''

import unittest
from reporting.extracts.format import setup_input_file_format, get_column_mapping


class TestFormat(unittest.TestCase):

    def setUp(self):
        setup_input_file_format()

    def test_mapping(self):
        ela_table_config = get_column_mapping('rpt_ela_sum')['overall']
        self.assertIsNotNone(ela_table_config)
        self.assertEqual(ela_table_config['state_code'], 'StateAbbreviation')
        self.assertEqual(ela_table_config['record_type'], 'RecordType')
        self.assertEqual(ela_table_config['disabil_student'], 'StudentWithDisabilities')

    def test_mapping_invalid(self):
        mapping_config = get_column_mapping('dummy_table')
        self.assertIsNotNone(mapping_config)
