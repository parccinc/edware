'''
Created on Jun 7, 2015

@author: dip
'''
from pyramid.registry import Registry
import tempfile
from beaker.cache import CacheManager, cache_managers
from beaker.util import parse_cache_config_options
from pyramid.testing import DummyRequest
from pyramid import testing
from pyramid.security import Allow
from parcc_common.security.constants import RolesConstants
import edauth
from edcore.tests.utils.unittest_with_edcore_sqlite import get_unittest_tenant_name,\
    Unittest_with_edcore_sqlite
from edauth.tests.test_helper.create_session import create_test_session
from edcore.security.tenant import set_tenant_map
from reporting.reports.helpers.constants import Constants, Subject, RptTable
import shutil
from edextract.exceptions import ExtractionError
from edextract.tasks.constants import ExtractionDataType
from reporting.security.roles.pii import PII  # @UnusedImport
from reporting.extracts.rif import RifExtract
from edapi.exceptions import ForbiddenError
from edauth.security.user import RoleRelation
from unittest.mock import patch
from reporting.extracts.base_extract import start_extract, create_new_task
from parcc_common.extracts.constants import ExtractConstants


class TestRifExtract(Unittest_with_edcore_sqlite):

    def setUp(self):
        self.reg = Registry()
        self.work_zone_dir = tempfile.mkdtemp()
        self.reg.settings = {'extract.work_zone_base_dir': self.work_zone_dir,
                             'extract.job.queue.async': 'myqueue'}
        cache_opts = {
            'cache.type': 'memory',
            'cache.regions': 'public.data,public.filtered_data,public.shortlived'
        }
        CacheManager(**parse_cache_config_options(cache_opts))
        # Set up user context
        self.__request = DummyRequest()
        # Must set hook_zca to false to work with unittest_with_sqlite
        self.__config = testing.setUp(registry=self.reg, request=self.__request, hook_zca=False)
        defined_roles = [(Allow, RolesConstants.RF_EXTRACT, ('view', 'logout'))]
        edauth.set_roles(defined_roles)
        set_tenant_map({get_unittest_tenant_name(): 'RI'})
        dummy_session = create_test_session([RolesConstants.RF_EXTRACT])
        self.__config.testing_securitypolicy(dummy_session.get_user())

    def tearDown(self):
        shutil.rmtree(self.work_zone_dir, ignore_errors=True)
        testing.tearDown()
        cache_managers.clear()

    def test_bad_permission_state_level(self):
        dummy_session = create_test_session([RolesConstants.PII])
        self.__config.testing_securitypolicy(dummy_session.get_user())
        extract = RifExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015,
                                Constants.GRADECOURSE: ['03'],
                                Constants.SUBJECT: Constants.SUBJECT1})
        self.assertRaises(ForbiddenError, start_extract, extract)

    def test_bad_permission_district_level(self):
        dummy_session = create_test_session([RolesConstants.SF_EXTRACT])
        dummy_session.set_user_context([RoleRelation(RolesConstants.RF_EXTRACT, get_unittest_tenant_name(), "AB", "R0001", None)])
        self.__config.testing_securitypolicy(dummy_session.get_user())
        extract = RifExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015,
                                Constants.GRADECOURSE: ['03'],
                                Constants.SUBJECT: Constants.SUBJECT1})
        self.assertRaises(ForbiddenError, start_extract, extract)

    @patch('reporting.extracts.base_extract.start_remote_extract')
    @patch('reporting.extracts.base_extract.BaseExtract.register_file')
    def test_check_permission_with_user_at_state_level(self, mock_register, mock_extract):
        mock_register.return_value = ("id", "http://url", "http://realurl")
        mock_extract.return_value = True
        dummy_session = create_test_session([RolesConstants.SF_EXTRACT])
        dummy_session.set_user_context([RoleRelation(RolesConstants.RF_EXTRACT, get_unittest_tenant_name(), "RI", None, None)])
        self.__config.testing_securitypolicy(dummy_session.get_user())
        extract = RifExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015,
                                Constants.GRADECOURSE: ['03'],
                                Constants.SUBJECT: Constants.SUBJECT1})
        self.assertTrue(start_extract, extract)

    @patch('reporting.extracts.base_extract.start_remote_extract')
    @patch('reporting.extracts.base_extract.BaseExtract.register_file')
    def test_check_permission_with_user_at_district_level(self, mock_register, mock_extract):
        mock_register.return_value = ("id", "http://url", "http://realurl")
        mock_extract.return_value = True
        dummy_session = create_test_session([RolesConstants.SF_EXTRACT])
        dummy_session.set_user_context([RoleRelation(RolesConstants.RF_EXTRACT, get_unittest_tenant_name(), "RI", "R0001", None)])
        self.__config.testing_securitypolicy(dummy_session.get_user())
        extract = RifExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015,
                                Constants.GRADECOURSE: ['03'],
                                Constants.SUBJECT: Constants.SUBJECT1})
        self.assertTrue(start_extract, extract)

    def test_set_subject_ela(self):
        extract = RifExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015,
                                Constants.SUBJECT: Constants.SUBJECT2, Constants.GRADECOURSE: ['03']})
        self.assertEqual(extract.subjects, [Constants.SUBJECT2])

    def test_set_subject_no_subject(self):
        extract = RifExtract(**{
            Constants.STATECODE: 'RI',
            Constants.YEAR: 2015,
            Constants.GRADECOURSE: ['03'],
            Constants.SUBJECT: 'bla'
        })
        self.assertRaises(ExtractionError, getattr, extract, 'subjects')

    def test_set_base_file_name_with_one_grade(self):
        extract = RifExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015,
                                Constants.SUBJECT: Constants.SUBJECT2, Constants.GRADECOURSE: ['03']})
        self.assertListEqual(extract.extraction_data_types, [ExtractionDataType.QUERY_CSV, ExtractionDataType.QUERY_JSON])
        file_name0 = extract.get_file_name(Constants.SUBJECT1, 'zip')
        self.assertTrue(file_name0.startswith("ReleasedItemFile_Summative_Math_03_2014-2015"))
        self.assertIn('zip', file_name0)

    def test_base_file_name_with_multi_grade(self):
        extract = RifExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015,
                                Constants.SUBJECT: Constants.SUBJECT2, Constants.GRADECOURSE: ['03', '04']})
        self.assertListEqual(extract.extraction_data_types, [ExtractionDataType.QUERY_CSV, ExtractionDataType.QUERY_JSON])
        file_name0 = extract.get_file_name(Constants.SUBJECT1, 'zip')
        self.assertTrue(file_name0.startswith("ReleasedItemFile_Summative_Math_multigrade_2014-2015"))
        self.assertIn('zip', file_name0)
        file_name1 = extract.get_file_name(Constants.SUBJECT2, 'zip')
        self.assertTrue(file_name1.startswith("ReleasedItemFile_Summative_ELA_multigrade_2014-2015"))
        self.assertIn('zip', file_name1)

    def test_get_table_name_math(self):
        extract = RifExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015,
                                Constants.SUBJECT: Constants.SUBJECT1, Constants.GRADECOURSE: ['03']})
        self.assertEqual(extract.get_table_name(Constants.SUBJECT1), RptTable.RPT_RIF_MATH)

    def test_get_table_name_ela(self):
        extract = RifExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015,
                                Constants.SUBJECT: Constants.SUBJECT2, Constants.GRADECOURSE: ['03']})
        self.assertEqual(extract.get_table_name(Constants.SUBJECT2), RptTable.RPT_RIF_ELA)

    def test_get_formatter(self):
        extract = RifExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015,
                                Constants.SUBJECT: Constants.SUBJECT2, Constants.GRADECOURSE: ['03']})
        formatter = extract.get_formatter()
        self.assertIsNotNone(formatter)

    def test_get_metadata_query(self):
        extract = RifExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015,
                                Constants.SUBJECT: Constants.SUBJECT2, Constants.GRADECOURSE: ['03']})
        self.assertIsNone(extract.get_metadata_query())

    def test_get_csv_extract_query(self):
        extract = RifExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015,
                                Constants.SUBJECT: Constants.SUBJECT2, Constants.GRADECOURSE: ['03']})
        query, table_name = next(extract.get_csv_extract_query())
        self.assertIsNotNone(query)
        self.assertEqual(table_name, RptTable.RPT_RIF_ELA)
        self.assertIn(RptTable.RPT_RIF_ELA, str(query))

    def test_create_new_task(self):
        extract = RifExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015,
                                Constants.SUBJECT: Constants.SUBJECT2, Constants.GRADECOURSE: ['03']})
        task = create_new_task(0, extract, ExtractionDataType.QUERY_CSV, Constants.SUBJECT2, 'blah', 'csv')
        self.assertIsNotNone(task)
        self.assertEqual(task['subject'], Subject.ELA)
        self.assertEqual(task['extract_type'], ExtractConstants.SUMMATIVE_RIF)
        self.assertIsNotNone(task['task_queries'])
        self.assertEqual(task['extraction_data_type'], ExtractionDataType.QUERY_CSV)
        self.assertIn('.csv', task['file_name'])

    def test_create_new_task_json_task(self):
        extract = RifExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015,
                                Constants.SUBJECT: Constants.SUBJECT1, Constants.GRADECOURSE: ['03']})
        task = create_new_task(1, extract, ExtractionDataType.QUERY_JSON, Constants.SUBJECT1, 'blah', 'json')
        self.assertIsNotNone(task)
        self.assertEqual(task['subject'], Subject.MATH)
        self.assertEqual(task['extract_type'], ExtractConstants.SUMMATIVE_RIF)
        self.assertEqual(task['extraction_data_type'], ExtractionDataType.QUERY_JSON)
        self.assertIsNotNone(task['task_queries'])
        self.assertIn('.json', task['file_name'])

    @patch('hpz_client.frs.http_file_upload.api.post')
    @patch('reporting.extracts.base_extract.BaseExtract.register_file')
    @patch('reporting.extracts.base_extract.start_remote_extract')
    def test_start_extract(self, mock_extract, mock_register, mock_post):
        mock_register.return_value = ("id", "http://url", "http://realurl")
        extract = RifExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015,
                                Constants.SUBJECT: Constants.SUBJECT1, Constants.GRADECOURSE: ['03']})
        rtn = start_extract(extract)
        self.assertIsNotNone(rtn[0])
        self.assertEqual('http://url', rtn[1])
        self.assertEqual('http://realurl', rtn[2])
        self.assertEqual(rtn[3], extract.request_id)
