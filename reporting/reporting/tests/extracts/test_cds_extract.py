'''
Created on Jun 7, 2015

@author: dip
'''
from pyramid.registry import Registry
import tempfile
from beaker.cache import CacheManager, cache_managers
from beaker.util import parse_cache_config_options
from pyramid.testing import DummyRequest
from pyramid import testing
from pyramid.security import Allow
from parcc_common.security.constants import RolesConstants
import edauth
from edcore.tests.utils.unittest_with_edcore_sqlite import get_unittest_tenant_name,\
    Unittest_with_edcore_sqlite
from edauth.tests.test_helper.create_session import create_test_session
from edcore.security.tenant import set_tenant_map, set_cds_tenant_map
from reporting.reports.helpers.constants import Constants, RptTable, Subject
import shutil
from edextract.tasks.constants import ExtractionDataType
from reporting.security.roles.pii import PII  # @UnusedImport
from reporting.extracts.cds import CDSExtract
from edapi.exceptions import ForbiddenError
from edauth.security.user import RoleRelation
from unittest.mock import patch, Mock
from edcore.database.cds_connector import CdsDBConnection
from reporting.extracts.base_extract import start_extract, create_new_task
from parcc_common.extracts.constants import ExtractConstants


class TestCDSExtract(Unittest_with_edcore_sqlite):

    def setUp(self):
        self.reg = Registry()
        self.work_zone_dir = tempfile.mkdtemp()
        self.reg.settings = {'extract.work_zone_base_dir': self.work_zone_dir,
                             'extract.job.queue.async': 'myqueue'}
        cache_opts = {
            'cache.type': 'memory',
            'cache.regions': 'public.data,public.filtered_data,public.shortlived'
        }
        CacheManager(**parse_cache_config_options(cache_opts))
        # Set up user context
        self.__request = DummyRequest()
        # Must set hook_zca to false to work with unittest_with_sqlite
        self.__config = testing.setUp(registry=self.reg, request=self.__request, hook_zca=False)
        defined_roles = [(Allow, RolesConstants.CDS_EXTRACT, ('view', 'logout'))]
        edauth.set_roles(defined_roles)
        set_tenant_map({get_unittest_tenant_name(): 'RI'})
        CdsDBConnection.get_datasource_name = Mock(return_value='edware.db.tomcat')
        set_cds_tenant_map({get_unittest_tenant_name(): 'RI'})
        dummy_session = create_test_session([RolesConstants.CDS_EXTRACT])
        self.__config.testing_securitypolicy(dummy_session.get_user())

    def tearDown(self):
        shutil.rmtree(self.work_zone_dir, ignore_errors=True)
        testing.tearDown()
        cache_managers.clear()

    def test_bad_permission_state_level(self):
        dummy_session = create_test_session([RolesConstants.PII])
        self.__config.testing_securitypolicy(dummy_session.get_user())
        extract = CDSExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        self.assertRaises(ForbiddenError, start_extract, extract)

    def test_bad_permission_district_level(self):
        dummy_session = create_test_session([RolesConstants.CDS_EXTRACT])
        dummy_session.set_user_context([RoleRelation(RolesConstants.CDS_EXTRACT, get_unittest_tenant_name(), "AB", "R0001", None)])
        self.__config.testing_securitypolicy(dummy_session.get_user())
        extract = CDSExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        self.assertRaises(ForbiddenError, start_extract, extract)

    @patch('reporting.extracts.base_extract.start_remote_extract')
    @patch('reporting.extracts.base_extract.BaseExtract.register_file')
    def test_check_permission_with_user_at_state_level(self, mock_register, mock_extract):
        mock_register.return_value = ("id", "http://url", "http://realurl")
        mock_extract.return_value = True
        dummy_session = create_test_session([RolesConstants.CDS_EXTRACT])
        dummy_session.set_user_context([RoleRelation(RolesConstants.CDS_EXTRACT, get_unittest_tenant_name(), "RI", None, None)])
        self.__config.testing_securitypolicy(dummy_session.get_user())
        extract = CDSExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        self.assertTrue(start_extract(extract))

    @patch('reporting.extracts.base_extract.start_remote_extract')
    @patch('reporting.extracts.base_extract.BaseExtract.register_file')
    def test_check_permission_with_user_at_district_level(self, mock_register, mock_extract):
        mock_register.return_value = ("id", "http://url", "http://realurl")
        mock_extract.return_value = True
        dummy_session = create_test_session([RolesConstants.CDS_EXTRACT])
        dummy_session.set_user_context([RoleRelation(RolesConstants.CDS_EXTRACT, get_unittest_tenant_name(), "RI", "R0001", None)])
        self.__config.testing_securitypolicy(dummy_session.get_user())
        extract = CDSExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        self.assertTrue(start_extract(extract))

    def test_check_file_name(self):
        extract = CDSExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        self.assertListEqual(extract.extraction_data_types, [ExtractionDataType.QUERY_CDS])
        file_name = extract.get_file_name(Constants.SUBJECT1, 'zip')
        self.assertIn('zip', file_name)
        file_name0 = extract.get_file_name(Constants.SUBJECT1, 'zip', RptTable.MATH_SUM)
        self.assertTrue(file_name0.startswith('CDSFile_SUMMATIVE_Math_2014-2015'))
        file_name1 = extract.get_file_name(Constants.SUBJECT1, 'zip', RptTable.MATH_STUDENT_ITEM_SCORE)
        self.assertTrue(file_name1.startswith('CDSFile_ITEM_Math_2014-2015'))
        file_name2 = extract.get_file_name(Constants.SUBJECT2, 'zip', RptTable.ELA_SUM)
        self.assertTrue(file_name2.startswith('CDSFile_SUMMATIVE_ELA_2014-2015'))
        file_name3 = extract.get_file_name(Constants.SUBJECT2, 'zip', RptTable.ELA_STUDENT_ITEM_SCORE)
        self.assertTrue(file_name3.startswith('CDSFile_ITEM_ELA_2014-2015'))

    def test_get_table_name(self):
        extract = CDSExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        self.assertEqual(extract.get_table_name(Constants.SUBJECT1), (RptTable.MATH_SUM, RptTable.MATH_STUDENT_ITEM_SCORE))
        self.assertEqual(extract.get_table_name(Constants.SUBJECT2), (RptTable.ELA_SUM, RptTable.ELA_STUDENT_ITEM_SCORE))

    def test_get_formatter(self):
        extract = CDSExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        formatter = extract.get_formatter()
        self.assertIsNotNone(formatter)

    def test_get_csv_extract_query(self):
        extract = CDSExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        query, table_name = next(extract.get_csv_extract_query(Constants.SUBJECT1))
        self.assertIsNotNone(query)
        self.assertEqual(table_name, RptTable.MATH_SUM)
        self.assertEqual(extract.subjects, [Constants.SUBJECT1, Constants.SUBJECT2])

    def test_create_new_task(self):
        extract = CDSExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        task = create_new_task(0, extract, ExtractionDataType.QUERY_CSV, Constants.SUBJECT1, 'bla', 'csv')
        self.assertIsNotNone(task)
        self.assertEqual(task['subject'], Subject.MATH)
        self.assertEqual(task['extract_type'], ExtractConstants.CDS)
        self.assertIsNotNone(task['task_queries'])
        self.assertEqual(task['extraction_data_type'], ExtractionDataType.QUERY_CSV)
        self.assertIn('.csv', task['file_name'])

    @patch('hpz_client.frs.http_file_upload.api.post')
    @patch('reporting.extracts.base_extract.BaseExtract.register_file')
    @patch('reporting.extracts.base_extract.start_remote_extract')
    def test_start_extract(self, mock_post, mock_register, mock_extract):
        mock_register.return_value = ("id", "http://url", "http://realurl")
        extract = CDSExtract(**{Constants.STATECODE: 'RI', Constants.YEAR: 2015})
        rtn = start_extract(extract)
        self.assertIsNotNone(rtn[0])
        self.assertEqual('http://url', rtn[1])
        self.assertEqual('http://realurl', rtn[2])
        self.assertEqual(rtn[3], extract.request_id)
