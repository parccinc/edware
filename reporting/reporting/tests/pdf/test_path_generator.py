'''
Created on May 17, 2013

@author: tosako
'''
import unittest
import os

from pyramid import testing
from pyramid.registry import Registry
from pyramid.testing import DummyRequest
from pyramid.security import Allow
from edcore.tests.utils.unittest_with_edcore_sqlite import Unittest_with_edcore_sqlite,\
    get_unittest_tenant_name
from edapi.exceptions import NotFoundException
from edauth.tests.test_helper.create_session import create_test_session
import edauth
from parcc_common.security.constants import RolesConstants
from edcore.security.tenant import set_tenant_map
from reporting.security.roles.pii import PII  # @UnusedImport
from reporting.reports.helpers.constants import Constants
from reporting.reports.helpers.report_table_map import ReportTableMap
from edextract.celery import setup_celery as setup_extract_celery
from reporting.pdf.path_generator import generate_pdf_path_by_student_id


class TestISRPdfNameFormatter(Unittest_with_edcore_sqlite):

    def setUp(self):
        self.__request = DummyRequest()
        reg = Registry()
        reg.settings = {}
        reg.settings['cache.expire'] = 10
        reg.settings['cache.regions'] = 'session'
        reg.settings['cache.type'] = 'memory'
        self.__config = testing.setUp(registry=reg, request=self.__request, hook_zca=False)
        defined_roles = [(Allow, RolesConstants.PII, ('view', 'logout'))]
        edauth.set_roles(defined_roles)
        set_tenant_map({get_unittest_tenant_name(): 'RI'})
        # Set up context security
        dummy_session = create_test_session([RolesConstants.PII])
        self.__config.testing_securitypolicy(dummy_session.get_user())
        settings = {'extract.celery.CELERY_ALWAYS_EAGER': True}
        setup_extract_celery(settings)

    def test_generate_isr_report_path_by_student_id(self):
        rpt = ReportTableMap(Constants.SUBJECT1)
        file_name = generate_pdf_path_by_student_id(rpt, 'RI', 'R0003', 'RP003', 'Grade 3', 2015, 'Mathematics', pdf_report_base_dir='/', student_ids='D3de8PGASi2QhIZ0zHLSNJXZI0okSm1VqPJMeJAw')
        self.assertEqual(len(file_name), 1)
        self.assertEqual(file_name['D3de8PGASi2QhIZ0zHLSNJXZI0okSm1VqPJMeJAw'], os.path.join('/', 'RI', '2014-2015', 'R0003', 'RP003', 'Grade 3', 'isr', 'SUMMATIVE', 'Mathematics', 'D3de8PGASi2QhIZ0zHLSNJXZI0okSm1VqPJMeJAw.en.g.pdf'))

    def test_generate_isr_report_path_by_student_id_studentguid_not_exist(self):
        rpt = ReportTableMap(Constants.SUBJECT1)
        self.assertRaises(NotFoundException, generate_pdf_path_by_student_id, rpt, 'RI', 'R0002', 'RP003', 'Grade 3', 2015, 'Math', pdf_report_base_dir='/', student_ids='idontexist')

    def test_generate_isr_report_path_by_student_id_for_color(self):
        rpt = ReportTableMap(Constants.SUBJECT1)
        file_name = generate_pdf_path_by_student_id(rpt, 'RI', 'R0003', 'RP003', 'Grade 3', 2015, 'Mathematics', pdf_report_base_dir='/', student_ids='D3de8PGASi2QhIZ0zHLSNJXZI0okSm1VqPJMeJAw', grayScale=False, lang='jp')
        self.assertEqual(len(file_name), 1)
        self.assertEqual(file_name['D3de8PGASi2QhIZ0zHLSNJXZI0okSm1VqPJMeJAw'], os.path.join('/', 'RI', '2014-2015', 'R0003', 'RP003', 'Grade 3', 'isr', 'SUMMATIVE', 'Mathematics', 'D3de8PGASi2QhIZ0zHLSNJXZI0okSm1VqPJMeJAw.jp.pdf'))

    def test_generate_isr_report_path_by_student_id_studentguid_not_existd_for_color(self):
        rpt = ReportTableMap(Constants.SUBJECT1)
        self.assertRaises(NotFoundException, generate_pdf_path_by_student_id, rpt, 'RI', 'R0002', 'RP003', 'Grade 3', 2012, 'ELA', pdf_report_base_dir='/', student_ids='ff1c2b1a-c15d-11e2-ae11-3c07546832b4', grayScale=False)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
