'''
Created on May 17, 2013

@author: dip
'''
import unittest
import tempfile
from os import path
from pyramid.testing import DummyRequest
from pyramid import testing
from pyramid.response import Response
from pyramid.registry import Registry
from edapi.httpexceptions import EdApiHTTPPreconditionFailed, \
    EdApiHTTPForbiddenAccess, EdApiHTTPInternalServerError
from edcore.tests.utils.unittest_with_edcore_sqlite import Unittest_with_edcore_sqlite, \
    get_unittest_tenant_name
import services
from services.celery import setup_celery
from reporting.services.pdf import sync_pdf_service, async_pdf_service
from edapi.exceptions import ForbiddenError
import shutil
from services.tests.tasks.test_pdf import get_cmd
from pyramid.security import Allow
import edauth
from edcore.security.tenant import set_tenant_map
from parcc_common.security.constants import RolesConstants
from edauth.tests.test_helper.create_session import create_test_session
from edauth.security.user import RoleRelation
from reporting.reports.helpers.constants import Constants, AssessmentType
from beaker.cache import CacheManager
from beaker.util import parse_cache_config_options
from services.tasks.pdf import prepare_path
from reporting.pdf.sync_generator import SyncPDFGenerator, SyncGeneratorConfig
from edextract.celery import setup_celery as setup_extract_celery
from reporting.pdf.path_generator import generate_pdf_path_by_student_id


class TestSyncGenerator(Unittest_with_edcore_sqlite):

    def setUp(self):
        cache_opts = {
            'cache.type': 'memory',
            'cache.regions': 'public.shortlived'
        }
        CacheManager(**parse_cache_config_options(cache_opts))
        self.__request = DummyRequest()
        reg = Registry()
        # Set up defined roles
        self.__tenant_name = get_unittest_tenant_name()
        set_tenant_map({self.__tenant_name: "RI"})
        self.__temp_dir = tempfile.mkdtemp()
        reg.settings = {}
        reg.settings['pdf.report_base_dir'] = self.__temp_dir
        reg.settings['pdf.single_generate.queue'] = 'test_queue'
        reg.settings['pdf.generate_timeout'] = 20
        self.__config = testing.setUp(registry=reg, request=self.__request, hook_zca=False)

        defined_roles = [(Allow, RolesConstants.PII, ('view', 'logout'))]
        edauth.set_roles(defined_roles)
        # Set up context security
        dummy_session = create_test_session([RolesConstants.PII])
        dummy_session.set_user_context([RoleRelation(RolesConstants.PII, self.__tenant_name, 'RI', 'R0003', 'RP001'),
                                        RoleRelation(RolesConstants.PII, self.__tenant_name, 'RI', 'R0003', 'RP002')])
        self.__config.testing_securitypolicy(dummy_session.get_user())

        # celery settings for UT
        settings = {'services.celery.CELERY_ALWAYS_EAGER': True}
        self.__request.cookies = {'edware': '123'}
        setup_celery(settings)
        settings = {'extract.celery.CELERY_ALWAYS_EAGER': True}
        setup_extract_celery(settings)

    def tearDown(self):
        shutil.rmtree(self.__temp_dir, ignore_errors=True)
        self.__request = None
        testing.tearDown()

    def test_async_pdf_serivce_with_invalid_payload(self):
        self.__request.method = 'POST'
        self.assertRaises(EdApiHTTPPreconditionFailed, async_pdf_service, None, self.__request)

    def test_async_pdf_service_with_invalid_param(self):
        self.__request.method = 'POST'
        self.__request.json_body = {}
        self.assertRaises(EdApiHTTPPreconditionFailed, async_pdf_service, None, self.__request)

    @unittest.skip("context security is disabled")
    def test_async_pdf_service_no_context(self):
        self.__request.method = 'POST'
        self.__request.json_body = {Constants.STUDENTGUID: '19489898-d469-41e2-babc-265ecbab2337', Constants.STATECODE: 'NC', Constants.YEAR: 2016}
        self.assertRaises(EdApiHTTPForbiddenAccess, async_pdf_service, None, self.__request)
