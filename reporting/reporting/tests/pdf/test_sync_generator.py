'''
Created on May 17, 2013

@author: dip
'''
import unittest
import tempfile
from os import path
from pyramid.testing import DummyRequest
from pyramid import testing
from pyramid.response import Response
from pyramid.registry import Registry
from edapi.httpexceptions import EdApiHTTPPreconditionFailed, \
    EdApiHTTPForbiddenAccess, EdApiHTTPInternalServerError
from edcore.tests.utils.unittest_with_edcore_sqlite import Unittest_with_edcore_sqlite, \
    get_unittest_tenant_name
import services
from services.celery import setup_celery
from reporting.services.pdf import sync_pdf_service
from edapi.exceptions import ForbiddenError
import shutil
from services.tests.tasks.test_pdf import get_cmd
from pyramid.security import Allow
import edauth
from edcore.security.tenant import set_tenant_map
from parcc_common.security.constants import RolesConstants
from edauth.tests.test_helper.create_session import create_test_session
from edauth.security.user import RoleRelation
from reporting.reports.helpers.constants import Constants, AssessmentType
from beaker.cache import CacheManager
from beaker.util import parse_cache_config_options
from services.tasks.pdf import prepare_path
from reporting.pdf.sync_generator import SyncPDFGenerator, SyncGeneratorConfig
from edextract.celery import setup_celery as setup_extract_celery
from reporting.pdf.path_generator import generate_pdf_path_by_student_id
from reporting.reports.helpers.report_table_map import ReportTableMap


class TestSyncGenerator(Unittest_with_edcore_sqlite):

    def setUp(self):
        self.setup_dummy_request()
        self.setup_registry()
        self.setup_dummy_session()
        self.setup_celery()

        # prepare empty file
        self.prepare_empty_pdf()

    def setup_registry(self):
        reg = Registry()
        # Set up defined roles
        self.__tenant_name = get_unittest_tenant_name()
        set_tenant_map({self.__tenant_name: "RI"})
        self.__temp_dir = tempfile.mkdtemp()
        reg.settings = {}
        reg.settings['pdf.report_base_dir'] = self.__temp_dir
        reg.settings['pdf.single_generate.queue'] = 'test_queue'
        reg.settings['pdf.generate_timeout'] = 20
        self.__config = testing.setUp(registry=reg, request=self._request, hook_zca=False)

    def setup_dummy_session(self):
        # setup cache
        cache_opts = {
            'cache.type': 'memory',
            'cache.regions': 'public.shortlived'
        }
        CacheManager(**parse_cache_config_options(cache_opts))
        # setup role
        defined_roles = [(Allow, RolesConstants.PII, ('view', 'logout'))]
        edauth.set_roles(defined_roles)
        # Set up context security
        dummy_session = create_test_session([RolesConstants.PII])
        dummy_session.set_user_context([
            RoleRelation(RolesConstants.PII, self.__tenant_name, 'RI', 'R0001', 'RN001')
        ])
        self.__config.testing_securitypolicy(dummy_session.get_user())

    def setup_dummy_request(self):
        # prepare request
        self._request = DummyRequest()
        self._request.cookies = {'edware': '123'}
        self._request.method = 'GET'
        self._params = {
            Constants.STATECODE: 'RI',
            Constants.DISTRICTGUID: 'R0001',
            Constants.SCHOOLGUID: 'RN001',
            Constants.GRADECOURSE: 'Grade 9',
            Constants.STUDENTGUID: 'EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q',
            Constants.ASMTTYPE: AssessmentType.SUMMATIVE,
            Constants.YEAR: 2015,
            Constants.SUBJECT: 'subject2',
        }
        self._request.GET = self._params

    def setup_celery(self):
        # celery settings for UT
        settings = {'services.celery.CELERY_ALWAYS_EAGER': True}
        setup_celery(settings)
        settings = {'extract.celery.CELERY_ALWAYS_EAGER': True}
        setup_extract_celery(settings)

    def prepare_empty_pdf(self):
        studentId = self._params[Constants.STUDENTGUID]
        rpt = ReportTableMap(Constants.SUBJECT2)
        pdf_file = generate_pdf_path_by_student_id(rpt,
                                                   'RI',
                                                   'R0001',
                                                   'RN001',
                                                   'Grade 9',
                                                   2015,
                                                   'ELA',
                                                   pdf_report_base_dir=self.__temp_dir,
                                                   student_ids=studentId,
                                                   asmt_type=AssessmentType.SUMMATIVE)
        prepare_path(pdf_file[studentId])
        with open(pdf_file[studentId], 'w') as file:
            file.write('%PDF-1.4')

    def tearDown(self):
        shutil.rmtree(self.__temp_dir, ignore_errors=True)
        self._request = None
        testing.tearDown()

    def test_sync_pdf_service_invalid_param(self):
        self._request.GET = {}
        self.assertRaises(EdApiHTTPPreconditionFailed, sync_pdf_service, None, self._request)

    def test_sync_pdf_service(self):
        # Override the wkhtmltopdf command
        services.tasks.pdf.pdf_procs = ['echo', 'dummy']
        response = sync_pdf_service(None, self._request)
        self.assertEqual(response.content_type, Constants.APPLICATION_PDF)

    def test_get_pdf_valid_params(self):
        # Override the wkhtmltopdf command
        services.tasks.pdf.pdf_procs = ['echo', 'dummy']
        response = sync_pdf_service(None, self._request)
        self.assertIsInstance(response, Response)
        self.assertIsNotNone(response.body)
        self.assertEqual(response.content_type, Constants.APPLICATION_PDF)

    def test_send_pdf_request(self):
        response = SyncPDFGenerator(**self._params).run()
        self.assertIsInstance(response, Response)
        self.assertEqual(response.content_type, Constants.APPLICATION_PDF)
        self.assertIsInstance(response.body, bytes)

    def test_send_pdf_request_with_pdf_generation_fail(self):
        # Important, this pdf must not exist in directory
        self._params[Constants.STUDENTGUID] = 'iddoesnotexist'
        services.tasks.pdf.pdf_procs = get_cmd()
        generator = SyncPDFGenerator(**self._params).run
        self.assertRaises(ForbiddenError, generator)

    def test_send_pdf_request_with_always_generate_flag(self):
        self.__config.registry.settings['pdf.always_generate'] = 'True'
        services.tasks.pdf.pdf_procs = get_cmd()
        generator = SyncPDFGenerator(**self._params).run
        self.assertRaises(EdApiHTTPInternalServerError, generator)

    @unittest.skip("no context security right now")
    def test_get_pdf_content_with_no_context(self):
        params = {}
        params[Constants.STUDENTGUID] = '19489898-d469-41e2-babc-265ecbab2337'
        params[Constants.STATECODE] = 'NC'
        params[Constants.ASMTTYPE] = 'SUMMATIVE'
        params[Constants.YEAR] = 2016
        self._request.cookies = {'edware': '123'}
        generator = SyncPDFGenerator(**params).run
        self.assertRaises(ForbiddenError, generator)

    @unittest.skip("no context security right now")
    def test_has_context_for_pdf_request(self):
        student_id = 'a5ddfe12-740d-4487-9179-de70f6ac33be'
        has_context = _has_context_for_pdf_request('NC', student_id)
        self.assertTrue(has_context)

    @unittest.skip("no context security right now")
    def test_has_context_for_pdf_request_with_no_context(self):
        student_id = 'invalid'
        has_context = _has_context_for_pdf_request('NC', student_id)
        self.assertFalse(has_context)

    @unittest.skip("context security is disabled")
    def test_sync_pdf_service_no_context(self):
        self._request.GET = {
            Constants.STUDENTGUID: '19489898-d469-41e2-babc-265ecbab2337',
            Constants.STATECODE: 'NC',
            Constants.YEAR: 2016
        }
        self.assertRaises(EdApiHTTPForbiddenAccess, sync_pdf_service, None, self._request)


class TestSyncGeneratorConfig(Unittest_with_edcore_sqlite):

    def setUp(self):
        # init request
        self._request = DummyRequest()
        self._request.cookies = {'edware': '123'}
        reg = Registry()
        # Set up defined roles
        self.__tenant_name = get_unittest_tenant_name()
        set_tenant_map({self.__tenant_name: "RI"})
        self.__temp_dir = tempfile.mkdtemp()
        reg.settings = {}
        reg.settings['pdf.report_base_dir'] = self.__temp_dir
        reg.settings['pdf.single_generate.queue'] = 'test_queue'
        reg.settings['pdf.celery_timeout'] = 20
        reg.settings['services.proxy_url'] = 'url'
        testing.setUp(registry=reg, request=self._request, hook_zca=False)

        self._params = {
            Constants.STUDENTGUID: "EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q",
            Constants.STATECODE: "RI",
            Constants.DISTRICTGUID: "R0001",
            Constants.SCHOOLGUID: "RN001",
            Constants.GRADECOURSE: "Grade 9",
            Constants.ASMTTYPE: AssessmentType.SUMMATIVE,
            Constants.YEAR: 2015,
            Constants.MODE: Constants.GRAY,
            Constants.LANG: 'en',
            Constants.SUBJECT: "subject2",
        }
        self._config = SyncGeneratorConfig(**self._params)

    def test_url(self):
        actual = self._config.url
        required_params = ('stateCode', 'studentGuid', 'year', 'subject')
        for parameters in required_params:
            query_string = "%s=%s" % (parameters, self._params[parameters])
            self.assertIn(query_string, actual)

    def test_file_name(self):
        expect = path.join(self.__temp_dir,
                           "RI/2014-2015/R0001/RN001/Grade 9/isr/SUMMATIVE/ELA/EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q.en.g.pdf")
        self.assertEqual(expect, self._config.file_name)

    def test_queue_name(self):
        self.assertEqual("test_queue", self._config.queue_name)

    def test_sync_generator_configuration(self):
        celery_options = {
            'timeout': 20,
            'grayscale': True,
            'cookie_name': 'edware',
            'always_generate': False,
            'proxy_url': 'url'
        }
        self.assertEqual(self._config.celery_options, celery_options)
