'''
Created on Nov 8, 2013

@author: dip
'''
import unittest
from unittest.mock import patch
from pyramid.testing import DummyRequest
from pyramid import testing
from pyramid.registry import Registry
from pyramid.response import Response
from beaker.cache import CacheManager
from beaker.util import parse_cache_config_options
from pyramid.security import Allow
from edcore.tests.utils.unittest_with_edcore_sqlite import Unittest_with_edcore_sqlite,\
    get_unittest_tenant_name
from edextract.celery import setup_celery
from edapi.httpexceptions import EdApiHTTPPreconditionFailed
from reporting.services.extract import post_extract_service
from edauth.tests.test_helper.create_session import create_test_session
import edauth
from edcore.security.tenant import set_tenant_map
from parcc_common.security.constants import RolesConstants
from reporting.reports.helpers.constants import Constants as ReportConstants
from parcc_common.extracts.constants import ExtractConstants
from reporting.security.roles.pii import PII  # @UnusedImport
from edauth.security.user import RoleRelation


class TestExtract(Unittest_with_edcore_sqlite):

    def setUp(self):
        cache_opts = {
            'cache.type': 'memory',
            'cache.regions': 'public.data,public.filtered_data,public.shortlived'
        }
        CacheManager(**parse_cache_config_options(cache_opts))
        self.__request = DummyRequest()
        # Must set hook_zca to false to work with uniittest_with_sqlite
        reg = Registry()
        reg.settings = {}
        reg.settings = {'hpz.file_upload_base_url': 'http://somehost:82/files'}
        self.__config = testing.setUp(registry=reg, request=self.__request, hook_zca=False)
        self.__tenant_name = get_unittest_tenant_name()

        defined_roles = [(Allow, RolesConstants.SF_EXTRACT, ('view', 'logout')),
                         (Allow, RolesConstants.RF_EXTRACT, ('view', 'logout')),
                         (Allow, RolesConstants.PF_EXTRACT, ('view', 'logout'))]
        edauth.set_roles(defined_roles)
        set_tenant_map({get_unittest_tenant_name(): 'RI'})
        # Set up context security
        dummy_session = create_test_session([RolesConstants.SF_EXTRACT])
        dummy_session.set_user_context([RoleRelation(RolesConstants.SF_EXTRACT, get_unittest_tenant_name(), 'RI', None, None),
                                        RoleRelation(RolesConstants.PF_EXTRACT, get_unittest_tenant_name(), 'RI', None, None),
                                        RoleRelation(RolesConstants.RF_EXTRACT, get_unittest_tenant_name(), 'RI', None, None)])
        self.__config.testing_securitypolicy(dummy_session.get_user())
        # celery settings for UT
        settings = {'extract.celery.CELERY_ALWAYS_EAGER': True}
        setup_celery(settings)
        set_tenant_map({'tomcat': 'RI'})

    def tearDown(self):
        self.__request = None
        testing.tearDown()

    @patch('hpz_client.frs.http_file_upload.api.post')
    @patch('reporting.extracts.base_extract.BaseExtract.register_file')
    @patch('reporting.extracts.base_extract.start_remote_extract')
    def test_post_valid_response_tenant_extract(self, mock_post, mock_register, mock_start):
        mock_register.return_value = ("id", "http://url", "http://realurl")
        self.__request.method = 'POST'
        self.__request.json_body = {ReportConstants.STATECODE: 'RI',
                                    ReportConstants.YEAR: 2015,
                                    ReportConstants.SUBJECT: 'subject2',
                                    ReportConstants.EXTRACTTYPE: ExtractConstants.SUMMATIVE_ASMT_RESULT,
                                    ReportConstants.GRADECOURSE: ['Grade 3']}
        results = post_extract_service(None, self.__request)
        self.assertIsInstance(results, Response)
        self.assertEqual("http://realurl", results.json_body[ReportConstants.FILES][0][ReportConstants.DOWNLOAD_URL])

    def test_post_invalid_payload(self):
        self.__request.method = 'POST'
        # # State code should not be an array
        self.__request.json_body = {ReportConstants.STATECODE: ['RI'],
                                    ReportConstants.YEAR: 2015,
                                    ReportConstants.SUBJECT: 'subject2',
                                    ReportConstants.EXTRACTTYPE: ExtractConstants.SUMMATIVE_ASMT_RESULT,
                                    ReportConstants.GRADECOURSE: ['Grade 3']}
        self.assertRaises(EdApiHTTPPreconditionFailed, post_extract_service, None, self.__request.json_body)

    def test_post_post_invalid_param(self):
        self.__request.method = 'POST'
        # # 'a' is not a valid parameter
        self.__request.json_body = {'a': 1}
        self.assertRaises(EdApiHTTPPreconditionFailed, post_extract_service, None, self.__request.json_body)

    @patch('hpz_client.frs.http_file_upload.api.post')
    @patch('reporting.extracts.base_extract.BaseExtract.register_file')
    @patch('reporting.extracts.base_extract.start_remote_extract')
    def test_post_valid_pf_extract(self, mock_post, mock_register, mock_start):
        mock_register.return_value = ('a1-b2-c3-d4-e1e10', 'http://somehost:82/download/a1-b2-c3-d4-e1e10', 'http://somehost:82/web/a1-b2-c3-d4-e1e10')
        self.__request.method = 'POST'
        self.__request.json_body = {ReportConstants.STATECODE: 'RI',
                                    ReportConstants.YEAR: 2015,
                                    ReportConstants.EXTRACTTYPE: ExtractConstants.PSYCHOMETRIC}
        response = post_extract_service(None, self.__request)
        self.assertIsInstance(response, Response)
        self.assertEqual(response.content_type, 'application/json')
        self.assertEqual('http://somehost:82/web/a1-b2-c3-d4-e1e10', response.json_body[ReportConstants.FILES][0][ReportConstants.DOWNLOAD_URL])

    @patch('hpz_client.frs.http_file_upload.api.post')
    @patch('reporting.extracts.base_extract.BaseExtract.register_file')
    @patch('reporting.extracts.base_extract.start_remote_extract')
    def test_post_valid_rf_extract(self, mock_post, mock_register, mock_start):
        mock_register.return_value = ("id", "http://url", "http://realurl")
        self.__request.method = 'POST'
        self.__request.json_body = {ReportConstants.STATECODE: 'RI',
                                    ReportConstants.YEAR: 2015,
                                    ReportConstants.SUBJECT: 'subject2',
                                    ReportConstants.EXTRACTTYPE: ExtractConstants.SUMMATIVE_RIF,
                                    ReportConstants.GRADECOURSE: ['Grade 4']}
        results = post_extract_service(None, self.__request)
        self.assertIsInstance(results, Response)
        self.assertEqual("http://realurl", results.json_body[ReportConstants.FILES][0][ReportConstants.DOWNLOAD_URL])

if __name__ == "__main__":
    unittest.main()
