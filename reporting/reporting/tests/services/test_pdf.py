'''
Created on May 17, 2013

@author: dip
'''
import unittest
import tempfile
from pyramid.testing import DummyRequest
from pyramid import testing
from pyramid.response import Response
from pyramid.registry import Registry
from edapi.httpexceptions import EdApiHTTPPreconditionFailed, \
    EdApiHTTPForbiddenAccess, EdApiHTTPInternalServerError
from edcore.tests.utils.unittest_with_edcore_sqlite import Unittest_with_edcore_sqlite, \
    get_unittest_tenant_name
import services
from services.celery import setup_celery
from reporting.services.pdf import sync_pdf_service, async_pdf_service
from edapi.exceptions import ForbiddenError
import shutil
from services.tests.tasks.test_pdf import get_cmd
from pyramid.security import Allow
import edauth
from edcore.security.tenant import set_tenant_map
from parcc_common.security.constants import RolesConstants
from edauth.tests.test_helper.create_session import create_test_session
from edauth.security.user import RoleRelation
from reporting.reports.helpers.constants import Constants, AssessmentType
from beaker.cache import CacheManager
from beaker.util import parse_cache_config_options
from services.tasks.pdf import prepare_path
from reporting.pdf.sync_generator import SyncPDFGenerator, SyncGeneratorConfig
from edextract.celery import setup_celery as setup_extract_celery
import os
from reporting.pdf.path_generator import generate_pdf_path_by_student_id
from reporting.reports.helpers.report_table_map import ReportTableMap
from reporting.security.roles.pii import PII  # @UnusedImport


class TestServices(Unittest_with_edcore_sqlite):

    def setUp(self):
        cache_opts = {
            'cache.type': 'memory',
            'cache.regions': 'public.shortlived'
        }
        CacheManager(**parse_cache_config_options(cache_opts))
        self.__request = DummyRequest()
        # Must set hook_zca to false to work with uniittest_with_sqlite
        reg = Registry()
        # Set up defined roles
        self.__tenant_name = get_unittest_tenant_name()
        set_tenant_map({self.__tenant_name: "RI"})
        self.__temp_dir = tempfile.mkdtemp()
        reg.settings = {}
        reg.settings['pdf.report_base_dir'] = self.__temp_dir
        reg.settings['pdf.single_generate.queue'] = 'test_queue'
        reg.settings['pdf.generate_timeout'] = 20
        self.__config = testing.setUp(registry=reg, request=self.__request, hook_zca=False)

        defined_roles = [(Allow, RolesConstants.PII, ('view', 'logout'))]
        edauth.set_roles(defined_roles)
        # Set up context security
        dummy_session = create_test_session([RolesConstants.PII])
        dummy_session.set_user_context([RoleRelation(RolesConstants.PII, self.__tenant_name, 'RI', 'R0001', None),
                                        RoleRelation(RolesConstants.PII, self.__tenant_name, 'RI', 'R0003', 'RP002')])
        self.__config.testing_securitypolicy(dummy_session.get_user())

        # celery settings for UT
        settings = {'services.celery.CELERY_ALWAYS_EAGER': True}
        self.__request.cookies = {'edware': '123'}
        setup_celery(settings)
        settings = {'extract.celery.CELERY_ALWAYS_EAGER': True}
        setup_extract_celery(settings)

    def tearDown(self):
        shutil.rmtree(self.__temp_dir, ignore_errors=True)
        self.__request = None
        testing.tearDown()

    def test_async_pdf_serivce_with_invalid_payload(self):
        self.__request.method = 'POST'
        self.assertRaises(EdApiHTTPPreconditionFailed, async_pdf_service, None, self.__request)

    def test_async_pdf_service_with_invalid_param(self):
        self.__request.method = 'POST'
        self.__request.json_body = {}
        self.assertRaises(EdApiHTTPPreconditionFailed, async_pdf_service, None, self.__request)

    @unittest.skip("context security is disabled")
    def test_async_pdf_service_no_context(self):
        self.__request.method = 'POST'
        self.__request.json_body = {Constants.STUDENTGUID: '19489898-d469-41e2-babc-265ecbab2337', Constants.STATECODE: 'NC', Constants.YEAR: 2016}
        self.assertRaises(EdApiHTTPForbiddenAccess, async_pdf_service, None, self.__request)

    def test_sync_pdf_service_invalid_param(self):
        self.__request.GET = {}
        self.assertRaises(EdApiHTTPPreconditionFailed, sync_pdf_service, None, self.__request)

    def test_sync_pdf_service(self):
        self.__request.method = 'GET'
        studentId = 'EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q'
        self.__request.GET = {
            Constants.STATECODE: 'RI',
            Constants.DISTRICTGUID: 'R0001',
            Constants.SCHOOLGUID: 'RN001',
            Constants.GRADECOURSE: 'Grade 9',
            Constants.STUDENTGUID: studentId,
            Constants.ASMTTYPE: AssessmentType.SUMMATIVE,
            Constants.SUBJECT: 'subject2',
            Constants.YEAR: 2015
        }
        self.__request.matchdict[Constants.REPORT] = 'studentReport.html'
        self.__request.cookies = {'edware': '123'}
        # prepare empty file
        rpt = ReportTableMap(Constants.SUBJECT2)
        pdf_file = generate_pdf_path_by_student_id(rpt, 'RI', 'R0001', 'RN001', 'Grade 9', 2015, 'ELA', pdf_report_base_dir=self.__temp_dir, student_ids=studentId)
        prepare_path(pdf_file[studentId])
        with open(pdf_file[studentId], 'w') as file:
            file.write('%PDF-1.4')
        # Override the wkhtmltopdf command
        services.tasks.pdf.pdf_procs = ['echo', 'dummy']
        response = sync_pdf_service(None, self.__request)
        self.assertEqual(response.content_type, Constants.APPLICATION_PDF)

    @unittest.skip("context security is disabled")
    def test_sync_pdf_service_no_context(self):
        self.__request.method = 'GET'
        self.__request.GET = {Constants.STUDENTGUID: '19489898-d469-41e2-babc-265ecbab2337', Constants.STATECODE: 'NC', Constants.YEAR: 2016}
        self.__request.matchdict[Constants.REPORT] = 'indivStudentReport.html'

        self.assertRaises(EdApiHTTPForbiddenAccess, sync_pdf_service, None, self.__request)

    def test_get_pdf_valid_params(self):
        studentId = 'EC7DhZ43a2ir2QoYTKNHQ2ATZr3t6O7Xajm5EA7Q'
        self.__request.GET[Constants.STUDENTGUID] = studentId
        self.__request.GET[Constants.STATECODE] = 'RI'
        self.__request.GET[Constants.DISTRICTGUID] = 'R0001'
        self.__request.GET[Constants.SCHOOLGUID] = 'RN001'
        self.__request.GET[Constants.GRADECOURSE] = 'Algebra I'
        self.__request.GET[Constants.YEAR] = 2015
        self.__request.GET[Constants.ASMTTYPE] = AssessmentType.SUMMATIVE
        self.__request.GET[Constants.SUBJECT] = 'subject1'
        self.__request.matchdict[Constants.REPORT] = 'studentReport.html'
        self.__request.cookies = {'edware': '123'}
        # prepare empty file
        rpt = ReportTableMap(Constants.SUBJECT1)
        pdf_file = generate_pdf_path_by_student_id(rpt, 'RI', 'R0001', 'RN001', 'Algebra I', 2015, 'Math', pdf_report_base_dir=self.__temp_dir, student_ids=studentId)
        prepare_path(pdf_file[studentId])
        with open(pdf_file[studentId], 'w') as file:
            file.write('%PDF-1.4')
        # Override the wkhtmltopdf command
        services.tasks.pdf.pdf_procs = ['echo', 'dummy']
        response = sync_pdf_service(None, self.__request)
        self.assertIsInstance(response, Response)
        self.assertIsNotNone(response.body)
        self.assertEqual(response.content_type, Constants.APPLICATION_PDF)
        self.assertIsInstance(response.body, bytes)

    def test_send_pdf_request_with_pdf_generation_fail(self):
        params = {}
        # Important, this pdf must not exist in directory
        params[Constants.STUDENTGUID] = 'MyXTFn1S8qz5lQJAjw11CeaMjoaIiQbo8KihC5ix'
        params[Constants.STATECODE] = 'RI'
        params[Constants.DISTRICTGUID] = 'R0001'
        params[Constants.SCHOOLGUID] = 'RN001'
        params[Constants.GRADECOURSE] = 'Grade 9'
        params[Constants.ASMT_YEAR] = 2015
        params[Constants.SUBJECT] = 'subject2'
        params[Constants.REPORT] = 'studentReport.html'
        self.__request.GET[Constants.ASMTTYPE] = AssessmentType.SUMMATIVE
        params['dummy'] = 'dummy'
        self.__request.cookies = {'edware': '123'}
        services.tasks.pdf.pdf_procs = get_cmd()
        generator = SyncPDFGenerator(**params).run
        self.assertRaises(EdApiHTTPInternalServerError, generator)

    @unittest.skip("no context security right now")
    def test_get_pdf_content_with_no_context(self):
        params = {}
        params[Constants.STUDENTGUID] = '19489898-d469-41e2-babc-265ecbab2337'
        params[Constants.STATECODE] = 'NC'
        params[Constants.ASMTTYPE] = 'SUMMATIVE'
        params[Constants.YEAR] = '2016'
        params[Constants.YEAR] = 2016
        self.__request.cookies = {'edware': '123'}
        generator = SyncPDFGenerator(**params).run
        self.assertRaises(ForbiddenError, generator)

    @unittest.skip("no context security right now")
    def test_has_context_for_pdf_request(self):
        student_id = 'a5ddfe12-740d-4487-9179-de70f6ac33be'
        has_context = _has_context_for_pdf_request('NC', student_id)
        self.assertTrue(has_context)

    @unittest.skip("no context security right now")
    def test_has_context_for_pdf_request_with_no_context(self):
        student_id = 'invalid'
        has_context = _has_context_for_pdf_request('NC', student_id)
        self.assertFalse(has_context)

    def test_sync_generator_configuration(self):
        params = {}
        params[Constants.STUDENTGUID] = "D3de8PGASi2QhIZ0zHLSNJXZI0okSm1VqPJMeJAw"
        params[Constants.STATECODE] = 'RI'
        params[Constants.DISTRICTGUID] = 'R0003'
        params[Constants.SCHOOLGUID] = 'RP003'
        params[Constants.GRADECOURSE] = 'Grade 3'
        params[Constants.YEAR] = 2015
        params[Constants.SUBJECT] = "subject1"
        config = SyncGeneratorConfig(**params)
        # test urls
        for key in params:
            self.assertIn(key, config.url)
        self.assertEqual(config.file_name, os.path.join(self.__temp_dir, 'RI/2014-2015/R0003/RP003/Grade 3/isr/SUMMATIVE/Math/D3de8PGASi2QhIZ0zHLSNJXZI0okSm1VqPJMeJAw.en.g.pdf'))
        self.assertEqual(config.queue_name, 'test_queue')
        celery_options = {'timeout': 20, 'grayscale': True, 'cookie_name': 'edware', 'always_generate': False, 'proxy_url': None}
        self.assertEqual(config.celery_options, celery_options)
