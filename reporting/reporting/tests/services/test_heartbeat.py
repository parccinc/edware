'''
Created on Apr 18, 2013

@author: dip
'''
import unittest
from edcore.tests.utils.unittest_with_edcore_sqlite import Unittest_with_edcore_sqlite_no_data_load
from reporting.services import heartbeat
from pyramid.testing import DummyRequest
from pyramid.httpexceptions import HTTPOk, HTTPServerError
from unittest.mock import patch
from services.celery import setup_celery
import logging


class TestHeartbeat(Unittest_with_edcore_sqlite_no_data_load):

    def setUp(self):
        settings = {'services.celery.CELERY_ALWAYS_EAGER': False}
        setup_celery(settings)
        logging.disable(logging.CRITICAL)  # Disable logging to avoid log messages for tests

    def tearDown(self):
        logging.disable(logging.NOTSET)  # Enable logging back

    def testValidDataSource(self):
        results = heartbeat.check_datasource(DummyRequest())
        self.assertIsInstance(results, HTTPOk)

    def testValidCelery(self):
        results = heartbeat.check_celery(DummyRequest())
        self.assertIsInstance(results, HTTPServerError)

    @patch('reporting.services.heartbeat.check_celery')
    @patch('reporting.services.heartbeat.check_datasource')
    def testValidHeartbeat(self, check_celery_patch, check_datasource_patch):
        check_celery_patch.return_value = HTTPServerError()
        check_datasource_patch.return_value = HTTPServerError()
        results = heartbeat.heartbeat(DummyRequest())
        self.assertIsInstance(results, HTTPServerError)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
