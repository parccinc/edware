'''
Created on Jun 20, 2013

@author: tosako
'''
import logging

from sqlalchemy.sql.expression import select, and_, func, literal, union, true, or_

from collections import namedtuple
from reporting.trigger.cache.recache import CacheTrigger
from reporting.reports.helpers.constants import RptTable, Constants
from reporting.reports.helpers.report_table_map import SUBJECT_TO_RPT_TABLE
from edcore.database.stats_connector import StatsDBConnection
from edcore.database.edcore_connector import EdCoreDBConnection
from edcore.database.utils.constants import UdlStatsConstants
from edcore.utils.utils import run_cron_job
from edcore.security.tenant import get_tenant_map
from reporting.trigger.udl_stats import get_udl_stats_for_new_data


logger = logging.getLogger('reporting')

RecacheParam = namedtuple(
    'RecacheParam',
    ['state_code', 'resp_dist_id', 'year', 'subject', 'grade_course']
)


def run_cron_recache(settings):
    '''
    Configure and run cron job to flush and re-cache reports

     :param dict settings:  configuration for the application
    '''
    run_cron_job(settings, 'trigger.recache.', precached_task)


def precached_task(settings):
    '''
    Precaches reports based on udl stats

    :param dict settings:  configuration for the application
    '''
    udl_stats_results = get_udl_stats_for_new_data(UdlStatsConstants.LAST_PRE_CACHED)
    for udl_stats_result in udl_stats_results:
        tenant = udl_stats_result.get(UdlStatsConstants.TENANT)
        batch_guid = udl_stats_result.get(UdlStatsConstants.BATCH_GUID)

        params = get_params_for_pre_caching(tenant, batch_guid)
        triggered_success = trigger_precache(tenant, params, settings)

        if triggered_success:
            update_ed_stats_for_precached(udl_stats_result[UdlStatsConstants.REC_ID])


def trigger_precache(tenant, district_params, settings):
    '''
    call pre-cache function

    :param string tenant:  name of the tenant
    :param string state_code:  stateCode representing the state
    :param list results:  list of results
    :rType:  boolean
    :returns:  True if precache is triggered and no exceptions are caught
    '''
    logger.debug('trigger_precache has [%d] results to process', len(district_params))
    if not district_params:
        return False
    triggered = True
    cache_trigger = CacheTrigger(tenant, settings)
    for param in district_params:
        triggered &= cache_trigger.precache_by_params(*param)
    return triggered


def update_ed_stats_for_precached(rec_id):
    '''
    update current timestamp to last_pre_cached field

    :param string tenant:  name of the tenant
    :param string state_code:  stateCode of the state
    '''
    with StatsDBConnection() as connector:
        udl_stats = connector.get_table(UdlStatsConstants.UDL_STATS)
        stmt = udl_stats.update(values={udl_stats.c.last_pre_cached: func.now()}).where(udl_stats.c.rec_id == rec_id)
        connector.execute(stmt)


def get_params_for_pre_caching(tenant, batch_guid):

    def get_year(result):
        # year format in result: 2014-2015
        year = result[Constants.YEAR][-4:]
        return int(year)

    def get_param_for_parcc(result):
        year = get_year(result)
        return RecacheParam(
            None,
            None,
            year,
            result[Constants.SUBJECT],
            result[Constants.GRADECOURSE],
        )

    def get_param_for_dist(result):
        year = get_year(result)
        return RecacheParam(
            state_code,
            result[Constants.DISTRICT_GUID],
            year,
            result[Constants.SUBJECT],
            result[Constants.GRADECOURSE],
        )

    def get_param_for_state(result):
        year = get_year(result)
        return RecacheParam(
            state_code,
            None,
            year,
            result[Constants.SUBJECT],
            result[Constants.GRADECOURSE],
        )

    def _get_common_where_condition(table):
        return and_(
            table.c.state_code == state_code,
            table.c.batch_guid == batch_guid,
            table.c.rec_status == Constants.CURRENT,
            or_(
                table.c.include_in_district == true(),
                table.c.include_in_state == true(),
            )
        )

    def get_grade_queries(connector):
        queries = []
        for subject, table_name in SUBJECT_TO_RPT_TABLE.items():
            table = connector.get_table(table_name)
            common_where_conditon = _get_common_where_condition(table)
            query = select([
                literal(subject).label(Constants.SUBJECT),
                table.c.resp_dist_id.label(Constants.DISTRICT_GUID),
                table.c.year.label(Constants.YEAR),
                table.c.asmt_grade.label(Constants.GRADECOURSE),
            ], from_obj=[table]).where(and_(
                common_where_conditon,
                table.c.asmt_grade.isnot(None),
            )
            ).group_by(
                Constants.DISTRICT_GUID,
                Constants.YEAR,
                table.c.asmt_grade,
            )
            queries.append(query)
        return queries

    def get_course_query(connector):
        table = connector.get_table(RptTable.MATH_SUM)
        common_where_conditon = _get_common_where_condition(table)
        return select([
            literal(Constants.SUBJECT1).label(Constants.SUBJECT),
            table.c.resp_dist_id.label(Constants.DISTRICT_GUID),
            table.c.year.label(Constants.YEAR),
            table.c.asmt_subject.label(Constants.GRADECOURSE)
        ], from_obj=[table]).where(and_(
            common_where_conditon,
            table.c.asmt_subject.isnot(None),
        )).group_by(
            Constants.DISTRICT_GUID,
            Constants.YEAR,
            table.c.asmt_subject,
        )

    district_params = []
    state_params = set()
    parcc_params = set()
    state_code = get_tenant_map().get(tenant)
    with EdCoreDBConnection(tenant=tenant) as connector:
        query_grade = get_grade_queries(connector)
        query_course = get_course_query(connector)
        query = union(query_course, *query_grade)
        results = connector.get_result(query)
        for result in results:
            district_params.append(get_param_for_dist(result))
            state_params.add(get_param_for_state(result))
            parcc_params.add(get_param_for_parcc(result))
    return list(parcc_params) + list(state_params) + district_params
