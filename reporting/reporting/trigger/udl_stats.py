from sqlalchemy.sql.expression import select, and_, or_, null

from edcore.database.stats_connector import StatsDBConnection
from edcore.database.utils.constants import UdlStatsConstants, LoadType


def get_udl_stats_for_new_data(process_check_flag):
    """
    Get stats data to determine data that has not been pre prepared
    :param process_check_flag: the flag which is used to check if data has been prepped for the particular process
    :return: results from the udl stats table for the data which has not been pre prepared
    """
    with StatsDBConnection() as connector:
        udl_stats = connector.get_table(UdlStatsConstants.UDL_STATS)
        query = select([
            udl_stats.c.rec_id.label(UdlStatsConstants.REC_ID),
            udl_stats.c.tenant.label(UdlStatsConstants.TENANT),
            udl_stats.c.load_start.label(UdlStatsConstants.LOAD_START),
            udl_stats.c.load_end.label(UdlStatsConstants.LOAD_END),
            udl_stats.c.record_loaded_count.label(UdlStatsConstants.RECORD_LOADED_COUNT),
            udl_stats.c.batch_guid.label(UdlStatsConstants.BATCH_GUID),
        ], from_obj=[udl_stats])
        where_condition = and_(
            udl_stats.c[process_check_flag] == null(),
            udl_stats.c.load_status == UdlStatsConstants.MIGRATE_INGESTED,
            or_(
                udl_stats.c.load_type == LoadType.LOAD_TYPE_ASSESSMENT_MATH,
                udl_stats.c.load_type == LoadType.LOAD_TYPE_ASSESSMENT_ELA
            )
        )
        query = query.where(where_condition)
        return connector.get_result(query)
