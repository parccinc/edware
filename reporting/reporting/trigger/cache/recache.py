'''
Created on Jun 20, 2013

@author: dip
'''
import os
import json
import logging

from edapi.cache import region_invalidate
from reporting.reports.summative.compare_pop_report import ComparingPopReport,\
    get_comparing_populations_cache_key, get_comparing_populations_cache_route
from reporting.reports.helpers.constants import (AssessmentType,
                                                 Views,
                                                 SUBCLAIM_TABLE_COL_MAP,
                                                 Constants, LevelValues)
from reporting.reports.summative.consortium import ConsortiumReport
from reporting.reports.summative.helpers.constants import Constants as SummativeConstants


logger = logging.getLogger('reporting')


class CacheTrigger(object):

    def __init__(self, tenant, settings):
        self.tenant = tenant
        self.settings = settings
        self.filters = read_config_from_json_file(settings.get('trigger.recache.filter.file'))

    def _cache(self, state_code, resp_dist_id, year, subject, grade_course, result, view, filters):
        '''
        Flush and recache cpop report for a particular year. If state_code is None, it triggers the consortium report

        :param string state_code:  stateCode representing the state
        :rtype:  dict
        :returns: comparing populations state view report
        '''
        cpop_class = ConsortiumReport if state_code is None else ComparingPopReport
        report = cpop_class(stateCode=state_code,
                            districtGuid=resp_dist_id,
                            year=year,
                            subject=subject,
                            gradeCourse=grade_course,
                            result=result,
                            view=view,
                            asmtType=AssessmentType.SUMMATIVE,
                            **filters)
        report.min_cell_size = self._get_min_cell_size(view)
        report.consortium_min_cell_size = self._get_consortium_min_cell_size(view)
        report.alert_cell_size = self._get_alert_cell_size()
        if cpop_class == ConsortiumReport:
            report.setup_min_cell_size(self.settings)

        region_name = get_comparing_populations_cache_route(report)
        args = get_comparing_populations_cache_key(report)
        flush_report_in_cache_region(report.get_unfiltered_results, region_name, *args)
        report.get_unfiltered_results()

    def _get_min_cell_size(self, view):
        return int(self.settings.get("min_cell_size.{0}.{1}".format(view, self.tenant), 0))

    def _get_consortium_min_cell_size(self, view):
        return int(self.settings.get("min_cell_size.{0}.{1}".format(view, Constants.CONSORTIUM_NAME), 0))

    def _get_alert_cell_size(self):
        return int(self.settings.get("alert_cell_size", 0))

    def precache_by_params(self, state_code, resp_dist_id, year, subject, grade_course):
        try:
            logger.debug('pre-caching state[%s], district[%s]', state_code, resp_dist_id)
            if resp_dist_id:
                filter_category = LevelValues.DISTRICT
            elif state_code:
                filter_category = LevelValues.STATE
            else:
                filter_category = LevelValues.PARCC
            for result, view in self._get_result_and_view():
                for filters in self._get_filters(filter_category):
                    self._cache(state_code, resp_dist_id, year, subject, grade_course, result, view, filters)
            return True
        except Exception as e:
            logger.error('Recache threw exception for state_code %s district_id %s',
                         state_code, resp_dist_id)
            logger.exception(e)
            return False

    def _get_result_and_view(self):
        result_types = [subclaim for subclaim in SUBCLAIM_TABLE_COL_MAP]
        result_types += [SummativeConstants.OVERALL]
        for result in result_types:
            for view in (Views.PERFORMANCE, Views.GROWTH):
                yield result, view

    def _get_filters(self, tenant, suffix='state'):
        '''
        Find the filter specific to my tenant, if it doesn't exist, use the generic
        filter If generic filter doesn't exist, return a list of one with empty
        filters to recache comparing populations with empty filters
        '''
        tenant_specific = tenant + '.' + suffix
        if tenant_specific in self.filters:
            filters = self.filters[tenant_specific]
        else:
            filters = self.filters.get(suffix, [])
        return filters + [{}]


def flush_report_in_cache_region(function, region, *args):
    '''
    Flush a cache region

    :param function:  the function that was cached by cache_region decorator
    :param args:  positional arguments that are part of the cache key
    :param region:  the name of the region to flush
    '''
    region_invalidate(function, region, *args)


def read_config_from_json_file(file_name):
    '''
    Read precache filtering setings from .json file

    :param string file_name:  path of json file that contains filters
    '''
    if file_name is None:
        return {}
    abspath = os.path.abspath(file_name)
    if not os.path.exists(abspath):
        raise Exception('File %s not found' % abspath)
    with open(abspath) as file:
        data = json.load(file)
    return data
