'''
Created on Jun 23, 2013

@author: tosako
'''
import logging
import urllib
import urllib.parse

from sqlalchemy.sql.expression import select, and_, func, distinct, literal, true

from batch.pdf.pdf_generator import PDFGenerator
from reporting.reports.helpers.constants import Constants, AssessmentType
from edcore.database.stats_connector import StatsDBConnection
from edcore.database.edcore_connector import EdCoreDBConnection
from reporting.pdf.constants import Constants as PDFConstants
from edcore.database.utils.constants import UdlStatsConstants, LoadType
from edcore.utils.utils import run_cron_job
from edcore.security.tenant import get_tenant_map
from reporting.pdf.path_generator import create_pdf_path
from reporting.reports.helpers.query_utils import execute_query_remotely
from reporting.reports.helpers.report_table_map import SUBJECT_TO_RPT_TABLE
from reporting.trigger.udl_stats import get_udl_stats_for_new_data


logger = logging.getLogger('reporting')


def create_pdf_data_query(connector, batch_guid, subject, state_code):
    summ_table = connector.get_table(SUBJECT_TO_RPT_TABLE[subject])
    query = select([distinct(summ_table.c.student_parcc_id).label(Constants.STUDENT_ID),
                    summ_table.c.year.label(Constants.ASMT_YEAR),
                    summ_table.c.resp_dist_id.label(Constants.DISTRICT_GUID),
                    summ_table.c.resp_school_id.label(Constants.SCHOOL_ID),
                    summ_table.c.asmt_grade.label(Constants.ASMT_GRADE),
                    summ_table.c.asmt_subject.label(Constants.ASMT_SUBJECT),
                    literal(AssessmentType.SUMMATIVE).label(Constants.ASMT_TYPE),
                    literal(subject).label(Constants.SUBJECT)],
                   from_obj=summ_table)
    query = query.where(and_(summ_table.c.state_code == state_code,
                             summ_table.c.batch_guid == batch_guid,
                             summ_table.c.rec_status == Constants.CURRENT,
                             summ_table.c.include_in_isr == true()))
    return query


def prepare_pre_pdf(tenant, state_code, batch_guid, subject):
    '''
    prepare which state and district are pre-cached
    '''
    with EdCoreDBConnection(tenant=tenant) as connector:
        query = create_pdf_data_query(connector, batch_guid, subject, state_code)
        return execute_query_remotely(state_code, query)


def trigger_pre_pdf(settings, state_code, tenant, results):
    '''
    call pre-pdf function

    :param string tenant:  name of the tenant
    :param string state_code:  stateCode representing the state
    :param list results:  list of results
    :rType:  boolean
    :returns:  True if pdf generation is triggered and no exceptions are caught
    '''
    triggered = False
    base_dir = settings.get('pdf.report_base_dir', '/tmp')
    logger.debug('trigger_pre_pdf has [%d] results to process', len(results))
    if len(results) > 0:
        triggered = True
        pdf_trigger = PDFGenerator(settings, tenant)
        for result in results:
            student_id = None
            try:
                student_id = result.get(Constants.STUDENT_ID)
                asmt_year = str(result.get(Constants.ASMT_YEAR))
                district_id = result.get(Constants.DISTRICT_GUID)
                school_id = result.get(Constants.SCHOOL_ID)
                asmt_grade = result.get(Constants.ASMT_GRADE) or result.get(Constants.ASMT_SUBJECT)
                asmt_type = result.get(Constants.ASMT_TYPE)
                subject = result.get(Constants.SUBJECT)
                subject_name = Constants.SUBJECT_MAP.get(subject)
                file_name = create_pdf_path(asmt_grade, asmt_year, district_id, base_dir, school_id, state_code, student_id, subject_name, asmt_type)
                # TODO: PARCC-124: Clarify if we need remove logging for this (contain PII information)?
                logger.debug('pre-pdf for [%s]', file_name)
                latest_year = asmt_year.split('-')[1]
                relative_pdf_url = build_url(student_id, state_code, school_id, district_id, latest_year, subject, asmt_grade, asmt_type)
                pdf_trigger.send_pdf_request(relative_pdf_url, file_name)
            except Exception as e:
                triggered = False
                # TODO: PARCC-124: Clarify if we need remove student_id from log?
                logger.warning('Pdf generation failed for %s', student_id)
                logger.exception(e)
    return triggered


def build_url(student_id, state_code, school_id, district_id, asmt_year, asmt_subject, asmt_grade, asmt_type, report=PDFConstants.SUMMATIVE_REPORT):
    '''
    Build and return the full url for ISR report
    '''
    # Encode the query parameters and append it to url
    encoded_params = urllib.parse.urlencode(
        {
            Constants.STUDENTGUID: student_id,
            Constants.PDF: 'true',
            Constants.STATECODE: state_code,
            Constants.SCHOOLGUID: school_id,
            Constants.DISTRICTGUID: district_id,
            Constants.YEAR: asmt_year,
            Constants.SUBJECT: asmt_subject,
            Constants.ASMT_TYPE: asmt_type,
            Constants.GRADECOURSE: asmt_grade,
        }
    )
    return report + "?%s" % encoded_params


def update_ed_stats_for_prepdf(rec_id):
    '''
    update current timestamp to last_pdf_generated field

    :param string tenant:  name of the tenant
    :param string state_code:  stateCode of the state
    '''
    with StatsDBConnection() as connector:
        udl_stats = connector.get_table(UdlStatsConstants.UDL_STATS)
        stmt = udl_stats.update(values={udl_stats.c.last_pdf_task_requested: func.now()}).where(udl_stats.c.rec_id == rec_id)
        connector.execute(stmt)


def prepdf_task(settings):
    '''
    Generate pdfs for students that have new assessments

    :param dict settings:  configuration for the application
    '''
    udl_stats_results = get_udl_stats_for_new_data(UdlStatsConstants.LAST_PDF_TASK_REQUESTED)
    tenant_to_state_code = get_tenant_map()
    for udl_stats_result in udl_stats_results:
        load_type = udl_stats_result.get(UdlStatsConstants.LOAD_TYPE)
        tenant = udl_stats_result.get(UdlStatsConstants.TENANT)
        state_code = tenant_to_state_code.get(tenant)
        batch_guid = udl_stats_result.get(UdlStatsConstants.BATCH_GUID)
        if load_type == LoadType.LOAD_TYPE_ASSESSMENT_MATH:
            subject = Constants.SUBJECT1
        else:
            subject = Constants.SUBJECT2
        summ_results = prepare_pre_pdf(tenant, state_code, batch_guid, subject)
        triggered_success = trigger_pre_pdf(settings, state_code, tenant, summ_results)
        if triggered_success:
            update_ed_stats_for_prepdf(udl_stats_result[UdlStatsConstants.REC_ID])


def run_cron_prepdf(settings):
    '''
    Configure and run cron job to regenerate pdfs for students with new assessment data

    :param dict settings: configuration for the application
    '''
    run_cron_job(settings, 'trigger.pdf.', prepdf_task)
