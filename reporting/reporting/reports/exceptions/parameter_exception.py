'''
Created on Mar 12, 2013

@author: tosako
'''


class ParameterException(Exception):
    '''
    Parameter Exception
    '''
    pass


class InvalidParameterException(ParameterException):
    '''
    Invalid Parameter Exception
    '''
    pass
