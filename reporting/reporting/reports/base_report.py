import logging
import pyramid
from pyramid.security import authenticated_userid

from sqlalchemy.sql.expression import and_

from abc import ABCMeta, abstractmethod
from edcore.database.edcore_connector import EdCoreDBConnection
from reporting.reports.helpers.query_utils import construct_query_info
from reporting.reports.helpers.report_table_map import SUBJECT_TO_RPT_TABLE
from reporting.reports.helpers.metadata import get_subjects_map, get_ordered_subjects_dict
from reporting.reports.helpers.query_utils import execute_mapped_queries_remotely
from reporting.reports.helpers.report_table_map import ReportTableMap
from reporting.reports.helpers.breadcrumbs import get_breadcrumbs_context
from reporting.reports.helpers.constants import Constants, AssessmentType, Subject, SubjectJsonKeys,\
    RptTable, Views
from reporting.reports.helpers.filters import FILTERS_CONFIG
from edcore.utils.utils import merge_dict, format_asmt_year
from reporting.reports.helpers.filters import get_filter_params
from reporting.reports.helpers.legend import get_formatted_legend_info, get_formatted_cut_points
from reporting.reports.helpers.grade_course import get_grade_course_expr
from reporting.reports.helpers.min_cell_size import get_minimum_cell_size_for_state,\
    get_minimum_alert_cell_size, get_minimum_cell_size
from edcore.utils.query import QueryMapKey
from edapi.exceptions import ForbiddenError
from reporting.security.context import is_permitted
from parcc_common.security.constants import RolesConstants


logger = logging.getLogger("reporting")

COMMON_REPORT_PARAMS = merge_dict({
    Constants.STATECODE: {
        "type": "string",
        "required": True,
        "pattern": "^[a-zA-Z]{2}$"
    },
    Constants.DISTRICTGUID: {
        "type": "string",
        "required": True,
        "pattern": "^[a-zA-Z0-9\-]{0,40}$",
    },
    Constants.YEAR: {
        "type": "integer",
        "required": True,
        "pattern": "^[1-9][0-9]{3}$"
    },
    Constants.SUBJECT: {
        "type": "string",
        "pattern": "^(%s|%s)$" % (Constants.SUBJECT1, Constants.SUBJECT2),
        "required": True
    },
    Constants.GRADECOURSE: {
        "type": "string",
        "pattern": "^[a-z0-9A-Z ]{2,30}$",
        "required": False
    }
}, FILTERS_CONFIG)


class BaseReport(metaclass=ABCMeta):
    """
    Base class for all reports
    """
    def __init__(self,
                 stateCode=None,
                 districtGuid=None,
                 schoolGuid=None,
                 gradeCourse=None,
                 studentGuid=None,
                 year=None,
                 asmtType=None,
                 subject=None,
                 result=None,
                 **kwargs):
        self.state_code = stateCode
        self.district_guid = districtGuid
        self.school_guid = schoolGuid
        self.grade_course = gradeCourse
        self.student_guid = studentGuid
        self.asmt_type = asmtType
        self.asmt_year = format_asmt_year(year)
        self.result = result
        self.filters = get_filter_params(kwargs)
        self.subjects_map = get_subjects_map()
        self.subject = subject
        self.permission = None
        self.is_course = False
        self.table_names = []  # overridden in derived classes
        self.report_type = None

    def get_legend(self, *args, **kwargs):
        return None

    def format_columns(self, results):
        return {}

    def format_assessments(self, results, assessments):
        results[Constants.ASSESSMENTS] = {}
        results[Constants.COLUMNS] = {}
        return results

    def create_subjects_list(self):
        """
        :return: an array of info of all the subjects which are relevant for the report
        Default behaviour is to return all subjects, derived classes should override this method
        if only particular subjects are required.
        """
        subjects_list = []
        for subject, label in get_ordered_subjects_dict().items():
            label_text = Subject.LABEL_MAP[label]
            subjects_list.append({
                SubjectJsonKeys.LABEL: label_text,
                SubjectJsonKeys.VALUE: subject
            })
        return subjects_list

    def get_breadcrumbs_context(self):
        # get the table relative to the current report request
        table_name = self.table_names[0][0]
        return get_breadcrumbs_context(table_name,
                                       state_code=self.state_code,
                                       district_guid=self.district_guid,
                                       school_guid=self.school_guid,
                                       asmt_grade=self.grade_course,
                                       student_guid=self.student_guid,
                                       is_course=self.is_course)

    def get_results(self):
        """
        :return: Return format differs depending on the type of report,
        refer to the derived classes for details
        """
        if self.permission:
            # If a report has a permission, we first validate that we have access to current hierarchy
            context_params = {Constants.STATECODE: self.state_code,
                              Constants.DISTRICTGUID: self.district_guid,
                              Constants.SCHOOLGUID: self.school_guid}
            # We only need to prevent user below school level
            if context_params.get(Constants.SCHOOLGUID) is not None \
                    and is_permitted(self.permission, context_params) is False:
                logging.warning("Permission denied to report.  User doesn't have correct permission.")
                raise ForbiddenError("Not authorized")

        results = {}
        mapped_results = self.run_query()
        legend = mapped_results.pop(QueryMapKey(type=Constants.LEGEND), None)

        assessments = self.build_assessment_map(mapped_results)

        results[Constants.CONTEXT] = self.get_breadcrumbs_context()
        results[Constants.SUBJECTS] = self.create_subjects_list()

        if self.has_results(assessments):
            if self.report_type == Constants.ITEM_ANALYSYS:    # temporary only for student roster item view
                self.format_assessments(results, assessments)
            else:
                results[Constants.ASSESSMENTS] = self.format_results(assessments)
                results[Constants.COLUMNS] = self.format_columns(assessments)

            if self.asmt_type == AssessmentType.SUMMATIVE:
                results[Constants.SUMMARY] = self.format_summary(mapped_results)

        if legend:
            results[Constants.LEGEND] = {
                Constants.LEVELS: get_formatted_legend_info(legend),
                Constants.CUTPOINTS: get_formatted_cut_points(legend),
            }
        return results

    def build_assessment_map(self, mapped_results):
        """
        :param mapped_results: results for assessments
        :return: assessments in case summative report or mapped_results
        """
        if self.asmt_type == AssessmentType.SUMMATIVE:
            assessments = {}
            for subj_key, subj_value in SUBJECT_TO_RPT_TABLE.items():
                map_key = QueryMapKey(subject=subj_value, type=Constants.ASSESSMENTS)
                if map_key in mapped_results:
                    assessments[subj_key] = mapped_results[map_key]
            # Special case for school report. I don't like it. But it's gonna stay now.
            if Constants.COURSES in mapped_results:
                assessments[Constants.COURSES] = mapped_results[Constants.COURSES]
            return assessments
        else:
            return mapped_results

    @abstractmethod
    def get_query(self, *table_name):
        raise NotImplementedError()

    @abstractmethod
    def format_results(self, results):
        raise NotImplementedError()

    @abstractmethod
    def run_query(self):
        raise NotImplementedError()

    @abstractmethod
    def has_results(self, assessments):
        raise NotImplementedError()

    @abstractmethod
    def get_required_table_names(self):
        """
        :return: a list of all tables tuples required for generating the required report
        """
        raise NotImplementedError()

    def format_summary(self, mapped_results):
        raise NotImplementedError()

    @property
    def staff_id(self):
        user = authenticated_userid(pyramid.threadlocal.get_current_request())
        return user.get_staff_id() if user else None

    def append_staff_id_where_condition(self, asmt_table, where_condition):
        if self.staff_id is not None:
            return and_(where_condition, asmt_table.c.staff_id == self.staff_id)
        return where_condition


class BaseSummativeReport(BaseReport):
    """
    Base class for all summative reports
    """

    def __init__(self,
                 stateCode=None,
                 districtGuid=None,
                 schoolGuid=None,
                 gradeCourse=None,
                 studentGuid=None,
                 year=None,
                 asmtType=None,
                 subject=None,
                 result=None,
                 with_item_score=False,
                 view=None,
                 **kwargs):
        def check_if_course(grade_course):
            return grade_course is not None and Constants.GRADE_PREFIX not in grade_course

        super().__init__(stateCode, districtGuid, schoolGuid, gradeCourse,
                         studentGuid, year, asmtType, subject, result, **kwargs)
        self.is_course = check_if_course(self.grade_course)
        self.view = view or Views.PERFORMANCE
        self.table_names = self.get_required_table_names(subject, with_item_score)  # returns list of tuples
        self.additional_tables = ()
        self._min_cell_size = None
        self._consortium_min_cell = None
        self._alert_cell_size = None
        self.report_type = None

    def format_summary(self, mapped_results):
        if self.has_aggregator():
            return self.get_aggregator_instance().format_summary_results(mapped_results)
        else:
            # not every report needs summary data
            return []

    def has_non_summary_component(self):
        return True

    def run_query(self):
        query_map = {}
        if self.has_non_summary_component():
            with EdCoreDBConnection(state_code=self.state_code) as conn:
                for table_name_tuple in self.table_names:
                    tables = [conn.get_table(t) for t in table_name_tuple + self.additional_tables]

                    query_map_key = QueryMapKey(subject=table_name_tuple[0], type=Constants.ASSESSMENTS)
                    query_map[query_map_key] = construct_query_info(query=self.get_query(*tables),
                                                                    state_code=self.state_code)

                    legend_query = self.get_legend(conn.get_table(RptTable.RPT_SUM_TEST_LEVEL_M))
                    if legend_query:
                        legend_query = construct_query_info(query=legend_query, state_code=self.state_code)
                        query_map[QueryMapKey(type=Constants.LEGEND)] = legend_query

        if self.has_aggregator():
            query_map = merge_dict(query_map, self.get_aggregator_instance().get_summary_query_map())
        if len(query_map) > 0:
            return execute_mapped_queries_remotely(query_map)
        else:
            return None

    def get_aggregator(self):
        raise NotImplementedError()

    def has_aggregator(self):
        return True

    def get_aggregator_instance(self):
        aggregator_class = self.get_aggregator()
        self._aggregator = aggregator_class(
            self.table_names,
            self.state_code,
            self.district_guid,
            self.school_guid,
            self.asmt_year,
            self.asmt_type,
            self.grade_course,
            self.result,
            self.filters,
            self.view,
            self.min_cell_size,
            self.consortium_min_cell_size,
            self.alert_cell_size,
            self.min_cell_size_msg,
        )
        return self._aggregator

    def get_legend(self, *args, **kwargs):
        return None

    def append_grade_course_where_condition(self, asmt_table, where_condition):
        expr = get_grade_course_expr(self.grade_course, asmt_table)
        if where_condition is not None:
            return and_(where_condition, expr)
        else:
            return and_(expr)

    def has_results(self, assessments):
        return len(assessments.get(Constants.SUBJECT1, [])) > 0 or len(assessments.get(Constants.SUBJECT2, [])) > 0

    def get_required_table_names(self, subject=None, with_item_score=None):
        return ReportTableMap(subject).get_table_names(with_item_score)

    @property
    def min_cell_size(self):
        if self._min_cell_size is None:
            self._min_cell_size = get_minimum_cell_size_for_state(self.state_code, self.view)
        return self._min_cell_size

    @min_cell_size.setter
    def min_cell_size(self, value):
        self._min_cell_size = value

    @property
    def consortium_min_cell_size(self):
        if self._consortium_min_cell is None:
            self._consortium_min_cell = get_minimum_cell_size(Constants.CONSORTIUM_NAME, self.view)
        return self._consortium_min_cell

    @consortium_min_cell_size.setter
    def consortium_min_cell_size(self, value):
        self._consortium_min_cell = value

    @property
    def alert_cell_size(self):
        if self._alert_cell_size is None:
            self._alert_cell_size = get_minimum_alert_cell_size()
        return self._alert_cell_size

    @alert_cell_size.setter
    def alert_cell_size(self, value):
        self._alert_cell_size = value

    @property
    def min_cell_size_msg(self):
        if self.view == Views.GROWTH:
            return Constants.MIN_CELL_GROWTH_MSG
        else:
            return Constants.MIN_CELL_PERF_MSG


class BaseDiagnosticReport(BaseReport):
    """
    Base class for all diagnostic reports
    """

    def __init__(self,
                 stateCode=None,
                 districtGuid=None,
                 schoolGuid=None,
                 gradeCourse=None,
                 studentGuid=None,
                 year=None,
                 asmtType=None,
                 subject=None,
                 result=None,
                 **kwargs):
        super().__init__(stateCode, districtGuid, schoolGuid, gradeCourse, studentGuid,
                         year, asmtType, subject, result, **kwargs)
        self.table_names = self.get_required_table_names()
        # diagnostics are not for courses, only grades so self.is_course isFalse (set in base report)
        # All diagnostic reports require PII permission
        self.permission = RolesConstants.PII

    def run_query(self):
        query_map = {}
        with EdCoreDBConnection(state_code=self.state_code) as conn:
            # queries
            for table_name_tuple in self.table_names:
                tables = [conn.get_table(t) for t in table_name_tuple]
                query = construct_query_info(query=self.get_query(*tables), state_code=self.state_code)
                query_map[QueryMapKey(subject=table_name_tuple[0], type=Constants.ASSESSMENTS)] = query

        if len(query_map) > 0:
            return execute_mapped_queries_remotely(query_map)
        else:
            return None

    def get_query_key(self):
        """
        :return: key for diagnostic query results, e.g. "assessment|rpt_ela_vocab|"
        """
        return QueryMapKey(subject=self.get_required_table_names()[0][0], type=Constants.ASSESSMENTS)

    def get_common_where_condition(self, asmt_table):
        where_condition = and_(
            asmt_table.c.year == self.asmt_year,
            asmt_table.c.state_code == self.state_code,
            asmt_table.c.resp_dist_id == self.district_guid,
            asmt_table.c.resp_school_id == self.school_guid,
            asmt_table.c.rec_status == Constants.CURRENT)
        return self.append_staff_id_where_condition(asmt_table, where_condition)

    @staticmethod
    def apply_order_by_name(asmt_table, query):
        return query.order_by(
            asmt_table.c.student_last_name,
            asmt_table.c.student_first_name,
            asmt_table.c.student_middle_name,
        )

    @staticmethod
    def get_common_select_columns(asmt_table):
        return [
            asmt_table.c.student_parcc_id,
            asmt_table.c.student_middle_name,
            asmt_table.c.student_first_name,
            asmt_table.c.student_last_name,
            asmt_table.c.asmt_grade,
            asmt_table.c.staff_id,
            asmt_table.c.asmt_date,
        ]

    def has_results(self, assessments):
        return len(assessments) > 0
