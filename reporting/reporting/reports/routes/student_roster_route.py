
from edapi.decorators import report_config, user_info
from edapi.httpexceptions import EdApiHTTPNotFound
from edapi.logging import audit_event
from edcore.utils.utils import merge_dict
from reporting.reports.base_report import COMMON_REPORT_PARAMS
from reporting.security.context import get_current_request_context
from reporting.reports.helpers.constants import Constants, AssessmentType
from reporting.reports.helpers.constants import Views
from reporting.security.tenant import validate_user_tenant
from reporting.reports.summative.student_roster_report import StudentRosterScoresReport, StudentRosterItemAnalysisReport
from reporting.reports.configuration.analytics import add_analytics_info


REPORT_PARAMS = merge_dict(COMMON_REPORT_PARAMS, {
    Constants.VIEW: {
        "type": "string",
        "pattern": "^(%s|%s)" % (Views.SCORES, Views.ITEM_ANALYSIS),
        "required": True
    },
    Constants.SCHOOLGUID: {
        "type": "string",
        "required": True,
        "pattern": "^[a-zA-Z0-9\-]{0,40}$",
    },
    Constants.ASMTTYPE: {
        "type": "string",
        "pattern": "^%s$" % (AssessmentType.SUMMATIVE, ),
        "required": True
    }
})


@report_config(name="student_roster", params=REPORT_PARAMS)
@validate_user_tenant
@user_info
@add_analytics_info
@get_current_request_context
@audit_event()
def get_student_roster_report(params):
    '''
    Student roster scores report.

    :param dict params:  dictionary of parameters for the scores report
    '''
    view = params[Constants.VIEW]
    if view == Views.SCORES:
        return StudentRosterScoresReport(**params).get_results()
    elif view == Views.ITEM_ANALYSIS:
        return StudentRosterItemAnalysisReport(with_item_score=True, **params).get_results()
    else:
        raise EdApiHTTPNotFound()
