'''
Created on May 8, 2015

@author: dip
'''
from edapi.decorators import report_config, user_info
from reporting.reports.base_report import COMMON_REPORT_PARAMS
from reporting.reports.helpers.constants import Constants, AssessmentType, Views, SUBCLAIM_TABLE_COL_MAP
from edapi.logging import audit_event
from edcore.utils.utils import merge_dict
from reporting.reports.summative.helpers.constants import Constants as SummativeConstants
from reporting.security.tenant import validate_user_tenant
from reporting.security.context import get_current_request_context
from reporting.reports.summative.consortium import ConsortiumReport
from reporting.reports.summative.compare_pop_report import ComparingPopReport
from reporting.reports.configuration.analytics import add_analytics_info


@report_config(
    name="comparing_populations",
    params=merge_dict(COMMON_REPORT_PARAMS, {
        Constants.STATECODE: {
            "type": "string",
            "required": False,  # differs from COMMON_REPORT_PARAMS
            "pattern": "^[a-zA-Z]{2}$",
        },
        Constants.DISTRICTGUID: {
            "type": "string",
            "required": False,  # differs from COMMON_REPORT_PARAMS
            "pattern": "^[a-zA-Z0-9\-]{0,50}$",
        },
        Constants.RESULTTYPE: {
            "type": "string",
            "pattern": "^(%s|%s)$" % (SummativeConstants.OVERALL, '|'.join(SUBCLAIM_TABLE_COL_MAP.keys())),
            "required": False
        },
        Constants.ASMTTYPE: {
            "type": "string",
            "pattern": "^(" + AssessmentType.SUMMATIVE + ")$",
            "required": True
        },
        Constants.VIEW: {
            "type": "string",
            "pattern": "^(%s|%s)" % (Views.GROWTH, Views.PERFORMANCE),
            "required": True
        }
    }))
@validate_user_tenant
@user_info
@add_analytics_info
@get_current_request_context
@audit_event()
def get_comparing_populations_report(params):
    '''
    Comparing Populations Report
    '''
    if params.get(Constants.STATECODE) is None:
        return ConsortiumReport(**params).get_results()
    return ComparingPopReport(**params).get_results()
