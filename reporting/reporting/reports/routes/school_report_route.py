from edapi.decorators import report_config, user_info
from edapi.logging import audit_event
from edcore.utils.utils import merge_dict
from reporting.reports.diagnostic.grade_level import GradeLevelReport
from reporting.reports.diagnostic.locator import LocatorReport
from reporting.reports.diagnostic.progression import ProgressionReport
from reporting.reports.diagnostic.reader_motivation_survey import ReaderMotivationSurveyReport
from reporting.reports.diagnostic.reading_comprehension import ReadingComprehensionReport
from reporting.reports.diagnostic.reading_fluency import ReadingFluencyReport
from reporting.reports.diagnostic.writing import WritingReport
from reporting.reports.helpers.constants import (AssessmentType, Constants, SUBCLAIM_TABLE_COL_MAP)
from reporting.reports.diagnostic.helpers.constants import DiagnosticReportType
from reporting.reports.summative.helpers.constants import Constants as SummativeConstants
from reporting.reports.base_report import COMMON_REPORT_PARAMS
from reporting.security.context import get_current_request_context
from reporting.security.tenant import validate_user_tenant
from reporting.reports.summative.school_report import SchoolLevelOverallReport
from reporting.reports.diagnostic.vocabulary import VocabularyReport
from reporting.reports.diagnostic.decoding import DecodingReport
from reporting.reports.diagnostic.overview import OverviewReport
from reporting.reports.diagnostic.cluster import ClusterReport
from reporting.reports.diagnostic.math_fluency import MathFluencyReport
from reporting.reports.configuration.analytics import add_analytics_info

REPORT_NAME = "school_report"


DIAGNOSTIC_REPORT_MAPPING = {
    DiagnosticReportType.VOCABULARY: VocabularyReport,
    DiagnosticReportType.WRITING: WritingReport,
    DiagnosticReportType.READING_FLUENCY: ReadingFluencyReport,
    DiagnosticReportType.DECODING: DecodingReport,
    DiagnosticReportType.OVERVIEW: OverviewReport,
    DiagnosticReportType.READING_COMPREHENSION: ReadingComprehensionReport,
    DiagnosticReportType.LOCATOR: LocatorReport,
    DiagnosticReportType.GRADE_LEVEL: GradeLevelReport,
    DiagnosticReportType.PROGRESSION: ProgressionReport,
    DiagnosticReportType.CLUSTER: ClusterReport,
    DiagnosticReportType.READER_MOTIVATION_SURVEY: ReaderMotivationSurveyReport,
    DiagnosticReportType.FLUENCY: MathFluencyReport,
}

SCHOOL_REPORT_PARAMS = merge_dict(COMMON_REPORT_PARAMS,
                                  {
                                      Constants.RESULTTYPE: {
                                          "type": "string",
                                          "pattern": "^(%s|%s)$" % (SummativeConstants.OVERALL, '|'.join(SUBCLAIM_TABLE_COL_MAP.keys())),
                                          "required": False
                                      },
                                      Constants.VIEW: {
                                          "type": "string",
                                          "pattern": "^(%s)" % ('|'.join(DiagnosticReportType().__dict__.values())),
                                          "required": False
                                      },
                                      Constants.SCHOOLGUID: {
                                          "type": "string",
                                          "required": True,
                                          "pattern": "^[a-zA-Z0-9\-]{0,40}$",
                                      }
                                  })


@report_config(name="summative_school_report", params=merge_dict(SCHOOL_REPORT_PARAMS,
                                                                 {
                                                                     Constants.ASMTTYPE: {
                                                                         "type": "string",
                                                                         "pattern": "^%s$" % (AssessmentType.SUMMATIVE, ),
                                                                         "required": True
                                                                     }
                                                                 }))
@validate_user_tenant
@user_info
@add_analytics_info
@get_current_request_context
@audit_event()
def get_school_report_summative(params):
    '''
    School Report - summative assessments
    '''
    return SchoolLevelOverallReport(**params).get_results()


@report_config(name="diagnostic_school_report", params=merge_dict(SCHOOL_REPORT_PARAMS,
                                                                  {
                                                                      Constants.ASMTTYPE: {
                                                                          "type": "string",
                                                                          "pattern": "^%s$" % (AssessmentType.DIAGNOSTIC, ),
                                                                          "required": True
                                                                      }
                                                                  }))
@validate_user_tenant
@user_info
@add_analytics_info
@get_current_request_context
@audit_event()
def get_school_report(params):
    '''
    School Report - diagnostic assessments
    '''
    view = params.get(Constants.VIEW)
    return DIAGNOSTIC_REPORT_MAPPING.get(view)(**params).get_results()
