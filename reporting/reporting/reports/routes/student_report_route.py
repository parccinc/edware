from edapi.decorators import report_config, user_info
from edapi.logging import audit_event
from edcore.utils.utils import merge_dict
from reporting.security.context import get_current_request_context
from reporting.reports.helpers.constants import Constants, AssessmentType
from reporting.security.tenant import validate_user_tenant
from reporting.reports.base_report import COMMON_REPORT_PARAMS
from reporting.reports.summative.student_report import StudentReport
from reporting.reports.diagnostic.student import StudentReport as DiagnosticStudentReport
from reporting.reports.configuration.analytics import add_analytics_info


STUDENT_REPORT_PARAMS = merge_dict(COMMON_REPORT_PARAMS,
                                   {
                                       Constants.SCHOOLGUID: {
                                           "type": "string",
                                           "required": True,
                                           "pattern": "^[a-zA-Z0-9\-]{0,40}$",
                                       },
                                       Constants.STUDENTGUID: {
                                           "type": "string",
                                           "required": True,
                                           "pattern": "^[a-zA-Z0-9\-]{0,40}$",
                                       },
                                   })


@report_config(name="summative_student_report", params=merge_dict(STUDENT_REPORT_PARAMS,
                                                                  {
                                                                      Constants.ASMTTYPE: {
                                                                          "type": "string",
                                                                          "pattern": "^%s$" % (AssessmentType.SUMMATIVE, ),
                                                                          "required": True
                                                                      }
                                                                  }))
@validate_user_tenant
@user_info
@add_analytics_info
@get_current_request_context
@audit_event()
def get_student_report(params):
    '''
    Individual student report.

    :param dict params:  dictionary of parameters for List of student report
    :return: Check the "assessments" portion of assets/data/en/common/schoolReport.json for a sample return value
    '''
    return StudentReport(**params).get_results()


@report_config(name="diagnostic_student_report", params=merge_dict(STUDENT_REPORT_PARAMS,
                                                                   {
                                                                       Constants.ASMTTYPE: {
                                                                           "type": "string",
                                                                           "pattern": "^%s$" % (AssessmentType.DIAGNOSTIC, ),
                                                                           "required": True
                                                                       }
                                                                   }))
@validate_user_tenant
@user_info
@add_analytics_info
@get_current_request_context
@audit_event()
def get_diagnostic_student_report(params):
    '''
    Individual student report.

    :param dict params:  dictionary of parameters for List of student report
    :return: Check the "assessments" portion of assets/data/en/common/schoolReport.json for a sample return value
    '''
    return DiagnosticStudentReport(**params).get_results()
