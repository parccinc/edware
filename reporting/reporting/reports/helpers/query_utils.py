import logging

import asyncio
from edextract.tasks.constants import Constants as QueryTask
from edcore.database.routing import ReportingDbConnection


log = logging.getLogger('reporting')


def execute_mapped_queries_remotely(query_map):
    '''
    Returns a map of query keys to corresponding query results
    Use this method when you want to run queries in parallel but still be able to separate the results
    '''
    results_map = {}

    @asyncio.coroutine
    def run_query(key, query_info):
        query = query_info.get(QueryTask.QUERY)
        is_public = query_info.get(QueryTask.IS_PUBLIC, False)
        tenant = query_info.get(QueryTask.TENANT)
        state_code = query_info.get(QueryTask.STATE_CODE)
        log.debug("Start execute query %s in parallel" % (query,))
        with ReportingDbConnection(tenant=tenant, state_code=state_code, is_public=is_public) as conn:
            results_map[key] = conn.get_result(query)

    loop = asyncio.new_event_loop()
    try:
        asyncio.set_event_loop(loop)
        tasks = [asyncio.async(run_query(key, query)) for key, query in query_map.items()]
        loop.run_until_complete(asyncio.wait(tasks))
    except Exception as e:
        log.exception(e)
    finally:
        loop.close()
    return results_map


def execute_query_remotely(state_code, query):
    '''
    Executes the query and returns the results
    '''
    with ReportingDbConnection(state_code=state_code) as conn:
        results = conn.get_result(query)
    return results


def construct_query_info(query, state_code=None, tenant=None, is_public=None):
    query_info = {QueryTask.QUERY: query}
    if state_code is not None:
        query_info[QueryTask.STATE_CODE] = state_code
    if tenant is not None:
        query_info[QueryTask.TENANT] = tenant
    if is_public is not None:
        query_info[QueryTask.IS_PUBLIC] = is_public
    return query_info
