'''
Created on Apr 24, 2015

@author: dip
'''
from reporting.reports.helpers.constants import Constants


def get_grade_course_expr(grade_course, asmt_table):
    '''
    Reusable logic to return grades and courses expression
    '''
    if grade_course.startswith(Constants.GRADE_PREFIX):
        expr = asmt_table.c.asmt_grade == grade_course
    else:
        expr = asmt_table.c.asmt_subject == grade_course
    return expr
