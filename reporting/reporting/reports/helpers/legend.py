__author__ = 'esnyder'
from reporting.reports.helpers.constants import Constants, ExtractConstants
from sqlalchemy.sql import select
from edcore.utils.utils import compile_query_to_sql_text
from sqlalchemy.sql.expression import and_

REC_STATUS_COLUMN = 'rec_status'


COLORS = {
    "1": "rgba(56, 152, 69, 0.15)",
    "2": "rgba(141, 199, 83, 0.15)",
    "3": "rgba(234, 204, 76, 0.15)",
    "4": "rgba(237, 141, 36, 0.15)",
    "5": "rgba(235, 103, 37, 0.15)",
}


LEGEND_TEXT = {
    "1": "Did not yet meet",
    "2": "Partially met",
    "3": "Approached",
    "4": "Met",
    "5": "Exceeded",
}


def get_legend_info_query(test_level_table, subject, gradecourse):
    test_code = ExtractConstants.GRADE_SUBJECT_TEST_CODE_MAP[subject][gradecourse]
    select_columns = [
        test_level_table.c.perf_lvl_id,
        test_level_table.c.cutpoint_label,
        test_level_table.c.cutpoint_low,
        test_level_table.c.cutpoint_upper,
    ]
    query = select(select_columns, from_obj=test_level_table)
    query = query.where(test_level_table.c.test_code == test_code)
    if REC_STATUS_COLUMN in test_level_table.columns:
        query = query.where(and_(test_level_table.c.rec_status == Constants.CURRENT))
    query = query.order_by(test_level_table.c.perf_lvl_id.asc())
    return compile_query_to_sql_text(query)


def get_formatted_legend_info(legend_info):
    levels = []
    for result in legend_info:
        level = result[Constants.PERFORMANCE_LEVEL_ID]
        levels.append({
            Constants.LEVEL: int(level),
            Constants.TITLE1: LEGEND_TEXT[level],
            Constants.TITLE2: 'expectations',
        })

    return levels


def get_formatted_cut_points(legend_info):
    cut_points = []
    for result in legend_info:
        level = result[Constants.PERFORMANCE_LEVEL_ID]
        cut_points.append({
            Constants.START: int(result[Constants.CUTPOINT_LOW]),
            Constants.END: int(result[Constants.CUTPOINT_UPPER]),
            Constants.COLOR: COLORS[level],
        })
    return cut_points
