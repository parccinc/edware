'''
Created on Mar 12, 2013

@author: dip
'''

from parcc_common.config.summative_ela_ref_table_data import ref_table_conf as ela_ref_table_conf
from parcc_common.config.summative_ela_ref_table_data_spring_2015 import ref_table_conf as ela_ref_table_conf_sprint_2015
from parcc_common.config.summative_ela_ref_table_data_fall_2015 import ref_table_conf as ela_ref_table_conf_fall_2015
from parcc_common.config.summative_ela_ref_table_data_spring_2016 import ref_table_conf as ela_ref_table_conf_spring_2016

from parcc_common.config.summative_math_ref_table_data import ref_table_conf as math_ref_table_conf
from parcc_common.config.summative_math_ref_table_data_spring_2015 import ref_table_conf as math_ref_table_conf_sprint_2015
from parcc_common.config.summative_math_ref_table_data_fall_2015 import ref_table_conf as math_ref_table_conf_fall_2015
from parcc_common.config.summative_math_ref_table_data_spring_2016 import ref_table_conf as math_ref_table_conf_spring_2016

from parcc_common.config.psychometric_ref_table_data import ref_table_conf as psycho_ref_table_conf
from parcc_common.config.summative_ela_item_ref_table_data import ref_table_conf as ela_rif_ref_table_conf
from parcc_common.config.summative_math_item_ref_table_data import ref_table_conf as math_rif_ref_table_conf

from collections import OrderedDict

SUBCLAIM_TABLE_COL_MAP = {'literarytext': 'subclaim1_category',
                          'infotext': 'subclaim2_category',
                          'vocab': 'subclaim3_category',
                          'writingexp': 'subclaim4_category',
                          'knwldconv': 'subclaim5_category',
                          'majorcontent': 'subclaim1_category',
                          'addcontent': 'subclaim4_category',
                          'modelapp': 'subclaim3_category',
                          'mathreason': 'subclaim2_category',
                          }


def subclaim_to_table_col(subclaim):
    return SUBCLAIM_TABLE_COL_MAP[subclaim]


class AssessmentType():
    '''
    constants for asmt types
    '''
    SUMMATIVE = 'SUMMATIVE'
    DIAGNOSTIC = 'DIAGNOSTIC'


class Subject():
    MATH = 'Math'
    ELA = 'ELA'
    LABEL_MAP = {
        MATH: "Mathematics",
        ELA: "ELA/L",
    }


class RptTable():
    ELA_SUM = 'rpt_ela_sum'
    MATH_SUM = 'rpt_math_sum'
    MATH_STUDENT_ITEM_SCORE = 'rpt_math_sum_item_score'
    ELA_STUDENT_ITEM_SCORE = 'rpt_ela_sum_item_score'
    RPT_SUM_TEST_LEVEL_M = 'rpt_sum_test_level_m'
    RPT_SUM_TEST_SCORE_M = 'rpt_sum_test_score_m'
    RPT_ITEM_P_DATA = 'rpt_item_p_data'
    RPT_RIF_MATH = 'rpt_math_sum_item_score'
    RPT_RIF_ELA = 'rpt_ela_sum_item_score'


class Constants():
    '''
    constants for reports
    '''
    ANALYTICS = 'analytics'
    SUMMARY = 'summary'
    CUTPOINT_LABEL = 'cutpoint_label'
    PERFORMANCE_LEVEL_ID = 'perf_lvl_id'
    CUTPOINT_LOW = 'cutpoint_low'
    CUTPOINT_UPPER = 'cutpoint_upper'
    LEGEND = 'legend'
    ASMT_GRADE = 'asmt_grade'
    ASMT_DATE = 'asmt_date'
    STATE_CODE = 'state_code'
    DISTRICT_NAME = 'district_name'
    DISTRICT_GUID = 'district_guid'
    SCHOOL_NAME = 'resp_school_name'
    SCHOOL_ID = 'school_id'
    SCHOOL_GUID = 'school_guid'
    STUDENT_ID = 'student_id'
    SCORE = 'score'
    STUDENT_PARCC_ID = 'student_parcc_id'
    PDF = 'pdf'
    ASMT_TYPE = 'asmt_type'
    LANG = 'lang'
    MODE = 'mode'
    COURSES = 'courses'

    # report types
    ITEM_ANALYSYS = 'item_analysis'

    # url params
    VIEW = 'view'
    YEAR = 'year'
    ASMTTYPE = 'asmtType'
    STUDENTGUID = 'studentGuid'
    SCHOOLGUID = 'schoolGuid'
    DISTRICTGUID = 'districtGuid'
    STATECODE = 'stateCode'
    GRADECOURSE = 'gradeCourse'
    EXTRACTTYPE = 'extractType'
    RESULTTYPE = 'result'
    # end url params

    # legend constants
    TITLE1 = 'title1'
    TITLE2 = 'title2'
    # end legend constants

    STUDENT_GUID = 'student_guid'
    STUDENT_MIDDLE_NAME = 'student_middle_name'
    STUDENT_LAST_NAME = 'student_last_name'
    STUDENT_FIRST_NAME = 'student_first_name'
    STUDENT_DISPLAY_NAME = 'student_display_name'
    STUDENT_DOB = 'student_dob'
    STAFF_ID = 'staff_id'

    PERCENTAGE = 'percentage'
    LEVEL = 'level'
    LEVELS = 'levels'
    CUTPOINTS = 'cut_points'
    NAME = 'name'
    MIN_CELL_SIZE = "min_cell_size"
    SUBJECT = 'subject'
    SUBJECTS = 'subjects'
    SUBJECT1 = 'subject1'
    SUBJECT2 = 'subject2'

    CONTEXT = 'context'
    ASMT_YEAR = 'asmt_year'
    CURRENT = 'C'
    FILES = 'files'
    DOWNLOAD_URL = 'download_url'
    APPLICATION_JSON = 'application/json'
    APPLICATION_PDF = 'application/pdf'
    REPORT = 'report'
    GRAY = 'gray'
    ASSESSMENTS = 'assessments'
    COLUMNS = 'columns'

    MIN_CELL_PERF_MSG = 'DATA SUPPRESSED TO PROTECT STUDENT PRIVACY'
    MIN_CELL_GROWTH_MSG = 'Growth data suppressed to protect student privacy'

    ASMTSUBJECT = 'asmtSubject'
    ASMT_SUBJECT = 'asmt_subject'
    CONSORTIUM_NAME = 'parcc'

    ASMTGRADE = 'asmtGrade'
    ASMTGUID = 'asmtGuid'
    SUBJECT_MAP = {
        SUBJECT1: Subject.MATH,
        SUBJECT2: Subject.ELA
    }

    START = 'start'
    END = 'end'
    COLOR = 'color'
    GRADE_PREFIX = 'Grade'


class Views:
    SCORES = 'scores'
    ITEM_ANALYSIS = 'itemAnalysis'
    GROWTH = 'growth'
    PERFORMANCE = 'performance'


ALL_PERFORMANCE_LEVELS = (1, 2, 3)

OVERALL_PERFORMANCE_LEVELS = (1, 2, 3, 4, 5)


class SubjectJsonKeys:
    LABEL = 'label'
    VALUE = 'value'


class LevelValues:
    DISTRICT = 'district'
    SCHOOL = 'school'
    STATE = 'state'
    PARCC = 'parcc'


class TablePrefix:
    RPT = 'rpt_'
    ELA = RPT + 'ela_'
    MATH = RPT + 'math_'


class ExtractConstants:
    REF_CONF_TABLE_MAPPING = {RptTable.ELA_SUM: OrderedDict({'overall': ela_ref_table_conf,
                                                             'spring_2015': ela_ref_table_conf_sprint_2015,
                                                             'fall_2015': ela_ref_table_conf_fall_2015,
                                                             'spring_2016': ela_ref_table_conf_spring_2016}),

                              RptTable.MATH_SUM: OrderedDict({'overall': math_ref_table_conf,
                                                              'spring_2015': math_ref_table_conf_sprint_2015,
                                                              'fall_2015': math_ref_table_conf_fall_2015,
                                                              'spring_2016': math_ref_table_conf_spring_2016}),

                              RptTable.RPT_RIF_ELA: ela_rif_ref_table_conf,
                              RptTable.RPT_RIF_MATH: math_rif_ref_table_conf,
                              RptTable.RPT_ITEM_P_DATA: psycho_ref_table_conf}
    GRADE_SUBJECT_TEST_CODE_MAP = {
        Constants.SUBJECT2: {
            'Grade 3': 'ELA03',
            'Grade 4': 'ELA04',
            'Grade 5': 'ELA05',
            'Grade 6': 'ELA06',
            'Grade 7': 'ELA07',
            'Grade 8': 'ELA08',
            'Grade 9': 'ELA09',
            'Grade 10': 'ELA10',
            'Grade 11': 'ELA11',
        },
        Constants.SUBJECT1: {
            'Grade 3': 'MAT03',
            'Grade 4': 'MAT04',
            'Grade 5': 'MAT05',
            'Grade 6': 'MAT06',
            'Grade 7': 'MAT07',
            'Grade 8': 'MAT08',
            'Algebra I': 'ALG01',
            'Algebra II': 'ALG02',
            'Geometry': 'GEO01',
            'Integrated Mathematics I': 'MAT1I',
            'Integrated Mathematics II': 'MAT2I',
            'Integrated Mathematics III': 'MAT3I'
        }
    }

SUMMATIVE_YEAR_SCHEMA_MAP = {
    2014: 'fall_2015',
    2015: 'fall_2015',
    2016: 'spring_2016'}


def get_required_schema_by_year(year: int, schema_dict: dict) -> OrderedDict:
    dict_keys = list(schema_dict.keys())
    if len(schema_dict.keys()) == 1:
        return schema_dict[dict_keys[0]]    # Return value of dict with 1 key
    return schema_dict[SUMMATIVE_YEAR_SCHEMA_MAP[year]]
