'''
Created on Jul 11, 2013

@author: tosako
'''
from sqlalchemy.sql.expression import or_, and_


class Filter():
    YES = 'Y'
    NO = 'N'

    def __init__(self, params):
        self.values = params.get(self.name)

    @property
    def column_name(self):
        return type(self)._COLUMN_NAME

    def append_condition(self, query, table):
        if self.values:
            query = query.where(table.c[self.column_name].in_(self.values))
        return query


class FilterGender(Filter):
    MALE = 'M'
    FEMALE = 'F'
    NAME = 'sex'
    _COLUMN_NAME = 'student_sex'
    _VALUE_MAP = {MALE: MALE,
                  FEMALE: FEMALE}

    def __init__(self, params):
        self.name = FilterGender.NAME
        super().__init__(params)

    def append_condition(self, query, table):
        if self.values:
            or_expr = []
            for value in self.values:
                or_expr.append(table.c[self.column_name] == FilterGender._VALUE_MAP[value])
            where_clause = or_(*or_expr)
            query = query.where(where_clause)
        return query


class FilterEthnicity(Filter):
    NAME = 'ethnicity'
    AMERICAN = '01'
    ASIAN = '02'
    BLACK = '03'
    HISPANIC = '04'
    WHITE = '05'
    PACIFIC = '06'
    MULTI = '07'
    _COLUMN_NAME = 'ethnicity'

    def __init__(self, params):
        self.name = FilterEthnicity.NAME
        super().__init__(params)


class FilterEconoDisadv(Filter):
    NAME = 'econoDisadvantage'
    _COLUMN_NAME = 'econo_disadvantage'

    def __init__(self, params):
        self.name = FilterEconoDisadv.NAME
        super().__init__(params)


class FilterDisabilities(Filter):
    NAME = 'disabilities'
    D_504 = '504'
    D_IEP = 'IEP'
    _COLUMN_NAME = 'disabil_student'

    def __init__(self, params):
        self.name = FilterDisabilities.NAME
        super().__init__(params)

    def append_condition(self, query, table):
        if self.values:
            # TODO: PARCC_127. PARCC should provide more info how to filter students with disadvantages
            if len(self.values) == 1 and self.YES in self.values:
                self.values.extend([FilterDisabilities.D_504, FilterDisabilities.D_IEP])
            query = query.where(table.c[self.column_name].in_(self.values))
        return query


class FilterEnglishLearner(Filter):
    NAME = 'englishLearner'
    ELL = 'ELL'
    LEP = 'LEP'
    BOTH = 'BOTH'
    columns = {ELL: 'ell', LEP: 'lep_status'}

    def __init__(self, params):
        self.name = FilterEnglishLearner.NAME
        super().__init__(params)

    def get_expr(self, name, query, table):
        return table.c[name] == Filter.YES

    def append_condition(self, query, table):
        if self.values:
            or_expr = []
            for value in self.values:
                if value == FilterEnglishLearner.BOTH:
                    and_expr = []
                    for v in FilterEnglishLearner.columns.values():
                        and_expr.append(self.get_expr(v, query, table))
                    or_expr.append(and_(*and_expr))
                else:
                    or_expr.append(self.get_expr(FilterEnglishLearner.columns[value], query, table))
            where_clause = or_(*or_expr)
            query = query.where(where_clause)
        return query


# Used in report_config for allowing demographics parameters for reports
FILTERS_CONFIG = {
    FilterEnglishLearner.NAME: {
        "type": "array",
        "required": False,
        "items": {
            "type": "string",
            "pattern": "^(" + FilterEnglishLearner.ELL + "|" + FilterEnglishLearner.LEP + "|" + FilterEnglishLearner.BOTH + ")$",
        }
    },
    FilterEconoDisadv.NAME: {
        "type": "array",
        "required": False,
        "items": {
            "type": "string",
            "pattern": "^(" + Filter.YES + "|" + Filter.NO + ")$",
        }
    },
    FilterDisabilities.NAME: {
        "type": "array",
        "required": False,
        "items": {
            "type": "string",
            "pattern": "^(" + Filter.YES + "|" + Filter.NO + ")$",
        }
    },
    FilterEthnicity.NAME: {
        "type": "array",
        "required": False,
        "items": {
            "type": "string",
            "pattern": "^(" + FilterEthnicity.AMERICAN + "|" + FilterEthnicity.ASIAN + "|" +
            FilterEthnicity.BLACK + "|" + FilterEthnicity.HISPANIC + "|" + FilterEthnicity.PACIFIC + "|" +
            FilterEthnicity.MULTI + "|" + FilterEthnicity.WHITE + ")$",
        }
    },
    FilterGender.NAME: {
        "type": "array",
        "required": False,
        "items": {
            "type": "string",
            "pattern": "^(" + FilterGender.MALE + "|" + FilterGender.FEMALE + ")$"
        }
    }
}


def apply_filter_to_query(query, table, params):
    '''
    Apply demographics filters to a query

    :param query:  a sqlalchemy query
    :param table: the table to append where clauses to
    :param dict params: dictionary that contains filter related key value
    '''
    for f in [FilterEconoDisadv,
              FilterEthnicity,
              FilterGender,
              FilterEnglishLearner,
              FilterDisabilities]:
        query = f(params).append_condition(query, table)
    return query


def has_filters(params):
    '''
    Returns true if params contain filter related key

    :param dict params: map of key value pair
    '''
    return not params.keys().isdisjoint(FILTERS_CONFIG.keys())


def get_filter_params(params):
    '''
    Returns params related to filters
    '''
    keys = set(params).intersection(set(FILTERS_CONFIG))
    return {k: params[k] for k in keys}
