import pyramid


def get_analytics_info():
    settings = pyramid.threadlocal.get_current_registry().settings
    return {
        'url': settings.get('analytics.url', '#')
    }
