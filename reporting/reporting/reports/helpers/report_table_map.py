'''
Created on Feb 19, 2015

@author: dip
'''
from reporting.reports.helpers.constants import RptTable, Constants


SUBJECT_TO_RPT_TABLE = {Constants.SUBJECT1: RptTable.MATH_SUM,
                        Constants.SUBJECT2: RptTable.ELA_SUM}

SUBJECT_TO_ITEM_SCORE_RPT_TABLE = {Constants.SUBJECT1: RptTable.MATH_STUDENT_ITEM_SCORE,
                                   Constants.SUBJECT2: RptTable.ELA_STUDENT_ITEM_SCORE}


def get_table_name(subject, with_item_score=False):
    if with_item_score:
        return SUBJECT_TO_RPT_TABLE.get(subject), SUBJECT_TO_ITEM_SCORE_RPT_TABLE.get(subject)
    else:
        return (SUBJECT_TO_RPT_TABLE.get(subject), )


def get_all_table_names(with_item_score=False):
    '''
    Returns all the table names
    Used when subject isn't given, we probably want to query all the subjects reporting table
    '''
    if with_item_score:
        table_list = []
        for key, _ in SUBJECT_TO_RPT_TABLE.items():
            table_list.append((SUBJECT_TO_RPT_TABLE[key], SUBJECT_TO_ITEM_SCORE_RPT_TABLE[key]))
        return table_list
    else:
        return [(table,) for table in SUBJECT_TO_RPT_TABLE.values()]


class ReportTableMap():
    '''
    Helper object for a report request to lookup table mapping based on subject
    '''
    def __init__(self, subject=None):
        self._subject = subject

    def get_table_names(self, with_item_score=False):
        '''
        Returns the table based on subject parameter in report
        '''
        if self._subject:
            return (get_table_name(self._subject, with_item_score),)
        else:
            return get_all_table_names(with_item_score)

    def get_table_name(self, with_item_score=False):
        assert self._subject is not None
        return self.get_table_names(with_item_score)[0][0]
