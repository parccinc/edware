'''
Created on Aug 1, 2013

@author: dawu
'''
from collections import OrderedDict
from reporting.reports.helpers.constants import Constants, Subject
from edcore.utils.utils import reverse_map


SUBJECTS_MAP = OrderedDict([(Subject.ELA, Constants.SUBJECT2), (Subject.MATH, Constants.SUBJECT1)])

ORDERED_SUBJECT_DICT = OrderedDict([(Constants.SUBJECT2, Subject.ELA), (Constants.SUBJECT1, Subject.MATH)])  # sigh


def get_ordered_subjects_dict():
    '''
    :returns: a list of subjects ordered in the order they are supposed to appear in FE dropdowns
    '''
    return ORDERED_SUBJECT_DICT


def get_subjects_map():
    '''
    :returns: dict of subjects mapping
    '''
    return SUBJECTS_MAP


def get_subject_by_alias(subject_alias):
    return reverse_map(SUBJECTS_MAP).get(subject_alias)
