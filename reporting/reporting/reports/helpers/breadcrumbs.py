'''
Breadcrumbs are a front-end navigation widget to traverse up
and down the report hierarchy. They are displayed at the top
of each report.

Created on Mar 8, 2013

@author: dip
'''
import datetime
from sqlalchemy.sql import and_, select
from reporting.reports.helpers.constants import Constants
from edcore.database.edcore_connector import EdCoreDBConnection
from reporting.reports.helpers.query_utils import execute_query_remotely
from sqlalchemy.sql.functions import func

STATE_NAMES = {
    'AL': 'Alabama',
    'AK': 'Alaska',
    'AZ': 'Arizona',
    'AR': 'Arkansas',
    'CA': 'California',
    'CO': 'Colorado',
    'CT': 'Connecticut',
    'DE': 'Delaware',
    'DC': 'District of Columbia',
    'FL': 'Florida',
    'GA': 'Georgia',
    'HI': 'Hawaii',
    'ID': 'Idaho',
    'IL': 'Illinois',
    'IN': 'Indiana',
    'IA': 'Iowa',
    'KS': 'Kansas',
    'KY': 'Kentucky',
    'LA': 'Louisiana',
    'ME': 'Maine',
    'MD': 'Maryland',
    'MA': 'Massachusetts',
    'MI': 'Michigan',
    'MN': 'Minnesota',
    'MS': 'Mississippi',
    'MO': 'Missouri',
    'MT': 'Montana',
    'NE': 'Nebraska',
    'NV': 'Nevada',
    'NH': 'New Hampshire',
    'NJ': 'New Jersey',
    'NM': 'New Mexico',
    'NY': 'New York',
    'NC': 'North Carolina',
    'ND': 'North Dakota',
    'OH': 'Ohio',
    'OK': 'Oklahoma',
    'OR': 'Oregon',
    'PA': 'Pennsylvania',
    'RI': 'Rhode Island',
    'SC': 'South Carolina',
    'SD': 'South Dakota',
    'TN': 'Tennessee',
    'TX': 'Texas',
    'UT': 'Utah',
    'VT': 'Vermont',
    'VA': 'Virginia',
    'WA': 'Washington',
    'WV': 'West Virginia',
    'WI': 'Wisconsin',
    'WY': 'Wyoming'
}


def get_state_name(state_code):
    '''
    Given a state code, return the state name
    If the state code is not know, we return Example State [stateCode]
    '''
    return STATE_NAMES.get(state_code, 'Example State {0}'.format(state_code))


def get_breadcrumbs_context(table_name, state_code=None, district_guid=None, school_guid=None, asmt_grade=None, student_guid=None, tenant=None, is_course=False):
    '''
    Given certain known information, returns breadcrumbs context
    It'll always return "home" breadcrumbs into results
    '''
    formatted_results = [{'type': 'home', 'name': 'Home'}]
    results = None
    if state_code:
        with EdCoreDBConnection(tenant=tenant, state_code=state_code) as connector:
            table = connector.get_table(table_name)
            # Limit result count to one
            # We limit the results to one since we'll get multiple rows with the same values
            # Think of the case of querying for state name and id, we'll get all the schools in that state
            query = select([table.c.state_code.label(Constants.STATE_CODE),
                            table.c.resp_dist_name.label(Constants.DISTRICT_NAME),
                            table.c.resp_school_name.label(Constants.SCHOOL_NAME),
                            table.c.student_first_name.label(Constants.STUDENT_FIRST_NAME),
                            table.c.student_last_name.label(Constants.STUDENT_LAST_NAME),
                            func.DATE(table.c.student_dob).label(Constants.STUDENT_DOB)],
                           from_obj=[table], limit=1)

            query = query.where(and_(table.c.rec_status == Constants.CURRENT))
            if state_code is not None:
                query = query.where(and_(table.c.state_code == state_code))
            if district_guid is not None:
                query = query.where(and_(table.c.resp_dist_id == district_guid))
                if school_guid is not None:
                    query = query.where(and_(table.c.resp_school_id == school_guid))
            if student_guid is not None:
                query = query.where(and_(table.c.student_parcc_id == student_guid))

            # run it and format the results
            results = execute_query_remotely(state_code, query)
    if results:
        result = results[0]
        # return an hierarchical ordered list
        formatted_results.append({'type': 'state', 'name': get_state_name(result[Constants.STATE_CODE]), 'id': result[Constants.STATE_CODE]})
        if district_guid is not None:
            formatted_results.append({'type': 'district', 'name': result[Constants.DISTRICT_NAME], 'id': district_guid})
            if school_guid is not None:
                formatted_results.append({'type': 'school', 'name': result[Constants.SCHOOL_NAME], 'id': school_guid})
                if asmt_grade is not None:
                    formatted_results.append({'type': 'grade', 'name': asmt_grade, 'id': asmt_grade, 'isCourse': is_course})
                    if student_guid is not None:
                        student_name = "%s %s" % (result[Constants.STUDENT_FIRST_NAME], result[Constants.STUDENT_LAST_NAME])
                        student_dob = result[Constants.STUDENT_DOB]
                        student = {
                            'id': student_guid,
                            'type': 'student',
                            'name': student_name,
                            'dob': student_dob.strftime('%m/%d/%y') if type(student_dob) is datetime.date else student_dob,
                        }
                        formatted_results.append(student)

    return {'items': formatted_results}
