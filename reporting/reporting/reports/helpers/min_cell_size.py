from pyramid.threadlocal import get_current_registry
from edcore.security.tenant import get_tenant_by_state_code


def get_minimum_cell_size_for_state(state_code, report_type):
    """
    :param report_type: the report type of minimum cell size needed
    :return: the minimum cell size for that state and type
    """
    tenant = get_tenant_by_state_code(state_code)
    return get_minimum_cell_size(tenant, report_type)


def get_minimum_cell_size(tenant, report_type, override_settings=None):
    settings = override_settings if override_settings else get_current_registry().settings
    return int(settings.get("min_cell_size.{0}.{1}".format(report_type, tenant), 0))


def get_minimum_alert_cell_size(override_settings=None):
    settings = override_settings if override_settings else get_current_registry().settings
    return int(settings.get("alert_cell_size", 0))
