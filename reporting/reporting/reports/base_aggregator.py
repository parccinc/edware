from sqlalchemy.sql.expression import and_, literal
from sqlalchemy.sql.expression import true

from collections import namedtuple
from edcore.database.edcore_connector import EdCoreDBConnection
from edcore.database.routing import ReportingDbConnection
from edcore.security.tenant import get_cds_states_text, get_cds_opt_out_states
from pyramid.security import authenticated_userid
from pyramid.threadlocal import get_current_request
from reporting.reports.helpers.breadcrumbs import get_state_name
from reporting.reports.helpers.constants import AssessmentType, Views, LevelValues
from reporting.reports.helpers.metadata import get_subjects_map
from reporting.reports.helpers.query_utils import \
    construct_query_info
from reporting.reports.helpers.grade_course import get_grade_course_expr
from reporting.reports.helpers.min_cell_size import get_minimum_cell_size_for_state,\
    get_minimum_alert_cell_size
from reporting.reports.helpers.constants import Constants
from edcore.utils.utils import merge_dict, reverse_map
from reporting.reports.helpers.report_table_map import SUBJECT_TO_RPT_TABLE
from edcore.utils.query import QueryMapKey


ReportLevel = namedtuple('ReportLevel', ['level', 'column', 'value'])


class BaseAggregator:

    def __init__(self,
                 table_names,
                 stateCode,
                 districtGuid=None,
                 schoolGuid=None,
                 asmtYear=None,
                 asmtType=None,
                 gradeCourse=None,
                 result=None,
                 filterParams=None,
                 view=None,
                 min_cell_size=None,
                 consortium_min_cell_size=None,
                 alert_cell_size=None,
                 min_cell_size_msg=None):
        self.table_names = table_names
        self.state_code = stateCode
        self.district_guid = districtGuid
        self.school_guid = schoolGuid
        self.asmt_type = asmtType
        self.asmt_year = asmtYear
        self.grade_course = gradeCourse
        self.result = result
        self.filters = filterParams
        self.levels = self._create_level_iterator(stateCode, districtGuid, schoolGuid, LevelValues.PARCC)
        self.view = view or Views.PERFORMANCE

        self.min_cell_size = min_cell_size
        self.consortium_min_cell_size = consortium_min_cell_size
        self.alert_cell_size = alert_cell_size
        self.min_cell_size_msg = min_cell_size_msg

    @staticmethod
    def _create_level_iterator(stateCode, districtGuid, schoolGuid, parccCode=None):
        levels = []
        if parccCode:
            levels.append(ReportLevel(LevelValues.PARCC, '', get_cds_states_text()))
        if stateCode:
            levels.append(ReportLevel(LevelValues.STATE, 'state_code', stateCode))
        if districtGuid:
            levels.append(ReportLevel(LevelValues.DISTRICT, 'resp_dist_id', districtGuid))
        if schoolGuid:
            levels.append(ReportLevel(LevelValues.SCHOOL, 'resp_school_id', schoolGuid))
        return levels

    @staticmethod
    def should_disable_summary(report_level):
        user_state_codes = set(authenticated_userid(get_current_request()).get_state_codes())
        disabled = report_level == LevelValues.PARCC and user_state_codes.issubset(get_cds_opt_out_states())
        return disabled

    def get_summary_query_map(self):
        summary_query_map = {}
        while self.levels:
            level = self.levels[-1]
            summary_query_map = merge_dict(summary_query_map, self._get_summary_query_map_for_level(level))
            self.levels.pop()
        return summary_query_map

    def _get_summary_query_map_for_level(self, level):
        if level.level == LevelValues.PARCC:
            return self._get_summary_query_map_for_parcc(level)
        query_map = {}
        # queries
        with EdCoreDBConnection(state_code=self.state_code) as conn:
            for tables in self.table_names:
                asmt_tables = [conn.get_table(table) for table in tables]
                where_condition = self._create_where_condition(asmt_tables[0])
                query_map[QueryMapKey(level=level.level, subject=tables[0], type=Constants.SUMMARY)] =\
                    construct_query_info(query=self.get_summary_query(level, where_condition, *asmt_tables),
                                         state_code=self.state_code)
        return query_map

    def _get_summary_query_map_for_parcc(self, parcc_level):
        query_map = {}
        # queries
        with ReportingDbConnection(tenant=Constants.CONSORTIUM_NAME, is_public=True) as conn:
            for tables in self.table_names:
                asmt_tables = [conn.get_table(table) for table in tables]
                where_condition = self._create_where_condition(asmt_tables[0])

                query = self.get_summary_query(parcc_level, where_condition, *asmt_tables)
                query_map_key = QueryMapKey(level=LevelValues.PARCC, subject=tables[0],
                                            type=Constants.SUMMARY, tenant=Constants.CONSORTIUM_NAME)

                query_map[query_map_key] = construct_query_info(query=query,
                                                                tenant=Constants.CONSORTIUM_NAME,
                                                                is_public=True)
        return query_map

    def format_summary_results(self, mapped_results):
        summaries = []
        table_to_subject_map = reverse_map(SUBJECT_TO_RPT_TABLE)
        while self.levels:
            institution_level = self.levels[-1]
            subject_to_summary_map = {}
            for type, subject, level, tenant in mapped_results.keys():
                if type == Constants.SUMMARY and level == institution_level.level:
                    summary_data = mapped_results[QueryMapKey(type, subject, level, tenant)]
                    if self.is_valid_summary_data(summary_data):
                        subject_to_summary_map[table_to_subject_map[subject]] = summary_data
            if len(subject_to_summary_map) > 0:
                summaries.append(self.format_summary(institution_level.level, subject_to_summary_map))
            self.levels.pop()
        return self._group_by_subject(summaries)

    def get_grade_course_where_expr(self, asmt_table):
        return get_grade_course_expr(self.grade_course, asmt_table)

    def _create_where_condition(self, asmt_table):
        return [asmt_table.c[level.column] == level.value for level in self.levels
                if level.level != LevelValues.PARCC]

    def _group_by_subject(self, summaries):
        results = {}
        for asmtType in (AssessmentType.SUMMATIVE,):
            results.setdefault(asmtType, {})
            for _, subject_key in get_subjects_map().items():
                results[asmtType].setdefault(subject_key, [])
                for summary in reversed(summaries):  # display lowest level first
                    results[asmtType][subject_key] += summary[asmtType][subject_key]
        return results

    def format_summary(self, level, results):
        raise NotImplementedError()

    def get_summary_query(self, asmt_table, level, level_where_condition):
        raise NotImplementedError()

    def is_valid_summary_data(self, summary_data):
        if summary_data:
            # for parcc level queries, we can get a row of Nones (apart from some constants)
            # even when there is no data so check if all non constant fields are None
            return [x for x in self.get_summary_data_without_constants(summary_data).values()
                    if x is not None]
        return False

    def get_summary_data_without_constants(self, summary_data):
        summary_data_no_constants = summary_data[0].copy()
        if Constants.NAME in summary_data_no_constants:
            del summary_data_no_constants[Constants.NAME]
        return summary_data_no_constants


def get_non_parcc_level_case(asmt_table, level, state_code=None):
    if state_code is not None:
        state_level_value = get_state_name(state_code)
    else:
        state_level_value = asmt_table.c.state_code
    return [(literal(level.level) == LevelValues.STATE, state_level_value),
            (literal(level.level) == LevelValues.DISTRICT, asmt_table.c.resp_dist_name),
            (literal(level.level) == LevelValues.SCHOOL, asmt_table.c.resp_school_name),
            ]


def get_non_parcc_common_where_condition(asmt_table, level):
    column = "include_in_{0}".format(level.level)
    return and_(asmt_table.c.rec_status == Constants.CURRENT,
                asmt_table.c[column] == true())
