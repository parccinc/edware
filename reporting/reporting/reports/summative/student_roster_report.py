from sqlalchemy import and_, case, func, literal, select
from sqlalchemy.sql.expression import not_

from edcore.security.tenant import get_cds_opt_out_states
from edcore.utils.utils import compile_query_to_sql_text
from reporting.reports.base_aggregator import get_non_parcc_common_where_condition
from reporting.reports.base_aggregator import get_non_parcc_level_case
from reporting.reports.base_report import BaseSummativeReport
from reporting.reports.base_aggregator import BaseAggregator
from reporting.reports.formatter.student_roster_formatter import (build_subclaim_perf_level_attr,
                                                                  format_item_analysis_summary,
                                                                  format_scores_view_results,
                                                                  format_scores_view_summary,
                                                                  format_item_analysis_assessments)
from reporting.reports.helpers.constants import (ALL_PERFORMANCE_LEVELS,
                                                 LevelValues,
                                                 Constants, ExtractConstants, RptTable)
from reporting.reports.summative.helpers.constants import StudentRosterConstants
from reporting.reports.helpers.filters import apply_filter_to_query
from reporting.security.context import select_with_context
from parcc_common.security.constants import RolesConstants


class StudentRosterScoresReport(BaseSummativeReport):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.permission = RolesConstants.PII

    def get_query(self, asmt_table):
        select_columns = [
            asmt_table.c.sum_scale_score,
            asmt_table.c.sum_read_scale_score,
            asmt_table.c.sum_write_scale_score,
            asmt_table.c.subclaim1_category,
            asmt_table.c.subclaim2_category,
            asmt_table.c.subclaim3_category,
            asmt_table.c.subclaim4_category,
            asmt_table.c.subclaim5_category,
            asmt_table.c.subclaim6_category,
            asmt_table.c.state_growth_percent,
            asmt_table.c.parcc_growth_percent,
            asmt_table.c.student_parcc_id,
            asmt_table.c.asmt_subject,
            asmt_table.c.student_middle_name,
            asmt_table.c.student_first_name,
            asmt_table.c.student_last_name,
            asmt_table.c.student_grade,
            asmt_table.c.asmt_grade,
            asmt_table.c.state_code,
            asmt_table.c.sum_perf_lvl,
            asmt_table.c.include_in_roster,
        ]
        where_condition = and_(
            asmt_table.c.state_code == self.state_code,
            asmt_table.c.resp_dist_id == self.district_guid,
            asmt_table.c.resp_school_id == self.school_guid,
            asmt_table.c.rec_status == Constants.CURRENT,
            asmt_table.c.year == self.asmt_year,
            not_(asmt_table.c.include_in_roster == 'N'))
        where_condition = self.append_grade_course_where_condition(
            asmt_table, where_condition)
        where_condition = self.append_staff_id_where_condition(asmt_table, where_condition)
        query = select_with_context(select_columns,
                                    from_obj=asmt_table,
                                    permission=self.permission,
                                    state_code=self.state_code).where(where_condition)
        query = apply_filter_to_query(query, asmt_table, self.filters)
        return compile_query_to_sql_text(query)

    def format_results(self, results):
        return format_scores_view_results(results)

    def get_aggregator(self):
        return StudentRosterScoresReportAggregator


class StudentRosterScoresReportAggregator(BaseAggregator):

    def get_summary_query(self, level, level_where_condition, asmt_table):

        where_condition = and_(
            asmt_table.c.year == self.asmt_year,
            self.get_grade_course_where_expr(asmt_table),
            *level_where_condition)

        select_columns = [
            func.avg(asmt_table.c.sum_scale_score).label('sum_scale_score'),
            func.avg(asmt_table.c.sum_perf_lvl).label('sum_perf_lvl'),
            func.avg(asmt_table.c.sum_read_scale_score).label('sum_read_scale_score'),
            func.avg(asmt_table.c.sum_write_scale_score).label('sum_write_scale_score')
        ]

        if level.level != LevelValues.PARCC:
            select_columns += [func.avg(asmt_table.c.parcc_growth_percent).label('parcc_growth_percent')]
        if level.level not in (LevelValues.PARCC, LevelValues.STATE):
            select_columns += [func.avg(asmt_table.c.state_growth_percent).label('state_growth_percent')]

        if level.level == LevelValues.PARCC:
            name_column = literal(level.value)
            id_column = literal(level.level)
        else:
            id_column = case([
                (literal(level.level) == LevelValues.STATE, asmt_table.c.state_code),
                (literal(level.level) == LevelValues.DISTRICT, asmt_table.c.resp_dist_id),
                (literal(level.level) == LevelValues.SCHOOL, asmt_table.c.resp_school_id),
            ])
            name_column = case(get_non_parcc_level_case(asmt_table, level, self.state_code))
            where_condition = and_(
                where_condition,
                get_non_parcc_common_where_condition(asmt_table, level),
            )
        name_column = name_column.label(Constants.NAME)
        id_column = id_column.label(StudentRosterConstants.ID)
        select_columns += [id_column, name_column]
        select_columns += self._get_subclaims_count(asmt_table)
        query = select(select_columns, from_obj=asmt_table).where(
            where_condition)
        query = apply_filter_to_query(query, asmt_table, self.filters)
        if level.level != LevelValues.PARCC:
            query = query.group_by(id_column, name_column)
        return compile_query_to_sql_text(query)

    def _get_subclaims_count(self, asmt_table):

        def count_subclaims_by_perf_level(subclaim):
            for performance_level in ALL_PERFORMANCE_LEVELS:
                label = build_subclaim_perf_level_attr(
                    subclaim, performance_level)
                yield func.sum(case([(asmt_table.c[subclaim] == performance_level, 1)], else_=0)).label(label)

        columns = []
        for subclaim in StudentRosterConstants.SUBCLAIM_CATEGORIES:
            for count_by_perf_level in count_subclaims_by_perf_level(subclaim):
                columns.append(count_by_perf_level)
        return columns

    def format_summary(self, report_level, results):
        disabled = self.should_disable_summary(report_level)
        return format_scores_view_summary(report_level, results, disabled)

    def get_summary_data_without_constants(self, summary_data):
        summary_data_no_constants = summary_data[0].copy()
        if StudentRosterConstants.ID in summary_data_no_constants:
            del summary_data_no_constants[StudentRosterConstants.ID]
            del summary_data_no_constants[Constants.NAME]
        return summary_data_no_constants


class StudentRosterItemAnalysisReport(BaseSummativeReport):

    def __init__(self, **params):
        super().__init__(**params)
        self.additional_tables = (RptTable.RPT_ITEM_P_DATA, )
        self.permission = RolesConstants.PII
        self.report_type = 'item_analysis'

    def get_query(self, asmt_table, student_item_score, item_metadata_table):
        select_columns = [asmt_table.c.student_last_name,
                          asmt_table.c.student_first_name,
                          student_item_score.c.parent_item_score,
                          asmt_table.c.student_middle_name,
                          asmt_table.c.student_grade,
                          asmt_table.c.asmt_grade,
                          asmt_table.c.sum_perf_lvl,
                          asmt_table.c.sum_scale_score,
                          asmt_table.c.state_code,
                          asmt_table.c.asmt_subject,
                          asmt_table.c.include_in_roster,
                          student_item_score.c.student_parcc_id,
                          student_item_score.c.test_uuid,
                          student_item_score.c.item_uin,
                          student_item_score.c.item_max_score.label(
                              StudentRosterConstants.ITEM_MAX_POINTS),
                          item_metadata_table.c.standard1,
                          item_metadata_table.c.standard2,
                          item_metadata_table.c.standard3,
                          item_metadata_table.c.standard4,
                          item_metadata_table.c.standard5,
                          item_metadata_table.c.standard6,
                          # item_metadata_table.c.evidence_statement,
                          item_metadata_table.c.test_item_type]
        from_object = [
            asmt_table.join(student_item_score,
                            and_(asmt_table.c.sum_score_rec_uuid == student_item_score.c.test_uuid,
                                 asmt_table.c.student_parcc_id == student_item_score.c.student_parcc_id)).join(
                item_metadata_table,
                and_(item_metadata_table.c.item_guid == student_item_score.c.item_uin,
                     item_metadata_table.c.asmt_subject ==
                     ExtractConstants.GRADE_SUBJECT_TEST_CODE_MAP[self.subject][self.grade_course]),
                True)]

        where_condition = and_(asmt_table.c.state_code == self.state_code,
                               asmt_table.c.resp_school_id == self.school_guid,
                               asmt_table.c.resp_dist_id == self.district_guid,
                               asmt_table.c.rec_status == Constants.CURRENT,
                               student_item_score.c.rec_status == Constants.CURRENT,
                               asmt_table.c.year == self.asmt_year,
                               not_(asmt_table.c.include_in_roster == 'N'))
        where_condition = self.append_grade_course_where_condition(
            asmt_table, where_condition)
        where_condition = self.append_staff_id_where_condition(asmt_table, where_condition)
        query = select_with_context(select_columns,
                                    from_obj=from_object,
                                    permission=self.permission,
                                    state_code=self.state_code).where(where_condition)
        query = apply_filter_to_query(query, asmt_table, self.filters)
        return compile_query_to_sql_text(query)

    def format_assessments(self, results, assmnts):
        return format_item_analysis_assessments(results, assmnts)

    def format_results(self, results):
        """
        We use method 'format_assessments' and we don't need implementations of this method
        Should have to avoid:
            "TypeError: Can't instantiate abstract class StudentRosterItemAnalysisReport
            with abstract methods format_results"
        """
        pass

    def format_columns(self, results):
        """
        We use method 'format_assessments' and we don't need implementations of this method
        Should have to avoid:
            "TypeError: Can't instantiate abstract class StudentRosterItemAnalysisReport
            with abstract methods format_columns"
        """
        pass

    def get_aggregator(self):
        return StudentRosterItemAnalysisAggregator


class StudentRosterItemAnalysisAggregator(BaseAggregator):

    def get_summary_query(self, level, level_where_condition, asmt_table, student_item_score):

        where_condition = and_(self.get_grade_course_where_expr(asmt_table),
                               asmt_table.c.year == self.asmt_year,
                               *level_where_condition)
        if level.level == LevelValues.PARCC:
            name_column = literal(level.value)
            from_obj = [asmt_table.join(
                student_item_score,
                and_(asmt_table.c.sum_score_rec_uuid == student_item_score.c.test_uuid))]
        else:
            name_column = case(get_non_parcc_level_case(asmt_table, level, self.state_code))
            where_condition = and_(
                where_condition,
                get_non_parcc_common_where_condition(asmt_table, level),
            )
            from_obj = [asmt_table.join(
                student_item_score,
                and_(asmt_table.c.sum_score_rec_uuid == student_item_score.c.test_uuid,
                     asmt_table.c.student_parcc_id == student_item_score.c.student_parcc_id))]
        name_column = name_column.label(Constants.NAME)

        query = select([func.avg(student_item_score.c.parent_item_score).label('parent_item_score'),
                        func.avg(asmt_table.c.sum_perf_lvl).label(
                            'sum_perf_lvl'),
                        func.avg(asmt_table.c.sum_scale_score).label(
                            'sum_scale_score'),
                        student_item_score.c.item_uin,
                        name_column],
                       from_obj=from_obj)
        query = query.where(where_condition)
        query = apply_filter_to_query(query, asmt_table, self.filters)
        if level.level == LevelValues.PARCC:
            query = query.group_by(student_item_score.c.item_uin)
        else:
            query = query.group_by(student_item_score.c.item_uin, name_column)
        return compile_query_to_sql_text(query)

    def format_summary(self, report_level, results):
        disabled = report_level == LevelValues.PARCC and self.state_code in get_cds_opt_out_states()
        return format_item_analysis_summary(report_level, results, disabled)
