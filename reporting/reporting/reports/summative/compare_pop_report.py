'''
Created on Mar 7, 2013

@author: dwu
'''
from sqlalchemy import and_, func, literal, select
from sqlalchemy.sql.expression import case, true

from reporting.reports.helpers.constants import Constants, subclaim_to_table_col, OVERALL_PERFORMANCE_LEVELS, \
    ALL_PERFORMANCE_LEVELS, LevelValues, AssessmentType
from edapi.cache import cache_region
from reporting.reports.helpers.filters import apply_filter_to_query, has_filters
from edcore.utils.utils import compile_query_to_sql_text
from reporting.reports.base_aggregator import BaseAggregator, get_non_parcc_level_case
from reporting.reports.base_aggregator import get_non_parcc_common_where_condition
from reporting.reports.base_report import BaseSummativeReport
from reporting.reports.formatter.compare_pop_formatter import format_cpop_results
from reporting.reports.formatter.common import build_overall_perf_level_attr, build_subclaim_perf_level_attr
from reporting.reports.helpers.legend import get_legend_info_query
from functools import partial
from reporting.reports.summative.helpers.constants import CPopConstants, Constants as SummativeConstants


CACHE_REGION_PUBLIC_DATA = 'public.data'
CACHE_REGION_PUBLIC_FILTERING_DATA = 'public.filtered_data'


def get_comparing_populations_cache_route(comparing_pop):
    '''
    Returns cache region based on whether filters exist
    If school_guid is present, return none - do not cache

    :param comparing_pop:  instance of ComparingPopReport
    '''
    if comparing_pop.school_guid is not None:
        return None  # do not cache school level
    return CACHE_REGION_PUBLIC_FILTERING_DATA if len(comparing_pop.filters.keys()) > 0 else CACHE_REGION_PUBLIC_DATA


def get_comparing_populations_cache_key(comparing_pop):
    '''
    Returns cache key for comparing populations report

    :param comparing_pop:  instance of ComparingPopReport
    :returns: a tuple representing a unique key for comparing populations report
    '''
    cache_args = []
    if comparing_pop.state_code is not None:
        cache_args.append(comparing_pop.state_code)
    if comparing_pop.district_guid is not None:
        cache_args.append(comparing_pop.district_guid)
    cache_args.append(comparing_pop.view)
    cache_args.append(comparing_pop.asmt_year)
    cache_args.append(comparing_pop.asmt_type)
    cache_args.append(comparing_pop.subject)
    cache_args.append(comparing_pop.grade_course)
    cache_args.append(comparing_pop.result)
    filters = comparing_pop.filters
    # sorts dictionary of keys
    cache_args.append(sorted(filters.items(), key=lambda x: x[0]))
    return tuple(cache_args)


def get_overall_levels(asmt_table):
    columns = []
    for performance_level in OVERALL_PERFORMANCE_LEVELS:
        label = build_overall_perf_level_attr(performance_level)
        columns.append(func.sum(case([(asmt_table.c.sum_perf_lvl == performance_level, 1)], else_=0)).label(label))
    return columns


def get_subscore_levels(result, asmt_table):
    claim_column = subclaim_to_table_col(result)
    columns = []
    for performance_level in ALL_PERFORMANCE_LEVELS:
        label = build_subclaim_perf_level_attr(claim_column, performance_level)
        columns.append(func.sum(case([(asmt_table.c[claim_column] == performance_level, 1)], else_=0)).label(label))
    return columns


def get_avg_columns(asmt_table):
    return [
        func.avg(asmt_table.c.sum_scale_score).label(Constants.SCORE),
        func.avg(asmt_table.c.sum_read_scale_score).label(SummativeConstants.READING),
        func.avg(asmt_table.c.sum_write_scale_score).label(SummativeConstants.WRITING),
    ]


class ComparingPopReport(BaseSummativeReport):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.result = SummativeConstants.OVERALL if not self.result else self.result
        if self.result == SummativeConstants.OVERALL:
            self.perf_level_columns_generator = get_overall_levels
        else:
            self.perf_level_columns_generator = partial(get_subscore_levels, self.result)

    def get_applicable_summary_info(self, summary_result):
        """
        Returns a disabled summary info if required
        """
        req_summary_result = summary_result
        if BaseAggregator.should_disable_summary(summary_result[SummativeConstants.SUMMARY_ROW_TYPE]):
            req_summary_result = {
                SummativeConstants.ROW_DISABLED: True,
                SummativeConstants.SUMMARY_ROW_TYPE: summary_result[SummativeConstants.SUMMARY_ROW_TYPE],
            }
        return req_summary_result

    def get_results(self):
        formatted_results = self.get_unfiltered_results()
        if Constants.SUMMARY in formatted_results:
            summary_results = formatted_results[Constants.SUMMARY][AssessmentType.SUMMATIVE][self.subject]
            filtered_results = [self.get_applicable_summary_info(summary_result) for summary_result in summary_results]
            formatted_results[Constants.SUMMARY][AssessmentType.SUMMATIVE][self.subject] = filtered_results
        return formatted_results

    @cache_region([CACHE_REGION_PUBLIC_DATA, CACHE_REGION_PUBLIC_FILTERING_DATA], router=get_comparing_populations_cache_route, key_generator=get_comparing_populations_cache_key)
    def get_unfiltered_results(self):
        """
        Returns results disregarding cds opt out flags.
        This is so that we can cache this information for all users and remove the parcc summaries for other users later
        """
        return super().get_results()

    def get_query(self, asmt_table):
        select_columns = [func.count(asmt_table.c.student_parcc_id).label(SummativeConstants.STUDENTS)]
        select_columns += get_avg_columns(asmt_table)
        select_columns += self.perf_level_columns_generator(asmt_table)

        where_condition = and_(asmt_table.c.state_code == self.state_code,
                               asmt_table.c.rec_status == Constants.CURRENT,
                               asmt_table.c.year == self.asmt_year)

        return self.build_query_with_entity_info(asmt_table, select_columns, where_condition)

    def build_query_with_entity_info(self, asmt_table, select_columns, where_condition):
        '''
        Append specific columns/where clause/group by according to the level of the report
        '''
        select_columns = self.get_select(asmt_table, select_columns)
        where_condition = self.get_where(asmt_table, where_condition)
        group_columns = self.get_guid_and_name(asmt_table)

        query = select(select_columns, from_obj=asmt_table).where(where_condition).group_by(*group_columns)
        query = apply_filter_to_query(query, asmt_table, self.filters)
        return compile_query_to_sql_text(query)

    def get_legend(self, table):
        if self.result == SummativeConstants.OVERALL:
            return get_legend_info_query(table, self.subject, self.grade_course)
        else:
            return None

    def get_guid_and_name(self, asmt_table):
        if self.district_guid:
            guid = asmt_table.c['resp_school_id']
            name = asmt_table.c['resp_school_name']
        else:
            guid = asmt_table.c['resp_dist_id']
            name = asmt_table.c['resp_dist_name']
        return guid, name

    def get_select(self, asmt_table, select_columns):
        guid, name = self.get_guid_and_name(asmt_table)
        select_columns.extend([
            name.label(Constants.NAME),
            guid.label(SummativeConstants.GUID),
            func.median(asmt_table.c.state_growth_percent).label(CPopConstants.STATE),
            func.median(asmt_table.c.parcc_growth_percent).label(CPopConstants.PARCC),
        ])
        return select_columns

    def get_where(self, asmt_table, where_condition):
        where_condition = self.append_grade_course_where_condition(asmt_table, where_condition)
        if self.district_guid:
            # district report, use include in school flag to include/exclude from school aggregates
            where_condition = and_(where_condition,
                                   asmt_table.c.resp_dist_id == self.district_guid,
                                   asmt_table.c.include_in_school == true())
        else:
            # state report, use include in district flag to include/exclude from district aggregates
            where_condition = and_(where_condition, asmt_table.c.include_in_district == true())
        return where_condition

    def format_results(self, results):
        return format_cpop_results(self.result,
                                   has_filters(self.filters),
                                   self.alert_cell_size,
                                   self.min_cell_size,
                                   self.min_cell_size_msg,
                                   results)

    def get_aggregator(self):
        return ComparingPopAggregator


class ComparingPopAggregator(BaseAggregator):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.result == SummativeConstants.OVERALL:
            self.perf_level_columns_generator = get_overall_levels
        else:
            self.perf_level_columns_generator = partial(get_subscore_levels, self.result)

    def get_summary_query(self, level, level_where_condition, asmt_table):

        where_condition = and_(
            asmt_table.c.year == self.asmt_year,
            self.get_grade_course_where_expr(asmt_table),
            *level_where_condition)

        if level.level == LevelValues.PARCC:
            name_column = literal(level.value)
            id_column = literal(level.level)
        else:
            id_column = case([
                (literal(level.level) == LevelValues.STATE, asmt_table.c.state_code),
                (literal(level.level) == LevelValues.DISTRICT, asmt_table.c.resp_dist_id),
            ])
            name_column = case(get_non_parcc_level_case(asmt_table, level, self.state_code))
            where_condition = and_(
                where_condition,
                get_non_parcc_common_where_condition(asmt_table, level),
            )
        name_column = name_column.label(Constants.NAME)
        id_column = id_column.label(SummativeConstants.GUID)
        select_columns = [
            func.count().label(SummativeConstants.STUDENTS),
            id_column,
            name_column
        ]
        select_columns += get_avg_columns(asmt_table)
        select_columns += self.perf_level_columns_generator(asmt_table)
        query = select(select_columns, from_obj=asmt_table).where(where_condition)
        query = apply_filter_to_query(query, asmt_table, self.filters)
        if level.level != LevelValues.PARCC:
            query = query.group_by(id_column, name_column)
        return compile_query_to_sql_text(query)

    def format_summary(self, level, results):
        min_cell_size = self.consortium_min_cell_size if level == LevelValues.PARCC else self.min_cell_size
        return format_cpop_results(self.result,
                                   has_filters(self.filters),
                                   self.alert_cell_size,
                                   min_cell_size,
                                   self.min_cell_size_msg,
                                   results,
                                   report_level=level)

    def get_summary_data_without_constants(self, summary_data):
        summary_data_no_constants = super().get_summary_data_without_constants(summary_data)
        if SummativeConstants.GUID in summary_data_no_constants:
            del summary_data_no_constants[SummativeConstants.GUID]
        if SummativeConstants.STUDENTS in summary_data_no_constants:
            del summary_data_no_constants[SummativeConstants.STUDENTS]
        return summary_data_no_constants
