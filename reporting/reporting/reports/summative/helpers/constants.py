class Constants:
    PERFORMANCE_SORT = 'perfSortValue'
    PERFORMANCELEVELS = 'performanceLevels'
    OVERALL = 'overall'
    STUDENTS = 'students'
    READING = 'reading'
    WRITING = 'writing'
    AVG = 'avg'
    RESULTTYPE = 'result'
    GUID = 'guid'
    NAME = 'name'
    ASMT_SCALED_SCORE = 'sum_scale_score'
    HAS_FILTERS = 'has_filters'
    ALERT_THRESHOLD = 'alert_threshold'
    IS_LESS_THAN_THRESHOLD = 'is_less_than_threshold'
    IS_LESS_THAN_MIN_CELL_SIZE = 'is_less_than_min_cell_size'
    MIN_CELL_SIZE_MSG = 'min_cell_size_msg'
    PARCC_GROWTH_PERCENT = 'parcc_growth_percent'
    STATE_GROWTH_PERCENT = 'state_growth_percent'
    SUMMARY_ROW_TYPE = 'type'
    ROW_DISABLED = 'disabled'
    LEVEL_TITLES = {1: "BELOW", 2: "NEAR", 3: "AT OR ABOVE"}
    STUDENT_COUNT = 'student_count'


class StudentReportConstants:
    ASMT_SUBJECT = 'asmt_subject'
    PERFORMANCE = 'performance'
    CLAIM2_SCORE = 'sum_write_scale_score'
    CLAIM1_SCORE = 'sum_read_scale_score'
    CLAIM_MAP = {
        CLAIM1_SCORE: Constants.READING,
        CLAIM2_SCORE: Constants.WRITING
    }
    SUM_WRITE_CSEM = 'sum_write_csem'
    SUM_READ_CSEM = 'sum_read_csem'
    ERROR_FIELD = 'error_field'
    SCORE_FIELD = 'score_field'
    SUM_CSEM = 'sum_csem'
    AVERAGE_CLAIM_SCORE = 'average_claim_score'
    AVERAGE_SCORE = 'average_score'
    TEXT_BODY_GRADECOURSE = 'textBodyGradeCourse'
    REPORT_TITLE_PREFIX = 'titlePrefix'
    RANGE = 'range'
    SUBCLAIM_CATEGORIES = 'subclaim_categories'
    SUBSCORE = 'subscore'
    CLAIMS = 'claims'
    ERROR = 'error'
    CUTPOINT_END = 'cutpoint_end'
    CUTPOINT_START = 'cutpoint_start'
    NAME = 'name'
    SCORE = 'score'
    PERFORMANCE_LEVEL = 'level'
    COURSE = 'course'
    SUBSCORES = 'subscores'
    ASMT_PERF_LVL = 'sum_perf_lvl'
    PARCC_LEVEL_4_AVERAGE = 'level4_avg'


class CPopConstants:
    MEDIAN = "median"
    STATE = 'state'
    PARCC = 'parcc'
    GROWTH = 'growth'
    CDS_OPT_OUT = 'CDS_OPT_OUT'


class StudentRosterConstants:
    TEST_ITEM_TYPE = 'test_item_type'
    STUDENT_GRADE = 'student_grade'
    ITEM_UIN = 'item_uin'
    ITEM_MAX_POINTS = 'item_max_points'
    PARENT_ITEM_SCORE = 'parent_item_score'
    ID = 'id'
    SUM_PERF_LVL = 'sum_perf_lvl'
    STANDARDS = 'standards'
    STANDARD1 = 'standard1'
    STANDARD2 = 'standard2'
    STANDARD3 = 'standard3'
    STANDARD4 = 'standard4'
    STANDARD5 = 'standard5'
    STANDARD6 = 'standard6'
    STANDARDS_LIST = [STANDARD1, STANDARD2, STANDARD3, STANDARD4, STANDARD5, STANDARD6]
    ITEM_TYPE = 'item_type'
    ITEM_NUM_RESPONSES = 'item_num_responses'
    SUBCLAIM1_CATEGORY = 'subclaim1_category'
    SUBCLAIM2_CATEGORY = 'subclaim2_category'
    SUBCLAIM3_CATEGORY = 'subclaim3_category'
    SUBCLAIM4_CATEGORY = 'subclaim4_category'
    SUBCLAIM5_CATEGORY = 'subclaim5_category'
    SUBCLAIM6_CATEGORY = 'subclaim6_category'
    SUBCLAIM_CATEGORIES = [
        SUBCLAIM1_CATEGORY,
        SUBCLAIM2_CATEGORY,
        SUBCLAIM3_CATEGORY,
        SUBCLAIM4_CATEGORY,
        SUBCLAIM5_CATEGORY,
        SUBCLAIM6_CATEGORY,
    ]
    INCLUDE_IN_ROSTER = 'include_in_roster'
    IS_INVALID_SCORE = 'INVALID_SCORE'
    OVERALL = 'Overall'
    DESCRIPTION = 'description'
