from sqlalchemy.sql.expression import true
from sqlalchemy import select, and_, case, func, literal

from collections import defaultdict
from edcore.database.routing import ReportingDbConnection
from edcore.utils.utils import reverse_map
from reporting.reports.helpers.query_utils import construct_query_info
from reporting.security.context import select_with_context
from reporting.reports.helpers.constants import Constants, RptTable, LevelValues
from reporting.reports.summative.helpers.constants import StudentReportConstants
from edcore.utils.utils import compile_query_to_sql_text
from reporting.reports.base_report import BaseSummativeReport
from reporting.reports.formatter.student_report_formatter import format_results, format_summary
from reporting.reports.helpers.report_table_map import SUBJECT_TO_RPT_TABLE
from reporting.reports.base_aggregator import BaseAggregator, get_non_parcc_common_where_condition, ReportLevel, \
    get_non_parcc_level_case
from reporting.reports.helpers.filters import apply_filter_to_query
from edcore.utils.query import QueryMapKey
from parcc_common.security.constants import RolesConstants


class StudentReport(BaseSummativeReport):

    def __init__(self, **params):
        super().__init__(**params)
        self.additional_tables = (RptTable.RPT_SUM_TEST_LEVEL_M,)
        self.permission = RolesConstants.PII

    def get_query(self, asmt_table, test_level_table):
        select_columns = [asmt_table.c.sum_perf_lvl,
                          asmt_table.c.sum_scale_score,
                          asmt_table.c.sum_csem,
                          asmt_table.c.sum_read_scale_score,
                          asmt_table.c.sum_read_csem,
                          asmt_table.c.sum_write_scale_score,
                          asmt_table.c.sum_write_csem,
                          asmt_table.c.asmt_subject,
                          asmt_table.c.asmt_grade,
                          asmt_table.c.subclaim1_category,
                          asmt_table.c.subclaim2_category,
                          asmt_table.c.subclaim3_category,
                          asmt_table.c.subclaim4_category,
                          asmt_table.c.subclaim5_category,
                          asmt_table.c.subclaim6_category,
                          test_level_table.c.perf_lvl_id,
                          test_level_table.c.cutpoint_label,
                          test_level_table.c.cutpoint_low,
                          test_level_table.c.cutpoint_upper
                          ]

        from_object = [asmt_table.join(test_level_table, and_(asmt_table.c.test_code == test_level_table.c.test_code))]
        where_condition = and_(asmt_table.c.state_code == self.state_code,
                               asmt_table.c.resp_dist_id == self.district_guid,
                               asmt_table.c.resp_school_id == self.school_guid,
                               asmt_table.c.student_parcc_id == self.student_guid,
                               asmt_table.c.rec_status == Constants.CURRENT,
                               asmt_table.c.year == self.asmt_year,
                               asmt_table.c.include_in_isr == true(),
                               test_level_table.c.rec_status == Constants.CURRENT)
        where_condition = self.append_grade_course_where_condition(
            asmt_table, where_condition)
        where_condition = self.append_staff_id_where_condition(
            asmt_table, where_condition)
        query = select_with_context(select_columns,
                                    from_obj=from_object,
                                    permission=self.permission,
                                    state_code=self.state_code).where(where_condition)
        query = query.order_by(test_level_table.c.perf_lvl_id.asc())
        return compile_query_to_sql_text(query)

    def format_results(self, results):
        return format_results(results)

    def get_aggregator(self):
        return StudentReportAggregator


class StudentReportAggregator(BaseAggregator):

    def get_summary_query(self, level, level_where_condition, asmt_table, perf_level=None):

        where_condition = and_(
            asmt_table.c.year == self.asmt_year,
            self.get_grade_course_where_expr(asmt_table),
            *level_where_condition
        )
        if perf_level is not None:
            where_condition = and_(
                where_condition,
                asmt_table.c.sum_perf_lvl == perf_level,
            )
        if level.level == LevelValues.PARCC:
            name_column = literal(level.level)
        else:
            name_column = case(get_non_parcc_level_case(asmt_table, level))
            where_condition = and_(
                where_condition,
                get_non_parcc_common_where_condition(asmt_table, level),
            )
        name_column = name_column.label(Constants.NAME)
        select_columns = [
            func.avg(asmt_table.c.sum_scale_score).label(StudentReportConstants.AVERAGE_SCORE),
            func.avg(asmt_table.c[StudentReportConstants.CLAIM1_SCORE]).label(StudentReportConstants.CLAIM1_SCORE),
            func.avg(asmt_table.c[StudentReportConstants.CLAIM2_SCORE]).label(StudentReportConstants.CLAIM2_SCORE),
            name_column,
        ]
        query = select(select_columns, from_obj=asmt_table).where(where_condition)
        query = apply_filter_to_query(query, asmt_table, self.filters)
        if level.level != LevelValues.PARCC:
            query = query.group_by(name_column)
        return compile_query_to_sql_text(query)

    def format_summary(self, report_level, results, disabled=False):
        return format_summary(report_level, results, disabled)

    def get_summary_query_map(self):
        summary_query_map = super().get_summary_query_map()
        parcc_level = ReportLevel(LevelValues.PARCC, '', LevelValues.PARCC)
        with ReportingDbConnection(tenant=Constants.CONSORTIUM_NAME, is_public=True) as conn:
            for tables in self.table_names:
                asmt_tables = [conn.get_table(table) for table in tables]
                summary_query_map[QueryMapKey(level=LevelValues.PARCC, subject=tables[0],
                                              type=StudentReportConstants.PARCC_LEVEL_4_AVERAGE)] = \
                    construct_query_info(query=self.get_summary_query(parcc_level, [], *asmt_tables, perf_level=3),
                                         tenant=Constants.CONSORTIUM_NAME, is_public=True)
        return summary_query_map

    def get_level_4_averages(self, mapped_results):
        level_4_averages = {}
        for table_tuple in self.table_names:
            subject_table = table_tuple[0]
            subject = reverse_map(SUBJECT_TO_RPT_TABLE)[subject_table]
            level_4_averages[subject] = defaultdict()
            for claim_key, claim_value in StudentReportConstants.CLAIM_MAP.items():
                key = QueryMapKey(level=LevelValues.PARCC, subject=subject_table, type=StudentReportConstants.PARCC_LEVEL_4_AVERAGE)
                lvl_4_result = mapped_results[key][0]
                level_4_averages[subject][claim_value] = lvl_4_result[claim_key]
        return level_4_averages

    def format_summary_results(self, mapped_results):
        summaries = []
        level_4_averages = self.get_level_4_averages(mapped_results)
        table_to_subject_map = reverse_map(SUBJECT_TO_RPT_TABLE)
        while self.levels:
            institution_level = self.levels[-1].level
            subject_to_summary_map = {}
            for type, subject_table, level, tenant in mapped_results.keys():
                if type == Constants.SUMMARY and level == institution_level:
                    subject = table_to_subject_map[subject_table]
                    results = mapped_results[QueryMapKey(type, subject_table, level, tenant)]
                    if self.is_valid_summary_data(results):
                        results = self._add_level_4_averages(results, level_4_averages[subject])
                        subject_to_summary_map[subject] = results
                        disabled = self.should_disable_summary(institution_level)
                        summaries.append(self.format_summary(institution_level, subject_to_summary_map, disabled))
            self.levels.pop()

        return self._group_by_subject(summaries)

    def _add_level_4_averages(self, results, level_4_averages):
        results_with_level_4_averages = []
        for result in results:
            result[StudentReportConstants.PARCC_LEVEL_4_AVERAGE] = level_4_averages
            results_with_level_4_averages.append(result)
        return results_with_level_4_averages

    def get_summary_data_without_constants(self, summary_data):
        summary_data_no_constants = super().get_summary_data_without_constants(summary_data)
        if StudentReportConstants.NAME in summary_data_no_constants:
            del summary_data_no_constants[StudentReportConstants.NAME]
        return summary_data_no_constants
