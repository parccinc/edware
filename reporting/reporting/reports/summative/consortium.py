'''
Created on May 9, 2015

@author: dip
'''
from sqlalchemy import and_, func, literal, select

from pyramid.security import authenticated_userid
from pyramid.threadlocal import get_current_request
from reporting.reports.helpers.constants import Constants, RptTable, AssessmentType, LevelValues
from reporting.reports.summative.helpers.constants import CPopConstants, Constants as SummativeConstants
from reporting.reports.helpers.filters import apply_filter_to_query, has_filters
from edcore.utils.utils import compile_query_to_sql_text, reverse_map
from reporting.reports.formatter.compare_pop_formatter import format_cpop_results
from reporting.reports.helpers.breadcrumbs import get_state_name
from reporting.reports.summative.compare_pop_report import get_avg_columns
from reporting.reports.helpers.report_table_map import SUBJECT_TO_RPT_TABLE
from reporting.reports.summative.compare_pop_report import ComparingPopReport
from reporting.reports.helpers.query_utils import execute_mapped_queries_remotely, construct_query_info
from edcore.security.tenant import get_cds_tenants_map, get_cds_states_text, get_cds_opt_out_states
from edcore.database.routing import ReportingDbConnection
from reporting.reports.helpers.min_cell_size import get_minimum_cell_size
from edcore.utils.query import QueryMapKey


class ConsortiumReport(ComparingPopReport):
    '''
    Consortium (Parcc) Report
    Uses CDS database to query aggregates
    '''
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cds_tenants_map = get_cds_tenants_map()
        self.tenant_min_cell_size = {}

    def setup_min_cell_size(self, override_settings=None):
        for tenant in self.cds_tenants_map:
            self.tenant_min_cell_size[tenant] = get_minimum_cell_size(tenant, self.view, override_settings)

    def get_query(self, name, guid, tenant, asmt_table, override_name=False):
        display_name = get_state_name(name) if override_name is False else name
        if not self.tenant_min_cell_size:
            self.setup_min_cell_size()
        min_cell_size = self.tenant_min_cell_size.get(tenant, 0)
        select_columns = [
            literal(display_name).label(Constants.NAME),
            literal(guid).label(SummativeConstants.GUID),
            func.count(asmt_table.c.test_code).label(SummativeConstants.STUDENTS),
            # embed minimum cell size for each state/consortium
            literal(min_cell_size).label(Constants.MIN_CELL_SIZE)
        ]
        select_columns += get_avg_columns(asmt_table)
        select_columns += self.perf_level_columns_generator(asmt_table)

        where_condition = and_(self.append_grade_course_where_condition(asmt_table, None),
                               asmt_table.c.year == self.asmt_year)

        query = select(select_columns, from_obj=asmt_table).where(where_condition)
        query = apply_filter_to_query(query, asmt_table, self.filters)
        # Add having cause to prevent zero results
        query = query.having(func.count(asmt_table.c.test_code).label(SummativeConstants.STUDENTS) > 0)
        return compile_query_to_sql_text(query)

    def run_query(self):
        query_map = {}
        is_public = True
        for tenant, state_code in self.cds_tenants_map.items():
            with ReportingDbConnection(tenant=tenant, is_public=is_public) as conn:
                for table_name in self.table_names:
                    tables = [conn.get_table(t) for t in table_name + self.additional_tables]
                    if tenant == Constants.CONSORTIUM_NAME:
                        query_map[QueryMapKey(type=Constants.SUMMARY)] = \
                            construct_query_info(query=self.get_summary_query(*tables),
                                                 tenant=tenant, is_public=is_public)
                        if self.result == SummativeConstants.OVERALL:
                            query_map[QueryMapKey(type=Constants.LEGEND)] = \
                                construct_query_info(query=self.get_legend(conn.get_table(RptTable.RPT_SUM_TEST_LEVEL_M)),
                                                     tenant=tenant, is_public=is_public)
                    else:
                        query_map[QueryMapKey(subject=table_name[0], type=Constants.ASSESSMENTS, tenant=tenant)] = \
                            construct_query_info(query=self.get_query(state_code, state_code, tenant, *tables),
                                                 tenant=tenant, is_public=is_public)
        return execute_mapped_queries_remotely(query_map)

    def build_assessment_map(self, mapped_results):

        def is_valid_tenant_with_results(tenant, result):
            # parcc is formmated in format_summary
            # if result is empty, exclude it
            return tenant in self.cds_tenants_map.keys() and tenant != Constants.CONSORTIUM_NAME \
                and len(result) > 0

        assessments = {self.subject: []}
        table_to_subject_map = reverse_map(SUBJECT_TO_RPT_TABLE)
        for _type, subject, level, tenant in mapped_results.keys():
            key = QueryMapKey(_type, subject, level, tenant)
            if _type == Constants.ASSESSMENTS and is_valid_tenant_with_results(tenant, mapped_results[key]):
                assessments[table_to_subject_map[subject]].append(
                    mapped_results[key][0])
        return assessments

    def has_results(self, assessments):
        return len(assessments[self.subject]) > 0

    def format_results(self, results):
        formatted_results = format_cpop_results(self.result,
                                                has_filters(self.filters),
                                                self.alert_cell_size,
                                                self.min_cell_size,
                                                self.min_cell_size_msg,
                                                results)
        formatted_results[AssessmentType.SUMMATIVE][self.subject].extend(self.get_opt_out_state_data())
        return formatted_results

    def get_opt_out_state_data(self):

        def create_data(state_code):
            return {
                Constants.NAME: get_state_name(state_code),
                SummativeConstants.GUID: state_code,
                CPopConstants.CDS_OPT_OUT: True,
            }

        return [create_data(state_code) for state_code in get_cds_opt_out_states()]

    def get_summary_query(self, asmt_table):
        return self.get_query(get_cds_states_text(),
                              Constants.CONSORTIUM_NAME,
                              Constants.CONSORTIUM_NAME,
                              asmt_table,
                              override_name=True)

    def format_summary(self, results):
        summary = {self.subject: results[QueryMapKey(type=Constants.SUMMARY)]}
        return format_cpop_results(self.result,
                                   has_filters(self.filters),
                                   self.alert_cell_size,
                                   self.min_cell_size,
                                   self.min_cell_size_msg,
                                   summary,
                                   report_level=Constants.CONSORTIUM_NAME)
