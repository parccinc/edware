from sqlalchemy import and_, case, func, select
from sqlalchemy.sql.expression import literal

from edcore.database.routing import ReportingDbConnection
from edcore.security.tenant import get_cds_states_text, get_cds_opt_out_states
from functools import partial
from edcore.database.edcore_connector import EdCoreDBConnection
from edcore.utils.utils import compile_query_to_sql_text, merge_dict
from pyramid.security import authenticated_userid
from pyramid.threadlocal import get_current_request
from reporting.reports.base_aggregator import get_non_parcc_common_where_condition, BaseAggregator
from reporting.reports.base_aggregator import ReportLevel
from reporting.reports.base_report import BaseSummativeReport
from reporting.reports.formatter.common import build_subclaim_perf_level_attr, build_overall_perf_level_attr
from reporting.reports.formatter.school_report_formatter import \
    format_school_level_results
from reporting.reports.helpers.constants import (ALL_PERFORMANCE_LEVELS, LevelValues,
                                                 OVERALL_PERFORMANCE_LEVELS,
                                                 Constants,
                                                 AssessmentType,
                                                 RptTable,
                                                 subclaim_to_table_col)
from reporting.reports.helpers.filters import (apply_filter_to_query,
                                               has_filters)
from reporting.reports.helpers.min_cell_size import get_minimum_cell_size
from reporting.reports.helpers.query_utils import \
    execute_mapped_queries_remotely, construct_query_info
from reporting.reports.helpers.report_table_map import SUBJECT_TO_RPT_TABLE
from edcore.utils.query import QueryMapKey
from reporting.reports.summative.helpers.constants import Constants as SummativeConstants


def get_overall_levels(asmt_table):
    columns = []
    for performance_level in OVERALL_PERFORMANCE_LEVELS:
        label = build_overall_perf_level_attr(performance_level)
        columns.append(func.sum(case([(asmt_table.c.sum_perf_lvl == performance_level, 1)], else_=0)).label(label))
    return columns


def get_subscore_levels(result, asmt_table):
    claim_column = subclaim_to_table_col(result)
    columns = []
    for performance_level in ALL_PERFORMANCE_LEVELS:
        label = build_subclaim_perf_level_attr(claim_column, performance_level)
        columns.append(func.sum(case([(asmt_table.c[claim_column] == performance_level, 1)], else_=0)).label(label))
    return columns


class SchoolLevelOverallReport(BaseSummativeReport):

    COURSES_KEY = "courses"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.result = SummativeConstants.OVERALL if not self.result else self.result
        self.all_levels = [
            ReportLevel(LevelValues.STATE, 'state_code', self.state_code),
            ReportLevel(LevelValues.DISTRICT, 'resp_dist_id', self.district_guid),
            ReportLevel(LevelValues.SCHOOL, 'resp_school_id', self.school_guid),
        ]
        user_state_codes = set(authenticated_userid(get_current_request()).get_state_codes())
        if not user_state_codes.issubset(get_cds_opt_out_states()):
            self.all_levels.append(ReportLevel(LevelValues.PARCC, '', get_cds_states_text()))

        if self.result == SummativeConstants.OVERALL:
            self.perf_level_columns_generator = get_overall_levels
        else:
            self.perf_level_columns_generator = partial(get_subscore_levels, self.result)

    def format_results(self, results):
        min_size_parcc = get_minimum_cell_size(Constants.CONSORTIUM_NAME, self.view)
        return format_school_level_results(self.subject,
                                           self.result,
                                           results,
                                           has_filters(self.filters),
                                           self.alert_cell_size,
                                           self.min_cell_size,
                                           min_size_parcc,
                                           self.min_cell_size_msg)

    def build_assessment_map(self, mapped_results):
        return mapped_results

    def run_query(self):
        summaries = {}
        summary_query_map = {}
        while self.all_levels:
            level = self.all_levels[-1]
            summary_query_map = merge_dict(summary_query_map, self.get_summary_query_map(level))
            self.all_levels.pop()

        summary_results = execute_mapped_queries_remotely(summary_query_map)
        for type, subject, level, tenant in summary_results.keys():
            value = summary_results[QueryMapKey(type, subject, level, tenant)]
            if type == self.COURSES_KEY:
                summaries.setdefault(self.COURSES_KEY, {})
                summaries[self.COURSES_KEY][level] = value
            else:
                summaries.setdefault(subject, {})
                summaries[subject][level] = value

        return summaries

    def _get_summary_query_map(self, conn, level, tenant=None):
        table = SUBJECT_TO_RPT_TABLE.get(self.subject)
        asmt_table = conn.get_table(table)
        if table == RptTable.MATH_SUM:
            return self._get_queries_for_math(asmt_table, level, tenant)
        else:  # ELA
            return self._get_query_for_ela(asmt_table, level, tenant)

    def _get_query_for_ela(self, asmt_table, level, tenant):
        key = QueryMapKey(subject=self.subject, level=level.level, type=Constants.SUMMARY, tenant=tenant)
        ela_query = construct_query_info(
            query=self.get_query_for_grades(level, asmt_table),
            tenant=tenant,
            is_public=bool(tenant),
            state_code=self.state_code
        )
        return {key: ela_query}

    def _get_queries_for_math(self, asmt_table, level, tenant):
        math_grades_key = QueryMapKey(subject=self.subject, level=level.level, type=Constants.SUMMARY, tenant=tenant)
        math_grades_query = construct_query_info(
            query=self.get_query_for_grades(level, asmt_table),
            tenant=tenant,
            is_public=bool(tenant),
            state_code=self.state_code
        )
        math_courses_key = QueryMapKey(level=level.level, type=self.COURSES_KEY, tenant=tenant)
        math_courses_query = construct_query_info(
            query=self.get_query_for_courses(level, asmt_table),
            tenant=tenant,
            is_public=bool(tenant),
            state_code=self.state_code)
        return {
            math_grades_key: math_grades_query,
            math_courses_key: math_courses_query,
        }

    def get_summary_query_map(self, level):
        # queries
        if level.level == LevelValues.PARCC:
            with ReportingDbConnection(tenant=Constants.CONSORTIUM_NAME, is_public=True) as conn:
                return self._get_summary_query_map(conn, level, Constants.CONSORTIUM_NAME)
        else:
            with EdCoreDBConnection(state_code=self.state_code) as conn:
                return self._get_summary_query_map(conn, level)

    def get_query(self):
        # run_query() function has been overwritten, therefore no need to
        # implement get_query() function
        pass

    def get_query_for_grades(self, level, asmt_table):
        # for ELA and Math grades, asmt_grade should not be null per spec; for
        # Math courses, asmt_grade must be empty and therefore fallback to
        # student_grade
        id_column = case(
            [(asmt_table.c.asmt_grade.is_(None), asmt_table.c.student_grade)],
            else_=asmt_table.c.asmt_grade
        ).label(SummativeConstants.GUID)
        name_column = asmt_table.c.asmt_subject.label(Constants.NAME)
        select_columns = [id_column, name_column]
        select_columns += self.get_aggregate_columns(level, asmt_table)
        select_columns += self.perf_level_columns_generator(asmt_table)

        # On the school report and student roster:
        # 1. For PARCC aggregate row, the Growth vs State and Growth vs PARCC fields should both always be blank
        # 2. For State aggregate row, the Growth vs State should always be blank
        # This is based on business requirements. These comparisons don't make logical sense.
        if level.level != LevelValues.PARCC:
            select_columns += [func.avg(asmt_table.c.parcc_growth_percent).label(SummativeConstants.PARCC_GROWTH_PERCENT)]
        if level.level not in (LevelValues.PARCC, LevelValues.STATE):
            select_columns += [func.avg(asmt_table.c.state_growth_percent).label(SummativeConstants.STATE_GROWTH_PERCENT)]

        where_condition = and_(asmt_table.c.year == self.asmt_year)
        if level.level != LevelValues.PARCC:
            where_condition = and_(
                where_condition,
                self.get_level_conditions(level, asmt_table),
                get_non_parcc_common_where_condition(asmt_table, level)
            )

        query = select(select_columns, from_obj=asmt_table).where(where_condition)
        query = apply_filter_to_query(query, asmt_table, self.filters)
        query = query.group_by(id_column, name_column)
        return compile_query_to_sql_text(query)

    def get_query_for_courses(self, level, asmt_table):
        name_column = asmt_table.c.asmt_subject.label(Constants.NAME)
        select_columns = [name_column]
        select_columns += self.get_aggregate_columns(level, asmt_table)
        select_columns += self.perf_level_columns_generator(asmt_table)

        # On the school report and student roster:
        # 1. For PARCC aggregate row, the Growth vs State and Growth vs PARCC fields should both always be blank
        # 2. For State aggregate row, the Growth vs State should always be blank
        # This is based on business requirements. These comparisons don't make logical sense.
        if level.level != LevelValues.PARCC:
            select_columns += [func.avg(asmt_table.c.parcc_growth_percent).label(SummativeConstants.PARCC_GROWTH_PERCENT)]
        if level.level not in (LevelValues.PARCC, LevelValues.STATE):
            select_columns += [func.avg(asmt_table.c.state_growth_percent).label(SummativeConstants.STATE_GROWTH_PERCENT)]

        # The only way to differentiate grades and courses in Math is by asmt_grade;
        # if asmt_grade is empty, then this record is for courses;
        # otherwise, it's for grades.
        where_condition = and_(asmt_table.c.asmt_grade.is_(None))
        where_condition = and_(where_condition, asmt_table.c.year == self.asmt_year)
        if level.level != LevelValues.PARCC:
            where_condition = and_(
                where_condition,
                self.get_level_conditions(level, asmt_table),
                get_non_parcc_common_where_condition(asmt_table, level),
            )

        query = select(select_columns, from_obj=asmt_table).where(where_condition)
        query = apply_filter_to_query(query, asmt_table, self.filters)
        query = query.group_by(name_column)
        return compile_query_to_sql_text(query)

    def get_aggregate_columns(self, level, asmt_table):
        columns = [
            func.count().label(SummativeConstants.STUDENTS),
            func.avg(asmt_table.c.sum_scale_score).label(Constants.SCORE),
            func.avg(asmt_table.c.sum_read_scale_score).label(SummativeConstants.READING),
            func.avg(asmt_table.c.sum_write_scale_score).label(SummativeConstants.WRITING),
        ]
        if level.level != LevelValues.PARCC:
            if self.staff_id is not None:
                student_count = func.sum(
                    case([(asmt_table.c.staff_id == self.staff_id, 1)], else_=0)
                ).label(SummativeConstants.STUDENT_COUNT)
            else:
                student_count = literal(1).label(SummativeConstants.STUDENT_COUNT)
            columns.append(student_count)
        return columns

    def get_level_conditions(self, level, asmt_table):
        where_condition = and_(asmt_table.c[level.column] == level.value)
        if level.level == LevelValues.SCHOOL or level.level == LevelValues.DISTRICT:
            where_condition = and_(where_condition,
                                   asmt_table.c.state_code == self.state_code,
                                   )
        if level.level == LevelValues.SCHOOL:
            where_condition = and_(where_condition,
                                   asmt_table.c.resp_dist_id == self.district_guid,
                                   )
        return where_condition

    def get_aggregator(self):
        # We need this just to conditionally enable the summary checkboxes
        return SchoolReportAggregator


class SchoolReportAggregator(BaseAggregator):

    def format_summary_results(self, mapped_results):
        subject_summaries = {}
        user_state_codes = set(authenticated_userid(get_current_request()).get_state_codes())
        for subject, subject_summary in mapped_results.items():
            subject_summaries.setdefault(subject, [])
            if LevelValues.SCHOOL in subject_summary and subject_summary[LevelValues.SCHOOL]:
                if LevelValues.PARCC in subject_summary and subject_summary[LevelValues.PARCC]:
                    subject_summaries[subject].append({SummativeConstants.SUMMARY_ROW_TYPE: LevelValues.PARCC})
                elif user_state_codes.issubset(get_cds_opt_out_states()):
                    subject_summaries[subject].append(
                        {
                            SummativeConstants.SUMMARY_ROW_TYPE: LevelValues.PARCC,
                            SummativeConstants.ROW_DISABLED: True,
                        }
                    )
                subject_summaries[subject].append({SummativeConstants.SUMMARY_ROW_TYPE: LevelValues.STATE})
                subject_summaries[subject].append({SummativeConstants.SUMMARY_ROW_TYPE: LevelValues.DISTRICT})
        return {AssessmentType.SUMMATIVE: subject_summaries}
