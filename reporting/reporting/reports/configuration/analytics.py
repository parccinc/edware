'''
Created on Jul 6, 2015

@author: dip
'''
from functools import wraps
from reporting.reports.helpers.constants import Constants
from reporting.reports.helpers.analytics import get_analytics_info


def add_analytics_info(orig_func):
    '''
    Decorator to add analytics info
    We want to use a decorator so that it's not part of the result set that is
    being cached in get_results()
    '''
    @wraps(orig_func)
    def wrap(*args, **kwds):
        results = orig_func(*args, **kwds)
        results[Constants.ANALYTICS] = get_analytics_info()
        return results
    return wrap
