from reporting.reports.formatter.common import strip_table_prefixes


class Constants:
    '''
    This class contains:
    General constants being used in multiple places
    And other constants which do not warrant having their own class
    '''
    SUGGESTED_GRADE_LEVEL = 'suggested_grade_level'  # used in locator
    PROGRESSION = 'progression'  # used in progression

    CLUSTER_GRADE_LEVEL = 'cluster_grade_level'
    ASMT_SCORE = 'asmt_scale_score'


class DiagnosticsTable():
    '''
    These should be the latter part of the corresponding table names.
    For example read_flu for Reading fluency (from the table name rpt_ela_read_flu)
    '''
    ELA_READING_FLUENCY = 'rpt_ela_read_flu'
    ELA_VOCABULARY = 'rpt_ela_vocab'
    ELA_WRITING = 'rpt_ela_writing'
    ELA_DECODE = 'rpt_ela_decod'
    ELA_READING_COMPREHENSION = 'rpt_ela_comp'
    ELA_READER_MOTIVATION = 'rpt_reader_motiv'
    MATH_LOCATOR = 'rpt_math_locator'
    MATH_GRADE_LEVEL = 'rpt_math_grade_level'
    MATH_PROGRESSION = 'rpt_math_progress'
    MATH_FLUENCY = 'rpt_math_flu'
    MATH_CLUSTER = 'rpt_math_cluster'


class DiagnosticReportType(object):
    OVERVIEW = 'overview'
    READING_COMPREHENSION = strip_table_prefixes(DiagnosticsTable.ELA_READING_COMPREHENSION)
    VOCABULARY = strip_table_prefixes(DiagnosticsTable.ELA_VOCABULARY)
    READING_FLUENCY = strip_table_prefixes(DiagnosticsTable.ELA_READING_FLUENCY)
    WRITING = strip_table_prefixes(DiagnosticsTable.ELA_WRITING)
    DECODING = strip_table_prefixes(DiagnosticsTable.ELA_DECODE)
    READER_MOTIVATION_SURVEY = strip_table_prefixes(DiagnosticsTable.ELA_READER_MOTIVATION)
    LOCATOR = strip_table_prefixes(DiagnosticsTable.MATH_LOCATOR)
    GRADE_LEVEL = strip_table_prefixes(DiagnosticsTable.MATH_GRADE_LEVEL)
    PROGRESSION = strip_table_prefixes(DiagnosticsTable.MATH_PROGRESSION)
    CLUSTER = strip_table_prefixes(DiagnosticsTable.MATH_CLUSTER)
    FLUENCY = strip_table_prefixes(DiagnosticsTable.MATH_FLUENCY)


class ReadingComprehensionConstants:
    IRL = 'irl'
    PASSAGE_INFO = 'passage_info'
    PASSAGE_TYPE = 'passage_type'
    PASSAGE_RMM = 'passage_rmm'
    PASSAGE1_TYPE = 'passage1_type'
    PASSAGE2_TYPE = 'passage2_type'
    PASSAGE3_TYPE = 'passage3_type'
    PASSAGE1_RMM = 'passage1_rmm'
    PASSAGE2_RMM = 'passage2_rmm'
    PASSAGE3_RMM = 'passage3_rmm'


class DecodingConstants:
    CVC = 'p1_cvc'
    BLEND = 'p2_blend'
    COMPLEX_VOWELS = 'p3_cvow'
    COMPLEX_CONSONANT = 'p4_ccons'
    MULTI_SY1 = 'p5_msyl1'
    MULTI_SY2 = 'p6_msyl2'


class ReadingFluencyConstants:
    WPM = 'wpm'
    WCPM = 'wcpm'
    ACCURACY = 'accuracy'
    EXPRESS = 'express'
    PASSAGE1_TYPE = 'passage1_type'
    PASSAGE2_TYPE = 'passage2_type'
    PASSAGE3_TYPE = 'passage3_type'
    PASSAGE_INFO = 'passage_info'
    PASSAGE_TYPE = 'passage_type'


class ReaderMotivationSurveyConstants(object):
    RESPONSE_PREFIX = 'response'
    NUM_RESPONSE_FIELDS = 19
    RESPONSE_SEPARATOR = "."
    RESPONSES = 'responses'


class ClusterConstants:
    PROB_CLUSTER6 = 'prob_cluster6'
    CODE_CLUSTER6 = 'code_cluster6'
    PROB_CLUSTER5 = 'prob_cluster5'
    CODE_CLUSTER5 = 'code_cluster5'
    PROB_CLUSTER4 = 'prob_cluster4'
    CODE_CLUSTER4 = 'code_cluster4'
    PROB_CLUSTER3 = 'prob_cluster3'
    CODE_CLUSTER3 = 'code_cluster3'
    PROB_CLUSTER2 = 'prob_cluster2'
    CODE_CLUSTER2 = 'code_cluster2'
    PROB_CLUSTER1 = 'prob_cluster1'
    CODE_CLUSTER1 = 'code_cluster1'
    LEVEL_LUSTER = 'level_cluster'
    PROB_CLUSTER = 'prob_cluster'
    CODE_CLUSTER = 'code_cluster'
    CLUSTER = 'cluster'
    CLUSTERS = 'clusters'


class WritingConstants:
    SCORE_NAR = 'score_nar'
    SCORE_LIT = 'score_lit'
    SCORE_RES = 'score_res'


class FluencyConstants:
    SKILL = 'skill'
    ITEMS_CORRECT = 'items_correct'
    TOTAL_TIME_PER_SKILL = 'total_time_per_skill'
    TOTAL_NUM_SKILL_ITEMS = 'total_num_skill_items'
    SKILL_PERCENT_CORRECT = 'percent_correct'
    SKILL1_NAME = 'skill1_name'
    SKILL1_TOTAL_ITEMS = 'skill1_total_items'
    SKILL1_NUMBER_CORRECT = 'skill1_number_correct'
    SKILL1_TIME = 'skill1_time'
    SKILL2_NAME = 'skill2_name'
    SKILL2_TOTAL_ITEMS = 'skill2_total_items'
    SKILL2_NUMBER_CORRECT = 'skill2_number_correct'
    SKILL2_TIME = 'skill2_time'
    SKILL3_NAME = 'skill3_name'
    SKILL3_TOTAL_ITEMS = 'skill3_total_items'
    SKILL3_NUMBER_CORRECT = 'skill3_number_correct'
    SKILL3_TIME = 'skill3_time'
    SKILL4_NAME = 'skill4_name'
    SKILL4_TOTAL_ITEMS = 'skill4_total_items'
    SKILL4_NUMBER_CORRECT = 'skill4_number_correct'
    SKILL4_TIME = 'skill4_time'
    SKILL5_NAME = 'skill5_name'
    SKILL5_TOTAL_ITEMS = 'skill5_total_items'
    SKILL5_NUMBER_CORRECT = 'skill5_number_correct'
    SKILL5_TIME = 'skill5_time'
    SKILL6_NAME = 'skill6_name'
    SKILL6_TOTAL_ITEMS = 'skill6_total_items'
    SKILL6_NUMBER_CORRECT = 'skill6_number_correct'
    SKILL6_TIME = 'skill6_time'
