from edcore.utils.utils import compile_query_to_sql_text
from reporting.reports.base_report import BaseDiagnosticReport
from reporting.reports.diagnostic.formatter.progression_formatter import format_results
from reporting.reports.helpers.filters import apply_filter_to_query
from reporting.reports.helpers.constants import Constants, AssessmentType
from reporting.reports.diagnostic.helpers.constants import DiagnosticsTable
from reporting.security.context import select_with_context


class ProgressionReport(BaseDiagnosticReport):

    @staticmethod
    def get_formatted_results(result):
        return format_results(result)

    @staticmethod
    def get_select_columns(asmt_table):
        select_columns = [
            asmt_table.c.progression,
            asmt_table.c.code_cluster1,
            asmt_table.c.prob_cluster1,
            asmt_table.c.code_cluster2,
            asmt_table.c.prob_cluster2,
            asmt_table.c.code_cluster3,
            asmt_table.c.prob_cluster3,
            asmt_table.c.code_cluster4,
            asmt_table.c.prob_cluster4,
            asmt_table.c.code_cluster5,
            asmt_table.c.prob_cluster5,
            asmt_table.c.code_cluster6,
            asmt_table.c.prob_cluster6,
        ]
        select_columns += BaseDiagnosticReport.get_common_select_columns(asmt_table)
        return select_columns

    def get_query(self, asmt_table):
        select_columns = self.get_select_columns(asmt_table)
        where_condition = self.get_common_where_condition(asmt_table)
        query = select_with_context(select_columns,
                                    from_obj=asmt_table,
                                    permission=self.permission,
                                    state_code=self.state_code).where(where_condition)
        query = apply_filter_to_query(query, asmt_table, self.filters)
        query = self.apply_order_by_name(asmt_table, query)
        return compile_query_to_sql_text(query)

    def format_results(self, results):
        key = self.get_query_key()
        formatted_results = self.get_formatted_results(results[key])
        return {
            AssessmentType.DIAGNOSTIC: {Constants.SUBJECT1: formatted_results}
        }

    def get_required_table_names(self):
        return [(DiagnosticsTable.MATH_PROGRESSION,)]
