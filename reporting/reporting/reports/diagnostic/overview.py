from edcore.utils.utils import compile_query_to_sql_text
from reporting.reports.base_report import BaseDiagnosticReport
from reporting.reports.diagnostic.formatter.overview_formatter import format_results
from reporting.reports.helpers.filters import apply_filter_to_query
from reporting.reports.helpers.constants import AssessmentType, Constants
from reporting.reports.diagnostic.helpers.constants import DiagnosticsTable
from reporting.security.context import select_with_context


class OverviewReport(BaseDiagnosticReport):

    def get_query(self, asmt_table):
        select_columns = self.get_common_select_columns(asmt_table)
        where_condition = self.get_common_where_condition(asmt_table)
        query = select_with_context(select_columns,
                                    from_obj=asmt_table,
                                    permission=self.permission,
                                    state_code=self.state_code).where(where_condition)
        query = apply_filter_to_query(query, asmt_table, self.filters)
        query = self.apply_order_by_name(asmt_table, query)
        return compile_query_to_sql_text(query)

    def format_results(self, results):
        formatted_results = format_results(results)
        return {
            AssessmentType.DIAGNOSTIC: {Constants.SUBJECT2: formatted_results}
        }

    def get_required_table_names(self):
        return [
            (DiagnosticsTable.ELA_READING_COMPREHENSION,),
            (DiagnosticsTable.ELA_VOCABULARY,),
            (DiagnosticsTable.ELA_READING_FLUENCY,),
            (DiagnosticsTable.ELA_WRITING,),
            (DiagnosticsTable.ELA_DECODE,),
        ]
