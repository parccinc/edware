from reporting.reports.formatter.common import get_common_diagnostic_format_data
from reporting.reports.diagnostic.helpers.constants import ReaderMotivationSurveyConstants


def format_results(records):
    return [create_student_result(record) for record in records]


def create_student_result(record):
    student_result = get_common_diagnostic_format_data(record)
    responses_array = []
    for i in range(0, ReaderMotivationSurveyConstants.NUM_RESPONSE_FIELDS):
        reader_motivation_key = ReaderMotivationSurveyConstants.RESPONSE_PREFIX + str(i + 1)
        # sample value from DB: A.B.C.D
        if reader_motivation_key in record and record[reader_motivation_key] is not None:
            responses_array.append(record[reader_motivation_key].split(ReaderMotivationSurveyConstants.RESPONSE_SEPARATOR))
        else:
            responses_array.append([])
    student_result[ReaderMotivationSurveyConstants.RESPONSES] = responses_array
    return student_result
