from reporting.reports.formatter.common import get_common_diagnostic_format_data
from reporting.reports.diagnostic.helpers.constants import FluencyConstants as FluencyConstants


def format_results(records):
    formatted_results = []
    for record in records:
        # Unfortunately, skills are stored as columns in the DB, but in the grid, they each get a new row.
        # A student can have between 1 and 6 skills; each one of these skills gets a new grid row with the
        # same student info, but different skill info.
        for i in range(1, 7):
            total_num_skill_items = "skill{0}_total_items".format(i)
            skill_name = "skill{0}_name".format(i)
            skill_number_correct = "skill{0}_number_correct".format(i)
            skill_time = "skill{0}_time".format(i)
            if not record[total_num_skill_items]:
                break
            formatted_results.append(create_student_result(record,
                                                           record[skill_name],
                                                           record[skill_number_correct],
                                                           record[skill_time],
                                                           record[total_num_skill_items]))
    return formatted_results


def create_student_result(record, skill_name, skill_number_correct, skill_time, total_num_skill_items):
    student_result = get_common_diagnostic_format_data(record)
    student_result[FluencyConstants.SKILL] = skill_name
    student_result[FluencyConstants.ITEMS_CORRECT] = str(skill_number_correct)
    student_result[FluencyConstants.TOTAL_TIME_PER_SKILL] = skill_time
    student_result[FluencyConstants.TOTAL_NUM_SKILL_ITEMS] = str(total_num_skill_items)
    percent_correct = 100 * float(skill_number_correct) / float(total_num_skill_items)
    student_result[FluencyConstants.SKILL_PERCENT_CORRECT] = str(round(percent_correct, 2))
    return student_result
