from reporting.reports.formatter.common import get_common_diagnostic_format_data
from reporting.reports.diagnostic.helpers.constants import ReadingFluencyConstants


def format_results(records):
    return [create_student_result(record) for record in records]


def append_passage_info(record, student_result, type_key):
    if record[type_key] is not None:
        student_result[ReadingFluencyConstants.PASSAGE_INFO].append(
            {
                ReadingFluencyConstants.PASSAGE_TYPE: record[type_key],
            }
        )


def create_student_result(record):
    student_result = get_common_diagnostic_format_data(record)
    student_result[ReadingFluencyConstants.WPM] = int(record[ReadingFluencyConstants.WPM])
    student_result[ReadingFluencyConstants.WCPM] = int(record[ReadingFluencyConstants.WCPM])
    student_result[ReadingFluencyConstants.ACCURACY] = int(record[ReadingFluencyConstants.ACCURACY])
    student_result[ReadingFluencyConstants.EXPRESS] = int(record[ReadingFluencyConstants.EXPRESS])
    student_result[ReadingFluencyConstants.PASSAGE_INFO] = []
    append_passage_info(record, student_result, ReadingFluencyConstants.PASSAGE1_TYPE)
    append_passage_info(record, student_result, ReadingFluencyConstants.PASSAGE2_TYPE)
    append_passage_info(record, student_result, ReadingFluencyConstants.PASSAGE3_TYPE)
    return student_result
