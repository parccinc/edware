from reporting.reports.formatter.common import get_common_diagnostic_format_data
from reporting.reports.diagnostic.formatter.cluster_formatter import format_cluster_fields_for_record
from reporting.reports.diagnostic.helpers.constants import Constants as DiagnosticConstants


def format_results(records):
    return [create_student_result(record) for record in records]


def create_student_result(record):
    student_result = get_common_diagnostic_format_data(record)
    student_result[DiagnosticConstants.PROGRESSION] = record[DiagnosticConstants.PROGRESSION]
    student_result = format_cluster_fields_for_record(record, student_result)
    return student_result
