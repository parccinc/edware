from reporting.reports.formatter.common import get_common_diagnostic_format_data
from reporting.reports.diagnostic.helpers.constants import DecodingConstants


def format_results(records):
    return [create_student_result(record) for record in records]


def create_student_result(record):
    student_result = get_common_diagnostic_format_data(record)
    student_result[DecodingConstants.CVC] = float(record[DecodingConstants.CVC])
    student_result[DecodingConstants.BLEND] = float(record[DecodingConstants.BLEND])
    student_result[DecodingConstants.COMPLEX_VOWELS] = float(record[DecodingConstants.COMPLEX_VOWELS])
    student_result[DecodingConstants.COMPLEX_CONSONANT] = float(record[DecodingConstants.COMPLEX_CONSONANT])
    student_result[DecodingConstants.MULTI_SY1] = float(record[DecodingConstants.MULTI_SY1])
    student_result[DecodingConstants.MULTI_SY2] = float(record[DecodingConstants.MULTI_SY2])
    return student_result
