from reporting.reports.helpers.constants import Constants
from reporting.reports.diagnostic.helpers.constants import DiagnosticsTable
from reporting.reports.formatter.common import get_common_diagnostic_format_data


ASSESSMENTS_MAPPING = {
    DiagnosticsTable.ELA_READING_COMPREHENSION: 'Reading Comprehension',
    DiagnosticsTable.ELA_VOCABULARY: 'Vocabulary',
    DiagnosticsTable.ELA_READING_FLUENCY: 'Reading Fluency',
    DiagnosticsTable.ELA_WRITING: 'Writing',
    DiagnosticsTable.ELA_DECODE: 'Decoding',
}


def format_results(results):
    formatted = []
    for key, records in results.items():
        view = ASSESSMENTS_MAPPING[key.subject]
        for record in records:
            formatted.append(create_student_result(record, view))
    return sorted(formatted, key=lambda x: (x[Constants.STUDENT_DISPLAY_NAME],
                                            x[Constants.ASMT_DATE],
                                            x[Constants.VIEW]))


def create_student_result(record, view):
    student_result = get_common_diagnostic_format_data(record)
    student_result[Constants.VIEW] = view
    return student_result
