from reporting.reports.diagnostic.helpers.constants import WritingConstants
from reporting.reports.formatter.common import get_common_diagnostic_format_data


def format_results(records):
    return [create_student_result(record) for record in records]


def create_student_result(record):
    student_result = get_common_diagnostic_format_data(record)
    student_result[WritingConstants.SCORE_RES] = int(record[WritingConstants.SCORE_RES])
    student_result[WritingConstants.SCORE_LIT] = int(record[WritingConstants.SCORE_LIT])
    student_result[WritingConstants.SCORE_NAR] = int(record[WritingConstants.SCORE_NAR])
    return student_result
