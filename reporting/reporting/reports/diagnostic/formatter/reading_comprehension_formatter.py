from reporting.reports.formatter.common import get_common_diagnostic_format_data
from reporting.reports.diagnostic.helpers.constants import Constants as DiagnosticConstants, \
    ReadingComprehensionConstants
from reporting.reports.helpers.constants import Constants


def format_results(records):
    return [create_student_result(record) for record in records]


def append_passage_info(record, student_result, type_key, rmm_key):
    if record[type_key] is not None:
        student_result[ReadingComprehensionConstants.PASSAGE_INFO].append(
            {
                ReadingComprehensionConstants.PASSAGE_TYPE: record[type_key],
                ReadingComprehensionConstants.PASSAGE_RMM: round(float(record[rmm_key]), 1)
            }
        )


def create_student_result(record):
    student_result = get_common_diagnostic_format_data(record)
    student_result[Constants.SCORE] = int(record[DiagnosticConstants.ASMT_SCORE])
    student_result[ReadingComprehensionConstants.IRL] = round(float(record[ReadingComprehensionConstants.IRL]), 1)
    student_result[ReadingComprehensionConstants.PASSAGE_INFO] = []
    append_passage_info(record, student_result, ReadingComprehensionConstants.PASSAGE1_TYPE, ReadingComprehensionConstants.PASSAGE1_RMM)
    append_passage_info(record, student_result, ReadingComprehensionConstants.PASSAGE2_TYPE, ReadingComprehensionConstants.PASSAGE2_RMM)
    append_passage_info(record, student_result, ReadingComprehensionConstants.PASSAGE3_TYPE, ReadingComprehensionConstants.PASSAGE3_RMM)
    return student_result
