from reporting.reports.formatter.common import get_common_diagnostic_format_data
from reporting.reports.diagnostic.helpers.constants import ClusterConstants as ClusterConstants


def format_results(records):
    return [create_student_result(record) for record in records]


def create_student_result(record):
    student_result = get_common_diagnostic_format_data(record)
    student_result = format_cluster_field_for_record(record, student_result)
    return student_result


def format_cluster_field_for_record(record, student_result):
    # can have 1 cluster for cluster report (this is based on cluster_formatter, for reports with multiple clusters)
    cluster = {}
    cluster[ClusterConstants.CODE_CLUSTER] = record[ClusterConstants.CODE_CLUSTER]
    cluster[ClusterConstants.PROB_CLUSTER] = float(record[ClusterConstants.PROB_CLUSTER])
    clusters = [cluster, ]
    student_result[ClusterConstants.CLUSTERS] = clusters
    return student_result


def format_cluster_fields_for_record(record, student_result):
    # can have between 1 and 6 clusters for other reports that have clusters (progression, grade level, ...)
    max_clusters = 6
    clusters = []
    for i in range(1, max_clusters + 1):
        code_cluster_key = "{cluster}{number}".format(cluster=ClusterConstants.CODE_CLUSTER, number=i)
        prob_cluster_key = "{cluster}{number}".format(cluster=ClusterConstants.PROB_CLUSTER, number=i)
        if not record[code_cluster_key]:
            break
        cluster = {}
        cluster[ClusterConstants.CODE_CLUSTER] = record[code_cluster_key]
        cluster[ClusterConstants.PROB_CLUSTER] = float(record[prob_cluster_key])
        clusters.append(cluster)
    student_result[ClusterConstants.CLUSTERS] = clusters
    return student_result
