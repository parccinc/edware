from reporting.reports.helpers.constants import Constants
from reporting.reports.formatter.common import get_common_diagnostic_format_data
from reporting.reports.diagnostic.helpers.constants import Constants as DiagnosticConstants


def format_results(records):
    return [create_student_result(record) for record in records]


def create_student_result(record):
    student_result = get_common_diagnostic_format_data(record)
    student_result[Constants.SCORE] = int(record[DiagnosticConstants.ASMT_SCORE])
    student_result[DiagnosticConstants.SUGGESTED_GRADE_LEVEL] = int(record[DiagnosticConstants.SUGGESTED_GRADE_LEVEL])
    return student_result
