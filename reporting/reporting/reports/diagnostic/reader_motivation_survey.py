from edcore.utils.utils import compile_query_to_sql_text
from reporting.reports.base_report import BaseDiagnosticReport
from reporting.reports.diagnostic.formatter.reader_motivation_survey_formatter import format_results
from reporting.reports.helpers.filters import apply_filter_to_query
from reporting.reports.helpers.constants import AssessmentType, Constants
from reporting.reports.diagnostic.helpers.constants import DiagnosticsTable
from reporting.security.context import select_with_context


class ReaderMotivationSurveyReport(BaseDiagnosticReport):

    @staticmethod
    def get_select_columns(asmt_table):
        select_columns = [
            asmt_table.c.response1,
            asmt_table.c.response2,
            asmt_table.c.response3,
            asmt_table.c.response4,
            asmt_table.c.response5,
            asmt_table.c.response6,
            asmt_table.c.response7,
            asmt_table.c.response8,
            asmt_table.c.response9,
            asmt_table.c.response10,
            asmt_table.c.response11,
            asmt_table.c.response12,
            asmt_table.c.response13,
            asmt_table.c.response14,
            asmt_table.c.response15,
            asmt_table.c.response16,
            asmt_table.c.response17,
            asmt_table.c.response18,
            asmt_table.c.response19,
        ]
        select_columns += BaseDiagnosticReport.get_common_select_columns(asmt_table)
        return select_columns

    @staticmethod
    def get_formatted_results(result):
        return format_results(result)

    def get_query(self, asmt_table):
        select_columns = self.get_select_columns(asmt_table)
        where_condition = self.get_common_where_condition(asmt_table)
        query = select_with_context(select_columns,
                                    from_obj=asmt_table,
                                    permission=self.permission,
                                    state_code=self.state_code).where(where_condition)
        query = apply_filter_to_query(query, asmt_table, self.filters)
        query = self.apply_order_by_name(asmt_table, query)
        return compile_query_to_sql_text(query)

    def format_results(self, results):
        key = self.get_query_key()
        return {AssessmentType.DIAGNOSTIC: {Constants.SUBJECT2: self.get_formatted_results(results[key])}}

    def get_required_table_names(self):
        return [(DiagnosticsTable.ELA_READER_MOTIVATION,)]
