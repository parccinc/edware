from sqlalchemy import and_

from edcore.utils.utils import compile_query_to_sql_text
from reporting.reports.base_report import BaseDiagnosticReport
from reporting.reports.diagnostic.cluster import ClusterReport
from reporting.reports.diagnostic.decoding import DecodingReport
from reporting.reports.diagnostic.grade_level import GradeLevelReport
from reporting.reports.diagnostic.locator import LocatorReport
from reporting.reports.diagnostic.math_fluency import MathFluencyReport
from reporting.reports.diagnostic.progression import ProgressionReport
from reporting.reports.diagnostic.reading_comprehension import ReadingComprehensionReport
from reporting.reports.diagnostic.reading_fluency import ReadingFluencyReport
from reporting.reports.diagnostic.vocabulary import VocabularyReport
from reporting.reports.diagnostic.writing import WritingReport
from reporting.reports.formatter.common import strip_table_prefixes
from reporting.reports.helpers.breadcrumbs import get_breadcrumbs_context
from reporting.reports.helpers.filters import apply_filter_to_query
from reporting.reports.helpers.constants import Constants, AssessmentType
from reporting.reports.diagnostic.helpers.constants import DiagnosticsTable
from reporting.security.context import select_with_context


class StudentReport(BaseDiagnosticReport):
    '''
    This is a special report which returns several types of diagnostic data for a student
    For ELA it returns writing, vocabulary, decoding, reading fluency and reading comprehension data

    Usage notes: To include another diagnostic report in this guy, just add the required class to DIAGNOSTIC_CLASS_MAPPING
    defined below. Also, make sure all classes included in DIAGNOSTIC_CLASS_MAPPING define the two static methods
    get_select_columns and get_formatted_results
    '''

    DIAGNOSTIC_CLASS_MAPPING = {
        Constants.SUBJECT1: {
            DiagnosticsTable.MATH_CLUSTER: ClusterReport,
            DiagnosticsTable.MATH_GRADE_LEVEL: GradeLevelReport,
            DiagnosticsTable.MATH_LOCATOR: LocatorReport,
            DiagnosticsTable.MATH_PROGRESSION: ProgressionReport,
            DiagnosticsTable.MATH_FLUENCY: MathFluencyReport,
        },
        Constants.SUBJECT2: {
            DiagnosticsTable.ELA_WRITING: WritingReport,
            DiagnosticsTable.ELA_VOCABULARY: VocabularyReport,
            DiagnosticsTable.ELA_DECODE: DecodingReport,
            DiagnosticsTable.ELA_READING_FLUENCY: ReadingFluencyReport,
            DiagnosticsTable.ELA_READING_COMPREHENSION: ReadingComprehensionReport,
        },
    }

    def __init__(self, subject=None, **kwargs):
        self.class_mapping = self.DIAGNOSTIC_CLASS_MAPPING[subject]
        super().__init__(subject=subject, **kwargs)

    def get_query(self, asmt_table):
        select_columns = self.class_mapping[asmt_table.name].get_select_columns(asmt_table)
        where_condition = and_(
            asmt_table.c.student_parcc_id == self.student_guid,
            self.get_common_where_condition(asmt_table)
        )
        query = select_with_context(select_columns,
                                    from_obj=asmt_table,
                                    permission=self.permission,
                                    state_code=self.state_code).where(where_condition)
        query = self.apply_order_by_name(asmt_table, query)
        query = apply_filter_to_query(query, asmt_table, self.filters)
        return compile_query_to_sql_text(query)

    def format_results(self, results):
        formatted = {}
        for key, records in results.items():
            subject = key.subject
            view = strip_table_prefixes(subject)
            formatted[view] = self.class_mapping[subject].get_formatted_results(records)
        return {AssessmentType.DIAGNOSTIC: {self.subject: formatted}}

    def get_required_table_names(self):
        return [(x,) for x in self.class_mapping.keys()]

    def get_breadcrumbs_context(self):
        # search all the tables for this student
        breadcrumbs = {}
        for table_name in self.class_mapping.keys():
            breadcrumbs = get_breadcrumbs_context(table_name,
                                                  state_code=self.state_code,
                                                  district_guid=self.district_guid,
                                                  school_guid=self.school_guid,
                                                  asmt_grade=self.grade_course,
                                                  student_guid=self.student_guid,
                                                  is_course=self.is_course)
            if len(breadcrumbs) > 1:
                break
        return breadcrumbs
