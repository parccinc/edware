from reporting.reports.formatter.common import (GroupByAssessmentTypes,
                                                get_overall_perf_level_distribution,
                                                get_subscore_perf_level_distribution)
from reporting.reports.helpers.constants import (subclaim_to_table_col, Constants)
from reporting.reports.summative.helpers.constants import CPopConstants
from reporting.reports.summative.helpers.constants import Constants as SummativeConstants


def create_row_from_record(result, has_filters, alert_size, min_size, min_cell_msg, record):
    if result == SummativeConstants.OVERALL:
        greater_than_level_3, perf_levels = get_overall_perf_level_distribution(record)
    else:
        claim_column = subclaim_to_table_col(result)
        greater_than_level_3, perf_levels = get_subscore_perf_level_distribution(claim_column, record)

    # Override min cell size value if the value exists in record level
    min_size = record.get(Constants.MIN_CELL_SIZE, min_size)

    student_count = record[SummativeConstants.STUDENTS]
    rtn = {Constants.NAME: record[Constants.NAME],
           SummativeConstants.GUID: record[SummativeConstants.GUID],
           SummativeConstants.HAS_FILTERS: has_filters,
           SummativeConstants.ALERT_THRESHOLD: alert_size,
           SummativeConstants.IS_LESS_THAN_THRESHOLD: min_size <= student_count < alert_size}

    if not has_filters:
        rtn[SummativeConstants.STUDENTS] = student_count

    if student_count < min_size:
        rtn[SummativeConstants.IS_LESS_THAN_MIN_CELL_SIZE] = True
        rtn[SummativeConstants.MIN_CELL_SIZE_MSG] = min_cell_msg
        rtn[SummativeConstants.PERFORMANCE_SORT] = -1
    else:
        growth = {}
        # We don't query for parcc or state median at aggregations level
        if record.get(CPopConstants.PARCC) is not None:
            growth[CPopConstants.PARCC] = round(record[CPopConstants.PARCC])
        if record.get(CPopConstants.STATE) is not None:
            growth[CPopConstants.STATE] = round(record[CPopConstants.STATE])
        rtn.update({
            Constants.SCORE: round(record[Constants.SCORE]),
            CPopConstants.GROWTH: growth,
            SummativeConstants.PERFORMANCELEVELS: perf_levels,
            Constants.LEVEL: greater_than_level_3,
            SummativeConstants.AVG: {
                SummativeConstants.READING: round(record[SummativeConstants.READING] or 0),
                SummativeConstants.WRITING: round(record[SummativeConstants.WRITING] or 0),
            },
            SummativeConstants.PERFORMANCE_SORT: greater_than_level_3,
        })
    return rtn


def format_cpop_results(result, has_filters, alert_size, min_size, min_cell_msg, records, report_level=None, disabled=False):
    def create_row(record):
        if disabled:
            row = {SummativeConstants.ROW_DISABLED: disabled}
        else:
            row = create_row_from_record(result, has_filters, alert_size, min_size, min_cell_msg, record)
        if report_level:
            row[SummativeConstants.SUMMARY_ROW_TYPE] = report_level
        return row

    def create_rows(records):
        # query returns a list with only 1 element, but we still need to loop
        # through in order to compliant with the template in `GroupByAssessmentTypes`
        return [create_row(record) for record in records]

    return GroupByAssessmentTypes(records).create_by(create_rows)
