from collections import defaultdict, OrderedDict
from functools import partial

from reporting.reports.formatter.common import GroupByAssessmentTypes
from reporting.reports.summative.helpers.constants import Constants as SummativeConstants
from reporting.reports.formatter.common import (get_overall_perf_level_distribution,
                                                get_subscore_perf_level_distribution)
from reporting.reports.helpers.constants import (Constants, LevelValues,
                                                 subclaim_to_table_col)


# to determine the order of nested rows in school report
REPORT_LEVEL_WEIGHTS = OrderedDict(
    [
        (LevelValues.SCHOOL, 1),
        (LevelValues.DISTRICT, 2),
        (LevelValues.STATE, 3),
        (LevelValues.PARCC, 4),
    ]
)


class GradeOrCourseFormatter:

    def __init__(self, subject, result, courses, has_filters, alert_size, min_size, min_size_parcc, min_cell_size_msg):
        self.subject = subject
        self.result = result
        self.has_filters = has_filters
        self.alert_size = alert_size
        self.min_size = min_size
        self.min_size_parcc = min_size_parcc
        self.min_cell_msg = min_cell_size_msg
        if result == SummativeConstants.OVERALL:
            self.perf_generator = get_overall_perf_level_distribution
        else:
            claim_column = subclaim_to_table_col(self.result)
            self.perf_generator = partial(get_subscore_perf_level_distribution, claim_column)
        self.courses = self.process_course_summary(courses)

    def process_course_summary(self, records):
        if not records:
            return {}
        courses_summary = defaultdict(dict)
        for level in REPORT_LEVEL_WEIGHTS.keys():
            if level in records:
                course = records[level]
                for record in course:
                    course_name, formatted, student_count = self._create_course(level, record)
                    if level == LevelValues.SCHOOL:
                        formatted.setdefault(SummativeConstants.STUDENT_COUNT, 0)
                        formatted[SummativeConstants.STUDENT_COUNT] += student_count
                    else:
                        # don't need this for parcc, state or district level if the course doesn't exist for the school level
                        if course_name not in courses_summary:
                            continue
                    formatted[Constants.NAME] = course_name
                    formatted[SummativeConstants.GUID] = course_name
                    courses_summary[course_name].update(formatted)
        return courses_summary

    def __call__(self, records):
        if not records:
            return []
        grades, courses = self.classify_grades_and_courses(records)
        courses = self.merge_with_aggregation(courses)
        # front-end requires results in order
        return sorted(grades.values(), key=lambda x: x[SummativeConstants.GUID]) + \
            sorted(courses.values(), key=lambda x: x[SummativeConstants.NAME])

    def classify_grades_and_courses(self, records):
        grades = defaultdict(dict)
        courses = defaultdict(list)
        for level in REPORT_LEVEL_WEIGHTS.keys():
            if level in records:
                grade_data = records[level]
                for record in grade_data:
                    if self._display_course(record):
                        guid, course, _ = self._create_course(level, record)
                        # collect grades for course, this is required for grade 08 above of Math
                        course[level]['is_nested'] = True
                        courses[guid].append(course)
                    else:
                        guid, grade, student_count = self._create_grade(level, record)
                        if level == LevelValues.SCHOOL:
                            grades[guid].setdefault(SummativeConstants.STUDENT_COUNT, 0)
                            grades[guid][SummativeConstants.STUDENT_COUNT] += student_count
                        else:
                            # don't need this for parcc, state or district level if the grade doesn't exist for the school level
                            if guid not in grades:
                                continue
                        grades[guid].update(grade)

        return grades, courses

    def _display_course(self, record):
        grade = record[SummativeConstants.GUID]
        return not grade.startswith(Constants.GRADE_PREFIX)

    def _create_course(self, level, record):
        guid = record[Constants.NAME]
        level_data = self.format_data_for_entity(record, level)
        level_data[Constants.NAME] = level
        grades = {level: level_data}
        student_count = record.get(SummativeConstants.STUDENT_COUNT, 0)
        return guid, grades, student_count

    def format_data_for_entity(self, record, level):
        '''
        Formats data for a particular entity
        '''
        name = record[Constants.NAME]
        guid = record.get(SummativeConstants.GUID, name)  # grade does not exist for courses summary
        student_count = record[SummativeConstants.STUDENTS]
        rtn = {
            Constants.NAME: name,
            SummativeConstants.GUID: guid,
            SummativeConstants.HAS_FILTERS: self.has_filters,
            SummativeConstants.ALERT_THRESHOLD: self.alert_size,
            SummativeConstants.IS_LESS_THAN_THRESHOLD: self.min_size <= student_count < self.alert_size,
            'grade': guid,
        }

        if not self.has_filters:
            rtn[SummativeConstants.STUDENTS] = student_count
        if student_count < self.min_size:
            rtn[SummativeConstants.IS_LESS_THAN_MIN_CELL_SIZE] = True
            rtn[SummativeConstants.MIN_CELL_SIZE_MSG] = self.min_cell_msg
            rtn[SummativeConstants.PERFORMANCE_SORT] = -1
            return rtn

        perf_sort_value, perf_levels = self.perf_generator(record)
        rtn.update({
            'level': perf_sort_value,
            'score': round(record[Constants.SCORE]),
            SummativeConstants.AVG: {SummativeConstants.READING: round(record[SummativeConstants.READING] or 0),
                                     SummativeConstants.WRITING: round(record[SummativeConstants.WRITING] or 0)},
            'performanceLevels': perf_levels,
            'perfSortValue': perf_sort_value,
        })
        if record.get(SummativeConstants.PARCC_GROWTH_PERCENT) is not None:
            rtn[SummativeConstants.PARCC_GROWTH_PERCENT] = round(record[SummativeConstants.PARCC_GROWTH_PERCENT])
        if record.get(SummativeConstants.STATE_GROWTH_PERCENT) is not None:
            rtn[SummativeConstants.STATE_GROWTH_PERCENT] = round(record[SummativeConstants.STATE_GROWTH_PERCENT])
        return rtn

    def _create_grade(self, level, record):
        level_data = self.format_data_for_entity(record, level)
        level_data[Constants.NAME] = level
        name = record[SummativeConstants.GUID]
        entity_for_level = {
            SummativeConstants.GUID: record[SummativeConstants.GUID],
            Constants.NAME: name,
            level: level_data,
        }
        student_count = record.get(SummativeConstants.STUDENT_COUNT, 0)
        return record[SummativeConstants.GUID], entity_for_level, student_count

    def merge_with_aggregation(self, courses):
        for course_name, value in self.courses.items():
            grades = self._sort_by_grade_and_level(courses[course_name])
            value['grades'] = grades
        return self.courses

    def _sort_by_grade_and_level(self, courses):

        def _grade_and_level_tuple(item):
            for level, data in item.items():
                return (data['grade'], REPORT_LEVEL_WEIGHTS[level])

        return sorted(courses, key=_grade_and_level_tuple)


def format_school_level_results(subject, result, records, has_filters, alert_size, min_size, min_size_parcc, min_cell_size_msg):
    courses = records.get('courses')
    create_grades = GradeOrCourseFormatter(subject, result, courses, has_filters, alert_size, min_size, min_size_parcc, min_cell_size_msg)
    return GroupByAssessmentTypes(records).create_by(create_grades)
