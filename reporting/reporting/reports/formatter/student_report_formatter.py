from reporting.reports.helpers.metadata import get_ordered_subjects_dict, get_subjects_map
from reporting.reports.helpers.constants import Subject, Constants, SubjectJsonKeys, SUBCLAIM_TABLE_COL_MAP
from reporting.reports.summative.helpers.constants import StudentReportConstants, StudentRosterConstants
from reporting.reports.summative.helpers.constants import Constants as SummativeConstants
from reporting.reports.formatter.common import GroupByAssessmentTypes
from collections import defaultdict


class AssessmentData(dict):

    def __init__(self, results):
        subjects_map = get_subjects_map()
        if Constants.SUBJECT1 in results:
            results = results[Constants.SUBJECT1]
            subject = Subject.MATH
            subscore_class = MathSubscore
        else:
            results = results[Constants.SUBJECT2]
            subject = Subject.ELA
            subscore_class = ELASubscore
        if len(results) > 0:
            self[subjects_map[subject]] = SubjectData(results, subscore_class(results[0]), subject)


class SubjectData(dict):

    def __init__(self, results, subscores, subject):
        self[SummativeConstants.OVERALL] = Overall(results)
        self[StudentReportConstants.SUBSCORES] = subscores
        self[StudentReportConstants.COURSE] = results[0][Constants.ASMT_SUBJECT]

        grade = results[0][Constants.ASMT_GRADE]
        if self.display_course(subject, grade):
            self[StudentReportConstants.REPORT_TITLE_PREFIX] = results[0][Constants.ASMT_SUBJECT]
            self[StudentReportConstants.TEXT_BODY_GRADECOURSE] = results[0][Constants.ASMT_SUBJECT]
        elif subject is Subject.ELA:
            self[StudentReportConstants.REPORT_TITLE_PREFIX] = grade
            self[StudentReportConstants.TEXT_BODY_GRADECOURSE] = grade
        else:
            self[StudentReportConstants.REPORT_TITLE_PREFIX] = grade
            self[StudentReportConstants.TEXT_BODY_GRADECOURSE] = Subject.LABEL_MAP[Subject.MATH]

    def display_course(self, subject, grade):
        return subject == Subject.MATH and grade is None


class Overall(dict):

    def __init__(self, results):
        for record in results:
            cutpoint_label = record[Constants.CUTPOINT_LABEL]
            performance_level_id = int(record[Constants.PERFORMANCE_LEVEL_ID])
            perf_level = record[StudentReportConstants.ASMT_PERF_LVL]
            self[StudentReportConstants.PERFORMANCE_LEVEL] = perf_level
            self[Constants.SCORE] = record[SummativeConstants.ASMT_SCALED_SCORE]
            if perf_level == performance_level_id:
                self[Constants.NAME] = "{0} understanding".format(cutpoint_label)
            performance_data = self.setdefault(StudentReportConstants.PERFORMANCE, [])
            performance_data.append({
                Constants.NAME: cutpoint_label,
                StudentReportConstants.PERFORMANCE_LEVEL: performance_level_id,
                StudentReportConstants.CUTPOINT_START: float(record[Constants.CUTPOINT_LOW]),
                StudentReportConstants.CUTPOINT_END: float(record[Constants.CUTPOINT_UPPER])
            })
            self[StudentReportConstants.ERROR] = float(record[StudentReportConstants.SUM_CSEM])


class SubScore(list):

    def __init__(self, record):
        list.__init__([])
        self.record = record
        claim_mappings = self.get_claim_mappings()
        for claim_mapping in claim_mappings:
            self.append(self.build_claim(claim_mapping))

    def get_claim_mappings(self):
        raise NotImplementedError()

    def build_claim(self, claim_mapping):
        claim = {}
        claim[StudentReportConstants.CLAIMS] = self.build_subclaims(claim_mapping)
        if StudentReportConstants.SCORE_FIELD in claim_mapping:
            claim[StudentReportConstants.SUBSCORE] = self.build_subscore(claim_mapping)
        return claim

    def build_subclaims(self, claim_mapping):
        subclaim_categories = claim_mapping[StudentReportConstants.SUBCLAIM_CATEGORIES]
        return [{"level": self.record[subclaim]} for subclaim in subclaim_categories]

    def build_subscore(self, claim_mapping):
        subscore = {}
        subscore[Constants.SCORE] = self.record[claim_mapping[StudentReportConstants.SCORE_FIELD]]
        if self.record[claim_mapping[StudentReportConstants.ERROR_FIELD]]:
            error_field = float(self.record[claim_mapping[StudentReportConstants.ERROR_FIELD]])
            subscore[StudentReportConstants.ERROR] = error_field
        return subscore


class MathSubscore(SubScore):
    def get_claim_mappings(self):
        '''
        Order is important here
        '''
        return [
            {
                StudentReportConstants.SUBCLAIM_CATEGORIES: (
                    SUBCLAIM_TABLE_COL_MAP['majorcontent'],
                    SUBCLAIM_TABLE_COL_MAP['mathreason'],
                    SUBCLAIM_TABLE_COL_MAP['addcontent'],
                    SUBCLAIM_TABLE_COL_MAP['modelapp'],
                ),
            },
        ]


class ELASubscore(SubScore):

    def get_claim_mappings(self):
        return [
            {
                StudentReportConstants.SUBCLAIM_CATEGORIES: (StudentRosterConstants.SUBCLAIM1_CATEGORY,
                                                             StudentRosterConstants.SUBCLAIM2_CATEGORY,
                                                             StudentRosterConstants.SUBCLAIM3_CATEGORY,),
                StudentReportConstants.SCORE_FIELD: StudentReportConstants.CLAIM1_SCORE,
                StudentReportConstants.ERROR_FIELD: StudentReportConstants.SUM_READ_CSEM,
            },
            {
                StudentReportConstants.SUBCLAIM_CATEGORIES: (StudentRosterConstants.SUBCLAIM4_CATEGORY,
                                                             StudentRosterConstants.SUBCLAIM5_CATEGORY,),
                StudentReportConstants.SCORE_FIELD: StudentReportConstants.CLAIM2_SCORE,
                StudentReportConstants.ERROR_FIELD: StudentReportConstants.SUM_READ_CSEM,
            },
        ]


def format_results(results):
    return AssessmentData(results)


def create_subjects_list(results):
    subjects_list = []
    for subject, label in get_ordered_subjects_dict().items():
        if subject in results:
            label_text = Subject.LABEL_MAP[label]
            subjects_list.append({
                SubjectJsonKeys.LABEL: label_text,
                SubjectJsonKeys.VALUE: subject
            })
    return subjects_list


def format_summary(report_level, records, disabled):
    def create_summaries(records):
        if disabled:
            return [{
                SummativeConstants.SUMMARY_ROW_TYPE: report_level,
                SummativeConstants.ROW_DISABLED: True,
            }]
        summary = defaultdict(dict)
        claim_1_name = StudentReportConstants.CLAIM_MAP[StudentReportConstants.CLAIM1_SCORE]
        claim_2_name = StudentReportConstants.CLAIM_MAP[StudentReportConstants.CLAIM2_SCORE]
        for record in records:
            claim1_avg_score = None
            claim2_avg_score = None
            claim1_lvl4_avg = None
            claim2_lvl4_avg = None
            if record[StudentReportConstants.AVERAGE_SCORE] is None:
                continue
            overall_avg_score = round(record[StudentReportConstants.AVERAGE_SCORE])
            if record[StudentReportConstants.CLAIM1_SCORE]:
                claim1_avg_score = round(record[StudentReportConstants.CLAIM1_SCORE])
            if record[StudentReportConstants.CLAIM2_SCORE]:
                claim2_avg_score = round(record[StudentReportConstants.CLAIM2_SCORE])
            if record[StudentReportConstants.PARCC_LEVEL_4_AVERAGE][claim_1_name]:
                claim1_lvl4_avg = round(record[StudentReportConstants.PARCC_LEVEL_4_AVERAGE][claim_1_name])
            if record[StudentReportConstants.PARCC_LEVEL_4_AVERAGE][claim_2_name]:
                claim2_lvl4_avg = round(record[StudentReportConstants.PARCC_LEVEL_4_AVERAGE][claim_2_name])
            summary.update({
                StudentReportConstants.AVERAGE_SCORE: overall_avg_score,
                StudentReportConstants.AVERAGE_CLAIM_SCORE: {
                    claim_1_name: claim1_avg_score,
                    claim_2_name: claim2_avg_score
                },
                StudentReportConstants.PARCC_LEVEL_4_AVERAGE: {
                    claim_1_name: claim1_lvl4_avg,
                    claim_2_name: claim2_lvl4_avg
                },
                Constants.NAME: record[Constants.NAME],
                SummativeConstants.SUMMARY_ROW_TYPE: report_level,
            })
        return [summary]

    return GroupByAssessmentTypes(records).create_by(create_summaries)
