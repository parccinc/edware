from string import ascii_letters

from collections import defaultdict
from reporting.reports.formatter.common import GroupByAssessmentTypes
from reporting.reports.formatter.common import build_subclaim_perf_level_attr
from reporting.reports.helpers.constants import ALL_PERFORMANCE_LEVELS
from reporting.reports.helpers.constants import Constants
from reporting.reports.helpers.percentage_calc import normalize_percentages
from reporting.reports.summative.helpers.constants import Constants as SummativeConstants
from reporting.reports.summative.helpers.constants import StudentRosterConstants
from reporting.reports.helpers.name_formatter import format_full_name_rev


def get_rounded_value(record, key):
    if key in record and record[key] is not None:
        return round(record[key])
    else:
        return ''


def format_scores_view_results(records):

    def create_row_per_student(records):
        return [create_student(record) for record in records]

    def create_student(record):
        student = {}
        student.update(record)
        student.update(create_personal_info(record))
        student.update(create_subclaims(record))
        include_in_roster = record[StudentRosterConstants.INCLUDE_IN_ROSTER]
        student[StudentRosterConstants.IS_INVALID_SCORE] = include_in_roster == 'W'
        return student

    def create_personal_info(record):
        student_display_name = format_full_name_rev(record[Constants.STUDENT_FIRST_NAME],
                                                    record[Constants.STUDENT_MIDDLE_NAME],
                                                    record[Constants.STUDENT_LAST_NAME])
        return {
            Constants.STUDENT_DISPLAY_NAME: student_display_name,
            Constants.STUDENT_GUID: record[Constants.STUDENT_PARCC_ID],
            StudentRosterConstants.STUDENT_GRADE: record[StudentRosterConstants.STUDENT_GRADE]
        }

    def create_subclaims(record):

        return {
            'Overall': {
                Constants.LEVEL: record[StudentRosterConstants.SUM_PERF_LVL],
                Constants.SCORE: get_rounded_value(record, SummativeConstants.ASMT_SCALED_SCORE),
            },
            SummativeConstants.PARCC_GROWTH_PERCENT: get_rounded_value(record, SummativeConstants.PARCC_GROWTH_PERCENT),
            SummativeConstants.STATE_GROWTH_PERCENT: get_rounded_value(record, SummativeConstants.STATE_GROWTH_PERCENT),
        }

    return GroupByAssessmentTypes(records).create_by(create_row_per_student)


def format_scores_view_summary(report_level, records, disabled=False):

    def create_distribution(subclaim, record):
        subclaim_perf_levels = [build_subclaim_perf_level_attr(subclaim, perf_level)
                                for perf_level in ALL_PERFORMANCE_LEVELS]
        total_count = sum([record[subclaim_perf_level] for subclaim_perf_level in subclaim_perf_levels], 0)
        perf_level_distribution = []
        percentages = []
        for perf_level in ALL_PERFORMANCE_LEVELS:
            attr = build_subclaim_perf_level_attr(subclaim, perf_level)
            percent = record[attr] * 100 / total_count if total_count else 0
            percentages.append(percent)
            perf_level_distribution.append(
                {
                    Constants.LEVEL: perf_level,
                    StudentRosterConstants.DESCRIPTION: SummativeConstants.LEVEL_TITLES[perf_level],
                }
            )
        if total_count:
            percentages = normalize_percentages(percentages)
        for i in range(len(perf_level_distribution)):
            level_dist = perf_level_distribution[i]
            level_dist['percentage'] = percentages[i]

        return perf_level_distribution

    def create_current_subclaims(record):
        subclaims = {}
        for subclaim in StudentRosterConstants.SUBCLAIM_CATEGORIES:
            subclaims[subclaim] = {"performanceLevels": create_distribution(subclaim, record)}
        return subclaims

    def create_summary(record):
        if disabled:
            return {
                SummativeConstants.SUMMARY_ROW_TYPE: report_level,
                SummativeConstants.ROW_DISABLED: disabled,
            }
        summary = {
            "type": report_level,
            "name": record["name"],
            "id": record["id"],
            "Overall": {
                "score": round(record["sum_scale_score"] or 0),
                "level": round(record["sum_perf_lvl"]),
            },
        }
        if record.get(SummativeConstants.PARCC_GROWTH_PERCENT) is not None:
            summary[SummativeConstants.PARCC_GROWTH_PERCENT] = round(record[SummativeConstants.PARCC_GROWTH_PERCENT])
        if record.get(SummativeConstants.STATE_GROWTH_PERCENT) is not None:
            summary[SummativeConstants.STATE_GROWTH_PERCENT] = round(record[SummativeConstants.STATE_GROWTH_PERCENT])
        if "sum_read_scale_score" in record:
            read_score = record["sum_read_scale_score"]
            if read_score is not None:
                summary["sum_read_scale_score"] = round(read_score)
        if "sum_write_scale_score" in record:
            write_score = record["sum_write_scale_score"]
            if write_score is not None:
                summary["sum_write_scale_score"] = round(write_score)
        summary.update(create_current_subclaims(record))
        return summary

    def create_summaries(records):
        # query returns a list with only 1 element, but we still need to loop
        # through in order to compliant with the template in `GroupByAssessmentTypes`
        return [create_summary(record) for record in records]

    return GroupByAssessmentTypes(records).create_by(create_summaries)


def format_item_analysis_assessments(results, assessments):
    """
    Item analysis report formatter
    :param results: incoming results that should be updated
    :param assessments: assessments data for processing
    :return: results with assessments and columns info
    """

    def build_assessment_info(records):
        """
        The main builder for students and columns information
        :param records: received data
        :return: students and columns information for report
        """
        students = defaultdict(dict)
        columns = {}

        for record in records:
            # process student info
            student_guid = record[Constants.STUDENT_PARCC_ID]
            item = build_student_item(record)
            students[student_guid].update(item)
            students[student_guid][Constants.STUDENT_PARCC_ID] = student_guid
            students[student_guid][Constants.STUDENT_LAST_NAME] = record[Constants.STUDENT_LAST_NAME]
            students[student_guid][Constants.STUDENT_FIRST_NAME] = record[Constants.STUDENT_FIRST_NAME]
            students[student_guid][StudentRosterConstants.STUDENT_GRADE] = \
                record[StudentRosterConstants.STUDENT_GRADE]

            # process column info
            item_uin = record[StudentRosterConstants.ITEM_UIN]
            item_max_points = record[StudentRosterConstants.ITEM_MAX_POINTS]
            item_standards = [record.get(el) for el in StudentRosterConstants.STANDARDS_LIST
                              if record.get(el)]
            item_type = record[StudentRosterConstants.TEST_ITEM_TYPE]

            # we only want to increment the number of responses if the student attempted the item
            response_count_increment = 0 if record.get(StudentRosterConstants.PARENT_ITEM_SCORE) is None \
                else 1

            if item_uin not in columns or StudentRosterConstants.ITEM_NUM_RESPONSES not in columns[item_uin]:
                # this is the first iteration for this item
                item_response_count = response_count_increment
            else:
                item_response_count = \
                    columns[item_uin][StudentRosterConstants.ITEM_NUM_RESPONSES] + response_count_increment
            columns[item_uin] = {
                Constants.NAME: item_uin,
                StudentRosterConstants.ITEM_MAX_POINTS: item_max_points,
                StudentRosterConstants.STANDARDS: item_standards,
                StudentRosterConstants.ITEM_TYPE: item_type,
                StudentRosterConstants.ITEM_NUM_RESPONSES: item_response_count
            }

        return sorted(students.values(), key=students_sorter), sorted(columns.values(), key=items_sorter)

    def build_student_item(record):
        student_display_name = format_full_name_rev(record[Constants.STUDENT_FIRST_NAME],
                                                    record[Constants.STUDENT_MIDDLE_NAME],
                                                    record[Constants.STUDENT_LAST_NAME])
        overall_level = record[StudentRosterConstants.SUM_PERF_LVL]
        overall_score = get_rounded_value(record, SummativeConstants.ASMT_SCALED_SCORE)
        item_name = record[StudentRosterConstants.ITEM_UIN]
        item_score = get_rounded_value(record, StudentRosterConstants.PARENT_ITEM_SCORE)
        is_invalid_score = record[StudentRosterConstants.INCLUDE_IN_ROSTER] == 'W'
        return {
            item_name: {
                Constants.SCORE: item_score
            },
            StudentRosterConstants.OVERALL: {
                Constants.LEVEL: overall_level,
                Constants.SCORE: overall_score
            },
            Constants.STUDENT_DISPLAY_NAME: student_display_name,
            StudentRosterConstants.IS_INVALID_SCORE: is_invalid_score,
        }

    def students_sorter(k):
        return (k[Constants.STUDENT_LAST_NAME],
                k[Constants.STUDENT_FIRST_NAME],
                k[Constants.STUDENT_PARCC_ID])

    def items_sorter(x):
        # This is used for sorting the columns to be displayed (ie. item 1, item 2, item3, etc.)
        return int(x[Constants.NAME].strip(ascii_letters + "-_"))

    def group_data(data):
        """
        This is the "builder" method, we should have it if we want to use
         current grouping logic in common.py file
        """
        return data

    results[Constants.ASSESSMENTS] = {}
    results[Constants.COLUMNS] = {}

    for subject, asessment_data in assessments.items():
        if asessment_data:
            students, columns = build_assessment_info(asessment_data)
            # We need group data by assessment type and we use existing logic from common.py
            # Use update, for cases when we receive data for more than 1 subject
            results[Constants.ASSESSMENTS].update(
                GroupByAssessmentTypes({subject: students}).create_by(group_data))
            results[Constants.COLUMNS].update(
                GroupByAssessmentTypes({subject: columns}).create_by(group_data))
    return results


def format_item_analysis_summary(report_level, records, disabled=False):

    def build_item(record):
        overall_level = round(record[StudentRosterConstants.SUM_PERF_LVL])
        overall_score = round(record[SummativeConstants.ASMT_SCALED_SCORE])
        item_name = record[StudentRosterConstants.ITEM_UIN]
        item_score = round(record[StudentRosterConstants.PARENT_ITEM_SCORE])
        row = {
            item_name: {Constants.SCORE: item_score},
            StudentRosterConstants.OVERALL: {
                Constants.LEVEL: overall_level,
                Constants.SCORE: overall_score
            },
            SummativeConstants.NAME: record[SummativeConstants.NAME],
            SummativeConstants.SUMMARY_ROW_TYPE: report_level,
        }
        return row

    def create_summaries(records):
        if disabled:
            return [{
                SummativeConstants.SUMMARY_ROW_TYPE: report_level,
                SummativeConstants.ROW_DISABLED: disabled,
            }]
        summary = defaultdict(dict)
        for record in records:
            item = build_item(record)
            summary.update(item)
        return [summary]

    return GroupByAssessmentTypes(records).create_by(create_summaries)
