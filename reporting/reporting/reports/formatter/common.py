import math
from reporting.reports.helpers.constants import (ALL_PERFORMANCE_LEVELS,
                                                 OVERALL_PERFORMANCE_LEVELS,
                                                 AssessmentType, Constants, TablePrefix)
from reporting.reports.helpers.metadata import get_subjects_map
from reporting.reports.helpers.name_formatter import format_full_name_rev


def ensure_sum_to_100(percentages):

    def _largest_remainder_rounding(list_to_round, total):
        # http://en.wikipedia.org/wiki/Largest_remainder_method
        values = [math.modf(entry) for entry in list_to_round]
        fractional_part = [entry[0] for entry in values]
        integer_part = [int(entry[1]) for entry in values]

        fractional_index_value_order = sorted(
            [(idx, value) for idx, value in enumerate(fractional_part)],
            key=lambda k: k[1]
        )
        running_total = sum(integer_part)
        while running_total < total and fractional_index_value_order:
            index, _ = fractional_index_value_order.pop()
            integer_part[index] += 1
            running_total += 1
        return integer_part

    return _largest_remainder_rounding(percentages, 100)


def get_overall_perf_level_distribution(result):

    def get_perf_percentage(level, total_count):
        return result[build_overall_perf_level_attr(level)] * 100.0 / total_count

    def _get_overall_perf_level_count(result):
        return sum(result[build_overall_perf_level_attr(level)] for level in OVERALL_PERFORMANCE_LEVELS)

    total_count = _get_overall_perf_level_count(result)
    percentages = [get_perf_percentage(level, total_count) for level in OVERALL_PERFORMANCE_LEVELS]
    percentages = ensure_sum_to_100(percentages)
    greater_than_level_4 = percentages[3] + percentages[4]

    all_levels = []
    for percentage, level in zip(percentages, OVERALL_PERFORMANCE_LEVELS):
        all_levels.append({
            Constants.PERCENTAGE: percentage,
            Constants.LEVEL: level
        })
    return greater_than_level_4, all_levels


def get_subscore_perf_level_distribution(claim_column, result):

    def get_perf_percentage(level, total_count):
        if total_count == 0:
            return 0
        return result[build_subclaim_perf_level_attr(claim_column, level)] * 100.0 / total_count

    def _get_subscore_perf_level_count(claim_column, result):
        return sum(result[build_subclaim_perf_level_attr(claim_column, level)] for level in ALL_PERFORMANCE_LEVELS)

    total_count = _get_subscore_perf_level_count(claim_column, result)
    percentages = [get_perf_percentage(level, total_count) for level in ALL_PERFORMANCE_LEVELS]
    percentages = ensure_sum_to_100(percentages)
    level_3 = percentages[2]

    all_levels = []
    for percentage, level in zip(percentages, ALL_PERFORMANCE_LEVELS):
        all_levels.append({
            Constants.PERCENTAGE: percentage,
            Constants.LEVEL: level,
        })
    return level_3, all_levels


def build_subclaim_perf_level_attr(subclaim, performance_level):
    return '%s_perf_%d' % (subclaim, performance_level)


def build_overall_perf_level_attr(performance_level):
    return 'overall_perf_%d' % (performance_level)


def get_common_diagnostic_format_data(record):
    student_result = {}
    student_display_name = format_full_name_rev(
        record[Constants.STUDENT_FIRST_NAME],
        record[Constants.STUDENT_MIDDLE_NAME],
        record[Constants.STUDENT_LAST_NAME]
    )
    student_result[Constants.STUDENT_GUID] = record[Constants.STUDENT_PARCC_ID]
    student_result[Constants.STUDENT_DISPLAY_NAME] = student_display_name
    student_result[Constants.ASMT_GRADE] = record[Constants.ASMT_GRADE]
    student_result[Constants.STAFF_ID] = record[Constants.STAFF_ID]
    student_result[Constants.ASMT_DATE] = record[Constants.ASMT_DATE].strftime('%m/%d/%Y')
    return student_result


def strip_table_prefixes(table_name):
    return table_name.replace(TablePrefix.ELA, '', 1)\
        .replace(TablePrefix.MATH, '', 1)\
        .replace(TablePrefix.RPT, '', 1)


class GroupByAssessmentTypes:

    def __init__(self, records):
        self.records = records

    def create_by(self, builder):
        # # only have one assessment type right now
        return {asmtType: GroupBySubjects(self.records).create_by(builder)
                for asmtType in (AssessmentType.SUMMATIVE,)}


class GroupBySubjects:

    def __init__(self, records):
        self.records = records

    def create_by(self, builder):
        # # I know this is weird, but subjects_map really maps from name to key
        return {subject_key: StudentRecords(self.records.get(subject_key, [])).create_by(builder)
                for subject_key in get_subjects_map().values()}


class StudentRecords:

    def __init__(self, records):
        self.records = records

    def create_by(self, builder_func):
        return [el for el in builder_func(self.records)]
