'''
Created on May 7, 2013

@author: dip
'''
from functools import wraps
from sqlalchemy.sql.expression import Select
from pyramid.security import authenticated_userid
import pyramid
from pyramid.httpexceptions import HTTPForbidden
from pyramid.threadlocal import get_current_registry
from reporting.reports.helpers.constants import Constants
from reporting.security.context_role_map import ContextRoleMap
from edcore.database.edcore_connector import EdCoreDBConnection
from edcore.security.tenant import get_tenant_by_state_code, get_public_access_states
from parcc_common.security.constants import RolesConstants


def select_with_context(columns=None, whereclause=None, from_obj=[],
                        permission=RolesConstants.PII, **kwargs):
    '''
    Returns a SELECT clause statement with context security attached in the WHERE clause

    Note: state_code must be passed in as kwargs for database routing for multi tenant users
    '''
    # Retrieve state code for db connection routing
    state_code = kwargs.get(Constants.STATE_CODE)
    kwargs.pop(Constants.STATE_CODE, None)
    with EdCoreDBConnection(state_code=state_code) as connector:
        # Get user role and guid
        user = __get_user_info()

        # Build query
        query = Select(columns, whereclause=whereclause, from_obj=from_obj, **kwargs)

        if permission not in user.get_roles():
            raise HTTPForbidden()
        context = __get_context_instance(permission, connector)
        # Get context security expression to attach to where clause
        query = context.add_context(get_tenant_by_state_code(state_code), user, query)

    return query


def check_context(permission, state_code, student_ids):
    '''
    Given a list of student guids, return true if user has access to see their data, false otherwise

    :param student_ids: guids of students that we want to check whether the user has context to
    :type student_ids: list
    '''
    if len(student_ids) is 0:
        return False

    with EdCoreDBConnection(state_code=state_code) as connector:
        # Get user role and guid
        user = __get_user_info()
        context = __get_context_instance(permission, connector)
        return context.check_context(get_tenant_by_state_code(state_code), state_code, user, student_ids)


def is_permitted(role, params):
    '''
    Given a role and params, return whether user has permission to the entire hierarchy (stops at school level)
    Since we stop at school level, we know that the user either can see all or see none of the current level
    '''
    user = __get_user_info()
    state_code = params.get(Constants.STATECODE)
    tenant = get_tenant_by_state_code(state_code)
    user_context = user.get_context()
    chain = user_context.get_chain(tenant, role, params)
    return chain['all'] or len(chain['guid']) > 0


def get_access_permissions(tenant, user, params):
    permissions = {'all': True}  # permission to access everything
    if not params.get(Constants.STATECODE):
        # consortium report
        permissions['all'] = False
        permissions['guid'] = list(set(get_public_access_states() + user.get_state_codes()))
    elif params.get(Constants.SCHOOLGUID):
        # student, student roster or school report
        permissions = user.get_context().get_chain(tenant, RolesConstants.PII, params)
    return permissions


def get_current_context(params):
    '''
    Given request parameters, determine if the user has context to the next hierarchy level
    '''
    user = __get_user_info()
    state_code = params.get(Constants.STATECODE)
    tenant = get_tenant_by_state_code(state_code)
    user_context = user.get_context()
    reporting_permissions = get_access_permissions(tenant, user, params)
    # For extracts, we just need to return a boolean of whether he has permission for the current tenant
    has_analytics_permission = user_context.is_permission_allow_for_tenant(tenant,
                                                                           RolesConstants.PII_ANALYTICS)
    sf_extract = user_context.is_permission_allow_for_tenant(tenant, RolesConstants.SF_EXTRACT)
    rf_extract = user_context.is_permission_allow_for_tenant(tenant, RolesConstants.RF_EXTRACT)
    pf_extract = user_context.is_permission_allow_for_tenant(tenant, RolesConstants.PF_EXTRACT)
    psrd_extract = user_context.is_permission_allow_for_tenant(tenant, RolesConstants.PSRD_EXTRACT)
    cds_extract = user_context.is_permission_allow_for_tenant(tenant, RolesConstants.CDS_EXTRACT)
    return {'pii': reporting_permissions,
            'pii_analytics': has_analytics_permission,
            'sf_extract': sf_extract,
            'rf_extract': rf_extract,
            'pf_extract': pf_extract,
            'psrd_extract': psrd_extract,
            'cds_extract': cds_extract,
            'display_extract': sf_extract or rf_extract or pf_extract or psrd_extract or cds_extract}


def get_current_request_context(origin_func):
    '''
    Decorator to return current user context
    '''
    @wraps(origin_func)
    def wrap(*args, **kwds):
        results = origin_func(*args, **kwds)
        if results:
            results['context']['permissions'] = get_current_context(*args)
        return results
    return wrap


def __get_user_info():
    '''
    Returns user object.  This is not the session object

    '''
    return authenticated_userid(pyramid.threadlocal.get_current_request())


def __get_context_instance(role, connector):
    '''
    Given a role in string, return the context instance for it
    '''
    # Get the context object
    context_obj = ContextRoleMap.get_context(role)
    # Instantiate it
    return context_obj(connector, role)
