'''
Created on May 9, 2013

@author: dip
'''
from edapi.exceptions import ForbiddenError
from sqlalchemy.sql.expression import distinct, and_, select
from reporting.reports.helpers.constants import Constants, RptTable
from reporting.reports.diagnostic.helpers.constants import DiagnosticsTable
from edschema.metadata.util import get_selectable_by_table_name
from reporting.reports.helpers.query_utils import execute_query_remotely
from edcore.utils.utils import compile_query_to_sql_text


class BaseRole(object):
    '''
    Base Class Role
    '''
    def __init__(self, connector, name):
        self.connector = connector
        self.name = name

    def add_context(self, tenant, user, query):
        return query

    def check_context(self, tenant, state_code, user, student_ids):
        '''
        Given a query, check for context
        '''
        queries = self.get_students(tenant, student_ids)
        for query in queries:
            query = self.add_context(tenant, user, query)
            results = execute_query_remotely(state_code, compile_query_to_sql_text(query))
            if len(student_ids) == len(results):
                return True
        return False

    def get_students(self, tenant, student_ids):
        '''
        Returns a query that gives a list of distinct student guids
        given that a list of student guids are supplied.
        '''
        queries = []
        for asmt_table_name in [RptTable.ELA_SUM, RptTable.MATH_SUM]:
            asmt_table = self.connector.get_table(asmt_table_name)
            query = select([distinct(asmt_table.c.student_parcc_id)],
                           from_obj=[asmt_table])
            query = query.where(and_(asmt_table.c.rec_status == Constants.CURRENT,
                                     asmt_table.c.student_parcc_id.in_(student_ids)))
            queries.append(query)
        return queries

    def get_context_tables(self, query):
        '''
        Get a list of context tables from the query
        '''
        return {obj for (obj, name) in get_selectable_by_table_name(query).items()
                if name in (RptTable.ELA_SUM,
                            RptTable.MATH_SUM,
                            RptTable.RPT_RIF_MATH,
                            RptTable.RPT_RIF_ELA,
                            DiagnosticsTable.ELA_READING_COMPREHENSION,
                            DiagnosticsTable.ELA_READING_FLUENCY,
                            DiagnosticsTable.ELA_VOCABULARY,
                            DiagnosticsTable.ELA_DECODE,
                            DiagnosticsTable.ELA_WRITING,
                            DiagnosticsTable.ELA_READER_MOTIVATION,
                            DiagnosticsTable.MATH_CLUSTER,
                            DiagnosticsTable.MATH_FLUENCY,
                            DiagnosticsTable.MATH_GRADE_LEVEL,
                            DiagnosticsTable.MATH_LOCATOR,
                            DiagnosticsTable.MATH_PROGRESSION)}


def verify_context(fn):
    '''
    Decorator used to validate that we throw Forbidden error when context is an empty list
    '''
    def wrapped(*args, **kwargs):
        context = fn(*args, **kwargs)
        if context is None:
            raise ForbiddenError
        return context
    return wrapped
