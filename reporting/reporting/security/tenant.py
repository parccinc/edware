'''
Created on Apr 3, 2014

@author: dip
'''
import logging
from functools import wraps
from pyramid.httpexceptions import HTTPForbidden
from pyramid.security import authenticated_userid
from pyramid.threadlocal import get_current_request
from edcore.security.tenant import get_state_code_to_tenant_map, is_public_access_state
from reporting.reports.helpers.constants import Constants

logger = logging.getLogger("reporting")


def validate_user_tenant(origin_func):
    '''
    Decorator to validate that user has access to the state from the request
    '''
    @wraps(origin_func)
    def wrap(*args, **kwds):
        if not has_access_to_state(*args):
            logger.warning("Permission denied to report. User doesn't have correct permission.")
            return HTTPForbidden()
        results = origin_func(*args, **kwds)
        return results
    return wrap


def has_access_to_state(params):
    '''
    Given a dictionary of request parameters, return true if an user has access to that tenant
    Returns true if user has access to the state
    If stateCode isn't found in params, it'll assume it's accessing consortium level,
    therefore we allow access.
    :param dict params:  dictionary of parameters to a request
    '''
    state_code = params.get(Constants.STATECODE)
    has_access = False
    if state_code is None:
        has_access = True
    else:
        __user = authenticated_userid(get_current_request())
        if is_public_access_state(state_code):
            return True
        if __user:
            user_tenants = __user.get_tenants()
            _map = get_state_code_to_tenant_map()
            if user_tenants:
                tenant = _map.get(state_code)
                has_access = True if tenant in user_tenants else False
    if not has_access:
        logger.warning('Access to the state "{}" is denied or state not exist.'.format(state_code))
    return has_access
