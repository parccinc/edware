'''
This package implements SBAC context security. Context security is the
authorization given to different roles (teacher, administrator, etc.)
to access student PII.
'''
