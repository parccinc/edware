'''
Created on Oct 25, 2013

@author: tosako
'''


class Constants():
    '''
    constants for utils
    '''
    PROPERTIES = 'properties'
    ENVNAME = 'envName'
    PROPERTYKEY = 'propertyKey'
    PROPERTYVALUE = 'propertyValue'
    ID = 'id'
    NAME = 'name'
    ENCRYPT = 'encrypt'
