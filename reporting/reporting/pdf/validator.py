from functools import wraps
from parcc_common.security.constants import RolesConstants
from reporting.security.context import check_context
from edapi.exceptions import ForbiddenError
import logging


logger = logging.getLogger("reporting")


def raise_forbidden_error():
    logger.warning("Permission denied to PDF. User doesn't have correct permission.")
    raise ForbiddenError('Access Denied')


def valid_access_permission(origin_func):

    @wraps(origin_func)
    def _has_context_for_pdf_request(generator, *args, **kwargs):
        '''
        Validates that user has context to student_id

        :param student_id:  guid(s) of the student(s)
        '''
        state_code = generator.config.state_code

        student_ids = generator.config.student_guid
        student_ids = student_ids if isinstance(student_ids, list) else [student_ids]
        try:
            has_access = check_context(RolesConstants.PII, state_code, student_ids)
        except ValueError:
            raise_forbidden_error()
        else:
            if has_access:
                logger.info('Permission confirmed. Continue to PDF')
            else:
                raise_forbidden_error()

        return origin_func(generator, *args, **kwargs)

    return _has_context_for_pdf_request
