from abc import ABCMeta, abstractmethod
import services.celery
import pyramid
from edapi.exceptions import ForbiddenError
from edapi.httpexceptions import EdApiHTTPForbiddenAccess, EdApiHTTPInternalServerError
from edcore.utils.utils import to_bool
from reporting.reports.helpers.constants import AssessmentType, Constants
from reporting.pdf.validator import valid_access_permission
from services.exceptions import PdfGenerationError


class PDFGenerator(metaclass=ABCMeta):

    def __init__(self, *args, **kwargs):
        config_class = self.get_configuration_class()
        self.config = config_class(*args, **kwargs)

    @abstractmethod
    def generate(self):
        raise NotImplementedError()

    @valid_access_permission
    def run(self):
        try:
            return self.generate()
        except ForbiddenError as e:
            raise EdApiHTTPForbiddenAccess(e.msg)
        except (PdfGenerationError, TimeoutError) as e:
            raise EdApiHTTPInternalServerError(e.msg)
        except Exception as e:
            raise EdApiHTTPInternalServerError("Internal Error: [{}]".format(e))

    @abstractmethod
    def get_configuration_class(self):
        raise NotImplementedError()


class Configuration:

    def __init__(self,
                 studentGuid=None,
                 stateCode=None,
                 districtGuid=None,
                 schoolGuid=None,
                 gradeCourse=None,
                 asmtType=AssessmentType.SUMMATIVE,
                 year=None,
                 pdf=Constants.GRAY,
                 lang='en',
                 subject=None,
                 **kwargs):
        self.student_guid = studentGuid
        self.state_code = stateCode
        self.district_guid = districtGuid
        self.school_guid = schoolGuid
        self.grades = gradeCourse if gradeCourse else []
        self.asmt_type = asmtType
        self.asmt_year = year
        self.subject = subject
        self.color_mode = pdf.lower()
        self.lang = lang.lower()
        self.pdf = pdf
        self.subprocess_timeout = services.celery.TIMEOUT
        self.is_grayscale = (pdf == Constants.GRAY)
        settings = pyramid.threadlocal.get_current_registry().settings
        self.celery_timeout = int(settings.get('pdf.celery_timeout', '30'))
        self.always_generate = to_bool(settings.get('pdf.always_generate', False))

        # Set up a couple additional variables
        self.pdf_base_dir = settings.get('pdf.report_base_dir', "/tmp")
        self.single_generate_queue = settings.get('pdf.single_generate.queue')
        self.proxy = settings.get('services.proxy_url')
