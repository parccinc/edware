'''
Created on May 17, 2013

@author: tosako
'''
import os
from sqlalchemy.sql.expression import Select, and_, distinct
from edapi.exceptions import NotFoundException
from reporting.reports.helpers.constants import Constants, AssessmentType
from edcore.database.edcore_connector import EdCoreDBConnection
from reporting.reports.helpers.query_utils import execute_query_remotely
from reporting.reports.helpers.grade_course import get_grade_course_expr
from edcore.utils.utils import format_asmt_year


def create_pdf_path(asmt_grade, asmt_year, district_guid, pdf_report_base_dir, school_guid, state_code,
                    student_guid, subject, asmt_type=AssessmentType.SUMMATIVE, grayScale=True, lang='en'):
    # sample path: /tmp/pdf/RI/2014-2015/R0003/RP002/09/isr/SUMMATIVE/ELA/GuxIDl2Fjyjs81hW6n9gmufC4ruRxwcV8DCuKqbi.en.g.pdf
    suffix = ".g.pdf" if grayScale else ".pdf"
    file_path = os.path.join(state_code, asmt_year, district_guid, school_guid, asmt_grade,
                             'isr', asmt_type, subject, student_guid + "." + lang + suffix)
    file_path = os.path.join(pdf_report_base_dir, file_path)
    return file_path


def generate_pdf_path_by_student_id(rpt_table_map, state_code, district_guid, school_guid,
                                    grade_course, asmt_year, subject, pdf_report_base_dir='/',
                                    student_ids=None, asmt_type=AssessmentType.SUMMATIVE,
                                    grayScale=True, lang='en'):
    '''
    Get Individual Student Report absolute path by student_id.
    If the directory path does not exist, then create it.
    For security, the directory will be created with only the owner can read-write.
    '''
    file_paths = {}
    if type(student_ids) is not list:
        student_ids = [student_ids]
    with EdCoreDBConnection(state_code=state_code) as connection:
        asmt_table = connection.get_table(rpt_table_map.get_table_name())
        query = Select([distinct(asmt_table.c.student_parcc_id).label(Constants.STUDENT_PARCC_ID),
                        asmt_table.c.state_code.label(Constants.STATE_CODE),
                        asmt_table.c.year.label(Constants.ASMT_YEAR),
                        asmt_table.c.resp_dist_id.label(Constants.DISTRICT_GUID),
                        asmt_table.c.resp_school_id.label(Constants.SCHOOL_GUID),
                        asmt_table.c.asmt_grade.label(Constants.ASMT_GRADE)],
                       from_obj=[asmt_table])
        query = query.where(and_(asmt_table.c.rec_status == Constants.CURRENT,
                                 asmt_table.c.year == format_asmt_year(asmt_year),
                                 asmt_table.c.state_code == state_code,
                                 asmt_table.c.resp_dist_id == district_guid,
                                 asmt_table.c.resp_school_id == school_guid,
                                 get_grade_course_expr(grade_course, asmt_table),
                                 asmt_table.c.student_parcc_id.in_(student_ids)))
        results = execute_query_remotely(state_code, query)

    if len(results) != len(student_ids):
        raise NotFoundException("student count does not match with result count")
    for result in results:
        student_guid = result[Constants.STUDENT_PARCC_ID]
        state_code = result[Constants.STATE_CODE]
        asmt_period_year = str(result[Constants.ASMT_YEAR])
        district_guid = result[Constants.DISTRICT_GUID]
        school_guid = result[Constants.SCHOOL_GUID]
        asmt_grade = result.get(Constants.ASMT_GRADE) or grade_course

        # get absolute file path name
        file_path = create_pdf_path(asmt_grade, asmt_period_year, district_guid, pdf_report_base_dir,
                                    school_guid, state_code, student_guid, subject,
                                    asmt_type, grayScale, lang)
        file_paths[student_guid] = file_path
    return file_paths
