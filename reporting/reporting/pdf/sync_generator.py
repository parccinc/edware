from reporting.reports.helpers.constants import Constants
from edauth.security.utils import get_session_cookie
from reporting.pdf.base import PDFGenerator, Configuration
from pyramid.response import Response
from services.tasks.pdf import get
from reporting.pdf.utils import create_student_pdf_url
from reporting.reports.helpers.report_table_map import ReportTableMap
from reporting.reports.helpers.metadata import get_subject_by_alias
from reporting.pdf.path_generator import generate_pdf_path_by_student_id


class SyncPDFGenerator(PDFGenerator):

    def get_configuration_class(self):
        return SyncGeneratorConfig

    def generate(self):
        _, cookie_value = get_session_cookie()
        url = self.config.url
        file_name = self.config.file_name
        options = self.config.celery_options
        queue_name = self.config.queue_name

        celery_response = get.apply_async(
            args=(cookie_value, url, file_name),
            kwargs=options,
            queue=queue_name)

        celery_timeout = self.config.celery_timeout
        pdf_stream = celery_response.get(timeout=celery_timeout)

        return Response(body=pdf_stream, content_type=Constants.APPLICATION_PDF)


class SyncGeneratorConfig(Configuration):

    @property
    def url(self):
        params = {
            Constants.STATECODE: self.state_code,
            Constants.DISTRICTGUID: self.district_guid,
            Constants.SCHOOLGUID: self.school_guid,
            Constants.GRADECOURSE: self.grades,
            Constants.STUDENTGUID: self.student_guid,
            Constants.YEAR: self.asmt_year,
            Constants.ASMTTYPE: self.asmt_type,
            Constants.SUBJECT: self.subject,
            Constants.PDF: self.pdf
        }
        return create_student_pdf_url(params)

    @property
    def file_name(self):
        subject_name = get_subject_by_alias(self.subject)
        table_map = ReportTableMap(self.subject)

        files_by_guid = generate_pdf_path_by_student_id(table_map, self.state_code, self.district_guid, self.school_guid, self.grades,
                                                        self.asmt_year, subject_name, pdf_report_base_dir=self.pdf_base_dir,
                                                        student_ids=[self.student_guid], asmt_type=self.asmt_type,
                                                        grayScale=self.is_grayscale, lang=self.lang)
        return files_by_guid[self.student_guid]

    @property
    def queue_name(self):
        return self.single_generate_queue

    @property
    def celery_options(self):
        cookie_name, _ = get_session_cookie()
        return {
            'cookie_name': cookie_name,
            'timeout': self.subprocess_timeout,
            'grayscale': self.is_grayscale,
            'always_generate': self.always_generate,
            'proxy_url': self.proxy
        }
