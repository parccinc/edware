from reporting.reports.helpers.filters import FILTERS_CONFIG
from edcore.utils.utils import merge_dict
from reporting.reports.helpers.constants import AssessmentType, Constants


class PDF_PARAMS:

    COMMON = {
        Constants.STATECODE: {
            "type": "string",
            "required": True,
            "pattern": "^[a-zA-Z]{2}$"
        },
        Constants.DISTRICTGUID: {
            "type": "string",
            "required": True,
            "pattern": "^[a-zA-Z0-9\-]{0,40}$",
        },
        Constants.SCHOOLGUID: {
            "type": "string",
            "required": True,
            "pattern": "^[a-zA-Z0-9\-]{0,40}$",
        },
        Constants.GRADECOURSE: {
            "type": "string",
            "pattern": "^[a-z0-9A-Z ]{2,30}$",
            "required": True
        },
        Constants.YEAR: {
            "type": "integer",
            "required": True,
            "pattern": "^[1-9][0-9]{3}$"
        },
        Constants.ASMTTYPE: {
            "type": "string",
            "pattern": "^(" + AssessmentType.SUMMATIVE + ")$",
            "required": True
        },
        Constants.PDF: {
            "type": "string",
            "required": False,
            "pattern": "^(gray|GRAY|color|COLOR)$",
        },
    }

    ASYNC = {
        "type": "object",
        "properties": merge_dict({
            Constants.STATECODE: {
                "type": "string",
                "required": True,
                "pattern": "^[a-zA-Z]{2}$"
            },
            Constants.STUDENTGUID: {
                "type": "array",
                "items": {
                    "type": "string",
                    "pattern": "^[a-zA-Z0-9\-]{0,50}$"
                },
                "minItems": 1,
                "uniqueItems": True,
                "required": False
            },
            Constants.DISTRICTGUID: {
                "type": "string",
                "required": False,
                "pattern": "^[a-zA-Z0-9\-]{0,50}$",
            },
            Constants.SCHOOLGUID: {
                "type": "string",
                "required": False,
                "pattern": "^[a-zA-Z0-9\-]{0,50}$",
            },
            Constants.GRADECOURSE: {
                "type": "array",
                "items": {
                    "type": "string",
                    "pattern": "^[a-z0-9A-Z ]{2,30}$"
                },
                "minitems": 1,
                "uniqueItems": True,
                "required": False
            },
        }, COMMON, FILTERS_CONFIG)
    }

    SYNC = {
        "type": "object",
        "properties": merge_dict({
            Constants.STUDENTGUID: {
                "type": "string",
                "pattern": "^[a-zA-Z0-9\-]{0,50}$",
                "required": True,
            },
            Constants.SUBJECT: {
                "type": "string",
                "required": True,
                "pattern": "^(%s|%s)$" % (Constants.SUBJECT1, Constants.SUBJECT2)
            },
        }, COMMON)
    }


class Constants():
    SUMMATIVE_REPORT = "studentSummativeReport.html"
