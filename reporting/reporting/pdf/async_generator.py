from reporting.reports.helpers.constants import Constants
from reporting.pdf.base import PDFGenerator, Configuration
from pyramid.response import Response
from celery.canvas import group, chain
from pyramid.security import authenticated_userid
from pyramid.threadlocal import get_current_request
import json
from services.tasks.pdf import archive, hpz_upload_cleanup, group_separator


class AsyncPDFGenerator(PDFGenerator):

    def get_configuration_class(self):
        return AsyncGeneratorConfig

    def generate(self):
        # Start the bulk merge
        # TODO: see services.tasks to get subtasks for bulk pdf
        self.start_bulk()
        # Return the JSON response while the bulk merge runs asynchronously
        return Response(body=json.dumps({}), content_type=Constants.APPLICATION_JSON)

    def start_bulk(self, archive_file_path, directory_to_archive, registration_id, gen_tasks, merge_tasks, cover_tasks,
                   merge_covers_tasks, pdf_base_dir):
        '''
        entry point to start a bulk PDF generation request for one or more students
        it groups the generation of individual PDFs into a celery task group and then chains it to the next task to merge
        the files into one PDF, archive the PDF into a zip, and upload the zip to HPZ
        '''

        workflow = chain(group(gen_tasks),
                         group_separator.subtask(immutable=True),
                         group(merge_tasks),
                         group_separator.subtask(immutable=True),
                         group(cover_tasks),
                         group_separator.subtask(immutable=True),
                         group(merge_covers_tasks),
                         archive.subtask(args=(archive_file_path, directory_to_archive), immutable=True),
                         hpz_upload_cleanup.subtask(args=(archive_file_path, registration_id, pdf_base_dir), immutable=True))
        workflow.apply_async()


class AsyncGeneratorConfig(Configuration):

    @property
    def user_name(self):
        # Get the user
        user = authenticated_userid(get_current_request())
        return user._User__info['name']['fullName'],

    @property
    def pdf_base_dir(self):
        return "/tmp"

    @property
    def directory_for_merged_pdfs(self):
        return "/tmp"
