import pyramid
from urllib.parse import urljoin
import urllib.parse
from reporting.pdf.constants import Constants as PDFConstants


def create_students_pdf_urls(student_guids):
    return {guid: create_student_pdf_url(guid) for guid in student_guids}


def create_student_pdf_url(params):
    encoded_params = urllib.parse.urlencode(params)
    base_url = create_base_url()
    return base_url + "?%s" % encoded_params


def create_base_url():
    return urljoin(pyramid.threadlocal.get_current_request().application_url, '/assets/html/%s' % PDFConstants.SUMMATIVE_REPORT)
