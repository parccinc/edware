from setuptools import setup, find_packages

requires = [
    "SQLAlchemy == 0.9.9",
    "kombu==2.5.16",
    "billiard==2.7.3.34",
    "celery==3.0.25",
    "anyjson==0.3.3",
    "amqp==1.0.13",
    "apscheduler==2.1.1",
    "mocket==1.0.0",
    "mock==1.0.1",
    "gns3-netifaces==0.10.4.1",
    "pyyaml==3.10",
]

tests_require = requires

docs_extras = [
    'Sphinx',
    'docutils',
    'repoze.sphinx.autointerface']

setup(name='edreplicate',
      version='0.1',
      description='Master/Slave refresh',
      classifiers=[
          "Programming Language :: Python",
      ],
      author='',
      author_email='',
      url='',
      keywords='edreplicate edware celery',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='nose.collector',
      install_requires=requires,
      tests_require=tests_require,
      extras_require={
          'docs': docs_extras, },
      entry_points="""\
      """,
      )
