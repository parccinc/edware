import logging
import time
import threading
from edreplicate.utils.constants import Constants
from edreplicate.conductor.player_tracker import PlayerTracker
from edreplicate.utils.exceptions import ConductorTimeoutException
from edreplicate.conductor.replication_monitor import replication_monitor


logger = logging.getLogger('edreplicate')


class Conductor:
    _lock = threading.Lock()

    def __init__(self, settings, player_task):

        self._settings = settings
        if not self._lock.acquire(timeout=int(settings["thread.lock_timeout"])):
            raise ConductorTimeoutException()

        self._player_tracker = PlayerTracker()
        self._player_tracker.clear()
        self._player_tracker.set_migration_in_process(True)
        self._player_task = player_task

    def __enter__(self):
        return self

    def __exit__(self, exc_type, value, tb):
        if self._player_tracker:
            self._player_tracker.set_migration_in_process(False)
        if self._lock.locked():
            self._lock.release()

    def __del__(self):
        if hasattr(self, '_player_tracker'):
            self._player_tracker.set_migration_in_process(False)
        if self._lock.locked():
            self._lock.release()

    def send_reset_players(self):
        group_ids = self._player_tracker.get_player_ids()
        if group_ids:
            self._player_task.apply_async((Constants.COMMAND_RESET_PLAYERS, group_ids),
                                          exchange=self._settings['player.exchange'])  # @UndefinedVariable
            self._log(Constants.COMMAND_RESET_PLAYERS, None, group_ids)
            for my_id in group_ids:
                self._player_tracker.reset_player(my_id)
        else:
            logger.debug('Command[{}] was not sent because there is no registered players'.format(
                Constants.COMMAND_RESET_PLAYERS))

    def accept_players(self):
        self._player_tracker.set_accept_player(True)

    def reject_players(self):
        self._player_tracker.set_accept_player(False)

    def find_players(self):
        self._player_task.apply_async((Constants.COMMAND_REGISTER_PLAYER, None),
                                      exchange=self._settings['player.exchange'])
        self._log(Constants.COMMAND_REGISTER_PLAYER, None, None)

    def get_player_ids(self):
        return self._player_tracker.get_player_ids()

    def grouping_players(self):
        player_ids = self._player_tracker.get_player_ids()
        if player_ids:
            for idx in range(len(player_ids)):
                # set group A for "0" or group B for "1"
                self._player_tracker.set_player_group(player_ids[idx],
                                                      Constants.PLAYER_GROUP_A if idx % 2 == 0 else Constants.PLAYER_GROUP_B)

    def send_disconnect_PGPool(self, player_group=None):
        group_ids = self._player_tracker.get_player_ids(player_group=player_group)
        self._player_task.apply_async((Constants.COMMAND_DISCONNECT_PGPOOL, group_ids),
                                      exchange=self._settings['player.exchange'])  # @UndefinedVariable
        self._log(Constants.COMMAND_DISCONNECT_PGPOOL, player_group, group_ids)

    def send_connect_PGPool(self, player_group=None):
        group_ids = self._player_tracker.get_player_ids(player_group=player_group)
        self._player_task.apply_async((Constants.COMMAND_CONNECT_PGPOOL, group_ids),
                                      exchange=self._settings['player.exchange'])  # @UndefinedVariable
        self._log(Constants.COMMAND_CONNECT_PGPOOL, player_group, group_ids)

    def send_stop_replication(self, player_group=None):
        group_ids = self._player_tracker.get_player_ids(player_group=player_group)
        self._player_task.apply_async((Constants.COMMAND_STOP_REPLICATION, group_ids),
                                      exchange=self._settings['player.exchange'])  # @UndefinedVariable
        self._log(Constants.COMMAND_STOP_REPLICATION, player_group, group_ids)

    def send_start_replication(self, player_group=None):
        group_ids = self._player_tracker.get_player_ids(player_group=player_group)
        self._player_task.apply_async((Constants.COMMAND_START_REPLICATION, group_ids),
                                      exchange=self._settings['player.exchange'])  # @UndefinedVariable
        self._log(Constants.COMMAND_START_REPLICATION, player_group, group_ids)

    def wait_PGPool_disconnected(self, player_group=None, timeout=30):
        self._wait_for_status(player_group, timeout, self._player_tracker.is_pgpool_disconnected)

    def wait_PGPool_connected(self, player_group=None, timeout=30):
        self._wait_for_status(player_group, timeout, self._player_tracker.is_pgpool_connected)

    def wait_replication_stopped(self, player_group=None, timeout=30):
        self._wait_for_status(player_group, timeout, self._player_tracker.is_replication_stopped)

    def wait_replication_started(self, player_group=None, timeout=30):
        self._wait_for_status(player_group, timeout, self._player_tracker.is_replication_started)

    def monitor_replication_status(self, player_group=None):
        group_ids = self._player_tracker.get_player_ids(player_group=player_group)
        replication_monitor(group_ids, self._settings)

    def _wait_for_status(self, player_group, timeout, func):
        group_ids = self._player_tracker.get_player_ids(player_group=player_group)
        start_time = time.time()
        for node_id in group_ids:
            while not func(node_id):
                if time.time() - start_time > timeout:
                    raise ConductorTimeoutException('{} timeout'.format(func.__qualname__))
                time.sleep(1)
        ids = ', '.join(str(x) for x in group_ids) if group_ids else None
        logger.debug('function[{f}] returned [{ids}]'.format(f=func.__qualname__, ids=ids))

    @staticmethod
    def _log(command, player_group, group_ids):
        if group_ids is None:
            group_ids = []
        logger.debug('Sent command[{cmd}] to group name[{player_group}] ids[{ids}]'.format(
            cmd=command,
            player_group=player_group if player_group else None,
            ids=', '.join(str(x) for x in group_ids)
        ))
