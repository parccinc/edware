import logging
from edreplicate.utils.constants import Constants
from kombu import Connection, Queue, Exchange
from kombu.mixins import ConsumerMixin
from edreplicate.conductor.player_tracker import PlayerTracker
import threading


logger = logging.getLogger('edreplicate')


class WorkerThread(threading.Thread):

    def __init__(self, settings):
        super().__init__()
        self._worker = Worker(settings)

    def run(self):
        self._worker.run()

    def stop(self):
        self._worker.should_stop = True


class Worker(ConsumerMixin):

    '''
    Consume messages from players
    '''

    def __init__(self, settings):
        self.connection = Connection(settings['celery.broker_url'])
        self._player_tracker = PlayerTracker()
        self._CONSUMER_COMMAND_HANDLERS = {
            Constants.ACK_COMMAND_FIND_PLAYER: self._player_tracker.add_player,
            Constants.ACK_COMMAND_START_REPLICATION: self._player_tracker.set_replication_started,
            Constants.ACK_COMMAND_CONNECT_PGPOOL: self._player_tracker.set_pgpool_connected,
            Constants.ACK_COMMAND_STOP_REPLICATION: self._player_tracker.set_replication_stopped,
            Constants.ACK_COMMAND_DISCONNECT_PGPOOL: self._player_tracker.set_pgpool_disconnected
        }
        self._exchange = Exchange(settings['conductor.exchange'], type='direct')
        self._queue = Queue(settings['conductor.queue'], exchange=self._exchange, routing_key=settings[
                            'conductor.routing_key'], durable=False)

    def get_consumers(self, Consumer, channel):
        consumer = Consumer(self._queue, callbacks=[self.on_message])
        consumer.purge()
        return [consumer]

    def on_message(self, body, message):
        try:
            message_ack_command = body[Constants.MESSAGE_ACK_COMMAND]
            node_id = body[Constants.MESSAGE_NODE_ID]
            logger.debug('Message Received from node_id[{n_id}] message[{msg}]'.format(
                n_id=node_id, msg=message_ack_command))
            function = self._CONSUMER_COMMAND_HANDLERS.get(message_ack_command)
            if function:
                function(node_id)
            else:
                logger.debug('No handler for message[{msg}] from node_id[{n_id}]'.format(
                    msg=message_ack_command, n_id=node_id))
        except Exception as e:
            logger.error(e)
        finally:
            message.ack()
