"""
This is a test harness for testing the Player component.  It needs to run on the Slave database server.

It is invoked as follows:

celeryd -c 1 --loglevel=ERROR --app=tests.dev_test_harness.test_player -n player_1 --logfile=/tmp/edreplicate_player.log --pidfile=/tmp/edreplicate_player.pid

Remember to update the -n parameter to be unique for each player.

Requires the following in ini file:

    starmigrate:
      tenant: cat
      analytics_schema: analytics
      starmigrate_schema: starmigrate
      source:  # Information about the Reporting Master
        db:
          cat:
            url: postgresql+psycopg2://edware:edware2013@localhost:5432/edware
            schema_name: edware_prod
          dog:
            url: postgresql+psycopg2://edware:edware2013@localhost:5432/edware
            schema_name: edware_prod
      dest: # Info about the Analytics Master
        db:
          cat:
            url: postgresql+psycopg2://edware:edware2013@localhost:5432/analytics
          dog:
            url: postgresql+psycopg2://edware:edware2013@localhost:5432/analytics
      edreplicate:
        conductor:
          exchange: starmigrate_conductor
          queue: starmigrate_conductor
          routing_key: starmigrate.conductor
          player_find_time_wait: 5
          thread:
            lock_timeout: 60
        player:
          task_name: starmigrate.tasks.player
          exchange: starmigrate_players
          queue: starmigrate_players
          routing_key: starmigrate.players
        replication_monitor:  # Info about the Analytics Master
          db:
            cat:
              url: postgresql+psycopg2://edware:edware2013@localhost:5432/analytics
              schema_name: repmgr_analytics_pg_cluster
            dog:
              url: postgresql+psycopg2://edware:edware2013@localhost:5432/analytics
              schema_name: repmgr_analytics_pg_cluster
          replication_lag_tolerance: 100
          apply_lag_tolerance: 100
          time_lag_tolerance: 100
          timeout: 28800
        celery:
          cat:
            BROKER_URL: amqp://edware_cat:edware1234@localhost/starmigrate_cat
          dog:
            BROKER_URL: amqp://edware_dog:edware1234@localhost/starmigrate_dog
          CELERY_RESULT_BACKEND: amqp
          CELERY_DEFAULT_QUEUE: celery
          CELERY_DEFAULT_ROUTING_KEY: default
          CELERY_DEFAULT_EXCHANGE_TYPE: direct
          CELERY_QUEUES:
            -   name: starmigrate_players
                exchange: fanout
                key: starmigrate_players
                durable: False
          CELERY_ROUTES:
            - starmigrate.tasks.player:  # Must match player.task_name
                queue: starmigrate_players
        thread:
          lock_timeout: 60


"""

import logging
import logging.config
import configparser
from copy import deepcopy
from celery import Celery
from edworker.celery import setup_celery
from edcore.database import initialize_db
from edreplicate.database.repmgr_connector import RepMgrDBConnection
from edreplicate.utils.constants import Constants
from edreplicate.utils.misc import extract_settings
from edreplicate.player.player import Player, player_task


def read_ini(file):
    config = configparser.ConfigParser()
    config.read(file)
    return config['app:main']

ini_file = '/opt/edware/conf/smarter.ini'

logging.config.fileConfig(ini_file)
settings = deepcopy(Constants.DEFAULT_SETTINGS)
settings.update(extract_settings(read_ini(ini_file), 'starmigrate.edreplicate', 'cat'))

celery = Celery()
setup_celery(celery, settings, 'celery')

# Initialize Player
initialize_db(RepMgrDBConnection, settings)
player = Player(settings)


@celery.task(name=settings['player.task_name'], ignore_result=True)
def celery_task(command, node):
    player_task(command, node)
