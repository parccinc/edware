import logging
import time
from copy import deepcopy
from edreplicate.utils.constants import Constants
from edreplicate.utils.misc import extract_settings
from edreplicate.utils.exceptions import NoPlayersFoundException
from edcore.database import initialize_db
from edreplicate.database.repmgr_connector import RepMgrDBConnection
from celery import Celery
from edworker.celery import setup_celery
from edreplicate.conductor.worker import WorkerThread
from edreplicate.conductor.conductor import Conductor
from edreplicate.conductor.player_tracker import PlayerTracker
from edreplicate.player.player import player_task

logger = logging.getLogger('edreplicate')
admin_logger = logging.getLogger('edreplicate_admin_logger')


class EdReplicateConductor():

    """
    The EdReplicateConductor object coordinates the Master/Slave refresh process.

    It uses the following config values, which are given as a dict when it's instantiated.

    config names need to be in parent.child format, example: conductor.exchange

        conductor:
          exchange: starmigrate_players  # The exchange that the conductor uses to send commands to the Players.
          queue: starmigrate_players # The queue
          routing_key: starmigrate.players # The routing key
          player_find_time_wait: 5  # Number of seconds to wait for Players to respond to a FIND request
        player:
          task_name: starmigrate.tasks.player # The name by which Celery identifies the task.  This should match the
                                              # name in the CELERY_ROUTES entry
        replication_monitor:
          db:
            schema_name: repmgr_analytics_pg_cluster  # The name of the schema where the RepMgr status tables and views live.
            url: 'postgresql+psycopg2://edware:edware2013@localhost:5432/analytics'  # The url of the Master database
          replication_lag_tolerance: 100 # The allowed replication difference, in bytes, between the master and slave database when refresh is complete.
          apply_lag_tolerance: 100 # The allowed apply difference, in bytes, when refresh is complete.
          time_lag_tolerance: 100 # The allowed difference, in seconds, between the current time and the latest monitor event
          timeout: 28800  # Time (seconds) allowed for a replication to complete
        celery:
          BROKER_URL: amqp://localhost/starmigrate_cat  # URL of the RabbitMQ server
          CELERY_RESULT_BACKEND: amqp
          CELERY_DEFAULT_QUEUE: celery
          CELERY_DEFAULT_ROUTING_KEY: default
          CELERY_DEFAULT_EXCHANGE_TYPE: direct
          CELERY_QUEUES:
            -   name: starmigrate_players  # Should match the conductor settings
                exchange: fanout
                key: starmigrate_players
                durable: False
          CELERY_ROUTES:
            - starmigrate.tasks.player:  # Must match player.task_name
                queue: starmigrate_players # Must match conductor.queue
        thread:
          lock_timeout: 60  # How many seconds to wait to get a thread lock.  Used by the Consumer thread that processes Player responses.
    """

    def __init__(self, settings, prefix=None, tenant=None):
        """
        Create a new EdReplicate object.

        :param settings dict: Dict with settings to use
        :param prefix str : Prefix that designates a subset of settings to user (optional)
        :param tenant str: Designate the tenant (optional)

        """

        # Get my settings
        self._settings = deepcopy(Constants.DEFAULT_SETTINGS)
        self._settings.update(extract_settings(settings, prefix, tenant))

        # Setup Celery for conductor
        self._celery = Celery()
        setup_celery(self._celery, self._settings, 'celery')

        @self._celery.task(name=self._settings['player.task_name'], ignore_result=True)
        def celery_task(command, nodes):
            player_task(command, nodes)

        # Initialize Replication Manager DB Connection
        initialize_db(RepMgrDBConnection, self._settings)

        # Setup Conductor
        self._conductor = Conductor(self._settings, celery_task)

        # Setup and start Consumer Thread to handle Player responses
        self._consumer_thread = WorkerThread(self._settings)
        self._consumer_thread.start()

        # Find Players
        self._conductor.accept_players()
        self._conductor.find_players()
        time.sleep(int(self._settings['conductor.player_find_time_wait']))
        self._conductor.reject_players()
        self._conductor.send_reset_players()
        player_ids = self._conductor.get_player_ids()
        if not player_ids:
            logger.info('No player was detected')
            admin_logger.info('No player was detected by the conductor')
            self._consumer_thread.stop()
            raise NoPlayersFoundException

    def __del__(self):
        self._consumer_thread.stop()

    def pre_update(self):
        """
        Prepares the Master/Slave cluster for updates.
        """

        logger.debug('edreplicate pre_update started')

        # Pre-update is only run if there's more than one player
        player_ids = self._conductor.get_player_ids()
        player_count = len(player_ids)

        try:
            if player_count > 1:
                logger.debug('Pre-update step 1 of 5 - Grouping Players')
                self._conductor.grouping_players()
                logger.debug('Pre-update step 2 of 5 - Disconnect group A from PGPool')
                self._conductor.send_disconnect_PGPool(player_group=Constants.PLAYER_GROUP_A)
                logger.debug('Pre-update step 3 of 5 - Confirm group A PGPool disconnect')
                self._conductor.wait_PGPool_disconnected(player_group=Constants.PLAYER_GROUP_A)
                logger.debug('Pre-update step 4 of 5 - Stop group B replication')
                self._conductor.send_stop_replication(player_group=Constants.PLAYER_GROUP_B)
                logger.debug('Pre-update step 5 of 5 - Confirm group B replication stopped')
                self._conductor.wait_replication_stopped(player_group=Constants.PLAYER_GROUP_B)
        except Exception as e:
            logger.error('Pre-update failed, sending reset request to all players')
            self._conductor.send_reset_players()
            logger.error('Pre-update: error')
            logger.error(e)
            admin_logger.error('Error detected by the conductor during migration')
        finally:
            logger.debug('edreplicate pre_update has completed')

    def post_update(self):

        logger.debug('edreplicate post_update started')

        # Only a small portion of Post-update is run if there's only one player
        player_ids = self._conductor.get_player_ids()
        player_count = len(player_ids)

        try:
            if player_count == 1:
                logger.debug('Single-player : Confirm replication complete')
                self._conductor.monitor_replication_status()

            elif player_count > 1:
                logger.debug('Post-update step 1 of 10 : Confirm group A replication complete')
                self._conductor.monitor_replication_status(player_group=Constants.PLAYER_GROUP_A)
                logger.debug('Post-update step 2 of 10 : Connect group A to PGPool')
                self._conductor.send_connect_PGPool(player_group=Constants.PLAYER_GROUP_A)
                logger.debug('Post-update step 3 of 10 : Confirm group A PGPool connect')
                self._conductor.wait_PGPool_connected(player_group=Constants.PLAYER_GROUP_A)
                logger.debug('Post-update step 4 of 10 : Disconnect group B from PGPool')
                self._conductor.send_disconnect_PGPool(player_group=Constants.PLAYER_GROUP_B)
                logger.debug('Post-update step 5 of 10 : Confirm group B PGPool disconnect')
                self._conductor.wait_PGPool_disconnected(player_group=Constants.PLAYER_GROUP_B)
                logger.debug('Post-update step 6 of 10 : Start group B replication')
                self._conductor.send_start_replication(player_group=Constants.PLAYER_GROUP_B)
                logger.debug('Post-update step 7 of 10 : Confirm group B replication started')
                self._conductor.wait_replication_started(player_group=Constants.PLAYER_GROUP_B)
                logger.debug('Post-update step 8 of 10 : Confirm group B replication complete')
                self._conductor.monitor_replication_status(player_group=Constants.PLAYER_GROUP_B)
                logger.debug('Post-update step 9 of 10 : Connect group B to PGPool')
                self._conductor.send_connect_PGPool(player_group=Constants.PLAYER_GROUP_B)
                logger.debug('Post-update step 10 of 10 : Confirm group B PGPool connect')
                self._conductor.wait_PGPool_connected(player_group=Constants.PLAYER_GROUP_B)
        except Exception as e:
            logger.error('regular_process: failed to migrate, sending reset request to all players')
            self._conductor.send_reset_players()
            logger.error('regular_process: error')
            logger.error(e)
            admin_logger.error('Error detected by the conductor during migration')
        finally:
            logger.debug('edreplicate post_update has completed')

        self._consumer_thread.stop()
