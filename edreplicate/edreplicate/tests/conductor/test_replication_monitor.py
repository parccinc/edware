from edreplicate.utils.exceptions import (
    NoReplicationToMonitorException,
    ReplicationToMonitorOrphanNodeException,
    ReplicationToMonitorOutOfSyncException
)
from edreplicate.conductor.replication_monitor import replication_monitor
from edreplicate.tests.shared import UnittestWithRepmgrSqlite
from edreplicate.utils.constants import Constants
import unittest
import time


class Test(UnittestWithRepmgrSqlite):

    @classmethod
    def setUpClass(cls):
        UnittestWithRepmgrSqlite.setUpClass()
        cls._settings = Constants().DEFAULT_SETTINGS
        cls._settings.update({'migrate.celery.CELERY_ALWAYS_EAGER': True})

    def test_replication_monitor_no_ids_exist_at_all(self):
        self.assertRaises(NoReplicationToMonitorException, replication_monitor, [100, 101, 102], settings=self._settings)
        self.assertRaises(NoReplicationToMonitorException, replication_monitor, [103], settings=self._settings)

    def test_replication_monitor_timeout(self):
        self._settings['replication_monitor.timeout'] = 5
        start_time = time.time()
        self.assertRaises(ReplicationToMonitorOutOfSyncException, replication_monitor, [2, 3, 4], settings=self._settings)
        end_time = time.time()
        self.assertTrue(end_time - start_time > self._settings['replication_monitor.timeout'])

    def test_replication_monitor_replication_lag_tolerance(self):
        self._settings['replication_monitor.timeout'] = 1
        self.assertRaises(ReplicationToMonitorOutOfSyncException, replication_monitor, [5], self._settings)
        self._settings['replication_monitor.replication_lag_tolerance'] = 1050
        rtn = replication_monitor([5], self._settings)
        self.assertTrue(rtn)

    def test_replication_monitor_apply_lag(self):
        self._settings['replication_monitor.timeout'] = 1
        self.assertRaises(ReplicationToMonitorOutOfSyncException, replication_monitor, [6], self._settings)
        self._settings['replication_monitor.apply_lag_tolerance'] = 1050
        rtn = replication_monitor([6], self._settings)
        self.assertTrue(rtn)

if __name__ == "__main__":
    unittest.main()
