from edreplicate.utils.exceptions import (
    ConductorTimeoutException,
    PlayerDelayedRegistrationException
)
from edreplicate.settings.config import (
    setup_celery,
    celery
)
from edreplicate.conductor.player_tracker import PlayerTracker
from edreplicate.tests.shared import UnittestWithPlayer
from edreplicate.conductor.conductor import Conductor
from edreplicate.utils.constants import Constants
from edreplicate.utils.misc import get_broker_url
from edreplicate.player.player import player_task
from kombu.connection import Connection
from edreplicate.tests import shared
from queue import Empty
import unittest


class Test(UnittestWithPlayer):
    _consumerThread = None
    _connection = None
    _queue = None

    @classmethod
    def setUpClass(cls):
        UnittestWithPlayer.setUpClass()
        cls._settings = Constants().DEFAULT_SETTINGS
        cls._settings.update({'migrate.celery.CELERY_ALWAYS_EAGER': True})
        cls._celery = celery
        setup_celery(cls._settings)
        Test._connection = Connection(get_broker_url())
        Test._queue = Test._connection.SimpleQueue(shared.queue, no_ack=True)

    @classmethod
    def tearDownClass(cls):
        Test._queue.close()
        Test._connection.close()

    def setUp(self):
        PlayerTracker().clear()
        self.tenant = 'cat'

    def test_conductor_lock(self):
        tested = False
        self._settings.update({'thread.lock_timeout': 1})

        @self._celery.task(name=self._settings['player.task_name'], ignore_result=True)
        def celery_task(command, nodes):
            player_task(command, nodes)

        with Conductor(self._settings, celery_task) as conductor:
            self.assertRaises(ConductorTimeoutException, Conductor, self._settings, player_task)
            tested = True
        self.assertTrue(tested)

    def test_accept_players(self):
        @self._celery.task(name=self._settings['player.task_name'], ignore_result=True)
        def celery_task(command, nodes):
            player_task(command, nodes)

        with Conductor(self._settings, celery_task) as conductor:
            conductor.accept_players()
            PlayerTracker().add_player(12345)
            ids = conductor.get_player_ids()
        self.assertEqual(1, len(ids))
        self.assertEqual(12345, ids[0])

    def test_reject_players(self):
        tested = False

        @self._celery.task(name=self._settings['player.task_name'], ignore_result=True)
        def celery_task(command, nodes):
            player_task(command, nodes)

        with Conductor(self._settings, celery_task) as conductor:
            conductor.reject_players()
            playerTracker = PlayerTracker()
            self.assertRaises(PlayerDelayedRegistrationException, playerTracker.add_player, 123123)
            tested = True
        self.assertTrue(tested)

    def test_find_players(self):
        @self._celery.task(name=self._settings['player.task_name'], ignore_result=True)
        def celery_task(command, nodes):
            player_task(command, nodes)

        with Conductor(self._settings, celery_task) as conductor:
            conductor.find_players()
            message = Test._queue.get(timeout=5)
        self.assertEqual(Constants.ACK_COMMAND_FIND_PLAYER, message.payload[Constants.MESSAGE_ACK_COMMAND])
        self.assertEqual(222, message.payload[Constants.MESSAGE_NODE_ID])

    def test_get_player_ids(self):
        tested = False

        @self._celery.task(name=self._settings['player.task_name'], ignore_result=True)
        def celery_task(command, nodes):
            player_task(command, nodes)

        with Conductor(self._settings, celery_task) as conductor:
            playerTracker = PlayerTracker()
            playerTracker.set_accept_player(True)
            playerTracker.add_player(123)
            playerTracker.add_player(222)
            playerTracker.add_player(333)
            playerTracker.add_player(444)
            playerTracker.set_accept_player(False)
            self.assertRaises(PlayerDelayedRegistrationException, playerTracker.add_player, 123123)
            ids = conductor.get_player_ids()
            self.assertEqual(4, len(ids))
            self.assertIn(123, ids)
            self.assertIn(222, ids)
            self.assertIn(333, ids)
            self.assertIn(444, ids)
            ids = playerTracker.get_player_ids(Constants.PLAYER_GROUP_A)
            self.assertEqual(0, len(ids))
            tested = True
        self.assertTrue(tested)

    def test_grouping_players(self):
        tested = False

        @self._celery.task(name=self._settings['player.task_name'], ignore_result=True)
        def celery_task(command, nodes):
            player_task(command, nodes)

        with Conductor(self._settings, celery_task) as conductor:
            playerTracker = PlayerTracker()
            playerTracker.set_accept_player(True)
            playerTracker.add_player(123)
            playerTracker.add_player(222)
            playerTracker.add_player(333)
            playerTracker.add_player(444)
            playerTracker.set_accept_player(False)
            conductor.grouping_players()
            groupA = playerTracker.get_player_ids(Constants.PLAYER_GROUP_A)
            self.assertEqual(2, len(groupA))
            groupB = playerTracker.get_player_ids(Constants.PLAYER_GROUP_B)
            self.assertEqual(2, len(groupB))
            tested = True
        self.assertTrue(tested)

    def test_send_disconnect_PGPool(self):
        @self._celery.task(name=self._settings['player.task_name'], ignore_result=True)
        def celery_task(command, nodes):
            player_task(command, nodes)

        with Conductor(self._settings, celery_task) as conductor:
            conductor.send_disconnect_PGPool()
            self.assertRaises(Empty, Test._queue.get, timeout=1)
            playerTracker = PlayerTracker()
            playerTracker.set_accept_player(True)
            playerTracker.add_player(222)
            playerTracker.set_accept_player(False)
            conductor.send_disconnect_PGPool()
            message = Test._queue.get(timeout=5)
        self.assertEqual(Constants.ACK_COMMAND_DISCONNECT_PGPOOL, message.payload[
                         Constants.MESSAGE_ACK_COMMAND])

    def test_send_connect_PGPool(self):
        @self._celery.task(name=self._settings['player.task_name'], ignore_result=True)
        def celery_task(command, nodes):
            player_task(command, nodes)

        with Conductor(self._settings, celery_task) as conductor:
            conductor.send_connect_PGPool()
            self.assertRaises(Empty, Test._queue.get, timeout=1)
            playerTracker = PlayerTracker()
            playerTracker.set_accept_player(True)
            playerTracker.add_player(222)
            playerTracker.set_accept_player(False)
            conductor.send_connect_PGPool()
            message = Test._queue.get(timeout=5)
        self.assertEqual(Constants.ACK_COMMAND_CONNECT_PGPOOL,
                         message.payload[Constants.MESSAGE_ACK_COMMAND])

    def test_send_stop_replication(self):
        @self._celery.task(name=self._settings['player.task_name'], ignore_result=True)
        def celery_task(command, nodes):
            player_task(command, nodes)

        with Conductor(self._settings, celery_task) as conductor:
            conductor.send_stop_replication()
            self.assertRaises(Empty, Test._queue.get, timeout=1)
            playerTracker = PlayerTracker()
            playerTracker.set_accept_player(True)
            playerTracker.add_player(222)
            playerTracker.set_accept_player(False)
            conductor.send_stop_replication()
            message = Test._queue.get(timeout=5)
        self.assertEqual(Constants.ACK_COMMAND_STOP_REPLICATION, message.payload[
                         Constants.MESSAGE_ACK_COMMAND])

    def test_send_start_replication(self):
        @self._celery.task(name=self._settings['player.task_name'], ignore_result=True)
        def celery_task(command, nodes):
            player_task(command, nodes)

        with Conductor(self._settings, celery_task) as conductor:
            conductor.send_start_replication()
            self.assertRaises(Empty, Test._queue.get, timeout=1)
            playerTracker = PlayerTracker()
            playerTracker.set_accept_player(True)
            playerTracker.add_player(222)
            playerTracker.set_accept_player(False)
            conductor.send_start_replication()
            message = Test._queue.get(timeout=5)
        self.assertEqual(Constants.ACK_COMMAND_START_REPLICATION, message.payload[
                         Constants.MESSAGE_ACK_COMMAND])

    def test_wait_PGPool_disconnected(self):
        tested = False

        @self._celery.task(name=self._settings['player.task_name'], ignore_result=True)
        def celery_task(command, nodes):
            player_task(command, nodes)

        with Conductor(self._settings, celery_task) as conductor:
            playerTracker = PlayerTracker()
            playerTracker.set_accept_player(True)
            playerTracker.add_player(222)
            playerTracker.set_accept_player(False)
            self.assertRaises(ConductorTimeoutException, conductor.wait_PGPool_disconnected, timeout=1)
            playerTracker.set_pgpool_disconnected(222)
            conductor.wait_PGPool_disconnected(timeout=1)
            tested = True
        self.assertTrue(tested)

    def test_wait_PGPool_connected(self):
        tested = False

        @self._celery.task(name=self._settings['player.task_name'], ignore_result=True)
        def celery_task(command, nodes):
            player_task(command, nodes)

        with Conductor(self._settings, celery_task) as conductor:
            playerTracker = PlayerTracker()
            playerTracker.set_accept_player(True)
            playerTracker.add_player(222)
            playerTracker.set_accept_player(False)
            self.assertRaises(ConductorTimeoutException, conductor.wait_PGPool_connected, timeout=1)
            playerTracker.set_pgpool_connected(222)
            conductor.wait_PGPool_connected(timeout=3)
            tested = True
        self.assertTrue(tested)

    def test_wait_replication_stopped(self):
        tested = False

        @self._celery.task(name=self._settings['player.task_name'], ignore_result=True)
        def celery_task(command, nodes):
            player_task(command, nodes)

        with Conductor(self._settings, celery_task) as conductor:
            playerTracker = PlayerTracker()
            playerTracker.set_accept_player(True)
            playerTracker.add_player(222)
            playerTracker.set_accept_player(False)
            self.assertRaises(ConductorTimeoutException, conductor.wait_replication_stopped, timeout=1)
            playerTracker.set_replication_stopped(222)
            conductor.wait_replication_stopped(timeout=1)
            tested = True
        self.assertTrue(tested)

    def test_wait_replication_started(self):
        tested = False

        @self._celery.task(name=self._settings['player.task_name'], ignore_result=True)
        def celery_task(command, nodes):
            player_task(command, nodes)

        with Conductor(self._settings, celery_task) as conductor:
            playerTracker = PlayerTracker()
            playerTracker.set_accept_player(True)
            playerTracker.add_player(222)
            playerTracker.set_accept_player(False)
            self.assertRaises(ConductorTimeoutException, conductor.wait_replication_started, timeout=1)
            playerTracker.set_replication_started(222)
            conductor.wait_replication_started(timeout=1)
            tested = True
        self.assertTrue(tested)

    def test_send_reset_players(self):
        @self._celery.task(name=self._settings['player.task_name'], ignore_result=True)
        def celery_task(command, nodes):
            player_task(command, nodes)

        with Conductor(self._settings, celery_task) as conductor:
            conductor.send_reset_players()
            self.assertRaises(Empty, Test._queue.get, timeout=1)
            playerTracker = PlayerTracker()
            playerTracker.set_accept_player(True)
            playerTracker.add_player(222)
            playerTracker.set_accept_player(False)
            conductor.send_reset_players()
            message = Test._queue.get(timeout=5)
        self.assertEqual(Constants.ACK_COMMAND_RESET_PLAYERS,
                         message.payload[Constants.MESSAGE_ACK_COMMAND])


if __name__ == "__main__":
    unittest.main()
