from edreplicate.utils.exceptions import (
    PlayerAlreadyRegisteredException,
    PlayerNotRegisteredException,
    PlayerStatusTimedoutException,
    PlayerDelayedRegistrationException
)
from edreplicate.settings.config import (
    setup_celery,
    celery
)
from edreplicate.conductor.player_tracker import PlayerTracker
from edreplicate.conductor.conductor import Conductor
from edreplicate.utils.constants import Constants
import unittest
import time


class Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._settings = Constants().DEFAULT_SETTINGS
        cls._settings.update({'migrate.celery.CELERY_ALWAYS_EAGER': True})
        cls._celery = celery
        setup_celery(cls._settings)

    def setUp(self):
        playertracker1 = PlayerTracker()
        playertracker1.clear()
        playertracker1.set_accept_player(True)
        playertracker1.add_player(123)
        playertracker1.set_accept_player(False)
        self.tenant = 'cat'

    def test_singletone(self):
        playertracker1 = PlayerTracker()
        playertracker2 = PlayerTracker()
        self.assertTrue(playertracker1 == playertracker2)

    def test_add_player(self):
        playertracker1 = PlayerTracker()
        playertracker2 = PlayerTracker()
        players = playertracker2.get_player_ids()
        self.assertEqual(1, len(players))
        playertracker1.set_accept_player(True)
        self.assertRaises(PlayerAlreadyRegisteredException, playertracker1.add_player, 123)
        playertracker1.add_player(2)
        playertracker1.set_accept_player(False)
        players = playertracker2.get_player_ids()
        self.assertEqual(2, len(players))
        self.assertIn(123, players)
        self.assertIn(2, players)

    def test_set_pgpool_connected(self):
        playertracker1 = PlayerTracker()
        self.assertFalse(playertracker1.is_pgpool_connected(123))
        self.assertFalse(playertracker1.is_pgpool_disconnected(123))
        playertracker1.set_pgpool_disconnected(123)
        self.assertFalse(playertracker1.is_pgpool_connected(123))
        self.assertTrue(playertracker1.is_pgpool_disconnected(123))

    def test_set_pgpool_disconnected(self):
        playertracker1 = PlayerTracker()
        playertracker1.set_pgpool_disconnected(123)
        self.assertFalse(playertracker1.is_pgpool_connected(123))
        self.assertTrue(playertracker1.is_pgpool_disconnected(123))
        playertracker1.set_pgpool_connected(123)
        self.assertTrue(playertracker1.is_pgpool_connected(123))
        self.assertFalse(playertracker1.is_pgpool_disconnected(123))

    def test_set_master_connected(self):
        playertracker1 = PlayerTracker()
        self.assertFalse(playertracker1.is_replication_started(123))
        self.assertFalse(playertracker1.is_replication_stopped(123))
        playertracker1.set_replication_stopped(123)
        self.assertFalse(playertracker1.is_replication_started(123))
        self.assertTrue(playertracker1.is_replication_stopped(123))

    def test_set_master_disconnected(self):
        playertracker1 = PlayerTracker()
        playertracker1.set_replication_stopped(123)
        self.assertFalse(playertracker1.is_replication_started(123))
        self.assertTrue(playertracker1.is_replication_stopped(123))
        playertracker1.set_replication_started(123)
        self.assertTrue(playertracker1.is_replication_started(123))
        self.assertFalse(playertracker1.is_pgpool_disconnected(123))

    def test_set_pgpool_connected_no_player_exist(self):
        playertracker1 = PlayerTracker()
        self.assertRaises(PlayerNotRegisteredException, playertracker1.set_pgpool_disconnected, 1)

    def test_is_pgpool_connected_no_player_exist(self):
        playertracker1 = PlayerTracker()
        self.assertRaises(PlayerStatusTimedoutException, playertracker1.is_pgpool_disconnected, 1, 1)

    def test_get_player_ids(self):
        playertracker1 = PlayerTracker()
        ids = playertracker1.get_player_ids()
        self.assertEqual(1, len(ids))
        playertracker1.set_accept_player(True)
        playertracker1.add_player(1)
        playertracker1.set_accept_player(False)
        ids = playertracker1.get_player_ids('A')
        self.assertEqual(0, len(ids))
        playertracker1.set_player_group(123, 'A')
        ids = playertracker1.get_player_ids()
        self.assertEqual(2, len(ids))
        ids = playertracker1.get_player_ids('A')
        self.assertEqual(1, len(ids))
        ids = playertracker1.get_player_ids('B')
        self.assertEqual(0, len(ids))

    def test_reset(self):
        playertracker1 = PlayerTracker()
        playertracker2 = PlayerTracker()
        ids = playertracker1.get_player_ids()
        self.assertEqual(1, len(ids))
        playertracker1.clear()
        ids = playertracker1.get_player_ids()
        self.assertEqual(0, len(ids))
        ids = playertracker2.get_player_ids()
        self.assertEqual(0, len(ids))

    def test__set_player_status(self):
        playertracker1 = PlayerTracker()
        playertracker1._set_player_status(123, Constants.PLAYER_PGPOOL_CONNECTION_STATUS, Constants.PLAYER_CONNECTION_STATUS_DISCONNECTED)
        self.assertTrue(playertracker1.is_pgpool_disconnected(123))
        self.assertRaises(PlayerNotRegisteredException, playertracker1._set_player_status, 1, Constants.PLAYER_PGPOOL_CONNECTION_STATUS, Constants.PLAYER_CONNECTION_STATUS_DISCONNECTED)

    def test__is_player_status(self):
        playertracker1 = PlayerTracker()
        self.assertFalse(playertracker1._is_player_status(123, 'test', 'abc'))
        playertracker1._set_player_status(123, 'test', 'abc')
        self.assertTrue(playertracker1._is_player_status(123, 'test', 'abc'))
        timeout = 3
        start_time = time.time()
        self.assertRaises(PlayerStatusTimedoutException, playertracker1._is_player_status, 1, 'test', 'abc', timeout)
        end_time = time.time()
        self.assertTrue(end_time - start_time > timeout)

    def test_set_accept_player(self):
        playertracker1 = PlayerTracker()
        playertracker1.clear()
        ids = playertracker1.get_player_ids()
        self.assertEqual(0, len(ids))
        playertracker1.set_accept_player(True)
        playertracker1.add_player(1)
        playertracker1.add_player(2)
        playertracker1.set_accept_player(False)
        ids = playertracker1.get_player_ids()
        self.assertEqual(2, len(ids))
        self.assertRaises(PlayerDelayedRegistrationException, playertracker1.add_player, 3)
        ids = playertracker1.get_player_ids()
        self.assertEqual(2, len(ids))

    def test_is_migration_in_process(self):
        @self._celery.task(name=self._settings['player.task_name'], ignore_result=True)
        def celery_task(command, nodes):
            player_task(command, nodes)

        playertracker1 = PlayerTracker()
        self.assertFalse(playertracker1.is_migration_in_process())
        with Conductor(self._settings, celery_task) as conductor:
            self.assertTrue(playertracker1.is_migration_in_process())
        self.assertFalse(playertracker1.is_migration_in_process())

    def test_set_timeout(self):
        playertracker1 = PlayerTracker()
        playertracker1.set_timeout(1)
        self.assertEqual(1, playertracker1.get_timeout())
        playertracker1.set_timeout(2)
        self.assertEqual(2, playertracker1.get_timeout())


if __name__ == "__main__":
    unittest.main()
