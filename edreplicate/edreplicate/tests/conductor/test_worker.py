from edreplicate.conductor.player_tracker import PlayerTracker
from edreplicate.conductor.worker import WorkerThread
from edreplicate.utils.constants import Constants
from edreplicate.player import reply_to_conductor
from edreplicate.tests.shared import exchange
from kombu import Connection
import threading
import unittest
import time


class Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._settings = Constants().DEFAULT_SETTINGS
        cls._settings.update({'migrate.celery.CELERY_ALWAYS_EAGER': True,
                              'celery.broker_url': 'memory://',
                              'conductor.exchange': 'test'})
        cls.lock = threading.Lock()

    def setUp(self):
        self.lock.acquire()
        self.__connection = Connection("memory://")
        self.__thread = WorkerThread(self._settings)
        self.__thread.start()
        time.sleep(1)
        PlayerTracker().clear()
        PlayerTracker().set_accept_player(True)

    def tearDown(self):
        self.__thread.stop()
        self.lock.release()

    def test_ACK_COMMAND_FIND_PLAYER(self):
        reply_to_conductor.register_player(112, self.__connection, exchange, self._settings['conductor.routing_key'])
        reply_to_conductor.register_player(115, self.__connection, exchange, self._settings['conductor.routing_key'])
        time.sleep(2)
        player_tracker = PlayerTracker()
        ids = player_tracker.get_player_ids(timeout=5)
        self.assertEqual(2, len(ids))
        self.assertIn(112, ids)
        self.assertIn(115, ids)

    def test_ACK_COMMAND_DISCONNECT_MASTER(self):
        reply_to_conductor.register_player(112, self.__connection, exchange, self._settings['conductor.routing_key'])
        reply_to_conductor.register_player(115, self.__connection, exchange, self._settings['conductor.routing_key'])
        reply_to_conductor.acknowledgement_master_disconnected(112, self.__connection, exchange, self._settings['conductor.routing_key'])
        time.sleep(2)
        player_tracker = PlayerTracker()
        self.assertTrue(player_tracker.is_replication_stopped(112))
        self.assertFalse(player_tracker.is_replication_stopped(115))

    def test_ACK_COMMAND_CONNECT_MASTER(self):
        reply_to_conductor.register_player(112, self.__connection, exchange, self._settings['conductor.routing_key'])
        reply_to_conductor.register_player(115, self.__connection, exchange, self._settings['conductor.routing_key'])
        time.sleep(2)
        player_tracker = PlayerTracker()
        self.assertFalse(player_tracker.is_replication_started(112))
        self.assertFalse(player_tracker.is_replication_started(115))
        reply_to_conductor.acknowledgement_master_disconnected(112, self.__connection, exchange, self._settings['conductor.routing_key'])
        time.sleep(2)
        self.assertTrue(player_tracker.is_replication_stopped(112))
        self.assertFalse(player_tracker.is_replication_started(115))
        reply_to_conductor.acknowledgement_master_connected(112, self.__connection, exchange, self._settings['conductor.routing_key'])
        time.sleep(2)
        self.assertTrue(player_tracker.is_replication_started(112))
        self.assertFalse(player_tracker.is_replication_started(115))

    def test_ACK_COMMAND_DISCONNECT_PGPOOL(self):
        reply_to_conductor.register_player(112, self.__connection, exchange, self._settings['conductor.routing_key'])
        reply_to_conductor.register_player(115, self.__connection, exchange, self._settings['conductor.routing_key'])
        reply_to_conductor.acknowledgement_pgpool_disconnected(112, self.__connection, exchange, self._settings['conductor.routing_key'])
        time.sleep(2)
        player_tracker = PlayerTracker()
        self.assertTrue(player_tracker.is_pgpool_disconnected(112))
        self.assertFalse(player_tracker.is_pgpool_disconnected(115))

    def test_ACK_COMMAND_CONNECT_PGPOOL(self):
        reply_to_conductor.register_player(112, self.__connection, exchange, self._settings['conductor.routing_key'])
        reply_to_conductor.register_player(115, self.__connection, exchange, self._settings['conductor.routing_key'])
        time.sleep(2)
        player_tracker = PlayerTracker()
        self.assertFalse(player_tracker.is_pgpool_connected(112))
        self.assertFalse(player_tracker.is_pgpool_connected(115))
        reply_to_conductor.acknowledgement_pgpool_disconnected(112, self.__connection, exchange, self._settings['conductor.routing_key'])
        time.sleep(2)
        self.assertTrue(player_tracker.is_pgpool_disconnected(112))
        self.assertFalse(player_tracker.is_pgpool_connected(115))
        reply_to_conductor.acknowledgement_pgpool_connected(112, self.__connection, exchange, self._settings['conductor.routing_key'])
        time.sleep(2)
        self.assertTrue(player_tracker.is_pgpool_connected(112))
        self.assertFalse(player_tracker.is_pgpool_connected(115))


if __name__ == "__main__":
    unittest.main()
