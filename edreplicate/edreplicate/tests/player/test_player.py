from edreplicate.utils.exceptions import IptablesCommandError
from edreplicate.tests.shared import UnittestWithRepmgrSqlite
from edreplicate.utils.constants import Constants
from edreplicate.player.player import Player
from mocket.mocket import Mocket
from unittest.mock import patch
import edreplicate.settings.config
import subprocess
import netifaces


class PlayerTaskTest(UnittestWithRepmgrSqlite):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls._settings = Constants().DEFAULT_SETTINGS
        cls._settings.update({'migrate.celery.CELERY_ALWAYS_EAGER': True,
                              'celery.broker_url': 'memory://',
                              'conductor.exchange': 'test'})

    def setUp(self):
        Player._instances.clear()
        self.hostname = 'localhost'
        self.cluster = 'test_cluster'
        with patch('netifaces.interfaces') as interfaces, \
                patch('netifaces.ifaddresses') as ifaddresses, \
                patch('socket.gethostname') as mockhost:
            interfaces.return_value = ['eth0']
            ifaddresses.return_value = {
                netifaces.AF_INET: [{'addr': '127.0.0.3'},
                                    {'addr': '127.0.0.2'},
                                    {'addr': '127.0.0.1'}]
            }
            mockhost.return_value = self.hostname
            self.interfaces = interfaces.start()
            self.ifaddresses = ifaddresses.start()
            self.addCleanup(self.interfaces.stop)
            self.addCleanup(self.ifaddresses.stop)
            self.player = Player(self._settings)
        # turn on mocket
        Mocket.enable()

    def tearDown(self):
        # since Player is a singleton object. we need to destroy it
        # turn off mocket when test is over
        Mocket.disable()

    def test__node_id(self):
        self.assertEqual(3, self.player.node_id)

    def test__hostname(self):
        self.assertEqual(self.hostname, self.player.hostname)

    def test_register_player(self):
        rtn = self.player.register_player()
        self.assertTrue(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, '_modify_rule')
    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    def test_reset_players_succeed(self, mock_check_rule, mock_modify_rule):
        mock_modify_rule.return_value = None
        mock_check_rule.return_value = True
        rtn = self.player.reset_players()
        self.assertTrue(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, '_modify_rule')
    @patch.object(edreplicate.player.iptables.IptablesChecker, 'check_block_input')
    def test_reset_players_with_pgpool_failed(self,
                                              mock_check_block_input,
                                              mock_modify_rule,
                                              mock_check_rule
                                              ):
        mock_check_block_input.return_value = True
        mock_check_rule.return_value = True
        mock_modify_rule.return_value = None
        rtn = self.player.reset_players()
        self.assertFalse(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, '_modify_rule')
    @patch.object(edreplicate.player.iptables.IptablesChecker, 'check_block_output')
    @patch('edreplicate.player.reply_to_conductor.acknowledgement_reset_players')
    def test_reset_players_with_master_failed(self, mock_conductor, mock_check_block_output, mock_modify_rule, mock_check_rule):
        mock_conductor.return_value = lambda: None
        mock_modify_rule.return_value = None
        mock_check_rule.return_value = True
        mock_check_block_output.return_value = True
        rtn = self.player.reset_players()
        self.assertFalse(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, '_modify_rule')
    @patch.object(edreplicate.player.iptables.IptablesChecker, 'check_block_output')
    @patch.object(edreplicate.player.iptables.IptablesChecker, 'check_block_input')
    @patch('edreplicate.player.reply_to_conductor.acknowledgement_reset_players')
    def test_reset_players_with_both_failed(self, mock_conductor, mock_check_block_input,
                                            mock_check_block_output, mock_modify_rule, mock_check_rule):
        mock_conductor.return_value = lambda: None
        mock_modify_rule.return_value = None
        mock_check_block_input.return_value = True
        mock_check_block_output.return_value = True
        mock_check_rule.return_value = True
        rtn = self.player.reset_players()
        self.assertFalse(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_input")
    @patch('edreplicate.player.reply_to_conductor.acknowledgement_pgpool_connected')
    def test_connect_pgpool_succeed(self, mock_conductor, mock_check_block_input, mock_modify_rule, mock_check_rule):
        mock_conductor.return_value = lambda: None
        mock_check_rule.return_value = True
        mock_modify_rule.return_value = None
        mock_check_block_input.return_value = False
        rtn = self.player.connect_pgpool()
        self.assertTrue(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_input")
    @patch('edreplicate.player.reply_to_conductor.acknowledgement_pgpool_connected')
    def test_connect_pgpool_failed(self, mock_conductor, mock_check_block_input, mock_modify_rule, mock_check_rule):
        mock_conductor.return_value = lambda: None
        mock_check_rule.return_value = True
        mock_modify_rule.return_value = None
        mock_check_block_input.return_value = True
        rtn = self.player.connect_pgpool()
        self.assertFalse(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_input")
    @patch('edreplicate.player.reply_to_conductor.acknowledgement_pgpool_disconnected')
    def test_disconnect_pgpool_succeed(self, mock_conductor, mock_check_block_input, mock_modify_rule, mock_check_rule):
        mock_conductor.return_value = lambda: None
        mock_modify_rule.return_value = None
        mock_check_rule.return_value = False
        mock_check_block_input.return_value = True
        rtn = self.player.disconnect_pgpool()
        self.assertTrue(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_input")
    @patch('edreplicate.player.reply_to_conductor.acknowledgement_pgpool_disconnected')
    def test_disconnect_pgpool_failed(self, mock_conductor, mock_check_block_input, mock_modify_rule, mock_check_rule):
        mock_conductor.return_value = lambda: None
        mock_modify_rule.return_value = None
        mock_check_rule.return_value = False
        mock_check_block_input.return_value = True
        rtn = self.player.disconnect_pgpool()
        self.assertTrue(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_output")
    @patch('edreplicate.player.reply_to_conductor.acknowledgement_master_connected')
    def test_connect_master_succeed(self, mock_conductor, mock_block_output, mock_modify_rule, mock_check_rule):
        mock_modify_rule.return_value = None
        mock_check_rule.return_value = True
        mock_block_output.return_value = False
        rtn = self.player.connect_master()
        self.assertTrue(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_output")
    @patch('edreplicate.player.reply_to_conductor.acknowledgement_master_connected')
    def test_connect_master_failed(self, mock_conductor, mock_block_output, mock_modify_rule, mock_check_rule):
        mock_conductor.return_value = lambda: None
        mock_modify_rule.return_value = None
        mock_check_rule.return_value = True
        mock_block_output.return_value = True
        rtn = self.player.connect_master()
        self.assertFalse(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_output")
    @patch('edreplicate.player.reply_to_conductor.acknowledgement_master_disconnected')
    def test_disconnect_master_succeed(self, mock_conductor, mock_block_output, mock_modify_rule, mock_check_rule):
        mock_conductor.return_value = lambda: None
        mock_modify_rule.return_value = None
        mock_block_output.return_value = True
        mock_check_rule.return_value = False
        rtn = self.player.disconnect_master()
        self.assertTrue(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_output")
    @patch('edreplicate.player.reply_to_conductor.acknowledgement_master_disconnected')
    def test_disconnect_master_failed(self, mock_conductor, mock_block_output, mock_modify_rule, mock_check_rule):
        mock_conductor.return_value = lambda: None
        mock_check_rule.return_value = False
        mock_modify_rule.return_value = None
        mock_block_output.return_value = False
        rtn = self.player.disconnect_master()
        self.assertFalse(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_output")
    @patch('edreplicate.player.reply_to_conductor.acknowledgement_master_disconnected')
    def test_run_command_stop_replication_with_node_id_not_in_nodes(self, mock_conductor, mock_block_output,
                                                                    mock_modify_rule, mock_check_rule):
        mock_modify_rule.return_value = None
        mock_block_output.return_value = True
        mock_check_rule.return_value = False
        mock_conductor.return_value = lambda: None
        rtn = self.player.run_command(Constants.COMMAND_STOP_REPLICATION, [])
        self.assertFalse(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_output")
    @patch('edreplicate.player.reply_to_conductor.acknowledgement_master_disconnected')
    def test_run_command_stop_replication_without_nodes(self, mock_conductor, mock_block_output, mock_modify_rule, mock_check_rule):
        mock_modify_rule.return_value = None
        mock_block_output.return_value = True
        mock_check_rule.return_value = False
        mock_conductor.return_value = lambda: None
        rtn = self.player.run_command(Constants.COMMAND_STOP_REPLICATION, None)
        self.assertFalse(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_input")
    @patch('edreplicate.player.reply_to_conductor.acknowledgement_pgpool_disconnected')
    def test_run_command_disconnect_pgpool_with_node_id_not_in_nodes(self, mock_conductor, mock_block_input,
                                                                     mock_modify_rule, mock_check_rule):
        mock_block_input.return_value = True
        mock_modify_rule.return_value = None
        mock_check_rule.return_value = False
        mock_conductor.return_value = lambda: None
        rtn = self.player.run_command(Constants.COMMAND_DISCONNECT_PGPOOL, [])
        self.assertFalse(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_input")
    @patch('edreplicate.player.reply_to_conductor.acknowledgement_pgpool_disconnected')
    def test_run_command_disconnect_pgpool_without_nodes(self, mock_conductor, mock_block_input, mock_modify_rule,
                                                         mock_check_rule):
        mock_block_input.return_value = True
        mock_modify_rule.return_value = None
        mock_check_rule.return_value = False
        mock_conductor.return_value = lambda: None
        rtn = self.player.run_command(Constants.COMMAND_DISCONNECT_PGPOOL, None)
        self.assertFalse(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_input")
    @patch('edreplicate.player.reply_to_conductor.acknowledgement_pgpool_connected')
    def test_run_command_connect_pgpool_with_node_id_in_nodes(self, mock_conductor, mock_block_input,
                                                              mock_modify_rule, mock_check_rule):
        mock_block_input.return_value = False
        mock_modify_rule.return_value = None
        mock_check_rule.return_value = True
        mock_conductor.return_value = lambda: None
        rtn = self.player.run_command(Constants.COMMAND_CONNECT_PGPOOL, [3])
        self.assertTrue(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_input")
    @patch('edreplicate.player.reply_to_conductor.acknowledgement_pgpool_connected')
    def test_run_command_connect_pgpool_with_node_id_not_in_nodes(self, mock_conductor, mock_block_input, mock_modify_rule,
                                                                  mock_check_rule):
        mock_block_input.return_value = False
        mock_modify_rule.return_value = None
        mock_check_rule.return_value = True
        mock_conductor.return_value = lambda: None
        rtn = self.player.run_command(Constants.COMMAND_CONNECT_PGPOOL, [])
        self.assertFalse(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_input")
    @patch('edreplicate.player.reply_to_conductor.acknowledgement_pgpool_connected')
    def test_run_command_connect_pgpool_without_nodes(self, mock_conductor, mock_block_input, mock_modify_rule, mock_check_rule):
        mock_block_input.return_value = False
        mock_modify_rule.return_value = None
        mock_conductor.return_value = lambda: None
        mock_check_rule.return_value = True
        rtn = self.player.run_command(Constants.COMMAND_CONNECT_PGPOOL, None)
        self.assertFalse(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_output")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_input")
    @patch('edreplicate.player.reply_to_conductor.register_player')
    def test_run_command_register_player(self, mock_conductor, mock_block_input, mock_block_output,
                                         mock_modify_rule, mock_check_rule):
        mock_modify_rule.return_value = None
        mock_block_input.return_value = False
        mock_block_output.return_value = False
        mock_check_rule.return_value = True
        mock_conductor.return_value = lambda: None
        rtn = self.player.run_command(Constants.COMMAND_REGISTER_PLAYER, None)
        self.assertTrue(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_output")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_input")
    @patch('edreplicate.player.reply_to_conductor.register_player')
    def test_run_command_register_player_with_node_id(self, mock_conductor, mock_block_input, mock_block_output,
                                                      mock_modify_rule, mock_check_rule):
        mock_modify_rule.return_value = None
        mock_block_input.return_value = False
        mock_block_output.return_value = False
        mock_conductor.return_value = lambda: None
        mock_check_rule.return_value = True
        rtn = self.player.run_command(Constants.COMMAND_REGISTER_PLAYER, [3])
        self.assertTrue(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_output")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_input")
    @patch('edreplicate.player.reply_to_conductor.acknowledgement_reset_players')
    def test_run_command_reset_players(self, mock_conductor, mock_block_input, mock_block_output, mock_modify_rule,
                                       mock_check_rule):
        mock_modify_rule.return_value = None
        mock_block_input.return_value = False
        mock_block_output.return_value = False
        mock_check_rule.return_value = True
        mock_conductor.return_value = lambda: None
        rtn = self.player.run_command(Constants.COMMAND_RESET_PLAYERS, None)
        self.assertTrue(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_output")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_input")
    @patch('edreplicate.player.reply_to_conductor.acknowledgement_reset_players')
    def test_run_command_reset_players_with_node_id(self, mock_conductor, mock_block_input, mock_block_output,
                                                    mock_modify_rule, mock_check_rule):
        mock_modify_rule.return_value = None
        mock_block_input.return_value = False
        mock_block_output.return_value = False
        mock_check_rule.return_value = True
        mock_conductor.return_value = lambda: None
        rtn = self.player.run_command(Constants.COMMAND_RESET_PLAYERS, [3])
        self.assertTrue(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_output")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_input")
    def test_run_command_not_implemented(self, mock_block_input, mock_block_output, mock_modify_rule, mock_check_rule):
        mock_modify_rule.return_value = None
        mock_block_input.return_value = False
        mock_block_output.return_value = False
        mock_check_rule.return_value = True
        rtn = self.player.run_command('Fake Command', None)
        self.assertFalse(rtn)

    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesController, "_modify_rule")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_output")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_input")
    def test_run_command_not_implemented_with_node_id(self, mock_block_input, mock_block_output, mock_modify_rule, mock_check_rule):
        mock_modify_rule.return_value = None
        mock_block_input.return_value = False
        mock_block_output.return_value = False
        mock_check_rule.return_value = True
        rtn = self.player.run_command('Fake Command', [3])
        self.assertFalse(rtn)

    @patch('subprocess.check_output')
    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_output")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_input")
    def test_iptables_command_exception(self, mock_block_input, mock_block_output, mock_check_rule,
                                        MockSubprocess):
        mock_block_input.return_value = True
        mock_block_output.return_value = False
        mock_check_rule.return_value = True
        MockSubprocess.side_effect = subprocess.CalledProcessError(-1, 'iptables')
        self.assertRaises(IptablesCommandError, self.player.run_command,
                          Constants.COMMAND_CONNECT_PGPOOL, [3])

    @patch('subprocess.check_output')
    @patch.object(edreplicate.player.iptables.IptablesController, "_check_rules")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_output")
    @patch.object(edreplicate.player.iptables.IptablesChecker, "check_block_input")
    def test_iptables_save_command_exception(self, mock_block_input, mock_block_output, mock_check_rule,
                                             MockSubprocess):
        mock_block_input.return_value = True
        mock_block_output.return_value = False
        mock_check_rule.return_value = True
        MockSubprocess.side_effect = subprocess.CalledProcessError(-1, 'iptables-save')
        self.assertRaises(IptablesCommandError, self.player.run_command,
                          Constants.COMMAND_CONNECT_PGPOOL, [3])
