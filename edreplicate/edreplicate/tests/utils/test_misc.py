from edreplicate.utils.misc import extract_settings
import unittest


class TestUtilsMisc(unittest.TestCase):

    def test_extract_settings(self):

        # Test tenant key transform
        result = extract_settings(settings={'prefix.key1': 'val1',
                                            'prefix.db.tenant.url': 'val2'},
                                  prefix='prefix',
                                  tenant='tenant')
        expected = {'key1': 'val1',
                    'db.url': 'val2'}
        self.assertEqual(result, expected)

        # Test non-wanted tenant recognition
        result = extract_settings(settings={'prefix.key1': 'val1',
                                            'prefix.db.tenant.url': 'val2',
                                            'prefix.db.tenant2.url': 'val3'},
                                  prefix='prefix',
                                  tenant='tenant')
        expected = {'key1': 'val1',
                    'db.url': 'val2'}
        self.assertEqual(result, expected)

        # Test second pattern
        result = extract_settings(settings={'prefix.key1': 'val1',
                                            'prefix.celery.tenant.broker_url': 'val2',
                                            'prefix.db.tenant2.url': 'val3'},
                                  prefix='prefix',
                                  tenant='tenant')
        expected = {'key1': 'val1',
                    'celery.broker_url': 'val2'}
        self.assertEqual(result, expected)

        result = extract_settings(settings={'prefix.key1': 'val1',
                                            'prefix.celery.tenant.broker_url': 'val2',
                                            'prefix.db.tenant2.url': 'val3',
                                            'prefix.key2.tenant2': 'val3'},
                                  prefix='prefix',
                                  tenant='tenant')
        expected = {'key1': 'val1',
                    'celery.broker_url': 'val2'}
        self.assertEqual(result, expected)

        result = extract_settings(settings={'a.b.tenant.c': 1,
                                            'c.d.tenant': 2})
        expected = {'a.b.tenant.c': 1,
                    'c.d.tenant': 2}
        self.assertEqual(result, expected)

        result = extract_settings(settings={'a.b.tenant.c': 1,
                                            'c.d.tenant': 2},
                                  prefix='a.b')
        expected = {'tenant.c': 1}
        self.assertEqual(result, expected)

        result = extract_settings({})
        expected = {}
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main()
