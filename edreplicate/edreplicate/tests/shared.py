from edschema.tests.database.utils.unittest_with_sqlite import Unittest_with_sqlite
from edreplicate.database.repmgr_connector import RepMgrDBConnection
from sqlalchemy.types import Text, Integer, Interval, DateTime
from sqlalchemy.schema import MetaData, Table, Column
from edreplicate.utils.constants import Constants
from edreplicate.utils.misc import get_broker_url
from edreplicate.player import reply_to_conductor
from edreplicate.player.player import Player
from kombu.entity import Exchange, Queue
from kombu import Connection
import unittest
import datetime
import logging
import os


exchange = Exchange('test', type='direct')
queue = Queue('test', exchange=exchange, routing_key='test.test', durable=False)


class UnittestWithRepmgrSqlite(Unittest_with_sqlite):

    @classmethod
    def setUpClass(cls):
        here = os.path.abspath(os.path.dirname(__file__))
        metadata = generate_repmgr_metadata()
        resources_dir = os.path.abspath(os.path.join(os.path.join(here, 'resources', 'repmgr')))
        super().setUpClass(RepMgrDBConnection.get_datasource_name(), metadata=metadata,
                           resources_dir=resources_dir, use_metadata_from_db=False)

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()


class UnittestWithPlayer(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        mock = MockPlayer()
        Player._instances[Player] = mock

    @classmethod
    def tearDownClass(cls):
        Player._instances.clear()


class MockPlayer(Player):

    def __init__(self):
        self.logger = None
        self.admin_logger = self.logger
        self.connection = Connection(get_broker_url())
        self.exchange = exchange
        self.routing_key = 'test.test'
        self.hostname = 'localhost'
        self.node_id = 222
        self.COMMAND_HANDLERS = {
            Constants.COMMAND_REGISTER_PLAYER: self.register_player,
            Constants.COMMAND_START_REPLICATION: self.connect_master,
            Constants.COMMAND_STOP_REPLICATION: self.disconnect_master,
            Constants.COMMAND_CONNECT_PGPOOL: self.connect_pgpool,
            Constants.COMMAND_DISCONNECT_PGPOOL: self.disconnect_pgpool,
            Constants.COMMAND_RESET_PLAYERS: self.reset_players
        }

    def __enter__(self):
        return self

    def register_player(self):
        reply_to_conductor.register_player(self.node_id, self.connection, self.exchange, self.routing_key)

    def connect_master(self):
        reply_to_conductor.acknowledgement_master_connected(self.node_id, self.connection, self.exchange, self.routing_key)

    def disconnect_master(self):
        reply_to_conductor.acknowledgement_master_disconnected(self.node_id, self.connection, self.exchange, self.routing_key)

    def connect_pgpool(self):
        reply_to_conductor.acknowledgement_pgpool_connected(self.node_id, self.connection, self.exchange, self.routing_key)

    def disconnect_pgpool(self):
        reply_to_conductor.acknowledgement_pgpool_disconnected(self.node_id, self.connection, self.exchange, self.routing_key)

    def reset_players(self):
        reply_to_conductor.acknowledgement_reset_players(self.node_id, self.connection, self.exchange, self.routing_key)


class MockLogger(logging.Logger):

    def __init__(self, *args, **kwargs):
        self.logged = []
        logging.Logger.__init__(self, *args, **kwargs)

    def _log(self, level, msg, args, **kwargs):
        self.logged.append((level, msg, args, kwargs))


def generate_repmgr_metadata(schema_name=None, bind=None):
    metadata = MetaData(schema=schema_name, bind=bind)
    repl_status = Table('repl_status', metadata,
                        Column('primary_node', Integer, nullable=False),
                        Column('standby_node', Integer, nullable=False),
                        Column('last_wal_primary_location', Text(255), nullable=False),
                        Column('last_wal_standby_location', Text(255), nullable=False),
                        Column('replication_lag', Text(255), nullable=False),
                        Column('apply_lag', Text(255), nullable=False),
                        Column('replication_time_lag', Interval, nullable=False, default=datetime.timedelta(0)),
                        Column('last_monitor_time', DateTime(True), nullable=False, default=datetime.datetime.strptime(
                            '2000-01-01 00:00:00', '%Y-%m-%d %H:%M:%S'))
                        )

    repl_nodes = Table('repl_nodes', metadata,
                       Column('id', Integer, primary_key=True),
                       Column('cluster', Text(255), nullable=False),
                       Column('name', Text(255), nullable=False),
                       Column('conninfo', Text(255), nullable=False)
                       )

    return metadata
