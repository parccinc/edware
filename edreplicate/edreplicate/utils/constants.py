class Constants():

    '''
    Constants related to edreplicate
    '''
    LOCALHOST = 'localhost'
    WORKER_NAME = 'edreplicate'
    REPL_STATUS = 'repl_status'
    REPL_NODES = 'repl_nodes'
    REPL_NODE_CONN_INFO = 'conninfo'
    REPL_NODE_CLUSTER = 'cluster'
    ID = 'id'
    REPL_STANDBY_NODE = 'standby_node'
    REPLICATION_LAG = 'replication_lag'
    APPLY_LAG = 'apply_lag'
    TIME_LAG = 'replication_time_lag'
    PLAYER_GROUP_A = 'A'
    PLAYER_GROUP_B = 'B'

    COMMAND_REGISTER_PLAYER = 'FIND_PLAYER'
    COMMAND_DISCONNECT_PGPOOL = 'DISCONNECT_PGPOOL'
    COMMAND_CONNECT_PGPOOL = 'CONNECT_PGPOOL'
    COMMAND_STOP_REPLICATION = 'STOP_REPLICATION'
    COMMAND_START_REPLICATION = 'START_REPLICATION'
    COMMAND_RESET_PLAYERS = 'RESET_PLAYERS'

    ACK_COMMAND_FIND_PLAYER = 'ACK_FIND_PLAYER'
    ACK_COMMAND_DISCONNECT_PGPOOL = 'ACK_DISCONNECT_PGPOOL'
    ACK_COMMAND_CONNECT_PGPOOL = 'ACK_CONNECT_PGPOOL'
    ACK_COMMAND_STOP_REPLICATION = 'ACK_STOP_REPLICATION'
    ACK_COMMAND_START_REPLICATION = 'ACK_START_REPLICATION'
    ACK_COMMAND_RESET_PLAYERS = 'ACK_RESET_PLAYERS'

    MESSAGE_NODE_ID = 'node_id'
    MESSAGE_ACK_COMMAND = 'ack_command'

    PLAYER_GROUP = 'player_group'
    PLAYER_PGPOOL_CONNECTION_STATUS = 'pgpool_connection_status'
    PLAYER_REPLICATION_STATUS = 'replication_status'
    PLAYER_CONNECTION_STATUS_DISCONNECTED = 0
    PLAYER_CONNECTION_STATUS_CONNECTED = 1
    PLAYER_CONNECTION_STATUS_UNKNOWN = 2
    PLAYER_REPLICATION_STATUS_STOPPED = 0
    PLAYER_REPLICATION_STATUS_STARTED = 1
    PLAYER_REPLICATION_STATUS_UNKNOWN = 2
    REPLICATION_STATUS_PAUSE = 't'
    REPLICATION_STATUS_ACTIVE = 'f'
    REPLICATION_STATUS_UNSURE = 'n'
    REPLICATION_CHECK_INTERVAL = 0.001
    REPLICATION_MAX_RETRIES = 100

    IPTABLES_SUDO = '/usr/bin/sudo'
    IPTABLES_CHAIN = 'EDREPLICATE_PGSQL'
    IPTABLES_COMMAND = '/sbin/iptables'
    IPTABLES_SAVE_COMMAND = '/sbin/iptables-save'
    IPTABLES_LIST = '-L'
    IPTABLES_DELETE = '-D'
    IPTABLES_INSERT = '-I'
    IPTABLES_APPEND = '-A'
    IPTABLES_JUMP = '-j'
    IPTABLES_SOURCE = '-s'
    IPTABLES_DEST = '-d'
    IPTABLES_FILTER = 'filter'
    IPTABLES_TABLE = '-t'
    IPTABLES_INPUT_CHAIN = 'INPUT'
    IPTABLES_OUTPUT_CHAIN = 'OUTPUT'
    CONDUCTOR_QUEUE = 'starmigrate_conductor'

    # Setting defaults
    DEFAULT_SETTINGS = {'conductor.exchange': 'edreplicate_conductor',  # Exchange that Conductor listens to for Player responses
                        'conductor.queue': 'edreplicate_conductor',
                        'conductor.routing_key': 'edreplicate.conductor',
                        'conductor.thread.lock_timeout': 60,
                        'conductor.player_find_time_wait': 5,
                        # Exchange that Conductor writes to, and Players listen
                        # to. Has to match celery.celery_queues.name
                        'player.exchange': 'edreplicate_players',
                        'celery.broker_url': 'localhost',
                        'celery.celery_default_exchange_type': 'direct',
                        'celery.celery_default_queue': 'edreplicate',
                        'celery.celery_default_routing_key': 'default',
                        'celery.celery_queues': "[{'exchange': 'fanout', 'name': 'edreplicate_players', 'key': 'edreplicate_players', 'durable': False}]",
                        'celery.celery_result_backend': 'amqp',
                        'celery.celery_routes': "[{'edreplicate.tasks.player': {'queue': 'edreplicate_players'}}]",
                        'thread.lock_timeout': 60,
                        'replication_monitor.thread.lock_timeout': 60,
                        'replication_monitor.replication_lag_tolerance': 100,
                        'replication_monitor.apply_lag_tolerance': 100,
                        'replication_monitor.time_lag_tolerance': 60,
                        'replication_monitor.timeout': 28800,
                        'replication_monitor.interval_check': 1800,
                        'repl_mgr.schema': 'repmgr_edware_pg_cluster',
                        'player.task_name': 'edreplicate.tasks.player'
                        }
