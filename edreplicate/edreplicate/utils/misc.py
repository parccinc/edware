from edreplicate.utils.exceptions import NoMasterFoundException, NoNodeIDFoundException
from edreplicate.settings.config import Config, setup_settings, get_setting
from edreplicate.database.repmgr_connector import RepMgrDBConnection
from edreplicate.utils.constants import Constants
from sqlalchemy.sql.expression import select
from edworker.celery import get_config_file
import configparser
import re


def read_ini(file):
    config = configparser.ConfigParser()
    config.read(file)
    return config['app:main']


def get_broker_url(config=None):
    if config is None:
        config_file = get_config_file()
        if config_file is None:
            config = configparser.ConfigParser()
            config['app:main'] = {}
        else:
            config = read_ini(config_file)

    url = "memory://"

    try:
        celery_always_eager = config.getboolean(Config.EAGER_MODE, False)
    except:
        celery_always_eager = False

    if not celery_always_eager:
        try:
            url = config.get(Config.BROKER_URL, url)
        except:
            pass
    return url


def get_my_master_by_id(my_node_id):
    master_hostname = None
    with RepMgrDBConnection() as conn:
        repl_nodes = conn.get_table(Constants.REPL_NODES)
        repl_status = conn.get_table(Constants.REPL_STATUS)
        query = select([repl_nodes.c.conninfo.label('name')],
                       from_obj=[repl_nodes
                                 .join(repl_status, repl_status.c.primary_node == repl_nodes.c.id)])\
            .where(repl_status.c.standby_node == my_node_id)
        results = conn.get_result(query)
        if results:
            result = results[0]
            node_name = result['name']
            m = re.match('^host=(\S+)\s+', node_name)
            if m:
                master_hostname = m.group(1)
    if not master_hostname:
        raise NoMasterFoundException()
    return master_hostname


def get_node_id_from_hostname(hostname):
    '''
    look up repl_nodes for node_id of the host.
    '''
    node_id = None
    with RepMgrDBConnection() as conn:
        repl_nodes = conn.get_table(Constants.REPL_NODES)
        query = select([repl_nodes.c.id.label('id')],
                       repl_nodes.c.name.like("%" + hostname + "%"),
                       from_obj=[repl_nodes])
        results = conn.get_result(query)
        if results:
            result = results[0]
            node_id = result['id']
    if not node_id:
        raise NoNodeIDFoundException()
    return node_id


def extract_settings(settings, prefix=None, tenant=None) -> dict:
    """
    .. function:: extract_settings(settings[, prefix[, tenant]])

        Utility function that takes a dict of settings, and extracts those that have the designated prefix.

        All keys are lower-cased in the process, to avoid case-mismatch problems later.

        If a value for tenant is supplied, then the tenant portion of the settings is extracted from the sub-tree and added
        to the main block, and settings for other tenants are removed.  They are recognized by looking for specific patterns.

        Example:
        >>> result=extract_settings( { 'prefix.key1' : 'val1',
        ...                            'prefix.key2.tenant.key3' : 'val2'
        ...                          },
        ...                          'prefix',
        ...                          'tenant')
        >>> print(result)
        {'key2.key3': 'val2'}


        :param dict settings: Dict with settings to use
        :param str prefix: Optional prefix that designates a subset of settings to user
        :param str tenant: The tenant whose settings should be added to main set
        :return: A dict with the settings we care about
        :rtype: dict
    """

    _settings = {}

    prefix_components = []
    if prefix:
        prefix_components = prefix.lower().split('.')
    prefix_count = len(prefix_components)

    # Copy input settings to local, dropping all entries that don't match the
    # prefix
    for key, val in settings.items():
        key_components = key.lower().split('.')

        if prefix_components == key_components[:prefix_count]:
            new_key = '.'.join(key_components[prefix_count:])
            _settings[new_key] = val

    # Tenant processing
    # Compile tenant list, seeded with provided tenant
    if tenant:
        tenants = set([tenant])
    else:
        tenants = set()

    for key, val in _settings.items():
        key_components = key.lower().split('.')
        # Look for specific tenant key patterns.  Hard-coded for lack of a better way.
        # ...db.tenant.url
        if len(key_components) >= 3 and key_components[-3] == 'db' and key_components[-1] == 'url':
            tenants.add(key_components[-2])
        # ...celery.tenant.broker_url
        if len(key_components) >= 3 and key_components[-3] == 'celery' and key_components[-1] == 'broker_url':
            tenants.add(key_components[-2])

    # Now that we have a list of tenants, process keys that contain tenant
    # names
    _settings2 = {}
    for key, val in _settings.items():
        key_components = key.lower().split('.')
        is_tenant_setting = False
        for _tenant in tenants:
            if _tenant in key_components:
                is_tenant_setting = True
                # If it's "our" tenant, add it with new key
                if _tenant == tenant:
                    key_components.remove(_tenant)
                    _settings2['.'.join(key_components)] = val
        if not is_tenant_setting:
            _settings2[key] = val

        _settings = _settings2

    return _settings


class Singleton(type):

    """
    This is a metaclass that implements the Singleton pattern.

    See http://stackoverflow.com/questions/6760685/creating-a-singleton-in-python
    """
    _instances = {}

    def __call__(self, *args, **kwargs):
        if self not in self._instances:
            self._instances[self] = super(
                Singleton, self).__call__(*args, **kwargs)
        return self._instances[self]
